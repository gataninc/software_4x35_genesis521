-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FCONFIG.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
-- FPGA Configuration
--            		---+          +-------------------------------------------------
-- nCONFIG (PCI)	   |<-8us---->|
--                     +----------+
--                  ----------+          +--------------------------------------------
-- nSTATUS (FPGA)      | 200ns|          |
--                            +----------+
--                  -----+                                                       +--
-- CONFIG_DONE (FPGA)    |                                                       |
--                       +-------------------------------------------------------+
--                                           +-----+     +-----+     +-----+     +--
-- DCLK (PCI)                                |     |     |     |     |     |     |  
--                  -------------------------+     +-----+     +-----+     +-----+  
--                                      +----------+-----------+-----------+----------+
-- DATA (PCI)       --------------------+          |           |           |          |
--                                      +----------+-----=-----+-----------+----------+
--
-- Step to Initialize FPGA
--	1. PCI must assert nCONFIG low. It shall remain low until:
--        a. nSTATUS rises and CONFIG_DONE is low
--   2. PCI must place data on bit, LSB to MSB @ falling edge of clock, and make sure
--	   it is stable at the rising edge of the clock.
--	3. If nstatus falls during this cycle, then an error has occured.
--
--   History:       <date> - <Author>
--		October 30, 2001 - Simulated in design at 40Mhz operation
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------
entity FCONFIG is 
	port( 
		IMR			: in		std_logic;		-- Master Reset
		CLK20		: in		std_logic;		-- Master Clock
		Config_En		: in		std_logic;
		DATA_LD		: in		std_logic;		-- Load Data from PCI
		CW_LD		: in		std_logic;		-- Load Control Word bit
		PCI_DQ		: in		std_logic_vector( 15 downto 0 );
		nStatus		: in		std_Logic;
		SHIFT_EN		: buffer	std_logic;		-- Shift Enable
		DCLK			: buffer	std_logic;		-- DCLK to FPGA
		DATA			: buffer	std_logic;		-- DATA to FPGA
		NCONFIG		: buffer	std_logic );		-- NCONFIG to FPGA
end FCONFIG;
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
architecture behavioral of FCONFIG is
	
	-- Constant Declarations -----------------------------------------------------------
	constant Zeroes		: std_logic_Vector( 15 downto 0 ) := "0000000000000000";
	constant CONFIG_CNT_MAX 	: integer := 179;	-- 359;		-- (9000ns/25ns)-1
	constant Shift_Cnt_Max  	: integer := 31;

	-- Signal Declarations -------------------------------------------------------------
	signal CONFIG_CNT 		: integer range 0 to Config_Cnt_Max;
	signal Config_Cnt_Tc	: std_logic;
	signal CW_LD_VEC		: std_logic_vector( 1 downto 0 );
	signal DATA_LD_VEC		: std_logic_vector( 1 downto 0 );


	signal iSHIFT_EN		: std_logic;
	signal SHIFT_CNT 		: integer range 0 to shift_cnt_max;
	signal Shift_Cnt_Tc		: std_logic;
	signal FPGA_REG		: std_logic_vector( 15 downto 0 );
	signal iDCLK			: std_logic;
	signal nstatus_Vec		: std_Logic_Vector( 1 downto 0 );	
	

begin
	
	-- Therefore:
	--   The PCI will write to this device, 16 bits at a time, following this, it shall
	--	shift out all 16 bits of data. There shall be a control word to select which
	--   FPGA is to be written as well as the status of the cycle as well as the status
	--   of the shifting operation.
	-- 		FPGA Control Word
	--				Bit 0 : EDS Config Done
	--				Bit 1 : EDS nSTATUS
	--				Bit 2 : EDS Shift in Progress
	--				Bit 3 : EDS nCONFIG Bit ( 1 = Write Low )
	--				Bit 4 : SEM Config Done
	--				Bit 5 : SEM nSTATUS
	--				Bit 6 : SEM Shift in Progress
	--				Bit 7 : SEM nCONFIG Bit ( 1 = Write Low )
	--		FPGA Control Word Definition:
	--				Bit 0 : 
    

	CONFIG_CNT_TC  <= '1' 		when ( Config_Cnt = Config_Cnt_Max ) else '0';
	Shift_Cnt_Tc	<= '1' 		when ( Shift_Cnt = Shift_Cnt_max ) else '0';

     ------------------------------------------------------------------------------------
     clock_proc : process( CLK20, IMR )
	begin
          if( IMR = '1' ) then 
          	NCONFIG		<= '1';
          	iSHIFT_EN		<= '0';
			FPGA_REG 		<= Zeroes( 15 downto 0 );
			shift_cnt		<= 0;
			CONFIG_CNT 	<= 0;
			CW_LD_VEC		<= "00";
			DATA_LD_VEC	<= "00";
			nstatus_Vec	<= "00";
			idclk		<= '0';

			DCLK			<= '0';
			DATA			<= '0';
		
          elsif(( CLK20'Event ) And ( CLK20 = '1' )) then
			CW_LD_VEC 	<= CW_LD_VEC(0) & CW_LD;
			DATA_LD_VEC	<= DATA_LD_VEC(0) & DATA_LD;
			nstatus_Vec	<= nstatus_Vec(0) & nstatus;

			-- FPGA Generates nCONFIG based on a command from the PCI Bus ------------
			-- The nCONFIG pulse width is 2us minimum
			-- Falling Edge of nCONFIG to falling edge of NSTATUS is 1 us
			if(( Config_En = '1' ) and ( CW_LD_VEC = "01" ))
				then NCONFIG <= '0';
			elsif(( CONFIG_CNT_TC = '1' ) or ( nstatus_Vec = "10" ))	-- When nStatus goes low
				then NCONFIG <= '1';
			end if;

			if( NCONFIG = '0' )
				then CONFIG_CNT <= CONFIG_CNT + 1;
				else CONFIG_CNT <= 0;
			end if;
			
			-- FPGA Generates nCONFIG based on a command from the PCI Bus ------------

			if(( COnfig_En = '1' ) and ( DATA_LD_VEC = "01" ))
				then iSHIFT_EN <= '1';
			elsif( SHIFT_CNT_TC = '1' )
				then iSHIFT_EN <= '0';
			end if;

			if( iSHIFT_EN = '0' )
				then Shift_Cnt <= 0;
				else Shift_Cnt <= Shift_Cnt + 1;
			end if;
			
--			elsif(( Config_En = '1' ) and ( iShift_En = '0' ) and ( DATA_LD_VEC = "01" ))
--				then Shift_Cnt <= 0;
--			elsif( iSHIFT_EN = '1' )
--				then shift_cnt <= shift_cnt + 1;
--			end if;

			if( iSHIFT_EN = '0' )
				then iDCLK <= '0';
				else iDCLK <= not( iDCLK );
			end if;
			
			if(( Config_En = '1' ) and ( iSHIFT_EN = '0' ) and ( DATA_LD_VEC = "01" ))
--			if(( Config_En = '1' ) and ( iShift_En = '0' )) 
				then FPGA_REG <= PCI_DQ;
			elsif(( iSHIFT_EN = '1' ) and ( iDCLK = '1' ))
				then FPGA_REG <= '0' & FPGA_REG(15 downto 1 );
			end if;

			SHIFT_EN	<= iSHIFT_EN;
			DATA 	<= FPGA_REG(0);
			DCLK		<= iDCLK;
          end if;
     end process;
     ------------------------------------------------------------------------------------
                                        
-----------------------------------------------------------------------------------------
end behavioral;			-- FCONFIG.VHD
-----------------------------------------------------------------------------------------

