-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FCONFIG.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
-- FPGA Configuration
--            		---+          +-------------------------------------------------
-- nCONFIG (PCI)	   |<-8us---->|
--                     +----------+
--                  ----------+          +--------------------------------------------
-- nSTATUS (FPGA)      | 200ns|          |
--                            +----------+
--                  -----+                                                       +--
-- CONFIG_DONE (FPGA)    |                                                       |
--                       +-------------------------------------------------------+
--                                           +-----+     +-----+     +-----+     +--
-- DCLK (PCI)                                |     |     |     |     |     |     |  
--                  -------------------------+     +-----+     +-----+     +-----+  
--                                      +----------+-----------+-----------+----------+
-- DATA (PCI)       --------------------+          |           |           |          |
--                                      +----------+-----=-----+-----------+----------+
--
-- Step to Initialize FPGA
--	1. PCI must assert nCONFIG low. It shall remain low until:
--        a. nSTATUS rises and CONFIG_DONE is low
--   2. PCI must place data on bit, LSB to MSB @ falling edge of clock, and make sure
--	   it is stable at the rising edge of the clock.
--	3. If nstatus falls during this cycle, then an error has occured.
--
--   History:       <date> - <Author>
--		October 30, 2001 - Simulated in design at 40Mhz operation
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk20		: std_logic := '0';
	signal Config_En	: std_logic;
	signal DATA_LD		: std_logic;		-- Load Data from PCI
	signal CW_LD		: std_logic;		-- Load Control Word bit
	signal PCI_DQ		: std_logic_vector( 15 downto 0 );
	signal nStatus		: std_Logic;
	signal SHIFT_EN	: std_logic;		-- Shift Enable
	signal DCLK		: std_logic;		-- DCLK to FPGA
	signal DATA		: std_logic;		-- DATA to FPGA
	signal NCONFIG		: std_logic;		-- NCONFIG to FPGA
	
	-----------------------------------------------------------------------------------------
	component FCONFIG is 
		port( 
			IMR			: in		std_logic;		-- Master Reset
			CLK20		: in		std_logic;		-- Master Clock
			Config_En		: in		std_logic;
			DATA_LD		: in		std_logic;		-- Load Data from PCI
			CW_LD		: in		std_logic;		-- Load Control Word bit
			PCI_DQ		: in		std_logic_vector( 15 downto 0 );
			nStatus		: in		std_Logic;
			SHIFT_EN		: out	std_logic;		-- Shift Enable
			DCLK			: out	std_logic;		-- DCLK to FPGA
			DATA			: out	std_logic;		-- DATA to FPGA
			NCONFIG		: out	std_logic );		-- NCONFIG to FPGA
	end component FCONFIG;
	-----------------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk20	<= not( clk20 ) after 25 ns;
	
	U : fconfig 
		port map(
			imr		=> imr,
			clk20	=> clk20,
			config_en	=> config_en,
			data_ld	=> data_ld,
			cw_ld	=> cw_ld,
			pci_dq	=> pci_dq,
			nstatus	=> nstatus,
			shift_en	=> shift_en,
			DCLK		=> dclk,
			DATA		=> data,
			NCONFIG	=> nconfig );

	nstatus_proc : process
	begin
		nstatus	<= '1';
		wait until (( nconfig'event ) and ( nconfig = '0' ));
		wait for 200 ns;
		nstatus <= '0';
		wait for 500 ns;
	end process;	
	

	tb_proc : process
	begin
		Config_En		<= '0';
		CW_LD		<= '0';

		data_ld		<= '0';
		PCI_DQ		<= x"0000";

		wait for 2 us;

		----------------------------------------------------------
		assert( false )
			report "Assert nConfig"
			severity note;
			
		Config_En	<= '1';
		wait for 2 us;
		wait until (( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;		
		CW_LD <= '1';

		wait until (( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;		
		CW_LD <= '0';
		----------------------------------------------------------
		----------------------------------------------------------
		assert( false )
			report "Now Write Data"
			severity note;
		PCI_DQ <= x"0000";
		for i in 0 to 15 loop
			wait until (( clk20'event ) and ( clk20 = '1' ));
			wait for 7 ns;		
			PCI_DQ(i) <= '1';
			Data_LD <= '1';
			wait until (( clk20'event ) and ( clk20 = '1' ));
			wait for 7 ns;		
			DATA_LD <= '0';
			wait until (( Shift_En'Event ) and ( SHift_en = '0' ));
			PCI_DQ(i) <= '0';
			wait for 200 ns;
		end loop;
		
		
		wait for 10 us;
		Config_En <= '0';
		assert( false )
			report "End of Simulations"
			severity failure;
		wait;
		
	end process;
-----------------------------------------------------------------------------------------
end test_bench;			-- FCONFIG.VHD
-----------------------------------------------------------------------------------------

