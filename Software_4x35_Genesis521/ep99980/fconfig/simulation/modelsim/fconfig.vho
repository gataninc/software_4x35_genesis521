-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 11:10:32"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	FCONFIG IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	Config_En : IN std_logic;
	DATA_LD : IN std_logic;
	CW_LD : IN std_logic;
	PCI_DQ : IN std_logic_vector(15 DOWNTO 0);
	nStatus : IN std_logic;
	SHIFT_EN : OUT std_logic;
	DCLK : OUT std_logic;
	DATA : OUT std_logic;
	NCONFIG : OUT std_logic
	);
END FCONFIG;

ARCHITECTURE structure OF FCONFIG IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_Config_En : std_logic;
SIGNAL ww_DATA_LD : std_logic;
SIGNAL ww_CW_LD : std_logic;
SIGNAL ww_PCI_DQ : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_nStatus : std_logic;
SIGNAL ww_SHIFT_EN : std_logic;
SIGNAL ww_DCLK : std_logic;
SIGNAL ww_DATA : std_logic;
SIGNAL ww_NCONFIG : std_logic;
SIGNAL CW_LD_VEC_a1_a : std_logic;
SIGNAL DATA_LD_VEC_a1_a : std_logic;
SIGNAL SHIFT_CNT_a3_a_a49 : std_logic;
SIGNAL SHIFT_CNT_a4_a : std_logic;
SIGNAL FPGA_REG_a1_a_a3612 : std_logic;
SIGNAL FPGA_REG_a3777 : std_logic;
SIGNAL FPGA_REG_a2_a_a3614 : std_logic;
SIGNAL FPGA_REG_a3781 : std_logic;
SIGNAL NCONFIG_a88 : std_logic;
SIGNAL FPGA_REG_a3_a_a3616 : std_logic;
SIGNAL FPGA_REG_a3785 : std_logic;
SIGNAL FPGA_REG_a3789 : std_logic;
SIGNAL FPGA_REG_a5_a_a3621 : std_logic;
SIGNAL FPGA_REG_a5_a_a3620 : std_logic;
SIGNAL FPGA_REG_a3792 : std_logic;
SIGNAL FPGA_REG_a6_a_a3623 : std_logic;
SIGNAL FPGA_REG_a6_a_a3622 : std_logic;
SIGNAL FPGA_REG_a3796 : std_logic;
SIGNAL FPGA_REG_a3797 : std_logic;
SIGNAL FPGA_REG_a7_a_a3625 : std_logic;
SIGNAL FPGA_REG_a7_a_a3624 : std_logic;
SIGNAL FPGA_REG_a3800 : std_logic;
SIGNAL FPGA_REG_a3801 : std_logic;
SIGNAL FPGA_REG_a8_a_a3627 : std_logic;
SIGNAL FPGA_REG_a8_a_a3626 : std_logic;
SIGNAL FPGA_REG_a3804 : std_logic;
SIGNAL FPGA_REG_a3805 : std_logic;
SIGNAL FPGA_REG_a9_a_a3629 : std_logic;
SIGNAL FPGA_REG_a9_a_a3628 : std_logic;
SIGNAL FPGA_REG_a3808 : std_logic;
SIGNAL FPGA_REG_a3809 : std_logic;
SIGNAL FPGA_REG_a10_a_a3631 : std_logic;
SIGNAL FPGA_REG_a10_a_a3630 : std_logic;
SIGNAL FPGA_REG_a3812 : std_logic;
SIGNAL FPGA_REG_a3813 : std_logic;
SIGNAL FPGA_REG_a11_a_a3633 : std_logic;
SIGNAL FPGA_REG_a11_a_a3632 : std_logic;
SIGNAL FPGA_REG_a3816 : std_logic;
SIGNAL FPGA_REG_a3817 : std_logic;
SIGNAL FPGA_REG_a12_a_a3635 : std_logic;
SIGNAL FPGA_REG_a12_a_a3634 : std_logic;
SIGNAL FPGA_REG_a3820 : std_logic;
SIGNAL FPGA_REG_a3821 : std_logic;
SIGNAL FPGA_REG_a13_a_a3637 : std_logic;
SIGNAL FPGA_REG_a13_a_a3636 : std_logic;
SIGNAL FPGA_REG_a3824 : std_logic;
SIGNAL FPGA_REG_a3825 : std_logic;
SIGNAL FPGA_REG_a14_a_a3639 : std_logic;
SIGNAL FPGA_REG_a14_a_a3638 : std_logic;
SIGNAL FPGA_REG_a3828 : std_logic;
SIGNAL FPGA_REG_a3829 : std_logic;
SIGNAL FPGA_REG_a15_a_a3641 : std_logic;
SIGNAL FPGA_REG_a15_a_a3640 : std_logic;
SIGNAL FPGA_REG_a3832 : std_logic;
SIGNAL FPGA_REG_a3833 : std_logic;
SIGNAL FPGA_REG_a15_a_a3836 : std_logic;
SIGNAL PCI_DQ_a1_a_acombout : std_logic;
SIGNAL PCI_DQ_a2_a_acombout : std_logic;
SIGNAL PCI_DQ_a3_a_acombout : std_logic;
SIGNAL PCI_DQ_a5_a_acombout : std_logic;
SIGNAL PCI_DQ_a6_a_acombout : std_logic;
SIGNAL PCI_DQ_a7_a_acombout : std_logic;
SIGNAL PCI_DQ_a8_a_acombout : std_logic;
SIGNAL PCI_DQ_a9_a_acombout : std_logic;
SIGNAL PCI_DQ_a10_a_acombout : std_logic;
SIGNAL PCI_DQ_a11_a_acombout : std_logic;
SIGNAL PCI_DQ_a12_a_acombout : std_logic;
SIGNAL PCI_DQ_a13_a_acombout : std_logic;
SIGNAL PCI_DQ_a14_a_acombout : std_logic;
SIGNAL PCI_DQ_a15_a_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL SHIFT_CNT_a0_a_a46 : std_logic;
SIGNAL SHIFT_CNT_a1_a : std_logic;
SIGNAL SHIFT_CNT_a1_a_a55 : std_logic;
SIGNAL SHIFT_CNT_a2_a : std_logic;
SIGNAL SHIFT_CNT_a0_a : std_logic;
SIGNAL SHIFT_CNT_a2_a_a52 : std_logic;
SIGNAL SHIFT_CNT_a3_a : std_logic;
SIGNAL iSHIFT_EN_a46 : std_logic;
SIGNAL DATA_LD_acombout : std_logic;
SIGNAL DATA_LD_VEC_a0_a : std_logic;
SIGNAL Config_En_acombout : std_logic;
SIGNAL clock_proc_a79 : std_logic;
SIGNAL iSHIFT_EN : std_logic;
SIGNAL SHIFT_EN_areg0 : std_logic;
SIGNAL DCLK_areg0 : std_logic;
SIGNAL PCI_DQ_a0_a_acombout : std_logic;
SIGNAL FPGA_REG_a0_a_a3610 : std_logic;
SIGNAL PCI_DQ_a4_a_acombout : std_logic;
SIGNAL FPGA_REG_a4_a_a3618 : std_logic;
SIGNAL FPGA_REG_a3793 : std_logic;
SIGNAL FPGA_REG_a4_a_a3619 : std_logic;
SIGNAL FPGA_REG_a3788 : std_logic;
SIGNAL FPGA_REG_a3_a_a3617 : std_logic;
SIGNAL FPGA_REG_a3784 : std_logic;
SIGNAL FPGA_REG_a2_a_a3615 : std_logic;
SIGNAL FPGA_REG_a3780 : std_logic;
SIGNAL FPGA_REG_a1_a_a3613 : std_logic;
SIGNAL FPGA_REG_a3776 : std_logic;
SIGNAL FPGA_REG_a0_a_a3611 : std_logic;
SIGNAL DATA_areg0 : std_logic;
SIGNAL CONFIG_CNT_a0_a : std_logic;
SIGNAL CONFIG_CNT_a0_a_a68 : std_logic;
SIGNAL CONFIG_CNT_a1_a_a65 : std_logic;
SIGNAL CONFIG_CNT_a2_a : std_logic;
SIGNAL CONFIG_CNT_a2_a_a74 : std_logic;
SIGNAL CONFIG_CNT_a3_a : std_logic;
SIGNAL CONFIG_CNT_a3_a_a71 : std_logic;
SIGNAL CONFIG_CNT_a4_a : std_logic;
SIGNAL CONFIG_CNT_a4_a_a83 : std_logic;
SIGNAL CONFIG_CNT_a5_a_a80 : std_logic;
SIGNAL CONFIG_CNT_a6_a : std_logic;
SIGNAL CONFIG_CNT_a6_a_a86 : std_logic;
SIGNAL CONFIG_CNT_a7_a : std_logic;
SIGNAL CONFIG_CNT_a5_a : std_logic;
SIGNAL CONFIG_CNT_a1_a : std_logic;
SIGNAL NCONFIG_a92 : std_logic;
SIGNAL NCONFIG_a90 : std_logic;
SIGNAL nStatus_acombout : std_logic;
SIGNAL nstatus_Vec_a0_a : std_logic;
SIGNAL nstatus_Vec_a1_a : std_logic;
SIGNAL NCONFIG_a83 : std_logic;
SIGNAL CW_LD_acombout : std_logic;
SIGNAL CW_LD_VEC_a0_a : std_logic;
SIGNAL clock_proc_a78 : std_logic;
SIGNAL NCONFIG_areg0 : std_logic;
SIGNAL ALT_INV_NCONFIG_areg0 : std_logic;
SIGNAL ALT_INV_iSHIFT_EN : std_logic;
SIGNAL ALT_INV_IMR_acombout : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_Config_En <= Config_En;
ww_DATA_LD <= DATA_LD;
ww_CW_LD <= CW_LD;
ww_PCI_DQ <= PCI_DQ;
ww_nStatus <= nStatus;
SHIFT_EN <= ww_SHIFT_EN;
DCLK <= ww_DCLK;
DATA <= ww_DATA;
NCONFIG <= ww_NCONFIG;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_NCONFIG_areg0 <= NOT NCONFIG_areg0;
ALT_INV_iSHIFT_EN <= NOT iSHIFT_EN;
ALT_INV_IMR_acombout <= NOT IMR_acombout;

CW_LD_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- CW_LD_VEC_a1_a = DFFE(CW_LD_VEC_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CW_LD_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CW_LD_VEC_a1_a);

DATA_LD_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DATA_LD_VEC_a1_a = DFFE(DATA_LD_VEC_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DATA_LD_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DATA_LD_VEC_a1_a);

SHIFT_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a3_a = DFFE(GLOBAL(iSHIFT_EN) & SHIFT_CNT_a3_a $ (SHIFT_CNT_a2_a_a52), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- SHIFT_CNT_a3_a_a49 = CARRY(!SHIFT_CNT_a2_a_a52 # !SHIFT_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a3_a,
	cin => SHIFT_CNT_a2_a_a52,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a3_a,
	cout => SHIFT_CNT_a3_a_a49);

SHIFT_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a4_a = DFFE(GLOBAL(iSHIFT_EN) & SHIFT_CNT_a3_a_a49 $ !SHIFT_CNT_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => SHIFT_CNT_a4_a,
	cin => SHIFT_CNT_a3_a_a49,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a4_a);

FPGA_REG_a1_a_a3612_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a1_a_a3612 = DFFE(PCI_DQ_a1_a_acombout & !iSHIFT_EN & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_DQ_a1_a_acombout,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a1_a_a3612);

FPGA_REG_a3777_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3777 = SHIFT_CNT_a0_a & !iSHIFT_EN & (FPGA_REG_a0_a_a3610 # FPGA_REG_a0_a_a3611) # !SHIFT_CNT_a0_a & (FPGA_REG_a0_a_a3610 # FPGA_REG_a0_a_a3611)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a0_a_a3610,
	datad => FPGA_REG_a0_a_a3611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3777);

FPGA_REG_a2_a_a3614_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a2_a_a3614 = DFFE(PCI_DQ_a2_a_acombout & !iSHIFT_EN & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_DQ_a2_a_acombout,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a2_a_a3614);

FPGA_REG_a3781_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3781 = FPGA_REG_a1_a_a3612 & (!SHIFT_CNT_a0_a # !iSHIFT_EN) # !FPGA_REG_a1_a_a3612 & FPGA_REG_a1_a_a3613 & (!SHIFT_CNT_a0_a # !iSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a1_a_a3612,
	datab => iSHIFT_EN,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a1_a_a3613,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3781);

PCI_DQ_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(1),
	combout => PCI_DQ_a1_a_acombout);

FPGA_REG_a3_a_a3616_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3_a_a3616 = DFFE(!iSHIFT_EN & (clock_proc_a79 & PCI_DQ_a3_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => clock_proc_a79,
	datad => PCI_DQ_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a3_a_a3616);

FPGA_REG_a3785_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3785 = FPGA_REG_a2_a_a3614 & (!SHIFT_CNT_a0_a # !iSHIFT_EN) # !FPGA_REG_a2_a_a3614 & FPGA_REG_a2_a_a3615 & (!SHIFT_CNT_a0_a # !iSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a2_a_a3614,
	datab => iSHIFT_EN,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a2_a_a3615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3785);

PCI_DQ_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(2),
	combout => PCI_DQ_a2_a_acombout);

FPGA_REG_a3789_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3789 = SHIFT_CNT_a0_a & !iSHIFT_EN & (FPGA_REG_a3_a_a3617 # FPGA_REG_a3_a_a3616) # !SHIFT_CNT_a0_a & (FPGA_REG_a3_a_a3617 # FPGA_REG_a3_a_a3616)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	datab => FPGA_REG_a3_a_a3617,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a3_a_a3616,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3789);

PCI_DQ_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(3),
	combout => PCI_DQ_a3_a_acombout);

FPGA_REG_a5_a_a3621_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a5_a_a3621 = DFFE(FPGA_REG_a3797 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3797 & FPGA_REG_a3796 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E0EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3797,
	datab => FPGA_REG_a3796,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a5_a_a3621);

FPGA_REG_a5_a_a3620_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a5_a_a3620 = DFFE(PCI_DQ_a5_a_acombout & !iSHIFT_EN & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_DQ_a5_a_acombout,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a5_a_a3620);

FPGA_REG_a3792_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3792 = SHIFT_CNT_a0_a & iSHIFT_EN & (FPGA_REG_a5_a_a3620 # FPGA_REG_a5_a_a3621)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a5_a_a3620,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a5_a_a3621,
	datad => iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3792);

FPGA_REG_a6_a_a3623_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a6_a_a3623 = DFFE(iSHIFT_EN & (FPGA_REG_a3801 # FPGA_REG_a3800) # !iSHIFT_EN & !clock_proc_a79 & (FPGA_REG_a3801 # FPGA_REG_a3800), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF8C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a3801,
	datac => clock_proc_a79,
	datad => FPGA_REG_a3800,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a6_a_a3623);

FPGA_REG_a6_a_a3622_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a6_a_a3622 = DFFE(!iSHIFT_EN & (clock_proc_a79 & PCI_DQ_a6_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => clock_proc_a79,
	datad => PCI_DQ_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a6_a_a3622);

FPGA_REG_a3796_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3796 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a6_a_a3623 # FPGA_REG_a6_a_a3622)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a6_a_a3623,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a6_a_a3622,
	datad => SHIFT_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3796);

FPGA_REG_a3797_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3797 = FPGA_REG_a5_a_a3620 & (!iSHIFT_EN # !SHIFT_CNT_a0_a) # !FPGA_REG_a5_a_a3620 & FPGA_REG_a5_a_a3621 & (!iSHIFT_EN # !SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a5_a_a3620,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a5_a_a3621,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3797);

PCI_DQ_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(5),
	combout => PCI_DQ_a5_a_acombout);

FPGA_REG_a7_a_a3625_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a7_a_a3625 = DFFE(FPGA_REG_a3805 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3805 & FPGA_REG_a3804 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3805,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3804,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a7_a_a3625);

FPGA_REG_a7_a_a3624_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a7_a_a3624 = DFFE(!iSHIFT_EN & (PCI_DQ_a7_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a7_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a7_a_a3624);

FPGA_REG_a3800_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3800 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a7_a_a3624 # FPGA_REG_a7_a_a3625)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a7_a_a3624,
	datad => FPGA_REG_a7_a_a3625,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3800);

FPGA_REG_a3801_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3801 = FPGA_REG_a6_a_a3622 & (!iSHIFT_EN # !SHIFT_CNT_a0_a) # !FPGA_REG_a6_a_a3622 & FPGA_REG_a6_a_a3623 & (!iSHIFT_EN # !SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a6_a_a3622,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a6_a_a3623,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3801);

PCI_DQ_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(6),
	combout => PCI_DQ_a6_a_acombout);

FPGA_REG_a8_a_a3627_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a8_a_a3627 = DFFE(FPGA_REG_a3809 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3809 & FPGA_REG_a3808 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3809,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3808,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a8_a_a3627);

FPGA_REG_a8_a_a3626_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a8_a_a3626 = DFFE(!iSHIFT_EN & PCI_DQ_a8_a_acombout & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iSHIFT_EN,
	datac => PCI_DQ_a8_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a8_a_a3626);

FPGA_REG_a3804_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3804 = SHIFT_CNT_a0_a & iSHIFT_EN & (FPGA_REG_a8_a_a3626 # FPGA_REG_a8_a_a3627)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a8_a_a3626,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a8_a_a3627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3804);

FPGA_REG_a3805_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3805 = iSHIFT_EN & !SHIFT_CNT_a0_a & (FPGA_REG_a7_a_a3624 # FPGA_REG_a7_a_a3625) # !iSHIFT_EN & (FPGA_REG_a7_a_a3624 # FPGA_REG_a7_a_a3625)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a7_a_a3624,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a7_a_a3625,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3805);

PCI_DQ_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(7),
	combout => PCI_DQ_a7_a_acombout);

FPGA_REG_a9_a_a3629_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a9_a_a3629 = DFFE(iSHIFT_EN & (FPGA_REG_a3812 # FPGA_REG_a3813) # !iSHIFT_EN & !clock_proc_a79 & (FPGA_REG_a3812 # FPGA_REG_a3813), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A8FC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a3812,
	datac => FPGA_REG_a3813,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a9_a_a3629);

FPGA_REG_a9_a_a3628_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a9_a_a3628 = DFFE(!iSHIFT_EN & (PCI_DQ_a9_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a9_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a9_a_a3628);

FPGA_REG_a3808_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3808 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a9_a_a3629 # FPGA_REG_a9_a_a3628)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a9_a_a3629,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a9_a_a3628,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3808);

FPGA_REG_a3809_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3809 = FPGA_REG_a8_a_a3626 & (!iSHIFT_EN # !SHIFT_CNT_a0_a) # !FPGA_REG_a8_a_a3626 & FPGA_REG_a8_a_a3627 & (!iSHIFT_EN # !SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a8_a_a3626,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a8_a_a3627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3809);

PCI_DQ_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(8),
	combout => PCI_DQ_a8_a_acombout);

FPGA_REG_a10_a_a3631_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a10_a_a3631 = DFFE(FPGA_REG_a3817 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3817 & FPGA_REG_a3816 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3817,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3816,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a10_a_a3631);

FPGA_REG_a10_a_a3630_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a10_a_a3630 = DFFE(!iSHIFT_EN & (PCI_DQ_a10_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a10_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a10_a_a3630);

FPGA_REG_a3812_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3812 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a10_a_a3631 # FPGA_REG_a10_a_a3630)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a10_a_a3631,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a10_a_a3630,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3812);

FPGA_REG_a3813_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3813 = iSHIFT_EN & !SHIFT_CNT_a0_a & (FPGA_REG_a9_a_a3629 # FPGA_REG_a9_a_a3628) # !iSHIFT_EN & (FPGA_REG_a9_a_a3629 # FPGA_REG_a9_a_a3628)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a9_a_a3629,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a9_a_a3628,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3813);

PCI_DQ_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(9),
	combout => PCI_DQ_a9_a_acombout);

FPGA_REG_a11_a_a3633_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a11_a_a3633 = DFFE(FPGA_REG_a3820 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3820 & FPGA_REG_a3821 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3820,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3821,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a11_a_a3633);

FPGA_REG_a11_a_a3632_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a11_a_a3632 = DFFE(!iSHIFT_EN & (PCI_DQ_a11_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a11_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a11_a_a3632);

FPGA_REG_a3816_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3816 = SHIFT_CNT_a0_a & iSHIFT_EN & (FPGA_REG_a11_a_a3633 # FPGA_REG_a11_a_a3632)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a11_a_a3633,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a11_a_a3632,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3816);

FPGA_REG_a3817_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3817 = iSHIFT_EN & !SHIFT_CNT_a0_a & (FPGA_REG_a10_a_a3631 # FPGA_REG_a10_a_a3630) # !iSHIFT_EN & (FPGA_REG_a10_a_a3631 # FPGA_REG_a10_a_a3630)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a10_a_a3631,
	datad => FPGA_REG_a10_a_a3630,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3817);

PCI_DQ_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(10),
	combout => PCI_DQ_a10_a_acombout);

FPGA_REG_a12_a_a3635_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a12_a_a3635 = DFFE(iSHIFT_EN & (FPGA_REG_a3825 # FPGA_REG_a3824) # !iSHIFT_EN & !clock_proc_a79 & (FPGA_REG_a3825 # FPGA_REG_a3824), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF8C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a3825,
	datac => clock_proc_a79,
	datad => FPGA_REG_a3824,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a12_a_a3635);

FPGA_REG_a12_a_a3634_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a12_a_a3634 = DFFE(!iSHIFT_EN & (PCI_DQ_a12_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a12_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a12_a_a3634);

FPGA_REG_a3820_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3820 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a12_a_a3635 # FPGA_REG_a12_a_a3634)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a12_a_a3635,
	datad => FPGA_REG_a12_a_a3634,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3820);

FPGA_REG_a3821_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3821 = FPGA_REG_a11_a_a3633 & (!iSHIFT_EN # !SHIFT_CNT_a0_a) # !FPGA_REG_a11_a_a3633 & FPGA_REG_a11_a_a3632 & (!iSHIFT_EN # !SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a11_a_a3633,
	datab => SHIFT_CNT_a0_a,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a11_a_a3632,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3821);

PCI_DQ_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(11),
	combout => PCI_DQ_a11_a_acombout);

FPGA_REG_a13_a_a3637_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a13_a_a3637 = DFFE(iSHIFT_EN & (FPGA_REG_a3828 # FPGA_REG_a3829) # !iSHIFT_EN & !clock_proc_a79 & (FPGA_REG_a3828 # FPGA_REG_a3829), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF8C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => FPGA_REG_a3828,
	datac => clock_proc_a79,
	datad => FPGA_REG_a3829,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a13_a_a3637);

FPGA_REG_a13_a_a3636_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a13_a_a3636 = DFFE(!iSHIFT_EN & PCI_DQ_a13_a_acombout & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iSHIFT_EN,
	datac => PCI_DQ_a13_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a13_a_a3636);

FPGA_REG_a3824_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3824 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a13_a_a3637 # FPGA_REG_a13_a_a3636)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a13_a_a3637,
	datad => FPGA_REG_a13_a_a3636,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3824);

FPGA_REG_a3825_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3825 = iSHIFT_EN & !SHIFT_CNT_a0_a & (FPGA_REG_a12_a_a3635 # FPGA_REG_a12_a_a3634) # !iSHIFT_EN & (FPGA_REG_a12_a_a3635 # FPGA_REG_a12_a_a3634)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a12_a_a3635,
	datad => FPGA_REG_a12_a_a3634,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3825);

PCI_DQ_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(12),
	combout => PCI_DQ_a12_a_acombout);

FPGA_REG_a14_a_a3639_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a14_a_a3639 = DFFE(FPGA_REG_a3833 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3833 & FPGA_REG_a3832 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E0EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3833,
	datab => FPGA_REG_a3832,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a14_a_a3639);

FPGA_REG_a14_a_a3638_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a14_a_a3638 = DFFE(PCI_DQ_a14_a_acombout & !iSHIFT_EN & clock_proc_a79, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_DQ_a14_a_acombout,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a14_a_a3638);

FPGA_REG_a3828_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3828 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a14_a_a3638 # FPGA_REG_a14_a_a3639)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a14_a_a3638,
	datab => FPGA_REG_a14_a_a3639,
	datac => iSHIFT_EN,
	datad => SHIFT_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3828);

FPGA_REG_a3829_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3829 = iSHIFT_EN & !SHIFT_CNT_a0_a & (FPGA_REG_a13_a_a3637 # FPGA_REG_a13_a_a3636) # !iSHIFT_EN & (FPGA_REG_a13_a_a3637 # FPGA_REG_a13_a_a3636)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a13_a_a3637,
	datad => FPGA_REG_a13_a_a3636,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3829);

PCI_DQ_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(13),
	combout => PCI_DQ_a13_a_acombout);

FPGA_REG_a15_a_a3641_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a15_a_a3641 = DFFE(FPGA_REG_a15_a_a3836 & (iSHIFT_EN & !SHIFT_CNT_a0_a # !iSHIFT_EN & (!clock_proc_a79)), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4070",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a15_a_a3836,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a15_a_a3641);

FPGA_REG_a15_a_a3640_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a15_a_a3640 = DFFE(!iSHIFT_EN & (PCI_DQ_a15_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a15_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a15_a_a3640);

FPGA_REG_a3832_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3832 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a15_a_a3640 # FPGA_REG_a15_a_a3641)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a15_a_a3640,
	datab => iSHIFT_EN,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a15_a_a3641,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3832);

FPGA_REG_a3833_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3833 = SHIFT_CNT_a0_a & !iSHIFT_EN & (FPGA_REG_a14_a_a3639 # FPGA_REG_a14_a_a3638) # !SHIFT_CNT_a0_a & (FPGA_REG_a14_a_a3639 # FPGA_REG_a14_a_a3638)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a14_a_a3639,
	datad => FPGA_REG_a14_a_a3638,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3833);

PCI_DQ_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(14),
	combout => PCI_DQ_a14_a_acombout);

FPGA_REG_a15_a_a3836_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a15_a_a3836 = FPGA_REG_a15_a_a3640 # FPGA_REG_a15_a_a3641

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FPGA_REG_a15_a_a3640,
	datad => FPGA_REG_a15_a_a3641,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a15_a_a3836);

PCI_DQ_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(15),
	combout => PCI_DQ_a15_a_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

SHIFT_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a0_a = DFFE(GLOBAL(iSHIFT_EN) & !SHIFT_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- SHIFT_CNT_a0_a_a46 = CARRY(SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "55AA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a0_a,
	cout => SHIFT_CNT_a0_a_a46);

SHIFT_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a1_a = DFFE(GLOBAL(iSHIFT_EN) & SHIFT_CNT_a1_a $ SHIFT_CNT_a0_a_a46, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- SHIFT_CNT_a1_a_a55 = CARRY(!SHIFT_CNT_a0_a_a46 # !SHIFT_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => SHIFT_CNT_a1_a,
	cin => SHIFT_CNT_a0_a_a46,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a1_a,
	cout => SHIFT_CNT_a1_a_a55);

SHIFT_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a2_a = DFFE(GLOBAL(iSHIFT_EN) & SHIFT_CNT_a2_a $ (!SHIFT_CNT_a1_a_a55), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- SHIFT_CNT_a2_a_a52 = CARRY(SHIFT_CNT_a2_a & (!SHIFT_CNT_a1_a_a55))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a2_a,
	cin => SHIFT_CNT_a1_a_a55,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a2_a,
	cout => SHIFT_CNT_a2_a_a52);

iSHIFT_EN_a46_I : apex20ke_lcell
-- Equation(s):
-- iSHIFT_EN_a46 = !SHIFT_CNT_a3_a # !SHIFT_CNT_a0_a # !SHIFT_CNT_a2_a # !SHIFT_CNT_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a1_a,
	datab => SHIFT_CNT_a2_a,
	datac => SHIFT_CNT_a0_a,
	datad => SHIFT_CNT_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => iSHIFT_EN_a46);

DATA_LD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DATA_LD,
	combout => DATA_LD_acombout);

DATA_LD_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DATA_LD_VEC_a0_a = DFFE(DATA_LD_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DATA_LD_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DATA_LD_VEC_a0_a);

Config_En_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Config_En,
	combout => Config_En_acombout);

clock_proc_a79_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a79 = !DATA_LD_VEC_a1_a & (DATA_LD_VEC_a0_a & Config_En_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DATA_LD_VEC_a1_a,
	datac => DATA_LD_VEC_a0_a,
	datad => Config_En_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a79);

iSHIFT_EN_aI : apex20ke_lcell
-- Equation(s):
-- iSHIFT_EN = DFFE(clock_proc_a79 # iSHIFT_EN & (iSHIFT_EN_a46 # !SHIFT_CNT_a4_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFD0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a4_a,
	datab => iSHIFT_EN_a46,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iSHIFT_EN);

SHIFT_EN_areg0_I : apex20ke_lcell
-- Equation(s):
-- SHIFT_EN_areg0 = DFFE(iSHIFT_EN, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => iSHIFT_EN,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_EN_areg0);

DCLK_areg0_I : apex20ke_lcell
-- Equation(s):
-- DCLK_areg0 = DFFE(SHIFT_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => SHIFT_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DCLK_areg0);

PCI_DQ_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(0),
	combout => PCI_DQ_a0_a_acombout);

FPGA_REG_a0_a_a3610_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a0_a_a3610 = DFFE(!iSHIFT_EN & (PCI_DQ_a0_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a0_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a0_a_a3610);

PCI_DQ_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DQ(4),
	combout => PCI_DQ_a4_a_acombout);

FPGA_REG_a4_a_a3618_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a4_a_a3618 = DFFE(!iSHIFT_EN & (PCI_DQ_a4_a_acombout & clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datac => PCI_DQ_a4_a_acombout,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a4_a_a3618);

FPGA_REG_a3793_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3793 = SHIFT_CNT_a0_a & !iSHIFT_EN & (FPGA_REG_a4_a_a3619 # FPGA_REG_a4_a_a3618) # !SHIFT_CNT_a0_a & (FPGA_REG_a4_a_a3619 # FPGA_REG_a4_a_a3618)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a0_a,
	datab => FPGA_REG_a4_a_a3619,
	datac => iSHIFT_EN,
	datad => FPGA_REG_a4_a_a3618,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3793);

FPGA_REG_a4_a_a3619_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a4_a_a3619 = DFFE(FPGA_REG_a3792 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3792 & FPGA_REG_a3793 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3792,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3793,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a4_a_a3619);

FPGA_REG_a3788_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3788 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a4_a_a3619 # FPGA_REG_a4_a_a3618)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSHIFT_EN,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a4_a_a3619,
	datad => FPGA_REG_a4_a_a3618,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3788);

FPGA_REG_a3_a_a3617_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3_a_a3617 = DFFE(FPGA_REG_a3789 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3789 & FPGA_REG_a3788 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CF8A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3789,
	datab => iSHIFT_EN,
	datac => clock_proc_a79,
	datad => FPGA_REG_a3788,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a3_a_a3617);

FPGA_REG_a3784_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3784 = SHIFT_CNT_a0_a & iSHIFT_EN & (FPGA_REG_a3_a_a3616 # FPGA_REG_a3_a_a3617)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3_a_a3616,
	datab => SHIFT_CNT_a0_a,
	datac => FPGA_REG_a3_a_a3617,
	datad => iSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3784);

FPGA_REG_a2_a_a3615_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a2_a_a3615 = DFFE(FPGA_REG_a3785 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3785 & FPGA_REG_a3784 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E0EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3785,
	datab => FPGA_REG_a3784,
	datac => iSHIFT_EN,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a2_a_a3615);

FPGA_REG_a3780_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3780 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a2_a_a3614 # FPGA_REG_a2_a_a3615)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a2_a_a3614,
	datab => iSHIFT_EN,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a2_a_a3615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3780);

FPGA_REG_a1_a_a3613_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a1_a_a3613 = DFFE(FPGA_REG_a3781 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3781 & FPGA_REG_a3780 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3781,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3780,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a1_a_a3613);

FPGA_REG_a3776_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a3776 = iSHIFT_EN & SHIFT_CNT_a0_a & (FPGA_REG_a1_a_a3612 # FPGA_REG_a1_a_a3613)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a1_a_a3612,
	datab => iSHIFT_EN,
	datac => SHIFT_CNT_a0_a,
	datad => FPGA_REG_a1_a_a3613,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FPGA_REG_a3776);

FPGA_REG_a0_a_a3611_I : apex20ke_lcell
-- Equation(s):
-- FPGA_REG_a0_a_a3611 = DFFE(FPGA_REG_a3777 & (iSHIFT_EN # !clock_proc_a79) # !FPGA_REG_a3777 & FPGA_REG_a3776 & (iSHIFT_EN # !clock_proc_a79), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_REG_a3777,
	datab => iSHIFT_EN,
	datac => FPGA_REG_a3776,
	datad => clock_proc_a79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_REG_a0_a_a3611);

DATA_areg0_I : apex20ke_lcell
-- Equation(s):
-- DATA_areg0 = DFFE(FPGA_REG_a0_a_a3610 # FPGA_REG_a0_a_a3611, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => FPGA_REG_a0_a_a3610,
	datad => FPGA_REG_a0_a_a3611,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DATA_areg0);

CONFIG_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a0_a = DFFE(GLOBAL(NCONFIG_areg0) & !CONFIG_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a0_a_a68 = CARRY(CONFIG_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CONFIG_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a0_a,
	cout => CONFIG_CNT_a0_a_a68);

CONFIG_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a1_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a1_a $ (CONFIG_CNT_a0_a_a68), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a1_a_a65 = CARRY(!CONFIG_CNT_a0_a_a68 # !CONFIG_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CONFIG_CNT_a1_a,
	cin => CONFIG_CNT_a0_a_a68,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a1_a,
	cout => CONFIG_CNT_a1_a_a65);

CONFIG_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a2_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a2_a $ !CONFIG_CNT_a1_a_a65, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a2_a_a74 = CARRY(CONFIG_CNT_a2_a & !CONFIG_CNT_a1_a_a65)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CONFIG_CNT_a2_a,
	cin => CONFIG_CNT_a1_a_a65,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a2_a,
	cout => CONFIG_CNT_a2_a_a74);

CONFIG_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a3_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a3_a $ CONFIG_CNT_a2_a_a74, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a3_a_a71 = CARRY(!CONFIG_CNT_a2_a_a74 # !CONFIG_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CONFIG_CNT_a3_a,
	cin => CONFIG_CNT_a2_a_a74,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a3_a,
	cout => CONFIG_CNT_a3_a_a71);

CONFIG_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a4_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a4_a $ !CONFIG_CNT_a3_a_a71, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a4_a_a83 = CARRY(CONFIG_CNT_a4_a & !CONFIG_CNT_a3_a_a71)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CONFIG_CNT_a4_a,
	cin => CONFIG_CNT_a3_a_a71,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a4_a,
	cout => CONFIG_CNT_a4_a_a83);

CONFIG_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a5_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a5_a $ (CONFIG_CNT_a4_a_a83), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a5_a_a80 = CARRY(!CONFIG_CNT_a4_a_a83 # !CONFIG_CNT_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CONFIG_CNT_a5_a,
	cin => CONFIG_CNT_a4_a_a83,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a5_a,
	cout => CONFIG_CNT_a5_a_a80);

CONFIG_CNT_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a6_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a6_a $ (!CONFIG_CNT_a5_a_a80), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- CONFIG_CNT_a6_a_a86 = CARRY(CONFIG_CNT_a6_a & (!CONFIG_CNT_a5_a_a80))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CONFIG_CNT_a6_a,
	cin => CONFIG_CNT_a5_a_a80,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a6_a,
	cout => CONFIG_CNT_a6_a_a86);

CONFIG_CNT_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- CONFIG_CNT_a7_a = DFFE(GLOBAL(NCONFIG_areg0) & CONFIG_CNT_a6_a_a86 $ CONFIG_CNT_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CONFIG_CNT_a7_a,
	cin => CONFIG_CNT_a6_a_a86,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CONFIG_CNT_a7_a);

NCONFIG_a88_I : apex20ke_lcell
-- Equation(s):
-- NCONFIG_a92 = CONFIG_CNT_a0_a & !CONFIG_CNT_a3_a & !CONFIG_CNT_a2_a & CONFIG_CNT_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CONFIG_CNT_a0_a,
	datab => CONFIG_CNT_a3_a,
	datac => CONFIG_CNT_a2_a,
	datad => CONFIG_CNT_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NCONFIG_a88,
	cascout => NCONFIG_a92);

NCONFIG_a90_I : apex20ke_lcell
-- Equation(s):
-- NCONFIG_a90 = (CONFIG_CNT_a4_a & !CONFIG_CNT_a6_a & CONFIG_CNT_a7_a & CONFIG_CNT_a5_a) & CASCADE(NCONFIG_a92)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CONFIG_CNT_a4_a,
	datab => CONFIG_CNT_a6_a,
	datac => CONFIG_CNT_a7_a,
	datad => CONFIG_CNT_a5_a,
	cascin => NCONFIG_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NCONFIG_a90);

nStatus_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_nStatus,
	combout => nStatus_acombout);

nstatus_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- nstatus_Vec_a0_a = DFFE(nStatus_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => nStatus_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => nstatus_Vec_a0_a);

nstatus_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- nstatus_Vec_a1_a = DFFE(nstatus_Vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => nstatus_Vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => nstatus_Vec_a1_a);

NCONFIG_a83_I : apex20ke_lcell
-- Equation(s):
-- NCONFIG_a83 = !nstatus_Vec_a0_a & nstatus_Vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => nstatus_Vec_a0_a,
	datad => nstatus_Vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NCONFIG_a83);

CW_LD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CW_LD,
	combout => CW_LD_acombout);

CW_LD_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- CW_LD_VEC_a0_a = DFFE(CW_LD_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CW_LD_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CW_LD_VEC_a0_a);

clock_proc_a78_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a78 = !CW_LD_VEC_a1_a & (Config_En_acombout & CW_LD_VEC_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CW_LD_VEC_a1_a,
	datac => Config_En_acombout,
	datad => CW_LD_VEC_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a78);

NCONFIG_areg0_I : apex20ke_lcell
-- Equation(s):
-- NCONFIG_areg0 = DFFE(clock_proc_a78 # !NCONFIG_a90 & NCONFIG_areg0 & !NCONFIG_a83, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF04",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => NCONFIG_a90,
	datab => NCONFIG_areg0,
	datac => NCONFIG_a83,
	datad => clock_proc_a78,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => NCONFIG_areg0);

SHIFT_EN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SHIFT_EN_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHIFT_EN);

DCLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DCLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DCLK);

DATA_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DATA_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DATA);

NCONFIG_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_NCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NCONFIG);
END structure;


