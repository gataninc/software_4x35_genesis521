onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/Config_en
add wave -noupdate -format Logic -radix binary /tb/data_ld
add wave -noupdate -format Logic -radix binary /tb/cw_ld
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_dq
add wave -noupdate -format Logic -radix binary /tb/nstatus
add wave -noupdate -format Logic -radix binary /tb/shift_en
add wave -noupdate -format Logic -radix binary /tb/dclk
add wave -noupdate -format Logic -radix binary /tb/data
add wave -noupdate -format Logic -radix binary /tb/nconfig
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {8044355 ps}
WaveRestoreZoom {0 ps} {105 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
