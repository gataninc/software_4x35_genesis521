------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	EP99980.VHD ( aka GLUEFPGA )
----------------------------------------------------------------------------

----------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:	
--		Glue Logic Interface consisting of:
--			PCI Interface
--			Electronic Serial Number IC (DS2401)
--			Temperature Sensor (TMP04)
--			PCI to Dual Port RAM
--			DSP to Dual Port RAM
--
-- NOTE:
--   Ver 3C31 PCI_RST_IN to PCI_RST_OUT changed from 2x 150ms to 2x 300ms
-- 	in attempt to fix HP X4200 workstation problems
--
--	in DSP_BOOT DON"T FORGET TO CHANGE
--		constant MAX_DSP_MR_CNT 		: integer := 3999999;	-- 200us @20Mhz
--		from the simulation value of 29;
----------------------------------------------------------------------------

--	Dual Port RAM Addresses
--		The Dual Port RAM is an 8K x 16 memory that is mapped to PCI space 
--		in the following mannor:
--	"1" DPRA[12:0] "XX". Since the lowest two address bits are used for byte 
--		addressing, but not	used on this board, and the Upper bit is set to 
--		1, zero is used for address not in the dual port ram. 
----------------------------------------------------------------------------
-- FPGA Configuration
--   The PCI will write to this device, 16 bits at a time, following this, 
--	it shall shift out all 16 bits of data. There shall be a control word to 
--	select which FPGA is to be written as well as the status of the cycle 
--	as well as the status of the shifting operation.
--
-- 		FPGA Control Word
--				Bit 0 : EDS Config Done
--				Bit 1 : EDS nSTATUS
--				Bit 2 : EDS Shift in Progress
--				Bit 3 : EDS nCONFIG Bit ( 1 = Write Low )
--				Bit 4 : SEM Config Done
--				Bit 5 : SEM nSTATUS
--				Bit 6 : SEM Shift in Progress
--				Bit 7 : SEM nCONFIG Bit ( 1 = Write Low )
--		FPGA Control Word Definition:
--				Bit 0 : 
--
--	--	Write to DSP Address of
--		0110		Sets OpCode Acknowledge
--		0111		Generates an Interrupt to the PCI
--
----------------------------------------------------------------------------

----------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

----------------------------------------------------------------------------

----------------------------------------------------------------------------
entity EP99980 is port( 
	S5920MR      	: in      std_logic;			-- Master Reset from S5920
	PCI_RST_IN	: in		std_logic;			-- Input Reset from PCI Bus
	PCI_RST_OUT	: buffer	std_logic;			-- Output Reset to AMCC Chip
     OMR			: buffer	std_logic;			-- Master Reset Output
     CLK20_IN       : in      std_logic;          	-- Master Clock 10 Mhz
	AO_CLK		: buffer	std_logic;			-- Master Clock 40 Mhz ( for PCI Chip )

	-- PCI Related I/Os ------------------------------------------------------
	ODXFER         : in      std_logic;                         -- Active Transfer Complete
     OPTADR         : in      std_logic;                         -- Active Mode Address Valid
	OPTATN         : in      std_logic;                         -- Pass Thru Attention
	PTWR           : in      std_logic;                         -- Pass Thru Write
     PTNUM          : in      std_logic_vector( 1 downto 0 );    -- Pass Thru Number
	OPTBE          : in      std_logic_vector( 3 downto 0 );    -- Pass Thru Byte Enable
	AO_INTR		: buffer	std_logic;					-- Add-On Bus Interrupt
	PCI_DSP_IRQ	: buffer	std_logic;					-- DSP Interrupt
	PCI_DQ		: inout	std_logic_vector( 15 downto 0 );	-- PCI Data Bus
	-- PCI Related I/Os ------------------------------------------------------

	-- DSP Related I/Os ------------------------------------------------------
	DSP_H3		: in		std_logic;
	DSP_WR		: in		std_logic;					-- DSP Write/Read ( 0=Write )
	DSP_STB1		: in		std_logic_vector(  3 downto 0 );	-- DSP Strobe 1
	DSP_A1		: in		std_logic_vector(  7 downto 0 );	-- DSP Address (lower Bits not used )
	DSP_A		: in		std_logic_vector( 15 downto 13 );	-- DSP Address
	DSP_D		: inout	std_logic_Vector( 15 downto 0 );	-- DSP Data Bus
	-- DSP Related I/Os ------------------------------------------------------

	-- DSP Configuration Related I/Os ----------------------------------------
	SB_INTR		: inout	std_logic;					-- aka DSP BOot
	SB_SDIN		: inout 	std_logic;					-- Shift Data
	OFSIN		: inout	std_logic;					-- Frame Sync
	OSCLK		: inout	std_logic;					-- Clock
	-- DSP Configuration Related I/Os ----------------------------------------

	-- EDS FPGA Configuration Interface --------------------------------------
	FPGA_NS		: in		std_logic;					-- EDS FPGA nSTATUS
	FPGA_CD		: in		std_logic;					-- EDS FPGA Config_Done
	FPGA_DCLK		: buffer	std_logic;					-- EDS FPGA DCLK
	FPGA_D0		: buffer	std_logic;					-- EDS FPGA Data
	FPGA_NC		: buffer	std_logic;					-- EDS nCONFIG
	-- EDS FPGA Configuration Interface --------------------------------------

	-- PCI-DSP Fifo Interface ------------------------------------------------
	PFF_EF		: in		std_logic_Vector( 1 downto 0 );	-- PCI Fifo Empty Flag
	PFF_FF		: in		std_logic_vector( 1 downto 0 );	-- PCI Fifo Full Flag
	PFF_WEN		: buffer  std_logic;					-- PCI Fifo Write Enable
	PFF_REN		: buffer	std_logic;					-- PCI Fifo Read Enable
	PFF_OE		: buffer	std_logic;					-- PCI Fifo Output Enable
	PFF_RS		: buffer  std_logic;					-- PCI Fifo Reset
	PFF_WCK		: buffer	std_logic;					-- PCI FIFO Write Clock
	-- PCI-DSP Fifo Interface ------------------------------------------------

	-- Miscellaneous Functions -----------------------------------------------
	TP			: buffer	std_logic_vector( 5 downto 0 ) );

	-- Miscellaneous Functions -----------------------------------------------
end EP99980;
----------------------------------------------------------------------------

----------------------------------------------------------------------------
architecture behavioral of EP99980 is
	-- Constant Declarations ----------------------------------------------
	constant VER			: integer := 16#4003#;			-- LEave at 9
	constant ZEROES		: std_logic_vector( 31 downto 0 ) := x"00000000";
	constant Signal_Tap		: integer := 0;

	-----------------------------------------------------------------------------
	--	Primary PLL for AO_CLK and internal 20Mhz Clock
	-- AO_CLK
	-- clock0_Boost/Clock0_Divide
	-- 2/1 = 40Mhz, NG - PCI FIFO too slow (25ns access time)
	-- 5/4 = 25Mhz
	-- 3/2 = 30Mhz  OK
	-- 5/3 = 33Mhz - NG - PCI FIFO too slow (25ns access time)
	-- 7/4 = 35Mhz - NG - PCI FIFO too slow (25ns access time)
	constant AO_BOOST  : integer := 3;
	constant AO_DIVIDE : integer := 2;

	-- Reset must be at least 10-H1 clock cycles but can take up to 20ms for oscillator to become stable.
	-- Thus a "power-Up reset shoudl generate a low pulse on the reset line for 100-200ms

	-- PCI Address Decodes ---------------------------------------------------
	constant iADR_ID   		: integer := 16#00#;	-- GLUE FPGA ID And Version
	constant iADR_TREG		: integer := 16#01#;	-- Test Register
	constant iADR_GLUE_CR	: integer := 16#02#;	-- Control Register
	constant iADR_FF_CNT   	: integer := 16#03#;	-- FIFO Word Count
	constant iADR_F_CW       : integer := 16#04#;	-- FPGA Download Control Word
	constant iADR_FPGA_B0	: integer := 16#05#;	-- DSP Boot Data low
	constant iADR_FPGA_NC	: integer := 16#06#;
	constant iADR_PFF		: integer := 16#07#;	-- SEE PCICTRL PCI FIFO Read
	constant iADR_PFF_RS	: integer := 16#08#;	-- PCI FIFO Reset ( Write Only )
	constant iADR_MR		: integer := 16#09#;	-- EDI-II Master REset, used to Boot the DSP
	constant iADR_DSP_BOOT	: integer := 16#0A#;	-- Start the DSP Boot Sequence
	constant iADR_DSP_B0	: integer := 16#0B#;	-- DSP Boot Data low
	constant iADR_DSP_B1	: integer := 16#0C#;	-- DSP Boot Data low
	constant iADR_PFF_STAT	: integer := 16#0D#;
	constant NUM_ADR 		: integer := 16#10#;	-- Number of PCI Address Decodes


	-- PCI Address Decodes in the DPRAM -----------------------------------------	
	constant iADR_OPCODE 		: integer := 16#2000#;	-- SEE PCICTRL
	constant iADR_IRQACK 		: integer := 16#2008#;	-- SEE PCICTRL
	
	-- PCI Address Decodes -----------------------------------------------------------------------
	
	-- New DSP Address Decodes
	constant DSP_DPRAM_DSP		: integer := 16#00#;	-- High DSP Address Bits for DPRAM
	constant DSP_DPRAM_FPGA		: integer := 16#01#;	-- High DSP Address Bits for Glue
	constant DSP_Board_ID		: integer := 16#02#;	-- High DSP Address Bits for Glue

	constant iADR_DSP_PCI_FIFO	: integer := 16#00#;	-- Low DSP Address Bits for PCI FIFO
	constant iADR_DSP_INTR_EN	: integer := 16#01#;	-- Enable PCI Interrupt
	constant iADR_DSP_INTR_DIS	: integer := 16#02#;	-- Disable PCI Interrupt
	constant iADR_DSP_ID		: integer := 16#03#;
	constant iADR_DSP_TREG		: integer := 16#04#;	-- DS
	constant iADR_DSP_FIFO_MR	: integer := 16#05#;
	constant iADR_DSP_FF_CNT		: integer := 16#06#;
	-- DSP Address Decodes -----------------------------------------------------------------------

	signal CLK20			: std_logic;
	signal CLK40			: std_logic;
	signal CLK40_D2		: std_logic;
	signal CREG 			: std_Logic_Vector( 7 downto 0 );
	signal TREG			: std_logic_Vector( 15 downto 0 );	

	signal DPR_ACC			: std_logic;		-- Dual Port RAM Access
	signal DSP_BOOT 		: Std_Logic;
	signal DSP_CLK			: std_logic;
	signal DSP_CONFIG		: std_logic;
	signal DSP_D0			: std_logic;
	signal DSP_EN			: std_logic;
	signal DSP_MR			: std_logic;
	signal DSP_Read		: std_logic;
	signal DSP_REG			: std_logic_vector( 31 downto 0 );
	signal DSP_SHIFT 		: std_logic;
	signal DP_OEL 			: std_logic;
	signal DP_WRL			: std_logic;
	signal DP_DSP_D 		: std_logic_Vector( 15 downto 0 );
	signal DP_PCI_D 		: std_logic_Vector( 15 downto 0 );
	signal DSP_DPRA		: std_logic_Vector( 8 downto 0 );
	signal DSP_FF_MR		: std_Logic;

	signal FPGA_CONFIG		: std_logic;

	signal FF_CNT			: std_logic_vector( 15 downto 0 );
	signal FC_SHIFT		: std_logic;				-- 11/12/99
	signal FF_REN			: std_logic;
	signal FF_RS			: std_logic;
	signal FF_OE			: std_logic;
	signal FF_WEN			: std_logic;
	signal FF_HF			: std_logic;

	signal FPGA_BOOT		: std_logic_Vector( 15 downto 0 );
	signal DSP_D_MUX		: std_logic_vector( 15 downto 0 );
	signal DSP_D_Mout		: std_logic_vector( 15 downto 0 );
	signal DP_OER			: std_Logic;
	signal DP_WRR			: std_logic;

	signal IMR			: std_logic;
	signal INTR_EN			: std_logic;
	signal PCI_DQ_MUX		: std_logic_vector( 15 downto 0 );
	signal PCI_DQ_MOUT		: std_logic_vector( 15 downto 0 );
	signal PFF_RS_VEC 		: std_logic_Vector(  1 downto 0 );

	signal PCI_RD			: std_logic;
	signal PCI_ADR			: std_logic_Vector(  8 downto 0 );
	signal PCI_DPRA		: std_logic_vector(  8 downto 0 );
     signal WE_DCD			: std_logic_Vector( 15 downto 0 );
	
	signal TP_SEL			: std_logic_VEctor( 3 downto 0 );

	signal WE_DSP_TREG		: std_logic;
	signal DSP_TREG		: std_logic_Vector( 15 downto 0 );
	signal ao_CLK_D2		: STD_lOGIC;
	signal dpram_sstr		: std_logic_vector( 3 downto 0 );
	signal PCI_WD			: std_logic_vector( 15 downto 0 );
	------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component DPRAM
		port(
			imr			: in		std_logic;
			clk_a		: in		std_logic;
			clk_b		: in		std_logic;
			wren_a		: in		std_logic;
			wren_b		: in 	std_logic;
			addr_a		: in		std_logic_Vector(   8 downto 0 );
			addr_b		: in		std_Logic_Vector(   8 downto 0 );
			data_a		: IN 	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
			data_b		: IN 	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
			q_a 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
			q_b 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
			state_str		: out	std_logic_Vector(  3 downto 0 );
			OpCodeIsr		: out 	std_Logic );
	END component;			-- DPRAM;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component DSPBOOT  
		port( 
	     	IMR       	: in      std_logic;                         -- Local Master Reset
		     CLK20		: in      std_logic;                         -- Local System Clock
			DSP_CONFIG	: in		std_Logic;
			DSP_BYTE_LO	: in 	std_logic;					-- Load Low Byte to DSP COnfig Reg
			DSP_MR_ED 	: in		std_logic;					-- DSP Master Reset Edge Detect
			DSP_BOOT_IN	: in		std_Logic;
			DSP_REG		: in		std_logic_Vector( 31 downto 0 );
			DSP_BOOT_OUT	: buffer	std_Logic;
			DSP_SHIFT		: buffer 	std_logic;
			DSP_CLK		: buffer	std_Logic;
			DSP_D0		: buffer	std_logic;
			DSP_MR		: buffer	std_logic );
	end component;		-- DSPBOOT;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component DSPCTRL  
		port( 
	     	IMR            : in      std_logic;                  		-- Local Master Reset
		     DSP_H3		: in      std_logic;                      	-- Local System Clock
			DSP_WR		: in		std_logic;
			DSP_EN		: in		std_logic;
			DSP_STB1		: in		std_logic_Vector( 3 downto 0 );
			DSP_A		: in		std_logic_Vector( 2 downto 0 );
			DSP_A1		: in		std_Logic_vector( 7 downto 0 );
			FF_HF		: in		std_logic;
			INTR_EN		: in		std_logic;
			PFF_FF		: in		std_logic_vector( 1 downto 0 );
			DSP_Read		: buffer	std_Logic;
			DP_OEL		: buffer	std_Logic;
			DP_WRL		: buffer	std_logic;
			WE_DSP_TREG	: buffer	std_logic;
			PFF_WEN		: buffer	std_logic;
			PFF_MR		: buffer	std_logic;
			AO_INTR		: buffer	std_logic );
	end component DSPCTRL;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component FCONFIG 
		port( 
			IMR			: in		std_logic;		-- Master Reset
			CLK20		: in		std_logic;		-- Master Clock
			Config_En		: in		std_logic;		-- COnfiguration Enable
			DATA_LD		: in		std_logic;		-- Load Data from PCI
			CW_LD		: in		std_logic;		-- Load Control Word bit
			PCI_DQ		: in		std_logic_vector( 15 downto 0 );
			SHIFT_EN		: buffer	std_logic;		-- Shift Enable
			DCLK			: buffer	std_logic;		-- DCLK to FPGA
			DATA			: buffer	std_logic;		-- DATA to FPGA
			NCONFIG		: buffer	std_logic );		-- NCONFIG to FPGA
	end component;		-- FCONFIG;
	-------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	component FFCNT is 
		port( 
	     	IMR            : in      std_logic;                  		-- Local Master Reset
			CLK40		: in		std_logic;
			FF_REN		: in		std_logic;
			FF_WEN		: in		std_logic;
			FF_RS		: in		std_Logic;
			FF_HF		: buffer	std_logic;
			FF_CNT		: buffer	std_logic_Vector( 15 downto 0 ) );
	end component FFCNT;
	-----------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component PCICTRL 
		port(
	     	IMR            : in      std_logic;                  		-- Local Master Reset
			CLK20		: in		std_logic;
		     AO_CLK		: in      std_logic;                  		-- Local System Clock
		     OPTATN         : in      std_logic;                      -- Pass Thru Attention
			PTWR           : in      std_logic;                      -- Pass Thru Write
		     ODXFER         : in      std_logic;                      -- Active Transfer Complete
	     	PTNUM          : in      std_logic_vector( 1 downto 0 ); -- Pass Thru Number
		     OPTBE          : in      std_logic_vector( 3 downto 0 ); -- Pass Thru Byte Enable
	     	OPTADR         : in      std_logic;                      -- Active Mode Address Valid
		     PCI_DIN        : in      std_logic_vector( 15 downto 0 );-- PCI Interface Data Bus
			PCI_RST_IN	: in		std_logic;
			PCI_RST_OUT	: buffer	std_logic;
			PFF_REN		: buffer  std_logic;				  -- PCI FIFO Read Enable
			PFF_OE		: buffer	std_logic;
		     PCI_RD         : buffer  std_logic;                       -- Local PCI Read COmmand
			DPR_ACC		: buffer	std_logic;				   -- Dual Port Ram Access
			DP_OER		: buffer	std_Logic;
			DP_WRR		: buffer	std_logic;
			PCI_ADR 		: buffer	std_logic_Vector( 8 downto 0 );		
			PCI_DPRA 		: buffer	std_logic_Vector( 8 downto 0 );		
	     	WE_DCD 	     : buffer  std_logic_vector( 15 downto 0 );
			PCI_WD		: buffer	std_logic_vector( 15 downto 0 ));
	end component;			-- PCICTRL;
	-------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component CLKBUF 
		PORT
		(
			inclock		: IN STD_LOGIC ;
			clock0		: OUT STD_LOGIC ;
			clock1		: OUT STD_LOGIC 
		);
	END component CLKBUF;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	component clk_dbl IS
		PORT
		(
			inclock		: IN STD_LOGIC ;
			clock0		: OUT STD_LOGIC 
		);
	END component clk_dbl;

	-------------------------------------------------------------------------------
begin

	clk_pll1 : CLKBUF 
		port map(
			inclock				=> CLK20_IN,
			clock0				=> CLK20,
			clock1				=> AO_CLK );

	clk_pll2 : clk_dbl 
		port map(
			inclock				=> CLK20_IN,
			clock0				=> CLK40 );
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- PCI Dual Port RAM Access ---------------------------------------------------
	-- 256 X 16
	U_DPRAM : DPRAM 
		port map(
			imr				=> imr,
			clk_a			=> AO_CLK,
			clk_b			=> DSP_H3,
			wren_a			=> DP_WRR,					-- Write from PCI Bus
			wren_b			=> DP_WRL,					-- Write from DSP
			addr_a			=> PCI_DPRA( 8 downto 0 ),		-- PCI Bus Address
			addr_b			=> DSP_DPRA( 8 downto 0 ),		-- DSP Bus Address
			data_a			=> PCI_DQ( 15 downto 0 ),		-- Input Data from PCI Bus
			data_b			=> DSP_D( 15 downto 0 ),
			q_a				=> DP_PCI_D,
			q_b 				=> DP_DSP_D,
			state_str			=> dpram_sstr,
			OpCodeIsr			=> PCI_DSP_IRQ );
	-------------------------------------------------------------------------------

	ST_GEN : if( Signal_Tap = 0 ) generate
		-------------------------------------------------------------------------------
		-- Module to "BOOT" the DSP
		U_DSPBOOT:  DSPBOOT 
			port map(
				IMR			=> IMR,					-- Master Reset
				CLK20		=> CLK20,					-- Master Clock
				DSP_CONFIG	=> DSP_CONFIG,				-- Configuration Bit for the DSP Sequencer
				DSP_BYTE_LO	=> WE_DCD( iADR_DSP_B0 ),	-- Decode of Load low-Byte to the DSP
				DSP_MR_ED		=> WE_DCD( iADR_MR ), 		-- DSP_Master Reset Edge Detect
				DSP_BOOT_IN	=> WE_DCD( iADR_DSP_BOOT ), 	-- DSP Boot from the PCI Bus
				DSP_REG		=> DSP_REG,				-- DSP Boot data ( 32 Bits )
				DSP_BOOT_OUT	=> DSP_BOOT,				-- DSP BOOT to the DSP
				DSP_SHIFT		=> DSP_SHIFT,				-- DSP SHift in progress
				DSP_CLK		=> DSP_CLK,				-- DSP Boot Clock
				DSP_D0		=> DSP_D0,				-- DSP BOot Data
				DSP_MR		=> DSP_MR );				-- DSP Master Reset
		-------------------------------------------------------------------------------

		-------------------------------------------------------------------------------
		U_DSPCTRL : DSPCTRL 
			port map(
	   		IMR            => IMR,
		     	DSP_H3		=> DSP_H3,
				DSP_WR		=> DSP_WR,
				DSP_EN		=> DSP_EN,
				DSP_STB1		=> DSP_STB1,
				DSP_A		=> DSP_A,
				DSP_A1		=> DSP_A1,
				FF_HF		=> FF_HF,
				INTR_EN		=> INTR_EN,
				PFF_FF		=> PFF_FF,
				DSP_READ		=> DSP_Read,
				DP_OEL		=> DP_OEL,
				DP_WRL		=> DP_WRL,
				WE_DSP_TREG	=> WE_DSP_TREG,
				PFF_WEN		=> FF_WEN,
				PFF_MR		=> DSP_FF_MR,
				AO_INTR		=> AO_INTR );
		-------------------------------------------------------------------------------
	end generate;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- FPGA Configuration Logic ---------------------------------------------------
	-- FCONFIG can be as high as 40Mhz
	U_FCONFIG	: FCONFIG
		port map(
			IMR			=> IMR,					-- Master Reset
			CLK20  		=> CLK20,					-- Master Clock
			Config_En		=> FPGA_Config,			-- Configuration Enable
			CW_LD		=> WE_DCD( iADR_FPGA_NC ), 	--  Control Word Load Bit
			DATA_LD		=> WE_DCD( iADR_FPGA_B0 ),	-- Data load from the PCI
			PCI_DQ		=> FPGA_BOOT,				-- PCI Data Bus
			SHIFT_EN		=> FC_SHIFT,				-- SHift Enable
			DCLK			=> FPGA_DCLK,				-- Shift Data Clock
			DATA			=> FPGA_D0,				-- Data Output
			NCONFIG		=> FPGA_NC );				-- NCONFIG
	-- FPGA Configuration Logic ---------------------------------------------------
	-------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	U_FFCNT : FFCNT 
		port map(
			imr			=> imr,
			clk40		=> clk40,
			FF_REN		=> FF_REN,
			FF_WEN		=> FF_WEN,
			FF_RS		=> FF_RS,
			FF_HF		=> FF_HF,
			FF_CNT		=> FF_CNT );
	----------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
     -- PCI Bus Interface
     U_PCICTRL : PCICTRL 
     	port map(
			IMR			=> IMR,			-- Master Reset
			CLK20		=> CLK20,
			AO_CLK		=> AO_CLK,		-- PCI Bus Add-On Clock
			OPTATN		=> OPTATN,		-- Pass-Thru Attention
			PTWR			=> PTWR,			-- Pass-Thru Write
			ODXFER		=> ODXFER,		-- Active Transfer Complete
			PTNUM		=> PTNUM,			-- Pass-Thru Number
			OPTBE		=> OPTBE,			-- Pass-Thru Byte Enable
			OPTADR		=> OPTADR,		-- Active Mode Address Valid
			PCI_DIN		=> PCI_DQ( 15 downto 0 ),		-- PCI Data Bus
			PCI_RST_IN	=> PCI_RST_IN,
			PCI_RST_OUT	=> PCI_RST_OUT,
			PFF_REN		=> FF_REN,		-- PCI FIFO Read Enable
			PFF_OE		=> FF_OE,
			PCI_RD		=> PCI_RD,		-- Local PCI Read Signal
			DPR_ACC		=> DPR_ACC,		-- Dual Port Ram Access
			DP_OER		=> DP_OER,
			DP_WRR		=> DP_WRR,
			PCI_ADR 		=> PCI_ADR,		-- PCI Address
			PCI_DPRA 		=> PCI_DPRA,
			WE_DCD		=> WE_DCD,		-- PCI Address Decode
			PCI_WD		=> PCI_WD );
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- DSP Test Register -------------------------------------------------------------------------
	DSP_TREG_FF : lpm_ff
		generic map(
			LPM_WIDTH		=> 16 )
		port map(	
			aclr			=> IMR,
			clock 		=> AO_CLK, 
			enable		=> WE_DSP_TREG,
			data			=> DSP_D,
			q			=> DSP_TREG );

	-------------------------------------------------------------------------------
	-- Parameter Storage Registers ( Some Require Power-On Initialization Values ) ---------------

	-- Test Register to test 16-bit data path from the PCI Bus
	TREG_FF : lpm_ff
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,		
			enable		=> WE_DCD( iADR_TREG ),
			data			=> PCI_WD,	
			q			=> TREG );

	-- Control Register 
	CREG_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 8 )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,	
			enable		=> WE_DCD( iADR_GLUE_CR ),
			data			=> PCI_WD( 7 downto 0 ),	
			q			=> CREG );

	FPGA_BOOT_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,					
			enable		=> WE_DCD( iADR_FPGA_B0 ),
			data			=> PCI_WD,	
			q			=> FPGA_BOOT );

	-- DSP Boot Register Low Bits
	DSP_REG_FF_0 : LPM_FF
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,		
			enable		=> WE_DCD( iADR_DSP_B0 ),
			data			=> PCI_WD, 
			q			=> DSP_REG( 15 downto 0 ));

	-- DSP Boot Register Low Bits
	DSP_REG_FF_1 : LPM_FF
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,		
			enable		=> WE_DCD( iADR_DSP_B1 ),
			data			=> PCI_WD, 
			q			=> DSP_REG( 31 downto 16 ));

	-- Parameter Storage Registers ( Some Require Power-On Initialization Values ) ---------------
     ----------------------------------------------------------------------------------------------
     ----------------------------------------------------------------------------------------------
	-- Master Reset Generation -------------------------------------------------------------------
	IMR			<= '1' 		when ( S5920MR = '0' ) else '0';	-- Master Reset
	
	OMR			<= '0' 		when ( S5920MR = '0' ) else		-- Master Reset
				   '0' 		when ( DSP_MR  = '1' ) else		-- DSP REset ( Also Resets EDS and SEM FPGAs
				   '1';										--	BUT NOT GLUE FPGA

     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- Register Mapping				
	DSP_CONFIG	<= CREG( 7 );			-- DSP Download Enable
	FPGA_CONFIG	<= CREG( 6 );			-- FPGA Download Enable
	INTR_EN		<= CREG( 5 );			-- Enabled Interrupt to PCI Bus
	DSP_EN		<= CREG( 4 );			-- Enables DSP Access to DPRAM
	TP_SEL		<= CREG( 3 downto 0 );
	
	-- 	CREG Bit 6 is not used ( Interrupt Active Indication )
     ----------------------------------------------------------------------------------------------
	-- DSP_DPRAM_DSP  = 0 on DSP_A(15:13) = "000" -> DSP_A(13) = '0'
	-- DSP_DPRAM_FPGA = 1 on DSP_A(15:13) = "001" -> DSP_A(13) = '1'
	-- TPW min = 25ns for FIFO Reset

	DSP_DPRA		<= DSP_A(13) & DSP_A1;
	PFF_REN 		<= not( FF_REN );
	PFF_WEN		<= not( FF_WEN );
	PFF_RS		<= not( FF_RS );
	PFF_OE		<= not( FF_OE );

	PFF_WCK		<= DSP_H3 when (( FF_WEN = '0' ) and ( DSP_STB1 = x"F" )) else
	                  '0'	when (( FF_WEN = '1' ) and ( DSP_STB1 = x"0" )) else
				   '1';
	

     ----------------------------------------------------------------------------------------------
	-- DSP Configuration 
	OSCLK		<= DSP_CLK		when ( DSP_CONFIG = '1' ) else 'Z';
	SB_SDIN		<= DSP_D0			when ( DSP_CONFIG = '1' ) else 'Z';
	OFSIN		<= DSP_SHIFT		when ( DSP_CONFIG = '1' ) else 'Z';
	SB_INTR		<= not( DSP_BOOT ) 	when ( DSP_CONFIG = '1' ) else 'Z';

	-- PCI Bus Access ---------------------------------------------------------------------------
	-- Final Output, drive the bus or Tristates
	PCI_DQ	<= PCI_DQ_MOUT		when ( PCI_RD = '1' ) else
		        "ZZZZZZZZZZZZZZZZ";	

	-- PCI Data Bus Mux from DPRAM or Other Internal Memories
	PCI_DQ_MOUT	<= DP_PCI_D 	when ( DP_OER = '1' ) else
			   	   PCI_DQ_MUX;

	-- Internal Memory Mux
	with CONV_INTEGER( PCI_ADR ) select
		PCI_DQ_MUX	
			<=   Conv_Std_Logic_Vector( Ver, 16 )		when iADR_ID,
				TREG								when iADR_TREG,
				FF_CNT 			 				when iADR_FF_CNT,
				AO_INTR
				& Zeroes( 14 downto 8 ) 
				& CREG( 7 downto 0 )				when iADR_GLUE_CR,			
					
				DSP_REG( 15 downto  0 )				when iADR_DSP_B0,
				DSP_REG( 31 downto 16 )				when iADR_DSP_B1,
					
				FPGA_BOOT							when iADR_FPGA_B0,

				ZEROES( 15 downto 6 )
					& DSP_CONFIG									-- 5 DSP Configuration Flag
					& DSP_SHIFT									-- 4 DSP Shift in Progress
					& FPGA_CONFIG									-- 3 EDS Configuration Flag
					& FC_SHIFT 									-- 2 Shift in Progress				   
					& FPGA_NS    									-- 1 SEM nSATUS
					& FPGA_CD						when iADR_F_CW,	-- 0 SEM Config Done
				ZEROES( 15 downto 4 )
					& not( PFF_FF )
					& not( PFF_EF )				when iADR_PFF_STAT,
				X"1970" 							when others;
   ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- DSP Data Bus Interface
	DSP_D		<= DSP_D_MOUT	when ( DSP_Read = '1' ) else
		   		"ZZZZZZZZZZZZZZZZ";

	DSP_D_MOUT	<= DP_DSP_D when ( DP_OEL = '1' ) else
				   DSP_D_MUX;

	with CONV_INTEGER( DSP_A1 ) select
		DSP_D_MUX <= 
			Zeroes( 15 downto 4 ) 
				& not( PFF_FF )
				& not( PFF_EF )					when iADR_DSP_PCI_FIFO,
			Conv_Std_Logic_Vector( Ver, 16 )			when iADR_DSP_ID,
			DSP_TREG								when iADR_DSP_TREG,
			FF_CNT								when iADR_DSP_FF_CNT,
		 	X"1970"								when others;
	-- DSP Data Bus Interface
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- Test Point Mux
	with TP_SEL select
		TP		<= 
			----- Basic Status Signals --------------------------------------
--			S5920MR					-- 5 ( aka IMR )
			CLK40_D2
			& PCI_RST_IN 				-- 4
			& PCI_RST_OUT				-- 3
			& DSP_H3					-- 2
			& ao_Clk_d2				-- 1
			& AO_INTR 	  when X"0", 	-- 0
			----- Basic Status Signals --------------------------------------
		
			----- EDS FPGA CONFIGURATION ------------------------------------
			FPGA_CONFIG				-- 5
			& FPGA_NC					-- 4
			& FPGA_NS					-- 3
			& FPGA_CD					-- 2
			& FPGA_DCLK				-- 1
			& FPGA_D0		when X"1",	-- 0
			----- EDS FPGA CONFIGURATION ------------------------------------

			----- DSP CONFIGURATION -----------------------------------------
			OFSIN					-- 5		(DSP_SHIFT)
			& SB_INTR					-- 4		(DSP_BOOT) Start of Configuration
			& WE_DCD( iADR_DSP_BOOT )	-- 3
			& DSP_MR					-- 2
			& OSCLK		            	-- 1		(DSP_CLK)
			& SB_SDIN		when X"2",  	-- 0		(DSP_DATA)
			----- DSP CONFIGURATION -----------------------------------------
			
			----- PCI FIFO --------------------------------------------------
--			& PFF_EF(0)				-- 4 PCI FIFO Empty Flag
			PFF_WCK					-- 5
 			& DSP_STB1(0)				-- 4 DSP Strobe
			& PFF_EF(0)				-- 3 PCI FIFO Full Flag
			& PFF_WEN					-- 2 PCI FIFO Write Enable
			& PFF_REN					-- 1	PCI FIFO Read Enable
			& PFF_OE	    	when X"3",	-- 0	PCI FIFO Reset
			----- PCI FIFO --------------------------------------------------
			"00"
			& FC_SHIFT 				-- 3 SHift Enable
			& FPGA_DCLK 				-- 2 Shift Data Clock
			& FPGA_D0 				-- 1 Data Output
			& FPGA_NC		when X"4",	-- 0

			WE_DSP_TREG				-- 5
			& DSP_WR					-- 4
			& DSP_STB1(0)				-- 3
			& DSP_A1(0)				-- 2
			& DSP_A(13)				-- 1
			& DSP_D(0)	when X"5",	-- 0

			DP_WRR					-- 5
			& FF_OE					-- 4
			& PCI_RD					-- 3
			& '0'					-- 2
			& DPR_ACC					-- 1
			& DP_OER		when X"6",	-- 0
			
			Zeroes( 5 downto 2 )
			& DSP_A(13)				-- 1
			& DP_WRL		when X"7",	-- 0
			
			"00"
			& dpram_sstr	when x"8",
			
		"000000" when others;
	-- Test Point Mux
     ----------------------------------------------------------------------------------------------
	AO_CLK_NR_PROC : process( AO_CLK )
	begin
		if(( AO_CLk'event ) and ( AO_CLK = '1' )) then
			AO_CLK_D2	<= not( AO_CLK_D2 );
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	AO_CLK_PROC : process( imr, CLK20 )	-- was AO_CLK
	begin
		if( imr = '1' ) then

			PFF_RS_VEC	<= "00";
			FF_RS		<= '1';
		elsif(( CLK20'event ) and ( CLK20 = '1' )) then
			if(( WE_DCD( iADR_PFF_RS ) = '1' ) or ( DSP_FF_MR = '1' ))
				then PFF_RS_VEC <= PFF_RS_VEC(0) & '1';
				else PFF_RS_VEC <= PFF_RS_VEC(0) & '0';
			end if;

			if( PFF_RS_VEC /= "00" )
				then FF_RS <= '1';
				else FF_RS <= '0';
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	CLK40_PROC : process( imr, CLK40 )
	begin
		if( imr = '1' ) then
			CLK40_D2  <= '0';
		elsif(( CLK40'Event ) and ( Clk40 = '1' )) then
			CLK40_D2 <= not( CLK40_D2 );
		end if;
	end process;
			
---------------------------------------------------------------------------------------------------
end behavioral;			-- EP99980.VHD ( aka GLUEFPGA )
---------------------------------------------------------------------------------------------------
