-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/06/2006 16:40:29"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	EP99980 IS
    PORT (
	S5920MR : IN std_logic;
	PCI_RST_IN : IN std_logic;
	PCI_RST_OUT : OUT std_logic;
	OMR : OUT std_logic;
	CLK20_IN : IN std_logic;
	AO_CLK : OUT std_logic;
	ODXFER : IN std_logic;
	OPTADR : IN std_logic;
	OPTATN : IN std_logic;
	PTWR : IN std_logic;
	PTNUM : IN std_logic_vector(1 DOWNTO 0);
	OPTBE : IN std_logic_vector(3 DOWNTO 0);
	AO_INTR : OUT std_logic;
	PCI_DSP_IRQ : OUT std_logic;
	PCI_DQ : INOUT std_logic_vector(15 DOWNTO 0);
	DSP_H3 : IN std_logic;
	DSP_WR : IN std_logic;
	DSP_STB1 : IN std_logic_vector(3 DOWNTO 0);
	DSP_A1 : IN std_logic_vector(7 DOWNTO 0);
	DSP_A : IN std_logic_vector(15 DOWNTO 13);
	DSP_D : INOUT std_logic_vector(15 DOWNTO 0);
	SB_INTR : INOUT std_logic;
	SB_SDIN : INOUT std_logic;
	OFSIN : INOUT std_logic;
	OSCLK : INOUT std_logic;
	FPGA_NS : IN std_logic;
	FPGA_CD : IN std_logic;
	FPGA_DCLK : OUT std_logic;
	FPGA_D0 : OUT std_logic;
	FPGA_NC : OUT std_logic;
	PFF_EF : IN std_logic_vector(1 DOWNTO 0);
	PFF_FF : IN std_logic_vector(1 DOWNTO 0);
	PFF_WEN : OUT std_logic;
	PFF_REN : OUT std_logic;
	PFF_OE : OUT std_logic;
	PFF_RS : OUT std_logic;
	PFF_WCK : OUT std_logic;
	TP : OUT std_logic_vector(5 DOWNTO 0)
	);
END EP99980;

ARCHITECTURE structure OF EP99980 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_S5920MR : std_logic;
SIGNAL ww_PCI_RST_IN : std_logic;
SIGNAL ww_PCI_RST_OUT : std_logic;
SIGNAL ww_OMR : std_logic;
SIGNAL ww_CLK20_IN : std_logic;
SIGNAL ww_AO_CLK : std_logic;
SIGNAL ww_ODXFER : std_logic;
SIGNAL ww_OPTADR : std_logic;
SIGNAL ww_OPTATN : std_logic;
SIGNAL ww_PTWR : std_logic;
SIGNAL ww_PTNUM : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_OPTBE : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_AO_INTR : std_logic;
SIGNAL ww_PCI_DSP_IRQ : std_logic;
SIGNAL ww_DSP_H3 : std_logic;
SIGNAL ww_DSP_WR : std_logic;
SIGNAL ww_DSP_STB1 : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_DSP_A1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_DSP_A : std_logic_vector(15 DOWNTO 13);
SIGNAL ww_FPGA_NS : std_logic;
SIGNAL ww_FPGA_CD : std_logic;
SIGNAL ww_FPGA_DCLK : std_logic;
SIGNAL ww_FPGA_D0 : std_logic;
SIGNAL ww_FPGA_NC : std_logic;
SIGNAL ww_PFF_EF : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_PFF_FF : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_PFF_WEN : std_logic;
SIGNAL ww_PFF_REN : std_logic;
SIGNAL ww_PFF_OE : std_logic;
SIGNAL ww_PFF_RS : std_logic;
SIGNAL ww_PFF_WCK : std_logic;
SIGNAL ww_TP : std_logic_vector(5 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL ao_CLK_D2 : std_logic;
SIGNAL Mux_a1134 : std_logic;
SIGNAL U_FCONFIG_aSHIFT_EN_areg0 : std_logic;
SIGNAL CLK40_D2 : std_logic;
SIGNAL Mux_a1154 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a0_a : std_logic;
SIGNAL U_FCONFIG_ai_a251 : std_logic;
SIGNAL U_DPRAM_ai_a217_1 : std_logic;
SIGNAL U_DPRAM_ai_a223_1 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a0_a : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19325 : std_logic;
SIGNAL TREG_FF_adffs_a1_a : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19335 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a2_a : std_logic;
SIGNAL TREG_FF_adffs_a2_a : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19336 : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19337 : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19338 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a3_a : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19349 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a4_a : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19353 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a5_a : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19360 : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19362 : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19363 : std_logic;
SIGNAL TREG_FF_adffs_a7_a : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19370 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19375 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a8_a : std_logic;
SIGNAL TREG_FF_adffs_a8_a : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19377 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a9_a : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19382 : std_logic;
SIGNAL TREG_FF_adffs_a10_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a10_a : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19393 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a11_a : std_logic;
SIGNAL TREG_FF_adffs_a11_a : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19394 : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19395 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a12_a : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19402 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a14_a : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19408 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a15_a : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19411 : std_logic;
SIGNAL TREG_FF_adffs_a15_a : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19412 : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19413 : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19414 : std_logic;
SIGNAL DSP_TREG_FF_adffs_a3_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a4_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a7_a : std_logic;
SIGNAL U_PCICTRL_aPCI_STATE_a8 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a65 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL U_FCONFIG_ai_a3168_1 : std_logic;
SIGNAL U_DPRAM_adata_mux_a0_a_a47 : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_ai_a277 : std_logic;
SIGNAL U_DPRAM_adata_mux_a1_a_a44 : std_logic;
SIGNAL U_DPRAM_adata_mux_a2_a_a41 : std_logic;
SIGNAL U_DPRAM_adata_mux_a3_a_a38 : std_logic;
SIGNAL U_DPRAM_adata_mux_a4_a_a35 : std_logic;
SIGNAL U_DPRAM_adata_mux_a5_a_a32 : std_logic;
SIGNAL U_DPRAM_adata_mux_a6_a_a29 : std_logic;
SIGNAL U_DPRAM_adata_mux_a7_a_a26 : std_logic;
SIGNAL U_DPRAM_adata_mux_a8_a_a23 : std_logic;
SIGNAL U_DPRAM_adata_mux_a9_a_a20 : std_logic;
SIGNAL U_DPRAM_adata_mux_a10_a_a17 : std_logic;
SIGNAL U_DPRAM_adata_mux_a11_a_a14 : std_logic;
SIGNAL U_DPRAM_adata_mux_a12_a_a11 : std_logic;
SIGNAL U_DPRAM_adata_mux_a13_a_a8 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a : std_logic;
SIGNAL U_DPRAM_adata_mux_a14_a_a5 : std_logic;
SIGNAL U_DPRAM_adata_mux_a15_a_a2 : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15 : std_logic;
SIGNAL U_FCONFIG_ai_a260 : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a6_a : std_logic;
SIGNAL U_DPRAM_areduce_nor_122_a34_1 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a0_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a0_a : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a1 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a1_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a1_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a2_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a2_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a3_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a3_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a4_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a4_a : std_logic;
SIGNAL PCI_DQ_MOUT_a4_a_a945 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a5_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a5_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a6_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a6_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a7_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a7_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a8_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a8_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a9_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a9_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a10_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a10_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a11_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a11_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a12_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a12_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a13_a : std_logic;
SIGNAL PCI_DQ_MUX_a13_a_a19418 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a13_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a13_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a14_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a14_a : std_logic;
SIGNAL U_DPRAM_areg_ff_a_d_adffs_a15_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_d_adffs_a15_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a2_a_a5 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a2_a_a4 : std_logic;
SIGNAL U_FFCNT_areduce_nor_20_a1 : std_logic;
SIGNAL U_PCICTRL_adata_en_a62 : std_logic;
SIGNAL U_FCONFIG_ai_a270 : std_logic;
SIGNAL U_FCONFIG_ai_a271 : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a59_1 : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a45_1 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a3_a_a6 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a3_a_a7 : std_logic;
SIGNAL U_FCONFIG_ai_a280 : std_logic;
SIGNAL U_FCONFIG_ai_a281 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a4_a_a9 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a4_a_a8 : std_logic;
SIGNAL U_FCONFIG_ai_a291 : std_logic;
SIGNAL U_FCONFIG_ai_a290 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a5_a_a10 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a5_a_a11 : std_logic;
SIGNAL U_FCONFIG_ai_a301 : std_logic;
SIGNAL U_FCONFIG_ai_a300 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a6_a_a13 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a6_a_a12 : std_logic;
SIGNAL U_FCONFIG_ai_a310 : std_logic;
SIGNAL U_FCONFIG_ai_a311 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a7_a_a14 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a7_a_a15 : std_logic;
SIGNAL U_FCONFIG_ai_a320 : std_logic;
SIGNAL U_FCONFIG_ai_a321 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a8_a_a16 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a8_a_a17 : std_logic;
SIGNAL U_FCONFIG_ai_a330 : std_logic;
SIGNAL U_FCONFIG_ai_a331 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a9_a_a19 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a9_a_a18 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a : std_logic;
SIGNAL U_FCONFIG_ai_a340 : std_logic;
SIGNAL U_FCONFIG_ai_a341 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a10_a_a20 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a10_a_a21 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a : std_logic;
SIGNAL U_FCONFIG_ai_a351 : std_logic;
SIGNAL U_FCONFIG_ai_a350 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a11_a_a22 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a11_a_a23 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a : std_logic;
SIGNAL U_FCONFIG_ai_a361 : std_logic;
SIGNAL U_FCONFIG_ai_a360 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a12_a_a25 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a12_a_a24 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a : std_logic;
SIGNAL U_FCONFIG_ai_a371 : std_logic;
SIGNAL U_FCONFIG_ai_a370 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a13_a_a26 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a13_a_a27 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a : std_logic;
SIGNAL U_FCONFIG_ai_a380 : std_logic;
SIGNAL U_FCONFIG_ai_a381 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a14_a_a29 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a14_a_a28 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a : std_logic;
SIGNAL U_FCONFIG_ai_a391 : std_logic;
SIGNAL U_FCONFIG_ai_a390 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a15_a_a30 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a15_a_a31 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a : std_logic;
SIGNAL U_FCONFIG_ai_a154 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a : std_logic;
SIGNAL CLK20_IN_acombout : std_logic;
SIGNAL PTNUM_a1_a_acombout : std_logic;
SIGNAL PCI_DQ_a0_a_a15 : std_logic;
SIGNAL PCI_DQ_a1_a_a14 : std_logic;
SIGNAL PCI_DQ_a2_a_a13 : std_logic;
SIGNAL PCI_DQ_a3_a_a12 : std_logic;
SIGNAL PCI_DQ_a4_a_a11 : std_logic;
SIGNAL PCI_DQ_a5_a_a10 : std_logic;
SIGNAL PCI_DQ_a6_a_a9 : std_logic;
SIGNAL PCI_DQ_a7_a_a8 : std_logic;
SIGNAL PCI_DQ_a8_a_a7 : std_logic;
SIGNAL PCI_DQ_a9_a_a6 : std_logic;
SIGNAL PCI_DQ_a10_a_a5 : std_logic;
SIGNAL PCI_DQ_a11_a_a4 : std_logic;
SIGNAL PCI_DQ_a12_a_a3 : std_logic;
SIGNAL PCI_DQ_a13_a_a2 : std_logic;
SIGNAL PCI_DQ_a14_a_a1 : std_logic;
SIGNAL PCI_DQ_a15_a_a0 : std_logic;
SIGNAL DSP_D_a0_a_a15 : std_logic;
SIGNAL DSP_D_a1_a_a14 : std_logic;
SIGNAL DSP_D_a2_a_a13 : std_logic;
SIGNAL DSP_D_a3_a_a12 : std_logic;
SIGNAL DSP_D_a4_a_a11 : std_logic;
SIGNAL DSP_D_a5_a_a10 : std_logic;
SIGNAL DSP_D_a6_a_a9 : std_logic;
SIGNAL DSP_D_a7_a_a8 : std_logic;
SIGNAL DSP_D_a8_a_a7 : std_logic;
SIGNAL DSP_D_a9_a_a6 : std_logic;
SIGNAL DSP_D_a10_a_a5 : std_logic;
SIGNAL DSP_D_a11_a_a4 : std_logic;
SIGNAL DSP_D_a12_a_a3 : std_logic;
SIGNAL DSP_D_a13_a_a2 : std_logic;
SIGNAL DSP_D_a14_a_a1 : std_logic;
SIGNAL DSP_D_a15_a_a0 : std_logic;
SIGNAL SB_INTR_a1 : std_logic;
SIGNAL SB_SDIN_a1 : std_logic;
SIGNAL OFSIN_a1 : std_logic;
SIGNAL OSCLK_a1 : std_logic;
SIGNAL clk_pll1_aaltclklock_component_aoutclock0 : std_logic;
SIGNAL PCI_RST_IN_acombout : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a109 : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a70 : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a79 : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a92 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a153 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18 : std_logic;
SIGNAL U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL U_PCICTRL_areduce_nor_18_a158 : std_logic;
SIGNAL U_PCICTRL_aPCI_STATE_a9 : std_logic;
SIGNAL U_PCICTRL_aPCI_STATE_a10 : std_logic;
SIGNAL U_PCICTRL_aPCI_RST_OUT_areg0 : std_logic;
SIGNAL S5920MR_acombout : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153 : std_logic;
SIGNAL clk_pll1_aaltclklock_component_aoutclock1 : std_logic;
SIGNAL OPTADR_acombout : std_logic;
SIGNAL OPTATN_acombout : std_logic;
SIGNAL OPTBE_a2_a_acombout : std_logic;
SIGNAL OPTBE_a1_a_acombout : std_logic;
SIGNAL OPTBE_a0_a_acombout : std_logic;
SIGNAL PTNUM_a0_a_acombout : std_logic;
SIGNAL OPTBE_a3_a_acombout : std_logic;
SIGNAL U_PCICTRL_adata_en_a71 : std_logic;
SIGNAL U_PCICTRL_adata_en_a69 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a8_a_a1 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a1_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a0_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a3_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a5_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a6_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a7_a_areg0 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_66_a27 : std_logic;
SIGNAL ODXFER_acombout : std_logic;
SIGNAL PTWR_acombout : std_logic;
SIGNAL U_PCICTRL_adata_en_a72 : std_logic;
SIGNAL U_PCICTRL_adata_en_a70 : std_logic;
SIGNAL U_PCICTRL_adata_en_ao : std_logic;
SIGNAL U_PCICTRL_adata_en_c20_vec_a0_a : std_logic;
SIGNAL U_PCICTRL_adata_en_c20_vec_a1_a : std_logic;
SIGNAL U_PCICTRL_ai_a24 : std_logic;
SIGNAL U_PCICTRL_aDPR_ACC_areg0 : std_logic;
SIGNAL U_PCICTRL_ai_a668 : std_logic;
SIGNAL U_PCICTRL_ai_a559 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a9_a_areg0 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 : std_logic;
SIGNAL OMR_a2 : std_logic;
SIGNAL U_PCICTRL_adata_en_a4 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a5_a : std_logic;
SIGNAL U_PCICTRL_ai_a560 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a2_a_areg0 : std_logic;
SIGNAL CREG_FF_adffs_a5_a : std_logic;
SIGNAL PFF_FF_a1_a_acombout : std_logic;
SIGNAL PFF_FF_a0_a_acombout : std_logic;
SIGNAL DSP_H3_acombout : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a : std_logic;
SIGNAL U_PCICTRL_ai_a626 : std_logic;
SIGNAL U_PCICTRL_ai_a631 : std_logic;
SIGNAL U_PCICTRL_ai_a651 : std_logic;
SIGNAL U_PCICTRL_adata_en_a40 : std_logic;
SIGNAL U_PCICTRL_ai_a572 : std_logic;
SIGNAL U_PCICTRL_ai_a15 : std_logic;
SIGNAL clk_pll2_aaltclklock_component_aoutclock0 : std_logic;
SIGNAL U_FFCNT_aFF_RENV_a0_a : std_logic;
SIGNAL U_FFCNT_aFF_RENV_a1_a : std_logic;
SIGNAL DSP_A1_a2_a_acombout : std_logic;
SIGNAL DSP_A1_a1_a_acombout : std_logic;
SIGNAL DSP_A1_a7_a_acombout : std_logic;
SIGNAL DSP_A1_a4_a_acombout : std_logic;
SIGNAL DSP_A1_a3_a_acombout : std_logic;
SIGNAL DSP_A1_a6_a_acombout : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a366 : std_logic;
SIGNAL DSP_A_a15_a_acombout : std_logic;
SIGNAL DSP_WR_acombout : std_logic;
SIGNAL DSP_A_a14_a_acombout : std_logic;
SIGNAL DSP_STB1_a1_a_acombout : std_logic;
SIGNAL DSP_STB1_a2_a_acombout : std_logic;
SIGNAL DSP_STB1_a3_a_acombout : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a442 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a441 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a438 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 : std_logic;
SIGNAL U_FFCNT_aFF_WENV_a0_a : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a0COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a1COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a2 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a1 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a0 : std_logic;
SIGNAL DSP_A1_a0_a_acombout : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a406 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a439 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a8_a_areg0 : std_logic;
SIGNAL PFF_RS_VEC_a0_a : std_logic;
SIGNAL PFF_RS_VEC_a1_a : std_logic;
SIGNAL FF_RS : std_logic;
SIGNAL U_FFCNT_aFF_WENV_a1_a : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a2COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a3COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a4COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a5COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a6COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a7COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a8COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a9 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a8 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a7 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a6 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a5 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a4 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a3 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a9COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a10 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a10COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a11 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a : std_logic;
SIGNAL U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a45 : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a108 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a11COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a12COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a13COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a14 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a13 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a12 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a : std_logic;
SIGNAL U_FFCNT_areduce_nor_19 : std_logic;
SIGNAL U_FFCNT_ai_a95 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a59 : std_logic;
SIGNAL U_FFCNT_areduce_nor_19_a109 : std_logic;
SIGNAL U_FFCNT_areduce_nor_20 : std_logic;
SIGNAL U_FFCNT_ai_a96 : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a14COUT : std_logic;
SIGNAL U_FFCNT_aadd_35_rtl_0_a15 : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a : std_logic;
SIGNAL U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a120 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a252 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a440 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a248 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a127 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0 : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL U_FCONFIG_ai_a3227 : std_logic;
SIGNAL U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL U_FCONFIG_ai_a3140 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a2_a_areg0 : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a4_a_areg0 : std_logic;
SIGNAL U_PCICTRL_ai_a580 : std_logic;
SIGNAL U_PCICTRL_ai_a577 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a5_a_areg0 : std_logic;
SIGNAL U_FCONFIG_aDATA_LD_VEC_a0_a : std_logic;
SIGNAL U_FCONFIG_aDATA_LD_VEC_a1_a : std_logic;
SIGNAL U_FCONFIG_ai_a3210 : std_logic;
SIGNAL U_FCONFIG_aiSHIFT_EN : std_logic;
SIGNAL U_FCONFIG_aiDCLK : std_logic;
SIGNAL U_FCONFIG_aDCLK_areg0 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a6_a : std_logic;
SIGNAL CREG_FF_adffs_a6_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a0_a_a0 : std_logic;
SIGNAL U_FCONFIG_ai_a3206 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a1_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a1_a : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a1_a_a2 : std_logic;
SIGNAL U_FCONFIG_ai_a261 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a1_a_a3 : std_logic;
SIGNAL U_FCONFIG_ai_a250 : std_logic;
SIGNAL U_FCONFIG_aFPGA_REG_a0_a_a1 : std_logic;
SIGNAL U_FCONFIG_aDATA_areg0 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a6_a_areg0 : std_logic;
SIGNAL U_FCONFIG_aCW_LD_VEC_a0_a : std_logic;
SIGNAL U_FCONFIG_aCW_LD_VEC_a1_a : std_logic;
SIGNAL U_FCONFIG_anstatus_Vec_a0_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL U_FCONFIG_ai_a3168 : std_logic;
SIGNAL U_FCONFIG_ai_a3239 : std_logic;
SIGNAL U_FCONFIG_anstatus_Vec_a1_a : std_logic;
SIGNAL U_FCONFIG_ai_a241 : std_logic;
SIGNAL U_FCONFIG_aNCONFIG_areg0 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_66_a36 : std_logic;
SIGNAL U_PCICTRL_ai_a563 : std_logic;
SIGNAL U_PCICTRL_ai_a138 : std_logic;
SIGNAL PFF_WCK_a249 : std_logic;
SIGNAL PFF_WCK_a248 : std_logic;
SIGNAL PFF_WCK_a250 : std_logic;
SIGNAL CREG_FF_adffs_a1_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a3_a : std_logic;
SIGNAL CREG_FF_adffs_a3_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a2_a : std_logic;
SIGNAL CREG_FF_adffs_a2_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a0_a : std_logic;
SIGNAL CREG_FF_adffs_a0_a : std_logic;
SIGNAL Mux_a1121 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a4_a : std_logic;
SIGNAL CREG_FF_adffs_a4_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a320 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a96 : std_logic;
SIGNAL U_DPRAM_awren_b_cb : std_logic;
SIGNAL U_DPRAM_awren_b_ca : std_logic;
SIGNAL U_DPRAM_alast : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a8_a_a1 : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a1_a_areg0 : std_logic;
SIGNAL U_PCICTRL_ai_a123 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a1_a : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a7_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a7_a : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a8_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a8_a : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a4_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a4_a : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a3_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a3_a : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a2_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a2_a : std_logic;
SIGNAL U_DPRAM_areduce_nor_122_a34 : std_logic;
SIGNAL U_DPRAM_areduce_nor_122_a57 : std_logic;
SIGNAL U_DPRAM_astate_a174 : std_logic;
SIGNAL U_DPRAM_ai_a217 : std_logic;
SIGNAL U_DPRAM_astate_a15 : std_logic;
SIGNAL U_DPRAM_awren_b_lat : std_logic;
SIGNAL U_DPRAM_ai_a223 : std_logic;
SIGNAL U_DPRAM_astate_a9 : std_logic;
SIGNAL U_DPRAM_astate_a171 : std_logic;
SIGNAL U_DPRAM_aSelect_147_rtl_0_a83 : std_logic;
SIGNAL U_DPRAM_awren_a_lat : std_logic;
SIGNAL U_DPRAM_astate_a169 : std_logic;
SIGNAL U_DPRAM_aSelect_147_rtl_0_a0 : std_logic;
SIGNAL U_DPRAM_astate_a173 : std_logic;
SIGNAL U_DPRAM_astate_a11 : std_logic;
SIGNAL U_DPRAM_astate_a12 : std_logic;
SIGNAL U_DPRAM_astate_a13 : std_logic;
SIGNAL U_DPRAM_areduce_or_25 : std_logic;
SIGNAL Mux_a1122 : std_logic;
SIGNAL Mux_a1128 : std_logic;
SIGNAL Mux_a1126 : std_logic;
SIGNAL Mux_a1127 : std_logic;
SIGNAL U_PCICTRL_ai_a700 : std_logic;
SIGNAL U_PCICTRL_ai_a699 : std_logic;
SIGNAL Mux_a1123 : std_logic;
SIGNAL Mux_a1124 : std_logic;
SIGNAL Mux_a1125 : std_logic;
SIGNAL Mux_a1129 : std_logic;
SIGNAL U_DPRAM_areduce_or_22 : std_logic;
SIGNAL Mux_a1133 : std_logic;
SIGNAL Mux_a1131 : std_logic;
SIGNAL Mux_a1130 : std_logic;
SIGNAL DSP_A_a13_a_acombout : std_logic;
SIGNAL Mux_a1132 : std_logic;
SIGNAL Mux_a1135 : std_logic;
SIGNAL Mux_a1136 : std_logic;
SIGNAL FPGA_CD_acombout : std_logic;
SIGNAL Mux_a1139 : std_logic;
SIGNAL Mux_a1140 : std_logic;
SIGNAL U_DPRAM_aSelect_151_rtl_1_a13 : std_logic;
SIGNAL U_DPRAM_areduce_or_19 : std_logic;
SIGNAL Mux_a1162 : std_logic;
SIGNAL Mux_a1161 : std_logic;
SIGNAL Mux_a1138 : std_logic;
SIGNAL Mux_a1141 : std_logic;
SIGNAL U_PCICTRL_areduce_nor_66 : std_logic;
SIGNAL U_PCICTRL_ai_a557 : std_logic;
SIGNAL U_PCICTRL_ai_a69 : std_logic;
SIGNAL Mux_a1146 : std_logic;
SIGNAL DSP_STB1_a0_a_acombout : std_logic;
SIGNAL Mux_a1145 : std_logic;
SIGNAL PFF_EF_a0_a_acombout : std_logic;
SIGNAL FPGA_NS_acombout : std_logic;
SIGNAL Mux_a1142 : std_logic;
SIGNAL Mux_a1143 : std_logic;
SIGNAL Mux_a1144 : std_logic;
SIGNAL Mux_a1147 : std_logic;
SIGNAL Mux_a1150 : std_logic;
SIGNAL Mux_a1151 : std_logic;
SIGNAL Mux_a1149 : std_logic;
SIGNAL Mux_a1152 : std_logic;
SIGNAL Mux_a1155 : std_logic;
SIGNAL Mux_a1148 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a2 : std_logic;
SIGNAL Mux_a1153 : std_logic;
SIGNAL Mux_a1156 : std_logic;
SIGNAL U_DPRAM_aWrite_En : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a0_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a0_a : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a0_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a0_a_a2 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a1_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a1_a_a5 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a2_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a2_a_a8 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a3_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a3_a_a11 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a4_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a4_a_a14 : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a5_a_areg0 : std_logic;
SIGNAL U_DPRAM_areg_ff_a_a_adffs_a5_a : std_logic;
SIGNAL DSP_A1_a5_a_acombout : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a5_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a5_a_a17 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a6_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a6_a_a20 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a7_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a7_a_a23 : std_logic;
SIGNAL U_DPRAM_areg_ff_b_a_adffs_a8_a : std_logic;
SIGNAL U_DPRAM_aaddr_mux_a8_a_a26 : std_logic;
SIGNAL U_PCICTRL_aPCI_DPRA_a6_a_areg0 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a0_a : std_logic;
SIGNAL U_PCICTRL_aPCI_ADR_a8_a_areg0 : std_logic;
SIGNAL PCI_DQ_MOUT_a15_a_a906 : std_logic;
SIGNAL PCI_DQ_MOUT_a15_a_a947 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a12_a_areg0 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a0_a : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19320 : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19321 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a1_a_areg0 : std_logic;
SIGNAL TREG_FF_adffs_a0_a : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19322 : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19323 : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19324 : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19326 : std_logic;
SIGNAL PCI_DQ_MOUT_a0_a_a915 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a1_a : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a11_a_areg0 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a1_a : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19332 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a1_a : std_logic;
SIGNAL PFF_EF_a1_a_acombout : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19327 : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19328 : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19329 : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19330 : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19331 : std_logic;
SIGNAL PCI_DQ_MUX_a1_a_a19333 : std_logic;
SIGNAL PCI_DQ_MOUT_a1_a_a916 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a2_a : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a2_a : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19339 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a2_a : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19334 : std_logic;
SIGNAL PCI_DQ_MUX_a2_a_a19340 : std_logic;
SIGNAL PCI_DQ_MOUT_a2_a_a917 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a3_a : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19342 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a3_a : std_logic;
SIGNAL TREG_FF_adffs_a3_a : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19343 : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19344 : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19345 : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19346 : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19341 : std_logic;
SIGNAL PCI_DQ_MUX_a3_a_a19347 : std_logic;
SIGNAL PCI_DQ_MOUT_a3_a_a918 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a4_a : std_logic;
SIGNAL TREG_FF_adffs_a4_a : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19350 : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19351 : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19352 : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19348 : std_logic;
SIGNAL PCI_DQ_MUX_a4_a_a19354 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a4_a : std_logic;
SIGNAL PCI_DQ_MOUT_a4_a_a952 : std_logic;
SIGNAL PCI_DQ_MOUT_a4_a_a948 : std_logic;
SIGNAL PCI_DQ_MOUT_a4_a_a920 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a5_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a5_a : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19356 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a7_a : std_logic;
SIGNAL CREG_FF_adffs_a7_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a5_a : std_logic;
SIGNAL TREG_FF_adffs_a5_a : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19357 : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19358 : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19359 : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19355 : std_logic;
SIGNAL PCI_DQ_MUX_a5_a_a19361 : std_logic;
SIGNAL PCI_DQ_MOUT_a5_a_a922 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a6_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a6_a : std_logic;
SIGNAL TREG_FF_adffs_a6_a : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19364 : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19365 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a6_a : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19366 : std_logic;
SIGNAL PCI_DQ_MUX_a6_a_a19367 : std_logic;
SIGNAL PCI_DQ_MOUT_a6_a_a924 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a7_a : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19368 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a7_a : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19369 : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19371 : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19372 : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19373 : std_logic;
SIGNAL PCI_DQ_MUX_a7_a_a19374 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a7_a : std_logic;
SIGNAL PCI_DQ_MOUT_a7_a_a925 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a8_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a8_a : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a8_a : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19379 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a8_a : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19376 : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19378 : std_logic;
SIGNAL PCI_DQ_MUX_a8_a_a19380 : std_logic;
SIGNAL PCI_DQ_MOUT_a8_a_a927 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a9_a : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a9_a : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19385 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a9_a : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19387 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a9_a : std_logic;
SIGNAL PCI_DQ_MOUT_a9_a_a928 : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19383 : std_logic;
SIGNAL TREG_FF_adffs_a9_a : std_logic;
SIGNAL PCI_DQ_MUX_a0_a_a19381 : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19384 : std_logic;
SIGNAL PCI_DQ_MOUT_a9_a_a929 : std_logic;
SIGNAL PCI_DQ_MUX_a10_a_a19390 : std_logic;
SIGNAL PCI_DQ_MUX_a9_a_a19386 : std_logic;
SIGNAL PCI_DQ_MUX_a10_a_a19391 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a10_a : std_logic;
SIGNAL PCI_DQ_MOUT_a10_a_a930 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a10_a : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a10_a : std_logic;
SIGNAL PCI_DQ_MUX_a10_a_a19388 : std_logic;
SIGNAL PCI_DQ_MUX_a10_a_a19389 : std_logic;
SIGNAL PCI_DQ_MOUT_a10_a_a931 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a11_a : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a11_a : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19396 : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19392 : std_logic;
SIGNAL PCI_DQ_MUX_a11_a_a19397 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a11_a : std_logic;
SIGNAL PCI_DQ_MOUT_a11_a_a933 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a12_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a12_a : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19398 : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19399 : std_logic;
SIGNAL FPGA_BOOT_FF_adffs_a12_a : std_logic;
SIGNAL TREG_FF_adffs_a12_a : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19400 : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19401 : std_logic;
SIGNAL PCI_DQ_MUX_a12_a_a19403 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a12_a : std_logic;
SIGNAL PCI_DQ_MOUT_a12_a_a935 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a13_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a13_a : std_logic;
SIGNAL TREG_FF_adffs_a13_a : std_logic;
SIGNAL PCI_DQ_MUX_a13_a_a19420 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a13_a : std_logic;
SIGNAL PCI_DQ_MUX_a13_a_a19421 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a13_a : std_logic;
SIGNAL PCI_DQ_MUX_a13_a_a19419 : std_logic;
SIGNAL PCI_DQ_MOUT_a15_a_a951 : std_logic;
SIGNAL PCI_DQ_MOUT_a13_a_a949 : std_logic;
SIGNAL PCI_DQ_MOUT_a13_a_a937 : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a14_a : std_logic;
SIGNAL TREG_FF_adffs_a14_a : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19405 : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a14_a : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19406 : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19407 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a14_a : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19404 : std_logic;
SIGNAL PCI_DQ_MUX_a14_a_a19409 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a14_a : std_logic;
SIGNAL PCI_DQ_MOUT_a14_a_a938 : std_logic;
SIGNAL U_DPRAM_adpram_ram_a_asram_aq_a15_a : std_logic;
SIGNAL U_PCICTRL_aData_FF_adffs_a15_a : std_logic;
SIGNAL DSP_REG_FF_0_adffs_a15_a : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19410 : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19415 : std_logic;
SIGNAL PCI_DQ_MUX_a15_a_a19416 : std_logic;
SIGNAL PCI_DQ_MOUT_a15_a_a939 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a0_a : std_logic;
SIGNAL DSP_D_Mout_a14_a_a2435 : std_logic;
SIGNAL DSP_D_Mout_a14_a_a2436 : std_logic;
SIGNAL DSP_TREG_FF_adffs_a0_a : std_logic;
SIGNAL DSP_D_MUX_a0_a_a10255 : std_logic;
SIGNAL DSP_D_MUX_a0_a_a10254 : std_logic;
SIGNAL DSP_D_MUX_a0_a_a10256 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a41 : std_logic;
SIGNAL DSP_D_Mout_a0_a_a2437 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a331 : std_logic;
SIGNAL a_aST_GEN_aU_DSPCTRL_ai_a119 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a1_a : std_logic;
SIGNAL DSP_D_MUX_a1_a_a10257 : std_logic;
SIGNAL DSP_TREG_FF_adffs_a1_a : std_logic;
SIGNAL DSP_D_MUX_a1_a_a10258 : std_logic;
SIGNAL DSP_D_MUX_a1_a_a10259 : std_logic;
SIGNAL DSP_D_Mout_a1_a_a2438 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a2_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a2_a : std_logic;
SIGNAL DSP_D_MUX_a2_a_a10260 : std_logic;
SIGNAL DSP_D_MUX_a2_a_a10261 : std_logic;
SIGNAL DSP_D_Mout_a2_a_a2439 : std_logic;
SIGNAL DSP_D_Mout_a2_a_a2440 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a3_a : std_logic;
SIGNAL DSP_D_MUX_a3_a_a10262 : std_logic;
SIGNAL DSP_D_MUX_a3_a_a10263 : std_logic;
SIGNAL DSP_D_Mout_a3_a_a2441 : std_logic;
SIGNAL DSP_D_Mout_a3_a_a2442 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a4_a : std_logic;
SIGNAL DSP_D_MUX_a4_a_a10264 : std_logic;
SIGNAL DSP_D_MUX_a4_a_a10265 : std_logic;
SIGNAL DSP_D_MUX_a4_a_a10266 : std_logic;
SIGNAL DSP_D_Mout_a14_a_a2443 : std_logic;
SIGNAL DSP_D_MUX_a12_a_a10267 : std_logic;
SIGNAL DSP_D_Mout_a4_a_a2444 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a5_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a5_a : std_logic;
SIGNAL DSP_D_MUX_a5_a_a10268 : std_logic;
SIGNAL DSP_D_MUX_a5_a_a10269 : std_logic;
SIGNAL DSP_D_Mout_a5_a_a2445 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a6_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a6_a : std_logic;
SIGNAL DSP_D_MUX_a6_a_a10270 : std_logic;
SIGNAL DSP_D_MUX_a6_a_a10271 : std_logic;
SIGNAL DSP_D_Mout_a6_a_a2446 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a7_a : std_logic;
SIGNAL DSP_D_MUX_a14_a_a10272 : std_logic;
SIGNAL DSP_D_MUX_a7_a_a10273 : std_logic;
SIGNAL DSP_D_Mout_a7_a_a2447 : std_logic;
SIGNAL DSP_D_Mout_a7_a_a2448 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a8_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a8_a : std_logic;
SIGNAL DSP_D_MUX_a8_a_a10274 : std_logic;
SIGNAL DSP_D_MUX_a8_a_a10275 : std_logic;
SIGNAL DSP_D_Mout_a8_a_a2449 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a9_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a9_a : std_logic;
SIGNAL DSP_D_MUX_a9_a_a10276 : std_logic;
SIGNAL DSP_D_Mout_a9_a_a2450 : std_logic;
SIGNAL DSP_D_Mout_a9_a_a2451 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a10_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a10_a : std_logic;
SIGNAL DSP_D_MUX_a10_a_a10277 : std_logic;
SIGNAL DSP_D_Mout_a10_a_a2452 : std_logic;
SIGNAL DSP_D_Mout_a10_a_a2453 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a11_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a11_a : std_logic;
SIGNAL DSP_D_MUX_a11_a_a10278 : std_logic;
SIGNAL DSP_D_MUX_a11_a_a10279 : std_logic;
SIGNAL DSP_D_Mout_a11_a_a2454 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a12_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a12_a : std_logic;
SIGNAL DSP_D_MUX_a12_a_a10280 : std_logic;
SIGNAL DSP_D_MUX_a12_a_a10281 : std_logic;
SIGNAL DSP_D_Mout_a12_a_a2455 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a13_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a13_a : std_logic;
SIGNAL DSP_D_MUX_a13_a_a10282 : std_logic;
SIGNAL DSP_D_Mout_a13_a_a2456 : std_logic;
SIGNAL DSP_D_Mout_a13_a_a2457 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a14_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a14_a : std_logic;
SIGNAL DSP_D_MUX_a14_a_a10291 : std_logic;
SIGNAL DSP_D_MUX_a14_a_a10290 : std_logic;
SIGNAL DSP_D_MUX_a14_a_a10284 : std_logic;
SIGNAL DSP_D_Mout_a14_a_a2458 : std_logic;
SIGNAL U_DPRAM_adpram_ram_b_asram_aq_a15_a : std_logic;
SIGNAL DSP_TREG_FF_adffs_a15_a : std_logic;
SIGNAL DSP_D_MUX_a15_a_a10285 : std_logic;
SIGNAL DSP_D_Mout_a15_a_a2459 : std_logic;
SIGNAL DSP_D_Mout_a15_a_a2460 : std_logic;
SIGNAL U_PCICTRL_aWE_DCD_a10_a_areg0 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_ai_a2 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_ai_a242 : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0 : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a11_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a7_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a6_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a4_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a3_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a10_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a : std_logic;
SIGNAL DSP_REG_FF_1_adffs_a15_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a : std_logic;
SIGNAL a_aST_GEN_aU_DSPBOOT_aDSP_D0_areg0 : std_logic;
SIGNAL U_DPRAM_astate_a14 : std_logic;
SIGNAL U_DPRAM_aOpCodeISR_areg0 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 : std_logic;
SIGNAL ALT_INV_OMR_a2 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0 : std_logic;
SIGNAL ALT_INV_U_FCONFIG_aNCONFIG_areg0 : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 : std_logic;
SIGNAL ALT_INV_U_PCICTRL_ai_a138 : std_logic;
SIGNAL ALT_INV_FF_RS : std_logic;
SIGNAL ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2 : std_logic;
SIGNAL ALT_INV_U_DPRAM_aOpCodeISR_areg0 : std_logic;
SIGNAL ALT_INV_U_FCONFIG_aiSHIFT_EN : std_logic;
SIGNAL ALT_INV_U_FFCNT_ai_a95 : std_logic;
SIGNAL ALT_INV_S5920MR_acombout : std_logic;
SIGNAL ALT_INV_PCI_RST_IN_acombout : std_logic;

BEGIN

ww_S5920MR <= S5920MR;
ww_PCI_RST_IN <= PCI_RST_IN;
PCI_RST_OUT <= ww_PCI_RST_OUT;
OMR <= ww_OMR;
ww_CLK20_IN <= CLK20_IN;
AO_CLK <= ww_AO_CLK;
ww_ODXFER <= ODXFER;
ww_OPTADR <= OPTADR;
ww_OPTATN <= OPTATN;
ww_PTWR <= PTWR;
ww_PTNUM <= PTNUM;
ww_OPTBE <= OPTBE;
AO_INTR <= ww_AO_INTR;
PCI_DSP_IRQ <= ww_PCI_DSP_IRQ;
ww_DSP_H3 <= DSP_H3;
ww_DSP_WR <= DSP_WR;
ww_DSP_STB1 <= DSP_STB1;
ww_DSP_A1 <= DSP_A1;
ww_DSP_A <= DSP_A;
ww_FPGA_NS <= FPGA_NS;
ww_FPGA_CD <= FPGA_CD;
FPGA_DCLK <= ww_FPGA_DCLK;
FPGA_D0 <= ww_FPGA_D0;
FPGA_NC <= ww_FPGA_NC;
ww_PFF_EF <= PFF_EF;
ww_PFF_FF <= PFF_FF;
PFF_WEN <= ww_PFF_WEN;
PFF_REN <= ww_PFF_REN;
PFF_OE <= ww_PFF_OE;
PFF_RS <= ww_PFF_RS;
PFF_WCK <= ww_PFF_WCK;
TP <= ww_TP;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_PCICTRL_aPCI_DPRA_a8_a_areg0 & U_PCICTRL_aPCI_DPRA_a7_a_areg0 & U_PCICTRL_aPCI_DPRA_a6_a_areg0 & U_PCICTRL_aPCI_DPRA_a5_a_areg0 & 
U_PCICTRL_aPCI_DPRA_a4_a_areg0 & U_PCICTRL_aPCI_DPRA_a3_a_areg0 & U_PCICTRL_aPCI_DPRA_a2_a_areg0 & U_PCICTRL_aPCI_DPRA_a1_a_areg0 & U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & U_DPRAM_aaddr_mux_a8_a_a26 & U_DPRAM_aaddr_mux_a7_a_a23 & U_DPRAM_aaddr_mux_a6_a_a20 & U_DPRAM_aaddr_mux_a5_a_a17 & U_DPRAM_aaddr_mux_a4_a_a14 & 
U_DPRAM_aaddr_mux_a3_a_a11 & U_DPRAM_aaddr_mux_a2_a_a8 & U_DPRAM_aaddr_mux_a1_a_a5 & U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & DSP_A_a13_a_acombout & DSP_A1_a7_a_acombout & DSP_A1_a6_a_acombout & DSP_A1_a5_a_acombout & DSP_A1_a4_a_acombout & DSP_A1_a3_a_acombout & 
DSP_A1_a2_a_acombout & DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout);
ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0 <= NOT a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0;
ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0 <= NOT a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0;
ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 <= NOT a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0;
ALT_INV_OMR_a2 <= NOT OMR_a2;
ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 <= NOT a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0;
ALT_INV_a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0 <= NOT a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0;
ALT_INV_U_FCONFIG_aNCONFIG_areg0 <= NOT U_FCONFIG_aNCONFIG_areg0;
ALT_INV_a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 <= NOT a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0;
ALT_INV_U_PCICTRL_ai_a138 <= NOT U_PCICTRL_ai_a138;
ALT_INV_FF_RS <= NOT FF_RS;
ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2 <= NOT a_aST_GEN_aU_DSPCTRL_ai_a2;
ALT_INV_U_DPRAM_aOpCodeISR_areg0 <= NOT U_DPRAM_aOpCodeISR_areg0;
ALT_INV_U_FCONFIG_aiSHIFT_EN <= NOT U_FCONFIG_aiSHIFT_EN;
ALT_INV_U_FFCNT_ai_a95 <= NOT U_FFCNT_ai_a95;
ALT_INV_S5920MR_acombout <= NOT S5920MR_acombout;
ALT_INV_PCI_RST_IN_acombout <= NOT PCI_RST_IN_acombout;

ao_CLK_D2_aI : apex20ke_lcell
-- Equation(s):
-- ao_CLK_D2 = DFFE(!ao_CLK_D2, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), , , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => ao_CLK_D2,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ao_CLK_D2);

Mux_a1134_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1134 = CREG_FF_adffs_a0_a & (U_FCONFIG_aDCLK_areg0 # CREG_FF_adffs_a1_a) # !CREG_FF_adffs_a0_a & (!CREG_FF_adffs_a1_a & ao_CLK_D2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ADA8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => U_FCONFIG_aDCLK_areg0,
	datac => CREG_FF_adffs_a1_a,
	datad => ao_CLK_D2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1134);

U_FCONFIG_aSHIFT_EN_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_EN_areg0 = DFFE(U_FCONFIG_aiSHIFT_EN, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), , , S5920MR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	ena => S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_EN_areg0);

CLK40_D2_aI : apex20ke_lcell
-- Equation(s):
-- CLK40_D2 = DFFE(!CLK40_D2, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => CLK40_D2,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CLK40_D2);

Mux_a1154_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1154 = CREG_FF_adffs_a1_a & (CREG_FF_adffs_a0_a) # !CREG_FF_adffs_a1_a & (CREG_FF_adffs_a0_a & CREG_FF_adffs_a6_a # !CREG_FF_adffs_a0_a & (CLK40_D2))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E3E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => CREG_FF_adffs_a1_a,
	datac => CREG_FF_adffs_a0_a,
	datad => CLK40_D2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1154);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158 = a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109 # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a,
	datad => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92 = a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a # a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a # 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109 = !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a # 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a109);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, 
-- , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a0_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT);

CLK20_IN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20_IN,
	combout => CLK20_IN_acombout);

FPGA_BOOT_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a0_a = DFFE(U_PCICTRL_aData_FF_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a0_a);

U_FCONFIG_ai_a251_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a251 = U_FCONFIG_aiDCLK & !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a0_a_a0 # U_FCONFIG_aFPGA_REG_a0_a_a1) # !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a0_a_a0 # U_FCONFIG_aFPGA_REG_a0_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aFPGA_REG_a0_a_a0,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_aFPGA_REG_a0_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a251);

DSP_REG_FF_0_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a0_a = DFFE(U_PCICTRL_aData_FF_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a0_a);

PCI_DQ_MUX_a0_a_a19325_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19325 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_0_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_0_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19325);

TREG_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a1_a = DFFE(U_PCICTRL_aData_FF_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a1_a);

PCI_DQ_MUX_a2_a_a19335_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19335 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3210",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => CREG_FF_adffs_a2_a,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19335);

FPGA_BOOT_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a2_a = DFFE(U_PCICTRL_aData_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a2_a);

TREG_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a2_a = DFFE(U_PCICTRL_aData_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aData_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a2_a);

PCI_DQ_MUX_a2_a_a19336_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19336 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a2_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => FPGA_BOOT_FF_adffs_a2_a,
	datad => TREG_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19336);

PCI_DQ_MUX_a2_a_a19337_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19337 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (PCI_DQ_MUX_a2_a_a19336) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_FCONFIG_aSHIFT_EN_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_FCONFIG_aSHIFT_EN_areg0,
	datad => PCI_DQ_MUX_a2_a_a19336,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19337);

PCI_DQ_MUX_a2_a_a19338_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19338 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (U_PCICTRL_aPCI_ADR_a3_a_areg0 # PCI_DQ_MUX_a2_a_a19335) # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a2_a_a19337 & !U_PCICTRL_aPCI_ADR_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CEC2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a2_a_a19337,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => PCI_DQ_MUX_a2_a_a19335,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19338);

DSP_REG_FF_0_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a3_a = DFFE(U_PCICTRL_aData_FF_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a3_a);

PCI_DQ_MUX_a4_a_a19349_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19349 = DSP_REG_FF_1_adffs_a4_a & !U_PCICTRL_aPCI_ADR_a0_a_areg0 # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5D5D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => DSP_REG_FF_1_adffs_a4_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19349);

DSP_REG_FF_0_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a4_a = DFFE(U_PCICTRL_aData_FF_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a4_a);

PCI_DQ_MUX_a4_a_a19353_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19353 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # DSP_REG_FF_0_adffs_a4_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_0_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19353);

DSP_REG_FF_0_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a5_a = DFFE(U_PCICTRL_aData_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a5_a);

PCI_DQ_MUX_a5_a_a19360_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19360 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # DSP_REG_FF_0_adffs_a5_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => DSP_REG_FF_0_adffs_a5_a,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19360);

PCI_DQ_MUX_a6_a_a19362_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19362 = DSP_REG_FF_1_adffs_a6_a & !U_PCICTRL_aPCI_ADR_a0_a_areg0 # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a6_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19362);

PCI_DQ_MUX_a6_a_a19363_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19363 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & (CREG_FF_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDF8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => CREG_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19363);

TREG_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a7_a = DFFE(U_PCICTRL_aData_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a7_a);

PCI_DQ_MUX_a7_a_a19370_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19370 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (TREG_FF_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => TREG_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19370);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a8) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a $ (!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT)) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a & (!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	datac => U_FFCNT_aadd_35_rtl_0_a8,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT);

PCI_DQ_MUX_a8_a_a19375_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19375 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19375);

FPGA_BOOT_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a8_a = DFFE(U_PCICTRL_aData_FF_adffs_a8_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a8_a);

TREG_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a8_a = DFFE(U_PCICTRL_aData_FF_adffs_a8_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a8_a);

PCI_DQ_MUX_a8_a_a19377_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19377 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & (FPGA_BOOT_FF_adffs_a8_a) # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & TREG_FF_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C840",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => TREG_FF_adffs_a8_a,
	datad => FPGA_BOOT_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19377);

DSP_REG_FF_0_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a9_a = DFFE(U_PCICTRL_aData_FF_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a9_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a9_a);

PCI_DQ_MUX_a9_a_a19382_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19382 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (U_PCICTRL_aPCI_ADR_a3_a_areg0 & DSP_REG_FF_0_adffs_a9_a # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8A80",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => DSP_REG_FF_0_adffs_a9_a,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19382);

TREG_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a10_a = DFFE(U_PCICTRL_aData_FF_adffs_a10_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a10_a);

FPGA_BOOT_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a10_a = DFFE(U_PCICTRL_aData_FF_adffs_a10_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a10_a);

PCI_DQ_MUX_a11_a_a19393_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19393 = !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a11_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => DSP_REG_FF_1_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19393);

FPGA_BOOT_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a11_a = DFFE(U_PCICTRL_aData_FF_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a11_a);

TREG_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a11_a = DFFE(U_PCICTRL_aData_FF_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a11_a);

PCI_DQ_MUX_a11_a_a19394_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19394 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a11_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8A80",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => FPGA_BOOT_FF_adffs_a11_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => TREG_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19394);

PCI_DQ_MUX_a11_a_a19395_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19395 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a11_a_a19393 # U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a11_a_a19394 & !U_PCICTRL_aPCI_ADR_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0AC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a11_a_a19393,
	datab => PCI_DQ_MUX_a11_a_a19394,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19395);

DSP_REG_FF_0_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a12_a = DFFE(U_PCICTRL_aData_FF_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a12_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a12_a);

PCI_DQ_MUX_a12_a_a19402_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19402 = DSP_REG_FF_0_adffs_a12_a # U_PCICTRL_aPCI_ADR_a2_a_areg0 # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => DSP_REG_FF_0_adffs_a12_a,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19402);

FPGA_BOOT_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a14_a = DFFE(U_PCICTRL_aData_FF_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a14_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a14_a);

PCI_DQ_MUX_a14_a_a19408_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19408 = FPGA_BOOT_FF_adffs_a14_a & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0202",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_BOOT_FF_adffs_a14_a,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19408);

FPGA_BOOT_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a15_a = DFFE(U_PCICTRL_aData_FF_adffs_a15_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a15_a);

PCI_DQ_MUX_a15_a_a19411_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19411 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & FPGA_BOOT_FF_adffs_a15_a & U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => FPGA_BOOT_FF_adffs_a15_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19411);

TREG_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a15_a = DFFE(U_PCICTRL_aData_FF_adffs_a15_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a15_a);

PCI_DQ_MUX_a15_a_a19412_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19412 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (TREG_FF_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	datad => TREG_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19412);

PCI_DQ_MUX_a15_a_a19413_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19413 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (PCI_DQ_MUX_a15_a_a19412) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & !a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F404",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => PCI_DQ_MUX_a15_a_a19412,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19413);

PCI_DQ_MUX_a15_a_a19414_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19414 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_aPCI_ADR_a2_a_areg0 # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & PCI_DQ_MUX_a15_a_a19411 # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (PCI_DQ_MUX_a15_a_a19413))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D9C8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => PCI_DQ_MUX_a15_a_a19411,
	datad => PCI_DQ_MUX_a15_a_a19413,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19414);

DSP_TREG_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a3_a = DFFE(DSP_D_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a3_a);

DSP_TREG_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a4_a = DFFE(DSP_D_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a4_a);

DSP_TREG_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a7_a = DFFE(DSP_D_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a7_a);

U_PCICTRL_aPCI_STATE_a8_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_STATE_a8 = DFFE(U_PCICTRL_aPCI_STATE_a8 # !U_PCICTRL_areduce_nor_18_a158 & !U_PCICTRL_areduce_nor_18_a153, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ABAB",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_STATE_a8,
	datab => U_PCICTRL_areduce_nor_18_a158,
	datac => U_PCICTRL_areduce_nor_18_a153,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_STATE_a8);

U_PCICTRL_areduce_nor_18_a65_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a65 = U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a # 
-- !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a,
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a,
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a65);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a $ 
-- (!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a & (!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a6_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a5_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a5_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a $ (U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT # !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT);

U_PCICTRL_aPTNUM_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTNUM(1),
	combout => PTNUM_a1_a_acombout);

U_DPRAM_adata_mux_a0_a_a47_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a0_a_a47 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a0_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a0_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a0_a_a47);

U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a = DFFE(U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a $ (U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	cin => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	cout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, 
-- , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a,
	cout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_ai_a277_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_ai_a277 = !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a # !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a # !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a # 
-- !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a,
	datab => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	datac => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	datad => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_ai_a277);

U_DPRAM_adata_mux_a1_a_a44_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a1_a_a44 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a1_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a1_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a1_a_a44);

U_DPRAM_adata_mux_a2_a_a41_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a2_a_a41 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a2_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a2_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a2_a_a41);

U_DPRAM_adata_mux_a3_a_a38_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a3_a_a38 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_d_adffs_a3_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_d_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_astate_a15,
	datac => U_DPRAM_areg_ff_a_d_adffs_a3_a,
	datad => U_DPRAM_areg_ff_b_d_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a3_a_a38);

U_DPRAM_adata_mux_a4_a_a35_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a4_a_a35 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a4_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a4_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a4_a_a35);

U_DPRAM_adata_mux_a5_a_a32_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a5_a_a32 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a5_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a5_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a5_a_a32);

U_DPRAM_adata_mux_a6_a_a29_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a6_a_a29 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a6_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_astate_a15,
	datac => U_DPRAM_areg_ff_b_d_adffs_a6_a,
	datad => U_DPRAM_areg_ff_a_d_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a6_a_a29);

U_DPRAM_adata_mux_a7_a_a26_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a7_a_a26 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_d_adffs_a7_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_d_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CACA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_d_adffs_a7_a,
	datab => U_DPRAM_areg_ff_b_d_adffs_a7_a,
	datac => U_DPRAM_astate_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a7_a_a26);

U_DPRAM_adata_mux_a8_a_a23_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a8_a_a23 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a8_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a8_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a8_a_a23);

U_DPRAM_adata_mux_a9_a_a20_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a9_a_a20 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a9_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a9_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a9_a_a20);

U_DPRAM_adata_mux_a10_a_a17_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a10_a_a17 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a10_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_astate_a15,
	datac => U_DPRAM_areg_ff_b_d_adffs_a10_a,
	datad => U_DPRAM_areg_ff_a_d_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a10_a_a17);

U_DPRAM_adata_mux_a11_a_a14_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a11_a_a14 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a11_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_b_d_adffs_a11_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a11_a_a14);

U_DPRAM_adata_mux_a12_a_a11_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a12_a_a11 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_d_adffs_a12_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_d_adffs_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E2E2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_d_adffs_a12_a,
	datab => U_DPRAM_astate_a15,
	datac => U_DPRAM_areg_ff_b_d_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a12_a_a11);

U_DPRAM_adata_mux_a13_a_a8_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a13_a_a8 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a13_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a13_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a13_a_a8);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a13) # (U_FFCNT_ai_a95 & U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT # !U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	datac => U_FFCNT_aadd_35_rtl_0_a13,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT);

U_DPRAM_adata_mux_a14_a_a5_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a14_a_a5 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_d_adffs_a14_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_d_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_d_adffs_a14_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_d_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a14_a_a5);

U_DPRAM_adata_mux_a15_a_a2_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_adata_mux_a15_a_a2 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_d_adffs_a15_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_d_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_a_d_adffs_a15_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_d_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_adata_mux_a15_a_a2);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a16_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a19_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a21_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a_aCOUT $ U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a21_a);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a18_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a18_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a $ !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a & !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a20_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a20_a_aCOUT);

U_FCONFIG_ai_a260_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a260 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a2_a_a5 # U_FCONFIG_aFPGA_REG_a2_a_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a2_a_a5,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a2_a_a4,
	datad => U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a260);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a $ (!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a & (!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a_aCOUT);

U_DPRAM_areg_ff_a_a_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a6_a = DFFE(U_PCICTRL_aPCI_DPRA_a6_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a6_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a6_a);

U_DPRAM_areg_ff_a_d_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a0_a = DFFE(PCI_DQ_a0_a_a15, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a0_a_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a0_a);

U_DPRAM_areg_ff_b_d_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a0_a = DFFE(DSP_D_a0_a_a15, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a0_a_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a0_a);

a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a $ 
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	cin => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	cout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT $ 
-- !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a,
	cin => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a);

a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a $ 
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	cin => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	cout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)
-- a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a & !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	cin => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	cout => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT);

U_DPRAM_areg_ff_a_d_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a1_a = DFFE(PCI_DQ_a1_a_a14, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a1_a_a14,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a1_a);

U_DPRAM_areg_ff_b_d_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a1_a = DFFE(DSP_D_a1_a_a14, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a1_a_a14,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a1_a);

U_DPRAM_areg_ff_a_d_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a2_a = DFFE(PCI_DQ_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a2_a);

U_DPRAM_areg_ff_b_d_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a2_a = DFFE(DSP_D_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a2_a);

U_DPRAM_areg_ff_b_d_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a3_a = DFFE(DSP_D_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a3_a);

U_DPRAM_areg_ff_a_d_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a3_a = DFFE(PCI_DQ_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a3_a);

U_DPRAM_areg_ff_a_d_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a4_a = DFFE(PCI_DQ_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a4_a);

U_DPRAM_areg_ff_b_d_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a4_a = DFFE(DSP_D_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a4_a);

U_DPRAM_areg_ff_b_d_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a5_a = DFFE(DSP_D_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a5_a);

U_DPRAM_areg_ff_a_d_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a5_a = DFFE(PCI_DQ_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a5_a);

U_DPRAM_areg_ff_a_d_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a6_a = DFFE(PCI_DQ_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a6_a);

U_DPRAM_areg_ff_b_d_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a6_a = DFFE(DSP_D_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a6_a);

U_DPRAM_areg_ff_a_d_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a7_a = DFFE(PCI_DQ_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a7_a);

U_DPRAM_areg_ff_b_d_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a7_a = DFFE(DSP_D_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a7_a);

U_DPRAM_areg_ff_a_d_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a8_a = DFFE(PCI_DQ_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a8_a);

U_DPRAM_areg_ff_b_d_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a8_a = DFFE(DSP_D_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a8_a);

U_DPRAM_areg_ff_a_d_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a9_a = DFFE(PCI_DQ_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a9_a);

U_DPRAM_areg_ff_b_d_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a9_a = DFFE(DSP_D_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a9_a);

U_DPRAM_areg_ff_a_d_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a10_a = DFFE(PCI_DQ_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a10_a);

U_DPRAM_areg_ff_b_d_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a10_a = DFFE(DSP_D_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a10_a);

U_DPRAM_areg_ff_b_d_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a11_a = DFFE(DSP_D_a11_a_a4, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a11_a_a4,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a11_a);

U_DPRAM_areg_ff_a_d_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a11_a = DFFE(PCI_DQ_a11_a_a4, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a11_a_a4,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a11_a);

U_DPRAM_areg_ff_a_d_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a12_a = DFFE(PCI_DQ_a12_a_a3, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a12_a_a3,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a12_a);

U_DPRAM_areg_ff_b_d_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a12_a = DFFE(DSP_D_a12_a_a3, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a12_a_a3,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a12_a);

FPGA_BOOT_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a13_a = DFFE(U_PCICTRL_aData_FF_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a13_a);

PCI_DQ_MUX_a13_a_a19418_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a13_a_a19418 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & FPGA_BOOT_FF_adffs_a13_a & U_PCICTRL_aPCI_ADR_a0_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => FPGA_BOOT_FF_adffs_a13_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a13_a_a19418);

U_DPRAM_areg_ff_a_d_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a13_a = DFFE(PCI_DQ_a13_a_a2, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a13_a_a2,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a13_a);

U_DPRAM_areg_ff_b_d_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a13_a = DFFE(DSP_D_a13_a_a2, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a13_a_a2,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a13_a);

U_DPRAM_areg_ff_a_d_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a14_a = DFFE(PCI_DQ_a14_a_a1, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a14_a_a1,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a14_a);

U_DPRAM_areg_ff_b_d_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a14_a = DFFE(DSP_D_a14_a_a1, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a14_a_a1,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a14_a);

U_DPRAM_areg_ff_a_d_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_d_adffs_a15_a = DFFE(PCI_DQ_a15_a_a0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a15_a_a0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_d_adffs_a15_a);

U_DPRAM_areg_ff_b_d_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_d_adffs_a15_a = DFFE(DSP_D_a15_a_a0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a15_a_a0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_d_adffs_a15_a);

U_FCONFIG_aFPGA_REG_a2_a_a5_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a2_a_a5 = DFFE(U_FCONFIG_ai_a271 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a271 & U_FCONFIG_ai_a270 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CF8A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a271,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_ai_a270,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a2_a_a5);

U_FCONFIG_aFPGA_REG_a2_a_a4_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a2_a_a4 = DFFE(!U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210 & CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_ai_a3210,
	datac => CREG_FF_adffs_a6_a,
	datad => FPGA_BOOT_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a2_a_a4);

U_PCICTRL_adata_en_a62_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_a62 = OPTADR_acombout & OPTATN_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OPTADR_acombout,
	datad => OPTATN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_adata_en_a62);

U_FCONFIG_ai_a270_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a270 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a3_a_a7 # U_FCONFIG_aFPGA_REG_a3_a_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a3_a_a7,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a3_a_a6,
	datad => U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a270);

U_FCONFIG_ai_a271_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a271 = U_FCONFIG_aFPGA_REG_a2_a_a5 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK) # !U_FCONFIG_aFPGA_REG_a2_a_a5 & U_FCONFIG_aFPGA_REG_a2_a_a4 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "32FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a2_a_a5,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a2_a_a4,
	datad => U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a271);

U_FCONFIG_aFPGA_REG_a3_a_a6_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a3_a_a6 = DFFE(U_FCONFIG_ai_a3210 & CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a3_a & !U_FCONFIG_aiSHIFT_EN, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3210,
	datab => CREG_FF_adffs_a6_a,
	datac => FPGA_BOOT_FF_adffs_a3_a,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a3_a_a6);

U_FCONFIG_aFPGA_REG_a3_a_a7_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a3_a_a7 = DFFE(U_FCONFIG_ai_a281 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a281 & U_FCONFIG_ai_a280 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CF8A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a281,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_ai_a280,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a3_a_a7);

U_FCONFIG_ai_a280_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a280 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a4_a_a8 # U_FCONFIG_aFPGA_REG_a4_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a4_a_a8,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a4_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a280);

U_FCONFIG_ai_a281_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a281 = U_FCONFIG_aFPGA_REG_a3_a_a6 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN) # !U_FCONFIG_aFPGA_REG_a3_a_a6 & U_FCONFIG_aFPGA_REG_a3_a_a7 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a3_a_a6,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a3_a_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a281);

U_FCONFIG_aFPGA_REG_a4_a_a9_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a4_a_a9 = DFFE(U_FCONFIG_ai_a291 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a291 & U_FCONFIG_ai_a290 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EE0E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a291,
	datab => U_FCONFIG_ai_a290,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a4_a_a9);

U_FCONFIG_aFPGA_REG_a4_a_a8_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a4_a_a8 = DFFE(!U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a4_a & U_FCONFIG_ai_a3210, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => CREG_FF_adffs_a6_a,
	datac => FPGA_BOOT_FF_adffs_a4_a,
	datad => U_FCONFIG_ai_a3210,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a4_a_a8);

U_FCONFIG_ai_a291_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a291 = U_FCONFIG_aFPGA_REG_a4_a_a8 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN) # !U_FCONFIG_aFPGA_REG_a4_a_a8 & U_FCONFIG_aFPGA_REG_a4_a_a9 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a4_a_a8,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a4_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a291);

U_FCONFIG_ai_a290_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a290 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a5_a_a10 # U_FCONFIG_aFPGA_REG_a5_a_a11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a5_a_a10,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a5_a_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a290);

U_FCONFIG_aFPGA_REG_a5_a_a10_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a5_a_a10 = DFFE(U_FCONFIG_ai_a3210 & !U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3210,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => CREG_FF_adffs_a6_a,
	datad => FPGA_BOOT_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a5_a_a10);

U_FCONFIG_aFPGA_REG_a5_a_a11_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a5_a_a11 = DFFE(U_FCONFIG_ai_a301 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a301 & U_FCONFIG_ai_a300 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EE0E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a301,
	datab => U_FCONFIG_ai_a300,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a5_a_a11);

U_FCONFIG_ai_a301_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a301 = U_FCONFIG_aFPGA_REG_a5_a_a10 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN) # !U_FCONFIG_aFPGA_REG_a5_a_a10 & U_FCONFIG_aFPGA_REG_a5_a_a11 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a5_a_a10,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a5_a_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a301);

U_FCONFIG_ai_a300_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a300 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a6_a_a13 # U_FCONFIG_aFPGA_REG_a6_a_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a6_a_a13,
	datad => U_FCONFIG_aFPGA_REG_a6_a_a12,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a300);

U_FCONFIG_aFPGA_REG_a6_a_a13_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a6_a_a13 = DFFE(U_FCONFIG_ai_a311 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a311 & U_FCONFIG_ai_a310 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3A2",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a311,
	datab => U_FCONFIG_ai_a3206,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a310,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a6_a_a13);

U_FCONFIG_aFPGA_REG_a6_a_a12_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a6_a_a12 = DFFE(!U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a6_a_a12);

U_FCONFIG_ai_a310_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a310 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a7_a_a14 # U_FCONFIG_aFPGA_REG_a7_a_a15)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a7_a_a14,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a7_a_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a310);

U_FCONFIG_ai_a311_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a311 = U_FCONFIG_aFPGA_REG_a6_a_a13 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN) # !U_FCONFIG_aFPGA_REG_a6_a_a13 & U_FCONFIG_aFPGA_REG_a6_a_a12 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a6_a_a13,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a6_a_a12,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a311);

U_FCONFIG_aFPGA_REG_a7_a_a14_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a7_a_a14 = DFFE(CREG_FF_adffs_a6_a & !U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a7_a_a14);

U_FCONFIG_aFPGA_REG_a7_a_a15_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a7_a_a15 = DFFE(U_FCONFIG_ai_a320 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a320 & U_FCONFIG_ai_a321 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E0EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a320,
	datab => U_FCONFIG_ai_a321,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a3206,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a7_a_a15);

U_FCONFIG_ai_a320_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a320 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a8_a_a17 # U_FCONFIG_aFPGA_REG_a8_a_a16)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a8_a_a17,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a8_a_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a320);

U_FCONFIG_ai_a321_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a321 = U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a7_a_a14 # U_FCONFIG_aFPGA_REG_a7_a_a15) # !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a7_a_a14 # U_FCONFIG_aFPGA_REG_a7_a_a15)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a7_a_a14,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a7_a_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a321);

U_FCONFIG_aFPGA_REG_a8_a_a16_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a8_a_a16 = DFFE(CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a8_a & U_FCONFIG_ai_a3210 & !U_FCONFIG_aiSHIFT_EN, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => FPGA_BOOT_FF_adffs_a8_a,
	datac => U_FCONFIG_ai_a3210,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a8_a_a16);

U_FCONFIG_aFPGA_REG_a8_a_a17_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a8_a_a17 = DFFE(U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_ai_a330 # U_FCONFIG_ai_a331) # !U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_ai_a3206 & (U_FCONFIG_ai_a330 # U_FCONFIG_ai_a331), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BBB0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_ai_a3206,
	datac => U_FCONFIG_ai_a330,
	datad => U_FCONFIG_ai_a331,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a8_a_a17);

U_FCONFIG_ai_a330_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a330 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a9_a_a19 # U_FCONFIG_aFPGA_REG_a9_a_a18)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a9_a_a19,
	datad => U_FCONFIG_aFPGA_REG_a9_a_a18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a330);

U_FCONFIG_ai_a331_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a331 = U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a8_a_a17 # U_FCONFIG_aFPGA_REG_a8_a_a16) # !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a8_a_a17 # U_FCONFIG_aFPGA_REG_a8_a_a16)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a8_a_a17,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a8_a_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a331);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a0_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a);

U_FCONFIG_aFPGA_REG_a9_a_a19_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a9_a_a19 = DFFE(U_FCONFIG_ai_a340 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a340 & U_FCONFIG_ai_a341 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EE0E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a340,
	datab => U_FCONFIG_ai_a341,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a9_a_a19);

U_FCONFIG_aFPGA_REG_a9_a_a18_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a9_a_a18 = DFFE(CREG_FF_adffs_a6_a & !U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a9_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a9_a_a18);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_0_adffs_a15_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a,
	datad => DSP_REG_FF_0_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a15_a);

U_FCONFIG_ai_a340_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a340 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a10_a_a20 # U_FCONFIG_aFPGA_REG_a10_a_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a10_a_a20,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_aFPGA_REG_a10_a_a21,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a340);

U_FCONFIG_ai_a341_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a341 = U_FCONFIG_aiDCLK & !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a9_a_a19 # U_FCONFIG_aFPGA_REG_a9_a_a18) # !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a9_a_a19 # U_FCONFIG_aFPGA_REG_a9_a_a18)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a9_a_a19,
	datad => U_FCONFIG_aFPGA_REG_a9_a_a18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a341);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a14_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a14_a);

U_FCONFIG_aFPGA_REG_a10_a_a20_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a10_a_a20 = DFFE(CREG_FF_adffs_a6_a & !U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a10_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a10_a_a20);

U_FCONFIG_aFPGA_REG_a10_a_a21_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a10_a_a21 = DFFE(U_FCONFIG_ai_a3206 & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_ai_a350 # U_FCONFIG_ai_a351) # !U_FCONFIG_ai_a3206 & (U_FCONFIG_ai_a350 # U_FCONFIG_ai_a351), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, 
-- , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5C4",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3206,
	datab => U_FCONFIG_ai_a350,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a351,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a10_a_a21);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a13_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a13_a);

U_FCONFIG_ai_a351_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a351 = U_FCONFIG_aFPGA_REG_a10_a_a20 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK) # !U_FCONFIG_aFPGA_REG_a10_a_a20 & U_FCONFIG_aFPGA_REG_a10_a_a21 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F2A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a10_a_a20,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_aFPGA_REG_a10_a_a21,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a351);

U_FCONFIG_ai_a350_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a350 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a11_a_a23 # U_FCONFIG_aFPGA_REG_a11_a_a22)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aFPGA_REG_a11_a_a23,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_aFPGA_REG_a11_a_a22,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a350);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a12_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a12_a);

U_FCONFIG_aFPGA_REG_a11_a_a22_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a11_a_a22 = DFFE(U_FCONFIG_ai_a3210 & !U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3210,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => CREG_FF_adffs_a6_a,
	datad => FPGA_BOOT_FF_adffs_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a11_a_a22);

U_FCONFIG_aFPGA_REG_a11_a_a23_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a11_a_a23 = DFFE(U_FCONFIG_ai_a3206 & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_ai_a361 # U_FCONFIG_ai_a360) # !U_FCONFIG_ai_a3206 & (U_FCONFIG_ai_a361 # U_FCONFIG_ai_a360), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, 
-- , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5C4",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3206,
	datab => U_FCONFIG_ai_a361,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a360,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a11_a_a23);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a11_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a11_a);

U_FCONFIG_ai_a361_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a361 = U_FCONFIG_aiDCLK & !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a11_a_a23 # U_FCONFIG_aFPGA_REG_a11_a_a22) # !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a11_a_a23 # U_FCONFIG_aFPGA_REG_a11_a_a22)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a11_a_a23,
	datad => U_FCONFIG_aFPGA_REG_a11_a_a22,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a361);

U_FCONFIG_ai_a360_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a360 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a12_a_a24 # U_FCONFIG_aFPGA_REG_a12_a_a25)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a12_a_a24,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a12_a_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a360);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_0_adffs_a10_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ACAC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a,
	datab => DSP_REG_FF_0_adffs_a10_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a10_a);

U_FCONFIG_aFPGA_REG_a12_a_a25_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a12_a_a25 = DFFE(U_FCONFIG_ai_a3206 & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_ai_a370 # U_FCONFIG_ai_a371) # !U_FCONFIG_ai_a3206 & (U_FCONFIG_ai_a370 # U_FCONFIG_ai_a371), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, 
-- , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5C4",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3206,
	datab => U_FCONFIG_ai_a370,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a371,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a12_a_a25);

U_FCONFIG_aFPGA_REG_a12_a_a24_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a12_a_a24 = DFFE(U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a12_a & CREG_FF_adffs_a6_a & !U_FCONFIG_aiSHIFT_EN, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a3210,
	datab => FPGA_BOOT_FF_adffs_a12_a,
	datac => CREG_FF_adffs_a6_a,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a12_a_a24);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a9_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a9_a);

U_FCONFIG_ai_a371_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a371 = U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a12_a_a24 # U_FCONFIG_aFPGA_REG_a12_a_a25) # !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a12_a_a24 # U_FCONFIG_aFPGA_REG_a12_a_a25)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a12_a_a24,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a12_a_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a371);

U_FCONFIG_ai_a370_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a370 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a13_a_a27 # U_FCONFIG_aFPGA_REG_a13_a_a26)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aFPGA_REG_a13_a_a27,
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aFPGA_REG_a13_a_a26,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a370);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a8_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a8_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a8_a);

U_FCONFIG_aFPGA_REG_a13_a_a26_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a13_a_a26 = DFFE(!U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a13_a_a26);

U_FCONFIG_aFPGA_REG_a13_a_a27_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a13_a_a27 = DFFE(U_FCONFIG_ai_a381 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a381 & U_FCONFIG_ai_a380 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E0EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a381,
	datab => U_FCONFIG_ai_a380,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a3206,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a13_a_a27);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a7_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a7_a);

U_FCONFIG_ai_a380_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a380 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a14_a_a29 # U_FCONFIG_aFPGA_REG_a14_a_a28)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a14_a_a29,
	datad => U_FCONFIG_aFPGA_REG_a14_a_a28,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a380);

U_FCONFIG_ai_a381_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a381 = U_FCONFIG_aFPGA_REG_a13_a_a26 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK) # !U_FCONFIG_aFPGA_REG_a13_a_a26 & U_FCONFIG_aFPGA_REG_a13_a_a27 & (!U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_aiDCLK)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "32FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a13_a_a26,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a13_a_a27,
	datad => U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a381);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a6_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a6_a);

U_FCONFIG_aFPGA_REG_a14_a_a29_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a14_a_a29 = DFFE(U_FCONFIG_ai_a390 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a390 & U_FCONFIG_ai_a391 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CF8A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a390,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_ai_a391,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a14_a_a29);

U_FCONFIG_aFPGA_REG_a14_a_a28_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a14_a_a28 = DFFE(!U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a14_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a14_a_a28);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a5_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a5_a);

U_FCONFIG_ai_a391_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a391 = U_FCONFIG_aFPGA_REG_a14_a_a29 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN) # !U_FCONFIG_aFPGA_REG_a14_a_a29 & U_FCONFIG_aFPGA_REG_a14_a_a28 & (!U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "32FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aFPGA_REG_a14_a_a29,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a14_a_a28,
	datad => U_FCONFIG_aiDCLK,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a391);

U_FCONFIG_ai_a390_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a390 = U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a15_a_a30 # U_FCONFIG_aFPGA_REG_a15_a_a31)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_aFPGA_REG_a15_a_a30,
	datad => U_FCONFIG_aFPGA_REG_a15_a_a31,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a390);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a4_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a4_a);

U_FCONFIG_aFPGA_REG_a15_a_a30_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a15_a_a30 = DFFE(!U_FCONFIG_aiSHIFT_EN & CREG_FF_adffs_a6_a & U_FCONFIG_ai_a3210 & FPGA_BOOT_FF_adffs_a15_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiSHIFT_EN,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_ai_a3210,
	datad => FPGA_BOOT_FF_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a15_a_a30);

U_FCONFIG_aFPGA_REG_a15_a_a31_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a15_a_a31 = DFFE(U_FCONFIG_ai_a154 & (U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_aiDCLK # !U_FCONFIG_aiSHIFT_EN & (!U_FCONFIG_ai_a3206)), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "220A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a154,
	datab => U_FCONFIG_aiDCLK,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a15_a_a31);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a3_a,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a3_a);

U_FCONFIG_ai_a154_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a154 = U_FCONFIG_aFPGA_REG_a15_a_a31 # U_FCONFIG_aFPGA_REG_a15_a_a30

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCFC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aFPGA_REG_a15_a_a31,
	datac => U_FCONFIG_aFPGA_REG_a15_a_a30,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a154);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a2_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a2_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_0_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_0_adffs_a1_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a1_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a = DFFE(!a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_0_adffs_a0_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => DSP_REG_FF_0_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a0_a);

PCI_DQ_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a0_a_a915,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(0),
	combout => PCI_DQ_a0_a_a15);

PCI_DQ_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a1_a_a916,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(1),
	combout => PCI_DQ_a1_a_a14);

PCI_DQ_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a2_a_a917,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(2),
	combout => PCI_DQ_a2_a_a13);

PCI_DQ_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a3_a_a918,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(3),
	combout => PCI_DQ_a3_a_a12);

PCI_DQ_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a4_a_a920,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(4),
	combout => PCI_DQ_a4_a_a11);

PCI_DQ_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a5_a_a922,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(5),
	combout => PCI_DQ_a5_a_a10);

PCI_DQ_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a6_a_a924,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(6),
	combout => PCI_DQ_a6_a_a9);

PCI_DQ_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a7_a_a925,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(7),
	combout => PCI_DQ_a7_a_a8);

PCI_DQ_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a8_a_a927,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(8),
	combout => PCI_DQ_a8_a_a7);

PCI_DQ_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a9_a_a929,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(9),
	combout => PCI_DQ_a9_a_a6);

PCI_DQ_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a10_a_a931,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(10),
	combout => PCI_DQ_a10_a_a5);

PCI_DQ_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a11_a_a933,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(11),
	combout => PCI_DQ_a11_a_a4);

PCI_DQ_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a12_a_a935,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(12),
	combout => PCI_DQ_a12_a_a3);

PCI_DQ_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a13_a_a937,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(13),
	combout => PCI_DQ_a13_a_a2);

PCI_DQ_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a14_a_a938,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(14),
	combout => PCI_DQ_a14_a_a1);

PCI_DQ_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DQ_MOUT_a15_a_a939,
	oe => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => PCI_DQ(15),
	combout => PCI_DQ_a15_a_a0);

DSP_D_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a0_a_a2437,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(0),
	combout => DSP_D_a0_a_a15);

DSP_D_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a1_a_a2438,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(1),
	combout => DSP_D_a1_a_a14);

DSP_D_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a2_a_a2440,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(2),
	combout => DSP_D_a2_a_a13);

DSP_D_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a3_a_a2442,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(3),
	combout => DSP_D_a3_a_a12);

DSP_D_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a4_a_a2444,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(4),
	combout => DSP_D_a4_a_a11);

DSP_D_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a5_a_a2445,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(5),
	combout => DSP_D_a5_a_a10);

DSP_D_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a6_a_a2446,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(6),
	combout => DSP_D_a6_a_a9);

DSP_D_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a7_a_a2448,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(7),
	combout => DSP_D_a7_a_a8);

DSP_D_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a8_a_a2449,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(8),
	combout => DSP_D_a8_a_a7);

DSP_D_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a9_a_a2451,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(9),
	combout => DSP_D_a9_a_a6);

DSP_D_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a10_a_a2453,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(10),
	combout => DSP_D_a10_a_a5);

DSP_D_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a11_a_a2454,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(11),
	combout => DSP_D_a11_a_a4);

DSP_D_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a12_a_a2455,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(12),
	combout => DSP_D_a12_a_a3);

DSP_D_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a13_a_a2457,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(13),
	combout => DSP_D_a13_a_a2);

DSP_D_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a14_a_a2458,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(14),
	combout => DSP_D_a14_a_a1);

DSP_D_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_Mout_a15_a_a2460,
	oe => a_aST_GEN_aU_DSPCTRL_ai_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => DSP_D(15),
	combout => DSP_D_a15_a_a0);

SB_INTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0,
	oe => CREG_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => SB_INTR,
	combout => SB_INTR_a1);

SB_SDIN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => a_aST_GEN_aU_DSPBOOT_aDSP_D0_areg0,
	oe => CREG_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => SB_SDIN,
	combout => SB_SDIN_a1);

OFSIN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	oe => CREG_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => OFSIN,
	combout => OFSIN_a1);

OSCLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "bidir",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	oe => CREG_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	ena => VCC,
	padio => OSCLK,
	combout => OSCLK_a1);

clk_pll1_aaltclklock_component_apll : apex20ke_pll
-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	effective_phase_shift => -678,
	effective_clk0_delay => 48375,
	effective_clk1_delay => 31708,
	input_frequency => 50000,
	clk0_multiply_by => 1,
	clk1_multiply_by => 3,
	clk0_divide_by => 1,
	clk1_divide_by => 2,
	lock_low => 5,
	lock_high => 5,
	invalid_lock_multiplier => 5,
	valid_lock_multiplier => 5,
	phase_shift => 0,
	simulation_type => "timing")
-- pragma translate_on
PORT MAP (
	clk => CLK20_IN_acombout,
	clk0 => clk_pll1_aaltclklock_component_aoutclock0,
	clk1 => clk_pll1_aaltclklock_component_aoutclock1);

PCI_RST_IN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_RST_IN,
	combout => PCI_RST_IN_acombout);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a = DFFE(!U_PCICTRL_areduce_nor_18 & !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a $ U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a $ !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a & !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a $ U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT);

U_PCICTRL_areduce_nor_18_a109_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a109 = !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a # 
-- !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a3_a,
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a5_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a2_a,
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a109);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a7_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a10_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a $ !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a & !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a $ (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a & (!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT);

U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a = DFFE(!U_PCICTRL_areduce_nor_18 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a $ (U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )
-- U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a,
	cin => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	sclr => U_PCICTRL_areduce_nor_18,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a,
	cout => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_acounter_cell_a15_a_aCOUT);

U_PCICTRL_areduce_nor_18_a70_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a70 = U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a # 
-- !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a17_a,
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a15_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a14_a,
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a70);

U_PCICTRL_areduce_nor_18_a79_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a79 = U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a # U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a # U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a # 
-- !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a11_a,
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a13_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a12_a,
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a79);

U_PCICTRL_areduce_nor_18_a92_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a92 = U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a # U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a # 
-- !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a8_a,
	datab => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a6_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a7_a,
	datad => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a92);

U_PCICTRL_areduce_nor_18_a153_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a153 = U_PCICTRL_areduce_nor_18_a65 # U_PCICTRL_areduce_nor_18_a70 # U_PCICTRL_areduce_nor_18_a79 # U_PCICTRL_areduce_nor_18_a92

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_areduce_nor_18_a65,
	datab => U_PCICTRL_areduce_nor_18_a70,
	datac => U_PCICTRL_areduce_nor_18_a79,
	datad => U_PCICTRL_areduce_nor_18_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a153);

U_PCICTRL_areduce_nor_18_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18 = U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a & !U_PCICTRL_areduce_nor_18_a109 & U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a & !U_PCICTRL_areduce_nor_18_a153

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0020",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a,
	datab => U_PCICTRL_areduce_nor_18_a109,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a,
	datad => U_PCICTRL_areduce_nor_18_a153,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18);

U_PCICTRL_areduce_nor_18_a158_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_18_a158 = U_PCICTRL_areduce_nor_18_a109 # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a # !U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a1_a,
	datac => U_PCICTRL_aPCI_MR_CNT_rtl_6_awysi_counter_asload_path_a0_a,
	datad => U_PCICTRL_areduce_nor_18_a109,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_18_a158);

U_PCICTRL_aPCI_STATE_a9_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_STATE_a9 = DFFE(U_PCICTRL_areduce_nor_18_a158 & (U_PCICTRL_aPCI_STATE_a9) # !U_PCICTRL_areduce_nor_18_a158 & (U_PCICTRL_areduce_nor_18_a153 & (U_PCICTRL_aPCI_STATE_a9) # !U_PCICTRL_areduce_nor_18_a153 & !U_PCICTRL_aPCI_STATE_a8), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FD01",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_STATE_a8,
	datab => U_PCICTRL_areduce_nor_18_a158,
	datac => U_PCICTRL_areduce_nor_18_a153,
	datad => U_PCICTRL_aPCI_STATE_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_STATE_a9);

U_PCICTRL_aPCI_STATE_a10_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_STATE_a10 = DFFE(U_PCICTRL_aPCI_STATE_a10 # U_PCICTRL_aPCI_STATE_a9 & !U_PCICTRL_areduce_nor_18_a153 & !U_PCICTRL_areduce_nor_18_a158, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAAE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_STATE_a10,
	datab => U_PCICTRL_aPCI_STATE_a9,
	datac => U_PCICTRL_areduce_nor_18_a153,
	datad => U_PCICTRL_areduce_nor_18_a158,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_STATE_a10);

U_PCICTRL_aPCI_RST_OUT_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_RST_OUT_areg0 = DFFE(U_PCICTRL_aPCI_STATE_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), PCI_RST_IN_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aPCI_STATE_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_RST_OUT_areg0);

S5920MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_S5920MR,
	combout => S5920MR_acombout);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a1_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a2_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a3_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a4_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a4_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a7_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a8_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a9_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a & !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a $ 
-- (!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a & (!a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a21_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a $ 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65 = !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a # 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a18_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a20_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a19_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79 = a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a # a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a # 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a10_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a11_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a12_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70 = a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a # a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a # 
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a # !a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a14_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a15_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a16_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_MR_CNT_rtl_2_awysi_counter_asload_path_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70);

a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153 = a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92 # a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65 # a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79 # a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a92,
	datab => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a65,
	datac => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a79,
	datad => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a70,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153);

U_PCICTRL_aOPTADR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTADR,
	combout => OPTADR_acombout);

U_PCICTRL_aOPTATN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTATN,
	combout => OPTATN_acombout);

U_PCICTRL_aOPTBE_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(2),
	combout => OPTBE_a2_a_acombout);

U_PCICTRL_aOPTBE_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(1),
	combout => OPTBE_a1_a_acombout);

U_PCICTRL_aOPTBE_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(0),
	combout => OPTBE_a0_a_acombout);

U_PCICTRL_aPTNUM_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTNUM(0),
	combout => PTNUM_a0_a_acombout);

U_PCICTRL_aOPTBE_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(3),
	combout => OPTBE_a3_a_acombout);

U_PCICTRL_adata_en_a40_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_a40 = !PTNUM_a1_a_acombout & !OPTBE_a0_a_acombout & !PTNUM_a0_a_acombout & !OPTBE_a3_a_acombout
-- U_PCICTRL_adata_en_a71 = !PTNUM_a1_a_acombout & !OPTBE_a0_a_acombout & !PTNUM_a0_a_acombout & !OPTBE_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PTNUM_a1_a_acombout,
	datab => OPTBE_a0_a_acombout,
	datac => PTNUM_a0_a_acombout,
	datad => OPTBE_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_adata_en_a40,
	cascout => U_PCICTRL_adata_en_a71);

U_PCICTRL_adata_en_a69_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_a69 = (!OPTBE_a2_a_acombout & !OPTBE_a1_a_acombout) & CASCADE(U_PCICTRL_adata_en_a71)
-- U_PCICTRL_adata_en_a72 = (!OPTBE_a2_a_acombout & !OPTBE_a1_a_acombout) & CASCADE(U_PCICTRL_adata_en_a71)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OPTBE_a2_a_acombout,
	datad => OPTBE_a1_a_acombout,
	cascin => U_PCICTRL_adata_en_a71,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_adata_en_a69,
	cascout => U_PCICTRL_adata_en_a72);

U_PCICTRL_aPCI_ADR_a8_a_a1_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a8_a_a1 = !PCI_DQ_a15_a_a0 & !OPTADR_acombout & !OPTATN_acombout & U_PCICTRL_adata_en_a69

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_a15_a_a0,
	datab => OPTADR_acombout,
	datac => OPTATN_acombout,
	datad => U_PCICTRL_adata_en_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_aPCI_ADR_a8_a_a1);

U_PCICTRL_aPCI_ADR_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a1_a_areg0 = DFFE(PCI_DQ_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a1_a_areg0);

U_PCICTRL_aPCI_ADR_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a0_a_areg0 = DFFE(PCI_DQ_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a0_a_areg0);

U_PCICTRL_aPCI_ADR_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a3_a_areg0 = DFFE(PCI_DQ_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a3_a_areg0);

U_PCICTRL_aPCI_ADR_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a5_a_areg0 = DFFE(PCI_DQ_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a5_a_areg0);

U_PCICTRL_aPCI_ADR_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a6_a_areg0 = DFFE(PCI_DQ_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a6_a_areg0);

U_PCICTRL_aPCI_ADR_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a7_a_areg0 = DFFE(PCI_DQ_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a7_a_areg0);

U_PCICTRL_areduce_nor_66_a27_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_66_a27 = !U_PCICTRL_aPCI_ADR_a8_a_areg0 & !U_PCICTRL_aPCI_ADR_a5_a_areg0 & !U_PCICTRL_aPCI_ADR_a6_a_areg0 & !U_PCICTRL_aPCI_ADR_a7_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a8_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a5_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a6_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_66_a27);

U_PCICTRL_aODXFER_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ODXFER,
	combout => ODXFER_acombout);

U_PCICTRL_aPTWR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTWR,
	combout => PTWR_acombout);

U_PCICTRL_adata_en_a70_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_a70 = (!ODXFER_acombout & PTWR_acombout) & CASCADE(U_PCICTRL_adata_en_a72)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ODXFER_acombout,
	datad => PTWR_acombout,
	cascin => U_PCICTRL_adata_en_a72,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_adata_en_a70);

U_PCICTRL_adata_en_ao_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_ao = DFFE(U_PCICTRL_adata_en_a62 & (U_PCICTRL_adata_en_a70 # U_PCICTRL_adata_en_ao & !U_PCICTRL_adata_en_c20_vec_a0_a) # !U_PCICTRL_adata_en_a62 & U_PCICTRL_adata_en_ao & !U_PCICTRL_adata_en_c20_vec_a0_a, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AE0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_adata_en_a62,
	datab => U_PCICTRL_adata_en_ao,
	datac => U_PCICTRL_adata_en_c20_vec_a0_a,
	datad => U_PCICTRL_adata_en_a70,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_adata_en_ao);

U_PCICTRL_adata_en_c20_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_c20_vec_a0_a = DFFE(U_PCICTRL_adata_en_ao, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_adata_en_ao,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_adata_en_c20_vec_a0_a);

U_PCICTRL_adata_en_c20_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_c20_vec_a1_a = DFFE(U_PCICTRL_adata_en_c20_vec_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_adata_en_c20_vec_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_adata_en_c20_vec_a1_a);

U_PCICTRL_ai_a24_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a24 = !OPTADR_acombout & !OPTATN_acombout & U_PCICTRL_adata_en_a69

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => OPTADR_acombout,
	datac => OPTATN_acombout,
	datad => U_PCICTRL_adata_en_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a24);

U_PCICTRL_aDPR_ACC_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aDPR_ACC_areg0 = DFFE(PCI_DQ_a15_a_a0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a24)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a15_a_a0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a24,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aDPR_ACC_areg0);

U_PCICTRL_ai_a668_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a668 = U_PCICTRL_adata_en_c20_vec_a1_a # U_PCICTRL_aDPR_ACC_areg0 # !U_PCICTRL_adata_en_c20_vec_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_adata_en_c20_vec_a1_a,
	datac => U_PCICTRL_adata_en_c20_vec_a0_a,
	datad => U_PCICTRL_aDPR_ACC_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a668);

U_PCICTRL_ai_a559_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a559 = !U_PCICTRL_aPCI_ADR_a4_a_areg0 & U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_areduce_nor_66_a27 & !U_PCICTRL_ai_a668

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_areduce_nor_66_a27,
	datad => U_PCICTRL_ai_a668,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a559);

U_PCICTRL_aWE_DCD_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a9_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a2_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_ai_a559, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_ai_a559,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a9_a_areg0);

a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 = DFFE(U_PCICTRL_aWE_DCD_a9_a_areg0 # a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 & (a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158 # a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFE0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a158,
	datab => a_aST_GEN_aU_DSPBOOT_areduce_nor_47_a153,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	datad => U_PCICTRL_aWE_DCD_a9_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0);

OMR_a2_I : apex20ke_lcell
-- Equation(s):
-- OMR_a2 = a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 # !S5920MR_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	datad => S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => OMR_a2);

U_PCICTRL_adata_en_a4_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_adata_en_a4 = OPTATN_acombout & OPTADR_acombout & U_PCICTRL_adata_en_a70

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => OPTATN_acombout,
	datac => OPTADR_acombout,
	datad => U_PCICTRL_adata_en_a70,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_adata_en_a4);

U_PCICTRL_aData_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a5_a = DFFE(PCI_DQ_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a5_a);

U_PCICTRL_ai_a560_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a560 = !U_PCICTRL_aPCI_ADR_a4_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_areduce_nor_66_a27 & !U_PCICTRL_ai_a668

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0010",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_areduce_nor_66_a27,
	datad => U_PCICTRL_ai_a668,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a560);

U_PCICTRL_aWE_DCD_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a2_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_ai_a560, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_ai_a560,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a2_a_areg0);

CREG_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a5_a = DFFE(U_PCICTRL_aData_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a5_a);

PFF_FF_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_FF(1),
	combout => PFF_FF_a1_a_acombout);

PFF_FF_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_FF(0),
	combout => PFF_FF_a0_a_acombout);

DSP_H3_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_H3,
	combout => DSP_H3_acombout);

a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a = DFFE(!PFF_FF_a1_a_acombout & !PFF_FF_a0_a_acombout, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PFF_FF_a1_a_acombout,
	datad => PFF_FF_a0_a_acombout,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a);

a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a = DFFE(a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a);

U_PCICTRL_ai_a626_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a626 = PCI_DQ_a8_a_a7 # PCI_DQ_a10_a_a5 # PCI_DQ_a5_a_a10 # !PCI_DQ_a4_a_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_a8_a_a7,
	datab => PCI_DQ_a4_a_a11,
	datac => PCI_DQ_a10_a_a5,
	datad => PCI_DQ_a5_a_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a626);

U_PCICTRL_ai_a631_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a631 = PCI_DQ_a7_a_a8 # PCI_DQ_a6_a_a9 # PCI_DQ_a9_a_a6 # OPTADR_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_a7_a_a8,
	datab => PCI_DQ_a6_a_a9,
	datac => PCI_DQ_a9_a_a6,
	datad => OPTADR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a631);

U_PCICTRL_ai_a651_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a651 = U_PCICTRL_ai_a626 # U_PCICTRL_ai_a631 # !PCI_DQ_a2_a_a13 # !PCI_DQ_a3_a_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF7",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_a3_a_a12,
	datab => PCI_DQ_a2_a_a13,
	datac => U_PCICTRL_ai_a626,
	datad => U_PCICTRL_ai_a631,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a651);

U_PCICTRL_ai_a572_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a572 = !OPTBE_a2_a_acombout & !OPTBE_a1_a_acombout & !OPTATN_acombout & U_PCICTRL_adata_en_a40

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTBE_a2_a_acombout,
	datab => OPTBE_a1_a_acombout,
	datac => OPTATN_acombout,
	datad => U_PCICTRL_adata_en_a40,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a572);

U_PCICTRL_ai_a15_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a15 = PTWR_acombout # U_PCICTRL_aDPR_ACC_areg0 # U_PCICTRL_ai_a651 # !U_PCICTRL_ai_a572

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PTWR_acombout,
	datab => U_PCICTRL_aDPR_ACC_areg0,
	datac => U_PCICTRL_ai_a651,
	datad => U_PCICTRL_ai_a572,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a15);

clk_pll2_aaltclklock_component_apll : apex20ke_pll
-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	effective_phase_shift => -678,
	effective_clk0_delay => 23375,
	input_frequency => 50000,
	clk0_multiply_by => 2,
	clk1_multiply_by => 9,
	clk0_divide_by => 1,
	clk1_divide_by => 8,
	lock_low => 5,
	lock_high => 5,
	invalid_lock_multiplier => 5,
	valid_lock_multiplier => 5,
	phase_shift => 0,
	simulation_type => "timing")
-- pragma translate_on
PORT MAP (
	clk => CLK20_IN_acombout,
	clk0 => clk_pll2_aaltclklock_component_aoutclock0);

U_FFCNT_aFF_RENV_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aFF_RENV_a0_a = DFFE(!U_PCICTRL_ai_a15, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_ai_a15,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aFF_RENV_a0_a);

U_FFCNT_aFF_RENV_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aFF_RENV_a1_a = DFFE(U_FFCNT_aFF_RENV_a0_a, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_FFCNT_aFF_RENV_a0_a,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aFF_RENV_a1_a);

DSP_A1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(2),
	combout => DSP_A1_a2_a_acombout);

DSP_A1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(1),
	combout => DSP_A1_a1_a_acombout);

DSP_A1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(7),
	combout => DSP_A1_a7_a_acombout);

DSP_A1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(4),
	combout => DSP_A1_a4_a_acombout);

DSP_A1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(3),
	combout => DSP_A1_a3_a_acombout);

DSP_A1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(6),
	combout => DSP_A1_a6_a_acombout);

a_aST_GEN_aU_DSPCTRL_ai_a366_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a366 = !DSP_A1_a5_a_acombout & !DSP_A1_a4_a_acombout & !DSP_A1_a3_a_acombout & !DSP_A1_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a5_a_acombout,
	datab => DSP_A1_a4_a_acombout,
	datac => DSP_A1_a3_a_acombout,
	datad => DSP_A1_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a366);

a_aST_GEN_aU_DSPCTRL_aDSP_A_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(15),
	combout => DSP_A_a15_a_acombout);

DSP_WR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_WR,
	combout => DSP_WR_acombout);

a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(14),
	combout => DSP_A_a14_a_acombout);

DSP_STB1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(1),
	combout => DSP_STB1_a1_a_acombout);

DSP_STB1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(2),
	combout => DSP_STB1_a2_a_acombout);

DSP_STB1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(3),
	combout => DSP_STB1_a3_a_acombout);

a_aST_GEN_aU_DSPCTRL_ai_a320_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a320 = !DSP_STB1_a0_a_acombout & !DSP_STB1_a1_a_acombout & !DSP_STB1_a2_a_acombout & !DSP_STB1_a3_a_acombout
-- a_aST_GEN_aU_DSPCTRL_ai_a442 = !DSP_STB1_a0_a_acombout & !DSP_STB1_a1_a_acombout & !DSP_STB1_a2_a_acombout & !DSP_STB1_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB1_a0_a_acombout,
	datab => DSP_STB1_a1_a_acombout,
	datac => DSP_STB1_a2_a_acombout,
	datad => DSP_STB1_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a320,
	cascout => a_aST_GEN_aU_DSPCTRL_ai_a442);

a_aST_GEN_aU_DSPCTRL_ai_a439_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a439 = (!DSP_A_a13_a_acombout & !DSP_A_a15_a_acombout & !DSP_WR_acombout & DSP_A_a14_a_acombout) & CASCADE(a_aST_GEN_aU_DSPCTRL_ai_a442)
-- a_aST_GEN_aU_DSPCTRL_ai_a441 = (!DSP_A_a13_a_acombout & !DSP_A_a15_a_acombout & !DSP_WR_acombout & DSP_A_a14_a_acombout) & CASCADE(a_aST_GEN_aU_DSPCTRL_ai_a442)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A_a13_a_acombout,
	datab => DSP_A_a15_a_acombout,
	datac => DSP_WR_acombout,
	datad => DSP_A_a14_a_acombout,
	cascin => a_aST_GEN_aU_DSPCTRL_ai_a442,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a439,
	cascout => a_aST_GEN_aU_DSPCTRL_ai_a441);

a_aST_GEN_aU_DSPCTRL_ai_a438_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a438 = (!DSP_A1_a7_a_acombout & a_aST_GEN_aU_DSPCTRL_ai_a366) & CASCADE(a_aST_GEN_aU_DSPCTRL_ai_a441)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a7_a_acombout,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a366,
	cascin => a_aST_GEN_aU_DSPCTRL_ai_a441,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a438);

a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 = DFFE(!DSP_A1_a0_a_acombout & !DSP_A1_a2_a_acombout & !DSP_A1_a1_a_acombout & a_aST_GEN_aU_DSPCTRL_ai_a438, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_A1_a2_a_acombout,
	datac => DSP_A1_a1_a_acombout,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a438,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0);

U_FFCNT_aFF_WENV_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aFF_WENV_a0_a = DFFE(a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aFF_WENV_a0_a);

U_FFCNT_aadd_35_rtl_0_a0_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a0 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a $ U_FFCNT_ai_a96
-- U_FFCNT_aadd_35_rtl_0_a0COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a & U_FFCNT_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a,
	datab => U_FFCNT_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a0,
	cout => U_FFCNT_aadd_35_rtl_0_a0COUT);

U_FFCNT_aadd_35_rtl_0_a1_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a1 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a0COUT
-- U_FFCNT_aadd_35_rtl_0_a1COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a0COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a & (!U_FFCNT_aadd_35_rtl_0_a0COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a0COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a1,
	cout => U_FFCNT_aadd_35_rtl_0_a1COUT);

U_FFCNT_aadd_35_rtl_0_a2_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a2 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a1COUT
-- U_FFCNT_aadd_35_rtl_0_a2COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a1COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a1COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a1COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a2,
	cout => U_FFCNT_aadd_35_rtl_0_a2COUT);

DSP_A1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(0),
	combout => DSP_A1_a0_a_acombout);

a_aST_GEN_aU_DSPCTRL_ai_a406_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a406 = !DSP_A1_a1_a_acombout & DSP_A1_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a406);

a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22 = DFFE(a_aST_GEN_aU_DSPCTRL_ai_a252, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPCTRL_ai_a252 = !DSP_A1_a7_a_acombout & a_aST_GEN_aU_DSPCTRL_ai_a406 & a_aST_GEN_aU_DSPCTRL_ai_a366 & a_aST_GEN_aU_DSPCTRL_ai_a439

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a7_a_acombout,
	datab => a_aST_GEN_aU_DSPCTRL_ai_a406,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a366,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a439,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22,
	cascout => a_aST_GEN_aU_DSPCTRL_ai_a252);

a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21 = DFFE(DSP_A1_a2_a_acombout, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a2_a_acombout,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21);

a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a = DFFE(a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22 & a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22,
	datac => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a);

a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a = DFFE(a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a);

a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0 = DFFE(a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a # a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a # a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21 & a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22, GLOBAL(DSP_H3_acombout), , , S5920MR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a21,
	datab => a_aST_GEN_aU_DSPCTRL_aFF_MR_a0_a_a22,
	datac => a_aST_GEN_aU_DSPCTRL_aFF_MR_a2_a,
	datad => a_aST_GEN_aU_DSPCTRL_aFF_MR_a1_a,
	clk => DSP_H3_acombout,
	ena => S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0);

U_PCICTRL_aWE_DCD_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a8_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a2_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_ai_a559, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_ai_a559,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a8_a_areg0);

PFF_RS_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- PFF_RS_VEC_a0_a = DFFE(a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0 # U_PCICTRL_aWE_DCD_a8_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => a_aST_GEN_aU_DSPCTRL_aPFF_MR_areg0,
	datad => U_PCICTRL_aWE_DCD_a8_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PFF_RS_VEC_a0_a);

PFF_RS_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- PFF_RS_VEC_a1_a = DFFE(PFF_RS_VEC_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PFF_RS_VEC_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PFF_RS_VEC_a1_a);

FF_RS_aI : apex20ke_lcell
-- Equation(s):
-- FF_RS = DFFE(!PFF_RS_VEC_a1_a & !PFF_RS_VEC_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PFF_RS_VEC_a1_a,
	datad => PFF_RS_VEC_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_RS);

U_FFCNT_aFF_WENV_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aFF_WENV_a1_a = DFFE(U_FFCNT_aFF_WENV_a0_a, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_FFCNT_aFF_WENV_a0_a,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aFF_WENV_a1_a);

U_FFCNT_aadd_35_rtl_0_a3_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a3 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a2COUT
-- U_FFCNT_aadd_35_rtl_0_a3COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a2COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a & (!U_FFCNT_aadd_35_rtl_0_a2COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a2COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a3,
	cout => U_FFCNT_aadd_35_rtl_0_a3COUT);

U_FFCNT_aadd_35_rtl_0_a4_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a4 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a3COUT
-- U_FFCNT_aadd_35_rtl_0_a4COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a3COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a3COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a3COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a4,
	cout => U_FFCNT_aadd_35_rtl_0_a4COUT);

U_FFCNT_aadd_35_rtl_0_a5_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a5 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a4COUT
-- U_FFCNT_aadd_35_rtl_0_a5COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a4COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a & (!U_FFCNT_aadd_35_rtl_0_a4COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a4COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a5,
	cout => U_FFCNT_aadd_35_rtl_0_a5COUT);

U_FFCNT_aadd_35_rtl_0_a6_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a6 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a5COUT
-- U_FFCNT_aadd_35_rtl_0_a6COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a5COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a5COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a5COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a6,
	cout => U_FFCNT_aadd_35_rtl_0_a6COUT);

U_FFCNT_aadd_35_rtl_0_a7_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a7 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a6COUT
-- U_FFCNT_aadd_35_rtl_0_a7COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a6COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a & (!U_FFCNT_aadd_35_rtl_0_a6COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a6COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a7,
	cout => U_FFCNT_aadd_35_rtl_0_a7COUT);

U_FFCNT_aadd_35_rtl_0_a8_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a8 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a7COUT
-- U_FFCNT_aadd_35_rtl_0_a8COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a7COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a7COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a7COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a8,
	cout => U_FFCNT_aadd_35_rtl_0_a8COUT);

U_FFCNT_aadd_35_rtl_0_a9_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a9 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a8COUT
-- U_FFCNT_aadd_35_rtl_0_a9COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a8COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a & (!U_FFCNT_aadd_35_rtl_0_a8COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a8COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a9,
	cout => U_FFCNT_aadd_35_rtl_0_a9COUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a2) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	datac => U_FFCNT_aadd_35_rtl_0_a2,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a3) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	datac => U_FFCNT_aadd_35_rtl_0_a3,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a4) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	datac => U_FFCNT_aadd_35_rtl_0_a4,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a5) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	datac => U_FFCNT_aadd_35_rtl_0_a5,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a6) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	datac => U_FFCNT_aadd_35_rtl_0_a6,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a7) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	datac => U_FFCNT_aadd_35_rtl_0_a7,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a7_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a9) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a $ (U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT)) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	datac => U_FFCNT_aadd_35_rtl_0_a9,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT);

U_FFCNT_aadd_35_rtl_0_a10_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a10 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a9COUT
-- U_FFCNT_aadd_35_rtl_0_a10COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a9COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a9COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a9COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a10,
	cout => U_FFCNT_aadd_35_rtl_0_a10COUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a10) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	datac => U_FFCNT_aadd_35_rtl_0_a10,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT);

U_FFCNT_aadd_35_rtl_0_a11_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a11 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a10COUT
-- U_FFCNT_aadd_35_rtl_0_a11COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a10COUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a & (!U_FFCNT_aadd_35_rtl_0_a10COUT # !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a10COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a11,
	cout => U_FFCNT_aadd_35_rtl_0_a11COUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a11) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	datac => U_FFCNT_aadd_35_rtl_0_a11,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT);

U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a
-- U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "AA55",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	cout => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a);

U_FFCNT_areduce_nor_19_a45_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_19_a45 = !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a & !U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	datad => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_19_a45_1,
	cascout => U_FFCNT_areduce_nor_19_a45);

U_FFCNT_areduce_nor_19_a108_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_19_a108 = (!U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a) & 
-- CASCADE(U_FFCNT_areduce_nor_19_a45)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	cascin => U_FFCNT_areduce_nor_19_a45,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_19_a108);

U_FFCNT_aadd_35_rtl_0_a12_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a12 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a11COUT
-- U_FFCNT_aadd_35_rtl_0_a12COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a11COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a11COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a11COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a12,
	cout => U_FFCNT_aadd_35_rtl_0_a12COUT);

U_FFCNT_aadd_35_rtl_0_a13_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a13 = U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 $ U_FFCNT_ai_a96 $ U_FFCNT_aadd_35_rtl_0_a12COUT
-- U_FFCNT_aadd_35_rtl_0_a13COUT = CARRY(U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 & !U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a12COUT # !U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 & (!U_FFCNT_aadd_35_rtl_0_a12COUT # 
-- !U_FFCNT_ai_a96))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a12COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a13,
	cout => U_FFCNT_aadd_35_rtl_0_a13COUT);

U_FFCNT_aadd_35_rtl_0_a14_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a14 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a $ U_FFCNT_ai_a96 $ !U_FFCNT_aadd_35_rtl_0_a13COUT
-- U_FFCNT_aadd_35_rtl_0_a14COUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a & (U_FFCNT_ai_a96 # !U_FFCNT_aadd_35_rtl_0_a13COUT) # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a & U_FFCNT_ai_a96 & !U_FFCNT_aadd_35_rtl_0_a13COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	datab => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a13COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a14,
	cout => U_FFCNT_aadd_35_rtl_0_a14COUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a12) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	datac => U_FFCNT_aadd_35_rtl_0_a12,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a12_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a14) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a $ !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	datac => U_FFCNT_aadd_35_rtl_0_a14,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a_aCOUT);

U_FFCNT_areduce_nor_19_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_19 = !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a # !U_FFCNT_areduce_nor_19_a108 # !U_FFCNT_areduce_nor_19_a109

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F7F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_areduce_nor_19_a109,
	datab => U_FFCNT_areduce_nor_19_a108,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_19_a1,
	cascout => U_FFCNT_areduce_nor_19);

U_FFCNT_ai_a95_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_ai_a95 = (!U_FFCNT_aFF_WENV_a0_a & U_FFCNT_aFF_WENV_a1_a & (U_FFCNT_aFF_RENV_a0_a # !U_FFCNT_aFF_RENV_a1_a)) & CASCADE(U_FFCNT_areduce_nor_19)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aFF_WENV_a0_a,
	datab => U_FFCNT_aFF_RENV_a0_a,
	datac => U_FFCNT_aFF_WENV_a1_a,
	datad => U_FFCNT_aFF_RENV_a1_a,
	cascin => U_FFCNT_areduce_nor_19,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_ai_a95);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a0) # (U_FFCNT_ai_a95 & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a) & FF_RS, GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_FFCNT_aadd_35_rtl_0_a0,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a1) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT # !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	datac => U_FFCNT_aadd_35_rtl_0_a1,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	cout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT);

U_FFCNT_areduce_nor_19_a59_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_19_a59 = !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_19_a59_1,
	cascout => U_FFCNT_areduce_nor_19_a59);

U_FFCNT_areduce_nor_19_a109_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_19_a109 = (!U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a) & CASCADE(U_FFCNT_areduce_nor_19_a59)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a,
	cascin => U_FFCNT_areduce_nor_19_a59,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_19_a109);

U_FFCNT_areduce_nor_20_aI : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_areduce_nor_20 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a # !U_FFCNT_areduce_nor_19_a108 # !U_FFCNT_areduce_nor_19_a109

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	datac => U_FFCNT_areduce_nor_19_a109,
	datad => U_FFCNT_areduce_nor_19_a108,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_areduce_nor_20_a1,
	cascout => U_FFCNT_areduce_nor_20);

U_FFCNT_ai_a96_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_ai_a96 = (U_FFCNT_aFF_RENV_a1_a & !U_FFCNT_aFF_RENV_a0_a & (U_FFCNT_aFF_WENV_a0_a # !U_FFCNT_aFF_WENV_a1_a)) & CASCADE(U_FFCNT_areduce_nor_20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aFF_WENV_a1_a,
	datab => U_FFCNT_aFF_RENV_a1_a,
	datac => U_FFCNT_aFF_WENV_a0_a,
	datad => U_FFCNT_aFF_RENV_a0_a,
	cascin => U_FFCNT_areduce_nor_20,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_ai_a96);

U_FFCNT_aadd_35_rtl_0_a15_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aadd_35_rtl_0_a15 = U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a $ (U_FFCNT_aadd_35_rtl_0_a14COUT $ U_FFCNT_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	datad => U_FFCNT_ai_a96,
	cin => U_FFCNT_aadd_35_rtl_0_a14COUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aadd_35_rtl_0_a15);

U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a = DFFE((!U_FFCNT_ai_a95 & U_FFCNT_aadd_35_rtl_0_a15) # (U_FFCNT_ai_a95 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a_aCOUT $ U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a) & FF_RS, 
-- GLOBAL(clk_pll2_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_FFCNT_aadd_35_rtl_0_a15,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	cin => U_FFCNT_aff_cnt_rtl_1_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_pll2_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_FF_RS,
	sload => ALT_INV_U_FFCNT_ai_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a);

U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15_I : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a = CARRY(U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a # !U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	cin => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15,
	cout => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a);

U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out = !U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a & !U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	cin => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out);

a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a = DFFE(!U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_aagb_out,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a);

a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a = DFFE(a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a);

a_aST_GEN_aU_DSPCTRL_ai_a120_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a120 = a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a & (a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a & !a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a # !a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a) # !a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a & (a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a & 
-- !a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_aFF_VEC_a0_a,
	datab => a_aST_GEN_aU_DSPCTRL_aFF_VEC_a1_a,
	datac => a_aST_GEN_aU_DSPCTRL_aHF_VEC_a0_a,
	datad => a_aST_GEN_aU_DSPCTRL_aHF_VEC_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a120);

a_aST_GEN_aU_DSPCTRL_ai_a440_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a440 = (CREG_FF_adffs_a5_a & !DSP_A1_a2_a_acombout) & CASCADE(a_aST_GEN_aU_DSPCTRL_ai_a252)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => CREG_FF_adffs_a5_a,
	datad => DSP_A1_a2_a_acombout,
	cascin => a_aST_GEN_aU_DSPCTRL_ai_a252,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a440);

a_aST_GEN_aU_DSPCTRL_ai_a248_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a248 = !DSP_A1_a7_a_acombout & !DSP_A1_a0_a_acombout & a_aST_GEN_aU_DSPCTRL_ai_a366 & a_aST_GEN_aU_DSPCTRL_ai_a439

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a7_a_acombout,
	datab => DSP_A1_a0_a_acombout,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a366,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a439,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a248);

a_aST_GEN_aU_DSPCTRL_ai_a127_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a127 = DSP_A1_a1_a_acombout & !DSP_A1_a2_a_acombout & a_aST_GEN_aU_DSPCTRL_ai_a248 # !a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5D55",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0,
	datab => DSP_A1_a1_a_acombout,
	datac => DSP_A1_a2_a_acombout,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a248,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a127);

a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0 = DFFE(a_aST_GEN_aU_DSPCTRL_ai_a440 # CREG_FF_adffs_a5_a & a_aST_GEN_aU_DSPCTRL_ai_a120 # !a_aST_GEN_aU_DSPCTRL_ai_a127, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F8FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a5_a,
	datab => a_aST_GEN_aU_DSPCTRL_ai_a120,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a440,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a127,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0);

U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a = DFFE(U_FCONFIG_aiSHIFT_EN & !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a,
	cout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT);

U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a = DFFE(U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a $ !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a & !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	cin => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	cout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT);

U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a = DFFE(U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a $ U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	cin => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	cout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT);

U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a = DFFE(U_FCONFIG_aiSHIFT_EN & U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT $ !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a,
	cin => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aiSHIFT_EN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a);

U_FCONFIG_ai_a3227_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3227 = !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "33FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a4_a,
	datad => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3227);

U_FCONFIG_ai_a3140_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3140 = U_FCONFIG_ai_a3227 # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a # !U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a1_a,
	datab => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a2_a,
	datac => U_FCONFIG_ai_a3227,
	datad => U_FCONFIG_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3140);

U_PCICTRL_aPCI_ADR_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a2_a_areg0 = DFFE(PCI_DQ_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a2_a_areg0);

U_PCICTRL_aPCI_ADR_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a4_a_areg0 = DFFE(PCI_DQ_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a4_a_areg0);

U_PCICTRL_ai_a580_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a580 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a580);

U_PCICTRL_ai_a577_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a577 = U_PCICTRL_areduce_nor_66_a27 & !U_PCICTRL_aPCI_ADR_a4_a_areg0 & U_PCICTRL_ai_a580 & !U_PCICTRL_ai_a668

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0020",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_areduce_nor_66_a27,
	datab => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datac => U_PCICTRL_ai_a580,
	datad => U_PCICTRL_ai_a668,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a577);

U_PCICTRL_aWE_DCD_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a5_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_ai_a577), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_PCICTRL_ai_a577,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a5_a_areg0);

U_FCONFIG_aDATA_LD_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aDATA_LD_VEC_a0_a = DFFE(U_PCICTRL_aWE_DCD_a5_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aWE_DCD_a5_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aDATA_LD_VEC_a0_a);

U_FCONFIG_aDATA_LD_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aDATA_LD_VEC_a1_a = DFFE(U_FCONFIG_aDATA_LD_VEC_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_aDATA_LD_VEC_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aDATA_LD_VEC_a1_a);

U_FCONFIG_ai_a3210_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3210 = !U_FCONFIG_aDATA_LD_VEC_a1_a & (U_FCONFIG_aDATA_LD_VEC_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aDATA_LD_VEC_a1_a,
	datad => U_FCONFIG_aDATA_LD_VEC_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3210);

U_FCONFIG_aiSHIFT_EN_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aiSHIFT_EN = DFFE(CREG_FF_adffs_a6_a & (U_FCONFIG_ai_a3210 # U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3140) # !CREG_FF_adffs_a6_a & U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3140, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3140,
	datad => U_FCONFIG_ai_a3210,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aiSHIFT_EN);

U_FCONFIG_aiDCLK_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aiDCLK = DFFE(!U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_FCONFIG_aiDCLK,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aiDCLK);

U_FCONFIG_aDCLK_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aDCLK_areg0 = DFFE(U_FCONFIG_aiDCLK, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_aiDCLK,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aDCLK_areg0);

U_PCICTRL_aData_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a6_a = DFFE(PCI_DQ_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a6_a);

CREG_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a6_a = DFFE(U_PCICTRL_aData_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a6_a);

U_FCONFIG_aFPGA_REG_a0_a_a0_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a0_a_a0 = DFFE(FPGA_BOOT_FF_adffs_a0_a & CREG_FF_adffs_a6_a & !U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_BOOT_FF_adffs_a0_a,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a3210,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a0_a_a0);

U_FCONFIG_ai_a3206_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3206 = !U_FCONFIG_aDATA_LD_VEC_a1_a & CREG_FF_adffs_a6_a & U_FCONFIG_aDATA_LD_VEC_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aDATA_LD_VEC_a1_a,
	datac => CREG_FF_adffs_a6_a,
	datad => U_FCONFIG_aDATA_LD_VEC_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3206);

U_PCICTRL_aData_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a1_a = DFFE(PCI_DQ_a1_a_a14, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a1_a_a14,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a1_a);

FPGA_BOOT_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a1_a = DFFE(U_PCICTRL_aData_FF_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a1_a);

U_FCONFIG_aFPGA_REG_a1_a_a2_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a1_a_a2 = DFFE(CREG_FF_adffs_a6_a & FPGA_BOOT_FF_adffs_a1_a & !U_FCONFIG_aiSHIFT_EN & U_FCONFIG_ai_a3210, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a6_a,
	datab => FPGA_BOOT_FF_adffs_a1_a,
	datac => U_FCONFIG_aiSHIFT_EN,
	datad => U_FCONFIG_ai_a3210,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a1_a_a2);

U_FCONFIG_ai_a261_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a261 = U_FCONFIG_aiDCLK & !U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a1_a_a3 # U_FCONFIG_aFPGA_REG_a1_a_a2) # !U_FCONFIG_aiDCLK & (U_FCONFIG_aFPGA_REG_a1_a_a3 # U_FCONFIG_aFPGA_REG_a1_a_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7770",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a1_a_a3,
	datad => U_FCONFIG_aFPGA_REG_a1_a_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a261);

U_FCONFIG_aFPGA_REG_a1_a_a3_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a1_a_a3 = DFFE(U_FCONFIG_ai_a260 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a260 & U_FCONFIG_ai_a261 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EE0E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a260,
	datab => U_FCONFIG_ai_a261,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_aiSHIFT_EN,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a1_a_a3);

U_FCONFIG_ai_a250_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a250 = U_FCONFIG_aiDCLK & U_FCONFIG_aiSHIFT_EN & (U_FCONFIG_aFPGA_REG_a1_a_a3 # U_FCONFIG_aFPGA_REG_a1_a_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8880",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aiDCLK,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_aFPGA_REG_a1_a_a3,
	datad => U_FCONFIG_aFPGA_REG_a1_a_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a250);

U_FCONFIG_aFPGA_REG_a0_a_a1_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aFPGA_REG_a0_a_a1 = DFFE(U_FCONFIG_ai_a251 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206) # !U_FCONFIG_ai_a251 & U_FCONFIG_ai_a250 & (U_FCONFIG_aiSHIFT_EN # !U_FCONFIG_ai_a3206), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CF8A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_ai_a251,
	datab => U_FCONFIG_aiSHIFT_EN,
	datac => U_FCONFIG_ai_a3206,
	datad => U_FCONFIG_ai_a250,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aFPGA_REG_a0_a_a1);

U_FCONFIG_aDATA_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aDATA_areg0 = DFFE(U_FCONFIG_aFPGA_REG_a0_a_a0 # U_FCONFIG_aFPGA_REG_a0_a_a1, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aFPGA_REG_a0_a_a0,
	datad => U_FCONFIG_aFPGA_REG_a0_a_a1,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aDATA_areg0);

U_PCICTRL_aWE_DCD_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a6_a_areg0 = DFFE(U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_ai_a560, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_ai_a560,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a6_a_areg0);

U_FCONFIG_aCW_LD_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCW_LD_VEC_a0_a = DFFE(U_PCICTRL_aWE_DCD_a6_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aWE_DCD_a6_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCW_LD_VEC_a0_a);

U_FCONFIG_aCW_LD_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCW_LD_VEC_a1_a = DFFE(U_FCONFIG_aCW_LD_VEC_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_aCW_LD_VEC_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCW_LD_VEC_a1_a);

U_FCONFIG_anstatus_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_anstatus_Vec_a0_a = DFFE(GND, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_anstatus_Vec_a0_a);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a $ U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT # !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a $ !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a & !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a2_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a $ !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a & !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a $ U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT, 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT # !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a,
	cout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a5_a_aCOUT);

U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a = DFFE(U_FCONFIG_aNCONFIG_areg0 & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a $ (U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a,
	cin => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a);

U_FCONFIG_ai_a3168_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3168 = !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a & 
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a6_a,
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a5_a,
	datac => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a7_a,
	datad => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3168_1,
	cascout => U_FCONFIG_ai_a3168);

U_FCONFIG_ai_a3239_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a3239 = (!U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a & !U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a & U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a & 
-- U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a) & CASCADE(U_FCONFIG_ai_a3168)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a3_a,
	datab => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a2_a,
	datac => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a0_a,
	datad => U_FCONFIG_aCONFIG_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	cascin => U_FCONFIG_ai_a3168,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a3239);

U_FCONFIG_anstatus_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_anstatus_Vec_a1_a = DFFE(U_FCONFIG_anstatus_Vec_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_FCONFIG_anstatus_Vec_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_anstatus_Vec_a1_a);

U_FCONFIG_ai_a241_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_ai_a241 = U_FCONFIG_ai_a3239 # !U_FCONFIG_anstatus_Vec_a0_a & U_FCONFIG_anstatus_Vec_a1_a # !U_FCONFIG_aNCONFIG_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7F5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aNCONFIG_areg0,
	datab => U_FCONFIG_anstatus_Vec_a0_a,
	datac => U_FCONFIG_ai_a3239,
	datad => U_FCONFIG_anstatus_Vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_FCONFIG_ai_a241);

U_FCONFIG_aNCONFIG_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_FCONFIG_aNCONFIG_areg0 = DFFE(!U_FCONFIG_aCW_LD_VEC_a1_a & CREG_FF_adffs_a6_a & U_FCONFIG_aCW_LD_VEC_a0_a # !U_FCONFIG_ai_a241, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "40FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aCW_LD_VEC_a1_a,
	datab => CREG_FF_adffs_a6_a,
	datac => U_FCONFIG_aCW_LD_VEC_a0_a,
	datad => U_FCONFIG_ai_a241,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_FCONFIG_aNCONFIG_areg0);

U_PCICTRL_areduce_nor_66_a36_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_66_a36 = U_PCICTRL_aPCI_ADR_a3_a_areg0 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 # !U_PCICTRL_aPCI_ADR_a2_a_areg0 # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_66_a36);

U_PCICTRL_ai_a563_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a563 = OPTADR_acombout & !U_PCICTRL_aDPR_ACC_areg0 & !PTWR_acombout & U_PCICTRL_ai_a572

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTADR_acombout,
	datab => U_PCICTRL_aDPR_ACC_areg0,
	datac => PTWR_acombout,
	datad => U_PCICTRL_ai_a572,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a563);

U_PCICTRL_ai_a138_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a138 = U_PCICTRL_areduce_nor_66_a27 & !U_PCICTRL_areduce_nor_66_a36 & !U_PCICTRL_aPCI_ADR_a4_a_areg0 & U_PCICTRL_ai_a563

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_areduce_nor_66_a27,
	datab => U_PCICTRL_areduce_nor_66_a36,
	datac => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datad => U_PCICTRL_ai_a563,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a138);

PFF_WCK_a249_I : apex20ke_lcell
-- Equation(s):
-- PFF_WCK_a249 = DSP_STB1_a0_a_acombout & !a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 & DSP_STB1_a3_a_acombout & DSP_STB1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB1_a0_a_acombout,
	datab => a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0,
	datac => DSP_STB1_a3_a_acombout,
	datad => DSP_STB1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_WCK_a249);

PFF_WCK_a248_I : apex20ke_lcell
-- Equation(s):
-- PFF_WCK_a248 = DSP_STB1_a0_a_acombout # DSP_STB1_a3_a_acombout # DSP_STB1_a1_a_acombout # !a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB1_a0_a_acombout,
	datab => a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0,
	datac => DSP_STB1_a3_a_acombout,
	datad => DSP_STB1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_WCK_a248);

PFF_WCK_a250_I : apex20ke_lcell
-- Equation(s):
-- PFF_WCK_a250 = DSP_STB1_a2_a_acombout & (DSP_H3_acombout # !PFF_WCK_a249) # !DSP_STB1_a2_a_acombout & (PFF_WCK_a248)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BF8C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_H3_acombout,
	datab => DSP_STB1_a2_a_acombout,
	datac => PFF_WCK_a249,
	datad => PFF_WCK_a248,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_WCK_a250);

CREG_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a1_a = DFFE(U_PCICTRL_aData_FF_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a1_a);

U_PCICTRL_aData_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a3_a = DFFE(PCI_DQ_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a3_a);

CREG_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a3_a = DFFE(U_PCICTRL_aData_FF_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a3_a);

U_PCICTRL_aData_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a2_a = DFFE(PCI_DQ_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a2_a);

CREG_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a2_a = DFFE(U_PCICTRL_aData_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a2_a);

U_PCICTRL_aData_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a0_a = DFFE(PCI_DQ_a0_a_a15, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a0_a_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a0_a);

CREG_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a0_a = DFFE(U_PCICTRL_aData_FF_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a0_a);

Mux_a1121_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1121 = CREG_FF_adffs_a3_a & !CREG_FF_adffs_a2_a & !CREG_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a3_a,
	datac => CREG_FF_adffs_a2_a,
	datad => CREG_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1121);

a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28 = !DSP_A_a14_a_acombout & !DSP_A_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A_a14_a_acombout,
	datad => DSP_A_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28);

U_PCICTRL_aData_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a4_a = DFFE(PCI_DQ_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a4_a);

CREG_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a4_a = DFFE(U_PCICTRL_aData_FF_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a4_a);

a_aST_GEN_aU_DSPCTRL_ai_a96_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a96 = !DSP_WR_acombout & a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28 & CREG_FF_adffs_a4_a & a_aST_GEN_aU_DSPCTRL_ai_a320

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WR_acombout,
	datab => a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28,
	datac => CREG_FF_adffs_a4_a,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a320,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a96);

U_DPRAM_awren_b_cb_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_awren_b_cb = DFFE(a_aST_GEN_aU_DSPCTRL_ai_a96 # U_DPRAM_awren_b_cb & !U_DPRAM_awren_b_ca, GLOBAL(DSP_H3_acombout), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_awren_b_cb,
	datac => U_DPRAM_awren_b_ca,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a96,
	clk => DSP_H3_acombout,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_awren_b_cb);

U_DPRAM_awren_b_ca_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_awren_b_ca = DFFE(U_DPRAM_awren_b_cb, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_DPRAM_awren_b_cb,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_awren_b_ca);

U_DPRAM_alast_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_alast = DFFE(!U_DPRAM_astate_a9 & (U_DPRAM_astate_a15 # U_DPRAM_alast), GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3232",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a15,
	datab => U_DPRAM_astate_a9,
	datac => U_DPRAM_alast,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_alast);

U_PCICTRL_aPCI_DPRA_a8_a_a1_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a8_a_a1 = PCI_DQ_a15_a_a0 & !OPTADR_acombout & !OPTATN_acombout & U_PCICTRL_adata_en_a69

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_a15_a_a0,
	datab => OPTADR_acombout,
	datac => OPTATN_acombout,
	datad => U_PCICTRL_adata_en_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_aPCI_DPRA_a8_a_a1);

U_PCICTRL_aPCI_DPRA_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a1_a_areg0 = DFFE(PCI_DQ_a3_a_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a3_a_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a1_a_areg0);

U_PCICTRL_ai_a123_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a123 = U_PCICTRL_aDPR_ACC_areg0 & PTWR_acombout & !ODXFER_acombout & U_PCICTRL_adata_en_a69

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aDPR_ACC_areg0,
	datab => PTWR_acombout,
	datac => ODXFER_acombout,
	datad => U_PCICTRL_adata_en_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a123);

U_DPRAM_areg_ff_a_a_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a1_a = DFFE(U_PCICTRL_aPCI_DPRA_a1_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a1_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a1_a);

U_PCICTRL_aPCI_DPRA_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a7_a_areg0 = DFFE(PCI_DQ_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a7_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a7_a = DFFE(U_PCICTRL_aPCI_DPRA_a7_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a7_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a7_a);

U_PCICTRL_aPCI_DPRA_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a8_a_areg0 = DFFE(PCI_DQ_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a8_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a8_a = DFFE(U_PCICTRL_aPCI_DPRA_a8_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a8_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a8_a);

U_PCICTRL_aPCI_DPRA_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a4_a_areg0 = DFFE(PCI_DQ_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a4_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a4_a = DFFE(U_PCICTRL_aPCI_DPRA_a4_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a4_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a4_a);

U_PCICTRL_aPCI_DPRA_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a3_a_areg0 = DFFE(PCI_DQ_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a3_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a3_a = DFFE(U_PCICTRL_aPCI_DPRA_a3_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a3_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a3_a);

U_PCICTRL_aPCI_DPRA_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a2_a_areg0 = DFFE(PCI_DQ_a4_a_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a4_a_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a2_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a2_a = DFFE(U_PCICTRL_aPCI_DPRA_a2_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a2_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a2_a);

U_DPRAM_areduce_nor_122_a34_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areduce_nor_122_a34 = !U_DPRAM_areg_ff_a_a_adffs_a5_a & !U_DPRAM_areg_ff_a_a_adffs_a4_a & !U_DPRAM_areg_ff_a_a_adffs_a3_a & !U_DPRAM_areg_ff_a_a_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a5_a,
	datab => U_DPRAM_areg_ff_a_a_adffs_a4_a,
	datac => U_DPRAM_areg_ff_a_a_adffs_a3_a,
	datad => U_DPRAM_areg_ff_a_a_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_areduce_nor_122_a34_1,
	cascout => U_DPRAM_areduce_nor_122_a34);

U_DPRAM_areduce_nor_122_a57_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areduce_nor_122_a57 = (!U_DPRAM_areg_ff_a_a_adffs_a6_a & (!U_DPRAM_areg_ff_a_a_adffs_a7_a & !U_DPRAM_areg_ff_a_a_adffs_a8_a)) & CASCADE(U_DPRAM_areduce_nor_122_a34)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a6_a,
	datac => U_DPRAM_areg_ff_a_a_adffs_a7_a,
	datad => U_DPRAM_areg_ff_a_a_adffs_a8_a,
	cascin => U_DPRAM_areduce_nor_122_a34,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_areduce_nor_122_a57);

U_DPRAM_astate_a174_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a174 = DFFE(U_DPRAM_areduce_nor_122_a57 & (U_DPRAM_areg_ff_a_a_adffs_a0_a $ !U_DPRAM_areg_ff_a_a_adffs_a1_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a0_a,
	datac => U_DPRAM_areg_ff_a_a_adffs_a1_a,
	datad => U_DPRAM_areduce_nor_122_a57,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a174);

U_DPRAM_ai_a217_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_ai_a217 = U_DPRAM_awren_b_lat & (!U_DPRAM_astate_a174 & !U_DPRAM_astate_a171 # !U_DPRAM_astate_a169)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "02AA",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_awren_b_lat,
	datab => U_DPRAM_astate_a174,
	datac => U_DPRAM_astate_a171,
	datad => U_DPRAM_astate_a169,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_ai_a217_1,
	cascout => U_DPRAM_ai_a217);

U_DPRAM_astate_a15_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a15 = DFFE((!U_DPRAM_alast # !U_DPRAM_awren_a_lat) & CASCADE(U_DPRAM_ai_a217), GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_awren_a_lat,
	datac => U_DPRAM_alast,
	cascin => U_DPRAM_ai_a217,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a15);

U_DPRAM_awren_b_lat_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_awren_b_lat = DFFE(U_DPRAM_awren_b_ca # U_DPRAM_awren_b_lat & !U_DPRAM_astate_a15, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCFC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_awren_b_ca,
	datac => U_DPRAM_awren_b_lat,
	datad => U_DPRAM_astate_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_awren_b_lat);

U_DPRAM_ai_a223_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_ai_a223 = U_DPRAM_awren_a_lat & (!U_DPRAM_astate_a171 & !U_DPRAM_astate_a174 # !U_DPRAM_astate_a169)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "222A",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_awren_a_lat,
	datab => U_DPRAM_astate_a169,
	datac => U_DPRAM_astate_a171,
	datad => U_DPRAM_astate_a174,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_ai_a223_1,
	cascout => U_DPRAM_ai_a223);

U_DPRAM_astate_a9_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a9 = DFFE((U_DPRAM_alast # !U_DPRAM_awren_b_lat) & CASCADE(U_DPRAM_ai_a223), GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFAF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_alast,
	datac => U_DPRAM_awren_b_lat,
	cascin => U_DPRAM_ai_a223,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a9);

U_DPRAM_astate_a171_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a171 = DFFE(!U_DPRAM_astate_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_DPRAM_astate_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a171);

U_DPRAM_aSelect_147_rtl_0_a83_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aSelect_147_rtl_0_a83 = !U_DPRAM_awren_b_lat & (!U_DPRAM_astate_a171 & !U_DPRAM_astate_a174 # !U_DPRAM_astate_a169)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1115",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_awren_b_lat,
	datab => U_DPRAM_astate_a169,
	datac => U_DPRAM_astate_a171,
	datad => U_DPRAM_astate_a174,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aSelect_147_rtl_0_a83);

U_DPRAM_awren_a_lat_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_awren_a_lat = DFFE(U_PCICTRL_ai_a123 # !U_DPRAM_astate_a9 & U_DPRAM_awren_a_lat, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_astate_a9,
	datac => U_DPRAM_awren_a_lat,
	datad => U_PCICTRL_ai_a123,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_awren_a_lat);

U_DPRAM_astate_a169_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a169 = DFFE(!U_DPRAM_astate_a14 & !U_DPRAM_astate_a15 & (U_DPRAM_awren_a_lat # !U_DPRAM_aSelect_147_rtl_0_a83), GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0051",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a14,
	datab => U_DPRAM_aSelect_147_rtl_0_a83,
	datac => U_DPRAM_awren_a_lat,
	datad => U_DPRAM_astate_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a169);

U_DPRAM_aSelect_147_rtl_0_a0_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aSelect_147_rtl_0_a0 = !U_DPRAM_astate_a171 & (!U_DPRAM_astate_a174) # !U_DPRAM_astate_a169

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3377",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a171,
	datab => U_DPRAM_astate_a169,
	datad => U_DPRAM_astate_a174,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aSelect_147_rtl_0_a0);

U_DPRAM_astate_a173_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a173 = DFFE(U_DPRAM_astate_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_DPRAM_astate_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a173);

U_DPRAM_astate_a11_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a11 = DFFE(U_DPRAM_astate_a174 & U_DPRAM_astate_a173, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_astate_a174,
	datad => U_DPRAM_astate_a173,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a11);

U_DPRAM_astate_a12_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a12 = DFFE(U_DPRAM_astate_a11, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_astate_a11,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a12);

U_DPRAM_astate_a13_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a13 = DFFE(U_DPRAM_astate_a12, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_DPRAM_astate_a12,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a13);

U_DPRAM_areduce_or_25_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areduce_or_25 = U_DPRAM_astate_a11 # U_DPRAM_aSelect_147_rtl_0_a0 # U_DPRAM_astate_a15 # U_DPRAM_astate_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a11,
	datab => U_DPRAM_aSelect_147_rtl_0_a0,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_astate_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_areduce_or_25);

Mux_a1122_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1122 = !CREG_FF_adffs_a1_a & Mux_a1121 & !U_DPRAM_areduce_or_25

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a1_a,
	datac => Mux_a1121,
	datad => U_DPRAM_areduce_or_25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1122);

Mux_a1128_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1128 = !CREG_FF_adffs_a3_a & !CREG_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => CREG_FF_adffs_a3_a,
	datad => CREG_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1128);

Mux_a1126_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1126 = CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a) # !CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a & (SB_SDIN_a1) # !CREG_FF_adffs_a1_a & !a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F1C1",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0,
	datab => CREG_FF_adffs_a0_a,
	datac => CREG_FF_adffs_a1_a,
	datad => SB_SDIN_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1126);

Mux_a1127_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1127 = Mux_a1126 & (!U_PCICTRL_ai_a138 # !CREG_FF_adffs_a0_a) # !Mux_a1126 & U_FCONFIG_aDATA_areg0 & CREG_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2CEC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aDATA_areg0,
	datab => Mux_a1126,
	datac => CREG_FF_adffs_a0_a,
	datad => U_PCICTRL_ai_a138,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1127);

U_PCICTRL_ai_a557_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a557 = !OPTATN_acombout & (!PTWR_acombout & U_PCICTRL_adata_en_a69)
-- U_PCICTRL_ai_a700 = !OPTATN_acombout & (!PTWR_acombout & U_PCICTRL_adata_en_a69)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datac => PTWR_acombout,
	datad => U_PCICTRL_adata_en_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a557,
	cascout => U_PCICTRL_ai_a700);

U_PCICTRL_ai_a699_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a699 = (U_PCICTRL_aDPR_ACC_areg0 & OPTADR_acombout) & CASCADE(U_PCICTRL_ai_a700)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aDPR_ACC_areg0,
	datad => OPTADR_acombout,
	cascin => U_PCICTRL_ai_a700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a699);

Mux_a1123_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1123 = CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a) # !CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a & (U_PCICTRL_ai_a699) # !CREG_FF_adffs_a1_a & !U_FCONFIG_aNCONFIG_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F1C1",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aNCONFIG_areg0,
	datab => CREG_FF_adffs_a0_a,
	datac => CREG_FF_adffs_a1_a,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1123);

Mux_a1124_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1124 = CREG_FF_adffs_a0_a & (Mux_a1123 & (a_aST_GEN_aU_DSPCTRL_ai_a96) # !Mux_a1123 & DSP_D_a0_a_a15) # !CREG_FF_adffs_a0_a & (Mux_a1123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F388",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a0_a_a15,
	datab => CREG_FF_adffs_a0_a,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a96,
	datad => Mux_a1123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1124);

Mux_a1125_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1125 = !CREG_FF_adffs_a3_a & (Mux_a1124 & CREG_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a3_a,
	datac => Mux_a1124,
	datad => CREG_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1125);

Mux_a1129_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1129 = Mux_a1122 # Mux_a1125 # Mux_a1128 & Mux_a1127

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1122,
	datab => Mux_a1128,
	datac => Mux_a1127,
	datad => Mux_a1125,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1129);

U_DPRAM_areduce_or_22_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areduce_or_22 = U_DPRAM_astate_a11 # U_DPRAM_astate_a12 # U_DPRAM_astate_a9 # U_DPRAM_aSelect_147_rtl_0_a0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a11,
	datab => U_DPRAM_astate_a12,
	datac => U_DPRAM_astate_a9,
	datad => U_DPRAM_aSelect_147_rtl_0_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_areduce_or_22);

Mux_a1133_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1133 = !CREG_FF_adffs_a1_a & Mux_a1121 & !U_DPRAM_areduce_or_22

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a1_a,
	datac => Mux_a1121,
	datad => U_DPRAM_areduce_or_22,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1133);

Mux_a1131_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1131 = CREG_FF_adffs_a1_a & U_PCICTRL_aDPR_ACC_areg0 # !CREG_FF_adffs_a1_a & (U_FCONFIG_aDATA_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aDPR_ACC_areg0,
	datac => U_FCONFIG_aDATA_areg0,
	datad => CREG_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1131);

Mux_a1130_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1130 = !CREG_FF_adffs_a3_a & CREG_FF_adffs_a2_a
-- Mux_a1162 = !CREG_FF_adffs_a3_a & CREG_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a3_a,
	datac => CREG_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1130,
	cascout => Mux_a1162);

DSP_A_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(13),
	combout => DSP_A_a13_a_acombout);

Mux_a1132_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1132 = Mux_a1130 & (CREG_FF_adffs_a0_a & (DSP_A_a13_a_acombout) # !CREG_FF_adffs_a0_a & Mux_a1131)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => Mux_a1131,
	datac => Mux_a1130,
	datad => DSP_A_a13_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1132);

Mux_a1135_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1135 = Mux_a1134 & (U_PCICTRL_ai_a15 # !CREG_FF_adffs_a1_a) # !Mux_a1134 & OSCLK_a1 & CREG_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA4A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1134,
	datab => OSCLK_a1,
	datac => CREG_FF_adffs_a1_a,
	datad => U_PCICTRL_ai_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1135);

Mux_a1136_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1136 = Mux_a1133 # Mux_a1132 # Mux_a1128 & Mux_a1135

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1133,
	datab => Mux_a1128,
	datac => Mux_a1132,
	datad => Mux_a1135,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1136);

FPGA_CD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FPGA_CD,
	combout => FPGA_CD_acombout);

Mux_a1139_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1139 = CREG_FF_adffs_a0_a & CREG_FF_adffs_a1_a # !CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a & a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0 # !CREG_FF_adffs_a1_a & (DSP_H3_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D9C8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => CREG_FF_adffs_a1_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_MR_areg0,
	datad => DSP_H3_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1139);

Mux_a1140_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1140 = CREG_FF_adffs_a0_a & (Mux_a1139 & !a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0 # !Mux_a1139 & (FPGA_CD_acombout)) # !CREG_FF_adffs_a0_a & (Mux_a1139)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "77A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0,
	datac => FPGA_CD_acombout,
	datad => Mux_a1139,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1140);

U_DPRAM_aSelect_151_rtl_1_a13_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aSelect_151_rtl_1_a13 = U_DPRAM_astate_a174 & U_DPRAM_astate_a173

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_astate_a174,
	datad => U_DPRAM_astate_a173,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aSelect_151_rtl_1_a13);

U_DPRAM_areduce_or_19_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areduce_or_19 = U_DPRAM_astate_a9 # U_DPRAM_astate_a15 # U_DPRAM_aSelect_147_rtl_0_a0 # U_DPRAM_aSelect_151_rtl_1_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a9,
	datab => U_DPRAM_astate_a15,
	datac => U_DPRAM_aSelect_147_rtl_0_a0,
	datad => U_DPRAM_aSelect_151_rtl_1_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_areduce_or_19);

Mux_a1161_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1161 = (CREG_FF_adffs_a0_a & (DSP_A1_a0_a_acombout) # !CREG_FF_adffs_a0_a & U_FCONFIG_aDCLK_areg0) & CASCADE(Mux_a1162)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a0_a,
	datac => U_FCONFIG_aDCLK_areg0,
	datad => DSP_A1_a0_a_acombout,
	cascin => Mux_a1162,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1161);

Mux_a1138_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1138 = !CREG_FF_adffs_a1_a & (Mux_a1161 # Mux_a1121 & !U_DPRAM_areduce_or_19)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F02",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1121,
	datab => U_DPRAM_areduce_or_19,
	datac => CREG_FF_adffs_a1_a,
	datad => Mux_a1161,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1138);

Mux_a1141_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1141 = Mux_a1138 # !CREG_FF_adffs_a2_a & !CREG_FF_adffs_a3_a & Mux_a1140

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF10",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a2_a,
	datab => CREG_FF_adffs_a3_a,
	datac => Mux_a1140,
	datad => Mux_a1138,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1141);

U_PCICTRL_areduce_nor_66_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_areduce_nor_66 = U_PCICTRL_aPCI_ADR_a4_a_areg0 # U_PCICTRL_areduce_nor_66_a36 # !U_PCICTRL_areduce_nor_66_a27

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datab => U_PCICTRL_areduce_nor_66_a27,
	datac => U_PCICTRL_areduce_nor_66_a36,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_areduce_nor_66);

U_PCICTRL_ai_a69_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_ai_a69 = OPTADR_acombout & U_PCICTRL_ai_a557 & (U_PCICTRL_aDPR_ACC_areg0 # U_PCICTRL_areduce_nor_66)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aDPR_ACC_areg0,
	datab => OPTADR_acombout,
	datac => U_PCICTRL_areduce_nor_66,
	datad => U_PCICTRL_ai_a557,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_PCICTRL_ai_a69);

Mux_a1146_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1146 = !CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a & (U_PCICTRL_ai_a69) # !CREG_FF_adffs_a1_a & U_FCONFIG_aSHIFT_EN_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3202",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FCONFIG_aSHIFT_EN_areg0,
	datab => CREG_FF_adffs_a0_a,
	datac => CREG_FF_adffs_a1_a,
	datad => U_PCICTRL_ai_a69,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1146);

DSP_STB1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(0),
	combout => DSP_STB1_a0_a_acombout);

Mux_a1145_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1145 = CREG_FF_adffs_a0_a & !CREG_FF_adffs_a1_a & DSP_STB1_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a0_a,
	datac => CREG_FF_adffs_a1_a,
	datad => DSP_STB1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1145);

PFF_EF_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_EF(0),
	combout => PFF_EF_a0_a_acombout);

FPGA_NS_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FPGA_NS,
	combout => FPGA_NS_acombout);

Mux_a1142_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1142 = CREG_FF_adffs_a0_a & (CREG_FF_adffs_a1_a # FPGA_NS_acombout) # !CREG_FF_adffs_a0_a & U_PCICTRL_aPCI_RST_OUT_areg0 & !CREG_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AEA4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => U_PCICTRL_aPCI_RST_OUT_areg0,
	datac => CREG_FF_adffs_a1_a,
	datad => FPGA_NS_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1142);

Mux_a1143_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1143 = CREG_FF_adffs_a1_a & (Mux_a1142 & (PFF_EF_a0_a_acombout) # !Mux_a1142 & U_PCICTRL_aWE_DCD_a10_a_areg0) # !CREG_FF_adffs_a1_a & (Mux_a1142)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aWE_DCD_a10_a_areg0,
	datab => PFF_EF_a0_a_acombout,
	datac => CREG_FF_adffs_a1_a,
	datad => Mux_a1142,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1143);

Mux_a1144_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1144 = !CREG_FF_adffs_a3_a & !CREG_FF_adffs_a2_a & Mux_a1143

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CREG_FF_adffs_a3_a,
	datac => CREG_FF_adffs_a2_a,
	datad => Mux_a1143,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1144);

Mux_a1147_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1147 = Mux_a1144 # Mux_a1130 & (Mux_a1146 # Mux_a1145)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1130,
	datab => Mux_a1146,
	datac => Mux_a1145,
	datad => Mux_a1144,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1147);

Mux_a1150_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1150 = CREG_FF_adffs_a1_a & (CREG_FF_adffs_a0_a # SB_INTR_a1) # !CREG_FF_adffs_a1_a & PCI_RST_IN_acombout & !CREG_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AEA4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a1_a,
	datab => PCI_RST_IN_acombout,
	datac => CREG_FF_adffs_a0_a,
	datad => SB_INTR_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1150);

Mux_a1151_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1151 = CREG_FF_adffs_a0_a & (Mux_a1150 & DSP_STB1_a0_a_acombout # !Mux_a1150 & (!U_FCONFIG_aNCONFIG_areg0)) # !CREG_FF_adffs_a0_a & (Mux_a1150)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BB0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB1_a0_a_acombout,
	datab => CREG_FF_adffs_a0_a,
	datac => U_FCONFIG_aNCONFIG_areg0,
	datad => Mux_a1150,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1151);

Mux_a1149_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1149 = Mux_a1148 & (CREG_FF_adffs_a1_a & (U_PCICTRL_ai_a138) # !CREG_FF_adffs_a1_a & DSP_WR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A808",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1148,
	datab => DSP_WR_acombout,
	datac => CREG_FF_adffs_a1_a,
	datad => U_PCICTRL_ai_a138,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1149);

Mux_a1152_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1152 = Mux_a1149 # !CREG_FF_adffs_a3_a & !CREG_FF_adffs_a2_a & Mux_a1151

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF10",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a3_a,
	datab => CREG_FF_adffs_a2_a,
	datac => Mux_a1151,
	datad => Mux_a1149,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1152);

Mux_a1155_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1155 = Mux_a1154 & (PFF_WCK_a250 # !CREG_FF_adffs_a1_a) # !Mux_a1154 & CREG_FF_adffs_a1_a & OFSIN_a1

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA62",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a1154,
	datab => CREG_FF_adffs_a1_a,
	datac => OFSIN_a1,
	datad => PFF_WCK_a250,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1155);

Mux_a1148_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1148 = CREG_FF_adffs_a2_a & !CREG_FF_adffs_a3_a & (CREG_FF_adffs_a0_a $ CREG_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0048",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => CREG_FF_adffs_a2_a,
	datac => CREG_FF_adffs_a1_a,
	datad => CREG_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1148);

a_aST_GEN_aU_DSPCTRL_ai_a2_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a2 = DSP_A1_a0_a_acombout # DSP_A1_a1_a_acombout # !a_aST_GEN_aU_DSPCTRL_ai_a438 # !DSP_A1_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_A1_a1_a_acombout,
	datac => DSP_A1_a2_a_acombout,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a438,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a2);

Mux_a1153_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1153 = Mux_a1148 & (CREG_FF_adffs_a1_a & U_PCICTRL_ai_a123 # !CREG_FF_adffs_a1_a & (!a_aST_GEN_aU_DSPCTRL_ai_a2))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "80C4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a1_a,
	datab => Mux_a1148,
	datac => U_PCICTRL_ai_a123,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1153);

Mux_a1156_I : apex20ke_lcell
-- Equation(s):
-- Mux_a1156 = Mux_a1153 # !CREG_FF_adffs_a3_a & !CREG_FF_adffs_a2_a & Mux_a1155

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF10",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a3_a,
	datab => CREG_FF_adffs_a2_a,
	datac => Mux_a1155,
	datad => Mux_a1153,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a1156);

U_DPRAM_aWrite_En_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aWrite_En = U_DPRAM_astate_a15 # U_DPRAM_astate_a9

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_astate_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aWrite_En);

U_PCICTRL_aPCI_DPRA_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a0_a_areg0 = DFFE(PCI_DQ_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a0_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a0_a = DFFE(U_PCICTRL_aPCI_DPRA_a0_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a0_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a0_a);

U_DPRAM_areg_ff_b_a_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a0_a = DFFE(DSP_A1_a0_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a0_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a0_a);

U_DPRAM_aaddr_mux_a0_a_a2_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a0_a_a2 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a0_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_a_a_adffs_a0_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_a_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a0_a_a2);

U_DPRAM_areg_ff_b_a_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a1_a = DFFE(DSP_A1_a1_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a1_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a1_a);

U_DPRAM_aaddr_mux_a1_a_a5_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a1_a_a5 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a1_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a1_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_a_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a1_a_a5);

U_DPRAM_areg_ff_b_a_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a2_a = DFFE(DSP_A1_a2_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a2_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a2_a);

U_DPRAM_aaddr_mux_a2_a_a8_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a2_a_a8 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_a_adffs_a2_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_a_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_a_adffs_a2_a,
	datac => U_DPRAM_areg_ff_a_a_adffs_a2_a,
	datad => U_DPRAM_astate_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a2_a_a8);

U_DPRAM_areg_ff_b_a_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a3_a = DFFE(DSP_A1_a3_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a3_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a3_a);

U_DPRAM_aaddr_mux_a3_a_a11_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a3_a_a11 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a3_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_a_a_adffs_a3_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_a_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a3_a_a11);

U_DPRAM_areg_ff_b_a_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a4_a = DFFE(DSP_A1_a4_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a4_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a4_a);

U_DPRAM_aaddr_mux_a4_a_a14_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a4_a_a14 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a4_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a4_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_a_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a4_a_a14);

U_PCICTRL_aPCI_DPRA_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a5_a_areg0 = DFFE(PCI_DQ_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a5_a_areg0);

U_DPRAM_areg_ff_a_a_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_a_a_adffs_a5_a = DFFE(U_PCICTRL_aPCI_DPRA_a5_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_ai_a123)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aPCI_DPRA_a5_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_ai_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_a_a_adffs_a5_a);

DSP_A1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(5),
	combout => DSP_A1_a5_a_acombout);

U_DPRAM_areg_ff_b_a_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a5_a = DFFE(DSP_A1_a5_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a5_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a5_a);

U_DPRAM_aaddr_mux_a5_a_a17_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a5_a_a17 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a5_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_a_a_adffs_a5_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_b_a_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a5_a_a17);

U_DPRAM_areg_ff_b_a_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a6_a = DFFE(DSP_A1_a6_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a6_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a6_a);

U_DPRAM_aaddr_mux_a6_a_a20_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a6_a_a20 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a6_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CACA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a6_a,
	datab => U_DPRAM_areg_ff_b_a_adffs_a6_a,
	datac => U_DPRAM_astate_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a6_a_a20);

U_DPRAM_areg_ff_b_a_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a7_a = DFFE(DSP_A1_a7_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a7_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a7_a);

U_DPRAM_aaddr_mux_a7_a_a23_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a7_a_a23 = U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_b_a_adffs_a7_a) # !U_DPRAM_astate_a15 & U_DPRAM_areg_ff_a_a_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CACA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_areg_ff_a_a_adffs_a7_a,
	datab => U_DPRAM_areg_ff_b_a_adffs_a7_a,
	datac => U_DPRAM_astate_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a7_a_a23);

U_DPRAM_areg_ff_b_a_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_areg_ff_b_a_adffs_a8_a = DFFE(DSP_A_a13_a_acombout, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , a_aST_GEN_aU_DSPCTRL_ai_a96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_A_a13_a_acombout,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPCTRL_ai_a96,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_areg_ff_b_a_adffs_a8_a);

U_DPRAM_aaddr_mux_a8_a_a26_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aaddr_mux_a8_a_a26 = U_DPRAM_astate_a15 & U_DPRAM_areg_ff_b_a_adffs_a8_a # !U_DPRAM_astate_a15 & (U_DPRAM_areg_ff_a_a_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_areg_ff_b_a_adffs_a8_a,
	datac => U_DPRAM_astate_a15,
	datad => U_DPRAM_areg_ff_a_a_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DPRAM_aaddr_mux_a8_a_a26);

U_PCICTRL_aPCI_DPRA_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_DPRA_a6_a_areg0 = DFFE(PCI_DQ_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_DPRA_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_DPRA_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_DPRA_a6_a_areg0);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a0_a_a47,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a0_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a0_a);

U_PCICTRL_aPCI_ADR_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aPCI_ADR_a8_a_areg0 = DFFE(PCI_DQ_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_aPCI_ADR_a8_a_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aPCI_ADR_a8_a_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aPCI_ADR_a8_a_areg0);

PCI_DQ_MOUT_a15_a_a906_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a15_a_a906 = !U_PCICTRL_aPCI_ADR_a4_a_areg0 & (!U_PCICTRL_aPCI_ADR_a5_a_areg0 & !U_PCICTRL_ai_a699)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a5_a_areg0,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a15_a_a906);

PCI_DQ_MOUT_a15_a_a947_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a15_a_a947 = !U_PCICTRL_aPCI_ADR_a7_a_areg0 & !U_PCICTRL_aPCI_ADR_a8_a_areg0 & !U_PCICTRL_aPCI_ADR_a6_a_areg0 & PCI_DQ_MOUT_a15_a_a906
-- PCI_DQ_MOUT_a15_a_a951 = !U_PCICTRL_aPCI_ADR_a7_a_areg0 & !U_PCICTRL_aPCI_ADR_a8_a_areg0 & !U_PCICTRL_aPCI_ADR_a6_a_areg0 & PCI_DQ_MOUT_a15_a_a906

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a7_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a8_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a6_a_areg0,
	datad => PCI_DQ_MOUT_a15_a_a906,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a15_a_a947,
	cascout => PCI_DQ_MOUT_a15_a_a951);

U_PCICTRL_aWE_DCD_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a12_a_areg0 = DFFE(U_PCICTRL_aPCI_ADR_a2_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_ai_a559, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_ai_a559,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a12_a_areg0);

DSP_REG_FF_1_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a0_a = DFFE(U_PCICTRL_aData_FF_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aData_FF_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a0_a);

PCI_DQ_MUX_a0_a_a19320_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19320 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (!PFF_EF_a0_a_acombout) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "08A8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => DSP_REG_FF_1_adffs_a0_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => PFF_EF_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19320);

PCI_DQ_MUX_a0_a_a19321_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19321 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00E2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a0_a,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19321);

U_PCICTRL_aWE_DCD_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a1_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a3_a_areg0 & (!U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_ai_a577), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_PCICTRL_ai_a577,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a1_a_areg0);

TREG_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a0_a = DFFE(U_PCICTRL_aData_FF_adffs_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aData_FF_adffs_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a0_a);

PCI_DQ_MUX_a0_a_a19322_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19322 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & FPGA_BOOT_FF_adffs_a0_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & (FPGA_CD_acombout)) # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (!U_PCICTRL_aPCI_ADR_a0_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88F3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_BOOT_FF_adffs_a0_a,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => FPGA_CD_acombout,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19322);

PCI_DQ_MUX_a0_a_a19323_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19323 = PCI_DQ_MUX_a0_a_a19322 # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & TREG_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => TREG_FF_adffs_a0_a,
	datad => PCI_DQ_MUX_a0_a_a19322,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19323);

PCI_DQ_MUX_a0_a_a19324_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19324 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a0_a_a19321 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a0_a_a19323))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E5E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a0_a_a19321,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a0_a_a19323,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19324);

PCI_DQ_MUX_a0_a_a19326_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19326 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a0_a_a19324 & PCI_DQ_MUX_a0_a_a19325 # !PCI_DQ_MUX_a0_a_a19324 & (PCI_DQ_MUX_a0_a_a19320)) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a0_a_a19324)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a0_a_a19325,
	datab => PCI_DQ_MUX_a0_a_a19320,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => PCI_DQ_MUX_a0_a_a19324,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19326);

PCI_DQ_MOUT_a0_a_a915_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a0_a_a915 = U_DPRAM_adpram_ram_a_asram_aq_a0_a & (U_PCICTRL_ai_a699 # PCI_DQ_MOUT_a15_a_a947 & PCI_DQ_MUX_a0_a_a19326) # !U_DPRAM_adpram_ram_a_asram_aq_a0_a & (PCI_DQ_MOUT_a15_a_a947 & PCI_DQ_MUX_a0_a_a19326)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a0_a,
	datab => U_PCICTRL_ai_a699,
	datac => PCI_DQ_MOUT_a15_a_a947,
	datad => PCI_DQ_MUX_a0_a_a19326,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a0_a_a915);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a1_a_a44,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a1_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a1_a);

U_PCICTRL_aWE_DCD_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a11_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_ai_a559, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_ai_a559,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a11_a_areg0);

DSP_REG_FF_0_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a1_a = DFFE(U_PCICTRL_aData_FF_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a1_a);

PCI_DQ_MUX_a1_a_a19332_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19332 = DSP_REG_FF_0_adffs_a1_a & !U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_0_adffs_a1_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19332);

DSP_REG_FF_1_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a1_a = DFFE(U_PCICTRL_aData_FF_adffs_a1_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a1_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a1_a);

PFF_EF_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_EF(1),
	combout => PFF_EF_a1_a_acombout);

PCI_DQ_MUX_a1_a_a19327_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19327 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (!PFF_EF_a1_a_acombout) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "40E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => DSP_REG_FF_1_adffs_a1_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PFF_EF_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19327);

PCI_DQ_MUX_a1_a_a19328_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19328 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3202",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a1_a,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19328);

PCI_DQ_MUX_a1_a_a19329_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19329 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & FPGA_BOOT_FF_adffs_a1_a & U_PCICTRL_aPCI_ADR_a2_a_areg0 # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & (FPGA_NS_acombout # !U_PCICTRL_aPCI_ADR_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B383",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_BOOT_FF_adffs_a1_a,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => FPGA_NS_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19329);

PCI_DQ_MUX_a1_a_a19330_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19330 = PCI_DQ_MUX_a1_a_a19329 # TREG_FF_adffs_a1_a & !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TREG_FF_adffs_a1_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PCI_DQ_MUX_a1_a_a19329,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19330);

PCI_DQ_MUX_a1_a_a19331_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19331 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a1_a_a19328 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a1_a_a19330))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E5E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a1_a_a19328,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a1_a_a19330,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19331);

PCI_DQ_MUX_a1_a_a19333_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a1_a_a19333 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a1_a_a19331 & PCI_DQ_MUX_a1_a_a19332 # !PCI_DQ_MUX_a1_a_a19331 & (PCI_DQ_MUX_a1_a_a19327)) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a1_a_a19331)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DDA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a1_a_a19332,
	datac => PCI_DQ_MUX_a1_a_a19327,
	datad => PCI_DQ_MUX_a1_a_a19331,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a1_a_a19333);

PCI_DQ_MOUT_a1_a_a916_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a1_a_a916 = U_PCICTRL_ai_a699 & (U_DPRAM_adpram_ram_a_asram_aq_a1_a # PCI_DQ_MOUT_a15_a_a947 & PCI_DQ_MUX_a1_a_a19333) # !U_PCICTRL_ai_a699 & (PCI_DQ_MOUT_a15_a_a947 & PCI_DQ_MUX_a1_a_a19333)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_ai_a699,
	datab => U_DPRAM_adpram_ram_a_asram_aq_a1_a,
	datac => PCI_DQ_MOUT_a15_a_a947,
	datad => PCI_DQ_MUX_a1_a_a19333,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a1_a_a916);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a2_a_a41,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a2_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a2_a);

DSP_REG_FF_0_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a2_a = DFFE(U_PCICTRL_aData_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a2_a);

PCI_DQ_MUX_a2_a_a19339_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19339 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_0_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_0_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19339);

DSP_REG_FF_1_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a2_a = DFFE(U_PCICTRL_aData_FF_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aData_FF_adffs_a2_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a2_a);

PCI_DQ_MUX_a2_a_a19334_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19334 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (!PFF_FF_a0_a_acombout) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "08A8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => DSP_REG_FF_1_adffs_a2_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => PFF_FF_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19334);

PCI_DQ_MUX_a2_a_a19340_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a2_a_a19340 = PCI_DQ_MUX_a2_a_a19338 & (PCI_DQ_MUX_a2_a_a19339 # !U_PCICTRL_aPCI_ADR_a3_a_areg0) # !PCI_DQ_MUX_a2_a_a19338 & (U_PCICTRL_aPCI_ADR_a3_a_areg0 & PCI_DQ_MUX_a2_a_a19334)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DA8A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a2_a_a19338,
	datab => PCI_DQ_MUX_a2_a_a19339,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => PCI_DQ_MUX_a2_a_a19334,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a2_a_a19340);

PCI_DQ_MOUT_a2_a_a917_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a2_a_a917 = U_DPRAM_adpram_ram_a_asram_aq_a2_a & (U_PCICTRL_ai_a699 # PCI_DQ_MUX_a2_a_a19340 & PCI_DQ_MOUT_a15_a_a947) # !U_DPRAM_adpram_ram_a_asram_aq_a2_a & PCI_DQ_MUX_a2_a_a19340 & (PCI_DQ_MOUT_a15_a_a947)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a2_a,
	datab => PCI_DQ_MUX_a2_a_a19340,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a2_a_a917);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a3_a_a38,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a3_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a3_a);

PCI_DQ_MUX_a3_a_a19342_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19342 = !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0E04",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => CREG_FF_adffs_a3_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19342);

FPGA_BOOT_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a3_a = DFFE(U_PCICTRL_aData_FF_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a3_a);

TREG_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a3_a = DFFE(U_PCICTRL_aData_FF_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a3_a);

PCI_DQ_MUX_a3_a_a19343_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19343 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a3_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => FPGA_BOOT_FF_adffs_a3_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => TREG_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19343);

PCI_DQ_MUX_a3_a_a19344_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19344 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (PCI_DQ_MUX_a3_a_a19343) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a6_a & U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => CREG_FF_adffs_a6_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PCI_DQ_MUX_a3_a_a19343,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19344);

PCI_DQ_MUX_a3_a_a19345_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19345 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a3_a_a19342 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a3_a_a19344))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E5E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a3_a_a19342,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a3_a_a19344,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19345);

PCI_DQ_MUX_a3_a_a19346_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19346 = DSP_REG_FF_0_adffs_a3_a & !U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2020",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_0_adffs_a3_a,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19346);

PCI_DQ_MUX_a3_a_a19341_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19341 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (U_PCICTRL_aPCI_ADR_a0_a_areg0 & (!PFF_FF_a1_a_acombout) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "08C8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_1_adffs_a3_a,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => PFF_FF_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19341);

PCI_DQ_MUX_a3_a_a19347_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a3_a_a19347 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a3_a_a19345 & PCI_DQ_MUX_a3_a_a19346 # !PCI_DQ_MUX_a3_a_a19345 & (PCI_DQ_MUX_a3_a_a19341)) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & PCI_DQ_MUX_a3_a_a19345

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E6C4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a3_a_a19345,
	datac => PCI_DQ_MUX_a3_a_a19346,
	datad => PCI_DQ_MUX_a3_a_a19341,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a3_a_a19347);

PCI_DQ_MOUT_a3_a_a918_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a3_a_a918 = U_DPRAM_adpram_ram_a_asram_aq_a3_a & (U_PCICTRL_ai_a699 # PCI_DQ_MUX_a3_a_a19347 & PCI_DQ_MOUT_a15_a_a947) # !U_DPRAM_adpram_ram_a_asram_aq_a3_a & PCI_DQ_MUX_a3_a_a19347 & (PCI_DQ_MOUT_a15_a_a947)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a3_a,
	datab => PCI_DQ_MUX_a3_a_a19347,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a3_a_a918);

a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0 = DFFE(CREG_FF_adffs_a7_a & (!a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0A0A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a7_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0);

a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH = DFFE(U_PCICTRL_aWE_DCD_a11_a_areg0 # a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH & !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F2F2",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => U_PCICTRL_aWE_DCD_a11_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH);

a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH # a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_ai_a277 # !a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a), 
-- GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_ai_a277,
	datab => a_aST_GEN_aU_DSPBOOT_aSHIFT_CNT_rtl_1_awysi_counter_asload_path_a0_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_BYTE_LATCH,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0);

FPGA_BOOT_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a4_a = DFFE(U_PCICTRL_aData_FF_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a4_a);

TREG_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a4_a = DFFE(U_PCICTRL_aData_FF_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a4_a);

PCI_DQ_MUX_a4_a_a19350_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19350 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a4_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => FPGA_BOOT_FF_adffs_a4_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => TREG_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19350);

PCI_DQ_MUX_a4_a_a19351_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19351 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (PCI_DQ_MUX_a4_a_a19350) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_aPCI_ADR_a2_a_areg0 & a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => PCI_DQ_MUX_a4_a_a19350,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19351);

PCI_DQ_MUX_a4_a_a19352_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19352 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a4_a_a19349 # U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a4_a_a19351 & !U_PCICTRL_aPCI_ADR_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0AC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a4_a_a19349,
	datab => PCI_DQ_MUX_a4_a_a19351,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19352);

PCI_DQ_MUX_a4_a_a19348_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19348 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFE2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a4_a,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19348);

PCI_DQ_MUX_a4_a_a19354_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a4_a_a19354 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a4_a_a19352 & PCI_DQ_MUX_a4_a_a19353 # !PCI_DQ_MUX_a4_a_a19352 & (PCI_DQ_MUX_a4_a_a19348)) # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a4_a_a19352)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BCB0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a4_a_a19353,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => PCI_DQ_MUX_a4_a_a19352,
	datad => PCI_DQ_MUX_a4_a_a19348,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a4_a_a19354);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a4_a_a35,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a4_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a4_a);

PCI_DQ_MOUT_a4_a_a945_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a4_a_a952 = U_PCICTRL_ai_a699 # !U_PCICTRL_aPCI_ADR_a8_a_areg0 & !U_PCICTRL_aPCI_ADR_a6_a_areg0 & !U_PCICTRL_aPCI_ADR_a4_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF01",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a8_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a6_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a4_a_areg0,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a4_a_a945,
	cascout => PCI_DQ_MOUT_a4_a_a952);

PCI_DQ_MOUT_a4_a_a948_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a4_a_a948 = (U_PCICTRL_ai_a699 # !U_PCICTRL_aPCI_ADR_a7_a_areg0 & !U_PCICTRL_aPCI_ADR_a5_a_areg0) & CASCADE(PCI_DQ_MOUT_a4_a_a952)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF03",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a7_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a5_a_areg0,
	datad => U_PCICTRL_ai_a699,
	cascin => PCI_DQ_MOUT_a4_a_a952,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a4_a_a948);

PCI_DQ_MOUT_a4_a_a920_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a4_a_a920 = U_PCICTRL_ai_a699 & (U_DPRAM_adpram_ram_a_asram_aq_a4_a) # !U_PCICTRL_ai_a699 & PCI_DQ_MUX_a4_a_a19354 # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a4_a_a19354,
	datab => U_DPRAM_adpram_ram_a_asram_aq_a4_a,
	datac => PCI_DQ_MOUT_a4_a_a948,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a4_a_a920);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a5_a_a32,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a5_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a5_a);

DSP_REG_FF_1_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a5_a = DFFE(U_PCICTRL_aData_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a5_a);

PCI_DQ_MUX_a5_a_a19356_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19356 = !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a5_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F33",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_1_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19356);

U_PCICTRL_aData_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a7_a = DFFE(PCI_DQ_a7_a_a8, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a7_a_a8,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a7_a);

CREG_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- CREG_FF_adffs_a7_a = DFFE(U_PCICTRL_aData_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => CREG_FF_adffs_a7_a);

FPGA_BOOT_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a5_a = DFFE(U_PCICTRL_aData_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a5_a);

TREG_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a5_a = DFFE(U_PCICTRL_aData_FF_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a5_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a5_a);

PCI_DQ_MUX_a5_a_a19357_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19357 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a5_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => FPGA_BOOT_FF_adffs_a5_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => TREG_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19357);

PCI_DQ_MUX_a5_a_a19358_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19358 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (PCI_DQ_MUX_a5_a_a19357) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & CREG_FF_adffs_a7_a & U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => CREG_FF_adffs_a7_a,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PCI_DQ_MUX_a5_a_a19357,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19358);

PCI_DQ_MUX_a5_a_a19359_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19359 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a5_a_a19356 # U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (!U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a5_a_a19358)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ADA8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a5_a_a19356,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a5_a_a19358,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19359);

PCI_DQ_MUX_a5_a_a19355_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19355 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & (CREG_FF_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EEFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	datac => CREG_FF_adffs_a5_a,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19355);

PCI_DQ_MUX_a5_a_a19361_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a5_a_a19361 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a5_a_a19359 & PCI_DQ_MUX_a5_a_a19360 # !PCI_DQ_MUX_a5_a_a19359 & (PCI_DQ_MUX_a5_a_a19355)) # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a5_a_a19359)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BCB0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a5_a_a19360,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => PCI_DQ_MUX_a5_a_a19359,
	datad => PCI_DQ_MUX_a5_a_a19355,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a5_a_a19361);

PCI_DQ_MOUT_a5_a_a922_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a5_a_a922 = U_PCICTRL_ai_a699 & U_DPRAM_adpram_ram_a_asram_aq_a5_a # !U_PCICTRL_ai_a699 & (PCI_DQ_MUX_a5_a_a19361) # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B8FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a5_a,
	datab => U_PCICTRL_ai_a699,
	datac => PCI_DQ_MUX_a5_a_a19361,
	datad => PCI_DQ_MOUT_a4_a_a948,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a5_a_a922);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a6_a_a29,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a6_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a6_a);

FPGA_BOOT_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a6_a = DFFE(U_PCICTRL_aData_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a6_a);

TREG_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a6_a = DFFE(U_PCICTRL_aData_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a6_a);

PCI_DQ_MUX_a6_a_a19364_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19364 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a6_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => FPGA_BOOT_FF_adffs_a6_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => TREG_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19364);

PCI_DQ_MUX_a6_a_a19365_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19365 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a6_a_a19363 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a6_a_a19364))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E3E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a6_a_a19363,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a6_a_a19364,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19365);

DSP_REG_FF_0_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a6_a = DFFE(U_PCICTRL_aData_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a6_a);

PCI_DQ_MUX_a6_a_a19366_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19366 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # DSP_REG_FF_0_adffs_a6_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_0_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19366);

PCI_DQ_MUX_a6_a_a19367_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a6_a_a19367 = PCI_DQ_MUX_a6_a_a19365 & (PCI_DQ_MUX_a6_a_a19366 # !U_PCICTRL_aPCI_ADR_a3_a_areg0) # !PCI_DQ_MUX_a6_a_a19365 & PCI_DQ_MUX_a6_a_a19362 & (U_PCICTRL_aPCI_ADR_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E2CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a6_a_a19362,
	datab => PCI_DQ_MUX_a6_a_a19365,
	datac => PCI_DQ_MUX_a6_a_a19366,
	datad => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a6_a_a19367);

PCI_DQ_MOUT_a6_a_a924_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a6_a_a924 = U_PCICTRL_ai_a699 & U_DPRAM_adpram_ram_a_asram_aq_a6_a # !U_PCICTRL_ai_a699 & (PCI_DQ_MUX_a6_a_a19367) # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B8FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a6_a,
	datab => U_PCICTRL_ai_a699,
	datac => PCI_DQ_MUX_a6_a_a19367,
	datad => PCI_DQ_MOUT_a4_a_a948,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a6_a_a924);

DSP_REG_FF_0_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a7_a = DFFE(U_PCICTRL_aData_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a7_a);

PCI_DQ_MUX_a7_a_a19368_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19368 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & DSP_REG_FF_0_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => DSP_REG_FF_0_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19368);

FPGA_BOOT_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a7_a = DFFE(U_PCICTRL_aData_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a7_a);

PCI_DQ_MUX_a7_a_a19369_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19369 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0 & FPGA_BOOT_FF_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => FPGA_BOOT_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19369);

PCI_DQ_MUX_a7_a_a19371_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19371 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & PCI_DQ_MUX_a7_a_a19370 # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & (CREG_FF_adffs_a7_a & U_PCICTRL_aPCI_ADR_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ACA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a7_a_a19370,
	datab => CREG_FF_adffs_a7_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19371);

PCI_DQ_MUX_a7_a_a19372_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19372 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_aPCI_ADR_a2_a_areg0 # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & PCI_DQ_MUX_a7_a_a19369 # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (PCI_DQ_MUX_a7_a_a19371))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D9C8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => PCI_DQ_MUX_a7_a_a19369,
	datad => PCI_DQ_MUX_a7_a_a19371,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19372);

PCI_DQ_MUX_a7_a_a19373_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19373 = DSP_REG_FF_1_adffs_a7_a & (!U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_1_adffs_a7_a,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19373);

PCI_DQ_MUX_a7_a_a19374_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a7_a_a19374 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a7_a_a19372 & (PCI_DQ_MUX_a7_a_a19373) # !PCI_DQ_MUX_a7_a_a19372 & PCI_DQ_MUX_a7_a_a19368) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a7_a_a19372)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F858",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a7_a_a19368,
	datac => PCI_DQ_MUX_a7_a_a19372,
	datad => PCI_DQ_MUX_a7_a_a19373,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a7_a_a19374);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a7_a_a26,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a7_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a7_a);

PCI_DQ_MOUT_a7_a_a925_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a7_a_a925 = PCI_DQ_MUX_a7_a_a19374 & (PCI_DQ_MOUT_a15_a_a947 # U_DPRAM_adpram_ram_a_asram_aq_a7_a & U_PCICTRL_ai_a699) # !PCI_DQ_MUX_a7_a_a19374 & U_DPRAM_adpram_ram_a_asram_aq_a7_a & U_PCICTRL_ai_a699

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a7_a_a19374,
	datab => U_DPRAM_adpram_ram_a_asram_aq_a7_a,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a7_a_a925);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a8_a_a23,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a8_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a8_a);

U_PCICTRL_aData_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a8_a = DFFE(PCI_DQ_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a8_a);

DSP_REG_FF_0_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a8_a = DFFE(U_PCICTRL_aData_FF_adffs_a8_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a8_a);

PCI_DQ_MUX_a8_a_a19379_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19379 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # DSP_REG_FF_0_adffs_a8_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => DSP_REG_FF_0_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19379);

DSP_REG_FF_1_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a8_a = DFFE(U_PCICTRL_aData_FF_adffs_a8_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a8_a);

PCI_DQ_MUX_a8_a_a19376_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19376 = !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a8_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7575",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => DSP_REG_FF_1_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19376);

PCI_DQ_MUX_a8_a_a19378_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19378 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 # PCI_DQ_MUX_a8_a_a19376) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & PCI_DQ_MUX_a8_a_a19377 & !U_PCICTRL_aPCI_ADR_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CEC2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a8_a_a19377,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a8_a_a19376,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19378);

PCI_DQ_MUX_a8_a_a19380_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a8_a_a19380 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a8_a_a19378 & (PCI_DQ_MUX_a8_a_a19379) # !PCI_DQ_MUX_a8_a_a19378 & PCI_DQ_MUX_a8_a_a19375) # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a8_a_a19378)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F388",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a8_a_a19375,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => PCI_DQ_MUX_a8_a_a19379,
	datad => PCI_DQ_MUX_a8_a_a19378,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a8_a_a19380);

PCI_DQ_MOUT_a8_a_a927_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a8_a_a927 = U_PCICTRL_ai_a699 & U_DPRAM_adpram_ram_a_asram_aq_a8_a # !U_PCICTRL_ai_a699 & (PCI_DQ_MUX_a8_a_a19380) # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ACFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a8_a,
	datab => PCI_DQ_MUX_a8_a_a19380,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a4_a_a948,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a8_a_a927);

U_PCICTRL_aData_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a9_a = DFFE(PCI_DQ_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a9_a);

FPGA_BOOT_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a9_a = DFFE(U_PCICTRL_aData_FF_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a9_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a9_a);

PCI_DQ_MUX_a9_a_a19385_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19385 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & FPGA_BOOT_FF_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => FPGA_BOOT_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19385);

DSP_REG_FF_1_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a9_a = DFFE(U_PCICTRL_aData_FF_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a9_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a9_a);

PCI_DQ_MUX_a9_a_a19387_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19387 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (PCI_DQ_MUX_a9_a_a19385 # PCI_DQ_MUX_a9_a_a19386 & DSP_REG_FF_1_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C8C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a9_a_a19386,
	datab => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => PCI_DQ_MUX_a9_a_a19385,
	datad => DSP_REG_FF_1_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19387);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a9_a_a20,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a9_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a9_a);

PCI_DQ_MOUT_a9_a_a928_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a9_a_a928 = U_DPRAM_adpram_ram_a_asram_aq_a9_a & U_PCICTRL_ai_a699

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_adpram_ram_a_asram_aq_a9_a,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a9_a_a928);

PCI_DQ_MUX_a9_a_a19383_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19383 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19383);

TREG_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a9_a = DFFE(U_PCICTRL_aData_FF_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a9_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a9_a);

PCI_DQ_MUX_a0_a_a19381_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a0_a_a19381 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (!U_PCICTRL_aPCI_ADR_a2_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a0_a_a19381);

PCI_DQ_MUX_a9_a_a19384_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19384 = PCI_DQ_MUX_a0_a_a19381 & (PCI_DQ_MUX_a9_a_a19382 # PCI_DQ_MUX_a9_a_a19383 & TREG_FF_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a9_a_a19382,
	datab => PCI_DQ_MUX_a9_a_a19383,
	datac => TREG_FF_adffs_a9_a,
	datad => PCI_DQ_MUX_a0_a_a19381,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19384);

PCI_DQ_MOUT_a9_a_a929_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a9_a_a929 = PCI_DQ_MOUT_a9_a_a928 # PCI_DQ_MOUT_a15_a_a947 & (PCI_DQ_MUX_a9_a_a19387 # PCI_DQ_MUX_a9_a_a19384)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FECC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a9_a_a19387,
	datab => PCI_DQ_MOUT_a9_a_a928,
	datac => PCI_DQ_MUX_a9_a_a19384,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a9_a_a929);

PCI_DQ_MUX_a10_a_a19390_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a10_a_a19390 = FPGA_BOOT_FF_adffs_a10_a & !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FPGA_BOOT_FF_adffs_a10_a,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a10_a_a19390);

PCI_DQ_MUX_a9_a_a19386_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a9_a_a19386 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_PCICTRL_aPCI_ADR_a3_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a9_a_a19386);

PCI_DQ_MUX_a10_a_a19391_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a10_a_a19391 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (PCI_DQ_MUX_a10_a_a19390 # DSP_REG_FF_1_adffs_a10_a & PCI_DQ_MUX_a9_a_a19386)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_1_adffs_a10_a,
	datab => PCI_DQ_MUX_a10_a_a19390,
	datac => PCI_DQ_MUX_a9_a_a19386,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a10_a_a19391);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a10_a_a17,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a10_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a10_a);

PCI_DQ_MOUT_a10_a_a930_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a10_a_a930 = U_DPRAM_adpram_ram_a_asram_aq_a10_a & U_PCICTRL_ai_a699

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => U_DPRAM_adpram_ram_a_asram_aq_a10_a,
	datad => U_PCICTRL_ai_a699,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a10_a_a930);

U_PCICTRL_aData_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a10_a = DFFE(PCI_DQ_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DQ_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a10_a);

DSP_REG_FF_0_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a10_a = DFFE(U_PCICTRL_aData_FF_adffs_a10_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a10_a);

PCI_DQ_MUX_a10_a_a19388_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a10_a_a19388 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (U_PCICTRL_aPCI_ADR_a3_a_areg0 & (DSP_REG_FF_0_adffs_a10_a) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	datac => DSP_REG_FF_0_adffs_a10_a,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a10_a_a19388);

PCI_DQ_MUX_a10_a_a19389_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a10_a_a19389 = PCI_DQ_MUX_a0_a_a19381 & (PCI_DQ_MUX_a10_a_a19388 # TREG_FF_adffs_a10_a & PCI_DQ_MUX_a9_a_a19383)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TREG_FF_adffs_a10_a,
	datab => PCI_DQ_MUX_a9_a_a19383,
	datac => PCI_DQ_MUX_a10_a_a19388,
	datad => PCI_DQ_MUX_a0_a_a19381,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a10_a_a19389);

PCI_DQ_MOUT_a10_a_a931_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a10_a_a931 = PCI_DQ_MOUT_a10_a_a930 # PCI_DQ_MOUT_a15_a_a947 & (PCI_DQ_MUX_a10_a_a19391 # PCI_DQ_MUX_a10_a_a19389)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FECC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a10_a_a19391,
	datab => PCI_DQ_MOUT_a10_a_a930,
	datac => PCI_DQ_MUX_a10_a_a19389,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a10_a_a931);

U_PCICTRL_aData_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a11_a = DFFE(PCI_DQ_a11_a_a4, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a11_a_a4,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a11_a);

DSP_REG_FF_0_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a11_a = DFFE(U_PCICTRL_aData_FF_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a11_a);

PCI_DQ_MUX_a11_a_a19396_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19396 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # DSP_REG_FF_0_adffs_a11_a # !U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_0_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19396);

PCI_DQ_MUX_a11_a_a19392_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19392 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a & U_PCICTRL_aPCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAEA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19392);

PCI_DQ_MUX_a11_a_a19397_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a11_a_a19397 = PCI_DQ_MUX_a11_a_a19395 & (PCI_DQ_MUX_a11_a_a19396 # !U_PCICTRL_aPCI_ADR_a1_a_areg0) # !PCI_DQ_MUX_a11_a_a19395 & (PCI_DQ_MUX_a11_a_a19392 & U_PCICTRL_aPCI_ADR_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D8AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a11_a_a19395,
	datab => PCI_DQ_MUX_a11_a_a19396,
	datac => PCI_DQ_MUX_a11_a_a19392,
	datad => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a11_a_a19397);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a11_a_a14,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a11_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a11_a);

PCI_DQ_MOUT_a11_a_a933_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a11_a_a933 = U_PCICTRL_ai_a699 & (U_DPRAM_adpram_ram_a_asram_aq_a11_a) # !U_PCICTRL_ai_a699 & PCI_DQ_MUX_a11_a_a19397 # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E2FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a11_a_a19397,
	datab => U_PCICTRL_ai_a699,
	datac => U_DPRAM_adpram_ram_a_asram_aq_a11_a,
	datad => PCI_DQ_MOUT_a4_a_a948,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a11_a_a933);

U_PCICTRL_aData_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a12_a = DFFE(PCI_DQ_a12_a_a3, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a12_a_a3,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a12_a);

DSP_REG_FF_1_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a12_a = DFFE(U_PCICTRL_aData_FF_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a12_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a12_a);

PCI_DQ_MUX_a12_a_a19398_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19398 = !U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_1_adffs_a12_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F55",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => DSP_REG_FF_1_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19398);

PCI_DQ_MUX_a12_a_a19399_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19399 = U_PCICTRL_aPCI_ADR_a2_a_areg0 # U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19399);

FPGA_BOOT_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- FPGA_BOOT_FF_adffs_a12_a = DFFE(U_PCICTRL_aData_FF_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a12_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FPGA_BOOT_FF_adffs_a12_a);

TREG_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a12_a = DFFE(U_PCICTRL_aData_FF_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U_PCICTRL_aData_FF_adffs_a12_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a12_a);

PCI_DQ_MUX_a12_a_a19400_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19400 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & FPGA_BOOT_FF_adffs_a12_a # !U_PCICTRL_aPCI_ADR_a2_a_areg0 & (TREG_FF_adffs_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => FPGA_BOOT_FF_adffs_a12_a,
	datac => TREG_FF_adffs_a12_a,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19400);

PCI_DQ_MUX_a12_a_a19401_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19401 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_PCICTRL_aPCI_ADR_a1_a_areg0 & PCI_DQ_MUX_a12_a_a19399 # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (PCI_DQ_MUX_a12_a_a19400))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E5E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datab => PCI_DQ_MUX_a12_a_a19399,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datad => PCI_DQ_MUX_a12_a_a19400,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19401);

PCI_DQ_MUX_a12_a_a19403_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a12_a_a19403 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a12_a_a19401 & PCI_DQ_MUX_a12_a_a19402 # !PCI_DQ_MUX_a12_a_a19401 & (PCI_DQ_MUX_a12_a_a19398)) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a12_a_a19401)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a12_a_a19402,
	datab => PCI_DQ_MUX_a12_a_a19398,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => PCI_DQ_MUX_a12_a_a19401,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a12_a_a19403);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a12_a_a11,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a12_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a12_a);

PCI_DQ_MOUT_a12_a_a935_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a12_a_a935 = U_PCICTRL_ai_a699 & (U_DPRAM_adpram_ram_a_asram_aq_a12_a) # !U_PCICTRL_ai_a699 & PCI_DQ_MUX_a12_a_a19403 # !PCI_DQ_MOUT_a4_a_a948

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E2FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a12_a_a19403,
	datab => U_PCICTRL_ai_a699,
	datac => U_DPRAM_adpram_ram_a_asram_aq_a12_a,
	datad => PCI_DQ_MOUT_a4_a_a948,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a12_a_a935);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a13_a_a8,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a13_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a13_a);

U_PCICTRL_aData_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a13_a = DFFE(PCI_DQ_a13_a_a2, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a13_a_a2,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a13_a);

TREG_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a13_a = DFFE(U_PCICTRL_aData_FF_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a13_a);

PCI_DQ_MUX_a13_a_a19420_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a13_a_a19420 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a3_a_areg0 & TREG_FF_adffs_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datad => TREG_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a13_a_a19420);

DSP_REG_FF_0_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a13_a = DFFE(U_PCICTRL_aData_FF_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a13_a);

PCI_DQ_MUX_a13_a_a19421_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a13_a_a19421 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & (U_PCICTRL_aPCI_ADR_a3_a_areg0 & DSP_REG_FF_0_adffs_a13_a # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A280",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => DSP_REG_FF_0_adffs_a13_a,
	datad => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a13_a_a19421);

DSP_REG_FF_1_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a13_a = DFFE(U_PCICTRL_aData_FF_adffs_a13_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a13_a);

PCI_DQ_MUX_a13_a_a19419_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a13_a_a19419 = U_PCICTRL_aPCI_ADR_a2_a_areg0 & (PCI_DQ_MUX_a13_a_a19418 # DSP_REG_FF_1_adffs_a13_a & PCI_DQ_MUX_a9_a_a19386)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a13_a_a19418,
	datab => DSP_REG_FF_1_adffs_a13_a,
	datac => PCI_DQ_MUX_a9_a_a19386,
	datad => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a13_a_a19419);

PCI_DQ_MOUT_a13_a_a949_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a13_a_a949 = (PCI_DQ_MUX_a13_a_a19419 # PCI_DQ_MUX_a0_a_a19381 & (PCI_DQ_MUX_a13_a_a19420 # PCI_DQ_MUX_a13_a_a19421)) & CASCADE(PCI_DQ_MOUT_a15_a_a951)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a0_a_a19381,
	datab => PCI_DQ_MUX_a13_a_a19420,
	datac => PCI_DQ_MUX_a13_a_a19421,
	datad => PCI_DQ_MUX_a13_a_a19419,
	cascin => PCI_DQ_MOUT_a15_a_a951,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a13_a_a949);

PCI_DQ_MOUT_a13_a_a937_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a13_a_a937 = PCI_DQ_MOUT_a13_a_a949 # U_PCICTRL_ai_a699 & U_DPRAM_adpram_ram_a_asram_aq_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_ai_a699,
	datac => U_DPRAM_adpram_ram_a_asram_aq_a13_a,
	datad => PCI_DQ_MOUT_a13_a_a949,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a13_a_a937);

U_PCICTRL_aData_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a14_a = DFFE(PCI_DQ_a14_a_a1, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a14_a_a1,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a14_a);

TREG_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- TREG_FF_adffs_a14_a = DFFE(U_PCICTRL_aData_FF_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a14_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TREG_FF_adffs_a14_a);

PCI_DQ_MUX_a14_a_a19405_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19405 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a # !U_PCICTRL_aPCI_ADR_a1_a_areg0 & (TREG_FF_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	datad => TREG_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19405);

DSP_REG_FF_0_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a14_a = DFFE(U_PCICTRL_aData_FF_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a14_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a14_a);

PCI_DQ_MUX_a14_a_a19406_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19406 = U_PCICTRL_aPCI_ADR_a3_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & (DSP_REG_FF_0_adffs_a14_a) # !U_PCICTRL_aPCI_ADR_a3_a_areg0 & (PCI_DQ_MUX_a14_a_a19405)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B830",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => PCI_DQ_MUX_a14_a_a19405,
	datad => DSP_REG_FF_0_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19406);

PCI_DQ_MUX_a14_a_a19407_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19407 = U_PCICTRL_aPCI_ADR_a0_a_areg0 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 # PCI_DQ_MUX_a14_a_a19406) # !U_PCICTRL_aPCI_ADR_a0_a_areg0 & PCI_DQ_MUX_a9_a_a19383 & !U_PCICTRL_aPCI_ADR_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AEA4",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datab => PCI_DQ_MUX_a9_a_a19383,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PCI_DQ_MUX_a14_a_a19406,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19407);

DSP_REG_FF_1_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a14_a = DFFE(U_PCICTRL_aData_FF_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a14_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a14_a);

PCI_DQ_MUX_a14_a_a19404_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19404 = !U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_PCICTRL_aPCI_ADR_a3_a_areg0 & DSP_REG_FF_1_adffs_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	datac => DSP_REG_FF_1_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19404);

PCI_DQ_MUX_a14_a_a19409_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a14_a_a19409 = PCI_DQ_MUX_a14_a_a19407 & (PCI_DQ_MUX_a14_a_a19408 # !U_PCICTRL_aPCI_ADR_a2_a_areg0) # !PCI_DQ_MUX_a14_a_a19407 & (U_PCICTRL_aPCI_ADR_a2_a_areg0 & PCI_DQ_MUX_a14_a_a19404)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BC8C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a14_a_a19408,
	datab => PCI_DQ_MUX_a14_a_a19407,
	datac => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datad => PCI_DQ_MUX_a14_a_a19404,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a14_a_a19409);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a14_a_a5,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a14_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a14_a);

PCI_DQ_MOUT_a14_a_a938_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a14_a_a938 = PCI_DQ_MUX_a14_a_a19409 & (PCI_DQ_MOUT_a15_a_a947 # U_DPRAM_adpram_ram_a_asram_aq_a14_a & U_PCICTRL_ai_a699) # !PCI_DQ_MUX_a14_a_a19409 & U_DPRAM_adpram_ram_a_asram_aq_a14_a & U_PCICTRL_ai_a699

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a14_a_a19409,
	datab => U_DPRAM_adpram_ram_a_asram_aq_a14_a,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a14_a_a938);

U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_a_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a15_a_a2,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_a_asram_asegment_a0_a_a15_a_modesel,
	dataout => U_DPRAM_adpram_ram_a_asram_aq_a15_a);

U_PCICTRL_aData_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aData_FF_adffs_a15_a = DFFE(PCI_DQ_a15_a_a0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , U_PCICTRL_adata_en_a4)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DQ_a15_a_a0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_adata_en_a4,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aData_FF_adffs_a15_a);

DSP_REG_FF_0_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_0_adffs_a15_a = DFFE(U_PCICTRL_aData_FF_adffs_a15_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_0_adffs_a15_a);

PCI_DQ_MUX_a15_a_a19410_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19410 = U_PCICTRL_aPCI_ADR_a1_a_areg0 & U_PCICTRL_aPCI_ADR_a0_a_areg0 & DSP_REG_FF_0_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => DSP_REG_FF_0_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19410);

PCI_DQ_MUX_a15_a_a19415_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19415 = DSP_REG_FF_1_adffs_a15_a & !U_PCICTRL_aPCI_ADR_a0_a_areg0 & !U_PCICTRL_aPCI_ADR_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0202",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_1_adffs_a15_a,
	datab => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19415);

PCI_DQ_MUX_a15_a_a19416_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MUX_a15_a_a19416 = PCI_DQ_MUX_a15_a_a19414 & (PCI_DQ_MUX_a15_a_a19415 # !U_PCICTRL_aPCI_ADR_a3_a_areg0) # !PCI_DQ_MUX_a15_a_a19414 & PCI_DQ_MUX_a15_a_a19410 & (U_PCICTRL_aPCI_ADR_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E4AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DQ_MUX_a15_a_a19414,
	datab => PCI_DQ_MUX_a15_a_a19410,
	datac => PCI_DQ_MUX_a15_a_a19415,
	datad => U_PCICTRL_aPCI_ADR_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MUX_a15_a_a19416);

PCI_DQ_MOUT_a15_a_a939_I : apex20ke_lcell
-- Equation(s):
-- PCI_DQ_MOUT_a15_a_a939 = U_DPRAM_adpram_ram_a_asram_aq_a15_a & (U_PCICTRL_ai_a699 # PCI_DQ_MUX_a15_a_a19416 & PCI_DQ_MOUT_a15_a_a947) # !U_DPRAM_adpram_ram_a_asram_aq_a15_a & PCI_DQ_MUX_a15_a_a19416 & (PCI_DQ_MOUT_a15_a_a947)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_a_asram_aq_a15_a,
	datab => PCI_DQ_MUX_a15_a_a19416,
	datac => U_PCICTRL_ai_a699,
	datad => PCI_DQ_MOUT_a15_a_a947,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DQ_MOUT_a15_a_a939);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a0_a_a47,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a0_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a0_a);

DSP_D_Mout_a14_a_a2435_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a14_a_a2435 = !DSP_A1_a5_a_acombout & !DSP_A1_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a5_a_acombout,
	datad => DSP_A1_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a14_a_a2435);

DSP_D_Mout_a14_a_a2436_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a14_a_a2436 = !DSP_A1_a7_a_acombout & !DSP_A1_a4_a_acombout & !DSP_A1_a3_a_acombout & DSP_D_Mout_a14_a_a2435

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a7_a_acombout,
	datab => DSP_A1_a4_a_acombout,
	datac => DSP_A1_a3_a_acombout,
	datad => DSP_D_Mout_a14_a_a2435,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a14_a_a2436);

DSP_TREG_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a0_a = DFFE(DSP_D_a0_a_a15, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a0_a_a15,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a0_a);

DSP_D_MUX_a0_a_a10255_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a0_a_a10255 = DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a # !DSP_A1_a1_a_acombout & (DSP_TREG_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a0_a,
	datac => DSP_TREG_FF_adffs_a0_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a0_a_a10255);

DSP_D_MUX_a0_a_a10254_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a0_a_a10254 = DSP_A1_a0_a_acombout & DSP_A1_a1_a_acombout # !DSP_A1_a0_a_acombout & !DSP_A1_a1_a_acombout & !PFF_EF_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0C3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_A1_a0_a_acombout,
	datac => DSP_A1_a1_a_acombout,
	datad => PFF_EF_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a0_a_a10254);

DSP_D_MUX_a0_a_a10256_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a0_a_a10256 = DSP_A1_a2_a_acombout & !DSP_A1_a0_a_acombout & DSP_D_MUX_a0_a_a10255 # !DSP_A1_a2_a_acombout & (DSP_D_MUX_a0_a_a10254)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7520",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a2_a_acombout,
	datab => DSP_A1_a0_a_acombout,
	datac => DSP_D_MUX_a0_a_a10255,
	datad => DSP_D_MUX_a0_a_a10254,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a0_a_a10256);

a_aST_GEN_aU_DSPCTRL_ai_a41_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a41 = DSP_WR_acombout & CREG_FF_adffs_a4_a & a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28 & a_aST_GEN_aU_DSPCTRL_ai_a320

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WR_acombout,
	datab => CREG_FF_adffs_a4_a,
	datac => a_aST_GEN_aU_DSPCTRL_aDSP_A_a1_a_a28,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a320,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a41);

DSP_D_Mout_a0_a_a2437_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a0_a_a2437 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a0_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a0_a_a10256)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a0_a,
	datab => DSP_D_Mout_a14_a_a2436,
	datac => DSP_D_MUX_a0_a_a10256,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a0_a_a2437);

a_aST_GEN_aU_DSPCTRL_ai_a331_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a331 = !DSP_A_a15_a_acombout & (!DSP_A_a13_a_acombout # !DSP_A_a14_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0555",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A_a15_a_acombout,
	datac => DSP_A_a14_a_acombout,
	datad => DSP_A_a13_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a331);

a_aST_GEN_aU_DSPCTRL_ai_a119_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPCTRL_ai_a119 = CREG_FF_adffs_a4_a & a_aST_GEN_aU_DSPCTRL_ai_a331 & DSP_WR_acombout & a_aST_GEN_aU_DSPCTRL_ai_a320

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CREG_FF_adffs_a4_a,
	datab => a_aST_GEN_aU_DSPCTRL_ai_a331,
	datac => DSP_WR_acombout,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a320,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPCTRL_ai_a119);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a1_a_a44,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a1_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a1_a);

DSP_D_MUX_a1_a_a10257_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a1_a_a10257 = DSP_A1_a1_a_acombout & (DSP_A1_a0_a_acombout) # !DSP_A1_a1_a_acombout & (!PFF_EF_a1_a_acombout & !DSP_A1_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AA05",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datac => PFF_EF_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a1_a_a10257);

DSP_TREG_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a1_a = DFFE(DSP_D_a1_a_a14, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a1_a_a14,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a1_a);

DSP_D_MUX_a1_a_a10258_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a1_a_a10258 = DSP_A1_a1_a_acombout & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a) # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_TREG_FF_adffs_a1_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a1_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a1_a_a10258);

DSP_D_MUX_a1_a_a10259_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a1_a_a10259 = DSP_A1_a2_a_acombout & !DSP_A1_a0_a_acombout & (DSP_D_MUX_a1_a_a10258) # !DSP_A1_a2_a_acombout & (DSP_D_MUX_a1_a_a10257)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7250",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a2_a_acombout,
	datab => DSP_A1_a0_a_acombout,
	datac => DSP_D_MUX_a1_a_a10257,
	datad => DSP_D_MUX_a1_a_a10258,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a1_a_a10259);

DSP_D_Mout_a1_a_a2438_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a1_a_a2438 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a1_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a1_a_a10259)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a1_a,
	datab => DSP_D_Mout_a14_a_a2436,
	datac => DSP_D_MUX_a1_a_a10259,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a1_a_a2438);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a2_a_a41,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a2_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a2_a);

DSP_TREG_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a2_a = DFFE(DSP_D_a2_a_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a2_a_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a2_a);

DSP_D_MUX_a2_a_a10260_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a2_a_a10260 = DSP_A1_a2_a_acombout & DSP_TREG_FF_adffs_a2_a # !DSP_A1_a2_a_acombout & (!PFF_FF_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0F3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_A1_a2_a_acombout,
	datac => DSP_TREG_FF_adffs_a2_a,
	datad => PFF_FF_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a2_a_a10260);

DSP_D_MUX_a2_a_a10261_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a2_a_a10261 = DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a & DSP_A1_a2_a_acombout # !DSP_A1_a1_a_acombout & (DSP_D_MUX_a2_a_a10260)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8F80",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a2_a,
	datab => DSP_A1_a2_a_acombout,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_D_MUX_a2_a_a10260,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a2_a_a10261);

DSP_D_Mout_a2_a_a2439_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a2_a_a2439 = !DSP_A1_a0_a_acombout & DSP_D_MUX_a2_a_a10261 & DSP_D_Mout_a14_a_a2436 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_D_MUX_a2_a_a10261,
	datac => DSP_D_Mout_a14_a_a2436,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a2_a_a2439);

DSP_D_Mout_a2_a_a2440_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a2_a_a2440 = DSP_D_Mout_a2_a_a2439 # a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datac => U_DPRAM_adpram_ram_b_asram_aq_a2_a,
	datad => DSP_D_Mout_a2_a_a2439,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a2_a_a2440);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a3_a_a38,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a3_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a3_a);

DSP_D_MUX_a3_a_a10262_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a3_a_a10262 = DSP_A1_a2_a_acombout & DSP_TREG_FF_adffs_a3_a # !DSP_A1_a2_a_acombout & (!PFF_FF_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0AF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_TREG_FF_adffs_a3_a,
	datac => DSP_A1_a2_a_acombout,
	datad => PFF_FF_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a3_a_a10262);

DSP_D_MUX_a3_a_a10263_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a3_a_a10263 = DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a & DSP_A1_a2_a_acombout # !DSP_A1_a1_a_acombout & (DSP_D_MUX_a3_a_a10262)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B380",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a3_a,
	datab => DSP_A1_a1_a_acombout,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a3_a_a10262,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a3_a_a10263);

DSP_D_Mout_a3_a_a2441_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a3_a_a2441 = !DSP_A1_a0_a_acombout & DSP_D_MUX_a3_a_a10263 & DSP_D_Mout_a14_a_a2436 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_D_MUX_a3_a_a10263,
	datac => DSP_D_Mout_a14_a_a2436,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a3_a_a2441);

DSP_D_Mout_a3_a_a2442_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a3_a_a2442 = DSP_D_Mout_a3_a_a2441 # a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datac => U_DPRAM_adpram_ram_b_asram_aq_a3_a,
	datad => DSP_D_Mout_a3_a_a2441,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a3_a_a2442);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a4_a_a35,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a4_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a4_a);

DSP_D_MUX_a4_a_a10264_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a4_a_a10264 = DSP_A1_a0_a_acombout # U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a & DSP_A1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a4_a,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a4_a_a10264);

DSP_D_MUX_a4_a_a10265_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a4_a_a10265 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a4_a_a10264 # DSP_TREG_FF_adffs_a4_a & !DSP_A1_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F020",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_TREG_FF_adffs_a4_a,
	datab => DSP_A1_a1_a_acombout,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a4_a_a10264,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a4_a_a10265);

DSP_D_MUX_a4_a_a10266_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a4_a_a10266 = DSP_A1_a1_a_acombout $ DSP_A1_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a4_a_a10266);

DSP_D_Mout_a14_a_a2443_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a14_a_a2443 = !DSP_A1_a7_a_acombout & !DSP_A1_a5_a_acombout & !DSP_A1_a3_a_acombout & !DSP_A1_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a7_a_acombout,
	datab => DSP_A1_a5_a_acombout,
	datac => DSP_A1_a3_a_acombout,
	datad => DSP_A1_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a14_a_a2443);

DSP_D_MUX_a12_a_a10267_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a12_a_a10267 = !DSP_A1_a4_a_acombout & DSP_D_Mout_a14_a_a2443 & (DSP_A1_a2_a_acombout # !DSP_D_MUX_a4_a_a10266)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0B00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a2_a_acombout,
	datab => DSP_D_MUX_a4_a_a10266,
	datac => DSP_A1_a4_a_acombout,
	datad => DSP_D_Mout_a14_a_a2443,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a12_a_a10267);

DSP_D_Mout_a4_a_a2444_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a4_a_a2444 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a4_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a4_a_a10265 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AACF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a4_a,
	datab => DSP_D_MUX_a4_a_a10265,
	datac => DSP_D_MUX_a12_a_a10267,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a4_a_a2444);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a5_a_a32,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a5_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a5_a);

DSP_TREG_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a5_a = DFFE(DSP_D_a5_a_a10, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a5_a_a10,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a5_a);

DSP_D_MUX_a5_a_a10268_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a5_a_a10268 = DSP_A1_a0_a_acombout # DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a5_a,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a5_a_a10268);

DSP_D_MUX_a5_a_a10269_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a5_a_a10269 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a5_a_a10268 # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_TREG_FF_adffs_a5_a,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a5_a_a10268,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a5_a_a10269);

DSP_D_Mout_a5_a_a2445_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a5_a_a2445 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a5_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a5_a_a10269 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a5_a,
	datab => DSP_D_MUX_a12_a_a10267,
	datac => DSP_D_MUX_a5_a_a10269,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a5_a_a2445);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a6_a_a29,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a6_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a6_a);

DSP_TREG_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a6_a = DFFE(DSP_D_a6_a_a9, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a6_a_a9,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a6_a);

DSP_D_MUX_a6_a_a10270_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a6_a_a10270 = DSP_A1_a0_a_acombout # DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a6_a,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a6_a_a10270);

DSP_D_MUX_a6_a_a10271_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a6_a_a10271 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a6_a_a10270 # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_TREG_FF_adffs_a6_a,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a6_a_a10270,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a6_a_a10271);

DSP_D_Mout_a6_a_a2446_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a6_a_a2446 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a6_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a6_a_a10271 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a6_a,
	datab => DSP_D_MUX_a12_a_a10267,
	datac => DSP_D_MUX_a6_a_a10271,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a6_a_a2446);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a7_a_a26,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a7_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a7_a);

DSP_D_MUX_a14_a_a10272_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a14_a_a10272 = DSP_A1_a2_a_acombout & (!DSP_A1_a0_a_acombout)
-- DSP_D_MUX_a14_a_a10291 = DSP_A1_a2_a_acombout & (!DSP_A1_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_A1_a2_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a14_a_a10272,
	cascout => DSP_D_MUX_a14_a_a10291);

DSP_D_MUX_a7_a_a10273_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a7_a_a10273 = DSP_A1_a1_a_acombout & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a) # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_TREG_FF_adffs_a7_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a7_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a7_a_a10273);

DSP_D_Mout_a7_a_a2447_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a7_a_a2447 = DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a14_a_a10272 & DSP_D_MUX_a7_a_a10273 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_Mout_a14_a_a2436,
	datab => DSP_D_MUX_a14_a_a10272,
	datac => DSP_D_MUX_a7_a_a10273,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a7_a_a2447);

DSP_D_Mout_a7_a_a2448_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a7_a_a2448 = DSP_D_Mout_a7_a_a2447 # a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datac => U_DPRAM_adpram_ram_b_asram_aq_a7_a,
	datad => DSP_D_Mout_a7_a_a2447,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a7_a_a2448);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a8_a_a23,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a8_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a8_a);

DSP_TREG_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a8_a = DFFE(DSP_D_a8_a_a7, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a8_a_a7,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a8_a);

DSP_D_MUX_a8_a_a10274_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a8_a_a10274 = DSP_A1_a0_a_acombout # U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a & DSP_A1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a8_a,
	datac => DSP_A1_a0_a_acombout,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a8_a_a10274);

DSP_D_MUX_a8_a_a10275_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a8_a_a10275 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a8_a_a10274 # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_TREG_FF_adffs_a8_a,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a8_a_a10274,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a8_a_a10275);

DSP_D_Mout_a8_a_a2449_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a8_a_a2449 = a_aST_GEN_aU_DSPCTRL_ai_a41 & (U_DPRAM_adpram_ram_b_asram_aq_a8_a) # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a8_a_a10275 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCF5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_MUX_a12_a_a10267,
	datab => U_DPRAM_adpram_ram_b_asram_aq_a8_a,
	datac => DSP_D_MUX_a8_a_a10275,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a8_a_a2449);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a9_a_a20,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a9_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a9_a);

DSP_TREG_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a9_a = DFFE(DSP_D_a9_a_a6, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a9_a_a6,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a9_a);

DSP_D_MUX_a9_a_a10276_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a9_a_a10276 = DSP_A1_a1_a_acombout & U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a # !DSP_A1_a1_a_acombout & (DSP_TREG_FF_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a9_a,
	datac => DSP_TREG_FF_adffs_a9_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a9_a_a10276);

DSP_D_Mout_a9_a_a2450_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a9_a_a2450 = DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a14_a_a10272 & DSP_D_MUX_a9_a_a10276 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_Mout_a14_a_a2436,
	datab => DSP_D_MUX_a14_a_a10272,
	datac => DSP_D_MUX_a9_a_a10276,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a9_a_a2450);

DSP_D_Mout_a9_a_a2451_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a9_a_a2451 = DSP_D_Mout_a9_a_a2450 # a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datac => U_DPRAM_adpram_ram_b_asram_aq_a9_a,
	datad => DSP_D_Mout_a9_a_a2450,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a9_a_a2451);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a10_a_a17,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a10_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a10_a);

DSP_TREG_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a10_a = DFFE(DSP_D_a10_a_a5, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a10_a_a5,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a10_a);

DSP_D_MUX_a10_a_a10277_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a10_a_a10277 = DSP_A1_a1_a_acombout & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a) # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_TREG_FF_adffs_a10_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a10_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a10_a_a10277);

DSP_D_Mout_a10_a_a2452_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a10_a_a2452 = DSP_D_MUX_a14_a_a10272 & DSP_D_MUX_a10_a_a10277 & DSP_D_Mout_a14_a_a2436 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_MUX_a14_a_a10272,
	datab => DSP_D_MUX_a10_a_a10277,
	datac => DSP_D_Mout_a14_a_a2436,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a10_a_a2452);

DSP_D_Mout_a10_a_a2453_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a10_a_a2453 = DSP_D_Mout_a10_a_a2452 # U_DPRAM_adpram_ram_b_asram_aq_a10_a & a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_adpram_ram_b_asram_aq_a10_a,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datad => DSP_D_Mout_a10_a_a2452,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a10_a_a2453);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a11_a_a14,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a11_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a11_a);

DSP_TREG_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a11_a = DFFE(DSP_D_a11_a_a4, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a11_a_a4,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a11_a);

DSP_D_MUX_a11_a_a10278_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a11_a_a10278 = DSP_A1_a0_a_acombout # U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a & DSP_A1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a11_a,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a11_a_a10278);

DSP_D_MUX_a11_a_a10279_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a11_a_a10279 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a11_a_a10278 # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_TREG_FF_adffs_a11_a,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a11_a_a10278,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a11_a_a10279);

DSP_D_Mout_a11_a_a2454_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a11_a_a2454 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a11_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a11_a_a10279 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D8DD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datab => U_DPRAM_adpram_ram_b_asram_aq_a11_a,
	datac => DSP_D_MUX_a11_a_a10279,
	datad => DSP_D_MUX_a12_a_a10267,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a11_a_a2454);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a12_a_a11,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a12_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a12_a);

DSP_TREG_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a12_a = DFFE(DSP_D_a12_a_a3, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a12_a_a3,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a12_a);

DSP_D_MUX_a12_a_a10280_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a12_a_a10280 = DSP_A1_a0_a_acombout # U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a & DSP_A1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a12_a,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a12_a_a10280);

DSP_D_MUX_a12_a_a10281_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a12_a_a10281 = DSP_A1_a2_a_acombout & (DSP_D_MUX_a12_a_a10280 # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_TREG_FF_adffs_a12_a,
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_D_MUX_a12_a_a10280,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a12_a_a10281);

DSP_D_Mout_a12_a_a2455_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a12_a_a2455 = a_aST_GEN_aU_DSPCTRL_ai_a41 & U_DPRAM_adpram_ram_b_asram_aq_a12_a # !a_aST_GEN_aU_DSPCTRL_ai_a41 & (DSP_D_MUX_a12_a_a10281 # !DSP_D_MUX_a12_a_a10267)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAF3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_adpram_ram_b_asram_aq_a12_a,
	datab => DSP_D_MUX_a12_a_a10267,
	datac => DSP_D_MUX_a12_a_a10281,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a12_a_a2455);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a13_a_a8,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a13_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a13_a);

DSP_TREG_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a13_a = DFFE(DSP_D_a13_a_a2, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a13_a_a2,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a13_a);

DSP_D_MUX_a13_a_a10282_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a13_a_a10282 = DSP_A1_a1_a_acombout & U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 # !DSP_A1_a1_a_acombout & (DSP_TREG_FF_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_FFCNT_aHALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	datac => DSP_TREG_FF_adffs_a13_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a13_a_a10282);

DSP_D_Mout_a13_a_a2456_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a13_a_a2456 = DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a13_a_a10282 & DSP_D_MUX_a14_a_a10272 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_Mout_a14_a_a2436,
	datab => DSP_D_MUX_a13_a_a10282,
	datac => DSP_D_MUX_a14_a_a10272,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a13_a_a2456);

DSP_D_Mout_a13_a_a2457_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a13_a_a2457 = DSP_D_Mout_a13_a_a2456 # U_DPRAM_adpram_ram_b_asram_aq_a13_a & a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_adpram_ram_b_asram_aq_a13_a,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datad => DSP_D_Mout_a13_a_a2456,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a13_a_a2457);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a14_a_a5,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a14_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a14_a);

DSP_TREG_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a14_a = DFFE(DSP_D_a14_a_a1, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a14_a_a1,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a14_a);

DSP_D_MUX_a14_a_a10290_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a14_a_a10290 = (DSP_A1_a1_a_acombout & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a) # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a14_a) & CASCADE(DSP_D_MUX_a14_a_a10291)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_TREG_FF_adffs_a14_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a14_a,
	datad => DSP_A1_a1_a_acombout,
	cascin => DSP_D_MUX_a14_a_a10291,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a14_a_a10290);

DSP_D_MUX_a14_a_a10284_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a14_a_a10284 = DSP_D_MUX_a14_a_a10290 # DSP_A1_a1_a_acombout & !DSP_A1_a2_a_acombout & DSP_A1_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF20",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a1_a_acombout,
	datab => DSP_A1_a2_a_acombout,
	datac => DSP_A1_a0_a_acombout,
	datad => DSP_D_MUX_a14_a_a10290,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a14_a_a10284);

DSP_D_Mout_a14_a_a2458_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a14_a_a2458 = a_aST_GEN_aU_DSPCTRL_ai_a41 & (U_DPRAM_adpram_ram_b_asram_aq_a14_a) # !a_aST_GEN_aU_DSPCTRL_ai_a41 & DSP_D_Mout_a14_a_a2436 & (DSP_D_MUX_a14_a_a10284)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_Mout_a14_a_a2436,
	datab => U_DPRAM_adpram_ram_b_asram_aq_a14_a,
	datac => DSP_D_MUX_a14_a_a10284,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a14_a_a2458);

U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "DPRAM:U_DPRAM|dpram_ram_b_asram_acontent",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => U_DPRAM_adata_mux_a15_a_a2,
	clk0 => clk_pll1_aaltclklock_component_aoutclock1,
	clk1 => DSP_H3_acombout,
	we => U_DPRAM_aWrite_En,
	re => VCC,
	waddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => U_DPRAM_adpram_ram_b_asram_asegment_a0_a_a15_a_modesel,
	dataout => U_DPRAM_adpram_ram_b_asram_aq_a15_a);

DSP_TREG_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_TREG_FF_adffs_a15_a = DFFE(DSP_D_a15_a_a0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , !a_aST_GEN_aU_DSPCTRL_ai_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a15_a_a0,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	ena => ALT_INV_a_aST_GEN_aU_DSPCTRL_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_TREG_FF_adffs_a15_a);

DSP_D_MUX_a15_a_a10285_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_MUX_a15_a_a10285 = DSP_A1_a1_a_acombout & (U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a) # !DSP_A1_a1_a_acombout & DSP_TREG_FF_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_TREG_FF_adffs_a15_a,
	datac => U_FFCNT_aff_cnt_rtl_1_awysi_counter_aq_a15_a,
	datad => DSP_A1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_MUX_a15_a_a10285);

DSP_D_Mout_a15_a_a2459_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a15_a_a2459 = DSP_D_Mout_a14_a_a2436 & DSP_D_MUX_a15_a_a10285 & DSP_D_MUX_a14_a_a10272 & !a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_Mout_a14_a_a2436,
	datab => DSP_D_MUX_a15_a_a10285,
	datac => DSP_D_MUX_a14_a_a10272,
	datad => a_aST_GEN_aU_DSPCTRL_ai_a41,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a15_a_a2459);

DSP_D_Mout_a15_a_a2460_I : apex20ke_lcell
-- Equation(s):
-- DSP_D_Mout_a15_a_a2460 = DSP_D_Mout_a15_a_a2459 # U_DPRAM_adpram_ram_b_asram_aq_a15_a & a_aST_GEN_aU_DSPCTRL_ai_a41

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => U_DPRAM_adpram_ram_b_asram_aq_a15_a,
	datac => a_aST_GEN_aU_DSPCTRL_ai_a41,
	datad => DSP_D_Mout_a15_a_a2459,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_D_Mout_a15_a_a2460);

U_PCICTRL_aWE_DCD_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_PCICTRL_aWE_DCD_a10_a_areg0 = DFFE(!U_PCICTRL_aPCI_ADR_a2_a_areg0 & U_PCICTRL_aPCI_ADR_a1_a_areg0 & !U_PCICTRL_aPCI_ADR_a0_a_areg0 & U_PCICTRL_ai_a559, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_PCICTRL_aPCI_ADR_a2_a_areg0,
	datab => U_PCICTRL_aPCI_ADR_a1_a_areg0,
	datac => U_PCICTRL_aPCI_ADR_a0_a_areg0,
	datad => U_PCICTRL_ai_a559,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_PCICTRL_aWE_DCD_a10_a_areg0);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a = DFFE(U_PCICTRL_aWE_DCD_a10_a_areg0, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aWE_DCD_a10_a_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a);

a_aST_GEN_aU_DSPBOOT_ai_a2_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_ai_a2 = !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a & a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a # !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_ai_a2);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a = DFFE(!a_aST_GEN_aU_DSPBOOT_ai_a2 & !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => a_aST_GEN_aU_DSPBOOT_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a = DFFE(!a_aST_GEN_aU_DSPBOOT_ai_a2 & a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a $ 
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => a_aST_GEN_aU_DSPBOOT_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	cout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a = DFFE(!a_aST_GEN_aU_DSPBOOT_ai_a2 & a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT $ 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a,
	cin => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	sclr => a_aST_GEN_aU_DSPBOOT_ai_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a);

a_aST_GEN_aU_DSPBOOT_ai_a242_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_ai_a242 = a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a & !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a & (a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0) # !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a & 
-- (a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a # !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a & a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7350",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a1_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a0_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_VEC_a0_a,
	datad => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aST_GEN_aU_DSPBOOT_ai_a242);

a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0 = DFFE(a_aST_GEN_aU_DSPBOOT_ai_a242 # a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0 & (!a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a # 
-- !a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF4C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a1_a,
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_CNT_rtl_0_awysi_counter_asload_path_a2_a,
	datad => a_aST_GEN_aU_DSPBOOT_ai_a242,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_BOOT_OUT_areg0);

DSP_REG_FF_1_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a11_a = DFFE(U_PCICTRL_aData_FF_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a11_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a11_a);

DSP_REG_FF_1_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a7_a = DFFE(U_PCICTRL_aData_FF_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a7_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a7_a);

DSP_REG_FF_1_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a6_a = DFFE(U_PCICTRL_aData_FF_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a6_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a6_a);

DSP_REG_FF_1_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a4_a = DFFE(U_PCICTRL_aData_FF_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a4_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a4_a);

DSP_REG_FF_1_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a3_a = DFFE(U_PCICTRL_aData_FF_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a3_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a3_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_1_adffs_a1_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ACAC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a16_a,
	datab => DSP_REG_FF_1_adffs_a1_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a2_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a2_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a17_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a3_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a3_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a18_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a4_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a4_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a19_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a5_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA0A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_REG_FF_1_adffs_a5_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a20_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a6_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a6_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a21_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a7_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a7_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a22_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_1_adffs_a8_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a23_a,
	datad => DSP_REG_FF_1_adffs_a8_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a9_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_1_adffs_a9_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a24_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a);

DSP_REG_FF_1_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a10_a = DFFE(U_PCICTRL_aData_FF_adffs_a10_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a10_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_1_adffs_a10_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a25_a,
	datad => DSP_REG_FF_1_adffs_a10_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a11_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_REG_FF_1_adffs_a11_a,
	datac => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a26_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a12_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_1_adffs_a12_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a27_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_1_adffs_a13_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a28_a,
	datad => DSP_REG_FF_1_adffs_a13_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & DSP_REG_FF_1_adffs_a14_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => DSP_REG_FF_1_adffs_a14_a,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a29_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a);

DSP_REG_FF_1_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_REG_FF_1_adffs_a15_a = DFFE(U_PCICTRL_aData_FF_adffs_a15_a, GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , U_PCICTRL_aWE_DCD_a12_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_PCICTRL_aData_FF_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => U_PCICTRL_aWE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_REG_FF_1_adffs_a15_a);

a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a) # !a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (DSP_REG_FF_1_adffs_a15_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), 
-- S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datac => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a30_a,
	datad => DSP_REG_FF_1_adffs_a15_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a);

a_aST_GEN_aU_DSPBOOT_aDSP_D0_areg0_I : apex20ke_lcell
-- Equation(s):
-- a_aST_GEN_aU_DSPBOOT_aDSP_D0_areg0 = DFFE(a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0 & (a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a), GLOBAL(clk_pll1_aaltclklock_component_aoutclock0), S5920MR_acombout, , a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => a_aST_GEN_aU_DSPBOOT_aDSP_SHIFT_areg0,
	datad => a_aST_GEN_aU_DSPBOOT_aiDSP_REG_a31_a,
	clk => clk_pll1_aaltclklock_component_aoutclock0,
	aclr => ALT_INV_S5920MR_acombout,
	ena => a_aST_GEN_aU_DSPBOOT_aDSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aST_GEN_aU_DSPBOOT_aDSP_D0_areg0);

U_DPRAM_astate_a14_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_astate_a14 = DFFE(U_DPRAM_astate_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U_DPRAM_astate_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_astate_a14);

U_DPRAM_aOpCodeISR_areg0_I : apex20ke_lcell
-- Equation(s):
-- U_DPRAM_aOpCodeISR_areg0 = DFFE(U_DPRAM_astate_a14 # U_DPRAM_astate_a12 # U_DPRAM_astate_a11 # U_DPRAM_astate_a13, GLOBAL(clk_pll1_aaltclklock_component_aoutclock1), S5920MR_acombout, , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U_DPRAM_astate_a14,
	datab => U_DPRAM_astate_a12,
	datac => U_DPRAM_astate_a11,
	datad => U_DPRAM_astate_a13,
	clk => clk_pll1_aaltclklock_component_aoutclock1,
	aclr => ALT_INV_S5920MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DPRAM_aOpCodeISR_areg0);

PCI_RST_OUT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U_PCICTRL_aPCI_RST_OUT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_RST_OUT);

OMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_OMR_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_OMR);

AO_CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => clk_pll1_aaltclklock_component_aoutclock1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AO_CLK);

AO_INTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_a_aST_GEN_aU_DSPCTRL_aAO_INTR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AO_INTR);

FPGA_DCLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U_FCONFIG_aDCLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FPGA_DCLK);

FPGA_D0_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U_FCONFIG_aDATA_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FPGA_D0);

FPGA_NC_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_FCONFIG_aNCONFIG_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FPGA_NC);

PFF_WEN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_a_aST_GEN_aU_DSPCTRL_aPFF_WEN_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_WEN);

PFF_REN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U_PCICTRL_ai_a15,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_REN);

PFF_OE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_PCICTRL_ai_a138,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_OE);

PFF_RS_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_RS,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_RS);

PFF_WCK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PFF_WCK_a250,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_WCK);

TP_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1129,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(0));

TP_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1136,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(1));

TP_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1141,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(2));

TP_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1147,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(3));

TP_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1152,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(4));

TP_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Mux_a1156,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TP(5));

U_DPRAM_aOpCodeISR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_DPRAM_aOpCodeISR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DSP_IRQ);
END structure;


