CLKBUF_inst : CLKBUF PORT MAP (
		inclock	 => inclock_sig,
		clock0	 => clock0_sig,
		clock1	 => clock1_sig
	);
