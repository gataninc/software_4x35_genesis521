-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 10:58:22"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	DPRAM IS
    PORT (
	IMR : IN std_logic;
	clk_a : IN std_logic;
	clk_b : IN std_logic;
	wren_a : IN std_logic;
	wren_b : IN std_logic;
	addr_a : IN std_logic_vector(8 DOWNTO 0);
	addr_b : IN std_logic_vector(8 DOWNTO 0);
	data_a : IN std_logic_vector(15 DOWNTO 0);
	data_b : IN std_logic_vector(15 DOWNTO 0);
	q_a : OUT std_logic_vector(15 DOWNTO 0);
	q_b : OUT std_logic_vector(15 DOWNTO 0);
	state_str : OUT std_logic_vector(3 DOWNTO 0);
	OpCodeISR : OUT std_logic
	);
END DPRAM;

ARCHITECTURE structure OF DPRAM IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_clk_a : std_logic;
SIGNAL ww_clk_b : std_logic;
SIGNAL ww_wren_a : std_logic;
SIGNAL ww_wren_b : std_logic;
SIGNAL ww_addr_a : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_addr_b : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_data_a : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_data_b : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_q_a : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_q_b : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_state_str : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_OpCodeISR : std_logic;
SIGNAL dpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dpram_ram_a_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_a_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dpram_ram_b_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL reg_ff_b_a_adffs_a8_a : std_logic;
SIGNAL wren_b_ca : std_logic;
SIGNAL state_a325 : std_logic;
SIGNAL state_a327 : std_logic;
SIGNAL wren_b_cb : std_logic;
SIGNAL data_a_a0_a_acombout : std_logic;
SIGNAL clk_a_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL wren_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a0_a : std_logic;
SIGNAL last : std_logic;
SIGNAL wren_b_lat : std_logic;
SIGNAL addr_a_a2_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a2_a : std_logic;
SIGNAL addr_a_a1_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a1_a : std_logic;
SIGNAL addr_a_a3_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a3_a : std_logic;
SIGNAL state_a315 : std_logic;
SIGNAL addr_a_a5_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a5_a : std_logic;
SIGNAL addr_a_a7_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a7_a : std_logic;
SIGNAL addr_a_a4_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a4_a : std_logic;
SIGNAL state_a316 : std_logic;
SIGNAL addr_a_a8_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a8_a : std_logic;
SIGNAL state_ast_wra_op0_a16 : std_logic;
SIGNAL state_ast_wra_op0_a15 : std_logic;
SIGNAL state_ast_wra_op1 : std_logic;
SIGNAL state_ast_wra_op2 : std_logic;
SIGNAL state_ast_wra_op3 : std_logic;
SIGNAL state_ast_wra_op4 : std_logic;
SIGNAL state_a318 : std_logic;
SIGNAL state_ast_idle_a35 : std_logic;
SIGNAL state_a334 : std_logic;
SIGNAL state_ast_wra : std_logic;
SIGNAL wren_a_lat : std_logic;
SIGNAL state_a333 : std_logic;
SIGNAL state_ast_wrb : std_logic;
SIGNAL data_b_a0_a_acombout : std_logic;
SIGNAL wren_b_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a0_a : std_logic;
SIGNAL data_mux_a0_a_a112 : std_logic;
SIGNAL clk_b_acombout : std_logic;
SIGNAL Write_En_a0 : std_logic;
SIGNAL addr_a_a0_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a0_a : std_logic;
SIGNAL addr_b_a0_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a0_a : std_logic;
SIGNAL addr_mux_a0_a_a63 : std_logic;
SIGNAL addr_b_a1_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a1_a : std_logic;
SIGNAL addr_mux_a1_a_a64 : std_logic;
SIGNAL addr_b_a2_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a2_a : std_logic;
SIGNAL addr_mux_a2_a_a65 : std_logic;
SIGNAL addr_b_a3_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a3_a : std_logic;
SIGNAL addr_mux_a3_a_a66 : std_logic;
SIGNAL addr_b_a4_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a4_a : std_logic;
SIGNAL addr_mux_a4_a_a67 : std_logic;
SIGNAL addr_b_a5_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a5_a : std_logic;
SIGNAL addr_mux_a5_a_a68 : std_logic;
SIGNAL addr_a_a6_a_acombout : std_logic;
SIGNAL reg_ff_a_a_adffs_a6_a : std_logic;
SIGNAL addr_b_a6_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a6_a : std_logic;
SIGNAL addr_mux_a6_a_a69 : std_logic;
SIGNAL addr_b_a7_a_acombout : std_logic;
SIGNAL reg_ff_b_a_adffs_a7_a : std_logic;
SIGNAL addr_mux_a7_a_a70 : std_logic;
SIGNAL addr_mux_a8_a_a71 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a0_a : std_logic;
SIGNAL data_a_a1_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a1_a : std_logic;
SIGNAL data_b_a1_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a1_a : std_logic;
SIGNAL data_mux_a1_a_a113 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a1_a : std_logic;
SIGNAL data_a_a2_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a2_a : std_logic;
SIGNAL data_b_a2_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a2_a : std_logic;
SIGNAL data_mux_a2_a_a114 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a2_a : std_logic;
SIGNAL data_a_a3_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a3_a : std_logic;
SIGNAL data_b_a3_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a3_a : std_logic;
SIGNAL data_mux_a3_a_a115 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a3_a : std_logic;
SIGNAL data_a_a4_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a4_a : std_logic;
SIGNAL data_b_a4_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a4_a : std_logic;
SIGNAL data_mux_a4_a_a116 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a4_a : std_logic;
SIGNAL data_a_a5_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a5_a : std_logic;
SIGNAL data_b_a5_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a5_a : std_logic;
SIGNAL data_mux_a5_a_a117 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a5_a : std_logic;
SIGNAL data_a_a6_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a6_a : std_logic;
SIGNAL data_b_a6_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a6_a : std_logic;
SIGNAL data_mux_a6_a_a118 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a6_a : std_logic;
SIGNAL data_a_a7_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a7_a : std_logic;
SIGNAL data_b_a7_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a7_a : std_logic;
SIGNAL data_mux_a7_a_a119 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a7_a : std_logic;
SIGNAL data_a_a8_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a8_a : std_logic;
SIGNAL data_b_a8_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a8_a : std_logic;
SIGNAL data_mux_a8_a_a120 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a8_a : std_logic;
SIGNAL data_a_a9_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a9_a : std_logic;
SIGNAL data_b_a9_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a9_a : std_logic;
SIGNAL data_mux_a9_a_a121 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a9_a : std_logic;
SIGNAL data_a_a10_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a10_a : std_logic;
SIGNAL data_b_a10_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a10_a : std_logic;
SIGNAL data_mux_a10_a_a122 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a10_a : std_logic;
SIGNAL data_a_a11_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a11_a : std_logic;
SIGNAL data_b_a11_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a11_a : std_logic;
SIGNAL data_mux_a11_a_a123 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a11_a : std_logic;
SIGNAL data_b_a12_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a12_a : std_logic;
SIGNAL data_a_a12_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a12_a : std_logic;
SIGNAL data_mux_a12_a_a124 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a12_a : std_logic;
SIGNAL data_a_a13_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a13_a : std_logic;
SIGNAL data_b_a13_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a13_a : std_logic;
SIGNAL data_mux_a13_a_a125 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a13_a : std_logic;
SIGNAL data_a_a14_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a14_a : std_logic;
SIGNAL data_b_a14_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a14_a : std_logic;
SIGNAL data_mux_a14_a_a126 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a14_a : std_logic;
SIGNAL data_b_a15_a_acombout : std_logic;
SIGNAL reg_ff_b_d_adffs_a15_a : std_logic;
SIGNAL data_a_a15_a_acombout : std_logic;
SIGNAL reg_ff_a_d_adffs_a15_a : std_logic;
SIGNAL data_mux_a15_a_a127 : std_logic;
SIGNAL dpram_ram_a_asram_aq_a15_a : std_logic;
SIGNAL addr_b_a8_a_acombout : std_logic;
SIGNAL dpram_ram_b_asram_aq_a0_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a1_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a2_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a3_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a4_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a5_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a6_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a7_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a8_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a9_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a10_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a11_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a12_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a13_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a14_a : std_logic;
SIGNAL dpram_ram_b_asram_aq_a15_a : std_logic;
SIGNAL state_ast_idle_a36 : std_logic;
SIGNAL Select_a185 : std_logic;
SIGNAL reduce_or_a2 : std_logic;
SIGNAL reduce_or_a1 : std_logic;
SIGNAL Select_a186 : std_logic;
SIGNAL reduce_or_a0 : std_logic;
SIGNAL OpCodeISR_areg0 : std_logic;
SIGNAL ALT_INV_reduce_or_a2 : std_logic;
SIGNAL ALT_INV_reduce_or_a1 : std_logic;
SIGNAL ALT_INV_reduce_or_a0 : std_logic;
SIGNAL ALT_INV_OpCodeISR_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_clk_a <= clk_a;
ww_clk_b <= clk_b;
ww_wren_a <= wren_a;
ww_wren_b <= wren_b;
ww_addr_a <= addr_a;
ww_addr_b <= addr_b;
ww_data_a <= data_a;
ww_data_b <= data_b;
q_a <= ww_q_a;
q_b <= ww_q_b;
state_str <= ww_state_str;
OpCodeISR <= ww_OpCodeISR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

dpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_a_a8_a_acombout & addr_a_a7_a_acombout & addr_a_a6_a_acombout & addr_a_a5_a_acombout & addr_a_a4_a_acombout & addr_a_a3_a_acombout & addr_a_a2_a_acombout & 
addr_a_a1_a_acombout & addr_a_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_mux_a8_a_a71 & addr_mux_a7_a_a70 & addr_mux_a6_a_a69 & addr_mux_a5_a_a68 & addr_mux_a4_a_a67 & addr_mux_a3_a_a66 & addr_mux_a2_a_a65 & addr_mux_a1_a_a64 & 
addr_mux_a0_a_a63);

dpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & addr_b_a8_a_acombout & addr_b_a7_a_acombout & addr_b_a6_a_acombout & addr_b_a5_a_acombout & addr_b_a4_a_acombout & addr_b_a3_a_acombout & addr_b_a2_a_acombout & 
addr_b_a1_a_acombout & addr_b_a0_a_acombout);
ALT_INV_reduce_or_a2 <= NOT reduce_or_a2;
ALT_INV_reduce_or_a1 <= NOT reduce_or_a1;
ALT_INV_reduce_or_a0 <= NOT reduce_or_a0;
ALT_INV_OpCodeISR_areg0 <= NOT OpCodeISR_areg0;

reg_ff_b_a_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a8_a = DFFE(addr_b_a8_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a8_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a8_a);

wren_b_ca_aI : apex20ke_lcell
-- Equation(s):
-- wren_b_ca = DFFE(wren_b_cb, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => wren_b_cb,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wren_b_ca);

wren_b_cb_aI : apex20ke_lcell
-- Equation(s):
-- wren_b_cb = DFFE(wren_b_acombout # !wren_b_ca & wren_b_cb, GLOBAL(clk_b_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF30",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wren_b_ca,
	datac => wren_b_cb,
	datad => wren_b_acombout,
	clk => clk_b_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wren_b_cb);

data_a_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(0),
	combout => data_a_a0_a_acombout);

clk_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk_a,
	combout => clk_a_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

wren_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_wren_a,
	combout => wren_a_acombout);

reg_ff_a_d_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a0_a = DFFE(data_a_a0_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a0_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a0_a);

last_aI : apex20ke_lcell
-- Equation(s):
-- last = DFFE(!state_ast_wra & (state_ast_wrb # last), GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => state_ast_wra,
	datad => last,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => last);

wren_b_lat_aI : apex20ke_lcell
-- Equation(s):
-- wren_b_lat = DFFE(wren_b_ca # !state_ast_wrb & wren_b_lat, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wren_b_ca,
	datac => state_ast_wrb,
	datad => wren_b_lat,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wren_b_lat);

addr_a_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(2),
	combout => addr_a_a2_a_acombout);

reg_ff_a_a_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a2_a = DFFE(addr_a_a2_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a2_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a2_a);

addr_a_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(1),
	combout => addr_a_a1_a_acombout);

reg_ff_a_a_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a1_a = DFFE(addr_a_a1_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a1_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a1_a);

addr_a_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(3),
	combout => addr_a_a3_a_acombout);

reg_ff_a_a_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a3_a = DFFE(addr_a_a3_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a3_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a3_a);

state_a315_I : apex20ke_lcell
-- Equation(s):
-- state_a315 = !reg_ff_a_a_adffs_a2_a & !reg_ff_a_a_adffs_a3_a & (reg_ff_a_a_adffs_a0_a $ !reg_ff_a_a_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0021",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reg_ff_a_a_adffs_a0_a,
	datab => reg_ff_a_a_adffs_a2_a,
	datac => reg_ff_a_a_adffs_a1_a,
	datad => reg_ff_a_a_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => state_a315);

addr_a_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(5),
	combout => addr_a_a5_a_acombout);

reg_ff_a_a_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a5_a = DFFE(addr_a_a5_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a5_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a5_a);

addr_a_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(7),
	combout => addr_a_a7_a_acombout);

reg_ff_a_a_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a7_a = DFFE(addr_a_a7_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a7_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a7_a);

addr_a_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(4),
	combout => addr_a_a4_a_acombout);

reg_ff_a_a_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a4_a = DFFE(addr_a_a4_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a4_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a4_a);

state_a316_I : apex20ke_lcell
-- Equation(s):
-- state_a316 = !reg_ff_a_a_adffs_a6_a & !reg_ff_a_a_adffs_a5_a & !reg_ff_a_a_adffs_a7_a & !reg_ff_a_a_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reg_ff_a_a_adffs_a6_a,
	datab => reg_ff_a_a_adffs_a5_a,
	datac => reg_ff_a_a_adffs_a7_a,
	datad => reg_ff_a_a_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => state_a316);

addr_a_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(8),
	combout => addr_a_a8_a_acombout);

reg_ff_a_a_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a8_a = DFFE(addr_a_a8_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a8_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a8_a);

state_ast_wra_op0_a16_I : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op0_a16 = DFFE(state_a315 & state_a316 & !reg_ff_a_a_adffs_a8_a, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => state_a315,
	datac => state_a316,
	datad => reg_ff_a_a_adffs_a8_a,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op0_a16);

state_ast_wra_op0_a15_I : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op0_a15 = DFFE(state_ast_wra, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => state_ast_wra,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op0_a15);

state_ast_wra_op1_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op1 = DFFE(state_ast_wra_op0_a16 & (state_ast_wra_op0_a15), GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wra_op0_a16,
	datac => state_ast_wra_op0_a15,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op1);

state_ast_wra_op2_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op2 = DFFE(state_ast_wra_op1, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => state_ast_wra_op1,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op2);

state_ast_wra_op3_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op3 = DFFE(state_ast_wra_op2, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => state_ast_wra_op2,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op3);

state_ast_wra_op4_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wra_op4 = DFFE(state_ast_wra_op3, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => state_ast_wra_op3,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra_op4);

state_a318_I : apex20ke_lcell
-- Equation(s):
-- state_a318 = !wren_b_lat & (!state_ast_idle_a36 & !state_ast_wra_op0_a16 # !state_ast_idle_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "001F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_idle_a36,
	datab => state_ast_wra_op0_a16,
	datac => state_ast_idle_a35,
	datad => wren_b_lat,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => state_a318);

state_ast_idle_a35_I : apex20ke_lcell
-- Equation(s):
-- state_ast_idle_a35 = DFFE(!state_ast_wrb & !state_ast_wra_op4 & (wren_a_lat # !state_a318), GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0405",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datab => wren_a_lat,
	datac => state_ast_wra_op4,
	datad => state_a318,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_idle_a35);

state_a327_I : apex20ke_lcell
-- Equation(s):
-- state_a334 = wren_a_lat & (!state_ast_idle_a36 & !state_ast_wra_op0_a16 # !state_ast_idle_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1F00",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_idle_a36,
	datab => state_ast_wra_op0_a16,
	datac => state_ast_idle_a35,
	datad => wren_a_lat,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => state_a327,
	cascout => state_a334);

state_ast_wra_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wra = DFFE((last # !wren_b_lat) & CASCADE(state_a334), GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCFF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => last,
	datad => wren_b_lat,
	cascin => state_a334,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wra);

wren_a_lat_aI : apex20ke_lcell
-- Equation(s):
-- wren_a_lat = DFFE(wren_a_acombout # wren_a_lat & !state_ast_wra, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wren_a_lat,
	datac => state_ast_wra,
	datad => wren_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wren_a_lat);

state_a325_I : apex20ke_lcell
-- Equation(s):
-- state_a333 = wren_b_lat & (!state_ast_idle_a36 & !state_ast_wra_op0_a16 # !state_ast_idle_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1F00",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_idle_a36,
	datab => state_ast_wra_op0_a16,
	datac => state_ast_idle_a35,
	datad => wren_b_lat,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => state_a325,
	cascout => state_a333);

state_ast_wrb_aI : apex20ke_lcell
-- Equation(s):
-- state_ast_wrb = DFFE((!last # !wren_a_lat) & CASCADE(state_a333), GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "33FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wren_a_lat,
	datad => last,
	cascin => state_a333,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_wrb);

data_b_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(0),
	combout => data_b_a0_a_acombout);

wren_b_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_wren_b,
	combout => wren_b_acombout);

reg_ff_b_d_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a0_a = DFFE(data_b_a0_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a0_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a0_a);

data_mux_a0_a_a112_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a0_a_a112 = state_ast_wrb & (reg_ff_b_d_adffs_a0_a) # !state_ast_wrb & reg_ff_a_d_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a0_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_d_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a0_a_a112);

clk_b_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk_b,
	combout => clk_b_acombout);

Write_En_a0_I : apex20ke_lcell
-- Equation(s):
-- Write_En_a0 = state_ast_wrb # state_ast_wra

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => state_ast_wra,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Write_En_a0);

addr_a_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(0),
	combout => addr_a_a0_a_acombout);

reg_ff_a_a_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a0_a = DFFE(addr_a_a0_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a0_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a0_a);

addr_b_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(0),
	combout => addr_b_a0_a_acombout);

reg_ff_b_a_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a0_a = DFFE(addr_b_a0_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a0_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a0_a);

addr_mux_a0_a_a63_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a0_a_a63 = state_ast_wrb & (reg_ff_b_a_adffs_a0_a) # !state_ast_wrb & (reg_ff_a_a_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_a_adffs_a0_a,
	datad => reg_ff_b_a_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a0_a_a63);

addr_b_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(1),
	combout => addr_b_a1_a_acombout);

reg_ff_b_a_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a1_a = DFFE(addr_b_a1_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a1_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a1_a);

addr_mux_a1_a_a64_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a1_a_a64 = state_ast_wrb & (reg_ff_b_a_adffs_a1_a) # !state_ast_wrb & (reg_ff_a_a_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_a_adffs_a1_a,
	datad => reg_ff_b_a_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a1_a_a64);

addr_b_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(2),
	combout => addr_b_a2_a_acombout);

reg_ff_b_a_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a2_a = DFFE(addr_b_a2_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a2_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a2_a);

addr_mux_a2_a_a65_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a2_a_a65 = state_ast_wrb & (reg_ff_b_a_adffs_a2_a) # !state_ast_wrb & (reg_ff_a_a_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_a_adffs_a2_a,
	datad => reg_ff_b_a_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a2_a_a65);

addr_b_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(3),
	combout => addr_b_a3_a_acombout);

reg_ff_b_a_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a3_a = DFFE(addr_b_a3_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a3_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a3_a);

addr_mux_a3_a_a66_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a3_a_a66 = state_ast_wrb & (reg_ff_b_a_adffs_a3_a) # !state_ast_wrb & reg_ff_a_a_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_a_adffs_a3_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_a_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a3_a_a66);

addr_b_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(4),
	combout => addr_b_a4_a_acombout);

reg_ff_b_a_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a4_a = DFFE(addr_b_a4_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a4_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a4_a);

addr_mux_a4_a_a67_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a4_a_a67 = state_ast_wrb & (reg_ff_b_a_adffs_a4_a) # !state_ast_wrb & reg_ff_a_a_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_a_adffs_a4_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_a_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a4_a_a67);

addr_b_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(5),
	combout => addr_b_a5_a_acombout);

reg_ff_b_a_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a5_a = DFFE(addr_b_a5_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a5_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a5_a);

addr_mux_a5_a_a68_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a5_a_a68 = state_ast_wrb & (reg_ff_b_a_adffs_a5_a) # !state_ast_wrb & reg_ff_a_a_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_a_adffs_a5_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_a_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a5_a_a68);

addr_a_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_a(6),
	combout => addr_a_a6_a_acombout);

reg_ff_a_a_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_a_adffs_a6_a = DFFE(addr_a_a6_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_a_a6_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_a_adffs_a6_a);

addr_b_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(6),
	combout => addr_b_a6_a_acombout);

reg_ff_b_a_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a6_a = DFFE(addr_b_a6_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a6_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a6_a);

addr_mux_a6_a_a69_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a6_a_a69 = state_ast_wrb & (reg_ff_b_a_adffs_a6_a) # !state_ast_wrb & reg_ff_a_a_adffs_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_a_adffs_a6_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_a_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a6_a_a69);

addr_b_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(7),
	combout => addr_b_a7_a_acombout);

reg_ff_b_a_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_a_adffs_a7_a = DFFE(addr_b_a7_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => addr_b_a7_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_a_adffs_a7_a);

addr_mux_a7_a_a70_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a7_a_a70 = state_ast_wrb & reg_ff_b_a_adffs_a7_a # !state_ast_wrb & (reg_ff_a_a_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_b_a_adffs_a7_a,
	datac => state_ast_wrb,
	datad => reg_ff_a_a_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a7_a_a70);

addr_mux_a8_a_a71_I : apex20ke_lcell
-- Equation(s):
-- addr_mux_a8_a_a71 = state_ast_wrb & reg_ff_b_a_adffs_a8_a # !state_ast_wrb & (reg_ff_a_a_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reg_ff_b_a_adffs_a8_a,
	datac => state_ast_wrb,
	datad => reg_ff_a_a_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => addr_mux_a8_a_a71);

dpram_ram_a_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a0_a_a112,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a0_a_modesel,
	dataout => dpram_ram_a_asram_aq_a0_a);

data_a_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(1),
	combout => data_a_a1_a_acombout);

reg_ff_a_d_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a1_a = DFFE(data_a_a1_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a1_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a1_a);

data_b_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(1),
	combout => data_b_a1_a_acombout);

reg_ff_b_d_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a1_a = DFFE(data_b_a1_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a1_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a1_a);

data_mux_a1_a_a113_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a1_a_a113 = state_ast_wrb & (reg_ff_b_d_adffs_a1_a) # !state_ast_wrb & reg_ff_a_d_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a1_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_d_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a1_a_a113);

dpram_ram_a_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a1_a_a113,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a1_a_modesel,
	dataout => dpram_ram_a_asram_aq_a1_a);

data_a_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(2),
	combout => data_a_a2_a_acombout);

reg_ff_a_d_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a2_a = DFFE(data_a_a2_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a2_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a2_a);

data_b_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(2),
	combout => data_b_a2_a_acombout);

reg_ff_b_d_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a2_a = DFFE(data_b_a2_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a2_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a2_a);

data_mux_a2_a_a114_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a2_a_a114 = state_ast_wrb & (reg_ff_b_d_adffs_a2_a) # !state_ast_wrb & reg_ff_a_d_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a2_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_d_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a2_a_a114);

dpram_ram_a_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a2_a_a114,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a2_a_modesel,
	dataout => dpram_ram_a_asram_aq_a2_a);

data_a_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(3),
	combout => data_a_a3_a_acombout);

reg_ff_a_d_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a3_a = DFFE(data_a_a3_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a3_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a3_a);

data_b_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(3),
	combout => data_b_a3_a_acombout);

reg_ff_b_d_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a3_a = DFFE(data_b_a3_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a3_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a3_a);

data_mux_a3_a_a115_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a3_a_a115 = state_ast_wrb & (reg_ff_b_d_adffs_a3_a) # !state_ast_wrb & reg_ff_a_d_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a3_a,
	datad => reg_ff_b_d_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a3_a_a115);

dpram_ram_a_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a3_a_a115,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a3_a_modesel,
	dataout => dpram_ram_a_asram_aq_a3_a);

data_a_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(4),
	combout => data_a_a4_a_acombout);

reg_ff_a_d_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a4_a = DFFE(data_a_a4_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a4_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a4_a);

data_b_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(4),
	combout => data_b_a4_a_acombout);

reg_ff_b_d_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a4_a = DFFE(data_b_a4_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a4_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a4_a);

data_mux_a4_a_a116_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a4_a_a116 = state_ast_wrb & (reg_ff_b_d_adffs_a4_a) # !state_ast_wrb & reg_ff_a_d_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a4_a,
	datad => reg_ff_b_d_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a4_a_a116);

dpram_ram_a_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a4_a_a116,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a4_a_modesel,
	dataout => dpram_ram_a_asram_aq_a4_a);

data_a_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(5),
	combout => data_a_a5_a_acombout);

reg_ff_a_d_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a5_a = DFFE(data_a_a5_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a5_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a5_a);

data_b_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(5),
	combout => data_b_a5_a_acombout);

reg_ff_b_d_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a5_a = DFFE(data_b_a5_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a5_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a5_a);

data_mux_a5_a_a117_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a5_a_a117 = state_ast_wrb & (reg_ff_b_d_adffs_a5_a) # !state_ast_wrb & reg_ff_a_d_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a5_a,
	datac => state_ast_wrb,
	datad => reg_ff_b_d_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a5_a_a117);

dpram_ram_a_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a5_a_a117,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a5_a_modesel,
	dataout => dpram_ram_a_asram_aq_a5_a);

data_a_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(6),
	combout => data_a_a6_a_acombout);

reg_ff_a_d_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a6_a = DFFE(data_a_a6_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a6_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a6_a);

data_b_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(6),
	combout => data_b_a6_a_acombout);

reg_ff_b_d_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a6_a = DFFE(data_b_a6_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a6_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a6_a);

data_mux_a6_a_a118_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a6_a_a118 = state_ast_wrb & (reg_ff_b_d_adffs_a6_a) # !state_ast_wrb & (reg_ff_a_d_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a6_a,
	datad => reg_ff_b_d_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a6_a_a118);

dpram_ram_a_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a6_a_a118,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a6_a_modesel,
	dataout => dpram_ram_a_asram_aq_a6_a);

data_a_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(7),
	combout => data_a_a7_a_acombout);

reg_ff_a_d_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a7_a = DFFE(data_a_a7_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a7_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a7_a);

data_b_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(7),
	combout => data_b_a7_a_acombout);

reg_ff_b_d_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a7_a = DFFE(data_b_a7_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a7_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a7_a);

data_mux_a7_a_a119_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a7_a_a119 = state_ast_wrb & (reg_ff_b_d_adffs_a7_a) # !state_ast_wrb & (reg_ff_a_d_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a7_a,
	datad => reg_ff_b_d_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a7_a_a119);

dpram_ram_a_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a7_a_a119,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a7_a_modesel,
	dataout => dpram_ram_a_asram_aq_a7_a);

data_a_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(8),
	combout => data_a_a8_a_acombout);

reg_ff_a_d_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a8_a = DFFE(data_a_a8_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a8_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a8_a);

data_b_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(8),
	combout => data_b_a8_a_acombout);

reg_ff_b_d_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a8_a = DFFE(data_b_a8_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a8_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a8_a);

data_mux_a8_a_a120_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a8_a_a120 = state_ast_wrb & (reg_ff_b_d_adffs_a8_a) # !state_ast_wrb & (reg_ff_a_d_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA50",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a8_a,
	datad => reg_ff_b_d_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a8_a_a120);

dpram_ram_a_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a8_a_a120,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a8_a_modesel,
	dataout => dpram_ram_a_asram_aq_a8_a);

data_a_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(9),
	combout => data_a_a9_a_acombout);

reg_ff_a_d_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a9_a = DFFE(data_a_a9_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a9_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a9_a);

data_b_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(9),
	combout => data_b_a9_a_acombout);

reg_ff_b_d_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a9_a = DFFE(data_b_a9_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a9_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a9_a);

data_mux_a9_a_a121_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a9_a_a121 = state_ast_wrb & (reg_ff_b_d_adffs_a9_a) # !state_ast_wrb & reg_ff_a_d_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a9_a,
	datad => reg_ff_b_d_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a9_a_a121);

dpram_ram_a_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a9_a_a121,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a9_a_modesel,
	dataout => dpram_ram_a_asram_aq_a9_a);

data_a_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(10),
	combout => data_a_a10_a_acombout);

reg_ff_a_d_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a10_a = DFFE(data_a_a10_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a10_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a10_a);

data_b_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(10),
	combout => data_b_a10_a_acombout);

reg_ff_b_d_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a10_a = DFFE(data_b_a10_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a10_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a10_a);

data_mux_a10_a_a122_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a10_a_a122 = state_ast_wrb & (reg_ff_b_d_adffs_a10_a) # !state_ast_wrb & reg_ff_a_d_adffs_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a10_a,
	datac => reg_ff_b_d_adffs_a10_a,
	datad => state_ast_wrb,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a10_a_a122);

dpram_ram_a_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a10_a_a122,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a10_a_modesel,
	dataout => dpram_ram_a_asram_aq_a10_a);

data_a_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(11),
	combout => data_a_a11_a_acombout);

reg_ff_a_d_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a11_a = DFFE(data_a_a11_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a11_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a11_a);

data_b_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(11),
	combout => data_b_a11_a_acombout);

reg_ff_b_d_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a11_a = DFFE(data_b_a11_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a11_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a11_a);

data_mux_a11_a_a123_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a11_a_a123 = state_ast_wrb & (reg_ff_b_d_adffs_a11_a) # !state_ast_wrb & reg_ff_a_d_adffs_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a11_a,
	datad => reg_ff_b_d_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a11_a_a123);

dpram_ram_a_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a11_a_a123,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a11_a_modesel,
	dataout => dpram_ram_a_asram_aq_a11_a);

data_b_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(12),
	combout => data_b_a12_a_acombout);

reg_ff_b_d_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a12_a = DFFE(data_b_a12_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a12_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a12_a);

data_a_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(12),
	combout => data_a_a12_a_acombout);

reg_ff_a_d_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a12_a = DFFE(data_a_a12_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a12_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a12_a);

data_mux_a12_a_a124_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a12_a_a124 = state_ast_wrb & reg_ff_b_d_adffs_a12_a # !state_ast_wrb & (reg_ff_a_d_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_b_d_adffs_a12_a,
	datad => reg_ff_a_d_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a12_a_a124);

dpram_ram_a_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a12_a_a124,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a12_a_modesel,
	dataout => dpram_ram_a_asram_aq_a12_a);

data_a_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(13),
	combout => data_a_a13_a_acombout);

reg_ff_a_d_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a13_a = DFFE(data_a_a13_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a13_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a13_a);

data_b_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(13),
	combout => data_b_a13_a_acombout);

reg_ff_b_d_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a13_a = DFFE(data_b_a13_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a13_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a13_a);

data_mux_a13_a_a125_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a13_a_a125 = state_ast_wrb & (reg_ff_b_d_adffs_a13_a) # !state_ast_wrb & reg_ff_a_d_adffs_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => reg_ff_a_d_adffs_a13_a,
	datac => reg_ff_b_d_adffs_a13_a,
	datad => state_ast_wrb,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a13_a_a125);

dpram_ram_a_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a13_a_a125,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a13_a_modesel,
	dataout => dpram_ram_a_asram_aq_a13_a);

data_a_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(14),
	combout => data_a_a14_a_acombout);

reg_ff_a_d_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a14_a = DFFE(data_a_a14_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a14_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a14_a);

data_b_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(14),
	combout => data_b_a14_a_acombout);

reg_ff_b_d_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a14_a = DFFE(data_b_a14_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a14_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a14_a);

data_mux_a14_a_a126_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a14_a_a126 = state_ast_wrb & (reg_ff_b_d_adffs_a14_a) # !state_ast_wrb & reg_ff_a_d_adffs_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_a_d_adffs_a14_a,
	datad => reg_ff_b_d_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a14_a_a126);

dpram_ram_a_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a14_a_a126,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a14_a_modesel,
	dataout => dpram_ram_a_asram_aq_a14_a);

data_b_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_b(15),
	combout => data_b_a15_a_acombout);

reg_ff_b_d_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_b_d_adffs_a15_a = DFFE(data_b_a15_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_b_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_b_a15_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_b_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_b_d_adffs_a15_a);

data_a_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_a(15),
	combout => data_a_a15_a_acombout);

reg_ff_a_d_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- reg_ff_a_d_adffs_a15_a = DFFE(data_a_a15_a_acombout, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , wren_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_a_a15_a_acombout,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	ena => wren_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reg_ff_a_d_adffs_a15_a);

data_mux_a15_a_a127_I : apex20ke_lcell
-- Equation(s):
-- data_mux_a15_a_a127 = state_ast_wrb & reg_ff_b_d_adffs_a15_a # !state_ast_wrb & (reg_ff_a_d_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_wrb,
	datac => reg_ff_b_d_adffs_a15_a,
	datad => reg_ff_a_d_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_mux_a15_a_a127);

dpram_ram_a_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_a|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a15_a_a127,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_a_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => dpram_ram_a_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_a_asram_asegment_a0_a_a15_a_modesel,
	dataout => dpram_ram_a_asram_aq_a15_a);

addr_b_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_addr_b(8),
	combout => addr_b_a8_a_acombout);

dpram_ram_b_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a0_a_a112,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a0_a_modesel,
	dataout => dpram_ram_b_asram_aq_a0_a);

dpram_ram_b_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a1_a_a113,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a1_a_modesel,
	dataout => dpram_ram_b_asram_aq_a1_a);

dpram_ram_b_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a2_a_a114,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a2_a_modesel,
	dataout => dpram_ram_b_asram_aq_a2_a);

dpram_ram_b_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a3_a_a115,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a3_a_modesel,
	dataout => dpram_ram_b_asram_aq_a3_a);

dpram_ram_b_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a4_a_a116,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a4_a_modesel,
	dataout => dpram_ram_b_asram_aq_a4_a);

dpram_ram_b_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a5_a_a117,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a5_a_modesel,
	dataout => dpram_ram_b_asram_aq_a5_a);

dpram_ram_b_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a6_a_a118,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a6_a_modesel,
	dataout => dpram_ram_b_asram_aq_a6_a);

dpram_ram_b_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a7_a_a119,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a7_a_modesel,
	dataout => dpram_ram_b_asram_aq_a7_a);

dpram_ram_b_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a8_a_a120,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a8_a_modesel,
	dataout => dpram_ram_b_asram_aq_a8_a);

dpram_ram_b_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a9_a_a121,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a9_a_modesel,
	dataout => dpram_ram_b_asram_aq_a9_a);

dpram_ram_b_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a10_a_a122,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a10_a_modesel,
	dataout => dpram_ram_b_asram_aq_a10_a);

dpram_ram_b_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a11_a_a123,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a11_a_modesel,
	dataout => dpram_ram_b_asram_aq_a11_a);

dpram_ram_b_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a12_a_a124,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a12_a_modesel,
	dataout => dpram_ram_b_asram_aq_a12_a);

dpram_ram_b_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a13_a_a125,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a13_a_modesel,
	dataout => dpram_ram_b_asram_aq_a13_a);

dpram_ram_b_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a14_a_a126,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a14_a_modesel,
	dataout => dpram_ram_b_asram_aq_a14_a);

dpram_ram_b_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:dpram_ram_b|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 16,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data_mux_a15_a_a127,
	clk0 => clk_a_acombout,
	clk1 => clk_b_acombout,
	we => Write_En_a0,
	re => VCC,
	waddr => dpram_ram_b_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => dpram_ram_b_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dpram_ram_b_asram_asegment_a0_a_a15_a_modesel,
	dataout => dpram_ram_b_asram_aq_a15_a);

state_ast_idle_a36_I : apex20ke_lcell
-- Equation(s):
-- state_ast_idle_a36 = DFFE(!state_ast_wra, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => state_ast_wra,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => state_ast_idle_a36);

Select_a185_I : apex20ke_lcell
-- Equation(s):
-- Select_a185 = !state_ast_wra_op0_a16 & !state_ast_idle_a36 # !state_ast_idle_a35

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "333F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => state_ast_idle_a35,
	datac => state_ast_wra_op0_a16,
	datad => state_ast_idle_a36,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a185);

reduce_or_a2_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a2 = state_ast_wra_op3 # state_ast_wrb # state_ast_wra_op1 # Select_a185

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wra_op3,
	datab => state_ast_wrb,
	datac => state_ast_wra_op1,
	datad => Select_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a2);

reduce_or_a1_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a1 = state_ast_wra_op2 # state_ast_wra # state_ast_wra_op1 # Select_a185

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wra_op2,
	datab => state_ast_wra,
	datac => state_ast_wra_op1,
	datad => Select_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a1);

Select_a186_I : apex20ke_lcell
-- Equation(s):
-- Select_a186 = state_ast_wra_op0_a15 & state_ast_wra_op0_a16

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => state_ast_wra_op0_a15,
	datad => state_ast_wra_op0_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a186);

reduce_or_a0_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a0 = state_ast_wrb # state_ast_wra # Select_a186 # Select_a185

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wrb,
	datab => state_ast_wra,
	datac => Select_a186,
	datad => Select_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a0);

OpCodeISR_areg0_I : apex20ke_lcell
-- Equation(s):
-- OpCodeISR_areg0 = DFFE(state_ast_wra_op2 # state_ast_wra_op3 # state_ast_wra_op1 # state_ast_wra_op4, GLOBAL(clk_a_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => state_ast_wra_op2,
	datab => state_ast_wra_op3,
	datac => state_ast_wra_op1,
	datad => state_ast_wra_op4,
	clk => clk_a_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => OpCodeISR_areg0);

q_a_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(0));

q_a_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(1));

q_a_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(2));

q_a_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(3));

q_a_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(4));

q_a_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(5));

q_a_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(6));

q_a_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(7));

q_a_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(8));

q_a_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(9));

q_a_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(10));

q_a_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(11));

q_a_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(12));

q_a_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(13));

q_a_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(14));

q_a_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_a_asram_aq_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_a(15));

q_b_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(0));

q_b_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(1));

q_b_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(2));

q_b_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(3));

q_b_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(4));

q_b_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(5));

q_b_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(6));

q_b_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(7));

q_b_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(8));

q_b_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(9));

q_b_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(10));

q_b_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(11));

q_b_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(12));

q_b_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(13));

q_b_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(14));

q_b_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => dpram_ram_b_asram_aq_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_q_b(15));

state_str_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_reduce_or_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_state_str(0));

state_str_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_reduce_or_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_state_str(1));

state_str_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_reduce_or_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_state_str(2));

state_str_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_state_str(3));

OpCodeISR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_OpCodeISR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_OpCodeISR);
END structure;


