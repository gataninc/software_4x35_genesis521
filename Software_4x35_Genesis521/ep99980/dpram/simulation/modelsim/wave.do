onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/ao_clk
add wave -noupdate -format Logic -radix binary /tb/dsp_h3
add wave -noupdate -format Logic -radix binary /tb/dp_wrr
add wave -noupdate -format Logic -radix binary /tb/dp_wrl
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_dpra
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_dpra
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_dq
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_d
add wave -noupdate -format Literal -radix hexadecimal /tb/dp_pci_d
add wave -noupdate -format Literal -radix hexadecimal /tb/dp_dsp_d
add wave -noupdate -format Logic -radix binary /tb/pci_dsp_irq
add wave -noupdate -format Literal -radix hexadecimal /tb/state_str
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {8044355 ps}
WaveRestoreZoom {0 ps} {105 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
