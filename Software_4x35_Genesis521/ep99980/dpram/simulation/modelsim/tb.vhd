library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
	
entity tb is
end tb;

architecture test_bench of tb is

	signal imr 		: std_logic := '1';
	signal ao_clk 		: std_logic := '0';
	signal dsp_h3		: std_logic := '0';
	signal DP_WRR		: std_logic;
	signal DP_WRL		: std_logic;					-- Write from DSP
	signal PCI_DPRA	: std_logic_Vector( 11 downto 0 );		-- PCI Bus Address
	signal DSP_DPRA	: std_logic_vector( 11 downto 0 );		-- DSP Bus Address
	signal PCI_DQ		: std_logic_Vector( 15 downto 0 );		-- Input Data from PCI Bus
	signal DSP_D		: std_logic_Vector( 15 downto 0 );
	signal DP_PCI_D	: std_logic_Vector( 15 downto 0 );
	signal DP_DSP_D	: std_logic_Vector( 15 downto 0 );
	signal PCI_DSP_IRQ	: std_logic;
	signal state_str	: std_logic_Vector( 3 downto 0 );

	-------------------------------------------------------------------------------
	component DPRAM
		port(
			imr			: in		std_logic;
			clk_a		: in		std_logic;
			clk_b		: in		std_logic;
			wren_a		: in		std_logic;
			wren_b		: in 	std_logic;
			addr_a		: in		std_logic_Vector(   8 downto 0 );
			addr_b		: in		std_Logic_Vector(   8 downto 0 );
			data_a		: IN 	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
			data_b		: IN 	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
			q_a 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
			q_b 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
			state_str		: OUT 	STD_LOGIC_VECTOR(  3 DOWNTO 0 );
			OpCodeIsr		: out 	std_Logic );
	END component;			-- DPRAM;
	-------------------------------------------------------------------------------
begin
	imr 		<= '0' after 645 ns;
	ao_clk	<= not( ao_clk ) after 32 ns;
	dsp_H3	<= not( dsp_h3 ) after 33 ns;

	-----------------------------------------------------------------------------------------
	U : DPRAM
		port map(
			imr				=> imr,
			clk_a			=> AO_CLK,
			clk_b			=> DSP_H3,
			wren_a			=> DP_WRR,					-- Write from PCI Bus
			wren_b			=> DP_WRL,					-- Write from DSP
			addr_a			=> PCI_DPRA( 8 downto 0 ),		-- PCI Bus Address
			addr_b			=> DSP_DPRA( 8 downto 0 ),		-- DSP Bus Address
			data_a			=> PCI_DQ( 15 downto 0 ),		-- Input Data from PCI Bus
			data_b			=> DSP_D( 15 downto 0 ),
			q_a				=> DP_PCI_D,
			q_b 				=> DP_DSP_D,
			state_str			=> state_str,
			OpCodeIsr			=> PCI_DSP_IRQ );
	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	AO_PROC : process
	begin
		DP_WRR		<= '0';
		PCI_DPRA		<= x"000";
		PCI_DQ		<= x"0000";
		wait for 5 us;
		
		for i in 0 to 10 loop
			wait until (( AO_CLk'Event ) and ( AO_CLK = '1' ));
			wait for 5 ns;
			PCI_DPRA		<= conv_std_logic_Vector( i, 12 );
			PCI_DQ		<= conv_std_logic_Vector( i+1, 16 );
			DP_WRR 		<= '1';
			wait until (( AO_CLk'Event ) and ( AO_CLK = '1' ));
			wait for 5 ns;
			DP_WRR		<= '0';
			wait for 5 us;
		end loop;
		
		wait ;
	end process;
	-----------------------------------------------------------------------------------------
	
	-----------------------------------------------------------------------------------------
	DSP_PROC : process
	begin
		DP_WRL		<= '0';
		DSP_DPRA		<= x"000";
		DSP_D		<= x"0000";
		
		for i in 0 to 10 loop
			wait for 4.90 us;
			wait until(( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			wait for  4 ns;
			DSP_DPRA		<= conv_std_logic_Vector( i+23, 12 );
			DSP_D		<= conv_std_logic_Vector( i+44, 16 );
			DP_WRL		<= '1';
			wait until(( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			wait for  4 ns;
			DP_WRL		<= '0';
		end loop;
	end process;
	-----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------
end test_bench;
----------------------------------------------------------------------------------------------
