-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DPRAM.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--		This is a "Generic DPRAM module" consisting of two banks of RAM.
--		Each bank of RAM is used a a read for the respective port.
--		The write is accomplished by writing the value into both banks in a small
--		sequencer.
--
--		The "Read" side from the memories is asynchronous 
--
--		The only constraint is that CLK_A >= CLK_B
--		
--		PORT A is the PCI Bus Side, Read can be registered
--
--		PORT B is the DSP Side
--
--	History
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
library LPM;
     use LPM.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
entity DPRAM is
	port(
		IMR			: in	 	std_logic;
		clk_a		: in	 	std_logic;
		clk_b		: in	 	std_logic;
		wren_a		: in	 	std_logic;
		wren_b		: in  	std_logic;
		addr_a		: in	 	std_logic_Vector(   8 downto 0 );
		addr_b		: in	 	std_Logic_Vector(   8 downto 0 );
		data_a		: IN  	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
		data_b		: IN  	STD_LOGIC_VECTOR(  15 DOWNTO 0 );
		q_a 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
		q_b 			: OUT 	STD_LOGIC_VECTOR( 15 DOWNTO 0 );
		state_str		: out	std_Logic_Vector( 3 downto 0 );
		OpCodeISR		: out 	std_logic );
END DPRAM;
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
ARCHITECTURE behavioral OF dpram IS
	constant ZEROES : std_logic_Vector( 31 downto 0 ) := "00000000000000000000000000000000";

	type state_type is (
		st_idle,
		st_wra,
		st_wra_op0,
		st_wra_op1,
		st_wra_op2,
		st_wra_op3,
		st_wra_op4,
		st_wrb );

	signal state		: state_type;
	
	signal wren_a_lat	: std_logic;
	signal wren_b_lat 	: std_logic;

	signal addr_mux 	: std_Logic_vector(  8 downto 0 );
	signal reg_a_a		: std_Logic_vector(  8 downto 0 );
	signal reg_b_a		: std_Logic_vector(  8 downto 0 );
	signal reg_a_d		: std_logic_vector( 15 downto 0 );
	signal reg_b_d		: std_logic_Vector( 15 downto 0 );
	signal data_mux	: std_logic_Vector( 15 downto 0 );
	signal Write_En	: std_logic;
	signal idle_state	: std_logic;
	signal wren_b_ca	: std_logic;
	signal wren_b_cb	: std_logic;
	signal last		: std_logic;

BEGIN
	with state select
		state_str <= x"0" when st_idle,
				   x"1" when st_wra,
				   x"2" when st_wrb,
				   x"3" when st_wra_op0,
				   x"4" when st_wra_op1,
				   x"5" when st_wra_op2,
				   x"6" when st_wra_op3,
				   x"7" when st_wra_op4,
				   x"F" when others;
				
				

	-- Memory for A Port			
	idle_state 	<= '1' 		when ( state = ST_IDLE ) else '0';
	Write_En 	 	<= '1' 		when (( state = ST_WRA ) or ( State = ST_WRB )) else '0';
	data_mux		<= reg_b_d	when ( state = ST_WRB ) else reg_a_d;
	addr_mux		<= reg_b_a	when ( state = ST_WRB ) else reg_a_a;

	-- Holding Register for Write to Memory from Port A
	reg_ff_a_d : lpm_ff
		generic map(
			LPM_WIDTH				=> 16,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk_a,
			enable				=> wren_a,
			data					=> data_a,
			q					=> reg_a_d );
			
	reg_ff_a_a : lpm_ff
		generic map(
			LPM_WIDTH				=> 9,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk_a,
			enable				=> wren_a,
			data					=> addr_a,
			q					=> reg_a_a );
			
	-- Holding Register for Write to Memory from Port B 
	reg_ff_b_d : lpm_ff
		generic map(
			LPM_WIDTH				=> 16,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk_a, -- clk_b,
			enable				=> wren_b,
			data					=> data_b,
			q					=> reg_b_d );
			
	reg_ff_b_a : lpm_ff
		generic map(
			LPM_WIDTH				=> 9,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk_a, -- clk_b,
			enable				=> wren_b,
			data					=> addr_b,
			q					=> reg_b_a );
			
	-- Memory for A Port
	dpram_ram_a : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=>  9,
			LPM_FILE				=> "INIT.MIF",
			LPM_INDATA 			=> "REGISTERED",
     		LPM_WRADDRESS_CONTROL 	=> "REGISTERED",
	   		LPM_OUTDATA			=> "REGISTERED",
	   		LPM_RDADDRESS_CONTROL 	=> "REGISTERED" )
  		port map(
			wrclock				=> clk_a,
			rdclock				=> clk_b,
			wren					=> Write_En,			-- Write Enable
			data					=> Data_Mux,			-- Write Data
			wraddress				=> Addr_Mux,			-- Write Address
			rdaddress				=> addr_a,			-- Read Address
			q					=> q_a );

	-- Memory for B Port
	dpram_ram_b : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=> 9,
			LPM_FILE				=> "INIT.MIF",
			LPM_INDATA 			=> "REGISTERED",
     		LPM_WRADDRESS_CONTROL 	=> "REGISTERED",
	   		LPM_OUTDATA			=> "REGISTERED",
	   		LPM_RDADDRESS_CONTROL 	=> "REGISTERED" )
		port map(
			wrclock				=> clk_a,
			rdclock				=> clk_b,
			wren					=> Write_En,			-- Write Enable
			data					=> Data_Mux,			-- Write Data
			wraddress				=> Addr_Mux,			-- Write Address
			rdaddress				=> addr_b,			-- Read Address
			q					=> q_b );
     ------------------------------------------------------------------------------------

	clockb_proc : process( imr, clk_b )
	begin
		if( imr = '1' ) then 
			wren_b_cb <= '0';
		elsif(( clk_b'Event ) and ( Clk_b = '1' )) then
			if( wren_b = '1' )
				then wren_b_cb <= '1';
			elsif( wren_b_ca = '1' )
				then wren_b_cb <= '0';
			end if;
		end if;
	end process;
     ------------------------------------------------------------------------------------

     ------------------------------------------------------------------------------------
	clocka_proc : process( imr, clk_a )

	begin
		if( imr = '1' ) then
			last			<= '0';
			wren_b_ca		<= '0';
			Wren_A_Lat 	<= '0';
			Wren_b_lat 	<= '0';
			state		<= st_idle;
			OpCodeIsr		<= '1';
		elsif(( clk_a'event ) and ( clk_a = '1' )) then
			wren_b_ca		<= wren_b_cb;
		
			if( wren_a = '1' )
				then Wren_a_lat <= '1';
			elsif( state = st_wra )
				then WrEn_A_Lat <= '0';
			end if;

			if( wren_b_ca = '1' )
				then Wren_b_lat <= '1';
			elsif( state = st_wrb )
				then WrEn_b_Lat <= '0';
			end if;
			
			if( state = st_wra )
				then last <= '0';
			elsif( state = st_wrb )
				then last <= '1';
			end if;

			case state is
				when st_idle =>
					if(( wren_a_lat = '1' ) and ( wren_b_lat = '1' ) and ( last = '0' ))	-- Last was A
						then state <= st_wrb;
					elsif(( wren_a_lat = '1' ) and ( wren_b_lat = '1' ) and ( last = '1' )) -- Last was B
						then state <= st_wra;
					elsif(( wren_a_lat = '1' ) and ( wren_b_lat = '0' ))
						then state <= st_wra;
					elsif(( wren_a_lat = '0' ) and ( wren_b_lat = '1' ))
						then state <= st_wrb;
					end if;
					
				when st_wra => if( conv_integer( reg_a_a ) = 0 ) 		-- OpCode
								then state <= st_wra_op0;		-- Assert OpCode Interrupt
							elsif( Conv_integer( reg_a_a ) = 3 )	-- Interrupt Acknowledge
								then state <= st_wra_op0;
								else state <= st_idle;
							end if;
				when st_wra_op0 =>	state <= st_wra_op1;
				when st_wra_op1 =>	state <= st_wra_op2;
				when st_wra_op2 =>	state <= st_wra_op3;
				when st_wra_op3 =>	state <= st_wra_op4;
				when st_wra_op4 =>	state <= st_idle;
				
				when st_wrb	 =>  state <= st_idle;
				when others 	 =>	state <= st_idle;
			end case;

			case state is
				when st_wra_op1 => OpCodeIsr <= '0';
				when st_wra_op2 => OpCodeIsr <= '0';
				when st_wra_op3 => OpCodeIsr <= '0';
				when st_wra_op4 => OpCodeIsr <= '0';
				when others	 => OpCodeIsr <= '1';
			end case;
		end if;
	end process;
-----------------------------------------------------------------------------------------
END behavioral;		-- DPRAM.VHD
-----------------------------------------------------------------------------------------

