-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FFCNT.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		November 2001 - MCS
--			Module has been debuggged and implemented in the DPP-II Board
--
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
library LPM;
     use LPM.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
entity FFCNT is 
	port( 
     	IMR            : in      std_logic;                  		-- Local Master Reset
		CLK40		: in		std_logic;
		FF_REN		: in		std_logic;
		FF_WEN		: in		std_logic;
		FF_RS		: in		std_Logic;
		FF_HF		: buffer	std_logic;
		ff_cnt		: buffer	std_logic_Vector( 15 downto 0 ) );
end FFCNT;
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
architecture behavioral of FFCNT is
	signal FF_RENV			: std_logic_Vector( 1 downto 0 );
	signal FF_WENV			: std_logic_Vector( 1 downto 0 );
	signal HF_CNT			: std_logic_Vector( 15 downto 0 );
	signal MF_CNT			: std_logic_Vector( 15 downto 0 );
	signal Max_Cnt			: std_logic;
	signal Min_Cnt			: std_logic;
	signal Cnt_Inc			: std_logic;
	signal Cnt_Dec			: std_logic;
-----------------------------------------------------------------------------------------
begin
	HF_CNT	<= Conv_std_logic_Vector( 8192, 16 );
	MF_CNT	<= Conv_std_logic_vector( 16384, 16 );

	HALF_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> ff_cnt,
			datab			=> HF_CNT,
			ageb				=> FF_HF );

--	FF_CNT_LATCH : lpm_latch
--		generic map(
--			LPM_WIDTH		=> 16,
--			LPM_TYPE		=> "LPM_LATCH" )
--		port map(
--			aclr			=> imr,
--			gate 		=> OPTATN,			-- when 1 - Flow Through, when 0 - Latched
--			data			=> iFF_CNT,
--			q			=> FF_CNT );

--	FF_CNT <= iFF_CNT;


	Max_Cnt 	<= '1' when ( ff_cnt = MF_CNT ) else '0';
	Min_Cnt 	<= '1' when ( ff_cnt = x"0000" ) else '0';
	Cnt_Inc	<= '1' when ( FF_WENV = "10" ) else '0';
	Cnt_Dec	<= '1' when ( FF_RENV = "10" ) else '0';
     ------------------------------------------------------------------------------------
     PCI_PROC : process( CLK40, IMR )

     begin
          if( IMR = '1' ) then
			FF_RENV	<= "00";
			FF_WENV	<= "00";
			ff_cnt	<= x"0000";

          elsif(( CLK40'Event ) and ( CLK40 = '1' )) then
			FF_RENV	<= FF_RENV(0) & FF_REN;
			FF_WENV	<= FF_WENV(0) & FF_WEN;
			
			if( FF_RS = '1' )
				then ff_cnt <= x"0000";
			elsif(( Cnt_Inc = '1' ) and ( Cnt_Dec = '0' ) and ( Max_Cnt = '0' ))
				then ff_cnt <= ff_cnt + 1;
			elsif(( Cnt_Inc = '0' ) and ( Cnt_Dec = '1' ) and ( Min_Cnt = '0' ))
				then ff_cnt <= ff_cnt - 1;
			end if;				
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
end behavioral;			-- FFCNT.VHD
-----------------------------------------------------------------------------------------

