onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk40
add wave -noupdate -format Logic -radix binary /tb/ff_ren
add wave -noupdate -format Logic -radix binary /tb/ff_wen
add wave -noupdate -format Logic -radix binary /tb/ff_rs
add wave -noupdate -format Logic -radix binary /tb/ff_hf
add wave -noupdate -format Analog-Step -radix decimal /tb/ff_cnt
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {8044 ns}
WaveRestoreZoom {1662 ns} {13932 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 39
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
