-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 11:11:36"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	FFCNT IS
    PORT (
	IMR : IN std_logic;
	CLK40 : IN std_logic;
	FF_REN : IN std_logic;
	FF_WEN : IN std_logic;
	FF_RS : IN std_logic;
	FF_HF : OUT std_logic;
	ff_cnt : OUT std_logic_vector(15 DOWNTO 0)
	);
END FFCNT;

ARCHITECTURE structure OF FFCNT IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK40 : std_logic;
SIGNAL ww_FF_REN : std_logic;
SIGNAL ww_FF_WEN : std_logic;
SIGNAL ww_FF_RS : std_logic;
SIGNAL ww_FF_HF : std_logic;
SIGNAL ww_ff_cnt : std_logic_vector(15 DOWNTO 0);
SIGNAL HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15 : std_logic;
SIGNAL FF_WENV_a1_a : std_logic;
SIGNAL FF_RENV_a0_a : std_logic;
SIGNAL FF_RENV_a1_a : std_logic;
SIGNAL FF_WENV_a0_a : std_logic;
SIGNAL PCI_PROC_a49 : std_logic;
SIGNAL ff_cnt_a13_a_areg0 : std_logic;
SIGNAL PCI_PROC_a53 : std_logic;
SIGNAL rtl_a148 : std_logic;
SIGNAL rtl_a150 : std_logic;
SIGNAL FF_REN_acombout : std_logic;
SIGNAL FF_WEN_acombout : std_logic;
SIGNAL HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 : std_logic;
SIGNAL add_a1169 : std_logic;
SIGNAL add_a1173 : std_logic;
SIGNAL add_a1177 : std_logic;
SIGNAL add_a1181 : std_logic;
SIGNAL add_a1185 : std_logic;
SIGNAL add_a1189 : std_logic;
SIGNAL add_a1191 : std_logic;
SIGNAL add_a1187 : std_logic;
SIGNAL add_a1183 : std_logic;
SIGNAL add_a1179 : std_logic;
SIGNAL add_a1175 : std_logic;
SIGNAL add_a1171 : std_logic;
SIGNAL add_a1167 : std_logic;
SIGNAL CLK40_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL FF_RS_acombout : std_logic;
SIGNAL rtl_a157 : std_logic;
SIGNAL rtl_a153 : std_logic;
SIGNAL PCI_PROC_a52 : std_logic;
SIGNAL ff_cnt_a0_a_areg0 : std_logic;
SIGNAL ff_cnt_a0_a_a1153 : std_logic;
SIGNAL ff_cnt_a1_a_a1156 : std_logic;
SIGNAL ff_cnt_a2_a_areg0 : std_logic;
SIGNAL ff_cnt_a2_a_a1159 : std_logic;
SIGNAL ff_cnt_a3_a_a1162 : std_logic;
SIGNAL ff_cnt_a4_a_areg0 : std_logic;
SIGNAL ff_cnt_a4_a_a1165 : std_logic;
SIGNAL ff_cnt_a5_a_areg0 : std_logic;
SIGNAL ff_cnt_a5_a_a1168 : std_logic;
SIGNAL ff_cnt_a6_a_areg0 : std_logic;
SIGNAL add_a1193 : std_logic;
SIGNAL add_a1195 : std_logic;
SIGNAL ff_cnt_a6_a_a1171 : std_logic;
SIGNAL ff_cnt_a7_a_areg0 : std_logic;
SIGNAL ff_cnt_a3_a_areg0 : std_logic;
SIGNAL ff_cnt_a1_a_areg0 : std_logic;
SIGNAL rtl_a156 : std_logic;
SIGNAL rtl_a152 : std_logic;
SIGNAL PCI_PROC_a56 : std_logic;
SIGNAL add_a1197 : std_logic;
SIGNAL add_a1199 : std_logic;
SIGNAL ff_cnt_a7_a_a1174 : std_logic;
SIGNAL ff_cnt_a8_a_areg0 : std_logic;
SIGNAL add_a1201 : std_logic;
SIGNAL add_a1203 : std_logic;
SIGNAL ff_cnt_a8_a_a1177 : std_logic;
SIGNAL ff_cnt_a9_a_areg0 : std_logic;
SIGNAL add_a1205 : std_logic;
SIGNAL add_a1207 : std_logic;
SIGNAL ff_cnt_a9_a_a1180 : std_logic;
SIGNAL ff_cnt_a10_a_areg0 : std_logic;
SIGNAL add_a1209 : std_logic;
SIGNAL add_a1211 : std_logic;
SIGNAL ff_cnt_a10_a_a1183 : std_logic;
SIGNAL ff_cnt_a11_a_areg0 : std_logic;
SIGNAL add_a1213 : std_logic;
SIGNAL add_a1215 : std_logic;
SIGNAL ff_cnt_a11_a_a1186 : std_logic;
SIGNAL ff_cnt_a12_a_areg0 : std_logic;
SIGNAL add_a1217 : std_logic;
SIGNAL add_a1229 : std_logic;
SIGNAL add_a1219 : std_logic;
SIGNAL add_a1227 : std_logic;
SIGNAL ff_cnt_a12_a_a1189 : std_logic;
SIGNAL ff_cnt_a13_a_a1198 : std_logic;
SIGNAL ff_cnt_a14_a_areg0 : std_logic;
SIGNAL add_a1221 : std_logic;
SIGNAL add_a1223 : std_logic;
SIGNAL ff_cnt_a14_a_a1192 : std_logic;
SIGNAL ff_cnt_a15_a_areg0 : std_logic;
SIGNAL HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL HALF_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL ALT_INV_HALF_COMPARE_acomparator_acmp_end_aagb_out : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK40 <= CLK40;
ww_FF_REN <= FF_REN;
ww_FF_WEN <= FF_WEN;
ww_FF_RS <= FF_RS;
FF_HF <= ww_FF_HF;
ff_cnt <= ww_ff_cnt;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_HALF_COMPARE_acomparator_acmp_end_aagb_out <= NOT HALF_COMPARE_acomparator_acmp_end_aagb_out;

FF_WENV_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_WENV_a1_a = DFFE(FF_WENV_a0_a, GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => FF_WENV_a0_a,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_WENV_a1_a);

FF_RENV_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_RENV_a0_a = DFFE(FF_REN_acombout, GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_REN_acombout,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_RENV_a0_a);

FF_RENV_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_RENV_a1_a = DFFE(FF_RENV_a0_a, GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_RENV_a0_a,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_RENV_a1_a);

FF_WENV_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_WENV_a0_a = DFFE(FF_WEN_acombout, GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_WEN_acombout,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_WENV_a0_a);

PCI_PROC_a49_I : apex20ke_lcell
-- Equation(s):
-- PCI_PROC_a49 = !FF_WENV_a0_a & FF_WENV_a1_a & (FF_RENV_a0_a # !FF_RENV_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_WENV_a0_a,
	datab => FF_RENV_a0_a,
	datac => FF_RENV_a1_a,
	datad => FF_WENV_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_PROC_a49);

ff_cnt_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a13_a_areg0 = DFFE((PCI_PROC_a52 & add_a1227) # (!PCI_PROC_a52 & HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 $ ff_cnt_a12_a_a1189) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a13_a_a1198 = CARRY(!ff_cnt_a12_a_a1189 # !HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	datac => add_a1227,
	cin => ff_cnt_a12_a_a1189,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a13_a_areg0,
	cout => ff_cnt_a13_a_a1198);

PCI_PROC_a53_I : apex20ke_lcell
-- Equation(s):
-- PCI_PROC_a53 = !FF_RENV_a0_a & FF_RENV_a1_a & (FF_WENV_a0_a # !FF_WENV_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_WENV_a0_a,
	datab => FF_RENV_a0_a,
	datac => FF_RENV_a1_a,
	datad => FF_WENV_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_PROC_a53);

FF_REN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_REN,
	combout => FF_REN_acombout);

FF_WEN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_WEN,
	combout => FF_WEN_acombout);

HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16_I : apex20ke_lcell
-- Equation(s):
-- HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 = ff_cnt_a13_a_areg0
-- HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a = CARRY(!ff_cnt_a13_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "AA55",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	cout => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a);

add_a1167_I : apex20ke_lcell
-- Equation(s):
-- add_a1167 = ff_cnt_a0_a_areg0 $ PCI_PROC_a56
-- add_a1169 = CARRY(ff_cnt_a0_a_areg0 & PCI_PROC_a56)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a0_a_areg0,
	datab => PCI_PROC_a56,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1167,
	cout => add_a1169);

add_a1171_I : apex20ke_lcell
-- Equation(s):
-- add_a1171 = ff_cnt_a1_a_areg0 $ PCI_PROC_a56 $ add_a1169
-- add_a1173 = CARRY(ff_cnt_a1_a_areg0 & !PCI_PROC_a56 & !add_a1169 # !ff_cnt_a1_a_areg0 & (!add_a1169 # !PCI_PROC_a56))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a1_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1169,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1171,
	cout => add_a1173);

add_a1175_I : apex20ke_lcell
-- Equation(s):
-- add_a1175 = ff_cnt_a2_a_areg0 $ PCI_PROC_a56 $ !add_a1173
-- add_a1177 = CARRY(ff_cnt_a2_a_areg0 & (PCI_PROC_a56 # !add_a1173) # !ff_cnt_a2_a_areg0 & PCI_PROC_a56 & !add_a1173)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a2_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1173,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1175,
	cout => add_a1177);

add_a1179_I : apex20ke_lcell
-- Equation(s):
-- add_a1179 = ff_cnt_a3_a_areg0 $ PCI_PROC_a56 $ add_a1177
-- add_a1181 = CARRY(ff_cnt_a3_a_areg0 & !PCI_PROC_a56 & !add_a1177 # !ff_cnt_a3_a_areg0 & (!add_a1177 # !PCI_PROC_a56))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a3_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1177,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1179,
	cout => add_a1181);

add_a1183_I : apex20ke_lcell
-- Equation(s):
-- add_a1183 = ff_cnt_a4_a_areg0 $ PCI_PROC_a56 $ !add_a1181
-- add_a1185 = CARRY(ff_cnt_a4_a_areg0 & (PCI_PROC_a56 # !add_a1181) # !ff_cnt_a4_a_areg0 & PCI_PROC_a56 & !add_a1181)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a4_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1181,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1183,
	cout => add_a1185);

add_a1187_I : apex20ke_lcell
-- Equation(s):
-- add_a1187 = ff_cnt_a5_a_areg0 $ PCI_PROC_a56 $ add_a1185
-- add_a1189 = CARRY(ff_cnt_a5_a_areg0 & !PCI_PROC_a56 & !add_a1185 # !ff_cnt_a5_a_areg0 & (!add_a1185 # !PCI_PROC_a56))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a5_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1187,
	cout => add_a1189);

add_a1191_I : apex20ke_lcell
-- Equation(s):
-- add_a1191 = ff_cnt_a6_a_areg0 $ PCI_PROC_a56 $ !add_a1189
-- add_a1193 = CARRY(ff_cnt_a6_a_areg0 & (PCI_PROC_a56 # !add_a1189) # !ff_cnt_a6_a_areg0 & PCI_PROC_a56 & !add_a1189)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a6_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1191,
	cout => add_a1193);

CLK40_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK40,
	combout => CLK40_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

FF_RS_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_RS,
	combout => FF_RS_acombout);

rtl_a150_I : apex20ke_lcell
-- Equation(s):
-- rtl_a157 = !ff_cnt_a11_a_areg0 & !ff_cnt_a9_a_areg0 & !ff_cnt_a10_a_areg0 & !ff_cnt_a8_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a11_a_areg0,
	datab => ff_cnt_a9_a_areg0,
	datac => ff_cnt_a10_a_areg0,
	datad => ff_cnt_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a150,
	cascout => rtl_a157);

rtl_a153_I : apex20ke_lcell
-- Equation(s):
-- rtl_a153 = (!ff_cnt_a12_a_areg0 & !ff_cnt_a15_a_areg0 & !HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16) & CASCADE(rtl_a157)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a12_a_areg0,
	datac => ff_cnt_a15_a_areg0,
	datad => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	cascin => rtl_a157,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a153);

PCI_PROC_a52_I : apex20ke_lcell
-- Equation(s):
-- PCI_PROC_a52 = ff_cnt_a14_a_areg0 & rtl_a152 & rtl_a153 # !PCI_PROC_a49

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D555",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a49,
	datab => ff_cnt_a14_a_areg0,
	datac => rtl_a152,
	datad => rtl_a153,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_PROC_a52);

ff_cnt_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a0_a_areg0 = DFFE((PCI_PROC_a52 & add_a1167) # (!PCI_PROC_a52 & !ff_cnt_a0_a_areg0) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a0_a_a1153 = CARRY(ff_cnt_a0_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a0_a_areg0,
	datac => add_a1167,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a0_a_areg0,
	cout => ff_cnt_a0_a_a1153);

ff_cnt_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a1_a_areg0 = DFFE((PCI_PROC_a52 & add_a1171) # (!PCI_PROC_a52 & ff_cnt_a1_a_areg0 $ (ff_cnt_a0_a_a1153)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a1_a_a1156 = CARRY(!ff_cnt_a0_a_a1153 # !ff_cnt_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a1_a_areg0,
	datac => add_a1171,
	cin => ff_cnt_a0_a_a1153,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a1_a_areg0,
	cout => ff_cnt_a1_a_a1156);

ff_cnt_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a2_a_areg0 = DFFE((PCI_PROC_a52 & add_a1175) # (!PCI_PROC_a52 & ff_cnt_a2_a_areg0 $ !ff_cnt_a1_a_a1156) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a2_a_a1159 = CARRY(ff_cnt_a2_a_areg0 & !ff_cnt_a1_a_a1156)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a2_a_areg0,
	datac => add_a1175,
	cin => ff_cnt_a1_a_a1156,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a2_a_areg0,
	cout => ff_cnt_a2_a_a1159);

ff_cnt_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a3_a_areg0 = DFFE((PCI_PROC_a52 & add_a1179) # (!PCI_PROC_a52 & ff_cnt_a3_a_areg0 $ (ff_cnt_a2_a_a1159)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a3_a_a1162 = CARRY(!ff_cnt_a2_a_a1159 # !ff_cnt_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a3_a_areg0,
	datac => add_a1179,
	cin => ff_cnt_a2_a_a1159,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a3_a_areg0,
	cout => ff_cnt_a3_a_a1162);

ff_cnt_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a4_a_areg0 = DFFE((PCI_PROC_a52 & add_a1183) # (!PCI_PROC_a52 & ff_cnt_a4_a_areg0 $ !ff_cnt_a3_a_a1162) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a4_a_a1165 = CARRY(ff_cnt_a4_a_areg0 & !ff_cnt_a3_a_a1162)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a4_a_areg0,
	datac => add_a1183,
	cin => ff_cnt_a3_a_a1162,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a4_a_areg0,
	cout => ff_cnt_a4_a_a1165);

ff_cnt_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a5_a_areg0 = DFFE((PCI_PROC_a52 & add_a1187) # (!PCI_PROC_a52 & ff_cnt_a5_a_areg0 $ ff_cnt_a4_a_a1165) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a5_a_a1168 = CARRY(!ff_cnt_a4_a_a1165 # !ff_cnt_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a5_a_areg0,
	datac => add_a1187,
	cin => ff_cnt_a4_a_a1165,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a5_a_areg0,
	cout => ff_cnt_a5_a_a1168);

ff_cnt_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a6_a_areg0 = DFFE((PCI_PROC_a52 & add_a1191) # (!PCI_PROC_a52 & ff_cnt_a6_a_areg0 $ (!ff_cnt_a5_a_a1168)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a6_a_a1171 = CARRY(ff_cnt_a6_a_areg0 & (!ff_cnt_a5_a_a1168))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a6_a_areg0,
	datac => add_a1191,
	cin => ff_cnt_a5_a_a1168,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a6_a_areg0,
	cout => ff_cnt_a6_a_a1171);

add_a1195_I : apex20ke_lcell
-- Equation(s):
-- add_a1195 = ff_cnt_a7_a_areg0 $ PCI_PROC_a56 $ add_a1193
-- add_a1197 = CARRY(ff_cnt_a7_a_areg0 & !PCI_PROC_a56 & !add_a1193 # !ff_cnt_a7_a_areg0 & (!add_a1193 # !PCI_PROC_a56))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a7_a_areg0,
	datab => PCI_PROC_a56,
	cin => add_a1193,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1195,
	cout => add_a1197);

ff_cnt_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a7_a_areg0 = DFFE((PCI_PROC_a52 & add_a1195) # (!PCI_PROC_a52 & ff_cnt_a7_a_areg0 $ (ff_cnt_a6_a_a1171)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a7_a_a1174 = CARRY(!ff_cnt_a6_a_a1171 # !ff_cnt_a7_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a7_a_areg0,
	datac => add_a1195,
	cin => ff_cnt_a6_a_a1171,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a7_a_areg0,
	cout => ff_cnt_a7_a_a1174);

rtl_a148_I : apex20ke_lcell
-- Equation(s):
-- rtl_a156 = !ff_cnt_a2_a_areg0 & !ff_cnt_a3_a_areg0 & !ff_cnt_a0_a_areg0 & !ff_cnt_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a2_a_areg0,
	datab => ff_cnt_a3_a_areg0,
	datac => ff_cnt_a0_a_areg0,
	datad => ff_cnt_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a148,
	cascout => rtl_a156);

rtl_a152_I : apex20ke_lcell
-- Equation(s):
-- rtl_a152 = (!ff_cnt_a5_a_areg0 & !ff_cnt_a6_a_areg0 & !ff_cnt_a4_a_areg0 & !ff_cnt_a7_a_areg0) & CASCADE(rtl_a156)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a5_a_areg0,
	datab => ff_cnt_a6_a_areg0,
	datac => ff_cnt_a4_a_areg0,
	datad => ff_cnt_a7_a_areg0,
	cascin => rtl_a156,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a152);

PCI_PROC_a56_I : apex20ke_lcell
-- Equation(s):
-- PCI_PROC_a56 = PCI_PROC_a53 & (ff_cnt_a14_a_areg0 # !rtl_a153 # !rtl_a152)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8AAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a53,
	datab => ff_cnt_a14_a_areg0,
	datac => rtl_a152,
	datad => rtl_a153,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_PROC_a56);

add_a1199_I : apex20ke_lcell
-- Equation(s):
-- add_a1199 = PCI_PROC_a56 $ ff_cnt_a8_a_areg0 $ !add_a1197
-- add_a1201 = CARRY(PCI_PROC_a56 & (ff_cnt_a8_a_areg0 # !add_a1197) # !PCI_PROC_a56 & ff_cnt_a8_a_areg0 & !add_a1197)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a8_a_areg0,
	cin => add_a1197,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1199,
	cout => add_a1201);

ff_cnt_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a8_a_areg0 = DFFE((PCI_PROC_a52 & add_a1199) # (!PCI_PROC_a52 & ff_cnt_a8_a_areg0 $ !ff_cnt_a7_a_a1174) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a8_a_a1177 = CARRY(ff_cnt_a8_a_areg0 & !ff_cnt_a7_a_a1174)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a8_a_areg0,
	datac => add_a1199,
	cin => ff_cnt_a7_a_a1174,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a8_a_areg0,
	cout => ff_cnt_a8_a_a1177);

add_a1203_I : apex20ke_lcell
-- Equation(s):
-- add_a1203 = PCI_PROC_a56 $ ff_cnt_a9_a_areg0 $ add_a1201
-- add_a1205 = CARRY(PCI_PROC_a56 & !ff_cnt_a9_a_areg0 & !add_a1201 # !PCI_PROC_a56 & (!add_a1201 # !ff_cnt_a9_a_areg0))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a9_a_areg0,
	cin => add_a1201,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1203,
	cout => add_a1205);

ff_cnt_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a9_a_areg0 = DFFE((PCI_PROC_a52 & add_a1203) # (!PCI_PROC_a52 & ff_cnt_a9_a_areg0 $ ff_cnt_a8_a_a1177) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a9_a_a1180 = CARRY(!ff_cnt_a8_a_a1177 # !ff_cnt_a9_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a9_a_areg0,
	datac => add_a1203,
	cin => ff_cnt_a8_a_a1177,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a9_a_areg0,
	cout => ff_cnt_a9_a_a1180);

add_a1207_I : apex20ke_lcell
-- Equation(s):
-- add_a1207 = PCI_PROC_a56 $ ff_cnt_a10_a_areg0 $ !add_a1205
-- add_a1209 = CARRY(PCI_PROC_a56 & (ff_cnt_a10_a_areg0 # !add_a1205) # !PCI_PROC_a56 & ff_cnt_a10_a_areg0 & !add_a1205)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a10_a_areg0,
	cin => add_a1205,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1207,
	cout => add_a1209);

ff_cnt_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a10_a_areg0 = DFFE((PCI_PROC_a52 & add_a1207) # (!PCI_PROC_a52 & ff_cnt_a10_a_areg0 $ (!ff_cnt_a9_a_a1180)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a10_a_a1183 = CARRY(ff_cnt_a10_a_areg0 & (!ff_cnt_a9_a_a1180))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a10_a_areg0,
	datac => add_a1207,
	cin => ff_cnt_a9_a_a1180,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a10_a_areg0,
	cout => ff_cnt_a10_a_a1183);

add_a1211_I : apex20ke_lcell
-- Equation(s):
-- add_a1211 = PCI_PROC_a56 $ ff_cnt_a11_a_areg0 $ add_a1209
-- add_a1213 = CARRY(PCI_PROC_a56 & !ff_cnt_a11_a_areg0 & !add_a1209 # !PCI_PROC_a56 & (!add_a1209 # !ff_cnt_a11_a_areg0))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a11_a_areg0,
	cin => add_a1209,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1211,
	cout => add_a1213);

ff_cnt_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a11_a_areg0 = DFFE((PCI_PROC_a52 & add_a1211) # (!PCI_PROC_a52 & ff_cnt_a11_a_areg0 $ (ff_cnt_a10_a_a1183)) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a11_a_a1186 = CARRY(!ff_cnt_a10_a_a1183 # !ff_cnt_a11_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a11_a_areg0,
	datac => add_a1211,
	cin => ff_cnt_a10_a_a1183,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a11_a_areg0,
	cout => ff_cnt_a11_a_a1186);

add_a1215_I : apex20ke_lcell
-- Equation(s):
-- add_a1215 = PCI_PROC_a56 $ ff_cnt_a12_a_areg0 $ !add_a1213
-- add_a1217 = CARRY(PCI_PROC_a56 & (ff_cnt_a12_a_areg0 # !add_a1213) # !PCI_PROC_a56 & ff_cnt_a12_a_areg0 & !add_a1213)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a12_a_areg0,
	cin => add_a1213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1215,
	cout => add_a1217);

ff_cnt_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a12_a_areg0 = DFFE((PCI_PROC_a52 & add_a1215) # (!PCI_PROC_a52 & ff_cnt_a12_a_areg0 $ !ff_cnt_a11_a_a1186) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a12_a_a1189 = CARRY(ff_cnt_a12_a_areg0 & !ff_cnt_a11_a_a1186)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a12_a_areg0,
	datac => add_a1215,
	cin => ff_cnt_a11_a_a1186,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a12_a_areg0,
	cout => ff_cnt_a12_a_a1189);

add_a1227_I : apex20ke_lcell
-- Equation(s):
-- add_a1227 = PCI_PROC_a56 $ HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 $ add_a1217
-- add_a1229 = CARRY(PCI_PROC_a56 & !HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16 & !add_a1217 # !PCI_PROC_a56 & (!add_a1217 # !HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	cin => add_a1217,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1227,
	cout => add_a1229);

add_a1219_I : apex20ke_lcell
-- Equation(s):
-- add_a1219 = PCI_PROC_a56 $ ff_cnt_a14_a_areg0 $ !add_a1229
-- add_a1221 = CARRY(PCI_PROC_a56 & (ff_cnt_a14_a_areg0 # !add_a1229) # !PCI_PROC_a56 & ff_cnt_a14_a_areg0 & !add_a1229)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datab => ff_cnt_a14_a_areg0,
	cin => add_a1229,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1219,
	cout => add_a1221);

ff_cnt_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a14_a_areg0 = DFFE((PCI_PROC_a52 & add_a1219) # (!PCI_PROC_a52 & ff_cnt_a14_a_areg0 $ !ff_cnt_a13_a_a1198) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )
-- ff_cnt_a14_a_a1192 = CARRY(ff_cnt_a14_a_areg0 & !ff_cnt_a13_a_a1198)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ff_cnt_a14_a_areg0,
	datac => add_a1219,
	cin => ff_cnt_a13_a_a1198,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a14_a_areg0,
	cout => ff_cnt_a14_a_a1192);

add_a1223_I : apex20ke_lcell
-- Equation(s):
-- add_a1223 = PCI_PROC_a56 $ (add_a1221 $ ff_cnt_a15_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_PROC_a56,
	datad => ff_cnt_a15_a_areg0,
	cin => add_a1221,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1223);

ff_cnt_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- ff_cnt_a15_a_areg0 = DFFE((PCI_PROC_a52 & add_a1223) # (!PCI_PROC_a52 & ff_cnt_a14_a_a1192 $ ff_cnt_a15_a_areg0) & !GLOBAL(FF_RS_acombout), GLOBAL(CLK40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => add_a1223,
	datad => ff_cnt_a15_a_areg0,
	cin => ff_cnt_a14_a_a1192,
	clk => CLK40_acombout,
	aclr => IMR_acombout,
	sclr => FF_RS_acombout,
	sload => PCI_PROC_a52,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ff_cnt_a15_a_areg0);

HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15_I : apex20ke_lcell
-- Equation(s):
-- HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a = CARRY(ff_cnt_a14_a_areg0 # !HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ff_cnt_a14_a_areg0,
	cin => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a_a15,
	cout => HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a);

HALF_COMPARE_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- HALF_COMPARE_acomparator_acmp_end_aagb_out = !HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a & !ff_cnt_a15_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ff_cnt_a15_a_areg0,
	cin => HALF_COMPARE_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => HALF_COMPARE_acomparator_acmp_end_aagb_out);

FF_HF_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_HALF_COMPARE_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_HF);

ff_cnt_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(0));

ff_cnt_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(1));

ff_cnt_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(2));

ff_cnt_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(3));

ff_cnt_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(4));

ff_cnt_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(5));

ff_cnt_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(6));

ff_cnt_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(7));

ff_cnt_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(8));

ff_cnt_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(9));

ff_cnt_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(10));

ff_cnt_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(11));

ff_cnt_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(12));

ff_cnt_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => HALF_COMPARE_acomparator_acmp_end_alcarry_a13_a_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(13));

ff_cnt_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(14));

ff_cnt_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ff_cnt_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ff_cnt(15));
END structure;


