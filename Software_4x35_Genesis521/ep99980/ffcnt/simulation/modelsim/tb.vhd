-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FFCNT.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		November 2001 - MCS
--			Module has been debuggged and implemented in the DPP-II Board
--
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------


library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr 	: std_logic := '1';
	signal clk40	: std_logic := '0';
	signal FF_REN	: std_logic;
	signal FF_WEN	: std_logic;
	signal FF_RS	: std_logic;
	signal FF_HF	: std_logic;
	signal FF_CNT	: std_logic_vector( 15 downto 0 );
	
	-----------------------------------------------------------------------------------------
	component FFCNT is 
		port( 
	     	IMR            : in      std_logic;                  		-- Local Master Reset
			CLK40		: in		std_logic;
			FF_REN		: in		std_logic;
			FF_WEN		: in		std_logic;
			FF_RS		: in		std_Logic;
			FF_HF		: out	std_logic;
			ff_cnt		: out	std_logic_Vector( 15 downto 0 ) );
	end component FFCNT;
	-----------------------------------------------------------------------------------------
begin
	IMR	<= '0' after 555 ns;
	CLK40 <= not( clk40 ) after 12.5 ns;
	
	U : FFCNT
		port map(
			imr	=> imr,
			clk40	=> clk40,
			FF_REN	=> FF_REN,
			FF_WEN	=> FF_WEN,
			FF_RS	=> FF_RS,
			FF_HF	=> FF_HF,
			FF_CNT	=> FF_CNT );
	FFCNT_PROC : process
	begin
		FF_RS	<= '0';
		FF_REN	<= '0';
		FF_WEN	<= '0';
		wait for 2 us;
		
		---------------------------------------------------------------------------------
		assert( false )
			report "Reset FIFO"
			severity note;
		wait until (( CLK40'event ) and ( clk40 = '1'));
		wait for 7 ns;
		FF_RS	<= '1';
		wait until (( CLK40'event ) and ( clk40 = '1'));
		wait for 7 ns;
		FF_RS	<= '0';
		---------------------------------------------------------------------------------

		---------------------------------------------------------------------------------
		for i in 0 to 15 loop
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_WEN	<= '1';
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_WEN 	<= '0';
			wait for 50 ns;
		end loop;
		---------------------------------------------------------------------------------
		wait for 1 us;
		---------------------------------------------------------------------------------
		for i in 0 to 20 loop
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_REN	<= '1';
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_REN 	<= '0';
			wait for 50 ns;
		end loop;
		---------------------------------------------------------------------------------
			
		wait for 1 us;
		---------------------------------------------------------------------------------
		for i in 0 to 7 loop
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_WEN	<= '1';
			wait until (( CLK40'event ) and ( clk40 = '1'));
			wait for 7 ns;
			FF_WEN 	<= '0';
			wait for 50 ns;
		end loop;
		---------------------------------------------------------------------------------
	
		wait until (( CLK40'event ) and ( clk40 = '1'));
		wait for 7 ns;
		FF_WEN 	<= '1';
		FF_REN	<= '1';
		wait until (( CLK40'event ) and ( clk40 = '1'));
		wait for 7 ns;
		FF_WEN 	<= '0';
		FF_REN	<= '0';
		---------------------------------------------------------------------------------

		wait for 10 us;
		assert( false )
			report "End of Simulation"
			severity failure;
		wait ;
	end process;

-----------------------------------------------------------------------------------------
end test_bench;			-- FFCNT.VHD
-----------------------------------------------------------------------------------------

