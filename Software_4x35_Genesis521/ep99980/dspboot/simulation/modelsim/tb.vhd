-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DSPBOOT.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		June 20, 2001 - MCS
--			Module has been debuggged and implemented in the EDI-III Board
--
--   Description:	
--                  PCI - to AMCC S5920 Interface Logic
--				The timing to boot the DSP (TMS320C32-60) is based on the DSP_CLK being 10MHZ
--				
--				Anything Faster or Slower will not allow the DSP to boot.
--
--
--        ---+     +-----------------------------------------------------------------------
-- DSP_MR    |     |
--           +-----+
--
--        -------------+     +-----------------------------------------------------------------------
-- INT3                |     |
--                     +-----+
--              +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +
-- DSP_CLK      |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
--         -----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+
--
--                                       +------------------------------------------------+
-- DSP_SHIFT                             |                                                |
--         ------------------------------+                                                +-----------------------
--
--                                                    /---------\ /---------\ /---------\ /---------\ 
-- DSP_D0  ------------------------------------------X           X           X           X           X-------------
--                                                    \_________/ \_________/ \_________/ \_________/
--
--
-----------------------------------------------------------------------------------------



library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
entity tb is
end tb;

architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk20		: std_logic := '0';
	signal DSP_CONFIG	: std_Logic;
	signal DSP_BYTE_LO	: std_logic;					-- Load Low Byte to DSP COnfig Reg
	signal DSP_MR_ED 	: std_logic;					-- DSP Master Reset Edge Detect
	signal DSP_BOOT_IN	: std_logic;
	signal DSP_REG		: std_logic_Vector( 31 downto 0 );
	signal DSP_BOOT_OUT	: std_Logic;
	signal DSP_SHIFT	: std_logic;
	signal DSP_CLK		: std_Logic;
	signal DSP_D0		: std_logic;
	signal DSP_MR		: std_logic;
	
	-----------------------------------------------------------------------------------------
	component DSPBOOT is 
		port( 
	     	IMR       	: in      std_logic;                         -- Local Master Reset
		     CLK20		: in      std_logic;                         -- Local System Clock
			DSP_CONFIG	: in		std_Logic;
			DSP_BYTE_LO	: in 	std_logic;					-- Load Low Byte to DSP COnfig Reg
			DSP_MR_ED 	: in		std_logic;					-- DSP Master Reset Edge Detect
			DSP_BOOT_IN	: in		std_logic;
			DSP_REG		: in		std_logic_Vector( 31 downto 0 );
			DSP_BOOT_OUT	: out	std_Logic;
			DSP_SHIFT		: out 	std_logic;
			DSP_CLK		: out	std_Logic;
			DSP_D0		: out	std_logic;
			DSP_MR		: out	std_logic );
	end component DSPBOOT;
	-----------------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk20	<= not( clk20 ) after 25 ns;
	
	U : DSPBOOT
		port map(
			imr			=> imr,
			clk20		=> clk20,
			dsp_Config	=> dsp_config,
			dsp_byte_lo	=> dsp_byte_lo,
			dsp_mr_ed		=> dsp_mr_ed,
			dsp_boot_in	=> dsp_boot_in,
			dsp_Reg		=> dsp_reg,
			DSP_BOOT_OUT	=> dsp_boot_out,
			DSP_SHIFT		=> dsp_shift,
			DSP_CLK		=> dsp_clk,
			DSP_D0		=> dsp_d0,
			DSP_MR		=> dsp_mr );
	
     ------------------------------------------------------------------------------------
	dsp_proc : process
	begin
		DSP_CONFIG	<= '0';
		DSP_BYTE_LO	<= '0';
		DSP_MR_ED 	<= '0';
		DSP_BOOT_IN	<= '0';
		DSP_REG		<= x"00000000";
		wait for 2 us;
		
          -------------------------------------------------------------------------------
		assert( false )
			report "Reset DSP"
			severity note;
		wait until(( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;
		DSP_MR_ED	<= '1';
		wait until(( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;
		DSP_MR_ED <= '0';
		
		wait for 10 us;
          -------------------------------------------------------------------------------

		assert( false )
			report "Inerrupt the DSP w/ DSP_BOOT_IN"
			severity note;
		wait until(( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;
		DSP_BOOT_IN <= '1';
		wait until(( clk20'event ) and ( clk20 = '1' ));
		wait for 7 ns;
		DSP_BOOT_IN <= '0';
		
		wait until (( DSP_BOOT_OUT'Event ) and ( DSP_BOOT_OUT = '0' ));

          -------------------------------------------------------------------------------
		assert( false )
			report "Write data to the DSP"
			severity note;
		DSP_CONFIG 	<= '1';
		
		for i in 0 to 31 loop
			wait until(( clk20'event ) and ( clk20 = '1' ));
			wait for 7 ns;
			DSP_BYTE_LO	<= '1';
			DSP_REG(i) 	<= '1';
			wait until(( clk20'event ) and ( clk20 = '1' ));
			wait for 7 ns;
			DSP_BYTE_LO 	<= '0';
			wait until (( DSP_SHIFT'Event ) and ( DSP_SHIFT = '0' ));
			DSP_REG(i) 	<= '0';
			wait for 200 ns;
		end loop;
		wait for 10 us;
			
			
		

          -------------------------------------------------------------------------------
		
		wait until (( DSP_MR'Event ) and ( DSP_MR = '0' ));
		assert( false )
			report "End of Simulation"
			severity failure;
		wait;
	end process;
     ------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
end test_bench;			-- DSPBOOT.VHD
-----------------------------------------------------------------------------------------

