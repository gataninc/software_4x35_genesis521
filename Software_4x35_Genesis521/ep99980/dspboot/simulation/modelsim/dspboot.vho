-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 11:04:35"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	DSPBOOT IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	DSP_CONFIG : IN std_logic;
	DSP_BYTE_LO : IN std_logic;
	DSP_MR_ED : IN std_logic;
	DSP_BOOT_IN : IN std_logic;
	DSP_REG : IN std_logic_vector(31 DOWNTO 0);
	DSP_BOOT_OUT : OUT std_logic;
	DSP_SHIFT : OUT std_logic;
	DSP_CLK : OUT std_logic;
	DSP_D0 : OUT std_logic;
	DSP_MR : OUT std_logic
	);
END DSPBOOT;

ARCHITECTURE structure OF DSPBOOT IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_DSP_CONFIG : std_logic;
SIGNAL ww_DSP_BYTE_LO : std_logic;
SIGNAL ww_DSP_MR_ED : std_logic;
SIGNAL ww_DSP_BOOT_IN : std_logic;
SIGNAL ww_DSP_REG : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_DSP_BOOT_OUT : std_logic;
SIGNAL ww_DSP_SHIFT : std_logic;
SIGNAL ww_DSP_CLK : std_logic;
SIGNAL ww_DSP_D0 : std_logic;
SIGNAL ww_DSP_MR : std_logic;
SIGNAL SHIFT_CNT_a2_a : std_logic;
SIGNAL DSP_BOOT_CNT_a119 : std_logic;
SIGNAL iDSP_REG_a30_a : std_logic;
SIGNAL iDSP_REG_a29_a : std_logic;
SIGNAL DSP_MR_CNT_a0_a : std_logic;
SIGNAL rtl_a244 : std_logic;
SIGNAL rtl_a232 : std_logic;
SIGNAL rtl_a238 : std_logic;
SIGNAL DSP_MR_CNT_a9_a : std_logic;
SIGNAL rtl_a234 : std_logic;
SIGNAL DSP_MR_CNT_a19_a : std_logic;
SIGNAL rtl_a236 : std_logic;
SIGNAL iDSP_REG_a28_a : std_logic;
SIGNAL iDSP_REG_a27_a : std_logic;
SIGNAL iDSP_REG_a26_a : std_logic;
SIGNAL iDSP_REG_a25_a : std_logic;
SIGNAL iDSP_REG_a24_a : std_logic;
SIGNAL iDSP_REG_a23_a : std_logic;
SIGNAL iDSP_REG_a22_a : std_logic;
SIGNAL iDSP_REG_a21_a : std_logic;
SIGNAL iDSP_REG_a20_a : std_logic;
SIGNAL iDSP_REG_a19_a : std_logic;
SIGNAL iDSP_REG_a18_a : std_logic;
SIGNAL iDSP_REG_a17_a : std_logic;
SIGNAL iDSP_REG_a16_a : std_logic;
SIGNAL iDSP_REG_a15_a : std_logic;
SIGNAL iDSP_REG_a14_a : std_logic;
SIGNAL iDSP_REG_a13_a : std_logic;
SIGNAL iDSP_REG_a12_a : std_logic;
SIGNAL iDSP_REG_a11_a : std_logic;
SIGNAL iDSP_REG_a10_a : std_logic;
SIGNAL iDSP_REG_a9_a : std_logic;
SIGNAL iDSP_REG_a8_a : std_logic;
SIGNAL iDSP_REG_a7_a : std_logic;
SIGNAL iDSP_REG_a6_a : std_logic;
SIGNAL iDSP_REG_a5_a : std_logic;
SIGNAL iDSP_REG_a4_a : std_logic;
SIGNAL iDSP_REG_a3_a : std_logic;
SIGNAL iDSP_REG_a2_a : std_logic;
SIGNAL iDSP_REG_a1_a : std_logic;
SIGNAL iDSP_REG_a0_a : std_logic;
SIGNAL DSP_REG_a30_a_acombout : std_logic;
SIGNAL DSP_REG_a29_a_acombout : std_logic;
SIGNAL DSP_REG_a28_a_acombout : std_logic;
SIGNAL DSP_REG_a27_a_acombout : std_logic;
SIGNAL DSP_REG_a26_a_acombout : std_logic;
SIGNAL DSP_REG_a25_a_acombout : std_logic;
SIGNAL DSP_REG_a24_a_acombout : std_logic;
SIGNAL DSP_REG_a23_a_acombout : std_logic;
SIGNAL DSP_REG_a22_a_acombout : std_logic;
SIGNAL DSP_REG_a21_a_acombout : std_logic;
SIGNAL DSP_REG_a20_a_acombout : std_logic;
SIGNAL DSP_REG_a19_a_acombout : std_logic;
SIGNAL DSP_REG_a18_a_acombout : std_logic;
SIGNAL DSP_REG_a17_a_acombout : std_logic;
SIGNAL DSP_REG_a16_a_acombout : std_logic;
SIGNAL DSP_REG_a15_a_acombout : std_logic;
SIGNAL DSP_REG_a14_a_acombout : std_logic;
SIGNAL DSP_REG_a13_a_acombout : std_logic;
SIGNAL DSP_REG_a12_a_acombout : std_logic;
SIGNAL DSP_REG_a11_a_acombout : std_logic;
SIGNAL DSP_REG_a10_a_acombout : std_logic;
SIGNAL DSP_REG_a9_a_acombout : std_logic;
SIGNAL DSP_REG_a8_a_acombout : std_logic;
SIGNAL DSP_REG_a7_a_acombout : std_logic;
SIGNAL DSP_REG_a6_a_acombout : std_logic;
SIGNAL DSP_REG_a5_a_acombout : std_logic;
SIGNAL DSP_REG_a4_a_acombout : std_logic;
SIGNAL DSP_REG_a3_a_acombout : std_logic;
SIGNAL DSP_REG_a2_a_acombout : std_logic;
SIGNAL DSP_REG_a1_a_acombout : std_logic;
SIGNAL DSP_REG_a0_a_acombout : std_logic;
SIGNAL DSP_BOOT_IN_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL DSP_BOOT_VEC_a0_a : std_logic;
SIGNAL DSP_BOOT_VEC_a1_a : std_logic;
SIGNAL DSP_BOOT_CNT_a0_a : std_logic;
SIGNAL DSP_BOOT_CNT_a122 : std_logic;
SIGNAL DSP_BOOT_CNT_a1_a : std_logic;
SIGNAL add_a665 : std_logic;
SIGNAL DSP_BOOT_CNT_a2_a : std_logic;
SIGNAL DSP_BOOT_OUT_a44 : std_logic;
SIGNAL DSP_BOOT_OUT_areg0 : std_logic;
SIGNAL DSP_CONFIG_acombout : std_logic;
SIGNAL DSP_CLK_areg0 : std_logic;
SIGNAL SHIFT_CNT_a0_a : std_logic;
SIGNAL SHIFT_CNT_a0_a_a51 : std_logic;
SIGNAL SHIFT_CNT_a1_a : std_logic;
SIGNAL SHIFT_CNT_a1_a_a54 : std_logic;
SIGNAL SHIFT_CNT_a2_a_a60 : std_logic;
SIGNAL SHIFT_CNT_a3_a_a63 : std_logic;
SIGNAL SHIFT_CNT_a4_a : std_logic;
SIGNAL SHIFT_CNT_a3_a : std_logic;
SIGNAL DSP_SHIFT_a35 : std_logic;
SIGNAL DSP_BYTE_LO_acombout : std_logic;
SIGNAL DSP_BYTE_LATCH : std_logic;
SIGNAL DSP_SHIFT_areg0 : std_logic;
SIGNAL DSP_REG_a31_a_acombout : std_logic;
SIGNAL iDSP_REG_a31_a : std_logic;
SIGNAL DSP_D0_areg0 : std_logic;
SIGNAL DSP_MR_CNT_a0_a_a186 : std_logic;
SIGNAL DSP_MR_CNT_a1_a : std_logic;
SIGNAL DSP_MR_CNT_a1_a_a183 : std_logic;
SIGNAL DSP_MR_CNT_a2_a : std_logic;
SIGNAL DSP_MR_CNT_a2_a_a180 : std_logic;
SIGNAL DSP_MR_CNT_a3_a : std_logic;
SIGNAL DSP_MR_CNT_a3_a_a177 : std_logic;
SIGNAL DSP_MR_CNT_a4_a : std_logic;
SIGNAL DSP_MR_CNT_a4_a_a198 : std_logic;
SIGNAL DSP_MR_CNT_a5_a : std_logic;
SIGNAL DSP_MR_CNT_a5_a_a195 : std_logic;
SIGNAL DSP_MR_CNT_a6_a : std_logic;
SIGNAL DSP_MR_CNT_a6_a_a192 : std_logic;
SIGNAL DSP_MR_CNT_a7_a : std_logic;
SIGNAL DSP_MR_CNT_a7_a_a189 : std_logic;
SIGNAL DSP_MR_CNT_a8_a : std_logic;
SIGNAL DSP_MR_CNT_a8_a_a210 : std_logic;
SIGNAL DSP_MR_CNT_a9_a_a207 : std_logic;
SIGNAL DSP_MR_CNT_a10_a : std_logic;
SIGNAL DSP_MR_CNT_a10_a_a204 : std_logic;
SIGNAL DSP_MR_CNT_a11_a : std_logic;
SIGNAL DSP_MR_CNT_a11_a_a201 : std_logic;
SIGNAL DSP_MR_CNT_a12_a : std_logic;
SIGNAL DSP_MR_CNT_a12_a_a222 : std_logic;
SIGNAL DSP_MR_CNT_a13_a_a219 : std_logic;
SIGNAL DSP_MR_CNT_a14_a : std_logic;
SIGNAL DSP_MR_CNT_a13_a : std_logic;
SIGNAL rtl_a245 : std_logic;
SIGNAL rtl_a239 : std_logic;
SIGNAL DSP_MR_CNT_a14_a_a216 : std_logic;
SIGNAL DSP_MR_CNT_a15_a : std_logic;
SIGNAL DSP_MR_CNT_a15_a_a213 : std_logic;
SIGNAL DSP_MR_CNT_a16_a : std_logic;
SIGNAL DSP_MR_CNT_a16_a_a231 : std_logic;
SIGNAL DSP_MR_CNT_a17_a : std_logic;
SIGNAL DSP_MR_CNT_a17_a_a234 : std_logic;
SIGNAL DSP_MR_CNT_a18_a_a228 : std_logic;
SIGNAL DSP_MR_CNT_a19_a_a225 : std_logic;
SIGNAL DSP_MR_CNT_a20_a : std_logic;
SIGNAL DSP_MR_CNT_a20_a_a240 : std_logic;
SIGNAL DSP_MR_CNT_a21_a : std_logic;
SIGNAL DSP_MR_CNT_a18_a : std_logic;
SIGNAL rtl_a246 : std_logic;
SIGNAL rtl_a240 : std_logic;
SIGNAL rtl_a3 : std_logic;
SIGNAL DSP_MR_ED_acombout : std_logic;
SIGNAL DSP_MR_areg0 : std_logic;
SIGNAL ALT_INV_DSP_SHIFT_areg0 : std_logic;
SIGNAL ALT_INV_DSP_CLK_areg0 : std_logic;
SIGNAL ALT_INV_DSP_MR_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_DSP_CONFIG <= DSP_CONFIG;
ww_DSP_BYTE_LO <= DSP_BYTE_LO;
ww_DSP_MR_ED <= DSP_MR_ED;
ww_DSP_BOOT_IN <= DSP_BOOT_IN;
ww_DSP_REG <= DSP_REG;
DSP_BOOT_OUT <= ww_DSP_BOOT_OUT;
DSP_SHIFT <= ww_DSP_SHIFT;
DSP_CLK <= ww_DSP_CLK;
DSP_D0 <= ww_DSP_D0;
DSP_MR <= ww_DSP_MR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_DSP_SHIFT_areg0 <= NOT DSP_SHIFT_areg0;
ALT_INV_DSP_CLK_areg0 <= NOT DSP_CLK_areg0;
ALT_INV_DSP_MR_areg0 <= NOT DSP_MR_areg0;

SHIFT_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a2_a = DFFE(GLOBAL(DSP_SHIFT_areg0) & SHIFT_CNT_a2_a $ (!SHIFT_CNT_a1_a_a54), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)
-- SHIFT_CNT_a2_a_a60 = CARRY(SHIFT_CNT_a2_a & (!SHIFT_CNT_a1_a_a54))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a2_a,
	cin => SHIFT_CNT_a1_a_a54,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	sclr => ALT_INV_DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a2_a,
	cout => SHIFT_CNT_a2_a_a60);

DSP_BOOT_CNT_a119_I : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_CNT_a119 = DSP_BOOT_OUT_areg0 & (DSP_BOOT_VEC_a1_a # !DSP_BOOT_VEC_a0_a)
-- DSP_BOOT_CNT_a122 = DSP_BOOT_OUT_areg0 & (DSP_BOOT_VEC_a1_a # !DSP_BOOT_VEC_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_BOOT_VEC_a1_a,
	datac => DSP_BOOT_OUT_areg0,
	datad => DSP_BOOT_VEC_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_BOOT_CNT_a119,
	cascout => DSP_BOOT_CNT_a122);

iDSP_REG_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a30_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a29_a # !DSP_SHIFT_areg0 & (DSP_REG_a30_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a29_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a30_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a30_a);

iDSP_REG_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a29_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a28_a # !DSP_SHIFT_areg0 & (DSP_REG_a29_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a28_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a29_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a29_a);

DSP_REG_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(30),
	combout => DSP_REG_a30_a_acombout);

DSP_MR_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a0_a = DFFE(GLOBAL(DSP_MR_areg0) & !DSP_MR_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a0_a_a186 = CARRY(DSP_MR_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "55AA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a0_a,
	cout => DSP_MR_CNT_a0_a_a186);

rtl_a232_I : apex20ke_lcell
-- Equation(s):
-- rtl_a244 = DSP_MR_CNT_a3_a & DSP_MR_CNT_a1_a & DSP_MR_CNT_a0_a & DSP_MR_CNT_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a3_a,
	datab => DSP_MR_CNT_a1_a,
	datac => DSP_MR_CNT_a0_a,
	datad => DSP_MR_CNT_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a232,
	cascout => rtl_a244);

rtl_a238_I : apex20ke_lcell
-- Equation(s):
-- rtl_a238 = (DSP_MR_CNT_a6_a & DSP_MR_CNT_a5_a & DSP_MR_CNT_a7_a & DSP_MR_CNT_a4_a) & CASCADE(rtl_a244)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a6_a,
	datab => DSP_MR_CNT_a5_a,
	datac => DSP_MR_CNT_a7_a,
	datad => DSP_MR_CNT_a4_a,
	cascin => rtl_a244,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a238);

DSP_MR_CNT_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a9_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a9_a $ (DSP_MR_CNT_a8_a_a210), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a9_a_a207 = CARRY(!DSP_MR_CNT_a8_a_a210 # !DSP_MR_CNT_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a9_a,
	cin => DSP_MR_CNT_a8_a_a210,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a9_a,
	cout => DSP_MR_CNT_a9_a_a207);

DSP_MR_CNT_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a19_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a19_a $ (DSP_MR_CNT_a18_a_a228), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a19_a_a225 = CARRY(!DSP_MR_CNT_a18_a_a228 # !DSP_MR_CNT_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a19_a,
	cin => DSP_MR_CNT_a18_a_a228,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a19_a,
	cout => DSP_MR_CNT_a19_a_a225);

iDSP_REG_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a28_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a27_a # !DSP_SHIFT_areg0 & (DSP_REG_a28_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a27_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a28_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a28_a);

DSP_REG_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(29),
	combout => DSP_REG_a29_a_acombout);

iDSP_REG_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a27_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a26_a # !DSP_SHIFT_areg0 & (DSP_REG_a27_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a26_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a27_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a27_a);

DSP_REG_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(28),
	combout => DSP_REG_a28_a_acombout);

iDSP_REG_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a26_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a25_a # !DSP_SHIFT_areg0 & (DSP_REG_a26_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a25_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a26_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a26_a);

DSP_REG_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(27),
	combout => DSP_REG_a27_a_acombout);

iDSP_REG_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a25_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a24_a # !DSP_SHIFT_areg0 & (DSP_REG_a25_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a24_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a25_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a25_a);

DSP_REG_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(26),
	combout => DSP_REG_a26_a_acombout);

iDSP_REG_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a24_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a23_a # !DSP_SHIFT_areg0 & (DSP_REG_a24_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a23_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a24_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a24_a);

DSP_REG_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(25),
	combout => DSP_REG_a25_a_acombout);

iDSP_REG_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a23_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a22_a # !DSP_SHIFT_areg0 & (DSP_REG_a23_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a22_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a23_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a23_a);

DSP_REG_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(24),
	combout => DSP_REG_a24_a_acombout);

iDSP_REG_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a22_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a21_a # !DSP_SHIFT_areg0 & (DSP_REG_a22_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a21_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a22_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a22_a);

DSP_REG_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(23),
	combout => DSP_REG_a23_a_acombout);

iDSP_REG_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a21_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a20_a # !DSP_SHIFT_areg0 & (DSP_REG_a21_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a20_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a21_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a21_a);

DSP_REG_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(22),
	combout => DSP_REG_a22_a_acombout);

iDSP_REG_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a20_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a19_a # !DSP_SHIFT_areg0 & (DSP_REG_a20_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a19_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a20_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a20_a);

DSP_REG_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(21),
	combout => DSP_REG_a21_a_acombout);

iDSP_REG_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a19_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a18_a # !DSP_SHIFT_areg0 & (DSP_REG_a19_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a18_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a19_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a19_a);

DSP_REG_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(20),
	combout => DSP_REG_a20_a_acombout);

iDSP_REG_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a18_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a17_a # !DSP_SHIFT_areg0 & (DSP_REG_a18_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a17_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a18_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a18_a);

DSP_REG_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(19),
	combout => DSP_REG_a19_a_acombout);

iDSP_REG_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a17_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a16_a # !DSP_SHIFT_areg0 & (DSP_REG_a17_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a16_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a17_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a17_a);

DSP_REG_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(18),
	combout => DSP_REG_a18_a_acombout);

iDSP_REG_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a16_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a15_a # !DSP_SHIFT_areg0 & (DSP_REG_a16_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a15_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a16_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a16_a);

DSP_REG_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(17),
	combout => DSP_REG_a17_a_acombout);

iDSP_REG_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a15_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a14_a # !DSP_SHIFT_areg0 & (DSP_REG_a15_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a14_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a15_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a15_a);

DSP_REG_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(16),
	combout => DSP_REG_a16_a_acombout);

iDSP_REG_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a14_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a13_a # !DSP_SHIFT_areg0 & (DSP_REG_a14_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a13_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a14_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a14_a);

DSP_REG_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(15),
	combout => DSP_REG_a15_a_acombout);

iDSP_REG_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a13_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a12_a # !DSP_SHIFT_areg0 & (DSP_REG_a13_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a12_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a13_a);

DSP_REG_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(14),
	combout => DSP_REG_a14_a_acombout);

iDSP_REG_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a12_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a11_a # !DSP_SHIFT_areg0 & (DSP_REG_a12_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a11_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a12_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a12_a);

DSP_REG_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(13),
	combout => DSP_REG_a13_a_acombout);

iDSP_REG_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a11_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a10_a # !DSP_SHIFT_areg0 & (DSP_REG_a11_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a10_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a11_a);

DSP_REG_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(12),
	combout => DSP_REG_a12_a_acombout);

iDSP_REG_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a10_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a9_a # !DSP_SHIFT_areg0 & (DSP_REG_a10_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a9_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a10_a);

DSP_REG_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(11),
	combout => DSP_REG_a11_a_acombout);

iDSP_REG_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a9_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a8_a # !DSP_SHIFT_areg0 & (DSP_REG_a9_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a8_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a9_a);

DSP_REG_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(10),
	combout => DSP_REG_a10_a_acombout);

iDSP_REG_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a8_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a7_a # !DSP_SHIFT_areg0 & (DSP_REG_a8_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a7_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a8_a);

DSP_REG_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(9),
	combout => DSP_REG_a9_a_acombout);

iDSP_REG_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a7_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a6_a # !DSP_SHIFT_areg0 & (DSP_REG_a7_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a6_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a7_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a7_a);

DSP_REG_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(8),
	combout => DSP_REG_a8_a_acombout);

iDSP_REG_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a6_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a5_a # !DSP_SHIFT_areg0 & (DSP_REG_a6_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a5_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a6_a);

DSP_REG_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(7),
	combout => DSP_REG_a7_a_acombout);

iDSP_REG_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a5_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a4_a # !DSP_SHIFT_areg0 & (DSP_REG_a5_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => iDSP_REG_a4_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a5_a);

DSP_REG_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(6),
	combout => DSP_REG_a6_a_acombout);

iDSP_REG_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a4_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a3_a # !DSP_SHIFT_areg0 & (DSP_REG_a4_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SHIFT_areg0,
	datac => iDSP_REG_a3_a,
	datad => DSP_REG_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a4_a);

DSP_REG_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(5),
	combout => DSP_REG_a5_a_acombout);

iDSP_REG_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a3_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a2_a # !DSP_SHIFT_areg0 & (DSP_REG_a3_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SHIFT_areg0,
	datac => iDSP_REG_a2_a,
	datad => DSP_REG_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a3_a);

DSP_REG_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(4),
	combout => DSP_REG_a4_a_acombout);

iDSP_REG_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a2_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a1_a # !DSP_SHIFT_areg0 & (DSP_REG_a2_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SHIFT_areg0,
	datac => iDSP_REG_a1_a,
	datad => DSP_REG_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a2_a);

DSP_REG_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(3),
	combout => DSP_REG_a3_a_acombout);

iDSP_REG_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a1_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a0_a # !DSP_SHIFT_areg0 & (DSP_REG_a1_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F3C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SHIFT_areg0,
	datac => iDSP_REG_a0_a,
	datad => DSP_REG_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a1_a);

DSP_REG_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(2),
	combout => DSP_REG_a2_a_acombout);

iDSP_REG_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a0_a = DFFE(!DSP_SHIFT_areg0 & (DSP_REG_a0_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SHIFT_areg0,
	datad => DSP_REG_a0_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a0_a);

DSP_REG_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(1),
	combout => DSP_REG_a1_a_acombout);

DSP_REG_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(0),
	combout => DSP_REG_a0_a_acombout);

DSP_BOOT_IN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_BOOT_IN,
	combout => DSP_BOOT_IN_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

DSP_BOOT_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_VEC_a0_a = DFFE(DSP_BOOT_IN_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_BOOT_IN_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_VEC_a0_a);

DSP_BOOT_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_VEC_a1_a = DFFE(DSP_BOOT_VEC_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_BOOT_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_VEC_a1_a);

DSP_BOOT_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_CNT_a0_a = DFFE(DSP_BOOT_OUT_areg0 & !DSP_BOOT_CNT_a0_a & (DSP_BOOT_VEC_a1_a # !DSP_BOOT_VEC_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "080A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_BOOT_OUT_areg0,
	datab => DSP_BOOT_VEC_a1_a,
	datac => DSP_BOOT_CNT_a0_a,
	datad => DSP_BOOT_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_CNT_a0_a);

DSP_BOOT_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_CNT_a1_a = DFFE((DSP_BOOT_CNT_a1_a $ (DSP_BOOT_OUT_areg0 & DSP_BOOT_CNT_a0_a)) & CASCADE(DSP_BOOT_CNT_a122), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6C6C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_BOOT_OUT_areg0,
	datab => DSP_BOOT_CNT_a1_a,
	datac => DSP_BOOT_CNT_a0_a,
	cascin => DSP_BOOT_CNT_a122,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_CNT_a1_a);

add_a665_I : apex20ke_lcell
-- Equation(s):
-- add_a665 = DSP_BOOT_OUT_areg0 & DSP_BOOT_CNT_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_BOOT_OUT_areg0,
	datad => DSP_BOOT_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a665);

DSP_BOOT_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_CNT_a2_a = DFFE(DSP_BOOT_CNT_a119 & (DSP_BOOT_CNT_a2_a $ (DSP_BOOT_CNT_a1_a & add_a665)), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2A80",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_BOOT_CNT_a119,
	datab => DSP_BOOT_CNT_a1_a,
	datac => add_a665,
	datad => DSP_BOOT_CNT_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_CNT_a2_a);

DSP_BOOT_OUT_a44_I : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_OUT_a44 = !DSP_BOOT_CNT_a0_a # !DSP_BOOT_CNT_a2_a # !DSP_BOOT_CNT_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_BOOT_CNT_a1_a,
	datac => DSP_BOOT_CNT_a2_a,
	datad => DSP_BOOT_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_BOOT_OUT_a44);

DSP_BOOT_OUT_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_BOOT_OUT_areg0 = DFFE(DSP_BOOT_OUT_a44 & (DSP_BOOT_OUT_areg0 # !DSP_BOOT_VEC_a1_a & DSP_BOOT_VEC_a0_a) # !DSP_BOOT_OUT_a44 & !DSP_BOOT_VEC_a1_a & (DSP_BOOT_VEC_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B3A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_BOOT_OUT_a44,
	datab => DSP_BOOT_VEC_a1_a,
	datac => DSP_BOOT_OUT_areg0,
	datad => DSP_BOOT_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BOOT_OUT_areg0);

DSP_CONFIG_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_CONFIG,
	combout => DSP_CONFIG_acombout);

DSP_CLK_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_CLK_areg0 = DFFE(!DSP_CLK_areg0 & DSP_CONFIG_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_CLK_areg0,
	datad => DSP_CONFIG_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_CLK_areg0);

SHIFT_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a0_a = DFFE(GLOBAL(DSP_SHIFT_areg0) & !SHIFT_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)
-- SHIFT_CNT_a0_a_a51 = CARRY(SHIFT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => SHIFT_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	sclr => ALT_INV_DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a0_a,
	cout => SHIFT_CNT_a0_a_a51);

SHIFT_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a1_a = DFFE(GLOBAL(DSP_SHIFT_areg0) & SHIFT_CNT_a1_a $ SHIFT_CNT_a0_a_a51, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)
-- SHIFT_CNT_a1_a_a54 = CARRY(!SHIFT_CNT_a0_a_a51 # !SHIFT_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => SHIFT_CNT_a1_a,
	cin => SHIFT_CNT_a0_a_a51,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	sclr => ALT_INV_DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a1_a,
	cout => SHIFT_CNT_a1_a_a54);

SHIFT_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a3_a = DFFE(GLOBAL(DSP_SHIFT_areg0) & SHIFT_CNT_a3_a $ (SHIFT_CNT_a2_a_a60), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)
-- SHIFT_CNT_a3_a_a63 = CARRY(!SHIFT_CNT_a2_a_a60 # !SHIFT_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a3_a,
	cin => SHIFT_CNT_a2_a_a60,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	sclr => ALT_INV_DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a3_a,
	cout => SHIFT_CNT_a3_a_a63);

SHIFT_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- SHIFT_CNT_a4_a = DFFE(GLOBAL(DSP_SHIFT_areg0) & SHIFT_CNT_a3_a_a63 $ !SHIFT_CNT_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => SHIFT_CNT_a4_a,
	cin => SHIFT_CNT_a3_a_a63,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	sclr => ALT_INV_DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHIFT_CNT_a4_a);

DSP_SHIFT_a35_I : apex20ke_lcell
-- Equation(s):
-- DSP_SHIFT_a35 = !SHIFT_CNT_a1_a # !SHIFT_CNT_a3_a # !SHIFT_CNT_a4_a # !SHIFT_CNT_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SHIFT_CNT_a2_a,
	datab => SHIFT_CNT_a4_a,
	datac => SHIFT_CNT_a3_a,
	datad => SHIFT_CNT_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_SHIFT_a35);

DSP_BYTE_LO_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_BYTE_LO,
	combout => DSP_BYTE_LO_acombout);

DSP_BYTE_LATCH_aI : apex20ke_lcell
-- Equation(s):
-- DSP_BYTE_LATCH = DFFE(DSP_BYTE_LO_acombout # DSP_BYTE_LATCH & !DSP_SHIFT_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_BYTE_LATCH,
	datac => DSP_SHIFT_areg0,
	datad => DSP_BYTE_LO_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_BYTE_LATCH);

DSP_SHIFT_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_SHIFT_areg0 = DFFE(DSP_BYTE_LATCH # DSP_SHIFT_areg0 & (DSP_SHIFT_a35 # !SHIFT_CNT_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECFC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_SHIFT_a35,
	datab => DSP_BYTE_LATCH,
	datac => DSP_SHIFT_areg0,
	datad => SHIFT_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_SHIFT_areg0);

DSP_REG_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_REG(31),
	combout => DSP_REG_a31_a_acombout);

iDSP_REG_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_REG_a31_a = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a30_a # !DSP_SHIFT_areg0 & (DSP_REG_a31_a_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_REG_a30_a,
	datac => DSP_SHIFT_areg0,
	datad => DSP_REG_a31_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_REG_a31_a);

DSP_D0_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_D0_areg0 = DFFE(DSP_SHIFT_areg0 & iDSP_REG_a31_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_CLK_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_SHIFT_areg0,
	datad => iDSP_REG_a31_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_D0_areg0);

DSP_MR_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a1_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a1_a $ DSP_MR_CNT_a0_a_a186, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a1_a_a183 = CARRY(!DSP_MR_CNT_a0_a_a186 # !DSP_MR_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a1_a,
	cin => DSP_MR_CNT_a0_a_a186,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a1_a,
	cout => DSP_MR_CNT_a1_a_a183);

DSP_MR_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a2_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a2_a $ !DSP_MR_CNT_a1_a_a183, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a2_a_a180 = CARRY(DSP_MR_CNT_a2_a & !DSP_MR_CNT_a1_a_a183)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a2_a,
	cin => DSP_MR_CNT_a1_a_a183,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a2_a,
	cout => DSP_MR_CNT_a2_a_a180);

DSP_MR_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a3_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a3_a $ DSP_MR_CNT_a2_a_a180, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a3_a_a177 = CARRY(!DSP_MR_CNT_a2_a_a180 # !DSP_MR_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a3_a,
	cin => DSP_MR_CNT_a2_a_a180,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a3_a,
	cout => DSP_MR_CNT_a3_a_a177);

DSP_MR_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a4_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a4_a $ !DSP_MR_CNT_a3_a_a177, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a4_a_a198 = CARRY(DSP_MR_CNT_a4_a & !DSP_MR_CNT_a3_a_a177)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a4_a,
	cin => DSP_MR_CNT_a3_a_a177,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a4_a,
	cout => DSP_MR_CNT_a4_a_a198);

DSP_MR_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a5_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a5_a $ DSP_MR_CNT_a4_a_a198, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a5_a_a195 = CARRY(!DSP_MR_CNT_a4_a_a198 # !DSP_MR_CNT_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a5_a,
	cin => DSP_MR_CNT_a4_a_a198,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a5_a,
	cout => DSP_MR_CNT_a5_a_a195);

DSP_MR_CNT_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a6_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a6_a $ !DSP_MR_CNT_a5_a_a195, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a6_a_a192 = CARRY(DSP_MR_CNT_a6_a & !DSP_MR_CNT_a5_a_a195)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a6_a,
	cin => DSP_MR_CNT_a5_a_a195,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a6_a,
	cout => DSP_MR_CNT_a6_a_a192);

DSP_MR_CNT_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a7_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a7_a $ DSP_MR_CNT_a6_a_a192, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a7_a_a189 = CARRY(!DSP_MR_CNT_a6_a_a192 # !DSP_MR_CNT_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a7_a,
	cin => DSP_MR_CNT_a6_a_a192,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a7_a,
	cout => DSP_MR_CNT_a7_a_a189);

DSP_MR_CNT_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a8_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a8_a $ !DSP_MR_CNT_a7_a_a189, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a8_a_a210 = CARRY(DSP_MR_CNT_a8_a & !DSP_MR_CNT_a7_a_a189)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a8_a,
	cin => DSP_MR_CNT_a7_a_a189,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a8_a,
	cout => DSP_MR_CNT_a8_a_a210);

DSP_MR_CNT_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a10_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a10_a $ !DSP_MR_CNT_a9_a_a207, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a10_a_a204 = CARRY(DSP_MR_CNT_a10_a & !DSP_MR_CNT_a9_a_a207)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a10_a,
	cin => DSP_MR_CNT_a9_a_a207,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a10_a,
	cout => DSP_MR_CNT_a10_a_a204);

DSP_MR_CNT_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a11_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a11_a $ DSP_MR_CNT_a10_a_a204, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a11_a_a201 = CARRY(!DSP_MR_CNT_a10_a_a204 # !DSP_MR_CNT_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a11_a,
	cin => DSP_MR_CNT_a10_a_a204,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a11_a,
	cout => DSP_MR_CNT_a11_a_a201);

DSP_MR_CNT_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a12_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a12_a $ !DSP_MR_CNT_a11_a_a201, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a12_a_a222 = CARRY(DSP_MR_CNT_a12_a & !DSP_MR_CNT_a11_a_a201)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a12_a,
	cin => DSP_MR_CNT_a11_a_a201,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a12_a,
	cout => DSP_MR_CNT_a12_a_a222);

DSP_MR_CNT_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a13_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a13_a $ (DSP_MR_CNT_a12_a_a222), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a13_a_a219 = CARRY(!DSP_MR_CNT_a12_a_a222 # !DSP_MR_CNT_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a13_a,
	cin => DSP_MR_CNT_a12_a_a222,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a13_a,
	cout => DSP_MR_CNT_a13_a_a219);

DSP_MR_CNT_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a14_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a14_a $ !DSP_MR_CNT_a13_a_a219, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a14_a_a216 = CARRY(DSP_MR_CNT_a14_a & !DSP_MR_CNT_a13_a_a219)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a14_a,
	cin => DSP_MR_CNT_a13_a_a219,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a14_a,
	cout => DSP_MR_CNT_a14_a_a216);

rtl_a234_I : apex20ke_lcell
-- Equation(s):
-- rtl_a245 = !DSP_MR_CNT_a9_a & !DSP_MR_CNT_a10_a & !DSP_MR_CNT_a8_a & DSP_MR_CNT_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a9_a,
	datab => DSP_MR_CNT_a10_a,
	datac => DSP_MR_CNT_a8_a,
	datad => DSP_MR_CNT_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a234,
	cascout => rtl_a245);

rtl_a239_I : apex20ke_lcell
-- Equation(s):
-- rtl_a239 = (!DSP_MR_CNT_a15_a & !DSP_MR_CNT_a14_a & !DSP_MR_CNT_a12_a & !DSP_MR_CNT_a13_a) & CASCADE(rtl_a245)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a15_a,
	datab => DSP_MR_CNT_a14_a,
	datac => DSP_MR_CNT_a12_a,
	datad => DSP_MR_CNT_a13_a,
	cascin => rtl_a245,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a239);

DSP_MR_CNT_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a15_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a15_a $ DSP_MR_CNT_a14_a_a216, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a15_a_a213 = CARRY(!DSP_MR_CNT_a14_a_a216 # !DSP_MR_CNT_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a15_a,
	cin => DSP_MR_CNT_a14_a_a216,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a15_a,
	cout => DSP_MR_CNT_a15_a_a213);

DSP_MR_CNT_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a16_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a16_a $ !DSP_MR_CNT_a15_a_a213, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a16_a_a231 = CARRY(DSP_MR_CNT_a16_a & !DSP_MR_CNT_a15_a_a213)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a16_a,
	cin => DSP_MR_CNT_a15_a_a213,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a16_a,
	cout => DSP_MR_CNT_a16_a_a231);

DSP_MR_CNT_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a17_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a17_a $ DSP_MR_CNT_a16_a_a231, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a17_a_a234 = CARRY(!DSP_MR_CNT_a16_a_a231 # !DSP_MR_CNT_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a17_a,
	cin => DSP_MR_CNT_a16_a_a231,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a17_a,
	cout => DSP_MR_CNT_a17_a_a234);

DSP_MR_CNT_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a18_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a18_a $ (!DSP_MR_CNT_a17_a_a234), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a18_a_a228 = CARRY(DSP_MR_CNT_a18_a & (!DSP_MR_CNT_a17_a_a234))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a18_a,
	cin => DSP_MR_CNT_a17_a_a234,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a18_a,
	cout => DSP_MR_CNT_a18_a_a228);

DSP_MR_CNT_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a20_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a20_a $ !DSP_MR_CNT_a19_a_a225, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_MR_CNT_a20_a_a240 = CARRY(DSP_MR_CNT_a20_a & !DSP_MR_CNT_a19_a_a225)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a20_a,
	cin => DSP_MR_CNT_a19_a_a225,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a20_a,
	cout => DSP_MR_CNT_a20_a_a240);

DSP_MR_CNT_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_MR_CNT_a21_a = DFFE(GLOBAL(DSP_MR_areg0) & DSP_MR_CNT_a20_a_a240 $ DSP_MR_CNT_a21_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => DSP_MR_CNT_a21_a,
	cin => DSP_MR_CNT_a20_a_a240,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_CNT_a21_a);

rtl_a236_I : apex20ke_lcell
-- Equation(s):
-- rtl_a246 = DSP_MR_CNT_a19_a & !DSP_MR_CNT_a17_a & DSP_MR_CNT_a18_a & DSP_MR_CNT_a16_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => DSP_MR_CNT_a19_a,
	datab => DSP_MR_CNT_a17_a,
	datac => DSP_MR_CNT_a18_a,
	datad => DSP_MR_CNT_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a236,
	cascout => rtl_a246);

rtl_a240_I : apex20ke_lcell
-- Equation(s):
-- rtl_a240 = (DSP_MR_CNT_a21_a & (DSP_MR_CNT_a20_a)) & CASCADE(rtl_a246)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_CNT_a21_a,
	datad => DSP_MR_CNT_a20_a,
	cascin => rtl_a246,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a240);

rtl_a3_I : apex20ke_lcell
-- Equation(s):
-- rtl_a3 = rtl_a238 & (rtl_a239 & rtl_a240)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a238,
	datac => rtl_a239,
	datad => rtl_a240,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a3);

DSP_MR_ED_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_MR_ED,
	combout => DSP_MR_ED_acombout);

DSP_MR_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_MR_areg0 = DFFE(DSP_MR_ED_acombout # DSP_MR_areg0 & !rtl_a3, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_MR_areg0,
	datac => rtl_a3,
	datad => DSP_MR_ED_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_MR_areg0);

DSP_BOOT_OUT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_BOOT_OUT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_BOOT_OUT);

DSP_SHIFT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_SHIFT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_SHIFT);

DSP_CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_DSP_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_CLK);

DSP_D0_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_D0_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_D0);

DSP_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_MR);
END structure;


