onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/dsp_config
add wave -noupdate -format Logic -radix binary /tb/dsp_byte_lo
add wave -noupdate -format Logic -radix binary /tb/dsp_mr_ed
add wave -noupdate -format Logic -radix binary /tb/dsp_boot_in
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_reg
add wave -noupdate -format Logic -radix binary /tb/dsp_shift
add wave -noupdate -format Logic -radix binary /tb/dsp_clk
add wave -noupdate -format Logic -radix binary /tb/dsp_d0
add wave -noupdate -format Logic -radix binary /tb/dsp_mr
add wave -noupdate -format Logic -radix binary /tb/dsp_boot_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {8044355 ps}
WaveRestoreZoom {0 ps} {105 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
