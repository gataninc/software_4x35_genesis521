-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DSPBOOT.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		June 20, 2001 - MCS
--			Module has been debuggged and implemented in the EDI-III Board
--
--   Description:	
--                  PCI - to AMCC S5920 Interface Logic
--				The timing to boot the DSP (TMS320C32-60) is based on the DSP_CLK being 10MHZ
--				
--				Anything Faster or Slower will not allow the DSP to boot.
--
--
--        ---+     +-----------------------------------------------------------------------
-- DSP_MR    |     |
--           +-----+
--
--        -------------+     +-----------------------------------------------------------------------
-- INT3                |     |
--                     +-----+
--              +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +
-- DSP_CLK      |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |     |
--         -----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----+
--
--                                       +------------------------------------------------+
-- DSP_SHIFT                             |                                                |
--         ------------------------------+                                                +-----------------------
--
--                                                    /---------\ /---------\ /---------\ /---------\ 
-- DSP_D0  ------------------------------------------X           X           X           X           X-------------
--                                                    \_________/ \_________/ \_________/ \_________/
--
--
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
library LPM;
     use LPM.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
entity DSPBOOT is 
	port( 
     	IMR       	: in      std_logic;                         -- Local Master Reset
	     CLK20		: in      std_logic;                         -- Local System Clock
		DSP_CONFIG	: in		std_Logic;
		DSP_BYTE_LO	: in 	std_logic;					-- Load Low Byte to DSP COnfig Reg
		DSP_MR_ED 	: in		std_logic;					-- DSP Master Reset Edge Detect
		DSP_BOOT_IN	: in		std_logic;
		DSP_REG		: in		std_logic_Vector( 31 downto 0 );
		DSP_BOOT_OUT	: buffer	std_Logic;
		DSP_SHIFT		: buffer 	std_logic;
		DSP_CLK		: buffer	std_Logic;
		DSP_D0		: buffer	std_logic;
		DSP_MR		: buffer	std_logic );
end DSPBOOT;
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
architecture behavioral of DSPBOOT is
     -- Constant Declarations
	constant SHIFT_CNT_MAX 		: integer := 31;		-- 63;
--	constant MAX_DSP_MR_CNT		: integer := 59;		-- 3us@20Mhz for Simulations 
	constant MAX_DSP_MR_CNT 		: integer := 3999999;	-- 200ms @20Mhz
	constant DSP_BOOT_CNT_MAX 	: integer := 7;

	constant ZEROES			: std_logic_Vector( 31 downto 0 ) :=
		"00000000000000000000000000000000";
	-- Any Type Declarations

     -- Combinatorial Signals
	signal SHIFT_CNT		: integer range 0 to SHIFT_CNT_MAX;
	signal SHIFT_CNT_TC		: std_Logic;
	signal iDSP_REG		: std_logic_vector( 31 downto 0 );
	signal DSP_MR_CNT		: integer range 0 to MAX_DSP_MR_CNT;


	signal DSP_BOOT_VEC 	: std_logic_VEctor( 1 downto 0 );
	signal DSP_BOOT_CNT 	: integer range 0 to DSP_BOOT_CNT_MAX;
	signal DSP_BOOT_CNT_TC 	: std_logic;
	signal DSP_REG31_DEL	: std_Logic;
	signal DSP_BYTE_LATCH	: std_Logic;

-----------------------------------------------------------------------------------------
begin
	SHIFT_CNT_TC 		<= '1' when ( SHIFT_CNT = SHIFT_CNT_MAX ) else '0';
	DSP_BOOT_CNT_TC	<= '1' when ( DSP_BOOT_CNT = DSP_BOOT_CNT_MAX ) else '0';
		
     ------------------------------------------------------------------------------------
 	CLK20_PROC : process( IMR, CLK20 )
     begin
          if( IMR = '1' ) then
			DSP_CLK		<= '1';
			DSP_MR		<= '0';
			DSP_MR_CNT	<= 0;
			DSP_BOOT_VEC	<= "00";
			DSP_BOOT_CNT	<= 0;
			DSP_BOOT_OUT	<= '0';
			DSP_BYTE_LATCH	<= '0';
			DSP_SHIFT		<= '0';
			DSP_D0		<= '0';
			iDSP_REG		<= ZEROES( 31 downto 0 );
			SHIFT_CNT		<= 0;

          elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			DSP_BOOT_VEC <= DSP_BOOT_VEC(0) & DSP_BOOT_IN;
			
			if( DSP_BOOT_VEC = "01" )
				then DSP_BOOT_OUT <= '1';
			elsif( DSP_BOOT_CNT_TC = '1' )
				then DSP_BOOT_OUT <= '0';
			end if;
	
			if( DSP_BOOT_VEC = "01" ) or ( DSP_BOOT_OUT = '0' )
				then DSP_BOOT_CNT <= 0;
			elsif( DSP_BOOT_OUT = '1' )
				theN DSP_BOOT_CNT <= DSP_BOOT_CNT + 1;
			end if;

			-- DSP Boot Sequencer --------------------------------------------------------------
			if( DSP_BYTE_LO = '1' ) 
				then DSP_BYTE_LATCH <= '1';
			elsif( DSP_SHIFT = '1' )
				then DSP_BYTE_LATCH <= '0';
			end if;

			if( DSP_CONFIG = '0' )
				then DSP_CLK <= '1';
				else DSP_CLK <= not( DSP_CLK );
			end if;

			-- Master Reset for the DSP used for booting the DSP -------------------------------
			if( DSP_MR_ED = '1' )
				then DSP_MR <= '1';
			elsif( DSP_MR_CNT = MAX_DSP_MR_CNT )
				then DSP_MR <= '0';
			end if;

			if( DSP_MR = '1' )
				then DSP_MR_CNT <= DSP_MR_CNT + 1;
				else DSP_MR_CNT <= 0;
			end if;
			-- Master Reset for the DSP used for booting the DSP -------------------------------

			if( DSP_CLK = '0' ) then		
				if( DSP_BYTE_LATCH = '1' ) 
					then DSP_SHIFT <= '1';
				elsif( SHIFT_CNT_TC = '1' )
					then DSP_SHIFT <= '0';
				end if;

				if( DSP_SHIFT = '1' )
					then DSP_D0 <= iDSP_REG(31);
					else DSP_D0 <= '0';
				end if;
			
				if( DSP_SHIFT = '0' ) 
					then iDSP_REG	<= DSP_REG;
				elsif( DSP_SHIFT = '1' )
					then iDSP_REG <= iDSP_REG( 30 downto 0 ) & '0';
				end if;

				if( DSP_SHIFT = '0' )
					then SHIFT_CNT <= 0;
					else SHIFT_CNT <= SHIFT_CNT + 1;
				end if;
			end if;
		end if;
	end process;			
     ------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
end behavioral;			-- DSPBOOT.VHD
-----------------------------------------------------------------------------------------

