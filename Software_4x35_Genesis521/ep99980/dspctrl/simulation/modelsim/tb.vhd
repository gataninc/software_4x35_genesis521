-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	dspctrl.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		June 24, 1999 - MCS
--			Module has been debuggged and implemented in the EDI-II Board
--
--   Description:	
--                  PCI - to AMCC S5920 Interface Logic
--   History:       <date> - <Author>
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------
entity tb is
end tb;

architecture test_bench of tb is

	signal IMR          : std_logic := '1';                  		-- Local Master Reset
	signal clk20		: std_logic := '0';
	signal DSP_H3		: std_logic := '0';               	-- Local System Clock
	signal DSP_EN		: std_logic := '1';
	signal DSP_WR		: std_logic;
	signal DSP_STB1	: std_logic_Vector( 3 downto 0 );
	signal DSP_A		: std_logic_Vector( 2 downto 0 );
	signal DSP_A1		: std_Logic_vector( 7 downto 0 );
	signal FF_HF		: std_logic;
	signal INTR_EN		: std_logic;
	signal PFF_EF		: std_logic_vector( 1 downto 0 ); -- PCI FIFO Empty Flag
	signal PFF_FF		: std_logic_vector( 1 downto 0 );
	signal DSP_Read	: std_Logic;
	signal DP_OEL		: std_Logic;
	signal DP_WRL		: std_logic;
	signal WE_DSP_TREG	: std_logic;
	signal PFF_WEN		: std_logic;
	signal PFF_WCK		: std_logic;
	signal AO_INTR		: std_logic;
	signal DSP_D		: std_logic_Vector( 15 downto 0 );

	-----------------------------------------------------------------------------------------
	component dspctrl is 
		port( 
	     	IMR            : in      std_logic;                  		-- Local Master Reset
		     DSP_H3		: in      std_logic;                      	-- Local System Clock
			DSP_WR		: in		std_logic;
			DSP_EN		: in		std_logic;
			DSP_STB1		: in		std_logic_Vector( 3 downto 0 );
			DSP_A		: in		std_logic_Vector( 2 downto 0 );
			DSP_A1		: in		std_Logic_vector( 7 downto 0 );
			FF_HF		: in		std_logic;
			INTR_EN		: in		std_logic;
			PFF_FF		: in		std_logic_vector( 1 downto 0 );
			DSP_Read		: out	std_Logic;
			DP_OEL		: out	std_Logic;
			DP_WRL		: out	std_logic;
			WE_DSP_TREG	: out	std_logic;
			PFF_WEN		: out	std_logic;
			PFF_MR		: out	std_logic;
			AO_INTR		: out	std_logic );
	end component dspctrl;
	-----------------------------------------------------------------------------------------
begin
	imr 			<= '0' after 923 ns;
	
	DSP_H3 		<= not( DSP_H3 ) after 15 ns;
	CLK20		<= not( CLK20  ) after 25 ns;

	FF_HF		<= '0';
	INTR_EN		<= '0';
	PFF_EF		<= "11";
	PFF_FF		<= "11";

	
	U : dspctrl
		port map(
			IMR            => imr,
		     DSP_H3		=> dsp_h3,
			DSP_WR		=> dsp_wr,
			DSP_EN		=> dsp_en,
			DSP_STB1		=> dsp_stb1,
			DSP_A		=> dsp_A,
			DSP_A1		=> dsp_a1,
			FF_HF		=> ff_hf,
			INTR_EN		=> intr_en,
			PFF_FF		=> PFF_FF,
			DSP_Read		=> DSP_Read,
			DP_OEL		=> DP_OEL,
			DP_WRL		=> DP_WRL,
			WE_DSP_TREG	=> WE_DSP_TREG,
			PFF_WEN		=> PFF_WEN,
			AO_INTR		=> AO_INTR );

-----------------------------------------------------------------------------------------
	dsp_wrt_proc : process
	begin
		DSP_WR		<= '1';
		DSP_STB1		<= x"F";
		DSP_A		<= "010";
		DSP_A1		<= x"FF";
		DSP_D		<= x"0000";
		wait for 10 us;

			wait until (( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			DSP_A	<= "010" after 7 ns;	-- Board ID
			DSP_A1 	<= x"00" after 7 ns;	-- PCI FIFO Address
			wait until (( DSP_H3'Event ) and ( DSP_H3 = '0' ));
			DSP_WR 	<= '0' after 7 ns;

			wait until (( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			DSP_STB1 	<= x"0" after 7 ns;
			DSP_D	<= x"FFFF";

			wait until (( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			wait until (( DSP_H3'Event ) and ( DSP_H3 = '1' ));
			DSP_STB1	<= x"F" after 7 ns;
			
			wait until (( DSP_H3'Event ) and ( DSP_H3 = '0' ));
			DSP_D	<= x"FFFF";
			DSP_A	<= "000" after 8 ns;	-- Board ID
			DSP_A1 	<= x"FF" after 8 ns;	-- PCI FIFO Address
			DSP_WR	<= '1' 	after 8 ns;
			
		wait;			
	
	end process;
-----------------------------------------------------------------------------------------
end test_bench;			-- dspctrl.VHD
-----------------------------------------------------------------------------------------

