-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 11:09:36"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	dspctrl IS
    PORT (
	IMR : IN std_logic;
	DSP_H3 : IN std_logic;
	DSP_WR : IN std_logic;
	DSP_EN : IN std_logic;
	DSP_STB1 : IN std_logic_vector(3 DOWNTO 0);
	DSP_A : IN std_logic_vector(2 DOWNTO 0);
	DSP_A1 : IN std_logic_vector(7 DOWNTO 0);
	FF_HF : IN std_logic;
	INTR_EN : IN std_logic;
	PFF_FF : IN std_logic_vector(1 DOWNTO 0);
	DSP_Read : OUT std_logic;
	DP_OEL : OUT std_logic;
	DP_WRL : OUT std_logic;
	WE_DSP_TREG : OUT std_logic;
	PFF_WEN : OUT std_logic;
	PFF_MR : OUT std_logic;
	AO_INTR : OUT std_logic
	);
END dspctrl;

ARCHITECTURE structure OF dspctrl IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_DSP_H3 : std_logic;
SIGNAL ww_DSP_WR : std_logic;
SIGNAL ww_DSP_EN : std_logic;
SIGNAL ww_DSP_STB1 : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_DSP_A : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_DSP_A1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_FF_HF : std_logic;
SIGNAL ww_INTR_EN : std_logic;
SIGNAL ww_PFF_FF : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_DSP_Read : std_logic;
SIGNAL ww_DP_OEL : std_logic;
SIGNAL ww_DP_WRL : std_logic;
SIGNAL ww_WE_DSP_TREG : std_logic;
SIGNAL ww_PFF_WEN : std_logic;
SIGNAL ww_PFF_MR : std_logic;
SIGNAL ww_AO_INTR : std_logic;
SIGNAL DSP_STB1_a1_a_acombout : std_logic;
SIGNAL DSP_A1_a5_a_acombout : std_logic;
SIGNAL DSP_A1_a4_a_acombout : std_logic;
SIGNAL DSP_EN_acombout : std_logic;
SIGNAL DSP_STB1_a2_a_acombout : std_logic;
SIGNAL DSP_STB1_a0_a_acombout : std_logic;
SIGNAL DSP_STB1_a3_a_acombout : std_logic;
SIGNAL DSP_Read_a121 : std_logic;
SIGNAL DSP_WR_acombout : std_logic;
SIGNAL DSP_A_a2_a_acombout : std_logic;
SIGNAL DSP_Read_a123 : std_logic;
SIGNAL DSP_A_a0_a_acombout : std_logic;
SIGNAL DSP_A_a1_a_acombout : std_logic;
SIGNAL DSP_Read_a122 : std_logic;
SIGNAL DSP_Read_a124 : std_logic;
SIGNAL DP_OEL_a10 : std_logic;
SIGNAL DP_WRL_a28 : std_logic;
SIGNAL DP_WRL_a29 : std_logic;
SIGNAL DSP_A1_a2_a_acombout : std_logic;
SIGNAL DSP_A1_a0_a_acombout : std_logic;
SIGNAL DSP_A1_a7_a_acombout : std_logic;
SIGNAL DSP_A1_a3_a_acombout : std_logic;
SIGNAL DSP_A1_a6_a_acombout : std_logic;
SIGNAL DSP_PROC_a117 : std_logic;
SIGNAL DSP_A1_a1_a_acombout : std_logic;
SIGNAL DSP_Read_a126 : std_logic;
SIGNAL DSP_PROC_a125 : std_logic;
SIGNAL DSP_PROC_a126 : std_logic;
SIGNAL WE_DSP_TREG_a34 : std_logic;
SIGNAL DSP_PROC_a118 : std_logic;
SIGNAL DSP_H3_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL PFF_WEN_areg0 : std_logic;
SIGNAL FF_MR_a0_a_a11 : std_logic;
SIGNAL FF_MR_a0_a_a12 : std_logic;
SIGNAL DSP_PROC_a7 : std_logic;
SIGNAL FF_MR_a1_a : std_logic;
SIGNAL FF_MR_a2_a : std_logic;
SIGNAL PFF_MR_a32 : std_logic;
SIGNAL PFF_MR_areg0 : std_logic;
SIGNAL INTR_EN_acombout : std_logic;
SIGNAL DSP_PROC_a119 : std_logic;
SIGNAL PFF_FF_a1_a_acombout : std_logic;
SIGNAL PFF_FF_a0_a_acombout : std_logic;
SIGNAL FF_VEC_a0_a : std_logic;
SIGNAL FF_HF_acombout : std_logic;
SIGNAL HF_VEC_a0_a : std_logic;
SIGNAL HF_VEC_a1_a : std_logic;
SIGNAL FF_VEC_a1_a : std_logic;
SIGNAL AO_INTR_a83 : std_logic;
SIGNAL AO_INTR_a81 : std_logic;
SIGNAL AO_INTR_a82 : std_logic;
SIGNAL AO_INTR_a84 : std_logic;
SIGNAL AO_INTR_areg0 : std_logic;
SIGNAL ALT_INV_AO_INTR_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_DSP_H3 <= DSP_H3;
ww_DSP_WR <= DSP_WR;
ww_DSP_EN <= DSP_EN;
ww_DSP_STB1 <= DSP_STB1;
ww_DSP_A <= DSP_A;
ww_DSP_A1 <= DSP_A1;
ww_FF_HF <= FF_HF;
ww_INTR_EN <= INTR_EN;
ww_PFF_FF <= PFF_FF;
DSP_Read <= ww_DSP_Read;
DP_OEL <= ww_DP_OEL;
DP_WRL <= ww_DP_WRL;
WE_DSP_TREG <= ww_WE_DSP_TREG;
PFF_WEN <= ww_PFF_WEN;
PFF_MR <= ww_PFF_MR;
AO_INTR <= ww_AO_INTR;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_AO_INTR_areg0 <= NOT AO_INTR_areg0;

DSP_STB1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(1),
	combout => DSP_STB1_a1_a_acombout);

DSP_A1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(5),
	combout => DSP_A1_a5_a_acombout);

DSP_A1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(4),
	combout => DSP_A1_a4_a_acombout);

DSP_EN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_EN,
	combout => DSP_EN_acombout);

DSP_STB1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(2),
	combout => DSP_STB1_a2_a_acombout);

DSP_STB1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(0),
	combout => DSP_STB1_a0_a_acombout);

DSP_STB1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB1(3),
	combout => DSP_STB1_a3_a_acombout);

DSP_Read_a121_I : apex20ke_lcell
-- Equation(s):
-- DSP_Read_a121 = !DSP_STB1_a1_a_acombout & !DSP_STB1_a2_a_acombout & !DSP_STB1_a0_a_acombout & !DSP_STB1_a3_a_acombout
-- DSP_Read_a126 = !DSP_STB1_a1_a_acombout & !DSP_STB1_a2_a_acombout & !DSP_STB1_a0_a_acombout & !DSP_STB1_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB1_a1_a_acombout,
	datab => DSP_STB1_a2_a_acombout,
	datac => DSP_STB1_a0_a_acombout,
	datad => DSP_STB1_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_Read_a121,
	cascout => DSP_Read_a126);

DSP_WR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_WR,
	combout => DSP_WR_acombout);

DSP_A_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(2),
	combout => DSP_A_a2_a_acombout);

DSP_Read_a123_I : apex20ke_lcell
-- Equation(s):
-- DSP_Read_a123 = DSP_WR_acombout & !DSP_A_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_WR_acombout,
	datad => DSP_A_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_Read_a123);

DSP_A_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(0),
	combout => DSP_A_a0_a_acombout);

DSP_A_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(1),
	combout => DSP_A_a1_a_acombout);

DSP_Read_a122_I : apex20ke_lcell
-- Equation(s):
-- DSP_Read_a122 = !DSP_A_a1_a_acombout # !DSP_A_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A_a0_a_acombout,
	datad => DSP_A_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_Read_a122);

DSP_Read_a124_I : apex20ke_lcell
-- Equation(s):
-- DSP_Read_a124 = DSP_EN_acombout & DSP_Read_a121 & DSP_Read_a123 & DSP_Read_a122

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_EN_acombout,
	datab => DSP_Read_a121,
	datac => DSP_Read_a123,
	datad => DSP_Read_a122,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_Read_a124);

DP_OEL_a10_I : apex20ke_lcell
-- Equation(s):
-- DP_OEL_a10 = DSP_EN_acombout & !DSP_A_a1_a_acombout & DSP_Read_a123 & DSP_Read_a121

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_EN_acombout,
	datab => DSP_A_a1_a_acombout,
	datac => DSP_Read_a123,
	datad => DSP_Read_a121,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DP_OEL_a10);

DP_WRL_a28_I : apex20ke_lcell
-- Equation(s):
-- DP_WRL_a28 = !DSP_A_a2_a_acombout & !DSP_WR_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A_a2_a_acombout,
	datad => DSP_WR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DP_WRL_a28);

DP_WRL_a29_I : apex20ke_lcell
-- Equation(s):
-- DP_WRL_a29 = DSP_EN_acombout & !DSP_A_a1_a_acombout & DP_WRL_a28 & DSP_Read_a121

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_EN_acombout,
	datab => DSP_A_a1_a_acombout,
	datac => DP_WRL_a28,
	datad => DSP_Read_a121,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DP_WRL_a29);

DSP_A1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(2),
	combout => DSP_A1_a2_a_acombout);

DSP_A1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(0),
	combout => DSP_A1_a0_a_acombout);

DSP_A1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(7),
	combout => DSP_A1_a7_a_acombout);

DSP_A1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(3),
	combout => DSP_A1_a3_a_acombout);

DSP_A1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(6),
	combout => DSP_A1_a6_a_acombout);

DSP_PROC_a117_I : apex20ke_lcell
-- Equation(s):
-- DSP_PROC_a117 = !DSP_A1_a5_a_acombout & !DSP_A1_a7_a_acombout & !DSP_A1_a3_a_acombout & !DSP_A1_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a5_a_acombout,
	datab => DSP_A1_a7_a_acombout,
	datac => DSP_A1_a3_a_acombout,
	datad => DSP_A1_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_PROC_a117);

DSP_A1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(1),
	combout => DSP_A1_a1_a_acombout);

DSP_PROC_a125_I : apex20ke_lcell
-- Equation(s):
-- DSP_PROC_a125 = (!DSP_A_a0_a_acombout & !DSP_A_a2_a_acombout & !DSP_WR_acombout & DSP_A_a1_a_acombout) & CASCADE(DSP_Read_a126)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A_a0_a_acombout,
	datab => DSP_A_a2_a_acombout,
	datac => DSP_WR_acombout,
	datad => DSP_A_a1_a_acombout,
	cascin => DSP_Read_a126,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_PROC_a125);

DSP_PROC_a118_I : apex20ke_lcell
-- Equation(s):
-- DSP_PROC_a118 = !DSP_A1_a4_a_acombout & DSP_PROC_a117 & !DSP_A1_a1_a_acombout & DSP_PROC_a125
-- DSP_PROC_a126 = !DSP_A1_a4_a_acombout & DSP_PROC_a117 & !DSP_A1_a1_a_acombout & DSP_PROC_a125

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a4_a_acombout,
	datab => DSP_PROC_a117,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_PROC_a125,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_PROC_a118,
	cascout => DSP_PROC_a126);

WE_DSP_TREG_a34_I : apex20ke_lcell
-- Equation(s):
-- WE_DSP_TREG_a34 = (DSP_A1_a2_a_acombout & !DSP_A1_a0_a_acombout) & CASCADE(DSP_PROC_a126)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a2_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	cascin => DSP_PROC_a126,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => WE_DSP_TREG_a34);

DSP_H3_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_H3,
	combout => DSP_H3_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

PFF_WEN_areg0_I : apex20ke_lcell
-- Equation(s):
-- PFF_WEN_areg0 = DFFE(DSP_PROC_a118 & !DSP_A1_a0_a_acombout & !DSP_A1_a2_a_acombout, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0202",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_PROC_a118,
	datab => DSP_A1_a0_a_acombout,
	datac => DSP_A1_a2_a_acombout,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PFF_WEN_areg0);

FF_MR_a0_a_a11_I : apex20ke_lcell
-- Equation(s):
-- FF_MR_a0_a_a11 = DFFE(DSP_A1_a2_a_acombout, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a2_a_acombout,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_MR_a0_a_a11);

FF_MR_a0_a_a12_I : apex20ke_lcell
-- Equation(s):
-- FF_MR_a0_a_a12 = DFFE(DSP_A1_a0_a_acombout & DSP_PROC_a118, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_A1_a0_a_acombout,
	datad => DSP_PROC_a118,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_MR_a0_a_a12);

DSP_PROC_a7_I : apex20ke_lcell
-- Equation(s):
-- DSP_PROC_a7 = FF_MR_a0_a_a11 & FF_MR_a0_a_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => FF_MR_a0_a_a11,
	datac => FF_MR_a0_a_a12,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_PROC_a7);

FF_MR_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_MR_a1_a = DFFE(FF_MR_a0_a_a11 & FF_MR_a0_a_a12, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => FF_MR_a0_a_a11,
	datad => FF_MR_a0_a_a12,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_MR_a1_a);

FF_MR_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_MR_a2_a = DFFE(FF_MR_a1_a, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_MR_a1_a,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_MR_a2_a);

PFF_MR_a32_I : apex20ke_lcell
-- Equation(s):
-- PFF_MR_a32 = FF_MR_a2_a # FF_MR_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FF_MR_a2_a,
	datad => FF_MR_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_MR_a32);

PFF_MR_areg0_I : apex20ke_lcell
-- Equation(s):
-- PFF_MR_areg0 = DFFE(IMR_acombout & PFF_MR_areg0 # !IMR_acombout & (DSP_PROC_a7 # PFF_MR_a32), GLOBAL(DSP_H3_acombout), , , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DDD8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IMR_acombout,
	datab => PFF_MR_areg0,
	datac => DSP_PROC_a7,
	datad => PFF_MR_a32,
	clk => DSP_H3_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PFF_MR_areg0);

INTR_EN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_INTR_EN,
	combout => INTR_EN_acombout);

DSP_PROC_a119_I : apex20ke_lcell
-- Equation(s):
-- DSP_PROC_a119 = DSP_A1_a0_a_acombout & DSP_PROC_a118

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_A1_a0_a_acombout,
	datac => DSP_PROC_a118,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_PROC_a119);

PFF_FF_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_FF(1),
	combout => PFF_FF_a1_a_acombout);

PFF_FF_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PFF_FF(0),
	combout => PFF_FF_a0_a_acombout);

FF_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_VEC_a0_a = DFFE(!PFF_FF_a1_a_acombout & !PFF_FF_a0_a_acombout, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PFF_FF_a1_a_acombout,
	datad => PFF_FF_a0_a_acombout,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_VEC_a0_a);

FF_HF_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_HF,
	combout => FF_HF_acombout);

HF_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- HF_VEC_a0_a = DFFE(FF_HF_acombout, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_HF_acombout,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => HF_VEC_a0_a);

HF_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- HF_VEC_a1_a = DFFE(HF_VEC_a0_a, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => HF_VEC_a0_a,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => HF_VEC_a1_a);

FF_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- FF_VEC_a1_a = DFFE(FF_VEC_a0_a, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => FF_VEC_a0_a,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_VEC_a1_a);

AO_INTR_a83_I : apex20ke_lcell
-- Equation(s):
-- AO_INTR_a83 = HF_VEC_a0_a & (FF_VEC_a0_a & !FF_VEC_a1_a # !HF_VEC_a1_a) # !HF_VEC_a0_a & FF_VEC_a0_a & (!FF_VEC_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0ACE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => HF_VEC_a0_a,
	datab => FF_VEC_a0_a,
	datac => HF_VEC_a1_a,
	datad => FF_VEC_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AO_INTR_a83);

AO_INTR_a81_I : apex20ke_lcell
-- Equation(s):
-- AO_INTR_a81 = !DSP_A1_a2_a_acombout & (DSP_A1_a1_a_acombout & !DSP_A1_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a2_a_acombout,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AO_INTR_a81);

AO_INTR_a82_I : apex20ke_lcell
-- Equation(s):
-- AO_INTR_a82 = !DSP_A1_a4_a_acombout & DSP_PROC_a117 & AO_INTR_a81 & DSP_PROC_a125

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a4_a_acombout,
	datab => DSP_PROC_a117,
	datac => AO_INTR_a81,
	datad => DSP_PROC_a125,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AO_INTR_a82);

AO_INTR_a84_I : apex20ke_lcell
-- Equation(s):
-- AO_INTR_a84 = INTR_EN_acombout & !AO_INTR_a83 & (AO_INTR_a82 # !AO_INTR_areg0) # !INTR_EN_acombout & (AO_INTR_a82 # !AO_INTR_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5F13",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => INTR_EN_acombout,
	datab => AO_INTR_areg0,
	datac => AO_INTR_a83,
	datad => AO_INTR_a82,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AO_INTR_a84);

AO_INTR_areg0_I : apex20ke_lcell
-- Equation(s):
-- AO_INTR_areg0 = DFFE(INTR_EN_acombout & !DSP_A1_a2_a_acombout & DSP_PROC_a119 # !AO_INTR_a84, GLOBAL(DSP_H3_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "20FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => INTR_EN_acombout,
	datab => DSP_A1_a2_a_acombout,
	datac => DSP_PROC_a119,
	datad => AO_INTR_a84,
	clk => DSP_H3_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AO_INTR_areg0);

DSP_Read_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DSP_Read_a124,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_Read);

DP_OEL_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DP_OEL_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DP_OEL);

DP_WRL_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DP_WRL_a29,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DP_WRL);

WE_DSP_TREG_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DSP_TREG_a34,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DSP_TREG);

PFF_WEN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PFF_WEN_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_WEN);

PFF_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PFF_MR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_MR);

AO_INTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_AO_INTR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AO_INTR);
END structure;


