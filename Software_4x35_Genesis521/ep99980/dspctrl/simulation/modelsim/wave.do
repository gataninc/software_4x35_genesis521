onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/dsp_h3
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/dsp_wr
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_stb1
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_a
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_a1
add wave -noupdate -format Logic -radix binary /tb/pff_wen
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {3764361 ns} {24133 ns}
WaveRestoreZoom {0 ns} {105 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
