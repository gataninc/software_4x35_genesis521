-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	dspctrl.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		June 24, 1999 - MCS
--			Module has been debuggged and implemented in the EDI-II Board
--
--   Description:	
--                  PCI - to AMCC S5920 Interface Logic
--   History:       <date> - <Author>
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
library LPM;
     use LPM.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
entity dspctrl is 
	port( 
     	IMR            : in      std_logic;                  		-- Local Master Reset
	     DSP_H3		: in      std_logic;                      	-- Local System Clock
		DSP_WR		: in		std_logic;
		DSP_EN		: in		std_logic;
		DSP_STB1		: in		std_logic_Vector( 3 downto 0 );
		DSP_A		: in		std_logic_Vector( 2 downto 0 );
		DSP_A1		: in		std_Logic_vector( 7 downto 0 );
		FF_HF		: in		std_logic;
		INTR_EN		: in		std_logic;
		PFF_FF		: in		std_logic_vector( 1 downto 0 );
		DSP_Read		: buffer	std_Logic;
		DP_OEL		: buffer	std_Logic;
		DP_WRL		: buffer	std_logic;
		WE_DSP_TREG	: buffer	std_logic;
		PFF_WEN		: buffer	std_logic;
		PFF_MR		: buffer	std_logic;
		AO_INTR		: buffer	std_logic );
end dspctrl;
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
architecture behavioral of dspctrl is
     -- Constant Declarations
	constant DSP_DPRAM_DSP 		: integer := 0;
	constant DSP_DPRAM_FPGA 		: integer := 1;
	constant DSP_Board_ID		: integer := 2;

	constant iADR_DSP_PCI_FIFO	: integer := 16#00#;	-- Low DSP Address Bits for PCI FIFO
	constant iADR_DSP_INTR_EN	: integer := 16#01#;	-- Enable PCI Interrupt
	constant iADR_DSP_INTR_DIS	: integer := 16#02#;	-- Disable PCI Interrupt
	constant iADR_DSP_ID		: integer := 16#03#;
	constant iADR_DSP_TREG		: integer := 16#04#;	-- DS
	constant iADR_DSP_FIFO_MR	: integer := 16#05#;
	constant iADR_DSP_FF_CNT		: integer := 16#06#;


	-- Any Type Declarations

     -- Combinatorial Signals
	signal FF_RENV		: std_logic_Vector( 1 downto 0 );
--	signal iFF_WEN		: std_logic;
	signal HF_VEC		: std_logic_Vector( 1 downto 0 );
	signal FF_VEC		: std_logic_Vector( 1 downto 0 );
	signal iDP_WRL		: std_logic_Vector( 1 downto 0 );
	signal FF_MR		: std_logic_vector( 2 downto 0 );


-----------------------------------------------------------------------------------------
begin
	WE_DSP_TREG	<= '1' when ( ( DSP_WR 	= '0' ) 
						and ( DSP_STB1 = "0000" ) 
						and ( DSP_A 	= DSP_BOARD_ID )
						and ( DSP_A1	= iADR_DSP_TREG ))  else '0';


	DSP_Read	<= '1' when (( DSP_EN 	= '1' 			) 
					and ( DSP_STB1 = "0000" 			) 
					and ( DSP_WR	= '1' 			) 
					and ( DSP_A 	= DSP_DPRAM_DSP 	)) else
			   '1' when (( DSP_EN 	= '1' 			)
					and ( DSP_STB1	= "0000" 			)
					and ( DSP_WR	= '1' 			)
					and ( DSP_A	= DSP_DPRAM_FPGA 	)) else 
			   '1' when (( DSP_EN	= '1' 			)
					and ( DSP_STB1	= "0000"			)
					and ( DSP_WR	= '1' 			)
					and ( DSP_A	= DSP_Board_ID		)) else '0';
					
	-------------------------------------------------------------------------------
	-- DSP output Enable to the DPRAM
 	DP_OEL	<= '1' when (( DSP_EN 	= '1' 			) 
					and ( DSP_STB1 = "0000" 			) 
					and ( DSP_WR	= '1' 			) 
					and ( DSP_A 	= DSP_DPRAM_DSP 	)) else
 	      	   '1' when (( DSP_EN 	= '1' 			) 
					and ( DSP_STB1 = "0000" 			) 
					and ( DSP_WR	= '1' 			) 
					and ( DSP_A 	= DSP_DPRAM_FPGA  	)) else '0';
		
	-- DSP Write to the DPRAM		
	DP_WRL	<= '1' when(  ( DSP_EN 	= '1' 			) 
					and ( DSP_STB1 = X"0" 			)
					and ( DSP_WR 	= '0' 			)
					and ( DSP_A	= DSP_DPRAM_DSP 	)) else
			   '1' when ( ( DSP_EN 	= '1' 			) 
					and ( DSP_STB1 = X"0" 			)
					and ( DSP_WR 	= '0' 			)
					and ( DSP_A	= DSP_DPRAM_FPGA 	)) else '0';
	
--	PFF_WCK	<= '1' when ( ( DSP_EN   = '1' 			)
--					and ( DSP_STB1 = x"0"			)
--					and ( DSP_WR   = '0' 			)
--					and ( DSP_A	= DSP_Board_Id		)
--					and ( DSP_A1  	= iADR_DSP_PCI_FIFO )) else '0';
     ------------------------------------------------------------------------------------
	nDSP_PROC : process( DSP_H3, IMR )
	begin
		if( IMR = '1' ) then
			PFF_WEN		<= '0';
		elsif(( DSP_H3'Event ) and ( DSP_H3 = '1' )) then
			if(( DSP_WR 		= '0' 	 		) 
				and ( DSP_STB1 = "0000" 	 		) 
				and ( DSP_A	= DSP_BOARD_ID 	)
				and ( DSP_A1	= iADR_DSP_PCI_FIFO ))
					then PFF_WEN <= '1';
					else PFF_WEN <= '0';
			end if;
		end if;
	end process;
     ------------------------------------------------------------------------------------
			
     ------------------------------------------------------------------------------------
	DSP_PROC : process( DSP_H3, IMR )
	begin
		if( IMR = '1' ) then
			HF_VEC		<= "00";
			FF_VEC		<= "00";
			AO_INTR 		<= '1';
			iDP_WRL		<= "00";
			FF_MR		<= "000";
		elsif(( DSP_H3'Event ) and ( DSP_H3 = '1' )) then
			
			-- PCI Half Full Flag --------------------------------------------------------------
			-- Form a vector for edge detection to Interrupt the PCI
			if( FF_HF = '1' )
				then HF_VEC <= HF_VEC(0) & '1';
				else HF_VEC <= HF_VEC(0) & '0';
			end if;

			if( PFF_FF = "00" )
				then FF_VEC <= FF_VEC(0) & '1';
				else FF_VEC <= FF_VEC(0) & '0';
			end if;

			-- PCI Half Full Flag --------------------------------------------------------------

			-- PCI Interrupt Generation --------------------------------------------------------
			if(( INTR_EN 		= '1' ) 
				and ( DSP_WR 	= '0' ) 
				and ( DSP_STB1 = "0000" ) 
				and ( DSP_A 	= DSP_BOARD_ID )
				and ( DSP_A1 	= iADR_DSP_INTR_EN )) 
					then AO_INTR <= '0';
					
			elsif(( INTR_EN = '1' ) and ( HF_VEC = "01" ))
				then AO_INTR <= '0';
				
			elsif(( INTR_EN = '1' ) and ( FF_VEC = "01" ))
				then AO_INTR <= '0';
				
			elsif(( DSP_WR 	= '0' ) 
				and ( DSP_STB1 = "0000" ) 
				and ( DSP_A 	= DSP_BOARD_ID )
				and ( DSP_A1	= iADR_DSP_INTR_DIS )) 
				then AO_INTR <= '1';
			end if;

			if( ( DSP_WR = '0' )
				and ( DSP_STB1 = x"0" )
				and ( DSP_A 	= DSP_BOARD_ID )
				and ( DSP_A1	= iADR_DSP_FIFO_MR )) 
					then FF_MR <= FF_MR( 1 downto 0 ) & '1';
					else FF_MR <= FF_MR( 1 downto 0 ) & '0';
			end if;

			if( FF_MR = 0 )
				then PFF_MR <= '0';
				else PFF_MR <= '1';
			end if;
		end if;
	end process;
     ------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
end behavioral;			-- dspctrl.VHD
-----------------------------------------------------------------------------------------

