del *.pof
del *.ssd
del *.htm
del *.rpt
del *.txt
del *.fsf
del *.ini
del transcript.*
del dfp_param.*
del db\*.* /q
del ls_work\*.* /q
del .work.*
del *.xdb
del simulation\modelsim\work\* /s /q /f
rem del *.sof
rem del *.hexout
