-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PCICTRL.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		November 2001 - MCS
--			Module has been debuggged and implemented in the DPP-II Board
--
-----------------------------------------------------------------------------------------
--
-- TODO take ODXFER out of the "Read Logic"
--
-----------------------------------------------------------------------------------------
library LPM;
     use LPM.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
entity PCICTRL is 
	port( 
     	IMR            : in      std_logic;                  		-- Local Master Reset
		CLK20		: in		std_logic;
	     AO_CLK		: in      std_logic;                  		-- Local System Clock
	     OPTATN         : in      std_logic;                      -- Pass Thru Attention
		PTWR           : in      std_logic;                      -- Pass Thru Write
	     ODXFER         : in      std_logic;                      -- Active Transfer Complete
     	PTNUM          : in      std_logic_vector( 1 downto 0 ); -- Pass Thru Number
	     OPTBE          : in      std_logic_vector( 3 downto 0 ); -- Pass Thru Byte Enable
     	OPTADR         : in      std_logic;                      -- Active Mode Address Valid
	     PCI_DIN        : in      std_logic_vector( 15 downto 0 );-- PCI Interface Data Bus
		PCI_RST_IN	: in		std_logic;
		PCI_RST_OUT	: buffer	std_logic;
		PFF_REN		: buffer  std_logic;				  -- PCI FIFO Read Enable
		PFF_OE		: buffer	std_logic;
	     PCI_RD         : buffer  std_logic;                       -- Local PCI Read COmmand
		DPR_ACC		: buffer	std_logic;				   -- Dual Port Ram Access
		DP_OER		: buffer	std_Logic;
		DP_WRR		: buffer	std_logic;
		PCI_ADR 		: buffer	std_logic_Vector( 8 downto 0 );		
		PCI_DPRA 		: buffer	std_logic_Vector( 8 downto 0 );		
     	WE_DCD 	     : buffer  std_logic_vector( 15 downto 0 );
		PCI_WD		: buffer	std_logic_vector( 15 downto 0 ));
end PCICTRL;	
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
architecture behavioral of PCICTRL is
     -- Constant Declarations
	constant iADR_PFF		: integer := 16#7#;
	constant iADR_PFF_STAT	: integer := 16#D#;

	constant PCI_MR_MAX		: integer := 3000000-1;		-- 150msX2 Ver B
		
     constant PASS_THRU_NUM   : integer := 16#00#; 
	constant WIDTHAD		: integer := 8;
	constant num_dcd		: integer := 15;
	type PCI_Type is (
		ST_MR0,
		ST_MR1,
		ST_MR2 );

	signal PCI_STATE 	: PCI_TYPE;

	-- Any Type Declarations

     -- Combinatorial Signals
	signal PCI_DCD			: std_logic_Vector( 15 downto 0 );
	signal PCI_MR_CNT		: integer range 0 to PCI_MR_MAX;
	signal PCI_MR_CNT_TC	: std_logic;
	signal data_en			: std_logic;
	signal data_en_ao 		: std_logic;
	signal data_en_c20_vec	: std_logic_vector( 1 downto 0 );

-----------------------------------------------------------------------------------------
begin
	PCI_MR_CNT_TC 		<= '1' when ( PCI_MR_CNT = PCI_MR_MAX ) else '0';

	-- Right Side of Dual Port RAM - PCI Bridge Chip ( Can operate greater than 40Mhz )
     DP_OER    <= '1' when (  ( PTNUM 	= PASS_THRU_NUM 	) 
					 and ( OPTATN 	= '0' 			) 
                          and ( PTWR 	= '0' 			) 
                          and ( OPTBE 	= "0000" 			) 
					 and ( OPTADR  = '1' 			)
					 and ( DPR_ACC = '1' 			)) else '0';

	-- PCI Read - Only active when driving the PCI_DQ bus
     PCI_RD    <= '1' when (  ( PTNUM 	= PASS_THRU_NUM 	) 
					 and ( OPTATN 	= '0' 			) 
                          and ( PTWR 	= '0' 			) 
                          and ( OPTBE 	= "0000" 			) 
					 and ( OPTADR  = '1' 			)
					 and ( DPR_ACC = '0' 			)
					 and ( PCI_ADR /= iADR_PFF 		)) else 
                   '1' when (  ( PTNUM 	= PASS_THRU_NUM 	) 
					 and ( OPTATN 	= '0' 			) 
                          and ( PTWR 	= '0' 			) 
                          and ( OPTBE 	= "0000" 			) 
					 and ( OPTADR  = '1' 			)
					 and ( DPR_ACC = '1' 			)) else '0';

	-- PCI FIFO Output Enable
	PFF_OE	<= '1' when ( ( PTNUM  	= PASS_THRU_NUM	)
					and ( OPTATN	= '0' 			)
					and ( PTWR	= '0' 			) 
					and ( OPTBE	= "0000"			)
					and ( OPTADR   = '1' 			)
					and ( DPR_ACC	= '0'			)
					and ( PCI_ADR	= iADR_PFF		)) else '0';

	DP_WRR    <= '1' when ( ( PTNUM 	= PASS_THRU_NUM 	)
					and ( PTWR 	= '1' 			)
					and ( OPTBE 	= "0000" 			)
					and ( ODXFER 	= '0' 			)
					and ( DPR_ACC 	= '1'			)) else '0';

--	WE_DCD_GEN : for i in 0 to 15 generate
--		WE_DCD(i) <= '1' when ( ( PCI_DCD(i) 	= '1' ) 
--						and ( PTNUM 		= PASS_THRU_NUM 	)
--						and ( PTWR 		= '1' 			)
--						and ( OPTBE 		= "0000" 			)
--						and ( ODXFER 		= '0' 			)
--						and ( DPR_ACC 		= '0' 			)) else '0';
--	end generate;


	PFF_REN <= '1' when (( OPTATN 	= '0' 				)	 
	   	               and ( PTNUM 	= PASS_THRU_NUM 		) 
					and ( PTWR	= '0' 				)
					and ( OPTBE 	= "0000" 				)
					and ( OPTADR	= '0' 				)
					and ( DPR_ACC	= '0' 				)
					and ( PCI_DIN( 10 downto 2 ) = iADR_PFF	)) else '0';


	-- ADD-ON BUS ACTIVE DATA CYCLE -------------------------------------------------
	Data_En <= '1' when (   ( OPTATN				= '1'			)
				     and ( PTNum    			= PASS_THRU_NUM 	)
					and ( PTWR				= '1' 		 	)
					and ( OPTBE				= "0000"		 	)
					and ( OPTADR				= '1' 			)
					and ( ODXFER				= '0' 			)) else '0';
	
	Data_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
		LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr			=> IMR,
			clock		=> AO_CLK,
			enable		=> Data_EN,
			data			=> PCI_Din,
			q			=> PCI_WD );
					
     ------------------------------------------------------------------------------------
     PCI_PROC : process( AO_CLK, IMR )

     begin
          if( IMR = '1' ) then
               -- Unused portion of the ADD-ON Bus of the AMCC S5920
               -- Allows for ADD-ON Register Access
			DPR_ACC 		<= '0';
               PCI_DCD   	<= x"0000";
			PCI_ADR		<= '0' & x"00";
			PCI_DPRA		<= '0' & x"00";
			Data_En_Ao 	<= '0';
			
          elsif(( AO_CLK'Event ) and ( AO_CLK = '1' )) then

			if( Data_En = '1' )
				then Data_En_Ao <= '1';
			elsif( Data_En_C20_Vec(0) = '1' )
				then DAta_En_Ao <= '0';
			end if;

               -- ADD-ON BUS ACTIVE ADDRESS CYCLE -------------------------------------------------
               -- Active Mode Pass Through Address Phase
               -- When PTATN#, PTADR# is active
               -- PTNUM matches the proper PTNUM
               -- And all 4 byte enables, PTBE are active
               -- The address is latched on the rising edge of the AO_CLK 

			-- PCI to DSP Interrupt ------------------------------------------------------------
			-- Address Phase
			if(	( OPTATN 		= '0' 			) 
				and ( OPTADR 	= '0' 			)  
                    and ( PTNUM 	= PASS_THRU_NUM 	) 
				and ( OPTBE 	= "0000" 			)) then
					DPR_ACC	<= PCI_DIN(15);
					
					if( PCI_DIN(15) = '0' )
						then PCI_ADR 	<= PCI_DIN( 10 downto 2 );
						else PCI_DPRA	<= PCI_DIN( 10 downto 2 );
					end if;

					for i in 0 to 15 loop
						if( PCI_DIN( 15 downto 2 ) = i ) 
     	                         then PCI_DCD(i) <= '1';
          	                    else PCI_DCD(i) <= '0';
               	          end if;
               	   	end loop;
               end if;
               -- END OF ACTIVE MODE ADDRESS DECODING------------------------------------
          end if;                       -- Rising Edge AO_CLK
     end process;
     ------------------------------------------------------------------------------------
	clk20_proc : process( clk20, imr ) 
	begin
		if( imr = '1' ) then
			data_en_c20_vec	<= "00";
			for i in 0 to num_dcd loop
				we_dcd(i) <= '0';
			end loop;
		elsif(( clk20'event ) and ( clk20 = '1' )) then
			data_en_C20_Vec <= data_en_C20_Vec(0) & data_en_ao;
						
			for i in 0 to Num_Dcd loop
				if(( data_en_C20_Vec = "01" ) and ( DPR_ACC = '0' ) and ( Conv_Integer( PCI_ADR ) = i ) )
					then WE_DCD(i) <= '1';
					else WE_DCD(i) <= '0';
				end if;
			end loop;
		end if;
	end process;
     ------------------------------------------------------------------------------------
		
     ------------------------------------------------------------------------------------
	PCI_MR_PROC : process( CLK20, PCI_RST_IN )
	begin
		if( PCI_RST_IN = '0' ) then 
			PCI_MR_CNT 	<= 0;
			PCI_STATE		<= ST_MR0;
			PCI_RST_OUT	<= '0';
		
		elsif(( CLK20'event ) and ( clk20 = '1' )) then


			if( PCI_MR_CNT_TC = '1' ) 
				then PCI_MR_CNT 	<= 0;
				else PCI_MR_CNT 	<= PCI_MR_CNT + 1;
			end if;
			
			case PCI_STATE is
				when ST_MR0 => 
					if( PCI_MR_CNT_TC = '1' )
						then PCI_STATE <= ST_MR1;
					end if;

				when ST_MR1 => 
					if( PCI_MR_CNT_TC = '1' )
						then PCI_STATE <= ST_MR2;
					end if;

				when ST_MR2 => 
					if( PCI_MR_CNT_TC = '1' )
						then PCI_STATE <= ST_MR2;
					end if;
					
				when others	=> PCI_STATE <= ST_MR0;
			end case;
			
			if( PCI_STATE = ST_MR2 )
				then PCI_RST_OUT <= '1';
				else PCI_RST_OUT <= '0';
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
end behavioral;			-- PCICTRL.VHD
-----------------------------------------------------------------------------------------

