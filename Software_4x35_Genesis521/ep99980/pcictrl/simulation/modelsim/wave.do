onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/ao_clk
add wave -noupdate -format Logic -radix binary /tb/optatn
add wave -noupdate -format Logic -radix binary /tb/ptwr
add wave -noupdate -format Logic -radix binary /tb/odxfer
add wave -noupdate -format Literal -radix binary /tb/ptnum
add wave -noupdate -format Literal -radix binary /tb/optbe
add wave -noupdate -format Logic -radix binary /tb/optadr
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_din
add wave -noupdate -format Logic -radix binary /tb/pci_rst_in
add wave -noupdate -format Logic -radix binary /tb/pci_rst_out
add wave -noupdate -format Logic -radix binary /tb/pff_ren
add wave -noupdate -format Logic -radix binary /tb/pff_oe
add wave -noupdate -format Logic -radix binary /tb/pci_rd
add wave -noupdate -format Logic -radix binary /tb/dpr_acc
add wave -noupdate -format Logic -radix binary /tb/dp_oer
add wave -noupdate -format Logic -radix binary /tb/dp_wrr
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_adr
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_dpra
add wave -noupdate -format Literal -radix hexadecimal /tb/we_dcd
add wave -noupdate -format Literal -radix hexadecimal /tb/pci_wd
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {50383 ns}
WaveRestoreZoom {1888 ns} {2429 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
