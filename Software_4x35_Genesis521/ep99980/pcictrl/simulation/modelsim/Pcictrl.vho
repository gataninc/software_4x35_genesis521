-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "01/04/2006 11:12:56"

-- 
-- Device: Altera EP20K30ETC144-2X Package TQFP144
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	PCICTRL IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	AO_CLK : IN std_logic;
	OPTATN : IN std_logic;
	PTWR : IN std_logic;
	ODXFER : IN std_logic;
	PTNUM : IN std_logic_vector(1 DOWNTO 0);
	OPTBE : IN std_logic_vector(3 DOWNTO 0);
	OPTADR : IN std_logic;
	PCI_DIN : IN std_logic_vector(15 DOWNTO 0);
	PCI_RST_IN : IN std_logic;
	PCI_RST_OUT : OUT std_logic;
	PFF_REN : OUT std_logic;
	PFF_OE : OUT std_logic;
	PCI_RD : OUT std_logic;
	DPR_ACC : OUT std_logic;
	DP_OER : OUT std_logic;
	DP_WRR : OUT std_logic;
	PCI_ADR : OUT std_logic_vector(8 DOWNTO 0);
	PCI_DPRA : OUT std_logic_vector(8 DOWNTO 0);
	WE_DCD : OUT std_logic_vector(15 DOWNTO 0);
	PCI_WD : OUT std_logic_vector(15 DOWNTO 0)
	);
END PCICTRL;

ARCHITECTURE structure OF PCICTRL IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_AO_CLK : std_logic;
SIGNAL ww_OPTATN : std_logic;
SIGNAL ww_PTWR : std_logic;
SIGNAL ww_ODXFER : std_logic;
SIGNAL ww_PTNUM : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_OPTBE : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_OPTADR : std_logic;
SIGNAL ww_PCI_DIN : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PCI_RST_IN : std_logic;
SIGNAL ww_PCI_RST_OUT : std_logic;
SIGNAL ww_PFF_REN : std_logic;
SIGNAL ww_PFF_OE : std_logic;
SIGNAL ww_PCI_RD : std_logic;
SIGNAL ww_DPR_ACC : std_logic;
SIGNAL ww_DP_OER : std_logic;
SIGNAL ww_DP_WRR : std_logic;
SIGNAL ww_PCI_ADR : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_PCI_DPRA : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_WE_DCD : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PCI_WD : std_logic_vector(15 DOWNTO 0);
SIGNAL rtl_a3 : std_logic;
SIGNAL data_en_a79 : std_logic;
SIGNAL PFF_REN_a111 : std_logic;
SIGNAL PFF_REN_a114 : std_logic;
SIGNAL PCI_MR_CNT_a19_a : std_logic;
SIGNAL rtl_a268 : std_logic;
SIGNAL PCI_MR_CNT_a16_a : std_logic;
SIGNAL PCI_MR_CNT_a10_a : std_logic;
SIGNAL PCI_MR_CNT_a11_a : std_logic;
SIGNAL PCI_MR_CNT_a12_a : std_logic;
SIGNAL rtl_a281 : std_logic;
SIGNAL rtl_a270 : std_logic;
SIGNAL PCI_MR_CNT_a7_a : std_logic;
SIGNAL PCI_MR_CNT_a9_a : std_logic;
SIGNAL PCI_MR_CNT_a6_a : std_logic;
SIGNAL rtl_a275 : std_logic;
SIGNAL rtl_a272 : std_logic;
SIGNAL add_a313 : std_logic;
SIGNAL add_a333 : std_logic;
SIGNAL add_a341 : std_logic;
SIGNAL add_a345 : std_logic;
SIGNAL add_a349 : std_logic;
SIGNAL add_a357 : std_logic;
SIGNAL add_a361 : std_logic;
SIGNAL add_a365 : std_logic;
SIGNAL OPTBE_a0_a_acombout : std_logic;
SIGNAL add_a389 : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL PCI_RST_IN_acombout : std_logic;
SIGNAL PCI_MR_CNT_a0_a : std_logic;
SIGNAL add_a391 : std_logic;
SIGNAL add_a393 : std_logic;
SIGNAL PCI_MR_CNT_a1_a : std_logic;
SIGNAL add_a395 : std_logic;
SIGNAL add_a373 : std_logic;
SIGNAL PCI_MR_CNT_a2_a : std_logic;
SIGNAL add_a375 : std_logic;
SIGNAL add_a377 : std_logic;
SIGNAL PCI_MR_CNT_a3_a : std_logic;
SIGNAL add_a379 : std_logic;
SIGNAL add_a381 : std_logic;
SIGNAL PCI_MR_CNT_a4_a : std_logic;
SIGNAL add_a383 : std_logic;
SIGNAL add_a385 : std_logic;
SIGNAL PCI_MR_CNT_a5_a : std_logic;
SIGNAL rtl_a282 : std_logic;
SIGNAL rtl_a276 : std_logic;
SIGNAL add_a387 : std_logic;
SIGNAL add_a367 : std_logic;
SIGNAL add_a359 : std_logic;
SIGNAL add_a369 : std_logic;
SIGNAL PCI_MR_CNT_a8_a : std_logic;
SIGNAL add_a371 : std_logic;
SIGNAL add_a363 : std_logic;
SIGNAL add_a343 : std_logic;
SIGNAL add_a347 : std_logic;
SIGNAL add_a351 : std_logic;
SIGNAL add_a353 : std_logic;
SIGNAL PCI_MR_CNT_a13_a : std_logic;
SIGNAL add_a355 : std_logic;
SIGNAL add_a325 : std_logic;
SIGNAL PCI_MR_CNT_a14_a : std_logic;
SIGNAL add_a327 : std_logic;
SIGNAL add_a329 : std_logic;
SIGNAL PCI_MR_CNT_a15_a : std_logic;
SIGNAL add_a331 : std_logic;
SIGNAL add_a335 : std_logic;
SIGNAL add_a337 : std_logic;
SIGNAL PCI_MR_CNT_a17_a : std_logic;
SIGNAL add_a339 : std_logic;
SIGNAL add_a311 : std_logic;
SIGNAL add_a315 : std_logic;
SIGNAL add_a321 : std_logic;
SIGNAL PCI_MR_CNT_a20_a : std_logic;
SIGNAL add_a323 : std_logic;
SIGNAL add_a317 : std_logic;
SIGNAL PCI_MR_CNT_a21_a : std_logic;
SIGNAL add_a309 : std_logic;
SIGNAL PCI_MR_CNT_a18_a : std_logic;
SIGNAL rtl_a280 : std_logic;
SIGNAL rtl_a274 : std_logic;
SIGNAL PCI_STATE_ast_mr0 : std_logic;
SIGNAL PCI_STATE_ast_mr2 : std_logic;
SIGNAL PCI_RST_OUT_areg0 : std_logic;
SIGNAL OPTATN_acombout : std_logic;
SIGNAL OPTADR_acombout : std_logic;
SIGNAL PCI_DIN_a6_a_acombout : std_logic;
SIGNAL PCI_DIN_a9_a_acombout : std_logic;
SIGNAL PCI_DIN_a4_a_acombout : std_logic;
SIGNAL PCI_DIN_a5_a_acombout : std_logic;
SIGNAL PCI_DIN_a2_a_acombout : std_logic;
SIGNAL PCI_DIN_a10_a_acombout : std_logic;
SIGNAL PTWR_acombout : std_logic;
SIGNAL PCI_DIN_a8_a_acombout : std_logic;
SIGNAL PCI_DIN_a7_a_acombout : std_logic;
SIGNAL PFF_REN_a117 : std_logic;
SIGNAL PFF_REN_a118 : std_logic;
SIGNAL PFF_REN_a113 : std_logic;
SIGNAL OPTBE_a2_a_acombout : std_logic;
SIGNAL OPTBE_a1_a_acombout : std_logic;
SIGNAL PTNUM_a0_a_acombout : std_logic;
SIGNAL OPTBE_a3_a_acombout : std_logic;
SIGNAL PTNUM_a1_a_acombout : std_logic;
SIGNAL data_en_a89 : std_logic;
SIGNAL data_en_a85 : std_logic;
SIGNAL PFF_REN_a103 : std_logic;
SIGNAL PCI_PROC_a19 : std_logic;
SIGNAL AO_CLK_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL PCI_DIN_a15_a_acombout : std_logic;
SIGNAL PCI_ADR_a8_a_a9 : std_logic;
SIGNAL PCI_ADR_a4_a_areg0 : std_logic;
SIGNAL PCI_ADR_a6_a_areg0 : std_logic;
SIGNAL PCI_ADR_a8_a_areg0 : std_logic;
SIGNAL PCI_ADR_a7_a_areg0 : std_logic;
SIGNAL rtl_a253 : std_logic;
SIGNAL PCI_DIN_a3_a_acombout : std_logic;
SIGNAL PCI_ADR_a1_a_areg0 : std_logic;
SIGNAL PCI_ADR_a2_a_areg0 : std_logic;
SIGNAL PCI_ADR_a0_a_areg0 : std_logic;
SIGNAL rtl_a254 : std_logic;
SIGNAL PFF_OE_a32 : std_logic;
SIGNAL PFF_OE_a2 : std_logic;
SIGNAL PCI_RD_a72 : std_logic;
SIGNAL DPR_ACC_areg0 : std_logic;
SIGNAL PFF_OE_a33 : std_logic;
SIGNAL PCI_RD_a0 : std_logic;
SIGNAL ODXFER_acombout : std_logic;
SIGNAL DP_WRR_a16 : std_logic;
SIGNAL PCI_ADR_a3_a_areg0 : std_logic;
SIGNAL PCI_ADR_a5_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a0_a_a17 : std_logic;
SIGNAL PCI_DPRA_a0_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a1_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a2_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a3_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a4_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a5_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a6_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a7_a_areg0 : std_logic;
SIGNAL PCI_DPRA_a8_a_areg0 : std_logic;
SIGNAL clk20_proc_a229 : std_logic;
SIGNAL clk20_proc_a230 : std_logic;
SIGNAL WE_DCD_a0_a_areg0 : std_logic;
SIGNAL rtl_a255 : std_logic;
SIGNAL clk20_proc_a231 : std_logic;
SIGNAL WE_DCD_a1_a_areg0 : std_logic;
SIGNAL WE_DCD_a2_a_areg0 : std_logic;
SIGNAL WE_DCD_a3_a_areg0 : std_logic;
SIGNAL WE_DCD_a4_a_areg0 : std_logic;
SIGNAL WE_DCD_a5_a_areg0 : std_logic;
SIGNAL WE_DCD_a6_a_areg0 : std_logic;
SIGNAL data_en_ao_a10 : std_logic;
SIGNAL data_en_a88 : std_logic;
SIGNAL data_en_a86 : std_logic;
SIGNAL data_en_ao : std_logic;
SIGNAL data_en_c20_vec_a0_a : std_logic;
SIGNAL data_en_c20_vec_a1_a : std_logic;
SIGNAL clk20_proc_a228 : std_logic;
SIGNAL WE_DCD_a7_a_areg0 : std_logic;
SIGNAL clk20_proc_a232 : std_logic;
SIGNAL WE_DCD_a8_a_areg0 : std_logic;
SIGNAL WE_DCD_a9_a_areg0 : std_logic;
SIGNAL WE_DCD_a10_a_areg0 : std_logic;
SIGNAL WE_DCD_a11_a_areg0 : std_logic;
SIGNAL WE_DCD_a12_a_areg0 : std_logic;
SIGNAL WE_DCD_a13_a_areg0 : std_logic;
SIGNAL WE_DCD_a14_a_areg0 : std_logic;
SIGNAL WE_DCD_a15_a_areg0 : std_logic;
SIGNAL PCI_DIN_a0_a_acombout : std_logic;
SIGNAL data_en_a78 : std_logic;
SIGNAL Data_FF_adffs_a0_a : std_logic;
SIGNAL PCI_DIN_a1_a_acombout : std_logic;
SIGNAL Data_FF_adffs_a1_a : std_logic;
SIGNAL Data_FF_adffs_a2_a : std_logic;
SIGNAL Data_FF_adffs_a3_a : std_logic;
SIGNAL Data_FF_adffs_a4_a : std_logic;
SIGNAL Data_FF_adffs_a5_a : std_logic;
SIGNAL Data_FF_adffs_a6_a : std_logic;
SIGNAL Data_FF_adffs_a7_a : std_logic;
SIGNAL Data_FF_adffs_a8_a : std_logic;
SIGNAL Data_FF_adffs_a9_a : std_logic;
SIGNAL Data_FF_adffs_a10_a : std_logic;
SIGNAL PCI_DIN_a11_a_acombout : std_logic;
SIGNAL Data_FF_adffs_a11_a : std_logic;
SIGNAL PCI_DIN_a12_a_acombout : std_logic;
SIGNAL Data_FF_adffs_a12_a : std_logic;
SIGNAL PCI_DIN_a13_a_acombout : std_logic;
SIGNAL Data_FF_adffs_a13_a : std_logic;
SIGNAL PCI_DIN_a14_a_acombout : std_logic;
SIGNAL Data_FF_adffs_a14_a : std_logic;
SIGNAL Data_FF_adffs_a15_a : std_logic;
SIGNAL ALT_INV_PCI_RST_IN_acombout : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_AO_CLK <= AO_CLK;
ww_OPTATN <= OPTATN;
ww_PTWR <= PTWR;
ww_ODXFER <= ODXFER;
ww_PTNUM <= PTNUM;
ww_OPTBE <= OPTBE;
ww_OPTADR <= OPTADR;
ww_PCI_DIN <= PCI_DIN;
ww_PCI_RST_IN <= PCI_RST_IN;
PCI_RST_OUT <= ww_PCI_RST_OUT;
PFF_REN <= ww_PFF_REN;
PFF_OE <= ww_PFF_OE;
PCI_RD <= ww_PCI_RD;
DPR_ACC <= ww_DPR_ACC;
DP_OER <= ww_DP_OER;
DP_WRR <= ww_DP_WRR;
PCI_ADR <= ww_PCI_ADR;
PCI_DPRA <= ww_PCI_DPRA;
WE_DCD <= ww_WE_DCD;
PCI_WD <= ww_PCI_WD;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_PCI_RST_IN_acombout <= NOT PCI_RST_IN_acombout;

rtl_a3_I : apex20ke_lcell
-- Equation(s):
-- rtl_a3 = rtl_a275 & rtl_a274 & rtl_a276

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a3);

OPTBE_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(0),
	combout => OPTBE_a0_a_acombout);

PCI_MR_CNT_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a19_a = DFFE(add_a313 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a313,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a19_a);

PCI_MR_CNT_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a16_a = DFFE(add_a333 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a333,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a16_a);

PCI_MR_CNT_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a10_a = DFFE(add_a341 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a341,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a10_a);

PCI_MR_CNT_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a11_a = DFFE(add_a345, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a345,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a11_a);

PCI_MR_CNT_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a12_a = DFFE(add_a349, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a349,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a12_a);

rtl_a270_I : apex20ke_lcell
-- Equation(s):
-- rtl_a281 = PCI_MR_CNT_a10_a & !PCI_MR_CNT_a11_a & !PCI_MR_CNT_a13_a & !PCI_MR_CNT_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a10_a,
	datab => PCI_MR_CNT_a11_a,
	datac => PCI_MR_CNT_a13_a,
	datad => PCI_MR_CNT_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a270,
	cascout => rtl_a281);

PCI_MR_CNT_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a7_a = DFFE(add_a357 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a357,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a7_a);

PCI_MR_CNT_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a9_a = DFFE(add_a361 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a361,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a9_a);

PCI_MR_CNT_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a6_a = DFFE(add_a365 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a365,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a6_a);

rtl_a275_I : apex20ke_lcell
-- Equation(s):
-- rtl_a275 = (PCI_MR_CNT_a9_a & !PCI_MR_CNT_a6_a & !PCI_MR_CNT_a8_a & PCI_MR_CNT_a7_a) & CASCADE(rtl_a281)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a9_a,
	datab => PCI_MR_CNT_a6_a,
	datac => PCI_MR_CNT_a8_a,
	datad => PCI_MR_CNT_a7_a,
	cascin => rtl_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a275);

add_a313_I : apex20ke_lcell
-- Equation(s):
-- add_a313 = PCI_MR_CNT_a19_a $ (add_a311)
-- add_a315 = CARRY(!add_a311 # !PCI_MR_CNT_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a19_a,
	cin => add_a311,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a313,
	cout => add_a315);

add_a333_I : apex20ke_lcell
-- Equation(s):
-- add_a333 = PCI_MR_CNT_a16_a $ (!add_a331)
-- add_a335 = CARRY(PCI_MR_CNT_a16_a & (!add_a331))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a16_a,
	cin => add_a331,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a333,
	cout => add_a335);

add_a341_I : apex20ke_lcell
-- Equation(s):
-- add_a341 = PCI_MR_CNT_a10_a $ (!add_a363)
-- add_a343 = CARRY(PCI_MR_CNT_a10_a & (!add_a363))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a10_a,
	cin => add_a363,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a341,
	cout => add_a343);

add_a345_I : apex20ke_lcell
-- Equation(s):
-- add_a345 = PCI_MR_CNT_a11_a $ (add_a343)
-- add_a347 = CARRY(!add_a343 # !PCI_MR_CNT_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a11_a,
	cin => add_a343,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a345,
	cout => add_a347);

add_a349_I : apex20ke_lcell
-- Equation(s):
-- add_a349 = PCI_MR_CNT_a12_a $ (!add_a347)
-- add_a351 = CARRY(PCI_MR_CNT_a12_a & (!add_a347))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a12_a,
	cin => add_a347,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a349,
	cout => add_a351);

add_a357_I : apex20ke_lcell
-- Equation(s):
-- add_a357 = PCI_MR_CNT_a7_a $ (add_a367)
-- add_a359 = CARRY(!add_a367 # !PCI_MR_CNT_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a7_a,
	cin => add_a367,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a357,
	cout => add_a359);

add_a361_I : apex20ke_lcell
-- Equation(s):
-- add_a361 = PCI_MR_CNT_a9_a $ (add_a371)
-- add_a363 = CARRY(!add_a371 # !PCI_MR_CNT_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a9_a,
	cin => add_a371,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a361,
	cout => add_a363);

add_a365_I : apex20ke_lcell
-- Equation(s):
-- add_a365 = PCI_MR_CNT_a6_a $ (!add_a387)
-- add_a367 = CARRY(PCI_MR_CNT_a6_a & (!add_a387))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a6_a,
	cin => add_a387,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a365,
	cout => add_a367);

add_a389_I : apex20ke_lcell
-- Equation(s):
-- add_a389 = !PCI_MR_CNT_a0_a
-- add_a391 = CARRY(PCI_MR_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a389,
	cout => add_a391);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

PCI_RST_IN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_RST_IN,
	combout => PCI_RST_IN_acombout);

PCI_MR_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a0_a = DFFE(add_a389, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a389,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a0_a);

add_a393_I : apex20ke_lcell
-- Equation(s):
-- add_a393 = PCI_MR_CNT_a1_a $ add_a391
-- add_a395 = CARRY(!add_a391 # !PCI_MR_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a1_a,
	cin => add_a391,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a393,
	cout => add_a395);

PCI_MR_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a1_a = DFFE(add_a393, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a393,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a1_a);

add_a373_I : apex20ke_lcell
-- Equation(s):
-- add_a373 = PCI_MR_CNT_a2_a $ !add_a395
-- add_a375 = CARRY(PCI_MR_CNT_a2_a & !add_a395)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a2_a,
	cin => add_a395,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a373,
	cout => add_a375);

PCI_MR_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a2_a = DFFE(add_a373, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a373,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a2_a);

add_a377_I : apex20ke_lcell
-- Equation(s):
-- add_a377 = PCI_MR_CNT_a3_a $ add_a375
-- add_a379 = CARRY(!add_a375 # !PCI_MR_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a3_a,
	cin => add_a375,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a377,
	cout => add_a379);

PCI_MR_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a3_a = DFFE(add_a377, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a377,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a3_a);

add_a381_I : apex20ke_lcell
-- Equation(s):
-- add_a381 = PCI_MR_CNT_a4_a $ !add_a379
-- add_a383 = CARRY(PCI_MR_CNT_a4_a & !add_a379)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a4_a,
	cin => add_a379,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a381,
	cout => add_a383);

PCI_MR_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a4_a = DFFE(add_a381, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a381,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a4_a);

add_a385_I : apex20ke_lcell
-- Equation(s):
-- add_a385 = PCI_MR_CNT_a5_a $ add_a383
-- add_a387 = CARRY(!add_a383 # !PCI_MR_CNT_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a5_a,
	cin => add_a383,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a385,
	cout => add_a387);

PCI_MR_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a5_a = DFFE(add_a385, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a385,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a5_a);

rtl_a272_I : apex20ke_lcell
-- Equation(s):
-- rtl_a282 = PCI_MR_CNT_a3_a & PCI_MR_CNT_a5_a & PCI_MR_CNT_a2_a & PCI_MR_CNT_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a3_a,
	datab => PCI_MR_CNT_a5_a,
	datac => PCI_MR_CNT_a2_a,
	datad => PCI_MR_CNT_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a272,
	cascout => rtl_a282);

rtl_a276_I : apex20ke_lcell
-- Equation(s):
-- rtl_a276 = (PCI_MR_CNT_a0_a & (PCI_MR_CNT_a1_a)) & CASCADE(rtl_a282)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a0_a,
	datad => PCI_MR_CNT_a1_a,
	cascin => rtl_a282,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a276);

add_a369_I : apex20ke_lcell
-- Equation(s):
-- add_a369 = PCI_MR_CNT_a8_a $ !add_a359
-- add_a371 = CARRY(PCI_MR_CNT_a8_a & !add_a359)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a8_a,
	cin => add_a359,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a369,
	cout => add_a371);

PCI_MR_CNT_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a8_a = DFFE(add_a369, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a369,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a8_a);

add_a353_I : apex20ke_lcell
-- Equation(s):
-- add_a353 = PCI_MR_CNT_a13_a $ add_a351
-- add_a355 = CARRY(!add_a351 # !PCI_MR_CNT_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a13_a,
	cin => add_a351,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a353,
	cout => add_a355);

PCI_MR_CNT_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a13_a = DFFE(add_a353, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a353,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a13_a);

add_a325_I : apex20ke_lcell
-- Equation(s):
-- add_a325 = PCI_MR_CNT_a14_a $ !add_a355
-- add_a327 = CARRY(PCI_MR_CNT_a14_a & !add_a355)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a14_a,
	cin => add_a355,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a325,
	cout => add_a327);

PCI_MR_CNT_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a14_a = DFFE(add_a325 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "70F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => add_a325,
	datad => rtl_a276,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a14_a);

add_a329_I : apex20ke_lcell
-- Equation(s):
-- add_a329 = PCI_MR_CNT_a15_a $ (add_a327)
-- add_a331 = CARRY(!add_a327 # !PCI_MR_CNT_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a15_a,
	cin => add_a327,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a329,
	cout => add_a331);

PCI_MR_CNT_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a15_a = DFFE(add_a329 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a329,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a15_a);

add_a337_I : apex20ke_lcell
-- Equation(s):
-- add_a337 = PCI_MR_CNT_a17_a $ add_a335
-- add_a339 = CARRY(!add_a335 # !PCI_MR_CNT_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a17_a,
	cin => add_a335,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a337,
	cout => add_a339);

PCI_MR_CNT_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a17_a = DFFE(add_a337, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a337,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a17_a);

add_a309_I : apex20ke_lcell
-- Equation(s):
-- add_a309 = PCI_MR_CNT_a18_a $ (!add_a339)
-- add_a311 = CARRY(PCI_MR_CNT_a18_a & (!add_a339))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a18_a,
	cin => add_a339,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a309,
	cout => add_a311);

add_a321_I : apex20ke_lcell
-- Equation(s):
-- add_a321 = PCI_MR_CNT_a20_a $ !add_a315
-- add_a323 = CARRY(PCI_MR_CNT_a20_a & !add_a315)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PCI_MR_CNT_a20_a,
	cin => add_a315,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a321,
	cout => add_a323);

PCI_MR_CNT_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a20_a = DFFE(add_a321, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a321,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a20_a);

add_a317_I : apex20ke_lcell
-- Equation(s):
-- add_a317 = add_a323 $ PCI_MR_CNT_a21_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_MR_CNT_a21_a,
	cin => add_a323,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a317);

PCI_MR_CNT_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a21_a = DFFE(add_a317 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a317,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a21_a);

PCI_MR_CNT_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- PCI_MR_CNT_a18_a = DFFE(add_a309 & (!rtl_a276 # !rtl_a274 # !rtl_a275), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => add_a309,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_MR_CNT_a18_a);

rtl_a268_I : apex20ke_lcell
-- Equation(s):
-- rtl_a280 = PCI_MR_CNT_a19_a & PCI_MR_CNT_a21_a & PCI_MR_CNT_a18_a & !PCI_MR_CNT_a20_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a19_a,
	datab => PCI_MR_CNT_a21_a,
	datac => PCI_MR_CNT_a18_a,
	datad => PCI_MR_CNT_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a268,
	cascout => rtl_a280);

rtl_a274_I : apex20ke_lcell
-- Equation(s):
-- rtl_a274 = (PCI_MR_CNT_a16_a & PCI_MR_CNT_a15_a & PCI_MR_CNT_a14_a & !PCI_MR_CNT_a17_a) & CASCADE(rtl_a280)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_MR_CNT_a16_a,
	datab => PCI_MR_CNT_a15_a,
	datac => PCI_MR_CNT_a14_a,
	datad => PCI_MR_CNT_a17_a,
	cascin => rtl_a280,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a274);

PCI_STATE_ast_mr0_aI : apex20ke_lcell
-- Equation(s):
-- PCI_STATE_ast_mr0 = DFFE(PCI_STATE_ast_mr0 # rtl_a275 & rtl_a274 & rtl_a276, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF80",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a275,
	datab => rtl_a274,
	datac => rtl_a276,
	datad => PCI_STATE_ast_mr0,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_STATE_ast_mr0);

PCI_STATE_ast_mr2_aI : apex20ke_lcell
-- Equation(s):
-- PCI_STATE_ast_mr2 = DFFE(PCI_STATE_ast_mr0 & (rtl_a3 # PCI_STATE_ast_mr2), GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FA00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a3,
	datac => PCI_STATE_ast_mr2,
	datad => PCI_STATE_ast_mr0,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_STATE_ast_mr2);

PCI_RST_OUT_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_RST_OUT_areg0 = DFFE(PCI_STATE_ast_mr2, GLOBAL(CLK20_acombout), GLOBAL(PCI_RST_IN_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_STATE_ast_mr2,
	clk => CLK20_acombout,
	aclr => ALT_INV_PCI_RST_IN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_RST_OUT_areg0);

OPTATN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTATN,
	combout => OPTATN_acombout);

OPTADR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTADR,
	combout => OPTADR_acombout);

PCI_DIN_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(6),
	combout => PCI_DIN_a6_a_acombout);

PCI_DIN_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(9),
	combout => PCI_DIN_a9_a_acombout);

PCI_DIN_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(4),
	combout => PCI_DIN_a4_a_acombout);

PCI_DIN_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(5),
	combout => PCI_DIN_a5_a_acombout);

PCI_DIN_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(2),
	combout => PCI_DIN_a2_a_acombout);

PCI_DIN_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(10),
	combout => PCI_DIN_a10_a_acombout);

PTWR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTWR,
	combout => PTWR_acombout);

PCI_DIN_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(8),
	combout => PCI_DIN_a8_a_acombout);

PCI_DIN_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(7),
	combout => PCI_DIN_a7_a_acombout);

PFF_REN_a111_I : apex20ke_lcell
-- Equation(s):
-- PFF_REN_a117 = PCI_DIN_a3_a_acombout & !PTWR_acombout & !PCI_DIN_a8_a_acombout & !PCI_DIN_a7_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PCI_DIN_a3_a_acombout,
	datab => PTWR_acombout,
	datac => PCI_DIN_a8_a_acombout,
	datad => PCI_DIN_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_REN_a111,
	cascout => PFF_REN_a117);

PFF_REN_a114_I : apex20ke_lcell
-- Equation(s):
-- PFF_REN_a118 = (!PCI_DIN_a5_a_acombout & PCI_DIN_a2_a_acombout & !PCI_DIN_a10_a_acombout) & CASCADE(PFF_REN_a117)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => PCI_DIN_a5_a_acombout,
	datac => PCI_DIN_a2_a_acombout,
	datad => PCI_DIN_a10_a_acombout,
	cascin => PFF_REN_a117,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_REN_a114,
	cascout => PFF_REN_a118);

PFF_REN_a113_I : apex20ke_lcell
-- Equation(s):
-- PFF_REN_a113 = (!DPR_ACC_areg0 & !PCI_DIN_a6_a_acombout & !PCI_DIN_a9_a_acombout & PCI_DIN_a4_a_acombout) & CASCADE(PFF_REN_a118)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DPR_ACC_areg0,
	datab => PCI_DIN_a6_a_acombout,
	datac => PCI_DIN_a9_a_acombout,
	datad => PCI_DIN_a4_a_acombout,
	cascin => PFF_REN_a118,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_REN_a113);

OPTBE_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(2),
	combout => OPTBE_a2_a_acombout);

OPTBE_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(1),
	combout => OPTBE_a1_a_acombout);

PTNUM_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTNUM(0),
	combout => PTNUM_a0_a_acombout);

OPTBE_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OPTBE(3),
	combout => OPTBE_a3_a_acombout);

PTNUM_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PTNUM(1),
	combout => PTNUM_a1_a_acombout);

data_en_a79_I : apex20ke_lcell
-- Equation(s):
-- data_en_a89 = !OPTBE_a0_a_acombout & !PTNUM_a0_a_acombout & !OPTBE_a3_a_acombout & !PTNUM_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => OPTBE_a0_a_acombout,
	datab => PTNUM_a0_a_acombout,
	datac => OPTBE_a3_a_acombout,
	datad => PTNUM_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_en_a79,
	cascout => data_en_a89);

data_en_a85_I : apex20ke_lcell
-- Equation(s):
-- data_en_a85 = (!OPTBE_a2_a_acombout & !OPTBE_a1_a_acombout) & CASCADE(data_en_a89)
-- data_en_a88 = (!OPTBE_a2_a_acombout & !OPTBE_a1_a_acombout) & CASCADE(data_en_a89)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OPTBE_a2_a_acombout,
	datad => OPTBE_a1_a_acombout,
	cascin => data_en_a89,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_en_a85,
	cascout => data_en_a88);

PFF_REN_a103_I : apex20ke_lcell
-- Equation(s):
-- PFF_REN_a103 = !OPTATN_acombout & !OPTADR_acombout & PFF_REN_a113 & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datab => OPTADR_acombout,
	datac => PFF_REN_a113,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_REN_a103);

PCI_PROC_a19_I : apex20ke_lcell
-- Equation(s):
-- PCI_PROC_a19 = !OPTATN_acombout & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OPTATN_acombout,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_PROC_a19);

AO_CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AO_CLK,
	combout => AO_CLK_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

PCI_DIN_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(15),
	combout => PCI_DIN_a15_a_acombout);

PCI_ADR_a8_a_a9_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a8_a_a9 = !OPTATN_acombout & !OPTADR_acombout & !PCI_DIN_a15_a_acombout & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datab => OPTADR_acombout,
	datac => PCI_DIN_a15_a_acombout,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_ADR_a8_a_a9);

PCI_ADR_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a4_a_areg0 = DFFE(PCI_DIN_a6_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a6_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a4_a_areg0);

PCI_ADR_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a6_a_areg0 = DFFE(PCI_DIN_a8_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a8_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a6_a_areg0);

PCI_ADR_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a8_a_areg0 = DFFE(PCI_DIN_a10_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a10_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a8_a_areg0);

PCI_ADR_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a7_a_areg0 = DFFE(PCI_DIN_a9_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a9_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a7_a_areg0);

rtl_a253_I : apex20ke_lcell
-- Equation(s):
-- rtl_a253 = !PCI_ADR_a5_a_areg0 & !PCI_ADR_a6_a_areg0 & !PCI_ADR_a8_a_areg0 & !PCI_ADR_a7_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a5_a_areg0,
	datab => PCI_ADR_a6_a_areg0,
	datac => PCI_ADR_a8_a_areg0,
	datad => PCI_ADR_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a253);

PCI_DIN_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(3),
	combout => PCI_DIN_a3_a_acombout);

PCI_ADR_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a1_a_areg0 = DFFE(PCI_DIN_a3_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DIN_a3_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a1_a_areg0);

PCI_ADR_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a2_a_areg0 = DFFE(PCI_DIN_a4_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a4_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a2_a_areg0);

PCI_ADR_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a0_a_areg0 = DFFE(PCI_DIN_a2_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a2_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a0_a_areg0);

rtl_a254_I : apex20ke_lcell
-- Equation(s):
-- rtl_a254 = !PCI_ADR_a3_a_areg0 & PCI_ADR_a1_a_areg0 & PCI_ADR_a2_a_areg0 & PCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a3_a_areg0,
	datab => PCI_ADR_a1_a_areg0,
	datac => PCI_ADR_a2_a_areg0,
	datad => PCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a254);

PFF_OE_a32_I : apex20ke_lcell
-- Equation(s):
-- PFF_OE_a32 = !DPR_ACC_areg0 & !PCI_ADR_a4_a_areg0 & rtl_a253 & rtl_a254

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DPR_ACC_areg0,
	datab => PCI_ADR_a4_a_areg0,
	datac => rtl_a253,
	datad => rtl_a254,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_OE_a32);

PFF_OE_a2_I : apex20ke_lcell
-- Equation(s):
-- PFF_OE_a2 = !PTWR_acombout & PCI_PROC_a19 & OPTADR_acombout & PFF_OE_a32

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PTWR_acombout,
	datab => PCI_PROC_a19,
	datac => OPTADR_acombout,
	datad => PFF_OE_a32,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_OE_a2);

PCI_RD_a72_I : apex20ke_lcell
-- Equation(s):
-- PCI_RD_a72 = !PTWR_acombout & PCI_PROC_a19 & OPTADR_acombout & !PFF_OE_a32

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PTWR_acombout,
	datab => PCI_PROC_a19,
	datac => OPTADR_acombout,
	datad => PFF_OE_a32,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_RD_a72);

DPR_ACC_areg0_I : apex20ke_lcell
-- Equation(s):
-- DPR_ACC_areg0 = DFFE(OPTADR_acombout & DPR_ACC_areg0 # !OPTADR_acombout & (PCI_PROC_a19 & (PCI_DIN_a15_a_acombout) # !PCI_PROC_a19 & DPR_ACC_areg0), GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D8CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTADR_acombout,
	datab => DPR_ACC_areg0,
	datac => PCI_DIN_a15_a_acombout,
	datad => PCI_PROC_a19,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DPR_ACC_areg0);

PFF_OE_a33_I : apex20ke_lcell
-- Equation(s):
-- PFF_OE_a33 = OPTADR_acombout & !PTWR_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OPTADR_acombout,
	datad => PTWR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PFF_OE_a33);

PCI_RD_a0_I : apex20ke_lcell
-- Equation(s):
-- PCI_RD_a0 = !OPTATN_acombout & DPR_ACC_areg0 & PFF_OE_a33 & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datab => DPR_ACC_areg0,
	datac => PFF_OE_a33,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_RD_a0);

ODXFER_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ODXFER,
	combout => ODXFER_acombout);

DP_WRR_a16_I : apex20ke_lcell
-- Equation(s):
-- DP_WRR_a16 = PTWR_acombout & DPR_ACC_areg0 & !ODXFER_acombout & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PTWR_acombout,
	datab => DPR_ACC_areg0,
	datac => ODXFER_acombout,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DP_WRR_a16);

PCI_ADR_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a3_a_areg0 = DFFE(PCI_DIN_a5_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a5_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a3_a_areg0);

PCI_ADR_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_ADR_a5_a_areg0 = DFFE(PCI_DIN_a7_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_ADR_a8_a_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a7_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_ADR_a8_a_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_ADR_a5_a_areg0);

PCI_DPRA_a0_a_a17_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a0_a_a17 = !OPTATN_acombout & !OPTADR_acombout & PCI_DIN_a15_a_acombout & data_en_a85

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datab => OPTADR_acombout,
	datac => PCI_DIN_a15_a_acombout,
	datad => data_en_a85,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PCI_DPRA_a0_a_a17);

PCI_DPRA_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a0_a_areg0 = DFFE(PCI_DIN_a2_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DIN_a2_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a0_a_areg0);

PCI_DPRA_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a1_a_areg0 = DFFE(PCI_DIN_a3_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DIN_a3_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a1_a_areg0);

PCI_DPRA_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a2_a_areg0 = DFFE(PCI_DIN_a4_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a4_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a2_a_areg0);

PCI_DPRA_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a3_a_areg0 = DFFE(PCI_DIN_a5_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a5_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a3_a_areg0);

PCI_DPRA_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a4_a_areg0 = DFFE(PCI_DIN_a6_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a6_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a4_a_areg0);

PCI_DPRA_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a5_a_areg0 = DFFE(PCI_DIN_a7_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a7_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a5_a_areg0);

PCI_DPRA_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a6_a_areg0 = DFFE(PCI_DIN_a8_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a8_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a6_a_areg0);

PCI_DPRA_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a7_a_areg0 = DFFE(PCI_DIN_a9_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a9_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a7_a_areg0);

PCI_DPRA_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- PCI_DPRA_a8_a_areg0 = DFFE(PCI_DIN_a10_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , PCI_DPRA_a0_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a10_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => PCI_DPRA_a0_a_a17,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PCI_DPRA_a8_a_areg0);

clk20_proc_a229_I : apex20ke_lcell
-- Equation(s):
-- clk20_proc_a229 = !PCI_ADR_a3_a_areg0 & !PCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_ADR_a3_a_areg0,
	datad => PCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clk20_proc_a229);

clk20_proc_a230_I : apex20ke_lcell
-- Equation(s):
-- clk20_proc_a230 = clk20_proc_a228 & !PCI_ADR_a4_a_areg0 & clk20_proc_a229 & rtl_a253

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => clk20_proc_a228,
	datab => PCI_ADR_a4_a_areg0,
	datac => clk20_proc_a229,
	datad => rtl_a253,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clk20_proc_a230);

WE_DCD_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a0_a_areg0 = DFFE(!PCI_ADR_a1_a_areg0 & (!PCI_ADR_a2_a_areg0 & clk20_proc_a230), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0500",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datac => PCI_ADR_a2_a_areg0,
	datad => clk20_proc_a230,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a0_a_areg0);

rtl_a255_I : apex20ke_lcell
-- Equation(s):
-- rtl_a255 = !PCI_ADR_a3_a_areg0 & PCI_ADR_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_ADR_a3_a_areg0,
	datad => PCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a255);

clk20_proc_a231_I : apex20ke_lcell
-- Equation(s):
-- clk20_proc_a231 = clk20_proc_a228 & !PCI_ADR_a4_a_areg0 & rtl_a255 & rtl_a253

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => clk20_proc_a228,
	datab => PCI_ADR_a4_a_areg0,
	datac => rtl_a255,
	datad => rtl_a253,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clk20_proc_a231);

WE_DCD_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a1_a_areg0 = DFFE(!PCI_ADR_a1_a_areg0 & clk20_proc_a231 & !PCI_ADR_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0404",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datab => clk20_proc_a231,
	datac => PCI_ADR_a2_a_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a1_a_areg0);

WE_DCD_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a2_a_areg0 = DFFE(PCI_ADR_a1_a_areg0 & (!PCI_ADR_a2_a_areg0 & clk20_proc_a230), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0A00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datac => PCI_ADR_a2_a_areg0,
	datad => clk20_proc_a230,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a2_a_areg0);

WE_DCD_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a3_a_areg0 = DFFE(PCI_ADR_a1_a_areg0 & clk20_proc_a231 & !PCI_ADR_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datab => clk20_proc_a231,
	datac => PCI_ADR_a2_a_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a3_a_areg0);

WE_DCD_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a4_a_areg0 = DFFE(!PCI_ADR_a1_a_areg0 & (PCI_ADR_a2_a_areg0 & clk20_proc_a230), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datac => PCI_ADR_a2_a_areg0,
	datad => clk20_proc_a230,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a4_a_areg0);

WE_DCD_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a5_a_areg0 = DFFE(!PCI_ADR_a1_a_areg0 & clk20_proc_a231 & PCI_ADR_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4040",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datab => clk20_proc_a231,
	datac => PCI_ADR_a2_a_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a5_a_areg0);

WE_DCD_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a6_a_areg0 = DFFE(PCI_ADR_a1_a_areg0 & (PCI_ADR_a2_a_areg0 & clk20_proc_a230), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a1_a_areg0,
	datac => PCI_ADR_a2_a_areg0,
	datad => clk20_proc_a230,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a6_a_areg0);

data_en_ao_a10_I : apex20ke_lcell
-- Equation(s):
-- data_en_ao_a10 = !data_en_c20_vec_a0_a & data_en_ao

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => data_en_c20_vec_a0_a,
	datad => data_en_ao,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_en_ao_a10);

data_en_a86_I : apex20ke_lcell
-- Equation(s):
-- data_en_a86 = (!ODXFER_acombout & PTWR_acombout) & CASCADE(data_en_a88)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ODXFER_acombout,
	datad => PTWR_acombout,
	cascin => data_en_a88,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_en_a86);

data_en_ao_aI : apex20ke_lcell
-- Equation(s):
-- data_en_ao = DFFE(data_en_ao_a10 # OPTATN_acombout & OPTADR_acombout & data_en_a86, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F8F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OPTATN_acombout,
	datab => OPTADR_acombout,
	datac => data_en_ao_a10,
	datad => data_en_a86,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_en_ao);

data_en_c20_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- data_en_c20_vec_a0_a = DFFE(data_en_ao, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => data_en_ao,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_en_c20_vec_a0_a);

data_en_c20_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- data_en_c20_vec_a1_a = DFFE(data_en_c20_vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => data_en_c20_vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_en_c20_vec_a1_a);

clk20_proc_a228_I : apex20ke_lcell
-- Equation(s):
-- clk20_proc_a228 = data_en_c20_vec_a0_a & !DPR_ACC_areg0 & !data_en_c20_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_en_c20_vec_a0_a,
	datac => DPR_ACC_areg0,
	datad => data_en_c20_vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clk20_proc_a228);

WE_DCD_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a7_a_areg0 = DFFE(rtl_a253 & rtl_a254 & clk20_proc_a228 & !PCI_ADR_a4_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a253,
	datab => rtl_a254,
	datac => clk20_proc_a228,
	datad => PCI_ADR_a4_a_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a7_a_areg0);

clk20_proc_a232_I : apex20ke_lcell
-- Equation(s):
-- clk20_proc_a232 = PCI_ADR_a3_a_areg0 & !PCI_ADR_a4_a_areg0 & clk20_proc_a228 & rtl_a253

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a3_a_areg0,
	datab => PCI_ADR_a4_a_areg0,
	datac => clk20_proc_a228,
	datad => rtl_a253,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clk20_proc_a232);

WE_DCD_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a8_a_areg0 = DFFE(!PCI_ADR_a2_a_areg0 & !PCI_ADR_a0_a_areg0 & !PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0100",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a8_a_areg0);

WE_DCD_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a9_a_areg0 = DFFE(!PCI_ADR_a2_a_areg0 & PCI_ADR_a0_a_areg0 & !PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a9_a_areg0);

WE_DCD_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a10_a_areg0 = DFFE(!PCI_ADR_a2_a_areg0 & !PCI_ADR_a0_a_areg0 & PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a10_a_areg0);

WE_DCD_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a11_a_areg0 = DFFE(!PCI_ADR_a2_a_areg0 & PCI_ADR_a0_a_areg0 & PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a11_a_areg0);

WE_DCD_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a12_a_areg0 = DFFE(PCI_ADR_a2_a_areg0 & !PCI_ADR_a0_a_areg0 & !PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a12_a_areg0);

WE_DCD_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a13_a_areg0 = DFFE(PCI_ADR_a2_a_areg0 & PCI_ADR_a0_a_areg0 & !PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a13_a_areg0);

WE_DCD_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a14_a_areg0 = DFFE(PCI_ADR_a2_a_areg0 & !PCI_ADR_a0_a_areg0 & PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a14_a_areg0);

WE_DCD_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a15_a_areg0 = DFFE(PCI_ADR_a2_a_areg0 & PCI_ADR_a0_a_areg0 & PCI_ADR_a1_a_areg0 & clk20_proc_a232, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PCI_ADR_a2_a_areg0,
	datab => PCI_ADR_a0_a_areg0,
	datac => PCI_ADR_a1_a_areg0,
	datad => clk20_proc_a232,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a15_a_areg0);

PCI_DIN_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(0),
	combout => PCI_DIN_a0_a_acombout);

data_en_a78_I : apex20ke_lcell
-- Equation(s):
-- data_en_a78 = OPTADR_acombout & OPTATN_acombout & data_en_a86

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => OPTADR_acombout,
	datac => OPTATN_acombout,
	datad => data_en_a86,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_en_a78);

Data_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a0_a = DFFE(PCI_DIN_a0_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a0_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a0_a);

PCI_DIN_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(1),
	combout => PCI_DIN_a1_a_acombout);

Data_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a1_a = DFFE(PCI_DIN_a1_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a1_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a1_a);

Data_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a2_a = DFFE(PCI_DIN_a2_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a2_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a2_a);

Data_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a3_a = DFFE(PCI_DIN_a3_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a3_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a3_a);

Data_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a4_a = DFFE(PCI_DIN_a4_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a4_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a4_a);

Data_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a5_a = DFFE(PCI_DIN_a5_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a5_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a5_a);

Data_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a6_a = DFFE(PCI_DIN_a6_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DIN_a6_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a6_a);

Data_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a7_a = DFFE(PCI_DIN_a7_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a7_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a7_a);

Data_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a8_a = DFFE(PCI_DIN_a8_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a8_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a8_a);

Data_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a9_a = DFFE(PCI_DIN_a9_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a9_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a9_a);

Data_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a10_a = DFFE(PCI_DIN_a10_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a10_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a10_a);

PCI_DIN_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(11),
	combout => PCI_DIN_a11_a_acombout);

Data_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a11_a = DFFE(PCI_DIN_a11_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a11_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a11_a);

PCI_DIN_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(12),
	combout => PCI_DIN_a12_a_acombout);

Data_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a12_a = DFFE(PCI_DIN_a12_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a12_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a12_a);

PCI_DIN_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(13),
	combout => PCI_DIN_a13_a_acombout);

Data_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a13_a = DFFE(PCI_DIN_a13_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a13_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a13_a);

PCI_DIN_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PCI_DIN(14),
	combout => PCI_DIN_a14_a_acombout);

Data_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a14_a = DFFE(PCI_DIN_a14_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PCI_DIN_a14_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a14_a);

Data_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Data_FF_adffs_a15_a = DFFE(PCI_DIN_a15_a_acombout, GLOBAL(AO_CLK_acombout), !GLOBAL(IMR_acombout), , data_en_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PCI_DIN_a15_a_acombout,
	clk => AO_CLK_acombout,
	aclr => IMR_acombout,
	ena => data_en_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data_FF_adffs_a15_a);

PCI_RST_OUT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_RST_OUT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_RST_OUT);

PFF_REN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PFF_REN_a103,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_REN);

PFF_OE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PFF_OE_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PFF_OE);

PCI_RD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_RD_a72,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_RD);

DPR_ACC_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DPR_ACC_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DPR_ACC);

DP_OER_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_RD_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DP_OER);

DP_WRR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DP_WRR_a16,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DP_WRR);

PCI_ADR_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(0));

PCI_ADR_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(1));

PCI_ADR_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(2));

PCI_ADR_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(3));

PCI_ADR_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(4));

PCI_ADR_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(5));

PCI_ADR_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(6));

PCI_ADR_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(7));

PCI_ADR_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_ADR_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_ADR(8));

PCI_DPRA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(0));

PCI_DPRA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(1));

PCI_DPRA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(2));

PCI_DPRA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(3));

PCI_DPRA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(4));

PCI_DPRA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(5));

PCI_DPRA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(6));

PCI_DPRA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(7));

PCI_DPRA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PCI_DPRA_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_DPRA(8));

WE_DCD_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(0));

WE_DCD_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(1));

WE_DCD_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(2));

WE_DCD_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(3));

WE_DCD_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(4));

WE_DCD_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(5));

WE_DCD_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(6));

WE_DCD_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(7));

WE_DCD_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(8));

WE_DCD_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(9));

WE_DCD_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(10));

WE_DCD_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(11));

WE_DCD_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(12));

WE_DCD_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(13));

WE_DCD_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(14));

WE_DCD_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(15));

PCI_WD_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(0));

PCI_WD_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(1));

PCI_WD_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(2));

PCI_WD_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(3));

PCI_WD_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(4));

PCI_WD_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(5));

PCI_WD_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(6));

PCI_WD_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(7));

PCI_WD_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(8));

PCI_WD_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(9));

PCI_WD_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(10));

PCI_WD_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(11));

PCI_WD_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(12));

PCI_WD_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(13));

PCI_WD_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(14));

PCI_WD_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Data_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PCI_WD(15));
END structure;


