-----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PCICTRL.VHD
-----------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 4, 1999
--   Board          : DPP-II Board
--   Schematic      : DPP-II: 4035.007.19700
--
--   FPGA Code P/N  : 4035.009.99980 - S/W, DPP-II, GLUE FPGA
--   FPGA ROM  P/N  : 9335.078.00??? - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00??? - EP20K30ETC144-2X
--
--   Description:
--   History:       <date> - <Author>
--		November 2001 - MCS
--			Module has been debuggged and implemented in the DPP-II Board
--
-----------------------------------------------------------------------------------------
--
-- TODO take ODXFER out of the "Read Logic"
--
-----------------------------------------------------------------------------------------


library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

-----------------------------------------------------------------------------------------
architecture test_bench of tb is

	signal IMR          : std_logic := '1';                  		-- Local Master Reset
	signal CLK20		: std_logic := '0';
	signal AO_CLK		: std_logic := '0';                  		-- Local System Clock
	signal OPTATN       : std_logic;                      -- Pass Thru Attention
	signal PTWR         : std_logic;                      -- Pass Thru Write
	signal ODXFER       : std_logic;                      -- Active Transfer Complete
	signal PTNUM        : std_logic_vector( 1 downto 0 ); -- Pass Thru Number
	signal OPTBE        : std_logic_vector( 3 downto 0 ); -- Pass Thru Byte Enable
	signal OPTADR       : std_logic;                      -- Active Mode Address Valid
	signal PCI_DIN      : std_logic_vector( 15 downto 0 );-- PCI Interface Data Bus
	signal PCI_RST_IN	: std_logic;
	signal PCI_RST_OUT	: std_logic;
	signal PFF_REN		: std_logic;				  -- PCI FIFO Read Enable
	signal PFF_OE		: std_logic;
	signal PCI_RD       : std_logic;                       -- Local PCI Read COmmand
	signal DPR_ACC		: std_logic;				   -- Dual Port Ram Access
	signal DP_OER		: std_Logic;
	signal DP_WRR		: std_logic;
	signal PCI_ADR 	: std_logic_Vector( 8 downto 0 );		
	signal PCI_DPRA 	: std_logic_Vector( 8 downto 0 );		
	signal WE_DCD 	     : std_logic_vector( 15 downto 0 );
	signal PCI_WD		: std_logic_vector( 15 downto 0 );


	signal write_trig 	: std_logic;
	signal Write_Done	: std_logic;
	signal read_trig	: std_logic;
	signal read_done	: std_logic;
	signal write		: std_logic_vector( 15 downto 0 );
	signal read		: std_logic_vector( 15 downto 0 );
	signal addr		: std_logic_vector( 15 downto 0 );

	-----------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------
	component PCICTRL is 
		port( 
	     	IMR            : in      std_logic;                  		-- Local Master Reset
			CLK20		: in		std_logic;
		     AO_CLK		: in      std_logic;                  		-- Local System Clock
		     OPTATN         : in      std_logic;                      -- Pass Thru Attention
			PTWR           : in      std_logic;                      -- Pass Thru Write
		     ODXFER         : in      std_logic;                      -- Active Transfer Complete
	     	PTNUM          : in      std_logic_vector( 1 downto 0 ); -- Pass Thru Number
		     OPTBE          : in      std_logic_vector( 3 downto 0 ); -- Pass Thru Byte Enable
	     	OPTADR         : in      std_logic;                      -- Active Mode Address Valid
		     PCI_DIN        : in      std_logic_vector( 15 downto 0 );-- PCI Interface Data Bus
			PCI_RST_IN	: in		std_logic;
			PCI_RST_OUT	: out	std_logic;
			PFF_REN		: out  std_logic;				  -- PCI FIFO Read Enable
			PFF_OE		: out	std_logic;
		     PCI_RD         : out  std_logic;                       -- Local PCI Read COmmand
			DPR_ACC		: out	std_logic;				   -- Dual Port Ram Access
			DP_OER		: out	std_Logic;
			DP_WRR		: out	std_logic;
			PCI_ADR 		: out	std_logic_Vector( 8 downto 0 );		
			PCI_DPRA 		: out	std_logic_Vector( 8 downto 0 );		
	     	WE_DCD 	     : out  std_logic_vector( 15 downto 0 );
			PCI_WD		: out	std_logic_vector( 15 downto 0 ));
	end component PCICTRL;	
	-----------------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;
	CLK20	<= not( clk20 ) after 25 ns;
	AO_CLK	<= not( AO_CLK	) after 14 ns;
	
	U : pcictrl 
		port map(
	     	IMR            => imr,
			CLK20		=> clk20,
		     AO_CLK		=> AO_CLK,
		     OPTATN         => OPTATN,
			PTWR           => PTWR,
		     ODXFER         => ODXFER,
	     	PTNUM          => PTNUM,
		     OPTBE          => OPTBE,
	     	OPTADR         => OPTADR,
		     PCI_DIN        => PCI_DIN,
			PCI_RST_IN	=> PCI_RST_IN,
			PCI_RST_OUT	=> PCI_RST_OUT,
			PFF_REN		=> PFF_REN,
			PFF_OE		=> PFF_OE,
		     PCI_RD         => PCI_RD,
			DPR_ACC		=> DPR_ACC,
			DP_OER		=> DP_OER,
			DP_WRR		=> DP_WRR,
			PCI_ADR 		=> PCI_ADR,
			PCI_DPRA 		=> PCI_DPRA,
	     	WE_DCD 	     => WE_DCD,
			PCI_WD		=> PCI_WD );
			
	
	-----------------------------------------------------------------------------------------
	amcc_proc : process
	begin
		OPTATN	<= '1';
		PTNUM	<= "11";
		PTWR		<= '1';
		OPTBE     <= x"F";
	     ODXFER    <= '1';
     	OPTADR    <= '1';
		PCI_DIN	<= x"0000";
		Read		<= x"0000";
		Write_Done	<= '0';
		Read_Done		<= '0';
		
		wait until (( Write_Trig'event ) or ( Read_Trig'Event ));
		if( Write_Trig = '1' ) then
			for i in 0 to 8 loop
				wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));
				wait for 5 ns;
				case i is
					when 0 => OPTATN <= '1'; PTNUM <= "11"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 1 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 2 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '1'; OPTBE <= x"0"; ODXFER <= '1'; OPTADR <= '0'; PCI_DIN <= addr;
					when 3 => OPTATN <= '1'; PTNUM <= "00"; PTWR <= '1'; OPTBE <= x"0"; ODXFER <= '0'; OPTADR <= '1'; PCI_DIN <= Write;
					when 4 => OPTATN <= '1'; PTNUM <= "00"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 5 => OPTATN <= '1'; PTNUM <= "11"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 6 => OPTATN <= '1'; PTNUM <= "11"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 7 => Write_Done <= '1';
					when 8 => Write_Done <= '0';
				end case;
			end loop;
		
		end if;
		
		if( Read_trig = '1' ) then
			for i in 0 to 8 loop
				wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));
				wait for 5 ns;
				case i is
					when 0 => OPTATN <= '1'; PTNUM <= "11"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 1 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '0'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 2 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '0'; OPTBE <= x"0"; ODXFER <= '1'; OPTADR <= '0'; PCI_DIN <= addr;
					when 3 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '0'; OPTBE <= x"0"; ODXFER <= '1'; OPTADR <= '1'; PCI_DIN <= x"0000";
					when 4 => OPTATN <= '0'; PTNUM <= "00"; PTWR <= '0'; OPTBE <= x"0"; ODXFER <= '0'; OPTADR <= '1'; 
					when 5 => OPTATN <= '1'; PTNUM <= "00"; PTWR <= '0'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 6 => OPTATN <= '1'; PTNUM <= "11"; PTWR <= '1'; OPTBE <= x"F"; ODXFER <= '1'; OPTADR <= '1'; 
					when 7 => Read_Done <= '1';
					when 8 => Read_Done <= '0';
				end case;
			end loop;
		end if;
	end process;

	tb_proc : process
	begin
		pci_rst_in	<= '0';
		Write_Trig	<= '0';
		Read_Trig		<= '0';
		Write		<= x"0000";
		addr			<= x"0000";
		wait for 2 us;
		
			-----------------------------------------------------------------------------------
		assert false
			report "Write to DPP"
			severity note;
		for i in 0 to 7 loop
			write(i) 	<= '1';
			addr		<= conv_std_logic_Vector( i, 14 ) & "00";
			Write_Trig <= '1';
			wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));			
			Write_Trig <= '0';
			wait until (( Write_Done'Event ) and ( Write_DOne = '1' ));
			wait for 1 us;
			Write(i) <= '0';
		end loop;
		-----------------------------------------------------------------------------------
		assert false
			report "Read from DPP"
			severity note;
		for i in 0 to 7 loop
			Read_Trig <= '1';
			addr		<= conv_std_logic_Vector( i, 14 ) & "00";
			wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));			
			Read_Trig <= '0';
			wait until (( Read_Done'Event ) and ( Read_DOne = '1' ));
			wait for 1 us;
		end loop;
		-----------------------------------------------------------------------------------
		
		-----------------------------------------------------------------------------------
		assert false
			report "Write to DPRAM"
			severity note;
		for i in 0 to 7 loop
			write(i) 	<= '1';
			addr		<= '1' & conv_std_logic_Vector( i, 13 ) & "00";
			Write_Trig <= '1';
			wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));			
			Write_Trig <= '0';
			wait until (( Write_Done'Event ) and ( Write_DOne = '1' ));
			wait for 1 us;
			Write(i) <= '0';
		end loop;
		-----------------------------------------------------------------------------------
		assert false
			report "Read from DPRAM"
			severity note;
		for i in 0 to 7 loop
			Read_Trig <= '1';
			addr		<= '1' & conv_std_logic_Vector( i, 13 ) & "00";
			wait  until(( AO_CLK'Event ) and ( AO_CLK = '1' ));			
			Read_Trig <= '0';
			wait until (( Read_Done'Event ) and ( Read_DOne = '1' ));
			wait for 1 us;
		end loop;
		-----------------------------------------------------------------------------------
		
		assert( false )
			report "End of Simulations"
			severity failure;		
		wait;
	end process;
-----------------------------------------------------------------------------------------
end test_bench;			-- PCICTRL.VHD
-----------------------------------------------------------------------------------------

