del *.vqm
del *.esf
del *.ssd
del *.htm
del *.rpt
del *.hexout
del *.txt
del *.fsf
del *.ini
del transcript.*
del dfp_param.*
del db\*.* /q
del ls_work\*.* /q
rem del *.pof
rem del *.sof

cd dpram
call _aclup.bat
cd ..

cd dspboot
call _aclup.bat
cd ..

cd dspctrl
call _aclup.bat
cd ..

cd fconfig
call _aclup.bat
cd ..

cd ffcnt
call _aclup.bat
cd ..

cd pcictrl
call _aclup.bat
cd ..

cd pciwrt2c10
call _aclup.bat
cd ..
