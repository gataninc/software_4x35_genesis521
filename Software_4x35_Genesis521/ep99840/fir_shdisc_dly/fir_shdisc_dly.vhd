-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_shdisc_dly.vhd
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules the Disc_Outputs and generates an Edge Detect for
--		Those that are enabled and not blocked.
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity fir_shdisc_dly is 
	port( 
	     imr            : in      std_logic;                    -- Master Reset
		clkf           : in      std_logic;			     -- Master Clock Input
		clks			: in		std_logic;
		PA_Reset		: in		std_logic;
		FIR_MR		: in		std_logic;
		dly_len		: in		std_logic_Vector( 8 downto 0 );
		Disc_En		: in		std_logic;
		HDisc_Out		: in		std_logic;
	     Disc_In        : in      std_logic;
		SHDisc_MEN	: buffer	std_logic_vector(  3 downto 0 );
		SHDisc_MDly 	: buffer	std_logic_vector(  8 downto 0 );		
	     Disc_Out       : buffer	std_logic );
end fir_shdisc_dly;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_shdisc_dly is
	constant Dly_Bits			: integer := 9;
	constant SHDisc_MDly_offset 	: integer := 10;

	signal Disc_out_Vec			: std_logic_Vector( 1 downto 0 );
	signal DiscF 				: std_logic;

	signal Disc_Dly_In			: std_logic_vector( 1 downto 0 );
	signal Disc_Dly_Out			: std_logic_vector( 1 downto 0 );
	signal wadr				: std_logic_vector( Dly_Bits-1 downto 0 );
	signal radr				: std_logic_vector( Dly_Bits-1 downto 0 );
	signal Disc_Dly_out1_Del		: std_logic;
	signal SH_ch_cnt 			: integer range 0 to 3;
	signal H_ch_cnt 			: integer range 0 to 3;

	signal shdisc_mdly_vec		: std_logic_Vector( 35 downto 0 );
	signal abort				: std_logic_vector( 3 downto 0 );
	signal HDisc_Out_Del		: std_logic;
	signal DiscF_Vec			: std_logic_vector( 1 downto 0 );
	signal Ext_Abort			: std_logic;
	signal SHDisc_MDly_Sub 		: std_logic_vector( 8 downto 0 );
	-------------------------------------------------------------------------------
	component SH_PW_Meas
		port( 
		     imr            : in      std_logic;                    -- Master Reset
			clk			: in		std_logic;
			Ext_Abort		: in		std_logic;
			Men			: in		std_logic;
			abort		: buffer	std_logic;
			MDly 		: buffer	std_logic_vector( 8 downto 0 ));		
	end component SH_PW_Meas;
	-------------------------------------------------------------------------------
	
begin
	

	dd_wadr_cntr : lpm_counter
		generic map(
			LPM_WIDTH				=> Dly_Bits,
			LPM_TYPE				=> "LPM_COUNTER" )
		port map(
			aclr					=> IMR,
			clock				=> clks,
			q					=> wadr );

	dd_radr_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH				=> Dly_Bits,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION		=> "UNSIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			dataa				=> wadr,
			datab				=> dly_len,
			result				=> radr );				

	-- Bit 0 is the SUper High Disc Delay
	-- Bit 1 is the SDD Pile-Up Rehject
	dd_mem : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 2,	-- Data Width
			LPM_WIDTHAD			=> Dly_Bits,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT.MIF",
			LPM_TYPE				=> "LPM_RAM_DP" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clks,
			rdclock 				=> clks,
			rdaddress				=> radr,
			wraddress 			=> wadr,
			data					=> Disc_Dly_In,
			q					=> Disc_Dly_Out );	
	--------------------------------------------------------------------
	-- Sample SuperHIgh Speed Disc w/ Higher Speed clock 
	-- to "stretch" it down to the lower speed
     clkf_proc : process( clkf, imr )
     begin
          if( imr = '1' ) then
			DiscF 	<= '0';
          elsif(( CLKf'Event ) and ( CLKf = '1' )) then
			if(Disc_In = '1' )
				then DiscF <= '1';
			elsif( Disc_Dly_In(0) = '1' )
				then DiscF <= '0';
			end if;
          end if;                  -- rising_edge
     end process;
	--------------------------------------------------------------------

	--------------------------------------------------------------------
	clks_proc : process( clks, imr )
	begin
		if( imr = '1' ) then
			Disc_Dly_In		<= "00";
			Disc_Dly_out1_Del 	<= '0';
			Disc_Out_Vec 		<= "00";
			Disc_Out			<= '0';			
		elsif(( clks'event ) and ( clks = '1' )) then
			Disc_Dly_In		<= '0' & DiscF;		-- Input to Delay Memory
			Disc_Dly_Out1_Del 	<= Disc_Dly_Out(1);
			Disc_Out_Vec 		<= Disc_Out_Vec( 0 ) & Disc_Dly_Out(0);

			if(( Disc_en = '0' ) or ( PA_Reset = '1' ))
				then Disc_Out <= '0';
			elsif( Disc_Out_Vec = "01" )
				then Disc_Out <= '1';
				else Disc_Out <= '0';
			end if;
		end if;
	end process;
	--------------------------------------------------------------------
	Ext_Abort <= '1' when (( PA_Reset = '1' ) or ( FIR_MR = '1' )) else '0';
	--------------------------------------------------------------------
	UM : for i in 0 to 3 generate
		U0 : SH_PW_Meas
			port map(
				imr			=> imr,
				clk			=> clks,
				Ext_Abort		=> Ext_Abort,
				Men			=> SHDisc_MEN(i),
				abort		=> abort(i),
				mdly			=> SHDisc_MDly_Vec( (9*i)+8 downto (9*i) ) );
	end generate;
	SHDisc_Mdly_Sub	<= conv_std_logic_vector( SHDisc_MDly_offset, 9 );
	--------------------------------------------------------------------
	SHDisc_MDly_Add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 9,
			LPM_DIRECTIOn		=> "SUB",
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clks,
			Cin				=> '1',
			dataa			=> SHDisc_MDly_Vec(  8 downto  0 ),
			datab			=> SHDisc_Mdly_Sub,
			result			=> SHDisc_Mdly );
	--------------------------------------------------------------------
	Auto_SHDisc_Proc : process( imr, clks )
	begin
		if( imr = '1' ) then
			SHDisc_MEN		<= x"0";		
			HDisc_Out_Del		<= '0';
			DiscF_Vec			<= "00";
			SH_ch_cnt			<= 0;			
			H_ch_cnt			<= 0;			
		
		elsif(( clks'event ) and ( clks = '1' )) then
			-- Logic to Automatically Measure the Input delay from Super High Disc to High Speed Disc
			-- Gate to be used to measure the delay from the SH Input to the H input
			HDisc_Out_Del	<= HDisc_Out;
			DiscF_Vec		<= Discf_Vec(0) & DiscF;	

			-- At the edge detect of SH Input, Increment the SH Channel Count
			if( DiscF_Vec = "01" ) 
				then SH_ch_cnt <= SH_ch_cnt + 1;
			elsif(( conv_integer( SHDisc_Men ) = 0 ) or ( Ext_Abort = '1' ))
				then SH_Ch_Cnt <= 0;
			end if;

			-- At the High Input, increment the H Channel Count			
			if(( HDisc_Out = '1' ) and ( HDisc_Out_Del = '0' ))
				then H_Ch_Cnt	<= H_Ch_Cnt + 1;
			elsif(( conv_integer( SHDisc_Men ) = 0 ) or ( Ext_Abort = '1' ))
				then H_Ch_Cnt <= 0;
			end if;
			
			for i in 0 to 3 loop
				if(( Abort(i) = '1' ) or ( Ext_Abort = '1' ))
					then SHDisc_Men(i) <= '0';
				elsif(( DiscF_Vec = "01" )  and ( SH_ch_cnt = i ))
					then SHDisc_Men(i) <= '1';
				elsif(( HDisc_Out = '1' ) and ( HDisc_Out_Del = '0' ) and ( H_Ch_Cnt = i ))
					then SHDisc_Men(i) <= '0';
				end if;
			end loop;		
		end if;
	end process;	
	--------------------------------------------------------------------
-------------------------------------------------------------------------------
end behavioral;               -- fir_shdisc_dly 
-------------------------------------------------------------------------------