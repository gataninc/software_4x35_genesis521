-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:27:37"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_shdisc_dly IS
    PORT (
	imr : IN std_logic;
	clkf : IN std_logic;
	clks : IN std_logic;
	PA_Reset : IN std_logic;
	FIR_MR : IN std_logic;
	dly_len : IN std_logic_vector(8 DOWNTO 0);
	Disc_En : IN std_logic;
	HDisc_Out : IN std_logic;
	Disc_In : IN std_logic;
	SHDisc_MEN : OUT std_logic_vector(3 DOWNTO 0);
	SHDisc_MDly : OUT std_logic_vector(8 DOWNTO 0);
	Disc_Out : OUT std_logic
	);
END fir_shdisc_dly;

ARCHITECTURE structure OF fir_shdisc_dly IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clkf : std_logic;
SIGNAL ww_clks : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_FIR_MR : std_logic;
SIGNAL ww_dly_len : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_Disc_En : std_logic;
SIGNAL ww_HDisc_Out : std_logic;
SIGNAL ww_Disc_In : std_logic;
SIGNAL ww_SHDisc_MEN : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_SHDisc_MDly : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_Disc_Out : std_logic;
SIGNAL dd_mem_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dd_mem_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL dd_mem_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Disc_Dly_In_a0_a : std_logic;
SIGNAL DiscF_Vec_a1_a : std_logic;
SIGNAL SH_ch_cnt_a0_a : std_logic;
SIGNAL SH_ch_cnt_a1_a : std_logic;
SIGNAL SHDisc_MEN_a323 : std_logic;
SIGNAL SHDisc_MEN_a327 : std_logic;
SIGNAL SHDisc_MEN_a331 : std_logic;
SIGNAL SHDisc_MEN_a335 : std_logic;
SIGNAL DiscF : std_logic;
SIGNAL Equal_a183 : std_logic;
SIGNAL Equal_a184 : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a5_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a7_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_a88 : std_logic;
SIGNAL a_aUM_a0_aU0_acnt_cen_a46 : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_a86 : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_a89 : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_a86 : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_a89 : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_vec_a0_a : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_vec_a1_a : std_logic;
SIGNAL a_aUM_a2_aU0_acnt_cen_a1 : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL a_aUM_a2_aU0_amen_dly : std_logic;
SIGNAL a_aUM_a2_aU0_acnt_clr_a10 : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_a92 : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_a86 : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_a93 : std_logic;
SIGNAL a_aUM_a2_aU0_aabort_a89 : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_vec_a0_a : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_vec_a1_a : std_logic;
SIGNAL a_aUM_a3_aU0_acnt_cen_a1 : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_a86 : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_a89 : std_logic;
SIGNAL Disc_In_acombout : std_logic;
SIGNAL clkf_acombout : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL clks_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL a_aUM_a0_aU0_amen_dly : std_logic;
SIGNAL a_aUM_a0_aU0_acnt_clr_a10 : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_a92 : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_a93 : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_a88 : std_logic;
SIGNAL FIR_MR_acombout : std_logic;
SIGNAL SHDisc_MEN_a339 : std_logic;
SIGNAL HDisc_Out_acombout : std_logic;
SIGNAL HDisc_Out_Del : std_logic;
SIGNAL Auto_SHDisc_Proc_a12 : std_logic;
SIGNAL H_ch_cnt_a0_a : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_vec_a0_a : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_vec_a1_a : std_logic;
SIGNAL a_aUM_a1_aU0_acnt_cen_a1 : std_logic;
SIGNAL a_aUM_a1_aU0_amen_dly : std_logic;
SIGNAL a_aUM_a1_aU0_acnt_clr_a10 : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_a92 : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_a93 : std_logic;
SIGNAL a_aUM_a1_aU0_aabort_a88 : std_logic;
SIGNAL Equal_a182 : std_logic;
SIGNAL SHDisc_MEN_a328 : std_logic;
SIGNAL SHDisc_MEN_a1_a_areg0 : std_logic;
SIGNAL Equal_a185 : std_logic;
SIGNAL Equal_a181 : std_logic;
SIGNAL SHDisc_MEN_a332 : std_logic;
SIGNAL SHDisc_MEN_a2_a_areg0 : std_logic;
SIGNAL Auto_SHDisc_Proc_a0 : std_logic;
SIGNAL H_ch_cnt_a1_a : std_logic;
SIGNAL SHDisc_MEN_a324 : std_logic;
SIGNAL SHDisc_MEN_a0_a_areg0 : std_logic;
SIGNAL a_aUM_a3_aU0_amen_dly : std_logic;
SIGNAL a_aUM_a3_aU0_acnt_clr_a10 : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_a92 : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_a93 : std_logic;
SIGNAL a_aUM_a3_aU0_aabort_a88 : std_logic;
SIGNAL SHDisc_MEN_a336 : std_logic;
SIGNAL SHDisc_MEN_a3_a_areg0 : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_vec_a0_a : std_logic;
SIGNAL a_aUM_a0_aU0_aabort_vec_a1_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_en_a23 : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a0_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a1_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a2_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acout_a1_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a3_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a4_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a78 : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a6_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acout_a0_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a82 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a86 : std_logic;
SIGNAL a_aUM_a0_aU0_amdly_ff_adffs_a8_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a88 : std_logic;
SIGNAL SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a90 : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL dly_len_a0_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a208 : std_logic;
SIGNAL dly_len_a1_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a212 : std_logic;
SIGNAL dly_len_a2_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a216 : std_logic;
SIGNAL dly_len_a3_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a220 : std_logic;
SIGNAL dly_len_a4_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a224 : std_logic;
SIGNAL dly_len_a5_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a228 : std_logic;
SIGNAL dly_len_a6_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a232 : std_logic;
SIGNAL dly_len_a7_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a236 : std_logic;
SIGNAL dly_len_a8_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a238 : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a240 : std_logic;
SIGNAL dd_mem_asram_aq_a0_a : std_logic;
SIGNAL Disc_out_Vec_a0_a : std_logic;
SIGNAL Disc_out_Vec_a1_a : std_logic;
SIGNAL Disc_En_acombout : std_logic;
SIGNAL Disc_Out_areg0 : std_logic;

BEGIN

ww_imr <= imr;
ww_clkf <= clkf;
ww_clks <= clks;
ww_PA_Reset <= PA_Reset;
ww_FIR_MR <= FIR_MR;
ww_dly_len <= dly_len;
ww_Disc_En <= Disc_En;
ww_HDisc_Out <= HDisc_Out;
ww_Disc_In <= Disc_In;
SHDisc_MEN <= ww_SHDisc_MEN;
SHDisc_MDly <= ww_SHDisc_MDly;
Disc_Out <= ww_Disc_Out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

dd_mem_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_wadr_cntr_awysi_counter_asload_path_a8_a & dd_wadr_cntr_awysi_counter_asload_path_a7_a & dd_wadr_cntr_awysi_counter_asload_path_a6_a & 
dd_wadr_cntr_awysi_counter_asload_path_a5_a & dd_wadr_cntr_awysi_counter_asload_path_a4_a & dd_wadr_cntr_awysi_counter_asload_path_a3_a & dd_wadr_cntr_awysi_counter_asload_path_a2_a & dd_wadr_cntr_awysi_counter_asload_path_a1_a & 
dd_wadr_cntr_awysi_counter_asload_path_a0_a);

dd_mem_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a240 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a236 & 
dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a232 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a228 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a224 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a220 & 
dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a216 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a212 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a208);

Disc_Dly_In_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Dly_In_a0_a = DFFE(DiscF, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DiscF,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Dly_In_a0_a);

DiscF_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DiscF_Vec_a1_a = DFFE(Disc_Dly_In_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Dly_In_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DiscF_Vec_a1_a);

SH_ch_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SH_ch_cnt_a0_a = DFFE(Equal_a183 & (!SH_ch_cnt_a0_a) # !Equal_a183 & !Equal_a184 & SH_ch_cnt_a0_a & SHDisc_MEN_a339, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1C0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a184,
	datab => Equal_a183,
	datac => SH_ch_cnt_a0_a,
	datad => SHDisc_MEN_a339,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SH_ch_cnt_a0_a);

SH_ch_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- SH_ch_cnt_a1_a = DFFE(Equal_a183 & (SH_ch_cnt_a1_a $ SH_ch_cnt_a0_a) # !Equal_a183 & SH_ch_cnt_a1_a & (!Auto_SHDisc_Proc_a0), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "486A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SH_ch_cnt_a1_a,
	datab => Equal_a183,
	datac => SH_ch_cnt_a0_a,
	datad => Auto_SHDisc_Proc_a0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SH_ch_cnt_a1_a);

SHDisc_MEN_a323_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a323 = !SH_ch_cnt_a0_a & !DiscF_Vec_a1_a & !SH_ch_cnt_a1_a & Disc_Dly_In_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SH_ch_cnt_a0_a,
	datab => DiscF_Vec_a1_a,
	datac => SH_ch_cnt_a1_a,
	datad => Disc_Dly_In_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a323);

SHDisc_MEN_a327_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a327 = SH_ch_cnt_a0_a & !DiscF_Vec_a1_a & !SH_ch_cnt_a1_a & Disc_Dly_In_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SH_ch_cnt_a0_a,
	datab => DiscF_Vec_a1_a,
	datac => SH_ch_cnt_a1_a,
	datad => Disc_Dly_In_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a327);

SHDisc_MEN_a331_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a331 = !SH_ch_cnt_a0_a & !DiscF_Vec_a1_a & SH_ch_cnt_a1_a & Disc_Dly_In_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SH_ch_cnt_a0_a,
	datab => DiscF_Vec_a1_a,
	datac => SH_ch_cnt_a1_a,
	datad => Disc_Dly_In_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a331);

SHDisc_MEN_a335_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a335 = SH_ch_cnt_a0_a & !DiscF_Vec_a1_a & SH_ch_cnt_a1_a & Disc_Dly_In_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SH_ch_cnt_a0_a,
	datab => DiscF_Vec_a1_a,
	datac => SH_ch_cnt_a1_a,
	datad => Disc_Dly_In_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a335);

DiscF_aI : apex20ke_lcell
-- Equation(s):
-- DiscF = DFFE(Disc_In_acombout # !Disc_Dly_In_a0_a & DiscF, GLOBAL(clkf_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Disc_Dly_In_a0_a,
	datac => DiscF,
	datad => Disc_In_acombout,
	clk => clkf_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DiscF);

Equal_a183_I : apex20ke_lcell
-- Equation(s):
-- Equal_a183 = !DiscF_Vec_a1_a & (Disc_Dly_In_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DiscF_Vec_a1_a,
	datad => Disc_Dly_In_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a183);

Equal_a184_I : apex20ke_lcell
-- Equation(s):
-- Equal_a184 = !SHDisc_MEN_a2_a_areg0 & !SHDisc_MEN_a1_a_areg0 & !SHDisc_MEN_a0_a_areg0 & !SHDisc_MEN_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a2_a_areg0,
	datab => SHDisc_MEN_a1_a_areg0,
	datac => SHDisc_MEN_a0_a_areg0,
	datad => SHDisc_MEN_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a184);

a_aUM_a0_aU0_amdly_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a5_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a5_a);

a_aUM_a0_aU0_amdly_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a7_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a7_a);

Disc_In_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In,
	combout => Disc_In_acombout);

clkf_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clkf,
	combout => clkf_acombout);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a $ (a_aUM_a2_aU0_acnt_cen_a1 & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a $ (a_aUM_a2_aU0_acnt_cen_a1 & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a $ (a_aUM_a2_aU0_acnt_cen_a1 & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F50A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datad => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a);

a_aUM_a2_aU0_aabort_a88_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aabort_a88 = (a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & SHDisc_MEN_a2_a_areg0) & 
-- CASCADE(a_aUM_a2_aU0_aabort_a93)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	datac => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	datad => SHDisc_MEN_a2_a_areg0,
	cascin => a_aUM_a2_aU0_aabort_a93,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a2_aU0_aabort_a88);

a_aUM_a0_aU0_acnt_cen_a46_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_acnt_cen_a46 = SHDisc_MEN_a0_a_areg0 & !a_aUM_a0_aU0_aabort_vec_a1_a & !a_aUM_a0_aU0_aabort_vec_a0_a & !a_aUM_a0_aU0_aabort_a88

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a0_a_areg0,
	datab => a_aUM_a0_aU0_aabort_vec_a1_a,
	datac => a_aUM_a0_aU0_aabort_vec_a0_a,
	datad => a_aUM_a0_aU0_aabort_a88,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_acnt_cen_a46);

a_aUM_a2_aU0_aabort_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aabort_vec_a0_a = DFFE(a_aUM_a2_aU0_aabort_a88, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a2_aU0_aabort_a88,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aabort_vec_a0_a);

a_aUM_a2_aU0_aabort_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aabort_vec_a1_a = DFFE(a_aUM_a2_aU0_aabort_vec_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a2_aU0_aabort_vec_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aabort_vec_a1_a);

a_aUM_a2_aU0_acnt_cen_a1_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_acnt_cen_a1 = !a_aUM_a2_aU0_aabort_a88 & SHDisc_MEN_a2_a_areg0 & !a_aUM_a2_aU0_aabort_vec_a0_a & !a_aUM_a2_aU0_aabort_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0004",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_aabort_a88,
	datab => SHDisc_MEN_a2_a_areg0,
	datac => a_aUM_a2_aU0_aabort_vec_a0_a,
	datad => a_aUM_a2_aU0_aabort_vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a2_aU0_acnt_cen_a1);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a $ (a_aUM_a2_aU0_acnt_cen_a1 & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

a_aUM_a2_aU0_amen_dly_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_amen_dly = DFFE(SHDisc_MEN_a2_a_areg0, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => SHDisc_MEN_a2_a_areg0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_amen_dly);

a_aUM_a2_aU0_acnt_clr_a10_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_acnt_clr_a10 = a_aUM_a2_aU0_aabort_a88 # !SHDisc_MEN_a2_a_areg0 & !a_aUM_a2_aU0_amen_dly

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SHDisc_MEN_a2_a_areg0,
	datac => a_aUM_a2_aU0_aabort_a88,
	datad => a_aUM_a2_aU0_amen_dly,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a2_aU0_acnt_clr_a10);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a $ (a_aUM_a2_aU0_acnt_cen_a1 & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a $ (a_aUM_a2_aU0_acnt_cen_a1 & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a $ (a_aUM_a2_aU0_acnt_cen_a1 & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

a_aUM_a2_aU0_aabort_a86_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aabort_a92 = a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & 
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	datac => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	datad => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a2_aU0_aabort_a86,
	cascout => a_aUM_a2_aU0_aabort_a92);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_acnt_cen_a1 $ (a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(a_aUM_a2_aU0_acnt_clr_a10) & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a $ (a_aUM_a2_aU0_acnt_cen_a1 & a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_acnt_cen_a1,
	datab => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cin => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a2_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cout => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aUM_a2_aU0_aabort_a89_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a2_aU0_aabort_a93 = (a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a & (a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)) & CASCADE(a_aUM_a2_aU0_aabort_a92)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	datac => a_aUM_a2_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cascin => a_aUM_a2_aU0_aabort_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a2_aU0_aabort_a89,
	cascout => a_aUM_a2_aU0_aabort_a93);

a_aUM_a3_aU0_aabort_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aabort_vec_a0_a = DFFE(a_aUM_a3_aU0_aabort_a88, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a3_aU0_aabort_a88,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aabort_vec_a0_a);

a_aUM_a3_aU0_aabort_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aabort_vec_a1_a = DFFE(a_aUM_a3_aU0_aabort_vec_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a3_aU0_aabort_vec_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aabort_vec_a1_a);

a_aUM_a3_aU0_acnt_cen_a1_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_acnt_cen_a1 = !a_aUM_a3_aU0_aabort_vec_a0_a & !a_aUM_a3_aU0_aabort_vec_a1_a & !a_aUM_a3_aU0_aabort_a88 & SHDisc_MEN_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_aabort_vec_a0_a,
	datab => a_aUM_a3_aU0_aabort_vec_a1_a,
	datac => a_aUM_a3_aU0_aabort_a88,
	datad => SHDisc_MEN_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a3_aU0_acnt_cen_a1);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

clks_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clks,
	combout => clks_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

a_aUM_a0_aU0_amen_dly_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amen_dly = DFFE(SHDisc_MEN_a0_a_areg0, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => SHDisc_MEN_a0_a_areg0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amen_dly);

a_aUM_a0_aU0_acnt_clr_a10_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_acnt_clr_a10 = a_aUM_a0_aU0_aabort_a88 # !SHDisc_MEN_a0_a_areg0 & !a_aUM_a0_aU0_amen_dly

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SHDisc_MEN_a0_a_areg0,
	datac => a_aUM_a0_aU0_aabort_a88,
	datad => a_aUM_a0_aU0_amen_dly,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_acnt_clr_a10);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_acnt_cen_a46 $ (a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a $ (a_aUM_a0_aU0_acnt_cen_a46 & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a $ (a_aUM_a0_aU0_acnt_cen_a46 & 
-- !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a $ (a_aUM_a0_aU0_acnt_cen_a46 & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a $ (a_aUM_a0_aU0_acnt_cen_a46 & 
-- !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a $ (a_aUM_a0_aU0_acnt_cen_a46 & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a $ (a_aUM_a0_aU0_acnt_cen_a46 & 
-- !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a $ (a_aUM_a0_aU0_acnt_cen_a46 & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(a_aUM_a0_aU0_acnt_clr_a10) & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a $ (a_aUM_a0_aU0_acnt_cen_a46 & 
-- !a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F50A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_acnt_cen_a46,
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	cin => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a0_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a);

a_aUM_a0_aU0_aabort_a86_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aabort_a92 = a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & 
-- a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	datac => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_aabort_a86,
	cascout => a_aUM_a0_aU0_aabort_a92);

a_aUM_a0_aU0_aabort_a89_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aabort_a93 = (a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a) & CASCADE(a_aUM_a0_aU0_aabort_a92)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cascin => a_aUM_a0_aU0_aabort_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_aabort_a89,
	cascout => a_aUM_a0_aU0_aabort_a93);

a_aUM_a0_aU0_aabort_a88_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aabort_a88 = (a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a & SHDisc_MEN_a0_a_areg0 & a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a) & 
-- CASCADE(a_aUM_a0_aU0_aabort_a93)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	datab => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	datac => SHDisc_MEN_a0_a_areg0,
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cascin => a_aUM_a0_aU0_aabort_a93,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_aabort_a88);

FIR_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FIR_MR,
	combout => FIR_MR_acombout);

SHDisc_MEN_a339_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a339 = !PA_Reset_acombout & !FIR_MR_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PA_Reset_acombout,
	datad => FIR_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a339);

HDisc_Out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_HDisc_Out,
	combout => HDisc_Out_acombout);

HDisc_Out_Del_aI : apex20ke_lcell
-- Equation(s):
-- HDisc_Out_Del = DFFE(HDisc_Out_acombout, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => HDisc_Out_acombout,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => HDisc_Out_Del);

Auto_SHDisc_Proc_a12_I : apex20ke_lcell
-- Equation(s):
-- Auto_SHDisc_Proc_a12 = !HDisc_Out_Del & (HDisc_Out_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => HDisc_Out_Del,
	datad => HDisc_Out_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Auto_SHDisc_Proc_a12);

H_ch_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- H_ch_cnt_a0_a = DFFE(H_ch_cnt_a0_a & !Equal_a184 & SHDisc_MEN_a339 & !Auto_SHDisc_Proc_a12 # !H_ch_cnt_a0_a & (Auto_SHDisc_Proc_a12), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F40",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a184,
	datab => SHDisc_MEN_a339,
	datac => H_ch_cnt_a0_a,
	datad => Auto_SHDisc_Proc_a12,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => H_ch_cnt_a0_a);

a_aUM_a1_aU0_aabort_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aabort_vec_a0_a = DFFE(a_aUM_a1_aU0_aabort_a88, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a1_aU0_aabort_a88,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aabort_vec_a0_a);

a_aUM_a1_aU0_aabort_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aabort_vec_a1_a = DFFE(a_aUM_a1_aU0_aabort_vec_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a1_aU0_aabort_vec_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aabort_vec_a1_a);

a_aUM_a1_aU0_acnt_cen_a1_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_acnt_cen_a1 = !a_aUM_a1_aU0_aabort_vec_a0_a & !a_aUM_a1_aU0_aabort_a88 & !a_aUM_a1_aU0_aabort_vec_a1_a & SHDisc_MEN_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_aabort_vec_a0_a,
	datab => a_aUM_a1_aU0_aabort_a88,
	datac => a_aUM_a1_aU0_aabort_vec_a1_a,
	datad => SHDisc_MEN_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a1_aU0_acnt_cen_a1);

a_aUM_a1_aU0_amen_dly_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_amen_dly = DFFE(SHDisc_MEN_a1_a_areg0, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => SHDisc_MEN_a1_a_areg0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_amen_dly);

a_aUM_a1_aU0_acnt_clr_a10_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_acnt_clr_a10 = a_aUM_a1_aU0_aabort_a88 # !a_aUM_a1_aU0_amen_dly & !SHDisc_MEN_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => a_aUM_a1_aU0_aabort_a88,
	datac => a_aUM_a1_aU0_amen_dly,
	datad => SHDisc_MEN_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a1_aU0_acnt_clr_a10);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_acnt_cen_a1 $ a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3CF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => a_aUM_a1_aU0_acnt_cen_a1,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a $ (a_aUM_a1_aU0_acnt_cen_a1 & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_acnt_cen_a1,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a $ (a_aUM_a1_aU0_acnt_cen_a1 & !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & (!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	datab => a_aUM_a1_aU0_acnt_cen_a1,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a $ (a_aUM_a1_aU0_acnt_cen_a1 & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_acnt_cen_a1,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a $ (a_aUM_a1_aU0_acnt_cen_a1 & !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & (!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	datab => a_aUM_a1_aU0_acnt_cen_a1,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a $ (a_aUM_a1_aU0_acnt_cen_a1 & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_acnt_cen_a1,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a $ (a_aUM_a1_aU0_acnt_cen_a1 & !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_acnt_cen_a1,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a $ (a_aUM_a1_aU0_acnt_cen_a1 & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_acnt_cen_a1,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(a_aUM_a1_aU0_acnt_clr_a10) & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a $ (!a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT & a_aUM_a1_aU0_acnt_cen_a1), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C3CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	datad => a_aUM_a1_aU0_acnt_cen_a1,
	cin => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a1_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a);

a_aUM_a1_aU0_aabort_a86_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aabort_a92 = a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a & 
-- a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	datac => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	datad => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a1_aU0_aabort_a86,
	cascout => a_aUM_a1_aU0_aabort_a92);

a_aUM_a1_aU0_aabort_a89_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aabort_a93 = (a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a) & CASCADE(a_aUM_a1_aU0_aabort_a92)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	datad => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cascin => a_aUM_a1_aU0_aabort_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a1_aU0_aabort_a89,
	cascout => a_aUM_a1_aU0_aabort_a93);

a_aUM_a1_aU0_aabort_a88_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a1_aU0_aabort_a88 = (a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a & SHDisc_MEN_a1_a_areg0) & 
-- CASCADE(a_aUM_a1_aU0_aabort_a93)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	datab => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	datac => a_aUM_a1_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	datad => SHDisc_MEN_a1_a_areg0,
	cascin => a_aUM_a1_aU0_aabort_a93,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a1_aU0_aabort_a88);

Equal_a182_I : apex20ke_lcell
-- Equation(s):
-- Equal_a182 = !HDisc_Out_Del & H_ch_cnt_a0_a & HDisc_Out_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => HDisc_Out_Del,
	datac => H_ch_cnt_a0_a,
	datad => HDisc_Out_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a182);

SHDisc_MEN_a328_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a328 = SHDisc_MEN_a327 # SHDisc_MEN_a1_a_areg0 & (H_ch_cnt_a1_a # !Equal_a182)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAEE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a327,
	datab => SHDisc_MEN_a1_a_areg0,
	datac => H_ch_cnt_a1_a,
	datad => Equal_a182,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a328);

SHDisc_MEN_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a1_a_areg0 = DFFE(!PA_Reset_acombout & !FIR_MR_acombout & !a_aUM_a1_aU0_aabort_a88 & SHDisc_MEN_a328, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => FIR_MR_acombout,
	datac => a_aUM_a1_aU0_aabort_a88,
	datad => SHDisc_MEN_a328,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MEN_a1_a_areg0);

Equal_a185_I : apex20ke_lcell
-- Equation(s):
-- Equal_a185 = !SHDisc_MEN_a0_a_areg0 & !SHDisc_MEN_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => SHDisc_MEN_a0_a_areg0,
	datad => SHDisc_MEN_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a185);

Equal_a181_I : apex20ke_lcell
-- Equation(s):
-- Equal_a181 = !HDisc_Out_Del & !H_ch_cnt_a0_a & HDisc_Out_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => HDisc_Out_Del,
	datac => H_ch_cnt_a0_a,
	datad => HDisc_Out_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a181);

SHDisc_MEN_a332_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a332 = SHDisc_MEN_a331 # SHDisc_MEN_a2_a_areg0 & (!Equal_a181 # !H_ch_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BAFA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a331,
	datab => H_ch_cnt_a1_a,
	datac => SHDisc_MEN_a2_a_areg0,
	datad => Equal_a181,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a332);

SHDisc_MEN_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a2_a_areg0 = DFFE(!a_aUM_a2_aU0_aabort_a88 & !PA_Reset_acombout & !FIR_MR_acombout & SHDisc_MEN_a332, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a2_aU0_aabort_a88,
	datab => PA_Reset_acombout,
	datac => FIR_MR_acombout,
	datad => SHDisc_MEN_a332,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MEN_a2_a_areg0);

Auto_SHDisc_Proc_a0_I : apex20ke_lcell
-- Equation(s):
-- Auto_SHDisc_Proc_a0 = !SHDisc_MEN_a3_a_areg0 & Equal_a185 & !SHDisc_MEN_a2_a_areg0 # !SHDisc_MEN_a339

-- pragma translate_off
GENERIC MAP (
	lut_mask => "04FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a3_a_areg0,
	datab => Equal_a185,
	datac => SHDisc_MEN_a2_a_areg0,
	datad => SHDisc_MEN_a339,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Auto_SHDisc_Proc_a0);

H_ch_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- H_ch_cnt_a1_a = DFFE(Auto_SHDisc_Proc_a12 & (H_ch_cnt_a1_a $ H_ch_cnt_a0_a) # !Auto_SHDisc_Proc_a12 & H_ch_cnt_a1_a & (!Auto_SHDisc_Proc_a0), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "660A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => H_ch_cnt_a1_a,
	datab => H_ch_cnt_a0_a,
	datac => Auto_SHDisc_Proc_a0,
	datad => Auto_SHDisc_Proc_a12,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => H_ch_cnt_a1_a);

SHDisc_MEN_a324_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a324 = SHDisc_MEN_a323 # SHDisc_MEN_a0_a_areg0 & (H_ch_cnt_a1_a # !Equal_a181)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAFA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a323,
	datab => H_ch_cnt_a1_a,
	datac => SHDisc_MEN_a0_a_areg0,
	datad => Equal_a181,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a324);

SHDisc_MEN_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a0_a_areg0 = DFFE(!PA_Reset_acombout & !a_aUM_a0_aU0_aabort_a88 & !FIR_MR_acombout & SHDisc_MEN_a324, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => a_aUM_a0_aU0_aabort_a88,
	datac => FIR_MR_acombout,
	datad => SHDisc_MEN_a324,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MEN_a0_a_areg0);

a_aUM_a3_aU0_amen_dly_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_amen_dly = DFFE(SHDisc_MEN_a3_a_areg0, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => SHDisc_MEN_a3_a_areg0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_amen_dly);

a_aUM_a3_aU0_acnt_clr_a10_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_acnt_clr_a10 = a_aUM_a3_aU0_aabort_a88 # !SHDisc_MEN_a3_a_areg0 & !a_aUM_a3_aU0_amen_dly

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SHDisc_MEN_a3_a_areg0,
	datac => a_aUM_a3_aU0_aabort_a88,
	datad => a_aUM_a3_aU0_amen_dly,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a3_aU0_acnt_clr_a10);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_acnt_cen_a1 $ (a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a $ (a_aUM_a3_aU0_acnt_cen_a1 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a $ (a_aUM_a3_aU0_acnt_cen_a1 & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a $ (a_aUM_a3_aU0_acnt_cen_a1 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a $ (a_aUM_a3_aU0_acnt_cen_a1 & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a $ (a_aUM_a3_aU0_acnt_cen_a1 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a $ (a_aUM_a3_aU0_acnt_cen_a1 & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a $ (a_aUM_a3_aU0_acnt_cen_a1 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	cout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a = DFFE(!a_aUM_a3_aU0_acnt_clr_a10 & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a $ (a_aUM_a3_aU0_acnt_cen_a1 & !a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F50A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_acnt_cen_a1,
	datad => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	cin => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	sclr => a_aUM_a3_aU0_acnt_clr_a10,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a);

a_aUM_a3_aU0_aabort_a86_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aabort_a92 = a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a & 
-- a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	datac => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	datad => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a3_aU0_aabort_a86,
	cascout => a_aUM_a3_aU0_aabort_a92);

a_aUM_a3_aU0_aabort_a89_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aabort_a93 = (a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a & (a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a)) & CASCADE(a_aUM_a3_aU0_aabort_a92)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	datac => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	cascin => a_aUM_a3_aU0_aabort_a92,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a3_aU0_aabort_a89,
	cascout => a_aUM_a3_aU0_aabort_a93);

a_aUM_a3_aU0_aabort_a88_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a3_aU0_aabort_a88 = (a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a & a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a & SHDisc_MEN_a3_a_areg0) & 
-- CASCADE(a_aUM_a3_aU0_aabort_a93)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	datab => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	datac => a_aUM_a3_aU0_aCnt_Counter_awysi_counter_asload_path_a7_a,
	datad => SHDisc_MEN_a3_a_areg0,
	cascin => a_aUM_a3_aU0_aabort_a93,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a3_aU0_aabort_a88);

SHDisc_MEN_a336_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a336 = SHDisc_MEN_a335 # SHDisc_MEN_a3_a_areg0 & (!Equal_a182 # !H_ch_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEEE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MEN_a335,
	datab => SHDisc_MEN_a3_a_areg0,
	datac => H_ch_cnt_a1_a,
	datad => Equal_a182,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MEN_a336);

SHDisc_MEN_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MEN_a3_a_areg0 = DFFE(!PA_Reset_acombout & !FIR_MR_acombout & !a_aUM_a3_aU0_aabort_a88 & SHDisc_MEN_a336, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => FIR_MR_acombout,
	datac => a_aUM_a3_aU0_aabort_a88,
	datad => SHDisc_MEN_a336,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MEN_a3_a_areg0);

a_aUM_a0_aU0_aabort_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aabort_vec_a0_a = DFFE(a_aUM_a0_aU0_aabort_a88, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aabort_a88,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aabort_vec_a0_a);

a_aUM_a0_aU0_aabort_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_aabort_vec_a1_a = DFFE(a_aUM_a0_aU0_aabort_vec_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_aabort_vec_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_aabort_vec_a1_a);

a_aUM_a0_aU0_amdly_en_a23_I : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_en_a23 = a_aUM_a0_aU0_amen_dly & !a_aUM_a0_aU0_aabort_vec_a1_a & !a_aUM_a0_aU0_aabort_vec_a0_a & !SHDisc_MEN_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amen_dly,
	datab => a_aUM_a0_aU0_aabort_vec_a1_a,
	datac => a_aUM_a0_aU0_aabort_vec_a0_a,
	datad => SHDisc_MEN_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aUM_a0_aU0_amdly_en_a23);

a_aUM_a0_aU0_amdly_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a0_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a0_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a0_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_amdly_ff_adffs_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a0_a);

a_aUM_a0_aU0_amdly_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a1_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a1_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a1_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a1_a = DFFE(!a_aUM_a0_aU0_amdly_ff_adffs_a1_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acout_a1_a = CARRY(a_aUM_a0_aU0_amdly_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => a_aUM_a0_aU0_amdly_ff_adffs_a1_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a1_a,
	cout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acout_a1_a);

a_aUM_a0_aU0_amdly_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a2_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a2_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a2_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a194 : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195 = SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acout_a1_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acout_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a2_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a2_a $ !SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a2_a,
	datad => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a2_a);

a_aUM_a0_aU0_amdly_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a3_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a3_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a3_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a3_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a3_a $ (a_aUM_a0_aU0_amdly_ff_adffs_a2_a # SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195), GLOBAL(clks_acombout), !GLOBAL(imr_acombout), 
-- , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "555A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a3_a,
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a2_a,
	datad => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a3_a);

a_aUM_a0_aU0_amdly_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a4_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a4_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a4_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a4_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a4_a $ (!a_aUM_a0_aU0_amdly_ff_adffs_a2_a & !SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195 # !a_aUM_a0_aU0_amdly_ff_adffs_a3_a), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9993",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a3_a,
	datab => a_aUM_a0_aU0_amdly_ff_adffs_a4_a,
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a2_a,
	datad => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a4_a);

SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a4_a # a_aUM_a0_aU0_amdly_ff_adffs_a3_a & (a_aUM_a0_aU0_amdly_ff_adffs_a2_a # SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EEEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a3_a,
	datab => a_aUM_a0_aU0_amdly_ff_adffs_a4_a,
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a2_a,
	datad => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_acs_buffer_a1_a_a195,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a);

SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a = DFFE(!a_aUM_a0_aU0_amdly_ff_adffs_a5_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acout_a0_a = CARRY(a_aUM_a0_aU0_amdly_ff_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "55AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a5_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a,
	cout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acout_a0_a);

SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a78_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a78 = SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a $ SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80 = CARRY(SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a & SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a5_a,
	datab => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a78,
	cout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80);

a_aUM_a0_aU0_amdly_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a6_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a6_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a6_a);

SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a136 : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137 = SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acout_a0_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acout_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137);

SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a6_a $ !SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a6_a,
	datad => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a);

SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a82_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a82 = SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a $ SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84 = CARRY(!SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80 # !SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a1_a,
	cin => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a80,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a82,
	cout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84);

SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a7_a $ (!a_aUM_a0_aU0_amdly_ff_adffs_a6_a & !SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137), GLOBAL(clks_acombout), 
-- !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a7_a,
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a6_a,
	datad => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a);

SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a86_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a86 = SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a $ !SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a88 = CARRY(SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a & !SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a2_a,
	cin => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a84,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a86,
	cout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a88);

a_aUM_a0_aU0_amdly_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- a_aUM_a0_aU0_amdly_ff_adffs_a8_a = DFFE(a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , a_aUM_a0_aU0_amdly_en_a23)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aUM_a0_aU0_aCnt_Counter_awysi_counter_asload_path_a8_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	ena => a_aUM_a0_aU0_amdly_en_a23,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => a_aUM_a0_aU0_amdly_ff_adffs_a8_a);

SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a = DFFE(a_aUM_a0_aU0_amdly_ff_adffs_a8_a $ (!a_aUM_a0_aU0_amdly_ff_adffs_a7_a & !a_aUM_a0_aU0_amdly_ff_adffs_a6_a & !SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137), 
-- GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCC9",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aUM_a0_aU0_amdly_ff_adffs_a7_a,
	datab => a_aUM_a0_aU0_amdly_ff_adffs_a8_a,
	datac => a_aUM_a0_aU0_amdly_ff_adffs_a6_a,
	datad => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_acs_buffer_a0_a_a137,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a);

SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a90_I : apex20ke_lcell
-- Equation(s):
-- SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a90 = SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a88 $ SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => SHDisc_MDly_Add_Sub_aadder1_0_a0_a_aresult_node_asout_node_a3_a,
	cin => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a88,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a90);

dd_wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!dd_wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a1_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a2_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a2_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a3_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a4_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a4_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a4_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a5_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a5_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a6_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a6_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a6_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a7_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a7_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a8_a = DFFE(dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT $ !dd_wadr_cntr_awysi_counter_asload_path_a8_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dd_wadr_cntr_awysi_counter_asload_path_a8_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a8_a);

dly_len_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(0),
	combout => dly_len_a0_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a208_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a208 = dd_wadr_cntr_awysi_counter_asload_path_a0_a $ dly_len_a0_a_acombout
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a0_a # !dly_len_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a0_a,
	datab => dly_len_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a208,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210);

dly_len_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(1),
	combout => dly_len_a1_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a212_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a212 = dd_wadr_cntr_awysi_counter_asload_path_a1_a $ dly_len_a1_a_acombout $ !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a1_a & dly_len_a1_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210 # !dd_wadr_cntr_awysi_counter_asload_path_a1_a & 
-- (dly_len_a1_a_acombout # !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	datab => dly_len_a1_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a210,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a212,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214);

dly_len_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(2),
	combout => dly_len_a2_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a216_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a216 = dd_wadr_cntr_awysi_counter_asload_path_a2_a $ dly_len_a2_a_acombout $ dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a2_a & (!dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214 # !dly_len_a2_a_acombout) # !dd_wadr_cntr_awysi_counter_asload_path_a2_a & 
-- !dly_len_a2_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	datab => dly_len_a2_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a214,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a216,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218);

dly_len_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(3),
	combout => dly_len_a3_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a220_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a220 = dd_wadr_cntr_awysi_counter_asload_path_a3_a $ dly_len_a3_a_acombout $ !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a3_a & dly_len_a3_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218 # !dd_wadr_cntr_awysi_counter_asload_path_a3_a & 
-- (dly_len_a3_a_acombout # !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	datab => dly_len_a3_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a218,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a220,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222);

dly_len_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(4),
	combout => dly_len_a4_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a224_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a224 = dd_wadr_cntr_awysi_counter_asload_path_a4_a $ dly_len_a4_a_acombout $ dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a4_a & (!dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222 # !dly_len_a4_a_acombout) # !dd_wadr_cntr_awysi_counter_asload_path_a4_a & 
-- !dly_len_a4_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	datab => dly_len_a4_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a222,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a224,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226);

dly_len_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(5),
	combout => dly_len_a5_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a228_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a228 = dd_wadr_cntr_awysi_counter_asload_path_a5_a $ dly_len_a5_a_acombout $ !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a5_a & dly_len_a5_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226 # !dd_wadr_cntr_awysi_counter_asload_path_a5_a & 
-- (dly_len_a5_a_acombout # !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	datab => dly_len_a5_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a226,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a228,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230);

dly_len_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(6),
	combout => dly_len_a6_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a232_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a232 = dd_wadr_cntr_awysi_counter_asload_path_a6_a $ dly_len_a6_a_acombout $ dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a6_a & (!dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230 # !dly_len_a6_a_acombout) # !dd_wadr_cntr_awysi_counter_asload_path_a6_a & 
-- !dly_len_a6_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	datab => dly_len_a6_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a230,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a232,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234);

dly_len_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(7),
	combout => dly_len_a7_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a236_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a236 = dd_wadr_cntr_awysi_counter_asload_path_a7_a $ dly_len_a7_a_acombout $ !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a238 = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a7_a & dly_len_a7_a_acombout & !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234 # !dd_wadr_cntr_awysi_counter_asload_path_a7_a & 
-- (dly_len_a7_a_acombout # !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	datab => dly_len_a7_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a234,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a236,
	cout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a238);

dly_len_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(8),
	combout => dly_len_a8_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a240_I : apex20ke_lcell
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a240 = dd_wadr_cntr_awysi_counter_asload_path_a8_a $ (dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a238 $ dly_len_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a8_a,
	datad => dly_len_a8_a_acombout,
	cin => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a238,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_a240);

dd_mem_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 9,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 511,
	logical_ram_depth => 512,
	logical_ram_name => "lpm_ram_dp:dd_mem|altdpram:sram|content",
	logical_ram_width => 2,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Disc_Dly_In_a0_a,
	clk0 => clks_acombout,
	clk1 => clks_acombout,
	we => VCC,
	re => VCC,
	waddr => dd_mem_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => dd_mem_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => dd_mem_asram_asegment_a0_a_a0_a_modesel,
	dataout => dd_mem_asram_aq_a0_a);

Disc_out_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_out_Vec_a0_a = DFFE(dd_mem_asram_aq_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => dd_mem_asram_aq_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_out_Vec_a0_a);

Disc_out_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_out_Vec_a1_a = DFFE(Disc_out_Vec_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_out_Vec_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_out_Vec_a1_a);

Disc_En_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En,
	combout => Disc_En_acombout);

Disc_Out_areg0_I : apex20ke_lcell
-- Equation(s):
-- Disc_Out_areg0 = DFFE(!PA_Reset_acombout & Disc_out_Vec_a0_a & !Disc_out_Vec_a1_a & Disc_En_acombout, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0400",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Disc_out_Vec_a0_a,
	datac => Disc_out_Vec_a1_a,
	datad => Disc_En_acombout,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Out_areg0);

SHDisc_MEN_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MEN_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MEN(0));

SHDisc_MEN_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MEN_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MEN(1));

SHDisc_MEN_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MEN_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MEN(2));

SHDisc_MEN_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MEN_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MEN(3));

SHDisc_MDly_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(0));

SHDisc_MDly_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(1));

SHDisc_MDly_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(2));

SHDisc_MDly_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(3));

SHDisc_MDly_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder0_a0_a_aresult_node_asout_node_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(4));

SHDisc_MDly_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a78,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(5));

SHDisc_MDly_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a82,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(6));

SHDisc_MDly_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a86,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(7));

SHDisc_MDly_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SHDisc_MDly_Add_Sub_aadder1_a0_a_aresult_node_acs_buffer_a0_a_a90,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SHDisc_MDly(8));

Disc_Out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Disc_Out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Disc_Out);
END structure;


