-- Copyright (C) 1991-2002 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 2.1 Build 189 09/05/2002 Service Pack 1 SJ Full Version"

-- DATE "05/19/2003 14:36:08"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for MODELSIM-ALTERA (VHDL OUTPUT FROM QUARTUS II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	disc_det IS
    PORT (
	clkf : IN std_logic;
	clks : IN std_logic;
	Disc_En : IN std_logic;
	Disc_In : IN std_logic_vector(1 DOWNTO 0);
	dly_len : IN std_logic_vector(8 DOWNTO 0);
	GMode : IN std_logic;
	imr : IN std_logic;
	PA_Reset : IN std_logic;
	Disc_Out : OUT std_logic_vector(1 DOWNTO 0)
	);
END disc_det;

ARCHITECTURE structure OF disc_det IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_clkf : std_logic;
SIGNAL ww_clks : std_logic;
SIGNAL ww_Disc_En : std_logic;
SIGNAL ww_Disc_In : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_dly_len : std_logic_vector(8 DOWNTO 0);
SIGNAL ww_GMode : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_Disc_Out : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_dd_mem_asram_asegment_a0_a_a0_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_dd_mem_asram_asegment_a0_a_a0_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_dd_mem_asram_asegment_a0_a_a1_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_dd_mem_asram_asegment_a0_a_a1_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL dd_mem_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL dd_mem_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Disc_En_acombout : std_logic;
SIGNAL Disc_In_a0_a_acombout : std_logic;
SIGNAL clkf_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL nx114 : std_logic;
SIGNAL DiscF : std_logic;
SIGNAL clks_acombout : std_logic;
SIGNAL Disc_Dly_In_0 : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL dd_wadr_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL dly_len_a0_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL dly_len_a1_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a0 : std_logic;
SIGNAL dly_len_a2_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a1 : std_logic;
SIGNAL dly_len_a3_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a2 : std_logic;
SIGNAL dly_len_a4_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a3 : std_logic;
SIGNAL dly_len_a5_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a4 : std_logic;
SIGNAL dly_len_a6_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a5 : std_logic;
SIGNAL dly_len_a7_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a6 : std_logic;
SIGNAL dly_len_a8_a_acombout : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL dd_mem_asram_aq_a0_a_a7 : std_logic;
SIGNAL dd_mem_asram_aq_a0_a : std_logic;
SIGNAL Disc_out_Vec_0 : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL Disc_out_Vec_1 : std_logic;
SIGNAL Disc_Out_dup0_0 : std_logic;
SIGNAL GMode_acombout : std_logic;
SIGNAL Disc_In_a1_a_acombout : std_logic;
SIGNAL Disc_Dly_In_1 : std_logic;
SIGNAL dd_mem_asram_aq_a1_a : std_logic;
SIGNAL Disc_Dly_out1_Del : std_logic;
SIGNAL nx51 : std_logic;
SIGNAL nx193 : std_logic;
SIGNAL Disc_Out_dup0_1 : std_logic;

BEGIN

ww_clkf <= clkf;
ww_clks <= clks;
ww_Disc_En <= Disc_En;
ww_Disc_In <= Disc_In;
ww_dly_len <= dly_len;
ww_GMode <= GMode;
ww_imr <= imr;
ww_PA_Reset <= PA_Reset;
Disc_Out <= ww_Disc_Out;

ww_dd_mem_asram_asegment_a0_a_a0_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_mem_asram_aq_a0_a_a7 & dd_mem_asram_aq_a0_a_a6 & dd_mem_asram_aq_a0_a_a5 & dd_mem_asram_aq_a0_a_a4 & dd_mem_asram_aq_a0_a_a3 & dd_mem_asram_aq_a0_a_a2 & 
dd_mem_asram_aq_a0_a_a1 & dd_mem_asram_aq_a0_a_a0 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a);

ww_dd_mem_asram_asegment_a0_a_a0_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_wadr_cntr_awysi_counter_asload_path_a8_a & dd_wadr_cntr_awysi_counter_asload_path_a7_a & dd_wadr_cntr_awysi_counter_asload_path_a6_a & 
dd_wadr_cntr_awysi_counter_asload_path_a5_a & dd_wadr_cntr_awysi_counter_asload_path_a4_a & dd_wadr_cntr_awysi_counter_asload_path_a3_a & dd_wadr_cntr_awysi_counter_asload_path_a2_a & dd_wadr_cntr_awysi_counter_asload_path_a1_a & 
dd_wadr_cntr_awysi_counter_asload_path_a0_a);

ww_dd_mem_asram_asegment_a0_a_a1_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_mem_asram_aq_a0_a_a7 & dd_mem_asram_aq_a0_a_a6 & dd_mem_asram_aq_a0_a_a5 & dd_mem_asram_aq_a0_a_a4 & dd_mem_asram_aq_a0_a_a3 & dd_mem_asram_aq_a0_a_a2 & 
dd_mem_asram_aq_a0_a_a1 & dd_mem_asram_aq_a0_a_a0 & dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a);

ww_dd_mem_asram_asegment_a0_a_a1_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & dd_wadr_cntr_awysi_counter_asload_path_a8_a & dd_wadr_cntr_awysi_counter_asload_path_a7_a & dd_wadr_cntr_awysi_counter_asload_path_a6_a & 
dd_wadr_cntr_awysi_counter_asload_path_a5_a & dd_wadr_cntr_awysi_counter_asload_path_a4_a & dd_wadr_cntr_awysi_counter_asload_path_a3_a & dd_wadr_cntr_awysi_counter_asload_path_a2_a & dd_wadr_cntr_awysi_counter_asload_path_a1_a & 
dd_wadr_cntr_awysi_counter_asload_path_a0_a);

Disc_En_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En,
	combout => Disc_En_acombout);

Disc_In_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(0),
	combout => Disc_In_a0_a_acombout);

clkf_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clkf,
	combout => clkf_acombout);

imr_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

ix40 : apex20ke_lcell 
-- Equation(s):
-- nx114 = Disc_Dly_In_0 # Disc_In_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Disc_Dly_In_0,
	datad => Disc_In_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx114);

reg_DiscF : apex20ke_lcell 
-- Equation(s):
-- DiscF = DFFE(Disc_In_a0_a_acombout, GLOBAL(clkf_acombout), !GLOBAL(imr_acombout), , nx114)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Disc_In_a0_a_acombout,
	clk => clkf_acombout,
	aclr => imr_acombout,
	ena => nx114,
	devclrn => devclrn,
	devpor => devpor,
	regout => DiscF);

clks_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clks,
	combout => clks_acombout);

reg_Disc_Dly_In_0 : apex20ke_lcell 
-- Equation(s):
-- Disc_Dly_In_0 = DFFE(DiscF, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => DiscF,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_Dly_In_0);

dd_wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!dd_wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a1_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a2_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a2_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a3_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a4_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a4_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a4_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a5_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a5_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a6_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a6_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a6_a & !dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a7_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a7_a $ dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )
-- dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !dd_wadr_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	cout => dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

dd_wadr_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- dd_wadr_cntr_awysi_counter_asload_path_a8_a = DFFE(dd_wadr_cntr_awysi_counter_asload_path_a8_a $ !dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dd_wadr_cntr_awysi_counter_asload_path_a8_a,
	cin => dd_wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dd_wadr_cntr_awysi_counter_asload_path_a8_a);

dly_len_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(0),
	combout => dly_len_a0_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a = dly_len_a0_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a0_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a0_a = CARRY(dd_wadr_cntr_awysi_counter_asload_path_a0_a # !dly_len_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "66DD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a0_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a0_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a0_a);

dly_len_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(1),
	combout => dly_len_a1_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a = dly_len_a1_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a1_a $ dd_radr_add_sub_aadder_aresult_node_acout_a0_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a1_a = CARRY(dly_len_a1_a_acombout & (!dd_radr_add_sub_aadder_aresult_node_acout_a0_a # !dd_wadr_cntr_awysi_counter_asload_path_a1_a) # !dly_len_a1_a_acombout & !dd_wadr_cntr_awysi_counter_asload_path_a1_a & !dd_radr_add_sub_aadder_aresult_node_acout_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a1_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a1_a);

dd_mem_asram_aq_a0_a_a0_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a0 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a0);

dly_len_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(2),
	combout => dly_len_a2_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a = dly_len_a2_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a2_a $ !dd_radr_add_sub_aadder_aresult_node_acout_a1_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a2_a = CARRY(dly_len_a2_a_acombout & dd_wadr_cntr_awysi_counter_asload_path_a2_a & !dd_radr_add_sub_aadder_aresult_node_acout_a1_a # !dly_len_a2_a_acombout & (dd_wadr_cntr_awysi_counter_asload_path_a2_a # !dd_radr_add_sub_aadder_aresult_node_acout_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a2_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a2_a);

dd_mem_asram_aq_a0_a_a1_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a1 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a1);

dly_len_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(3),
	combout => dly_len_a3_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a = dly_len_a3_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a3_a $ dd_radr_add_sub_aadder_aresult_node_acout_a2_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a3_a = CARRY(dly_len_a3_a_acombout & (!dd_radr_add_sub_aadder_aresult_node_acout_a2_a # !dd_wadr_cntr_awysi_counter_asload_path_a3_a) # !dly_len_a3_a_acombout & !dd_wadr_cntr_awysi_counter_asload_path_a3_a & !dd_radr_add_sub_aadder_aresult_node_acout_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a3_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a3_a);

dd_mem_asram_aq_a0_a_a2_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a2 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a2);

dly_len_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(4),
	combout => dly_len_a4_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a = dly_len_a4_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a4_a $ !dd_radr_add_sub_aadder_aresult_node_acout_a3_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a4_a = CARRY(dly_len_a4_a_acombout & dd_wadr_cntr_awysi_counter_asload_path_a4_a & !dd_radr_add_sub_aadder_aresult_node_acout_a3_a # !dly_len_a4_a_acombout & (dd_wadr_cntr_awysi_counter_asload_path_a4_a # !dd_radr_add_sub_aadder_aresult_node_acout_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a4_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a4_a);

dd_mem_asram_aq_a0_a_a3_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a3 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a3);

dly_len_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(5),
	combout => dly_len_a5_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a = dly_len_a5_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a5_a $ dd_radr_add_sub_aadder_aresult_node_acout_a4_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a5_a = CARRY(dly_len_a5_a_acombout & (!dd_radr_add_sub_aadder_aresult_node_acout_a4_a # !dd_wadr_cntr_awysi_counter_asload_path_a5_a) # !dly_len_a5_a_acombout & !dd_wadr_cntr_awysi_counter_asload_path_a5_a & !dd_radr_add_sub_aadder_aresult_node_acout_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a5_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a5_a);

dd_mem_asram_aq_a0_a_a4_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a4 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a4);

dly_len_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(6),
	combout => dly_len_a6_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a = dly_len_a6_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a6_a $ !dd_radr_add_sub_aadder_aresult_node_acout_a5_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a6_a = CARRY(dly_len_a6_a_acombout & dd_wadr_cntr_awysi_counter_asload_path_a6_a & !dd_radr_add_sub_aadder_aresult_node_acout_a5_a # !dly_len_a6_a_acombout & (dd_wadr_cntr_awysi_counter_asload_path_a6_a # !dd_radr_add_sub_aadder_aresult_node_acout_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a6_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a6_a);

dd_mem_asram_aq_a0_a_a5_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a5 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a5);

dly_len_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(7),
	combout => dly_len_a7_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a = dly_len_a7_a_acombout $ dd_wadr_cntr_awysi_counter_asload_path_a7_a $ dd_radr_add_sub_aadder_aresult_node_acout_a6_a
-- dd_radr_add_sub_aadder_aresult_node_acout_a7_a = CARRY(dly_len_a7_a_acombout & (!dd_radr_add_sub_aadder_aresult_node_acout_a6_a # !dd_wadr_cntr_awysi_counter_asload_path_a7_a) # !dly_len_a7_a_acombout & !dd_wadr_cntr_awysi_counter_asload_path_a7_a & !dd_radr_add_sub_aadder_aresult_node_acout_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dly_len_a7_a_acombout,
	datab => dd_wadr_cntr_awysi_counter_asload_path_a7_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	cout => dd_radr_add_sub_aadder_aresult_node_acout_a7_a);

dd_mem_asram_aq_a0_a_a6_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a6 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a6);

dly_len_ibuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_dly_len(8),
	combout => dly_len_a8_a_acombout);

dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a = dly_len_a8_a_acombout $ dd_radr_add_sub_aadder_aresult_node_acout_a7_a $ !dd_wadr_cntr_awysi_counter_asload_path_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CC3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => dly_len_a8_a_acombout,
	datad => dd_wadr_cntr_awysi_counter_asload_path_a8_a,
	cin => dd_radr_add_sub_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a);

dd_mem_asram_aq_a0_a_a7_I : apex20ke_lcell 
-- Equation(s):
-- dd_mem_asram_aq_a0_a_a7 = !dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => dd_radr_add_sub_aadder_aresult_node_acs_buffer_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => dd_mem_asram_aq_a0_a_a7);

dd_mem_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "LPM_RAM_DP:dd_mem|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 2,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Disc_Dly_In_0,
	clk0 => clks_acombout,
	clk1 => clks_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_dd_mem_asram_asegment_a0_a_a0_a_waddress,
	raddr => ww_dd_mem_asram_asegment_a0_a_a0_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => dd_mem_asram_asegment_a0_a_a0_a_modesel,
	dataout => dd_mem_asram_aq_a0_a);

reg_Disc_out_Vec_0 : apex20ke_lcell 
-- Equation(s):
-- Disc_out_Vec_0 = DFFE(dd_mem_asram_aq_a0_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => dd_mem_asram_aq_a0_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_out_Vec_0);

PA_Reset_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

reg_Disc_out_Vec_1 : apex20ke_lcell 
-- Equation(s):
-- Disc_out_Vec_1 = DFFE(Disc_out_Vec_0, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Disc_out_Vec_0,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_out_Vec_1);

reg_Disc_Out_0 : apex20ke_lcell 
-- Equation(s):
-- Disc_Out_dup0_0 = DFFE(Disc_En_acombout & Disc_out_Vec_0 & !PA_Reset_acombout & !Disc_out_Vec_1, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_acombout,
	datab => Disc_out_Vec_0,
	datac => PA_Reset_acombout,
	datad => Disc_out_Vec_1,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_Out_dup0_0);

GMode_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_GMode,
	combout => GMode_acombout);

Disc_In_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(1),
	combout => Disc_In_a1_a_acombout);

reg_Disc_Dly_In_1 : apex20ke_lcell 
-- Equation(s):
-- Disc_Dly_In_1 = DFFE(Disc_In_a1_a_acombout, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Disc_In_a1_a_acombout,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_Dly_In_1);

dd_mem_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "LPM_RAM_DP:dd_mem|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 512,
	logical_ram_width => 2,
	address_width => 9,
	first_address => 0,
	last_address => 511,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Disc_Dly_In_1,
	clk0 => clks_acombout,
	clk1 => clks_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_dd_mem_asram_asegment_a0_a_a1_a_waddress,
	raddr => ww_dd_mem_asram_asegment_a0_a_a1_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => dd_mem_asram_asegment_a0_a_a1_a_modesel,
	dataout => dd_mem_asram_aq_a1_a);

reg_Disc_Dly_out1_Del : apex20ke_lcell 
-- Equation(s):
-- Disc_Dly_out1_Del = DFFE(dd_mem_asram_aq_a1_a, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => dd_mem_asram_aq_a1_a,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_Dly_out1_Del);

ix49 : apex20ke_lcell 
-- Equation(s):
-- nx51 = !Disc_Dly_out1_Del & dd_mem_asram_aq_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => Disc_Dly_out1_Del,
	datad => dd_mem_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => nx51);

ix48 : apex20ke_lcell 
-- Equation(s):
-- nx193 = (Disc_En_acombout & !PA_Reset_acombout & GMode_acombout) & CASCADE(nx51)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0A00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_acombout,
	datac => PA_Reset_acombout,
	datad => GMode_acombout,
	cascin => nx51,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx193);

reg_Disc_Out_1 : apex20ke_lcell 
-- Equation(s):
-- Disc_Out_dup0_1 = DFFE(nx193, GLOBAL(clks_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => nx193,
	clk => clks_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Disc_Out_dup0_1);

Disc_Out_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Disc_Out_dup0_0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Disc_Out(0));

Disc_Out_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Disc_Out_dup0_1,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Disc_Out(1));
END structure;


