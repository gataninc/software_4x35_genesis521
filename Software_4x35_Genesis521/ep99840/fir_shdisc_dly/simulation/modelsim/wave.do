onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk40
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/pa_reset
add wave -noupdate -format Logic -radix binary /tb/fir_mr
add wave -noupdate -format Logic -radix binary /tb/gmode
add wave -noupdate -format Logic -radix binary /tb/hdisc_out
add wave -noupdate -format Literal -radix binary -expand /tb/shdisc_men
add wave -noupdate -format Literal -radix hexadecimal /tb/dly_len
add wave -noupdate -format Literal -radix hexadecimal -expand /tb/disc_in
add wave -noupdate -format Literal -radix hexadecimal -expand /tb/disc_out
add wave -noupdate -format Literal -radix hexadecimal /tb/shdisc_mdly
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {13432 ns} {10000 ns}
WaveRestoreZoom {0 ns} {42 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
