-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_disc_det.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules the Disc_Outputs and generates an Edge Detect for
--		Those that are enabled and not blocked.
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
-------------------------------------------------------------------------------
	signal IMR		: std_logic := '1';
	signal CLK40		: std_logic := '0';
	signal CLK20		: std_logic := '0';
	signal PA_Reset	: std_logic := '0';
	signal DLY_LEN 	: std_logic_vector( 8 downto 0 );
	signal Disc_In		: std_logic_Vector( 1 downto 0 );
	signal Disc_out	: std_logic_Vector( 1 downto 0 );
	signal GMode		: std_logic;
	signal Disc_En		: std_logic;
	signal disc_blk	: std_logic;
	signal HDisc_Out	: std_logic;
	signal SHDisc_MEN	: std_logic_vector( 3 downto 0 );
	signal SHDisc_MDly 	: std_logic_vector( 8 downto 0 );		
	signal HDisc_Trig	: std_logic;
	signal fir_mr		: std_logic;

	
	Type array1 Is array (127 downto 0) of std_logic;
	
	signal HDisc_Dly 	: array1;
	signal wadr 		: std_logic_vector( 6 downto 0 ) := "0000000";
	signal radr 		: std_logic_vector( 6 downto 0 );
	signal auto_dly	: std_logic;
	
	component fir_shdisc_dly 
		port( 
		     imr            : in      std_logic;                    -- Master Reset
			clkf           : in      std_logic;			     -- Master Clock Input
			clks			: in		std_logic;
			GMode		: in		std_logic;
			PA_Reset		: in		std_logic;
			fir_mr		: in		std_logic;
			auto_dly		: in		std_logic;
			dly_len		: in		std_logic_Vector( 8 downto 0 );
			Disc_En		: in		std_logic;
			HDisc_Out		: in		std_logic;
		     Disc_In        : in      std_logic_vector( 1 downto 0 );
			SHDisc_MEN	: out	std_logic_vector( 3 downto 0 );
			SHDisc_MDly 	: out	std_logic_vector( 8 downto 0 );		
		     Disc_Out       : out	std_logic_vector( 1 downto 0 ) );
	end component fir_shdisc_dly;

begin
	IMR 		<= '0' after 1 us;
	CLK40 	<= not( CLK40 ) after 12.5 ns;
	CLK20 	<= not( CLK20 ) after 25 ns;
	DLY_LEN	<= conv_std_logic_Vector( 49, 9 );

	GMode 	<= '1';
	Disc_En 	<= '1';
	PA_Reset	<= '0';
	auto_dly	<= '1';
	fir_mr	<= '0';
	
	U : fir_shdisc_dly 
		port map(
			IMR 			=> IMR,
			CLKF			=> CLK40,
			CLKS			=> CLK20,
			GMode		=> GMode,
			PA_Reset		=> PA_Reset,
			fir_mr		=> fir_mr,
			auto_dly		=> Auto_Dly,
			DLY_LEN		=> DLY_LEN,
			Disc_En		=> Disc_En,
			HDisc_Out		=> HDisc_Out,
			Disc_In		=> Disc_In,
			SHDisc_MEN	=> SHDisc_MEN,
			SHDisc_MDly 	=> SHDisc_MDly,
			Disc_out		=> Disc_out );
			

     --------------------------------------------------------------------------
	Disc0_Proc : process
	begin
		Disc_In(0) <= '0';
		wait for 10 us;
		
		Disc_In(0) <= '1';
		wait for 680 ns;
		Disc_In(0) <= '0';
		wait for 870 ns;
		Disc_In(0) <= '1';
		wait for 1.16 us;
		Disc_In(0) <= '0';
		
		wait for 620 ns;

		Disc_In(0) <= '1';
		wait for 680 ns;
		Disc_In(0) <= '0';
		wait for 870 ns;
		Disc_In(0) <= '1';
		wait for 1.16 us;
		Disc_In(0) <= '0';

		wait for 15 us;		
		Disc_In(0) <= '1';
		wait for 680 ns;
		Disc_In(0) <= '0';
		wait for 870 ns;
		Disc_In(0) <= '1';
		wait for 1.16 us;
		Disc_In(0) <= '0';

		wait for 15 us;		
	end process;
	
     --------------------------------------------------------------------------
	
     --------------------------------------------------------------------------
	Disc1_Proc : process
	begin
		Disc_In(1) <= '0';
		wait for 10 us;
		Disc_in(1) <= '0';
		wait for 3 us;
		Disc_in(1) <= '0';
		
		wait for 620 ns;
		Disc_in(1) <= '1';
		wait for 2.2 us;
		Disc_in(1) <= '0';
		
		
		wait for 30 us;
	end process;
     --------------------------------------------------------------------------


	HDisc_Proc : process
	begin
		HDisc_Trig <= '0';
		wait until (( Disc_In(0)'Event ) and ( Disc_In(0) = '1' ));
		wait until (( CLK20'event ) and ( Clk20 = '1' ));
		HDisc_Trig <= '1';
		wait until (( CLK20'event ) and ( Clk20 = '1' ));
	end process;
	

	radr <= wadr - 58;
	
	
	clock_proc : process( clk20 )
	begin
		if(( clk20'event ) and ( clk20 = '1' )) then
			HDisc_Dly( conv_integer(wadr)) 	<= HDisc_Trig;
			HDisc_Out 		<= HDisc_Dly( conv_integer(radr) );	
			wadr 			<= wadr + 1;
		end if;
	end process;
	
-------------------------------------------------------------------------------
end test_bench;               -- Disc_Det
-------------------------------------------------------------------------------