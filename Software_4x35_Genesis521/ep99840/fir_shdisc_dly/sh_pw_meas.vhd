-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_shdisc_dly.vhd
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules the Disc_Outputs and generates an Edge Detect for
--		Those that are enabled and not blocked.
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity SH_PW_Meas is 
	port( 
	     imr            : in      std_logic;                    -- Master Reset
		clk			: in		std_logic;
		Ext_abort		: in		std_logic;
		Men			: in		std_logic;
		abort		: buffer	std_logic;
		MDly 		: buffer	std_logic_vector( 8 downto 0 ));		
end SH_PW_Meas;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of SH_PW_Meas is

	signal men_dly		: std_logic;
	signal Cnt		: std_logic_Vector( 8 downto 0 );
	signal cnt_clr		: std_logic;
	signal cnt_cen		: std_logic;
	signal mdly_en		: std_logic;
	signal abort_vec	: std_logic_vector( 1 downto 0 );
	
begin
	cnt_clr	<= '1' when (( men   = '0' ) and ( men_dly = '0' )) else
			   '1' when ( abort = '1' ) else
			   '0';
			
	cnt_cen	<= '1' when (( abort = '0' ) and ( abort_Vec = "00" ) and ( men = '1' )) else '0';
	
	abort	<= '1' when (( men   = '1' ) and ( cnt = "1" & x"FF" )) else '0';
	
	Cnt_Counter : lpm_counter
		generic map(
			LPM_WIDTH	=> 9,
			LPM_TYPE 	=> "LPM_COUNTER" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			sclr		=> cnt_clr,
			cnt_en	=> cnt_cen,
			q		=> cnt );

	mdly_en	<= '1' when (  ( abort 		= '0'  ) 
					 and ( abort_vec 	= "00" )
					 and ( men 		= '0'  ) 
					 and ( men_dly 	= '1'  )) else '0';

	mdly_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 9,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			enable	=> mdly_en,
			data		=> cnt,
			q		=> mdly );
	
	--------------------------------------------------------------------
	clk_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			men_dly		<= '0';
			abort_vec		<= "00";

		elsif(( clk'event ) and ( clk = '1' )) then
			men_dly 		<= men;
			abort_Vec 	<= abort_vec(0) & abort;
		end if;
	end process;
-------------------------------------------------------------------------------
end behavioral;               -- SH_PW_Meas 
-------------------------------------------------------------------------------