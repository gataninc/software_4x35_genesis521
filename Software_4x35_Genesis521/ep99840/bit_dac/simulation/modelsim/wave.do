onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/bit_en
add wave -noupdate -format Logic -radix hexadecimal /tb/time_enable
add wave -noupdate -format Literal -radix hexadecimal /tb/peak1
add wave -noupdate -format Literal -radix hexadecimal /tb/peak2
add wave -noupdate -format Literal -radix hexadecimal /tb/cps
add wave -noupdate -format Literal -radix hexadecimal /tb/pk2dly
add wave -noupdate -format literal /tb/u/BIT_STATE
add wave -noupdate -format Logic -radix hexadecimal /tb/ld_bit_dac
add wave -noupdate -format Logic -radix hexadecimal /tb/bit_dac_data
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.0050000000000000001 /tb/dac
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {15323604 ps} {103930746 ps}
WaveRestoreZoom {0 ps} {525 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
