-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:17:29"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	bit_dac IS
    PORT (
	IMR : IN std_logic;
	clk20 : IN std_logic;
	BIT_EN : IN std_logic;
	Time_Enable : IN std_logic;
	peak1 : IN std_logic_vector(11 DOWNTO 0);
	peak2 : IN std_logic_vector(11 DOWNTO 0);
	cps : IN std_logic_vector(15 DOWNTO 0);
	pk2dly : IN std_logic_vector(15 DOWNTO 0);
	LD_bit_dac : OUT std_logic;
	bit_dac_DATA : OUT std_logic_vector(15 DOWNTO 0)
	);
END bit_dac;

ARCHITECTURE structure OF bit_dac IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_clk20 : std_logic;
SIGNAL ww_BIT_EN : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_peak1 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_peak2 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_cps : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_pk2dly : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_LD_bit_dac : std_logic;
SIGNAL ww_bit_dac_DATA : std_logic_vector(15 DOWNTO 0);
SIGNAL DNL_DAC_VAL_a0_a : std_logic;
SIGNAL DNL_DAC_VAL_a1_a : std_logic;
SIGNAL DNL_DAC_VAL_a4_a : std_logic;
SIGNAL iBIT_VAL_a9_a : std_logic;
SIGNAL iBIT_VAL_a10_a_a925 : std_logic;
SIGNAL iBIT_VAL_a11_a_a928 : std_logic;
SIGNAL iBIT_VAL_a11_a : std_logic;
SIGNAL iBIT_VAL_a12_a_a931 : std_logic;
SIGNAL iBIT_VAL_a12_a : std_logic;
SIGNAL iBIT_VAL_a13_a_a934 : std_logic;
SIGNAL iBIT_VAL_a13_a : std_logic;
SIGNAL iBIT_VAL_a14_a_a937 : std_logic;
SIGNAL iBIT_VAL_a14_a : std_logic;
SIGNAL iBIT_VAL_a15_a : std_logic;
SIGNAL BIT_CNT_a9_a : std_logic;
SIGNAL Equal_a670 : std_logic;
SIGNAL BIT_CNT_a0_a : std_logic;
SIGNAL Equal_a674 : std_logic;
SIGNAL BIT_CNT_a6_a : std_logic;
SIGNAL us1_cnt_a4_a : std_logic;
SIGNAL us1_cnt_a3_a : std_logic;
SIGNAL Equal_a679 : std_logic;
SIGNAL Equal_a682 : std_logic;
SIGNAL Equal_a689 : std_logic;
SIGNAL DNL_CNT_a8_a : std_logic;
SIGNAL DNL_CNT_a3_a : std_logic;
SIGNAL Select_a1890 : std_logic;
SIGNAL add_a1567 : std_logic;
SIGNAL add_a1575 : std_logic;
SIGNAL add_a1576 : std_logic;
SIGNAL add_a1577 : std_logic;
SIGNAL add_a1583 : std_logic;
SIGNAL add_a1581 : std_logic;
SIGNAL add_a1595 : std_logic;
SIGNAL add_a1601 : std_logic;
SIGNAL add_a1617 : std_logic;
SIGNAL Select_a1901 : std_logic;
SIGNAL Select_a1902 : std_logic;
SIGNAL UPD_PROC_a5 : std_logic;
SIGNAL Equal_a704 : std_logic;
SIGNAL Equal_a707 : std_logic;
SIGNAL pk2dly_a4_a_acombout : std_logic;
SIGNAL pk2dly_a9_a_acombout : std_logic;
SIGNAL pk2dly_a8_a_acombout : std_logic;
SIGNAL pk2dly_a12_a_acombout : std_logic;
SIGNAL pk2dly_a5_a_acombout : std_logic;
SIGNAL cps_a9_a_acombout : std_logic;
SIGNAL cps_a8_a_acombout : std_logic;
SIGNAL cps_a14_a_acombout : std_logic;
SIGNAL cps_a6_a_acombout : std_logic;
SIGNAL peak1_a2_a_acombout : std_logic;
SIGNAL peak1_a10_a_acombout : std_logic;
SIGNAL peak2_a11_a_acombout : std_logic;
SIGNAL BIT_EN_acombout : std_logic;
SIGNAL peak1_a0_a_acombout : std_logic;
SIGNAL peak1_a11_a_acombout : std_logic;
SIGNAL peak1_a9_a_acombout : std_logic;
SIGNAL peak1_a5_a_acombout : std_logic;
SIGNAL peak1_a8_a_acombout : std_logic;
SIGNAL peak1_a7_a_acombout : std_logic;
SIGNAL UPD_PROC_a209 : std_logic;
SIGNAL UPD_PROC_a210 : std_logic;
SIGNAL peak2_a3_a_acombout : std_logic;
SIGNAL peak2_a2_a_acombout : std_logic;
SIGNAL peak2_a0_a_acombout : std_logic;
SIGNAL peak2_a5_a_acombout : std_logic;
SIGNAL peak2_a4_a_acombout : std_logic;
SIGNAL peak2_a6_a_acombout : std_logic;
SIGNAL peak2_a9_a_acombout : std_logic;
SIGNAL peak2_a8_a_acombout : std_logic;
SIGNAL peak2_a10_a_acombout : std_logic;
SIGNAL Equal_a710 : std_logic;
SIGNAL Equal_a711 : std_logic;
SIGNAL Equal_a706 : std_logic;
SIGNAL clk20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL Bit_Disable : std_logic;
SIGNAL UPD_PROC_a2 : std_logic;
SIGNAL a_aGND : std_logic;
SIGNAL add_a1589 : std_logic;
SIGNAL us1_cnt_a0_a : std_logic;
SIGNAL add_a1591 : std_logic;
SIGNAL add_a1585 : std_logic;
SIGNAL us1_cnt_a1_a : std_logic;
SIGNAL add_a1587 : std_logic;
SIGNAL add_a1593 : std_logic;
SIGNAL us1_cnt_a2_a : std_logic;
SIGNAL Equal_a680 : std_logic;
SIGNAL Select_a1886 : std_logic;
SIGNAL peak1_a3_a_acombout : std_logic;
SIGNAL peak1_a4_a_acombout : std_logic;
SIGNAL peak1_a1_a_acombout : std_logic;
SIGNAL UPD_PROC_a208 : std_logic;
SIGNAL DNL_TEST : std_logic;
SIGNAL BIT_CNT_a1_a : std_logic;
SIGNAL pk2dly_a1_a_acombout : std_logic;
SIGNAL Equal_a669 : std_logic;
SIGNAL pk2dly_a2_a_acombout : std_logic;
SIGNAL pk2dly_a0_a_acombout : std_logic;
SIGNAL Equal_a671 : std_logic;
SIGNAL BIT_CNT_a4_a_a148 : std_logic;
SIGNAL BIT_CNT_a5_a : std_logic;
SIGNAL BIT_CNT_a5_a_a169 : std_logic;
SIGNAL BIT_CNT_a6_a_a187 : std_logic;
SIGNAL BIT_CNT_a7_a_a181 : std_logic;
SIGNAL BIT_CNT_a8_a : std_logic;
SIGNAL BIT_CNT_a8_a_a151 : std_logic;
SIGNAL BIT_CNT_a9_a_a154 : std_logic;
SIGNAL BIT_CNT_a10_a : std_logic;
SIGNAL pk2dly_a10_a_acombout : std_logic;
SIGNAL pk2dly_a11_a_acombout : std_logic;
SIGNAL Equal_a672 : std_logic;
SIGNAL Equal_a673 : std_logic;
SIGNAL BIT_CNT_a10_a_a163 : std_logic;
SIGNAL BIT_CNT_a11_a : std_logic;
SIGNAL BIT_CNT_a11_a_a166 : std_logic;
SIGNAL BIT_CNT_a12_a : std_logic;
SIGNAL BIT_CNT_a12_a_a172 : std_logic;
SIGNAL BIT_CNT_a13_a : std_logic;
SIGNAL BIT_CNT_a13_a_a184 : std_logic;
SIGNAL BIT_CNT_a14_a : std_logic;
SIGNAL BIT_CNT_a14_a_a190 : std_logic;
SIGNAL BIT_CNT_a15_a : std_logic;
SIGNAL pk2dly_a3_a_acombout : std_logic;
SIGNAL pk2dly_a15_a_acombout : std_logic;
SIGNAL Equal_a675 : std_logic;
SIGNAL BIT_CNT_a7_a : std_logic;
SIGNAL pk2dly_a7_a_acombout : std_logic;
SIGNAL pk2dly_a13_a_acombout : std_logic;
SIGNAL Equal_a676 : std_logic;
SIGNAL pk2dly_a14_a_acombout : std_logic;
SIGNAL pk2dly_a6_a_acombout : std_logic;
SIGNAL Equal_a677 : std_logic;
SIGNAL Equal_a678 : std_logic;
SIGNAL BIT_STATE_ast_pk2 : std_logic;
SIGNAL bit_dac_DATA_a46 : std_logic;
SIGNAL Select_a1896 : std_logic;
SIGNAL Select_a1897 : std_logic;
SIGNAL Select_a1898 : std_logic;
SIGNAL cps_a5_a_acombout : std_logic;
SIGNAL cps_a12_a_acombout : std_logic;
SIGNAL Equal_a686 : std_logic;
SIGNAL cps_a7_a_acombout : std_logic;
SIGNAL cps_a13_a_acombout : std_logic;
SIGNAL Equal_a688 : std_logic;
SIGNAL cps_a15_a_acombout : std_logic;
SIGNAL cps_a3_a_acombout : std_logic;
SIGNAL Equal_a687 : std_logic;
SIGNAL Equal_a690 : std_logic;
SIGNAL PK2_MODE : std_logic;
SIGNAL Select_a1894 : std_logic;
SIGNAL UPD_PROC_a207 : std_logic;
SIGNAL BIT_STATE_ast_pk2d : std_logic;
SIGNAL Equal_a691 : std_logic;
SIGNAL Select_a1899 : std_logic;
SIGNAL BIT_STATE_ast_idle : std_logic;
SIGNAL BIT_STATE_ast_dnl0 : std_logic;
SIGNAL BIT_STATE_ast_dnl1 : std_logic;
SIGNAL Select_a1888 : std_logic;
SIGNAL BIT_STATE_ast_dnl2 : std_logic;
SIGNAL BIT_STATE_ast_dnl3 : std_logic;
SIGNAL reduce_or_a25 : std_logic;
SIGNAL BIT_CNT_a0_a_a157 : std_logic;
SIGNAL BIT_CNT_a1_a_a145 : std_logic;
SIGNAL BIT_CNT_a2_a : std_logic;
SIGNAL BIT_CNT_a2_a_a160 : std_logic;
SIGNAL BIT_CNT_a3_a : std_logic;
SIGNAL BIT_CNT_a3_a_a178 : std_logic;
SIGNAL BIT_CNT_a4_a : std_logic;
SIGNAL cps_a1_a_acombout : std_logic;
SIGNAL cps_a4_a_acombout : std_logic;
SIGNAL Equal_a681 : std_logic;
SIGNAL cps_a0_a_acombout : std_logic;
SIGNAL cps_a2_a_acombout : std_logic;
SIGNAL Equal_a683 : std_logic;
SIGNAL cps_a10_a_acombout : std_logic;
SIGNAL cps_a11_a_acombout : std_logic;
SIGNAL Equal_a684 : std_logic;
SIGNAL Equal_a685 : std_logic;
SIGNAL UPD_PROC_a8 : std_logic;
SIGNAL BIT_STATE_ast_pre : std_logic;
SIGNAL BIT_STATE_ast_pk1 : std_logic;
SIGNAL iLD_bit_dac : std_logic;
SIGNAL LD_bit_dac_areg0 : std_logic;
SIGNAL reduce_or_a1 : std_logic;
SIGNAL add_a1565 : std_logic;
SIGNAL iBIT_VAL_a15_a_a942 : std_logic;
SIGNAL iBIT_VAL_a0_a : std_logic;
SIGNAL Select_a1854 : std_logic;
SIGNAL bit_dac_DATA_a0_a_areg0 : std_logic;
SIGNAL peak2_a1_a_acombout : std_logic;
SIGNAL add_a1566 : std_logic;
SIGNAL iBIT_VAL_a0_a_a895 : std_logic;
SIGNAL iBIT_VAL_a1_a : std_logic;
SIGNAL Select_a1856 : std_logic;
SIGNAL bit_dac_DATA_a1_a_areg0 : std_logic;
SIGNAL add_a1633 : std_logic;
SIGNAL DNL_CNT_a0_a : std_logic;
SIGNAL add_a1635 : std_logic;
SIGNAL add_a1629 : std_logic;
SIGNAL DNL_CNT_a1_a : std_logic;
SIGNAL add_a1631 : std_logic;
SIGNAL add_a1627 : std_logic;
SIGNAL add_a1619 : std_logic;
SIGNAL add_a1623 : std_logic;
SIGNAL add_a1615 : std_logic;
SIGNAL add_a1611 : std_logic;
SIGNAL add_a1607 : std_logic;
SIGNAL add_a1603 : std_logic;
SIGNAL add_a1597 : std_logic;
SIGNAL DNL_CNT_a9_a : std_logic;
SIGNAL add_a1609 : std_logic;
SIGNAL DNL_CNT_a6_a : std_logic;
SIGNAL add_a1605 : std_logic;
SIGNAL DNL_CNT_a7_a : std_logic;
SIGNAL Equal_a692 : std_logic;
SIGNAL add_a1625 : std_logic;
SIGNAL DNL_CNT_a2_a : std_logic;
SIGNAL add_a1613 : std_logic;
SIGNAL DNL_CNT_a5_a : std_logic;
SIGNAL add_a1621 : std_logic;
SIGNAL DNL_CNT_a4_a : std_logic;
SIGNAL Equal_a693 : std_logic;
SIGNAL Equal_a694 : std_logic;
SIGNAL UPD_PROC_a4 : std_logic;
SIGNAL Time_Enable_acombout : std_logic;
SIGNAL UPD_PROC_a3 : std_logic;
SIGNAL DNL_DAC_VAL_a0_a_a152 : std_logic;
SIGNAL DNL_DAC_VAL_a1_a_a155 : std_logic;
SIGNAL DNL_DAC_VAL_a2_a : std_logic;
SIGNAL iBIT_VAL_a1_a_a898 : std_logic;
SIGNAL iBIT_VAL_a2_a : std_logic;
SIGNAL Select_a1858 : std_logic;
SIGNAL bit_dac_DATA_a2_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a2_a_a158 : std_logic;
SIGNAL DNL_DAC_VAL_a3_a : std_logic;
SIGNAL add_a1568 : std_logic;
SIGNAL iBIT_VAL_a2_a_a901 : std_logic;
SIGNAL iBIT_VAL_a3_a : std_logic;
SIGNAL Select_a1860 : std_logic;
SIGNAL bit_dac_DATA_a3_a_areg0 : std_logic;
SIGNAL add_a1569 : std_logic;
SIGNAL iBIT_VAL_a3_a_a904 : std_logic;
SIGNAL iBIT_VAL_a4_a : std_logic;
SIGNAL Select_a1862 : std_logic;
SIGNAL bit_dac_DATA_a4_a_areg0 : std_logic;
SIGNAL add_a1570 : std_logic;
SIGNAL iBIT_VAL_a4_a_a907 : std_logic;
SIGNAL iBIT_VAL_a5_a : std_logic;
SIGNAL DNL_DAC_VAL_a3_a_a161 : std_logic;
SIGNAL DNL_DAC_VAL_a4_a_a164 : std_logic;
SIGNAL DNL_DAC_VAL_a5_a : std_logic;
SIGNAL Select_a1864 : std_logic;
SIGNAL bit_dac_DATA_a5_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a5_a_a167 : std_logic;
SIGNAL DNL_DAC_VAL_a6_a : std_logic;
SIGNAL peak1_a6_a_acombout : std_logic;
SIGNAL add_a1571 : std_logic;
SIGNAL iBIT_VAL_a5_a_a910 : std_logic;
SIGNAL iBIT_VAL_a6_a : std_logic;
SIGNAL Select_a1866 : std_logic;
SIGNAL bit_dac_DATA_a6_a_areg0 : std_logic;
SIGNAL peak2_a7_a_acombout : std_logic;
SIGNAL add_a1572 : std_logic;
SIGNAL iBIT_VAL_a6_a_a913 : std_logic;
SIGNAL iBIT_VAL_a7_a : std_logic;
SIGNAL DNL_DAC_VAL_a6_a_a170 : std_logic;
SIGNAL DNL_DAC_VAL_a7_a : std_logic;
SIGNAL Select_a1868 : std_logic;
SIGNAL bit_dac_DATA_a7_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a7_a_a173 : std_logic;
SIGNAL DNL_DAC_VAL_a8_a : std_logic;
SIGNAL add_a1573 : std_logic;
SIGNAL iBIT_VAL_a7_a_a916 : std_logic;
SIGNAL iBIT_VAL_a8_a : std_logic;
SIGNAL Select_a1870 : std_logic;
SIGNAL bit_dac_DATA_a8_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a8_a_a176 : std_logic;
SIGNAL DNL_DAC_VAL_a9_a : std_logic;
SIGNAL Select_a1872 : std_logic;
SIGNAL bit_dac_DATA_a9_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a9_a_a179 : std_logic;
SIGNAL DNL_DAC_VAL_a10_a : std_logic;
SIGNAL add_a1574 : std_logic;
SIGNAL iBIT_VAL_a8_a_a919 : std_logic;
SIGNAL iBIT_VAL_a9_a_a922 : std_logic;
SIGNAL iBIT_VAL_a10_a : std_logic;
SIGNAL Select_a1874 : std_logic;
SIGNAL bit_dac_DATA_a10_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a10_a_a182 : std_logic;
SIGNAL DNL_DAC_VAL_a11_a : std_logic;
SIGNAL Select_a1876 : std_logic;
SIGNAL bit_dac_DATA_a11_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a11_a_a185 : std_logic;
SIGNAL DNL_DAC_VAL_a12_a : std_logic;
SIGNAL Select_a1878 : std_logic;
SIGNAL bit_dac_DATA_a12_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a12_a_a188 : std_logic;
SIGNAL DNL_DAC_VAL_a13_a : std_logic;
SIGNAL Select_a1880 : std_logic;
SIGNAL bit_dac_DATA_a13_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a13_a_a191 : std_logic;
SIGNAL DNL_DAC_VAL_a14_a : std_logic;
SIGNAL Select_a1882 : std_logic;
SIGNAL bit_dac_DATA_a14_a_areg0 : std_logic;
SIGNAL DNL_DAC_VAL_a14_a_a194 : std_logic;
SIGNAL DNL_DAC_VAL_a15_a : std_logic;
SIGNAL Select_a1884 : std_logic;
SIGNAL bit_dac_DATA_a15_a_areg0 : std_logic;
SIGNAL ALT_INV_BIT_EN_acombout : std_logic;

BEGIN

ww_IMR <= IMR;
ww_clk20 <= clk20;
ww_BIT_EN <= BIT_EN;
ww_Time_Enable <= Time_Enable;
ww_peak1 <= peak1;
ww_peak2 <= peak2;
ww_cps <= cps;
ww_pk2dly <= pk2dly;
LD_bit_dac <= ww_LD_bit_dac;
bit_dac_DATA <= ww_bit_dac_DATA;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_BIT_EN_acombout <= NOT BIT_EN_acombout;

DNL_DAC_VAL_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a0_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a0_a $ UPD_PROC_a4, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a0_a_a152 = CARRY(DNL_DAC_VAL_a0_a & UPD_PROC_a4)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a0_a,
	datab => UPD_PROC_a4,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a0_a,
	cout => DNL_DAC_VAL_a0_a_a152);

DNL_DAC_VAL_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a1_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a1_a $ (DNL_DAC_VAL_a0_a_a152), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a1_a_a155 = CARRY(!DNL_DAC_VAL_a0_a_a152 # !DNL_DAC_VAL_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a1_a,
	cin => DNL_DAC_VAL_a0_a_a152,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a1_a,
	cout => DNL_DAC_VAL_a1_a_a155);

DNL_DAC_VAL_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a4_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a4_a $ (!DNL_DAC_VAL_a3_a_a161), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a4_a_a164 = CARRY(DNL_DAC_VAL_a4_a & (!DNL_DAC_VAL_a3_a_a161))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a4_a,
	cin => DNL_DAC_VAL_a3_a_a161,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a4_a,
	cout => DNL_DAC_VAL_a4_a_a164);

iBIT_VAL_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a9_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a9_a $ add_a1574 $ iBIT_VAL_a8_a_a919, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a9_a_a922 = CARRY(iBIT_VAL_a9_a & !add_a1574 & !iBIT_VAL_a8_a_a919 # !iBIT_VAL_a9_a & (!iBIT_VAL_a8_a_a919 # !add_a1574))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a9_a,
	datab => add_a1574,
	cin => iBIT_VAL_a8_a_a919,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a9_a,
	cout => iBIT_VAL_a9_a_a922);

iBIT_VAL_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a10_a = DFFE(GLOBAL(BIT_EN_acombout) & add_a1575 $ iBIT_VAL_a10_a $ !iBIT_VAL_a9_a_a922, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a10_a_a925 = CARRY(add_a1575 & (iBIT_VAL_a10_a # !iBIT_VAL_a9_a_a922) # !add_a1575 & iBIT_VAL_a10_a & !iBIT_VAL_a9_a_a922)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a1575,
	datab => iBIT_VAL_a10_a,
	cin => iBIT_VAL_a9_a_a922,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a10_a,
	cout => iBIT_VAL_a10_a_a925);

iBIT_VAL_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a11_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a11_a $ add_a1576 $ iBIT_VAL_a10_a_a925, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a11_a_a928 = CARRY(iBIT_VAL_a11_a & !add_a1576 & !iBIT_VAL_a10_a_a925 # !iBIT_VAL_a11_a & (!iBIT_VAL_a10_a_a925 # !add_a1576))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a11_a,
	datab => add_a1576,
	cin => iBIT_VAL_a10_a_a925,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a11_a,
	cout => iBIT_VAL_a11_a_a928);

iBIT_VAL_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a12_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a12_a $ (!iBIT_VAL_a11_a_a928), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a12_a_a931 = CARRY(iBIT_VAL_a12_a & (!iBIT_VAL_a11_a_a928))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a12_a,
	cin => iBIT_VAL_a11_a_a928,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a12_a,
	cout => iBIT_VAL_a12_a_a931);

iBIT_VAL_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a13_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a13_a $ (iBIT_VAL_a12_a_a931), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a13_a_a934 = CARRY(!iBIT_VAL_a12_a_a931 # !iBIT_VAL_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a13_a,
	cin => iBIT_VAL_a12_a_a931,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a13_a,
	cout => iBIT_VAL_a13_a_a934);

iBIT_VAL_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a14_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a14_a $ (!iBIT_VAL_a13_a_a934), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a14_a_a937 = CARRY(iBIT_VAL_a14_a & (!iBIT_VAL_a13_a_a934))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a14_a,
	cin => iBIT_VAL_a13_a_a934,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a14_a,
	cout => iBIT_VAL_a14_a_a937);

iBIT_VAL_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a15_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a15_a $ (iBIT_VAL_a14_a_a937), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a15_a,
	cin => iBIT_VAL_a14_a_a937,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a15_a);

pk2dly_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(4),
	combout => pk2dly_a4_a_acombout);

pk2dly_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(9),
	combout => pk2dly_a9_a_acombout);

pk2dly_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(8),
	combout => pk2dly_a8_a_acombout);

BIT_CNT_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a9_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a9_a $ (BIT_CNT_a8_a_a151)) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a9_a_a154 = CARRY(!BIT_CNT_a8_a_a151 # !BIT_CNT_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a9_a,
	datac => a_aGND,
	cin => BIT_CNT_a8_a_a151,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a9_a,
	cout => BIT_CNT_a9_a_a154);

Equal_a670_I : apex20ke_lcell
-- Equation(s):
-- Equal_a670 = BIT_CNT_a9_a & pk2dly_a9_a_acombout & (BIT_CNT_a8_a $ !pk2dly_a8_a_acombout) # !BIT_CNT_a9_a & !pk2dly_a9_a_acombout & (BIT_CNT_a8_a $ !pk2dly_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a9_a,
	datab => BIT_CNT_a8_a,
	datac => pk2dly_a8_a_acombout,
	datad => pk2dly_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a670);

BIT_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a0_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a0_a $ Equal_a680) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a0_a_a157 = CARRY(BIT_CNT_a0_a & Equal_a680)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a0_a,
	datab => Equal_a680,
	datac => a_aGND,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a0_a,
	cout => BIT_CNT_a0_a_a157);

pk2dly_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(12),
	combout => pk2dly_a12_a_acombout);

pk2dly_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(5),
	combout => pk2dly_a5_a_acombout);

Equal_a674_I : apex20ke_lcell
-- Equation(s):
-- Equal_a674 = BIT_CNT_a12_a & pk2dly_a12_a_acombout & (BIT_CNT_a5_a $ !pk2dly_a5_a_acombout) # !BIT_CNT_a12_a & !pk2dly_a12_a_acombout & (BIT_CNT_a5_a $ !pk2dly_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a12_a,
	datab => BIT_CNT_a5_a,
	datac => pk2dly_a5_a_acombout,
	datad => pk2dly_a12_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a674);

BIT_CNT_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a6_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a6_a $ (!BIT_CNT_a5_a_a169)) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a6_a_a187 = CARRY(BIT_CNT_a6_a & (!BIT_CNT_a5_a_a169))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a6_a,
	datac => a_aGND,
	cin => BIT_CNT_a5_a_a169,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a6_a,
	cout => BIT_CNT_a6_a_a187);

us1_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a4_a = DFFE(!Equal_a680 & (add_a1577), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a680,
	datad => add_a1577,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a4_a);

us1_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a3_a = DFFE(add_a1581, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add_a1581,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a3_a);

Equal_a679_I : apex20ke_lcell
-- Equation(s):
-- Equal_a679 = !us1_cnt_a3_a & us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => us1_cnt_a3_a,
	datad => us1_cnt_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a679);

cps_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(9),
	combout => cps_a9_a_acombout);

cps_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(8),
	combout => cps_a8_a_acombout);

Equal_a682_I : apex20ke_lcell
-- Equation(s):
-- Equal_a682 = BIT_CNT_a9_a & cps_a9_a_acombout & (BIT_CNT_a8_a $ !cps_a8_a_acombout) # !BIT_CNT_a9_a & !cps_a9_a_acombout & (BIT_CNT_a8_a $ !cps_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a9_a,
	datab => BIT_CNT_a8_a,
	datac => cps_a9_a_acombout,
	datad => cps_a8_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a682);

cps_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(14),
	combout => cps_a14_a_acombout);

cps_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(6),
	combout => cps_a6_a_acombout);

Equal_a689_I : apex20ke_lcell
-- Equation(s):
-- Equal_a689 = BIT_CNT_a14_a & cps_a14_a_acombout & (BIT_CNT_a6_a $ !cps_a6_a_acombout) # !BIT_CNT_a14_a & !cps_a14_a_acombout & (BIT_CNT_a6_a $ !cps_a6_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a14_a,
	datab => BIT_CNT_a6_a,
	datac => cps_a14_a_acombout,
	datad => cps_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a689);

DNL_CNT_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a8_a = DFFE(DNL_TEST & add_a1601 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1601,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a8_a);

DNL_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a3_a = DFFE(DNL_TEST & add_a1617 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1617,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a3_a);

Select_a1890_I : apex20ke_lcell
-- Equation(s):
-- Select_a1890 = !DNL_TEST & !Bit_Disable & BIT_EN_acombout & BIT_STATE_ast_pk2d

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_TEST,
	datab => Bit_Disable,
	datac => BIT_EN_acombout,
	datad => BIT_STATE_ast_pk2d,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1890);

peak1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(2),
	combout => peak1_a2_a_acombout);

add_a1567_I : apex20ke_lcell
-- Equation(s):
-- add_a1567 = BIT_STATE_ast_pk1 & peak1_a2_a_acombout # !BIT_STATE_ast_pk1 & (peak2_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_pk1,
	datac => peak1_a2_a_acombout,
	datad => peak2_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1567);

peak1_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(10),
	combout => peak1_a10_a_acombout);

add_a1575_I : apex20ke_lcell
-- Equation(s):
-- add_a1575 = BIT_STATE_ast_pk1 & peak1_a10_a_acombout # !BIT_STATE_ast_pk1 & (peak2_a10_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => peak1_a10_a_acombout,
	datac => BIT_STATE_ast_pk1,
	datad => peak2_a10_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1575);

peak2_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(11),
	combout => peak2_a11_a_acombout);

add_a1576_I : apex20ke_lcell
-- Equation(s):
-- add_a1576 = BIT_STATE_ast_pk1 & (peak1_a11_a_acombout) # !BIT_STATE_ast_pk1 & peak2_a11_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_pk1,
	datac => peak2_a11_a_acombout,
	datad => peak1_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1576);

add_a1577_I : apex20ke_lcell
-- Equation(s):
-- add_a1577 = add_a1583 $ !us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => us1_cnt_a4_a,
	cin => add_a1583,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1577);

add_a1581_I : apex20ke_lcell
-- Equation(s):
-- add_a1581 = us1_cnt_a3_a $ (add_a1595)
-- add_a1583 = CARRY(!add_a1595 # !us1_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a3_a,
	cin => add_a1595,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1581,
	cout => add_a1583);

add_a1593_I : apex20ke_lcell
-- Equation(s):
-- add_a1593 = us1_cnt_a2_a $ !add_a1587
-- add_a1595 = CARRY(us1_cnt_a2_a & !add_a1587)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a2_a,
	cin => add_a1587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1593,
	cout => add_a1595);

add_a1601_I : apex20ke_lcell
-- Equation(s):
-- add_a1601 = DNL_CNT_a8_a $ (!add_a1607)
-- add_a1603 = CARRY(DNL_CNT_a8_a & (!add_a1607))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a8_a,
	cin => add_a1607,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1601,
	cout => add_a1603);

add_a1617_I : apex20ke_lcell
-- Equation(s):
-- add_a1617 = DNL_CNT_a3_a $ (add_a1627)
-- add_a1619 = CARRY(!add_a1627 # !DNL_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a3_a,
	cin => add_a1627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1617,
	cout => add_a1619);

Select_a1901_I : apex20ke_lcell
-- Equation(s):
-- Select_a1901 = BIT_STATE_ast_pk2 # BIT_STATE_ast_pk1 & !PK2_MODE

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CECE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datab => BIT_STATE_ast_pk2,
	datac => PK2_MODE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1901);

Select_a1902_I : apex20ke_lcell
-- Equation(s):
-- Select_a1902 = !DNL_TEST & !UPD_PROC_a2 & (Select_a1901 # !BIT_STATE_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0023",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1901,
	datab => DNL_TEST,
	datac => BIT_STATE_ast_idle,
	datad => UPD_PROC_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1902);

UPD_PROC_a5_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a5 = BIT_STATE_ast_dnl0 & !Equal_a694

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BIT_STATE_ast_dnl0,
	datad => Equal_a694,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a5);

BIT_EN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_EN,
	combout => BIT_EN_acombout);

peak1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(0),
	combout => peak1_a0_a_acombout);

peak1_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(11),
	combout => peak1_a11_a_acombout);

peak1_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(9),
	combout => peak1_a9_a_acombout);

peak1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(5),
	combout => peak1_a5_a_acombout);

peak1_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(8),
	combout => peak1_a8_a_acombout);

peak1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(7),
	combout => peak1_a7_a_acombout);

UPD_PROC_a209_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a209 = !peak1_a6_a_acombout & !peak1_a5_a_acombout & !peak1_a8_a_acombout & !peak1_a7_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a6_a_acombout,
	datab => peak1_a5_a_acombout,
	datac => peak1_a8_a_acombout,
	datad => peak1_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a209);

UPD_PROC_a210_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a210 = !peak1_a10_a_acombout & !peak1_a11_a_acombout & !peak1_a9_a_acombout & UPD_PROC_a209

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a10_a_acombout,
	datab => peak1_a11_a_acombout,
	datac => peak1_a9_a_acombout,
	datad => UPD_PROC_a209,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a210);

peak2_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(3),
	combout => peak2_a3_a_acombout);

peak2_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(2),
	combout => peak2_a2_a_acombout);

peak2_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(0),
	combout => peak2_a0_a_acombout);

peak2_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(5),
	combout => peak2_a5_a_acombout);

peak2_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(4),
	combout => peak2_a4_a_acombout);

peak2_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(6),
	combout => peak2_a6_a_acombout);

peak2_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(9),
	combout => peak2_a9_a_acombout);

peak2_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(8),
	combout => peak2_a8_a_acombout);

peak2_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(10),
	combout => peak2_a10_a_acombout);

Equal_a704_I : apex20ke_lcell
-- Equation(s):
-- Equal_a710 = !peak2_a11_a_acombout & !peak2_a9_a_acombout & !peak2_a8_a_acombout & !peak2_a10_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a11_a_acombout,
	datab => peak2_a9_a_acombout,
	datac => peak2_a8_a_acombout,
	datad => peak2_a10_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a704,
	cascout => Equal_a710);

Equal_a707_I : apex20ke_lcell
-- Equation(s):
-- Equal_a711 = (!peak2_a7_a_acombout & !peak2_a5_a_acombout & !peak2_a4_a_acombout & !peak2_a6_a_acombout) & CASCADE(Equal_a710)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a7_a_acombout,
	datab => peak2_a5_a_acombout,
	datac => peak2_a4_a_acombout,
	datad => peak2_a6_a_acombout,
	cascin => Equal_a710,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a707,
	cascout => Equal_a711);

Equal_a706_I : apex20ke_lcell
-- Equation(s):
-- Equal_a706 = (!peak2_a1_a_acombout & !peak2_a3_a_acombout & !peak2_a2_a_acombout & !peak2_a0_a_acombout) & CASCADE(Equal_a711)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak2_a1_a_acombout,
	datab => peak2_a3_a_acombout,
	datac => peak2_a2_a_acombout,
	datad => peak2_a0_a_acombout,
	cascin => Equal_a711,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a706);

clk20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk20,
	combout => clk20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

Bit_Disable_aI : apex20ke_lcell
-- Equation(s):
-- Bit_Disable = DFFE(UPD_PROC_a208 & !peak1_a0_a_acombout & UPD_PROC_a210 & Equal_a706, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => UPD_PROC_a208,
	datab => peak1_a0_a_acombout,
	datac => UPD_PROC_a210,
	datad => Equal_a706,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Bit_Disable);

UPD_PROC_a2_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a2 = Bit_Disable # !BIT_EN_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Bit_Disable,
	datad => BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a2);

a_aGND_aI : apex20ke_lcell
-- Equation(s):
-- a_aGND = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aGND);

add_a1589_I : apex20ke_lcell
-- Equation(s):
-- add_a1589 = !us1_cnt_a0_a
-- add_a1591 = CARRY(us1_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1589,
	cout => add_a1591);

us1_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a0_a = DFFE(add_a1589, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add_a1589,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a0_a);

add_a1585_I : apex20ke_lcell
-- Equation(s):
-- add_a1585 = us1_cnt_a1_a $ (add_a1591)
-- add_a1587 = CARRY(!add_a1591 # !us1_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a1_a,
	cin => add_a1591,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1585,
	cout => add_a1587);

us1_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a1_a = DFFE(add_a1585, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add_a1585,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a1_a);

us1_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a2_a = DFFE(!Equal_a680 & (add_a1593), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a680,
	datac => add_a1593,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a2_a);

Equal_a680_I : apex20ke_lcell
-- Equation(s):
-- Equal_a680 = Equal_a679 & us1_cnt_a0_a & us1_cnt_a1_a & !us1_cnt_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a679,
	datab => us1_cnt_a0_a,
	datac => us1_cnt_a1_a,
	datad => us1_cnt_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a680);

Select_a1886_I : apex20ke_lcell
-- Equation(s):
-- Select_a1886 = DNL_TEST & !Bit_Disable & BIT_EN_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_TEST,
	datab => Bit_Disable,
	datac => BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1886);

peak1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(3),
	combout => peak1_a3_a_acombout);

peak1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(4),
	combout => peak1_a4_a_acombout);

peak1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(1),
	combout => peak1_a1_a_acombout);

UPD_PROC_a208_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a208 = !peak1_a2_a_acombout & !peak1_a3_a_acombout & !peak1_a4_a_acombout & !peak1_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a2_a_acombout,
	datab => peak1_a3_a_acombout,
	datac => peak1_a4_a_acombout,
	datad => peak1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a208);

DNL_TEST_aI : apex20ke_lcell
-- Equation(s):
-- DNL_TEST = DFFE(peak1_a0_a_acombout & UPD_PROC_a208 & Equal_a706 & UPD_PROC_a210, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak1_a0_a_acombout,
	datab => UPD_PROC_a208,
	datac => Equal_a706,
	datad => UPD_PROC_a210,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_TEST);

BIT_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a1_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a1_a $ (BIT_CNT_a0_a_a157)) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a1_a_a145 = CARRY(!BIT_CNT_a0_a_a157 # !BIT_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a1_a,
	datac => a_aGND,
	cin => BIT_CNT_a0_a_a157,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a1_a,
	cout => BIT_CNT_a1_a_a145);

pk2dly_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(1),
	combout => pk2dly_a1_a_acombout);

Equal_a669_I : apex20ke_lcell
-- Equation(s):
-- Equal_a669 = pk2dly_a4_a_acombout & BIT_CNT_a4_a & (BIT_CNT_a1_a $ !pk2dly_a1_a_acombout) # !pk2dly_a4_a_acombout & !BIT_CNT_a4_a & (BIT_CNT_a1_a $ !pk2dly_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pk2dly_a4_a_acombout,
	datab => BIT_CNT_a1_a,
	datac => BIT_CNT_a4_a,
	datad => pk2dly_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a669);

pk2dly_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(2),
	combout => pk2dly_a2_a_acombout);

pk2dly_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(0),
	combout => pk2dly_a0_a_acombout);

Equal_a671_I : apex20ke_lcell
-- Equation(s):
-- Equal_a671 = BIT_CNT_a0_a & pk2dly_a0_a_acombout & (BIT_CNT_a2_a $ !pk2dly_a2_a_acombout) # !BIT_CNT_a0_a & !pk2dly_a0_a_acombout & (BIT_CNT_a2_a $ !pk2dly_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a0_a,
	datab => BIT_CNT_a2_a,
	datac => pk2dly_a2_a_acombout,
	datad => pk2dly_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a671);

BIT_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a4_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a4_a $ !BIT_CNT_a3_a_a178) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a4_a_a148 = CARRY(BIT_CNT_a4_a & !BIT_CNT_a3_a_a178)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a4_a,
	datac => a_aGND,
	cin => BIT_CNT_a3_a_a178,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a4_a,
	cout => BIT_CNT_a4_a_a148);

BIT_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a5_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a5_a $ BIT_CNT_a4_a_a148) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a5_a_a169 = CARRY(!BIT_CNT_a4_a_a148 # !BIT_CNT_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a5_a,
	datac => a_aGND,
	cin => BIT_CNT_a4_a_a148,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a5_a,
	cout => BIT_CNT_a5_a_a169);

BIT_CNT_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a7_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a7_a $ (BIT_CNT_a6_a_a187)) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a7_a_a181 = CARRY(!BIT_CNT_a6_a_a187 # !BIT_CNT_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a7_a,
	datac => a_aGND,
	cin => BIT_CNT_a6_a_a187,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a7_a,
	cout => BIT_CNT_a7_a_a181);

BIT_CNT_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a8_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a8_a $ !BIT_CNT_a7_a_a181) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a8_a_a151 = CARRY(BIT_CNT_a8_a & !BIT_CNT_a7_a_a181)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a8_a,
	datac => a_aGND,
	cin => BIT_CNT_a7_a_a181,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a8_a,
	cout => BIT_CNT_a8_a_a151);

BIT_CNT_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a10_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a10_a $ !BIT_CNT_a9_a_a154) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a10_a_a163 = CARRY(BIT_CNT_a10_a & !BIT_CNT_a9_a_a154)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a10_a,
	datac => a_aGND,
	cin => BIT_CNT_a9_a_a154,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a10_a,
	cout => BIT_CNT_a10_a_a163);

pk2dly_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(10),
	combout => pk2dly_a10_a_acombout);

pk2dly_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(11),
	combout => pk2dly_a11_a_acombout);

Equal_a672_I : apex20ke_lcell
-- Equation(s):
-- Equal_a672 = BIT_CNT_a11_a & pk2dly_a11_a_acombout & (BIT_CNT_a10_a $ !pk2dly_a10_a_acombout) # !BIT_CNT_a11_a & !pk2dly_a11_a_acombout & (BIT_CNT_a10_a $ !pk2dly_a10_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a11_a,
	datab => BIT_CNT_a10_a,
	datac => pk2dly_a10_a_acombout,
	datad => pk2dly_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a672);

Equal_a673_I : apex20ke_lcell
-- Equation(s):
-- Equal_a673 = Equal_a670 & Equal_a669 & Equal_a671 & Equal_a672

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a670,
	datab => Equal_a669,
	datac => Equal_a671,
	datad => Equal_a672,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a673);

BIT_CNT_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a11_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a11_a $ BIT_CNT_a10_a_a163) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a11_a_a166 = CARRY(!BIT_CNT_a10_a_a163 # !BIT_CNT_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a11_a,
	datac => a_aGND,
	cin => BIT_CNT_a10_a_a163,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a11_a,
	cout => BIT_CNT_a11_a_a166);

BIT_CNT_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a12_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a12_a $ !BIT_CNT_a11_a_a166) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a12_a_a172 = CARRY(BIT_CNT_a12_a & !BIT_CNT_a11_a_a166)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a12_a,
	datac => a_aGND,
	cin => BIT_CNT_a11_a_a166,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a12_a,
	cout => BIT_CNT_a12_a_a172);

BIT_CNT_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a13_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a13_a $ BIT_CNT_a12_a_a172) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a13_a_a184 = CARRY(!BIT_CNT_a12_a_a172 # !BIT_CNT_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a13_a,
	datac => a_aGND,
	cin => BIT_CNT_a12_a_a172,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a13_a,
	cout => BIT_CNT_a13_a_a184);

BIT_CNT_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a14_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a14_a $ !BIT_CNT_a13_a_a184) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a14_a_a190 = CARRY(BIT_CNT_a14_a & !BIT_CNT_a13_a_a184)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a14_a,
	datac => a_aGND,
	cin => BIT_CNT_a13_a_a184,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a14_a,
	cout => BIT_CNT_a14_a_a190);

BIT_CNT_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a15_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a15_a $ (BIT_CNT_a14_a_a190)) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a15_a,
	datac => a_aGND,
	cin => BIT_CNT_a14_a_a190,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a15_a);

pk2dly_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(3),
	combout => pk2dly_a3_a_acombout);

pk2dly_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(15),
	combout => pk2dly_a15_a_acombout);

Equal_a675_I : apex20ke_lcell
-- Equation(s):
-- Equal_a675 = BIT_CNT_a3_a & pk2dly_a3_a_acombout & (BIT_CNT_a15_a $ !pk2dly_a15_a_acombout) # !BIT_CNT_a3_a & !pk2dly_a3_a_acombout & (BIT_CNT_a15_a $ !pk2dly_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a3_a,
	datab => BIT_CNT_a15_a,
	datac => pk2dly_a3_a_acombout,
	datad => pk2dly_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a675);

pk2dly_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(7),
	combout => pk2dly_a7_a_acombout);

pk2dly_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(13),
	combout => pk2dly_a13_a_acombout);

Equal_a676_I : apex20ke_lcell
-- Equation(s):
-- Equal_a676 = BIT_CNT_a13_a & pk2dly_a13_a_acombout & (BIT_CNT_a7_a $ !pk2dly_a7_a_acombout) # !BIT_CNT_a13_a & !pk2dly_a13_a_acombout & (BIT_CNT_a7_a $ !pk2dly_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a13_a,
	datab => BIT_CNT_a7_a,
	datac => pk2dly_a7_a_acombout,
	datad => pk2dly_a13_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a676);

pk2dly_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(14),
	combout => pk2dly_a14_a_acombout);

pk2dly_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pk2dly(6),
	combout => pk2dly_a6_a_acombout);

Equal_a677_I : apex20ke_lcell
-- Equation(s):
-- Equal_a677 = BIT_CNT_a6_a & pk2dly_a6_a_acombout & (BIT_CNT_a14_a $ !pk2dly_a14_a_acombout) # !BIT_CNT_a6_a & !pk2dly_a6_a_acombout & (BIT_CNT_a14_a $ !pk2dly_a14_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a6_a,
	datab => BIT_CNT_a14_a,
	datac => pk2dly_a14_a_acombout,
	datad => pk2dly_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a677);

Equal_a678_I : apex20ke_lcell
-- Equation(s):
-- Equal_a678 = Equal_a674 & Equal_a675 & Equal_a676 & Equal_a677

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a674,
	datab => Equal_a675,
	datac => Equal_a676,
	datad => Equal_a677,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a678);

BIT_STATE_ast_pk2_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_pk2 = DFFE(Select_a1890 & Equal_a673 & Equal_a680 & Equal_a678, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1890,
	datab => Equal_a673,
	datac => Equal_a680,
	datad => Equal_a678,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_pk2);

bit_dac_DATA_a46_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a46 = !BIT_STATE_ast_pk1 & !BIT_STATE_ast_pk2

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BIT_STATE_ast_pk1,
	datad => BIT_STATE_ast_pk2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => bit_dac_DATA_a46);

Select_a1896_I : apex20ke_lcell
-- Equation(s):
-- Select_a1896 = BIT_STATE_ast_pk2d & (DNL_TEST # UPD_PROC_a2) # !BIT_STATE_ast_pk2d & !bit_dac_DATA_a46 & (DNL_TEST # UPD_PROC_a2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AF8C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk2d,
	datab => DNL_TEST,
	datac => bit_dac_DATA_a46,
	datad => UPD_PROC_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1896);

Select_a1897_I : apex20ke_lcell
-- Equation(s):
-- Select_a1897 = Select_a1896 # !Select_a1886 & (BIT_STATE_ast_dnl0 # BIT_STATE_ast_dnl2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF32",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Select_a1886,
	datac => BIT_STATE_ast_dnl2,
	datad => Select_a1896,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1897);

Select_a1898_I : apex20ke_lcell
-- Equation(s):
-- Select_a1898 = Select_a1897 # UPD_PROC_a2 & (BIT_STATE_ast_pre # !BIT_STATE_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => UPD_PROC_a2,
	datab => BIT_STATE_ast_idle,
	datac => BIT_STATE_ast_pre,
	datad => Select_a1897,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1898);

cps_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(5),
	combout => cps_a5_a_acombout);

cps_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(12),
	combout => cps_a12_a_acombout);

Equal_a686_I : apex20ke_lcell
-- Equation(s):
-- Equal_a686 = BIT_CNT_a12_a & cps_a12_a_acombout & (BIT_CNT_a5_a $ !cps_a5_a_acombout) # !BIT_CNT_a12_a & !cps_a12_a_acombout & (BIT_CNT_a5_a $ !cps_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a12_a,
	datab => BIT_CNT_a5_a,
	datac => cps_a5_a_acombout,
	datad => cps_a12_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a686);

cps_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(7),
	combout => cps_a7_a_acombout);

cps_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(13),
	combout => cps_a13_a_acombout);

Equal_a688_I : apex20ke_lcell
-- Equation(s):
-- Equal_a688 = BIT_CNT_a13_a & cps_a13_a_acombout & (BIT_CNT_a7_a $ !cps_a7_a_acombout) # !BIT_CNT_a13_a & !cps_a13_a_acombout & (BIT_CNT_a7_a $ !cps_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a13_a,
	datab => BIT_CNT_a7_a,
	datac => cps_a7_a_acombout,
	datad => cps_a13_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a688);

cps_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(15),
	combout => cps_a15_a_acombout);

cps_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(3),
	combout => cps_a3_a_acombout);

Equal_a687_I : apex20ke_lcell
-- Equation(s):
-- Equal_a687 = BIT_CNT_a15_a & cps_a15_a_acombout & (BIT_CNT_a3_a $ !cps_a3_a_acombout) # !BIT_CNT_a15_a & !cps_a15_a_acombout & (BIT_CNT_a3_a $ !cps_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a15_a,
	datab => BIT_CNT_a3_a,
	datac => cps_a15_a_acombout,
	datad => cps_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a687);

Equal_a690_I : apex20ke_lcell
-- Equation(s):
-- Equal_a690 = Equal_a689 & Equal_a686 & Equal_a688 & Equal_a687

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a689,
	datab => Equal_a686,
	datac => Equal_a688,
	datad => Equal_a687,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a690);

PK2_MODE_aI : apex20ke_lcell
-- Equation(s):
-- PK2_MODE = DFFE(!Equal_a706, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Equal_a706,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PK2_MODE);

Select_a1894_I : apex20ke_lcell
-- Equation(s):
-- Select_a1894 = BIT_STATE_ast_pk1 & !DNL_TEST & !UPD_PROC_a2 & PK2_MODE

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datab => DNL_TEST,
	datac => UPD_PROC_a2,
	datad => PK2_MODE,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1894);

UPD_PROC_a207_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a207 = BIT_STATE_ast_pk2d & (!Equal_a678 # !Equal_a673) # !Equal_a680

-- pragma translate_off
GENERIC MAP (
	lut_mask => "75F5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a680,
	datab => Equal_a673,
	datac => BIT_STATE_ast_pk2d,
	datad => Equal_a678,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a207);

BIT_STATE_ast_pk2d_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_pk2d = DFFE(Select_a1894 # Select_a1890 & (Equal_a691 # UPD_PROC_a207), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1890,
	datab => Equal_a691,
	datac => Select_a1894,
	datad => UPD_PROC_a207,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_pk2d);

Equal_a691_I : apex20ke_lcell
-- Equation(s):
-- Equal_a691 = !BIT_STATE_ast_pk2d & (!Equal_a685 # !Equal_a690)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "030F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a690,
	datac => BIT_STATE_ast_pk2d,
	datad => Equal_a685,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a691);

Select_a1899_I : apex20ke_lcell
-- Equation(s):
-- Select_a1899 = !BIT_STATE_ast_idle & (Equal_a691 # UPD_PROC_a207) # !Select_a1886

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5F5D",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1886,
	datab => Equal_a691,
	datac => BIT_STATE_ast_idle,
	datad => UPD_PROC_a207,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1899);

BIT_STATE_ast_idle_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_idle = DFFE(!Select_a1898 & (!BIT_STATE_ast_dnl1 & !BIT_STATE_ast_dnl3 # !Select_a1899), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "010F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl1,
	datab => BIT_STATE_ast_dnl3,
	datac => Select_a1898,
	datad => Select_a1899,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_idle);

BIT_STATE_ast_dnl0_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_dnl0 = DFFE(Select_a1886 & (BIT_STATE_ast_dnl3 & !UPD_PROC_a8 # !BIT_STATE_ast_idle), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A8A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1886,
	datab => BIT_STATE_ast_dnl3,
	datac => BIT_STATE_ast_idle,
	datad => UPD_PROC_a8,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_dnl0);

BIT_STATE_ast_dnl1_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_dnl1 = DFFE(Select_a1886 & (BIT_STATE_ast_dnl0 # BIT_STATE_ast_dnl1 & UPD_PROC_a8), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C8C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl1,
	datab => Select_a1886,
	datac => BIT_STATE_ast_dnl0,
	datad => UPD_PROC_a8,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_dnl1);

Select_a1888_I : apex20ke_lcell
-- Equation(s):
-- Select_a1888 = Equal_a690 & Equal_a680 & Equal_a685

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a690,
	datac => Equal_a680,
	datad => Equal_a685,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1888);

BIT_STATE_ast_dnl2_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_dnl2 = DFFE(BIT_STATE_ast_dnl1 & Select_a1886 & Select_a1888, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_dnl1,
	datac => Select_a1886,
	datad => Select_a1888,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_dnl2);

BIT_STATE_ast_dnl3_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_dnl3 = DFFE(Select_a1886 & (BIT_STATE_ast_dnl2 # BIT_STATE_ast_dnl3 & UPD_PROC_a8), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C8C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl3,
	datab => Select_a1886,
	datac => BIT_STATE_ast_dnl2,
	datad => UPD_PROC_a8,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_dnl3);

reduce_or_a25_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a25 = !BIT_STATE_ast_pre & !BIT_STATE_ast_dnl3 & !BIT_STATE_ast_pk2d & !BIT_STATE_ast_dnl1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pre,
	datab => BIT_STATE_ast_dnl3,
	datac => BIT_STATE_ast_pk2d,
	datad => BIT_STATE_ast_dnl1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a25);

BIT_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a2_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a2_a $ !BIT_CNT_a1_a_a145) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a2_a_a160 = CARRY(BIT_CNT_a2_a & !BIT_CNT_a1_a_a145)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a2_a,
	datac => a_aGND,
	cin => BIT_CNT_a1_a_a145,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a2_a,
	cout => BIT_CNT_a2_a_a160);

BIT_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a3_a = DFFE((reduce_or_a25 & a_aGND) # (!reduce_or_a25 & BIT_CNT_a3_a $ BIT_CNT_a2_a_a160) & !GLOBAL(UPD_PROC_a2), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- BIT_CNT_a3_a_a178 = CARRY(!BIT_CNT_a2_a_a160 # !BIT_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a3_a,
	datac => a_aGND,
	cin => BIT_CNT_a2_a_a160,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a2,
	sload => reduce_or_a25,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a3_a,
	cout => BIT_CNT_a3_a_a178);

cps_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(1),
	combout => cps_a1_a_acombout);

cps_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(4),
	combout => cps_a4_a_acombout);

Equal_a681_I : apex20ke_lcell
-- Equation(s):
-- Equal_a681 = BIT_CNT_a1_a & cps_a1_a_acombout & (BIT_CNT_a4_a $ !cps_a4_a_acombout) # !BIT_CNT_a1_a & !cps_a1_a_acombout & (BIT_CNT_a4_a $ !cps_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a1_a,
	datab => BIT_CNT_a4_a,
	datac => cps_a1_a_acombout,
	datad => cps_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a681);

cps_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(0),
	combout => cps_a0_a_acombout);

cps_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(2),
	combout => cps_a2_a_acombout);

Equal_a683_I : apex20ke_lcell
-- Equation(s):
-- Equal_a683 = BIT_CNT_a0_a & cps_a0_a_acombout & (BIT_CNT_a2_a $ !cps_a2_a_acombout) # !BIT_CNT_a0_a & !cps_a0_a_acombout & (BIT_CNT_a2_a $ !cps_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a0_a,
	datab => BIT_CNT_a2_a,
	datac => cps_a0_a_acombout,
	datad => cps_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a683);

cps_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(10),
	combout => cps_a10_a_acombout);

cps_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_cps(11),
	combout => cps_a11_a_acombout);

Equal_a684_I : apex20ke_lcell
-- Equation(s):
-- Equal_a684 = BIT_CNT_a11_a & cps_a11_a_acombout & (BIT_CNT_a10_a $ !cps_a10_a_acombout) # !BIT_CNT_a11_a & !cps_a11_a_acombout & (BIT_CNT_a10_a $ !cps_a10_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a11_a,
	datab => BIT_CNT_a10_a,
	datac => cps_a10_a_acombout,
	datad => cps_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a684);

Equal_a685_I : apex20ke_lcell
-- Equation(s):
-- Equal_a685 = Equal_a682 & Equal_a681 & Equal_a683 & Equal_a684

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a682,
	datab => Equal_a681,
	datac => Equal_a683,
	datad => Equal_a684,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a685);

UPD_PROC_a8_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a8 = UPD_PROC_a207 # !BIT_STATE_ast_pk2d & (!Equal_a690 # !Equal_a685)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF15",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk2d,
	datab => Equal_a685,
	datac => Equal_a690,
	datad => UPD_PROC_a207,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a8);

BIT_STATE_ast_pre_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_pre = DFFE(Select_a1902 # !UPD_PROC_a2 & BIT_STATE_ast_pre & UPD_PROC_a8, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BAAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1902,
	datab => UPD_PROC_a2,
	datac => BIT_STATE_ast_pre,
	datad => UPD_PROC_a8,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_pre);

BIT_STATE_ast_pk1_aI : apex20ke_lcell
-- Equation(s):
-- BIT_STATE_ast_pk1 = DFFE(!Bit_Disable & BIT_EN_acombout & BIT_STATE_ast_pre & Select_a1888, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Bit_Disable,
	datab => BIT_EN_acombout,
	datac => BIT_STATE_ast_pre,
	datad => Select_a1888,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_STATE_ast_pk1);

iLD_bit_dac_aI : apex20ke_lcell
-- Equation(s):
-- iLD_bit_dac = DFFE(BIT_STATE_ast_dnl0 # BIT_STATE_ast_pk1 # BIT_STATE_ast_pk2 # BIT_STATE_ast_dnl2, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => BIT_STATE_ast_pk1,
	datac => BIT_STATE_ast_pk2,
	datad => BIT_STATE_ast_dnl2,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iLD_bit_dac);

LD_bit_dac_areg0_I : apex20ke_lcell
-- Equation(s):
-- LD_bit_dac_areg0 = DFFE(iLD_bit_dac, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iLD_bit_dac,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => LD_bit_dac_areg0);

reduce_or_a1_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a1 = BIT_STATE_ast_pk1 # BIT_STATE_ast_dnl0 # BIT_STATE_ast_dnl2 # BIT_STATE_ast_pk2

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datab => BIT_STATE_ast_dnl0,
	datac => BIT_STATE_ast_dnl2,
	datad => BIT_STATE_ast_pk2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a1);

add_a1565_I : apex20ke_lcell
-- Equation(s):
-- add_a1565 = BIT_STATE_ast_pk1 & peak1_a0_a_acombout # !BIT_STATE_ast_pk1 & (peak2_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DD88",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datab => peak1_a0_a_acombout,
	datad => peak2_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1565);

iBIT_VAL_a15_a_a942_I : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a15_a_a942 = BIT_STATE_ast_pk2 # BIT_STATE_ast_pk1 # !BIT_EN_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_pk2,
	datac => BIT_STATE_ast_pk1,
	datad => BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => iBIT_VAL_a15_a_a942);

iBIT_VAL_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a0_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a0_a $ add_a1565, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a0_a_a895 = CARRY(iBIT_VAL_a0_a & add_a1565)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a0_a,
	datab => add_a1565,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a0_a,
	cout => iBIT_VAL_a0_a_a895);

Select_a1854_I : apex20ke_lcell
-- Equation(s):
-- Select_a1854 = DNL_DAC_VAL_a0_a & (BIT_STATE_ast_dnl0 # !bit_dac_DATA_a46 & iBIT_VAL_a0_a) # !DNL_DAC_VAL_a0_a & !bit_dac_DATA_a46 & (iBIT_VAL_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B3A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a0_a,
	datab => bit_dac_DATA_a46,
	datac => BIT_STATE_ast_dnl0,
	datad => iBIT_VAL_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1854);

bit_dac_DATA_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a0_a_areg0 = DFFE(Select_a1854 # !reduce_or_a1 & bit_dac_DATA_a0_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reduce_or_a1,
	datac => bit_dac_DATA_a0_a_areg0,
	datad => Select_a1854,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a0_a_areg0);

peak2_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(1),
	combout => peak2_a1_a_acombout);

add_a1566_I : apex20ke_lcell
-- Equation(s):
-- add_a1566 = BIT_STATE_ast_pk1 & (peak1_a1_a_acombout) # !BIT_STATE_ast_pk1 & peak2_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_pk1,
	datac => peak2_a1_a_acombout,
	datad => peak1_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1566);

iBIT_VAL_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a1_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a1_a $ add_a1566 $ iBIT_VAL_a0_a_a895, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a1_a_a898 = CARRY(iBIT_VAL_a1_a & !add_a1566 & !iBIT_VAL_a0_a_a895 # !iBIT_VAL_a1_a & (!iBIT_VAL_a0_a_a895 # !add_a1566))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a1_a,
	datab => add_a1566,
	cin => iBIT_VAL_a0_a_a895,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a1_a,
	cout => iBIT_VAL_a1_a_a898);

Select_a1856_I : apex20ke_lcell
-- Equation(s):
-- Select_a1856 = DNL_DAC_VAL_a1_a & (BIT_STATE_ast_dnl0 # !bit_dac_DATA_a46 & iBIT_VAL_a1_a) # !DNL_DAC_VAL_a1_a & (!bit_dac_DATA_a46 & iBIT_VAL_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8F88",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a1_a,
	datab => BIT_STATE_ast_dnl0,
	datac => bit_dac_DATA_a46,
	datad => iBIT_VAL_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1856);

bit_dac_DATA_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a1_a_areg0 = DFFE(Select_a1856 # !reduce_or_a1 & bit_dac_DATA_a1_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => reduce_or_a1,
	datac => bit_dac_DATA_a1_a_areg0,
	datad => Select_a1856,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a1_a_areg0);

add_a1633_I : apex20ke_lcell
-- Equation(s):
-- add_a1633 = UPD_PROC_a5 $ DNL_CNT_a0_a
-- add_a1635 = CARRY(UPD_PROC_a5 & DNL_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => UPD_PROC_a5,
	datab => DNL_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1633,
	cout => add_a1635);

DNL_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a0_a = DFFE(DNL_TEST & add_a1633, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DNL_TEST,
	datad => add_a1633,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a0_a);

add_a1629_I : apex20ke_lcell
-- Equation(s):
-- add_a1629 = DNL_CNT_a1_a $ (add_a1635)
-- add_a1631 = CARRY(!add_a1635 # !DNL_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a1_a,
	cin => add_a1635,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1629,
	cout => add_a1631);

DNL_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a1_a = DFFE(DNL_TEST & (add_a1629), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_TEST,
	datad => add_a1629,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a1_a);

add_a1625_I : apex20ke_lcell
-- Equation(s):
-- add_a1625 = DNL_CNT_a2_a $ (!add_a1631)
-- add_a1627 = CARRY(DNL_CNT_a2_a & (!add_a1631))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a2_a,
	cin => add_a1631,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1625,
	cout => add_a1627);

add_a1621_I : apex20ke_lcell
-- Equation(s):
-- add_a1621 = DNL_CNT_a4_a $ (!add_a1619)
-- add_a1623 = CARRY(DNL_CNT_a4_a & (!add_a1619))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a4_a,
	cin => add_a1619,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1621,
	cout => add_a1623);

add_a1613_I : apex20ke_lcell
-- Equation(s):
-- add_a1613 = DNL_CNT_a5_a $ (add_a1623)
-- add_a1615 = CARRY(!add_a1623 # !DNL_CNT_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a5_a,
	cin => add_a1623,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1613,
	cout => add_a1615);

add_a1609_I : apex20ke_lcell
-- Equation(s):
-- add_a1609 = DNL_CNT_a6_a $ (!add_a1615)
-- add_a1611 = CARRY(DNL_CNT_a6_a & (!add_a1615))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a6_a,
	cin => add_a1615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1609,
	cout => add_a1611);

add_a1605_I : apex20ke_lcell
-- Equation(s):
-- add_a1605 = DNL_CNT_a7_a $ (add_a1611)
-- add_a1607 = CARRY(!add_a1611 # !DNL_CNT_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a7_a,
	cin => add_a1611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1605,
	cout => add_a1607);

add_a1597_I : apex20ke_lcell
-- Equation(s):
-- add_a1597 = DNL_CNT_a9_a $ (add_a1603)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a9_a,
	cin => add_a1603,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1597);

DNL_CNT_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a9_a = DFFE(DNL_TEST & add_a1597 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1597,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a9_a);

DNL_CNT_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a6_a = DFFE(DNL_TEST & add_a1609 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1609,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a6_a);

DNL_CNT_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a7_a = DFFE(DNL_TEST & add_a1605 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1605,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a7_a);

Equal_a692_I : apex20ke_lcell
-- Equation(s):
-- Equal_a692 = DNL_CNT_a8_a & DNL_CNT_a9_a & DNL_CNT_a6_a & DNL_CNT_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a8_a,
	datab => DNL_CNT_a9_a,
	datac => DNL_CNT_a6_a,
	datad => DNL_CNT_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a692);

DNL_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a2_a = DFFE(DNL_TEST & add_a1625, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DNL_TEST,
	datad => add_a1625,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a2_a);

DNL_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a5_a = DFFE(DNL_TEST & add_a1613 & (!Equal_a694 # !BIT_STATE_ast_dnl0), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => Equal_a694,
	datac => DNL_TEST,
	datad => add_a1613,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a5_a);

DNL_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_CNT_a4_a = DFFE(DNL_TEST & (add_a1621), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_TEST,
	datad => add_a1621,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_CNT_a4_a);

Equal_a693_I : apex20ke_lcell
-- Equation(s):
-- Equal_a693 = DNL_CNT_a3_a & !DNL_CNT_a2_a & DNL_CNT_a5_a & !DNL_CNT_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a3_a,
	datab => DNL_CNT_a2_a,
	datac => DNL_CNT_a5_a,
	datad => DNL_CNT_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a693);

Equal_a694_I : apex20ke_lcell
-- Equation(s):
-- Equal_a694 = !DNL_CNT_a0_a & !DNL_CNT_a1_a & Equal_a692 & Equal_a693

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_CNT_a0_a,
	datab => DNL_CNT_a1_a,
	datac => Equal_a692,
	datad => Equal_a693,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a694);

UPD_PROC_a4_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a4 = BIT_STATE_ast_dnl0 & Equal_a694

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BIT_STATE_ast_dnl0,
	datad => Equal_a694,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a4);

Time_Enable_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => Time_Enable_acombout);

UPD_PROC_a3_I : apex20ke_lcell
-- Equation(s):
-- UPD_PROC_a3 = !Time_Enable_acombout # !DNL_TEST

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DNL_TEST,
	datad => Time_Enable_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => UPD_PROC_a3);

DNL_DAC_VAL_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a2_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a2_a $ !DNL_DAC_VAL_a1_a_a155, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a2_a_a158 = CARRY(DNL_DAC_VAL_a2_a & !DNL_DAC_VAL_a1_a_a155)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a2_a,
	cin => DNL_DAC_VAL_a1_a_a155,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a2_a,
	cout => DNL_DAC_VAL_a2_a_a158);

iBIT_VAL_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a2_a = DFFE(GLOBAL(BIT_EN_acombout) & add_a1567 $ iBIT_VAL_a2_a $ !iBIT_VAL_a1_a_a898, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a2_a_a901 = CARRY(add_a1567 & (iBIT_VAL_a2_a # !iBIT_VAL_a1_a_a898) # !add_a1567 & iBIT_VAL_a2_a & !iBIT_VAL_a1_a_a898)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a1567,
	datab => iBIT_VAL_a2_a,
	cin => iBIT_VAL_a1_a_a898,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a2_a,
	cout => iBIT_VAL_a2_a_a901);

Select_a1858_I : apex20ke_lcell
-- Equation(s):
-- Select_a1858 = bit_dac_DATA_a46 & DNL_DAC_VAL_a2_a & BIT_STATE_ast_dnl0 # !bit_dac_DATA_a46 & (iBIT_VAL_a2_a # DNL_DAC_VAL_a2_a & BIT_STATE_ast_dnl0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D5C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => bit_dac_DATA_a46,
	datab => DNL_DAC_VAL_a2_a,
	datac => BIT_STATE_ast_dnl0,
	datad => iBIT_VAL_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1858);

bit_dac_DATA_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a2_a_areg0 = DFFE(Select_a1858 # bit_dac_DATA_a2_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a2_a_areg0,
	datac => reduce_or_a1,
	datad => Select_a1858,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a2_a_areg0);

DNL_DAC_VAL_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a3_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a3_a $ DNL_DAC_VAL_a2_a_a158, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a3_a_a161 = CARRY(!DNL_DAC_VAL_a2_a_a158 # !DNL_DAC_VAL_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a3_a,
	cin => DNL_DAC_VAL_a2_a_a158,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a3_a,
	cout => DNL_DAC_VAL_a3_a_a161);

add_a1568_I : apex20ke_lcell
-- Equation(s):
-- add_a1568 = BIT_STATE_ast_pk1 & (peak1_a3_a_acombout) # !BIT_STATE_ast_pk1 & peak2_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EE44",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datab => peak2_a3_a_acombout,
	datad => peak1_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1568);

iBIT_VAL_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a3_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a3_a $ add_a1568 $ iBIT_VAL_a2_a_a901, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a3_a_a904 = CARRY(iBIT_VAL_a3_a & !add_a1568 & !iBIT_VAL_a2_a_a901 # !iBIT_VAL_a3_a & (!iBIT_VAL_a2_a_a901 # !add_a1568))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a3_a,
	datab => add_a1568,
	cin => iBIT_VAL_a2_a_a901,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a3_a,
	cout => iBIT_VAL_a3_a_a904);

Select_a1860_I : apex20ke_lcell
-- Equation(s):
-- Select_a1860 = BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a3_a # !bit_dac_DATA_a46 & iBIT_VAL_a3_a) # !BIT_STATE_ast_dnl0 & !bit_dac_DATA_a46 & (iBIT_VAL_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B3A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => bit_dac_DATA_a46,
	datac => DNL_DAC_VAL_a3_a,
	datad => iBIT_VAL_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1860);

bit_dac_DATA_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a3_a_areg0 = DFFE(Select_a1860 # !reduce_or_a1 & bit_dac_DATA_a3_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reduce_or_a1,
	datac => bit_dac_DATA_a3_a_areg0,
	datad => Select_a1860,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a3_a_areg0);

add_a1569_I : apex20ke_lcell
-- Equation(s):
-- add_a1569 = BIT_STATE_ast_pk1 & (peak1_a4_a_acombout) # !BIT_STATE_ast_pk1 & (peak2_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datac => peak1_a4_a_acombout,
	datad => peak2_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1569);

iBIT_VAL_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a4_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a4_a $ add_a1569 $ !iBIT_VAL_a3_a_a904, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a4_a_a907 = CARRY(iBIT_VAL_a4_a & (add_a1569 # !iBIT_VAL_a3_a_a904) # !iBIT_VAL_a4_a & add_a1569 & !iBIT_VAL_a3_a_a904)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a4_a,
	datab => add_a1569,
	cin => iBIT_VAL_a3_a_a904,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a4_a,
	cout => iBIT_VAL_a4_a_a907);

Select_a1862_I : apex20ke_lcell
-- Equation(s):
-- Select_a1862 = DNL_DAC_VAL_a4_a & (BIT_STATE_ast_dnl0 # !bit_dac_DATA_a46 & iBIT_VAL_a4_a) # !DNL_DAC_VAL_a4_a & !bit_dac_DATA_a46 & (iBIT_VAL_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B3A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a4_a,
	datab => bit_dac_DATA_a46,
	datac => BIT_STATE_ast_dnl0,
	datad => iBIT_VAL_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1862);

bit_dac_DATA_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a4_a_areg0 = DFFE(Select_a1862 # !reduce_or_a1 & bit_dac_DATA_a4_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reduce_or_a1,
	datac => bit_dac_DATA_a4_a_areg0,
	datad => Select_a1862,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a4_a_areg0);

add_a1570_I : apex20ke_lcell
-- Equation(s):
-- add_a1570 = BIT_STATE_ast_pk1 & (peak1_a5_a_acombout) # !BIT_STATE_ast_pk1 & peak2_a5_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => peak2_a5_a_acombout,
	datac => BIT_STATE_ast_pk1,
	datad => peak1_a5_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1570);

iBIT_VAL_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a5_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a5_a $ add_a1570 $ iBIT_VAL_a4_a_a907, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a5_a_a910 = CARRY(iBIT_VAL_a5_a & !add_a1570 & !iBIT_VAL_a4_a_a907 # !iBIT_VAL_a5_a & (!iBIT_VAL_a4_a_a907 # !add_a1570))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a5_a,
	datab => add_a1570,
	cin => iBIT_VAL_a4_a_a907,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a5_a,
	cout => iBIT_VAL_a5_a_a910);

DNL_DAC_VAL_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a5_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a5_a $ DNL_DAC_VAL_a4_a_a164, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a5_a_a167 = CARRY(!DNL_DAC_VAL_a4_a_a164 # !DNL_DAC_VAL_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a5_a,
	cin => DNL_DAC_VAL_a4_a_a164,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a5_a,
	cout => DNL_DAC_VAL_a5_a_a167);

Select_a1864_I : apex20ke_lcell
-- Equation(s):
-- Select_a1864 = BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a5_a # iBIT_VAL_a5_a & !bit_dac_DATA_a46) # !BIT_STATE_ast_dnl0 & iBIT_VAL_a5_a & (!bit_dac_DATA_a46)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => iBIT_VAL_a5_a,
	datac => DNL_DAC_VAL_a5_a,
	datad => bit_dac_DATA_a46,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1864);

bit_dac_DATA_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a5_a_areg0 = DFFE(Select_a1864 # bit_dac_DATA_a5_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a5_a_areg0,
	datac => reduce_or_a1,
	datad => Select_a1864,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a5_a_areg0);

DNL_DAC_VAL_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a6_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a6_a $ !DNL_DAC_VAL_a5_a_a167, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a6_a_a170 = CARRY(DNL_DAC_VAL_a6_a & !DNL_DAC_VAL_a5_a_a167)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a6_a,
	cin => DNL_DAC_VAL_a5_a_a167,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a6_a,
	cout => DNL_DAC_VAL_a6_a_a170);

peak1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak1(6),
	combout => peak1_a6_a_acombout);

add_a1571_I : apex20ke_lcell
-- Equation(s):
-- add_a1571 = BIT_STATE_ast_pk1 & (peak1_a6_a_acombout) # !BIT_STATE_ast_pk1 & (peak2_a6_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datac => peak1_a6_a_acombout,
	datad => peak2_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1571);

iBIT_VAL_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a6_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a6_a $ add_a1571 $ !iBIT_VAL_a5_a_a910, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a6_a_a913 = CARRY(iBIT_VAL_a6_a & (add_a1571 # !iBIT_VAL_a5_a_a910) # !iBIT_VAL_a6_a & add_a1571 & !iBIT_VAL_a5_a_a910)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a6_a,
	datab => add_a1571,
	cin => iBIT_VAL_a5_a_a910,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a6_a,
	cout => iBIT_VAL_a6_a_a913);

Select_a1866_I : apex20ke_lcell
-- Equation(s):
-- Select_a1866 = BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a6_a # !bit_dac_DATA_a46 & iBIT_VAL_a6_a) # !BIT_STATE_ast_dnl0 & !bit_dac_DATA_a46 & (iBIT_VAL_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B3A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => bit_dac_DATA_a46,
	datac => DNL_DAC_VAL_a6_a,
	datad => iBIT_VAL_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1866);

bit_dac_DATA_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a6_a_areg0 = DFFE(Select_a1866 # !reduce_or_a1 & bit_dac_DATA_a6_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => reduce_or_a1,
	datac => bit_dac_DATA_a6_a_areg0,
	datad => Select_a1866,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a6_a_areg0);

peak2_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_peak2(7),
	combout => peak2_a7_a_acombout);

add_a1572_I : apex20ke_lcell
-- Equation(s):
-- add_a1572 = BIT_STATE_ast_pk1 & (peak1_a7_a_acombout) # !BIT_STATE_ast_pk1 & (peak2_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datac => peak1_a7_a_acombout,
	datad => peak2_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1572);

iBIT_VAL_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a7_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a7_a $ add_a1572 $ iBIT_VAL_a6_a_a913, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a7_a_a916 = CARRY(iBIT_VAL_a7_a & !add_a1572 & !iBIT_VAL_a6_a_a913 # !iBIT_VAL_a7_a & (!iBIT_VAL_a6_a_a913 # !add_a1572))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a7_a,
	datab => add_a1572,
	cin => iBIT_VAL_a6_a_a913,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a7_a,
	cout => iBIT_VAL_a7_a_a916);

DNL_DAC_VAL_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a7_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a7_a $ DNL_DAC_VAL_a6_a_a170, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a7_a_a173 = CARRY(!DNL_DAC_VAL_a6_a_a170 # !DNL_DAC_VAL_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a7_a,
	cin => DNL_DAC_VAL_a6_a_a170,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a7_a,
	cout => DNL_DAC_VAL_a7_a_a173);

Select_a1868_I : apex20ke_lcell
-- Equation(s):
-- Select_a1868 = BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a7_a # iBIT_VAL_a7_a & !bit_dac_DATA_a46) # !BIT_STATE_ast_dnl0 & iBIT_VAL_a7_a & (!bit_dac_DATA_a46)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_dnl0,
	datab => iBIT_VAL_a7_a,
	datac => DNL_DAC_VAL_a7_a,
	datad => bit_dac_DATA_a46,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1868);

bit_dac_DATA_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a7_a_areg0 = DFFE(Select_a1868 # bit_dac_DATA_a7_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a7_a_areg0,
	datac => reduce_or_a1,
	datad => Select_a1868,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a7_a_areg0);

DNL_DAC_VAL_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a8_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a8_a $ !DNL_DAC_VAL_a7_a_a173, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a8_a_a176 = CARRY(DNL_DAC_VAL_a8_a & !DNL_DAC_VAL_a7_a_a173)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a8_a,
	cin => DNL_DAC_VAL_a7_a_a173,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a8_a,
	cout => DNL_DAC_VAL_a8_a_a176);

add_a1573_I : apex20ke_lcell
-- Equation(s):
-- add_a1573 = BIT_STATE_ast_pk1 & (peak1_a8_a_acombout) # !BIT_STATE_ast_pk1 & (peak2_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_STATE_ast_pk1,
	datac => peak1_a8_a_acombout,
	datad => peak2_a8_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1573);

iBIT_VAL_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- iBIT_VAL_a8_a = DFFE(GLOBAL(BIT_EN_acombout) & iBIT_VAL_a8_a $ add_a1573 $ !iBIT_VAL_a7_a_a916, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , iBIT_VAL_a15_a_a942)
-- iBIT_VAL_a8_a_a919 = CARRY(iBIT_VAL_a8_a & (add_a1573 # !iBIT_VAL_a7_a_a916) # !iBIT_VAL_a8_a & add_a1573 & !iBIT_VAL_a7_a_a916)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a8_a,
	datab => add_a1573,
	cin => iBIT_VAL_a7_a_a916,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	ena => iBIT_VAL_a15_a_a942,
	sclr => ALT_INV_BIT_EN_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iBIT_VAL_a8_a,
	cout => iBIT_VAL_a8_a_a919);

Select_a1870_I : apex20ke_lcell
-- Equation(s):
-- Select_a1870 = bit_dac_DATA_a46 & BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a8_a # !bit_dac_DATA_a46 & (iBIT_VAL_a8_a # BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D5C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => bit_dac_DATA_a46,
	datab => BIT_STATE_ast_dnl0,
	datac => DNL_DAC_VAL_a8_a,
	datad => iBIT_VAL_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1870);

bit_dac_DATA_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a8_a_areg0 = DFFE(Select_a1870 # !reduce_or_a1 & bit_dac_DATA_a8_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => reduce_or_a1,
	datac => bit_dac_DATA_a8_a_areg0,
	datad => Select_a1870,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a8_a_areg0);

DNL_DAC_VAL_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a9_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a9_a $ (DNL_DAC_VAL_a8_a_a176), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a9_a_a179 = CARRY(!DNL_DAC_VAL_a8_a_a176 # !DNL_DAC_VAL_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a9_a,
	cin => DNL_DAC_VAL_a8_a_a176,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a9_a,
	cout => DNL_DAC_VAL_a9_a_a179);

Select_a1872_I : apex20ke_lcell
-- Equation(s):
-- Select_a1872 = iBIT_VAL_a9_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a9_a # !bit_dac_DATA_a46) # !iBIT_VAL_a9_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F222",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a9_a,
	datab => bit_dac_DATA_a46,
	datac => BIT_STATE_ast_dnl0,
	datad => DNL_DAC_VAL_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1872);

bit_dac_DATA_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a9_a_areg0 = DFFE(Select_a1872 # !reduce_or_a1 & bit_dac_DATA_a9_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reduce_or_a1,
	datac => bit_dac_DATA_a9_a_areg0,
	datad => Select_a1872,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a9_a_areg0);

DNL_DAC_VAL_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a10_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a10_a $ !DNL_DAC_VAL_a9_a_a179, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a10_a_a182 = CARRY(DNL_DAC_VAL_a10_a & !DNL_DAC_VAL_a9_a_a179)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a10_a,
	cin => DNL_DAC_VAL_a9_a_a179,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a10_a,
	cout => DNL_DAC_VAL_a10_a_a182);

add_a1574_I : apex20ke_lcell
-- Equation(s):
-- add_a1574 = BIT_STATE_ast_pk1 & (peak1_a9_a_acombout) # !BIT_STATE_ast_pk1 & peak2_a9_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_STATE_ast_pk1,
	datac => peak2_a9_a_acombout,
	datad => peak1_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a1574);

Select_a1874_I : apex20ke_lcell
-- Equation(s):
-- Select_a1874 = bit_dac_DATA_a46 & BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a10_a # !bit_dac_DATA_a46 & (iBIT_VAL_a10_a # BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D5C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => bit_dac_DATA_a46,
	datab => BIT_STATE_ast_dnl0,
	datac => DNL_DAC_VAL_a10_a,
	datad => iBIT_VAL_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1874);

bit_dac_DATA_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a10_a_areg0 = DFFE(Select_a1874 # !reduce_or_a1 & bit_dac_DATA_a10_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => reduce_or_a1,
	datac => bit_dac_DATA_a10_a_areg0,
	datad => Select_a1874,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a10_a_areg0);

DNL_DAC_VAL_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a11_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a11_a $ (DNL_DAC_VAL_a10_a_a182), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a11_a_a185 = CARRY(!DNL_DAC_VAL_a10_a_a182 # !DNL_DAC_VAL_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DNL_DAC_VAL_a11_a,
	cin => DNL_DAC_VAL_a10_a_a182,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a11_a,
	cout => DNL_DAC_VAL_a11_a_a185);

Select_a1876_I : apex20ke_lcell
-- Equation(s):
-- Select_a1876 = iBIT_VAL_a11_a & (DNL_DAC_VAL_a11_a & BIT_STATE_ast_dnl0 # !bit_dac_DATA_a46) # !iBIT_VAL_a11_a & DNL_DAC_VAL_a11_a & BIT_STATE_ast_dnl0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a11_a,
	datab => DNL_DAC_VAL_a11_a,
	datac => BIT_STATE_ast_dnl0,
	datad => bit_dac_DATA_a46,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1876);

bit_dac_DATA_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a11_a_areg0 = DFFE(Select_a1876 # bit_dac_DATA_a11_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a11_a_areg0,
	datac => reduce_or_a1,
	datad => Select_a1876,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a11_a_areg0);

DNL_DAC_VAL_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a12_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a12_a $ !DNL_DAC_VAL_a11_a_a185, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a12_a_a188 = CARRY(DNL_DAC_VAL_a12_a & !DNL_DAC_VAL_a11_a_a185)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a12_a,
	cin => DNL_DAC_VAL_a11_a_a185,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a12_a,
	cout => DNL_DAC_VAL_a12_a_a188);

Select_a1878_I : apex20ke_lcell
-- Equation(s):
-- Select_a1878 = iBIT_VAL_a12_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a12_a # !bit_dac_DATA_a46) # !iBIT_VAL_a12_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F222",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a12_a,
	datab => bit_dac_DATA_a46,
	datac => BIT_STATE_ast_dnl0,
	datad => DNL_DAC_VAL_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1878);

bit_dac_DATA_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a12_a_areg0 = DFFE(Select_a1878 # !reduce_or_a1 & bit_dac_DATA_a12_a_areg0, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF50",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reduce_or_a1,
	datac => bit_dac_DATA_a12_a_areg0,
	datad => Select_a1878,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a12_a_areg0);

DNL_DAC_VAL_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a13_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a13_a $ DNL_DAC_VAL_a12_a_a188, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a13_a_a191 = CARRY(!DNL_DAC_VAL_a12_a_a188 # !DNL_DAC_VAL_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a13_a,
	cin => DNL_DAC_VAL_a12_a_a188,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a13_a,
	cout => DNL_DAC_VAL_a13_a_a191);

Select_a1880_I : apex20ke_lcell
-- Equation(s):
-- Select_a1880 = iBIT_VAL_a13_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a13_a # !bit_dac_DATA_a46) # !iBIT_VAL_a13_a & BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CE0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a13_a,
	datab => BIT_STATE_ast_dnl0,
	datac => bit_dac_DATA_a46,
	datad => DNL_DAC_VAL_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1880);

bit_dac_DATA_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a13_a_areg0 = DFFE(Select_a1880 # bit_dac_DATA_a13_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0FC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a13_a_areg0,
	datac => Select_a1880,
	datad => reduce_or_a1,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a13_a_areg0);

DNL_DAC_VAL_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a14_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a14_a $ !DNL_DAC_VAL_a13_a_a191, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )
-- DNL_DAC_VAL_a14_a_a194 = CARRY(DNL_DAC_VAL_a14_a & !DNL_DAC_VAL_a13_a_a191)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DNL_DAC_VAL_a14_a,
	cin => DNL_DAC_VAL_a13_a_a191,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a14_a,
	cout => DNL_DAC_VAL_a14_a_a194);

Select_a1882_I : apex20ke_lcell
-- Equation(s):
-- Select_a1882 = iBIT_VAL_a14_a & (BIT_STATE_ast_dnl0 & DNL_DAC_VAL_a14_a # !bit_dac_DATA_a46) # !iBIT_VAL_a14_a & BIT_STATE_ast_dnl0 & (DNL_DAC_VAL_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CE0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a14_a,
	datab => BIT_STATE_ast_dnl0,
	datac => bit_dac_DATA_a46,
	datad => DNL_DAC_VAL_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1882);

bit_dac_DATA_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a14_a_areg0 = DFFE(Select_a1882 # bit_dac_DATA_a14_a_areg0 & !reduce_or_a1, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0FC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a14_a_areg0,
	datac => Select_a1882,
	datad => reduce_or_a1,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a14_a_areg0);

DNL_DAC_VAL_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DNL_DAC_VAL_a15_a = DFFE(!GLOBAL(UPD_PROC_a3) & DNL_DAC_VAL_a14_a_a194 $ DNL_DAC_VAL_a15_a, GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DNL_DAC_VAL_a15_a,
	cin => DNL_DAC_VAL_a14_a_a194,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	sclr => UPD_PROC_a3,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DNL_DAC_VAL_a15_a);

Select_a1884_I : apex20ke_lcell
-- Equation(s):
-- Select_a1884 = iBIT_VAL_a15_a & bit_dac_DATA_a46 & (!BIT_STATE_ast_dnl0 # !DNL_DAC_VAL_a15_a) # !iBIT_VAL_a15_a & (!BIT_STATE_ast_dnl0 # !DNL_DAC_VAL_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3F15",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iBIT_VAL_a15_a,
	datab => DNL_DAC_VAL_a15_a,
	datac => BIT_STATE_ast_dnl0,
	datad => bit_dac_DATA_a46,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1884);

bit_dac_DATA_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- bit_dac_DATA_a15_a_areg0 = DFFE(Select_a1884 & (bit_dac_DATA_a15_a_areg0 # reduce_or_a1), GLOBAL(clk20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => bit_dac_DATA_a15_a_areg0,
	datac => reduce_or_a1,
	datad => Select_a1884,
	clk => clk20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => bit_dac_DATA_a15_a_areg0);

LD_bit_dac_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => LD_bit_dac_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LD_bit_dac);

bit_dac_DATA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(0));

bit_dac_DATA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(1));

bit_dac_DATA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(2));

bit_dac_DATA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(3));

bit_dac_DATA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(4));

bit_dac_DATA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(5));

bit_dac_DATA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(6));

bit_dac_DATA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(7));

bit_dac_DATA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(8));

bit_dac_DATA_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(9));

bit_dac_DATA_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(10));

bit_dac_DATA_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(11));

bit_dac_DATA_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(12));

bit_dac_DATA_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(13));

bit_dac_DATA_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(14));

bit_dac_DATA_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => bit_dac_DATA_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_bit_dac_DATA(15));
END structure;


