---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	BITDAC.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 16, 1999
--   Board          : FAD FPGA
--   Schematic      : 4035.007.20700
--
--   FPGA Code P/N  : 4035.009.99880 - SOFTWARE, FA-D FPGA
--   FPGA ROM  P/N  : 9335.078.00727 - ALTERA EPC2LC20
--   FPGA      P/N  : 9335.078.00735 - FPF20K100AQC240-3
--
--   Description:	
--		Module to generate the BIT DAC outputs based on 4 parameters
--			CPS, PK2D, PK1 and PK2
--
--
--
--
--		--- 		     	+---------+
--		 ^	     		|         |
-- PK1+PK2 |	     		|         |
--		 |	     		|         |
--         |		          |         |
--	     ---  +--------------+         |         +------------
--	  PK1 ^   |         			|         |
--         |   |         			|         |
--	----------+         			+---------+
--   |<-CPS--->|<--PK2D------>|<-CPS--->|<-CPS--->|
--
--
--   History:       <date> - <Author>
--		03/16/06 - MCS: Ver 4010
--			Added DNL Test when Peak1 = 1 and Peak2 = 0
--			will output 1000 of the same value before incrementing to the next
--		12/05/05 - MCS
--			Ver 357D: Created BIT_Disable which is set when PEak1 AND peak2 are 0
--			which prevents the sequencer from running.
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		01/19/01 - Ver 0.95
--			Full USB Read/Write Support
--			LAst Time Constant in NVRam Support
--		11/20/00 - Ver 0.70
--			Polarity of SW_PSLOPE was reversed 
--				OFF = Negative/ ON= Positive Slope
--		11/16/00 - Ver 0.69 
--			Initial Code that appears to be fully operational
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--library LPM;
--     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity bit_dac is 
	port( 
	     IMR            : in      std_logic;				 	-- Master Reset
     	clk20          : in      std_logic;                     	-- Master Clock
		BIT_EN		: in		std_Logic;					-- BIT Enable
		Time_Enable	: in		std_logic;
	     peak1     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 1 Value
     	peak2     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 2 Value
	     cps       	: in      std_logic_vector( 15 downto 0 );	-- CPS Value
     	pk2dly    	: in      std_logic_vector( 15 downto 0 );   -- Peak2 Delay Ver 523
		LD_bit_dac	: buffer	std_logic;					-- Load BIT DAC
		bit_dac_DATA	: buffer	std_logic_vector( 15 downto 0 ) );	-- BIT DAC Data
	end bit_dac;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of bit_dac is
     -- Constant Declarations
	constant us1_cnt_max	: integer := 19;
	constant DNL_CNT_MAX 	: integer := 1000;

	type DAC_State is (
		St_Idle,
		St_Pre,
		St_PK1,
		St_PK2d,
		St_PK2,
		ST_DNL0,
		ST_DNL1,
		ST_DNL2,
		ST_DNL3 );

     -- Registered Signals
	signal BIT_STATE 		: DAC_State;
	signal BIT_CNT			: std_logic_vector( 15 downto 0 );
	signal BIT_CNT_TC		: std_logic;
	signal us1_cnt_Tc 		: std_logic;
	signal us1_cnt			: integer range 0 to us1_cnt_max;
	signal PK2_MODE		: std_Logic;
	signal iLD_bit_dac		: std_logic;
	signal iBIT_VAL		: std_Logic_Vector( 15 downto 0 );
	signal Bit_Disable		: std_logic;

	signal DNL_TEST		: std_Logic;	
	signal DNL_CNT			: integer range 0 to DNL_CNT_MAX;
	signal DNL_CNT_TC		: std_logic;
	signal DNL_DAC_VAL		: std_logic_Vector( 15 downto 0 );
begin
	us1_cnt_tc 		<= '1' when ( us1_cnt = us1_cnt_max ) else '0';
	DNL_CNT_TC		<= '1' when ( DNL_CNT = DNL_CNT_MAX ) else '0';
	bit_cnt_tc 		<= '1' when (( BIT_STATE = ST_PK2D ) and ( bit_cnt = Pk2Dly )) else 
	             		   '1' when (( BIT_STATE /= ST_PK2D ) and ( Bit_Cnt = CPS )) else
	              		   '0';
   ----------------------------------------------------------------------------------------------
     UPD_PROC : process( clk20, IMR )
     begin
          if( IMR = '1' ) then
			DNL_TEST		<= '0';
			Bit_Disable	<= '0';
			PK2_Mode		<= '0';
			bit_Cnt		<= x"0000";
			bit_dac_data 	<= x"0000";
			iBit_Val		<= x"0000";

			LD_bit_dac 	<= '0';
			iLD_bit_dac	<= '0';
			DNL_DAC_VAL 	<= x"0000";
			DNL_CNT		<= 0;
			
			BIT_STATE		<= ST_IDLE;
			us1_Cnt		<= 0;
          elsif(( clk20'Event ) and ( clk20 = '1' )) then
			if(( Conv_Integer( Peak1 ) = 1 ) and ( Conv_Integer( Peak2 ) = 0 ))
				then DNL_TEST <= '1';
				else DNL_TEST <= '0';
			end if;
			
			if(( Conv_Integer( Peak1 ) = 0 ) and ( COnv_Integer( Peak2 ) = 0 ))
				then Bit_Disable <= '1';
				else Bit_Disable <= '0';			
			end if;
			
			if( Conv_Integer( Peak2 ) = 0 )
				then Pk2_Mode <= '0';
				else Pk2_Mode <= '1';
			end if;

			if(( Bit_En = '0' ) or ( Bit_Disable = '1' ))
				then Bit_Cnt <= x"0000";
			else
				case BIT_STATE is
					when ST_IDLE	=> Bit_Cnt <= x"0000";
					when ST_PRE 	=> if( US1_Cnt_Tc = '1' ) then Bit_Cnt <= Bit_Cnt + 1; end if;
					when ST_PK1	=> Bit_Cnt <= x"0000";
					when ST_PK2D 	=> if( US1_Cnt_Tc = '1' ) then Bit_Cnt <= Bit_Cnt + 1; end if;
					when ST_PK2	=> Bit_Cnt <= x"0000";
					when ST_DNL0	=> Bit_Cnt <= x"0000";
					when ST_DNL1 	=> if( US1_Cnt_Tc = '1' ) then Bit_Cnt <= Bit_Cnt + 1; end if;
					when ST_DNL2	=> Bit_Cnt <= x"0000";
					when ST_DNL3 	=> if( US1_Cnt_Tc = '1' ) then Bit_Cnt <= Bit_Cnt + 1; end if;
					when others	=>
				end case;
			end if;
					
			case BIT_STATE is
				when ST_PK1 	=> iLD_BIT_DAC <= '1'; Bit_Dac_Data <= not( iBIT_VAL(15)) & ibit_Val( 14 downto 0 );
				when ST_PK2 	=> iLD_BIT_DAC <= '1'; Bit_Dac_Data <= not( iBIT_VAL(15)) & ibit_Val( 14 downto 0 );
				when ST_DNL0 	=> iLD_BIT_DAC <= '1'; Bit_Dac_Data <= not( DNL_DAC_VAL(15)) & DNL_DAC_VAL( 14 downto 0 );
				when ST_DNL2 	=> iLD_BIT_DAC <= '1'; Bit_Dac_Data <= x"8000";
				when others	=> iLD_BIT_DAC <= '0'; 
			end case;

			LD_bit_dac	<= iLD_bit_dac;

			if( BIT_EN = '0' )
				then iBIT_VAL <= x"0000";
			else
				case BIT_STATE is
					when ST_PK1 => iBIT_VAL <= iBIT_VAL + ( x"0" & Peak1 );
					when ST_PK2 => iBIT_VAL <= iBIT_VAL + ( x"0" & Peak2 );
					when others =>
				end case;
			end if;


  
			if( us1_cnt_tc = '1' )
				then us1_cnt <= 0;
				else us1_cnt <= us1_cnt + 1;
			end if;


			if(( DNL_TEST = '0' ) or ( Time_Enable = '0' ))
				then DNL_DAC_VAL <= x"0000";
			elsif(( BIT_STATE = ST_DNL0 ) and ( DNL_CNT_TC = '1' ))
				then DNL_DAC_VAL <= DNL_DAC_VAL + 1;
			end if;
			
			if( DNL_TEST = '0' )
				then DNL_CNT <= 0;
			elsif(( BIT_STATE = ST_DNL0 ) and ( DNL_CNT_TC = '1' ))
				then DNL_CNT <= 0;
			elsif(( BIT_STATE = ST_DNL0 ) and ( DNL_CNT_TC = '0' ))
				then DNL_CNT <= DNL_CNT + 1;
			end if;
			
			case BIT_STATE is
				when St_Idle 	=>
					if(( BIT_EN = '1' ) and ( Bit_Disable = '0' )) then
						if( DNL_TEST = '0' )
							then BIT_STATE <= ST_PRE;
							else BIT_STATE <= ST_DNL0;
						end if;
					end if;
					
				when St_Pre	=> 
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ))
						then BIT_STATE <= ST_IDLE;
					elsif(( BIT_CNT_TC = '1' ) and ( us1_cnt_tc = '1' ))
						then BIT_STATE <= ST_PK1;
					end if;
					
				when St_PK1	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '1' ))
						then BIT_STATE <= ST_IDLE;
					elsif( PK2_MODE = '1' )
						then BIT_STATE <= St_PK2D;
						else BIT_STATE <= ST_Pre;
					end if;
					
				when St_PK2d	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '1' ))
						then BIT_STATE <= ST_IDLE;
					elsif(( BIT_CNT_TC = '1' ) and ( us1_cnt_tc = '1' ))
						then BIT_STATE <= ST_PK2;
					end if;
					
				when St_PK2	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '1' ))
						then BIT_STATE <= ST_IDLE;
						else BIT_STATE <= st_Pre;
					end if;
				
				-- DNL Test Sequencer ----------------------------------------------------
				when ST_DNL0 	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '0' ))
						then BIT_STATE <= ST_IDLE;
						else BIT_STATE <= ST_DNL1;
					end if;
					
				when ST_DNL1	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '0' ))
						then BIT_STATE <= ST_IDLE;
					elsif(( BIT_CNT_TC = '1' ) and ( us1_cnt_tc = '1' ))
						then BIT_STATE <= ST_DNL2;
					end if;

				when ST_DNL2 	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '0' ))
						then BIT_STATE <= ST_IDLE;
						else BIT_STATE <= ST_DNL3;
					end if;

				when ST_DNL3	=>
					if(( BIT_EN = '0' ) or ( Bit_Disable = '1' ) or ( DNL_TEST = '0' ))
						then BIT_STATE <= ST_IDLE;
					elsif(( BIT_CNT_TC = '1' ) and ( us1_cnt_Tc = '1' ))
						then BIT_STATE <= ST_DNL0;
					end if;
				-- DNL Test Sequencer ----------------------------------------------------

				when others	=>
					BIT_STATE <= ST_IDLE;
			end case;
		end if;
     end process;
     ----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- bit_dac.VHD
---------------------------------------------------------------------------------------------------

