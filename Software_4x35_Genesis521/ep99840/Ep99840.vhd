----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	EP99840.VHD ( aka EDSFPGA.VHD)
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev D
--
--	Top Level		: 4035.009.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module takes monitors the positive/negative slope as well as the Baseline Trigger
--        It looks for a rising slope above the baseline trigger and makes sure that the pulse
--        width is greater that the specified risetime parameter.
--        It has an output that indicates Invalid Rise Time so the stream will be rejected.
--
--
--	
--
-- See EDI-II Notebook page 72 
-- DSP Data Read Timeing:
--	1 Wait State
--					 |<-- 33ns-->|
--            +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     
-- DSP_H3     |     |     |     |     |     |     |     |     |     |     |     |
--       -----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----
--
--                   ---->|    |<--- 7ns          |
--
--	    -------------------\ /----------------------\ /------------------------------
-- ADDR                      x                        x
--       -------------------/ \----------------------/ \------------------------------
--
--                             |            -->|  |<--- 10ns
--                             |             /---------\
-- DATA  -----------------------------------x           x-----------------------------
--                             |             \---------/
--                             |<-- Tpd max -->| = ( 2x 33ns ) - 7ns - 10ns = 49ns, 80% = 40ns
--						
--
--
--   History:       <date> - <Author> - Ver
--		04/24/07 - MCS - Ver 4x30
--			Apollo 10 Disable 2X Gain Switch
--		04/23/07 - MCS - Ver 4x2F
--			Apollo 10 Enable 2X Gain Switch ( again )
--		04/03/07 - MCS - Ver 4x2C
--			Apollo 10 Enable 2x gain switch
--		03/21/07 - MCS - Ver 4x2B
--			Disc is inverted ( confirmend with scope on U10-1 on DPP2 FR board ).
--
--		03/14/07 - MCS - Ver 4x2A
--			New Detector type DET_204NFR with a code of 5. This type will not throw the x2 gain switch
--			type DET_204 was renamed to DET_204FR
--			
--		03/02/07 - MCS - Ver 4x29
--			PA_MODE(3) had to be inverted for Det type DET_204
--			Disc_Inv(0) is to not be inverted from AD_Invert
--		02/22/07 - MCS - Ver 4x28
--			New Register DET_TYPE at address 0x63 in the FPGA used to contain more detector types	
-- 				DET_204				= 0 ( Existing )
-- 				DET_UNICORN			= 1 ( Existing )
-- 				DET_AP40 				= 2 ( Existing )
-- 				DET_WDS( LEX or TEX ) 	= 3 ( Existing )
-- 				DET_AP10				= 4 � New Decode
--			Added "AD_INVERT" to be set when the A/D input is to be inverted, 
--			which will invert to module AD9240 as well as set the appropraite bits
--			for the SHDisc Inversion
--		10/02/06 - MCS - Ver 4x28
--			Correct a bug in which the TEMP_SENSE from the 204 preamp makes it way through the hot-swap control and out to HV_DIS(1)
--			which has been tied to the RTEM_RED led input.
--			THe fix will force that output to 0 when a motorized slide is selected otherwise
--			use HOT_SWAP or PA_RESET_GEN
--			Fix will also require going into rTEM Control and change the "RTEM_SEL" to reflect, None, rTEM, UMS2
--
-- 		08/23/06 - MCS - Ver 4x27
--			Added a RESET_GEN_CNT_MAX to limit the pulse width for the reset set to 200us
--		08/22/06 - MCS - Ver 4x26: Module AD9240.VHD
--			Max/Min Adjust threshold changed to original method due to reset being stuck active with a ramp that is too small
--			seen by increased gain from VItus SDD

--		08/11/06 - MCS - Ver 4x25: Module AD9240.VHD
--			Added the circuit to force a reset generate if the ramp is saturated,
--			ie no reset for some time..
--
--		***** 08/10/06 - Ver 4x24 ***** GENESIS 5.1 Release Candidate *******************************************
--
--		08/10/06 - MCS Ver 4x24 - Genesis 5.1 Release Candidate
--			ADDED OTR_EN which is disabled (0) when Preset Mode is Live Spectrum Mapping or WDS Modes
--			otherwise it is enabled ( Per Laszlo's request )
--		08/08/06 : MCS Ver 4x23 
--			Module AD9240: Added Hysterysis to Preamp Reset
--		08/09/06 - DSP: MCS Ver 4x23													
--			Edsutil.C : SetROISCARM( int ROI_NUM ) changed the address to the SCA Look-up memory 
--			to be just the index not 1/2 the index										
--		07/05/06 : MCS Ver 4x22
--			Changes made in FIFO in DSO mode to facilitate better DSO and FFT Modes of operation
--			DSO Mode given its own keys in the FIFO
--			FF_CNT incoroprated to count the number of words written to the FIFO only for DSO Mode
--			each 1/8 of full fifo will cause na interrupt to the DSP to start reading out the FIFO Data
--		06/30/06 : MCS Ver 4x20
--			Not a complete version
--		06/26/06 : MCS Ver 4x19
--			Preset_DOne_Cnt_Reg is enabled when PRESET_OUT = 0 AND PS_OUT_LSM = 0
--			in other words, allowed to be updated when not near the Preset Done Condition
--			Set order of Peak Data higher priority then Preset Done
--			FIFO Word counter added to maintain the number of words written to the FIFO
--			during DSO Mode
--		5/22/06: MCS Ver 4x16
--			RT_IN(0) held at a logic low to force the RT Bus pin 5 to be tristate ( by nature 
--			of the open collector device and the pullup resistor )
--		5/16/06: MCS Ver 4015
--			Added logic to stop clock/live time in fir_cntrate when the ADC is out-of-range
--			Added logic to prevent DET_SAT in PA_RESET_DET when ADC is less than 1/2 scale
--		4/4/06: MCS Ver 4011 (cont)
--			Corrected DSO Mode for Raw A/D data to be signed binary to 14-bits.
--		3/27/06: MCS Ver 4011
--			Rebuilt to Quartus 5.1 SP 2
--		03/16/06 - MCS: Ver 4010
--			Added DNL Test when Peak1 = 1 and Peak2 = 0
--			will output 1000 of the same value before incrementing to the next
--		01/24/06 : MCS: Ver 4007
--			EvCh DAC Settings moved into this file ( from DSP )
--			Which Selects either Unicorn Detector with its DAC values
--			or the 204 Detector with its own set of DAC values.
--		01/12/06 : MCS: Ver 4004
--			WDS Code Changes
--			Offset moved to FIR_BLR from FIR_PPROC
--		01/09/06 : MCS: Ver 4003
--			AD9240 - Adaptive Reset added to automatically adjust the threshold levels to keep
--			the ramp in the 90% range of Rmin and Rmax
--		12/20/05 - MCS: Ver 4101
--			Version is in the range 41xx where the 1 indicates SILI Code
--			WDS moved to 99841 wthe Version 42xx
--			DSP Version to 4001
--		12/15/05 - MCS: Ver 3581
--			Module AD9240: AD_MAXMIN incorporated into this module
--			Fine Gain added 25%
--		12/13/05 - MCS: Ver 3580
--			Module AD9240: AD_Sel removed from module, and debug_ad always A/D output
--			Module AD_MAXMIN: Ramp_Mode = 0 added "Clamp off" any PA_RESET
--
--		12/12/05 - MCS - Ver 357F
--			AD9240 module as altered to keep the sum of 2 samples @ 20Mhz from a 10MHZ convereter
--			delivering 15 bits at 20Mhz instead of 14 bits at 10Mhz
--			FIR_GEN2 module: Mux_Start returned back to 1
--			FIR_PPROC Module: eliminated FIne Gain Multiplier which was moved to ad9240
--			AD_MAXMin Module: Ramp Mode added to use "PA_RESET" for edge detects for MAx/Min else just the decay
--			Side affect, Coarse Gain at 102.4us set to 50K, Preamp Gain and thresholds must be changed
--			
--
--		12/08/05 - MCS - Ver 357E
--			Fine Gain was removed ( or held fixed ) by mult max_fir by 16384 (4000hex)
--			but the multiplier was moved to the AD9240 module. 
--			Apply the fine gain to the input ramp, then run it through the filter
--			but leave the zero at the tail end 
--		12/05/05 - MCS
--			Ver 357D: Created BIT_Disable which is set when PEak1 AND peak2 are 0
--			which prevents the sequencer from running.
--		10/12/05 - MCS - Ver 3576
--			Inverted SDD_PUR_IN to support new Ketek AXAS
--		10/11/05 - MCS - Ver 3575
--			Updated FIR_WDS, and FIR_AC_COUPLE to support WDS
--		09/01/05 - MCS - Ver 3571
--			Non Linear Correction changed to 9 bits
--		08/17/05 - MCS - Ver 3570
--			No changes, just recompiled on new workstation
--		08/03/05 - MCS - Ver 356F
--			Enabled Detector Reset Circuitry
--		7/25/05 - 356E
--			Nonlinearity range changed from +/-8 channels to +/- 16 channels
--			Values from 0 t0 127 shift up in 1/8 channel increments
--			values from -1 to -128 shift down in 1/8 channel increments
--		7/18/05 - 356D
--			KSDD Mode changed not to be inverted to handle unipolar pulses from A/D


--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		October 19, 2004 - 355E
--			RTEMCTRL
--				Modified UMS2 module to ignore the hi-cnt and det_sat input for 5 seconds after 
--				the UMS-2 stops moving ( via falling edge detect of UMS2_ENABLE )
--		August 13, 2004 - 355B
--			FIR_PSTEER
--				Added TC_SEL into module
--				Added DISC_IN4_DIS blocking pulse to block secondary Energy Discriminator pulse from asserting PUR 
--				for the lower amp times (< 25.6us ). 
--				Noise riding on the BLM Filter output seems to be causing second pulses for low energy spectrum (C),
--				this blocking function will vary with amp time 1/4
--		July 29, 2003 - MCS 3556
--			FIR_RMS Acc_Width decreased by 4 to mult the RMS value by 16
--		June 9, 2004 - MCS 3555
--			added module FIR_RMS to FIR_BLOCK to create an average of the baseline for 1 second
--			before "double-buffering" it so it can be read.
--			It only looks at the lower 8 bits of the "baseline corrected" filter output
--			in the absense of a pulse (pbusy).
--			The data is available at address iADR_RMS_OUT
--		January 29, 2004 3545
--		Module; FIR_BLR
--			CLK_CNT_MAX has been changed to be equal to the Amp Time which is used to determine 
--			how often the baseline is samples for the baseline histogram.
--			Baseline Histogram should not be use while collecting spectral data.
--		Additioanlly the LSB weighting of the baseline spectrum was changed from 10eV (or 10eV/Ch)
--			to 5eV/Ch or 2.5eV/Ch
--		April 5, 2004 - MCS
--			Additional test points were added to the DSO Decode 19 to measure PA Reset and Super High Speed Disc Delay
--		August 23, 2001 - MCS
--			Added Decode of Input from Cross Port, if Bit 15 is Set starts "START_VEC" which
--			First issues a TIME_CLR then a TIME_START			
--		November 27, 2000 - MCS - Ver 3.201
--			AD_OVR removed from the PEAK_DONE term
--			and moved to the Meas_Done Term
--		*******************************************************************************
--		September 22, 2000 - MCS:
--			Final Release
--		*******************************************************************************
-----------------------------------------------------------------------------------------


library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
----------------------------------------------------------------------------------------
-- ENTITY FOR REV D
----------------------------------------------------------------------------------------
entity EP99840 is
     port( 
		-- System Signals -----------------------------------------------------------------------
		OMR       	: in      std_logic;							-- Master Reset
          CLK20_IN  	: in      std_logic_vector( 1 downto 0 );              -- Master Clock 10 Mhz

		-- DSP Signals --------------------------------------------------------------------------
		DSP_H3		: in		std_logic;							-- Clock from DSP 33ns period
          DSP_WR		: in		std_logic;							-- DSP Write Signal
          DSP_STB		: in		std_logic_vector(  3 downto 0 );			-- DSP Strobes
          DSP_A		: in		std_logic_vector( 15 downto 13 );			-- DSP Address
		DSP_A1		: in		std_logic_vector(  7 downto 0 );			-- DSP Address ( Low Bits )
          DSP_IRQ		: buffer	std_logic;							-- Interrupt to DSP
          DSP_D		: inout	std_logic_vector( 15 downto 0 );			-- DSP Data[15:00]

		-- PreAmp Releated Signals --------------------------------------------------------------
		OHV_Enable	: in		std_logic_vector( 1 downto 0 );
		IHV_Dis		: buffer	std_logic_Vector( 1 downto 0 );
		PA_MODE		: buffer	std_logic_Vector( 3 downto 0 );

		-- Analog Discriminator Signals ----------------------------------------------------------
		SHDisc		: in		std_logic;							-- 50ns Discriminator
		DISC_INV		: buffer	std_logic_Vector( 1 downto 0 );
		DISC_EVCH		: buffer	std_logic_Vector( 1 downto 0 );

		-- A/D Data Signals ---------------------------------------------------------------------
		CLK_FAD		: buffer	std_logic;
		OTR			: in		std_logic;							-- Out of Range input from A/D
		adata		: in		std_logic_Vector( 13 downto 0 );			-- Slow A/D Data

		-- Real Time Signals --------------------------------------------------------------------
		RT_OUT		: in		std_logic_Vector( 1 downto 0 );			-- AS seen by Scan Generator
		RT_IN		: buffer	std_logic_vector( 1 downto 0 );			-- AS seen by Scan Generator

		-- SCA Output 
		SCA			: buffer	std_logic_Vector( 7 downto 0 );
		-- DSP FiFo -----------------------------------------------------------------------------
		-- Writes at 10Mhz (CLK10_Out)
		-- Reads at DSP_H3
		FF_EF		: in		std_logic;							-- Fifo Empty Flag
		FF_FF		: in		std_logic;							-- Fifo Full Flag
		FF_OE		: buffer	std_logic;							-- FIFO Output Enable
		FF_RD		: buffer	std_logic;							-- Fifo Read
		FF_WR		: buffer	std_logic;							-- Fifo Write
		FF_RS		: buffer	std_logic;							-- Fifo Reset
		CLK20		: buffer	std_logic;
		FF_RCK		: buffer	std_logic;
		FF_WDATA		: buffer	std_logic_Vector( 17 downto 0 );			-- FIFO Write Data

		-- RTEM Support Signals ---------------------------------------------------------------
		RTEM_TILT		: in		std_logic;
		RTEM_RED		: in		std_logic;
		RTEM_GRN		: in		std_logic;
		RTEM_ANALYZE	: buffer	std_logic;
		RTEM_RETRACT	: buffer	std_logic;			

		-- DAC8420 Interface -----------------------------------------------------
		DA_CLK		: buffer	std_logic;
		DA_DOUT		: buffer	std_logic;
		DA_UPD		: buffer	std_logic;
		DA_OFS		: buffer	std_logic;
		-- DAC8420 Interface -----------------------------------------------------

		-- LTC1595 Interface ( Coarse Gain ) -------------------------------------
		CG_LD		: buffer	std_logic;
		CG_CLK		: buffer	std_logic;
		CG_Data		: buffer	std_logic;
		-- LTC1595 Interface ( Coarse Gain ) -------------------------------------

		-- ADS8321 BITDAC Interface ----------------------------------------------
		AD_Dout		: in		std_logic;	-- A/D Data Output
		AD_CS		: buffer	std_logic;	-- A/D Chip Select
		AD_CLK		: buffer	std_logic;	-- A/D Clock ( 2Mhz )
		-- ADS8321 BITDAC Interface ----------------------------------------------

		-- Misc Signals -------------------------------------------------------------------------
		ATP_SEL		: buffer	std_logic_Vector( 3 downto 0 );
	     SERIAL_NO		: inout	std_logic;					-- Electronic Serial Number
     	TMP_SENSE      : in      std_logic_vector(  2 downto 0 );	-- Temperature Sensor Input (TMP_SENSE(1) = MOVE_FLAG for WDS)
          TP			: buffer	std_logic_vector(  7 downto 0 ));	-- Output TP
     end EP99840;
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
architecture BEHAVIORAL of EP99840 is
	constant VER					: integer := 16#4030#;
	constant ZEROES				: std_logic_vector( 31 downto 0 ) := x"00000000";

	constant PA_DET0				: std_logic_vector( 1 downto 0 ) := "00";
	constant PA_DET1				: std_logic_vector( 1 downto 0 ) := "01";
	constant PA_BITA				: std_logic_vector( 1 downto 0 ) := "10";
	constant PA_BITD				: std_logic_vector( 1 downto 0 ) := "11";
	
	constant fir_gen				: integer := 1;
	constant TEMP_GEN				: integer := 1;
	constant RTEM_GEN				: integer := 1;
     ------------------------------------------------------------------------------------
	-- DSP Decode Related --------------------------------------------------------------
     ------------------------------------------------------------------------------------
	-- DSP Decode Related --------------------------------------------------------------
	constant iADR_ID    			: integer := 16#0000#;	-- ID Word
	constant iADR_STAT   			: integer := 16#0001#;	-- Status Word
	constant iADR_CW   				: integer := 16#0002#;	-- Control Word
--	constant iADR_SN0				: integer := 16#0003#;	-- Serial Number Bits 15:0
--	constant iADR_SN1				: integer := 16#0004#;	-- Serial Number Bits 31:16
--	constant iADR_SN2				: integer := 16#0005#;	-- Serial Number Bits 47:32
--	constant iADR_SN3				: integer := 16#0006#;	-- Serial Number Bits 63:48
	constant iADR_DSO_START			: integer := 16#0007#;	-- Start DSO
	constant iADR_TP_SEL			: integer := 16#0008#;	
	constant iADR_INT_EN			: integer := 16#0009#;	-- Interrupt Enable/Disable
	constant iADR_CGain				: integer := 16#000A#;	-- Coarse Gain (Analog)
	constant iADR_FGain				: integer := 16#000B#;	-- Fine Gain (Digital)
	constant iADR_OFFSET   			: integer := 16#000C#;	-- Offset ( Digital )
	constant iADR_CLIP_MIN			: integer := 16#000D#;
	constant iADR_CLIP_MAX			: integer := 16#000E#;
	constant iADR_DISC1				: integer := 16#000F#;
	
	constant iADR_DISC2				: integer := 16#0010#;
	constant iADR_EVCH				: integer := 16#0011#;
	constant iADR_BIT				: integer := 16#0012#;
	constant iADR_TIME_START			: integer := 16#0013#;	
	constant iADR_TIME_STOP			: integer := 16#0014#;	
	constant iADR_TIME_CLR			: integer := 16#0015#;	
	constant iADR_PRESET_MODE 		: integer := 16#0016#;	
	constant iADR_PRESET_LO			: integer := 16#0017#;	
	constant iADR_PRESET_HI  		: integer := 16#0018#;
	constant iADR_PRESET_ACK			: integer := 16#0019#;
	constant iADR_RTEM_INI			: integer := 16#001A#;
	constant iADR_RTEM_IN			: integer := 16#001B#;
	constant iADR_RTEM_OUT			: integer := 16#001C#;
	constant iADR_RTEM_HCMR			: integer := 16#001D#;
	constant iADR_RTEM_hc_RATE_lo		: integer := 16#001E#;
	constant iADR_RTEM_hc_RATE_hi		: integer := 16#001F#;
	
	constant iADR_RTEM_hc_THRESHOLD	: integer := 16#0020#;
	constant iADR_RAMA				: integer := 16#0021#;		-- 0x23, Look-Up Ram Address
	constant iADR_SCA				: integer := 16#0022#;		-- 0x08, Analog SCA/Digital ( Direct )
	constant iADR_SCA_LU			: integer := 16#0023#;		-- 0x09, Digital SCA Look-Up Data
	constant iADR_DSCA_PW			: integer := 16#0024#;		-- 0x0A, Digital SCA Pulse Width
	constant iADR_ROI_LU			: integer := 16#0025#;		-- 0x10, ROI Lookup Data 
--	constant iADR_PA_RESET_NTHR		: integer := 16#0026#;
--	constant iADR_PA_RESET_PTHR		: integer := 16#0027#;
	constant iADR_FIFO_LO			: integer := 16#0028#;		-- 0x11, FIFO Low Word
	constant iADR_FIFO_HI			: integer := 16#0029#;		-- 0x12, FIFO High Word
	constant iADR_FIFO_MR			: integer := 16#002A#;		-- 0x13, FIFO Master Reset
	constant iAdr_Disc_En			: integer := 16#002B#;			-- Discriminator Enables
	constant iADR_TC_SEL			: integer := 16#002C#;		-- Adaptive Mode/Time Constant Select
	constant iADR_DLEVEL0			: integer := 16#002D#;		-- High
	constant iADR_DLEVEL1			: integer := 16#002E#;		-- Medium
	constant iADR_DLEVEL2			: integer := 16#002F#;		-- Low
	
	constant iADR_DLEVEL3			: integer := 16#0030#;		-- Energy
	constant iADR_BLANK_Mode			: integer := 16#0031#;		-- Energy
	constant iADR_BLANK_DELAY		: integer := 16#0032#;		-- Energy
	constant iADR_BLANK_WIDTH		: integer := 16#0033#;		-- Energy
	constant iADR_PS_SEL			: integer := 16#0034#;
	constant iADR_BIT_PEAK1			: integer := 16#0035#;		-- Normalization Select
	constant iADR_BIT_PEAK2			: integer := 16#0036#;		-- Normalization Select
	constant iADR_BIT_CPS			: integer := 16#0037#;		-- Normalization Select
	constant iADR_BIT_PK2D			: integer := 16#0038#;		-- Normalization Select
	constant iADR_TLO  				: integer := 16#0039#;		-- 100ms Timer
	constant iADR_THI				: integer := 16#003A#;		-- 100ms Timer
	constant iADR_DU0_TLO			: integer := 16#003B#;		-- 100ms Timer
	constant iADR_DU0_THI			: integer := 16#003C#;		-- 100ms Timer
	constant iADR_DU1_TLO			: integer := 16#003D#;		-- 100ms Timer
	constant iADR_DU1_THI			: integer := 16#003E#;		-- 100ms Timer
	constant iADR_IN_CPSL			: integer := 16#003F#;		-- 100ms Timer
	
	constant iADR_IN_CPSH			: integer := 16#0040#;		-- 100ms Timer
	constant iADR_OUT_CPSL			: integer := 16#0041#;		-- 100ms Timer
	constant iADR_OUT_CPSH			: integer := 16#0042#;		-- 100ms Timer
	constant iADR_ROI_SUML			: integer := 16#0043#;		-- 100ms Timer
	constant iADR_ROI_SUMH			: integer := 16#0044#;		-- 100ms Timer
	constant iADR_LTIME_LO			: integer := 16#0045#;		-- 100ms Timer
	constant iADR_LTIME_HI			: integer := 16#0046#;		-- 100ms Timer
	constant iADR_CTIME_LO			: integer := 16#0047#;		-- 100ms Timer
	constant iADR_CTIME_HI			: integer := 16#0048#;		-- 100ms Timer
	constant iADR_RAMP_LO 			: integer := 16#0049#;		-- 100ms Timer
	constant iADR_RAMP_HI  			: integer := 16#004A#;		-- 100ms Timer
	constant iADR_Reset_Width		: integer := 16#004B#;		-- 100ms Timer
	constant iADR_NET_CPSL			: integer := 16#004C#;		-- 100ms Timer
	constant iADR_NET_CPSH			: integer := 16#004D#;		-- 100ms Timer
	constant iADR_NET_NPSL			: integer := 16#004E#;		-- 100ms Timer
	constant iADR_NET_NPSH			: integer := 16#004F#;		-- 100ms Timer
	
	constant iADR_rms_out			: integer := 16#0050#;
	constant iADR_RMS_THR			: integer := 16#0051#;
	constant iADR_PA_TIME_LO			: integer := 16#0052#;		-- 100ms Timer
	constant iADR_PA_TIME_HI			: integer := 16#0053#;		-- 100ms Timer
	constant iADR_AUTO_SHDISC		: integer := 16#0054#;
	constant iADR_RTEM_SEL			: integer := 16#0055#;		-- 100ms Timer
--	constant iADR_RTEM_Low			: integer := 16#0056#;	-- was 54
--	constant iADR_RTEM_Med			: integer := 16#0057#;	-- was 55
--	constant iADR_RTEM_High			: integer := 16#0058#;	-- was 56
	constant iADR_RTEM_CNT_L			: integer := 16#0059#;	-- was 57
	constant iADR_RTEM_CNT_H			: integer := 16#005A#;	-- was 58
	constant iADR_SH_Disc_Cnts		: integer := 16#005B#;		
	constant iADR_H_Disc_Cnts		: integer := 16#005C#;
	constant iADR_M_Disc_Cnts		: integer := 16#005D#;
	constant iADR_L_Disc_Cnts		: integer := 16#005E#;
	constant iADR_E_Disc_Cnts		: integer := 16#005F#;
	
	constant iADR_DECAY 			: integer := 16#0060#;
	constant iADR_BIT_ADC			: integer := 16#0061#;	
	constant iADR_FIR_MR			: integer := 16#0062#;
	constant iADR_DET_TYPE			: integer := 16#0063#;
	
	
	constant iADR_RTEM_CNT_100_L		: integer := 16#006B#;
	constant iADR_RTEM_CNT_100_H		: integer := 16#006C#;
	constant iADR_RTEM_CNT_MAX_L		: integer := 16#006F#;
	
	constant iADR_RTEM_CNT_MAX_H		: integer := 16#0070#;
	constant iADR_RTEM_CMD_MID_IN		: integer := 16#0071#;
	constant iADR_RTEM_CMD_MID_OUT	: integer := 16#0072#;
	constant iADR_RTEM_WD_MR			: integer := 16#0073#;
	constant iADR_FADC				: integer := 16#0074#;
	constant iADR_DSO_TRIG			: integer := 16#0075#;
	constant iADR_DSO_INT			: integer := 16#0076#;
	constant iADR_SHDisc_Dly_Sel		: integer := 16#0077#;
	constant iADR_RTEM_TEST_REG		: integer := 16#0078#;
	constant iADR_FIR_DLY_LEN		: integer := 16#0079#;
	constant iADR_FIR_DLY_INC		: integer := 16#007A#;
	constant iADR_Disc_Evch			: integer := 16#007B#;
	constant iADR_PRESET_DONE_CNT		: integer := 16#007C#;
	constant iADR_Reset_Mr			: integer := 16#007E#;
	constant iADR_DSO_FF_CNT			: integer := 16#007F#;
	-- DSP Decode Related ------------------------------------------------------------------------

	-- COntrol Word Bit Positions ----------------------------------------------------------------
	constant CW_BIT_ENABLE 			: integer := 0;
	constant CW_DSCA_POL			: integer := 1;
	constant CW_BLEVEL_EN			: integer := 2;
	constant CW_EDX_XFER			: integer := 3;
	constant CW_MEMORY_ACCESS		: integer := 4;
	constant CW_SCA_EN				: integer := 5;
	constant CW_DSO_EN				: integer := 6;
	constant CW_ADisc_En			: integer := 7;

	constant CW_LSMAP_TEST			: integer := 9;
	constant CW_VIDEO_THR_MODE		: integer := 10;
	constant CW_PA_SOURCE_LO			: integer := 11;
	constant CW_PA_SOURCE_HI			: integer := 12;
--	constant CW_GAIN_2X				: integer := 13;
--	constant CW_Det_Type_Lo 			: integer := 14;	No Longer used Ver 4x28
--	constant CW_Det_Type_Hi 			: integer := 15;
	-- COntrol Word Bit Positions ----------------------------------------------------------------

	-- CW_Det_Type_Hi downto CW_Det_Type_Lo definitions
	constant DET_204FR			: integer := 0;		-- 4x2A
	constant DET_UNICORN		: integer := 1;		-- Unicorn Detector
	constant DET_AP40			: integer := 2;
	constant DET_WDS			: integer := 3;		-- 3574
	constant DET_AP10			: integer := 4;
	constant DET_204NFR			: integer := 5;		-- 4x2A

	-- Type Declarations -------------------------------------------------------------------------

	-- Signal Declarations ------------------------------------------------------------------------
	signal adata_n			: std_logic_Vector( 13 downto 0 );
	signal AD_Data 		: std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
	signal AD_CLR			: std_Logic;
	signal Act_Reset		: std_logic;
	signal AD_BUSY			: std_logic;
	signal AD_INVERT		: std_logic;							-- Ver 4x28
	signal ad_data_in		: std_logic_Vector( 13 downto 0 );
	signal AD9240_PCT_50	: std_logic;
	
	signal BIT_ADATA		: std_logic_Vector( 15 downto 0 );
	signal BIT_DAC_EN		: std_Logic;
	signal BIT_Peak1		: std_logic_Vector( 11 downto 0 );
	signal BIT_Peak2		: std_logic_Vector( 11 downto 0 );
	signal BIT_CPS			: std_logic_Vector( 15 downto 0 );
	signal BIT_PK2D		: std_logic_Vector( 15 downto 0 );
	signal BIT_DAC_LD		: std_logic;
	signal BIT_DAC_DATA		: std_logic_Vector( 15 downto 0 );
	signal BIT			: std_logic_vector( 11 downto 0 );
	signal BLEVEL			: std_logic_Vector(  7 downto 0 );	-- Baseline Level
	signal BLR_PBUSY		: std_logic;
	signal blevel_upd		: std_logic;
	signal blank_Mode		: std_logic_vector(  1 downto 0 );
	signal blank_Delay		: std_logic_Vector( 15 downto 0 );
	signal blank_Width		: std_Logic_Vector( 15 downto 0 );
	
	signal CLK40			: std_logic;

	signal CPEAK			: std_logic_Vector( 11 downto 0 );	-- Corrected Peak Value	12/10/99
	signal CWORD			: std_logic_vector( 15 downto 0 );		-- Control Word
	signal CPS			: std_logic_vector( 20 downto 0 );	-- Input CPS
	signal CTIME			: std_logic_Vector( 31 downto 0 );
	signal CGain			: std_logic_Vector( 15 downto 0 );
	signal CLIP_MIN		: std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
	signal CLIP_MAX		: std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value

	signal ad_data_mux		: std_logic_Vector( 15 downto 0 );
	signal fad_data_Mux		: std_logic_Vector( 15 downto 0 );
	signal AD_Min			: std_logic_Vector( 13 downto 0 );
	signal AD_Max			: std_logic_Vector( 13 downto 0 );

	signal debug_ad		: std_logic_vector( 15 downto 0 );
	signal DSP_RAMA 		: std_logic_vector( 15 downto 0 );			-- DSP Ram Address
	signal DSCA_PW			: std_logic_Vector( 15 downto 0 );		-- Digital SCA Pulse Width
	signal dso_sel			: std_logic_Vector( 5 downto 0 );
	signal Disc_En			: std_logic_vector( 5 downto 0 );
	signal DSP_WD			: std_Logic_Vector( 15 downto 0 );
	signal DSP_READ		: std_Logic;
	signal DEbug			: std_logic_Vector( 15 downto 0 );
	signal DISC1			: std_logic_vector( 11 downto 0 );
	signal DISC2			: std_logic_vector( 11 downto 0 );
	signal Det_Sat			: std_logic;							-- Detctor 0 Saturated
	signal DSO_TRIG		: std_logic_Vector( 3 downto 0 );
	signal DiscCnts		: std_logic_Vector( 39 downto 0 );
	signal decay			: std_logic_vector( 15 downto 0 );
	signal DSO_INT			: std_Logic_Vector( 15 downto 0 );
	signal DISCEVCH		: std_logic_Vector(  1 downto 0 );
	signal Det_Type		: std_logic_Vector(  3 downto 0 );

	
	signal EVCH			: std_logic_vector( 11 downto 0 );
	signal Disc_EvCh_Ld		: std_logic;
	signal EvCh_LD			: std_logic;
	signal EvChDac			: std_logic_Vector( 11 downto 0 );
	signal Ld_EvCh			: std_logic;
	
	signal fdisc			: std_logic;
	signal FGAIN			: std_logic_Vector( 15 downto 0 );			-- 11/5/99
	signal fir_dly_len		: std_logic_Vector(  3 downto 0 );
	signal fir_offset_inc	: std_logic_vector( 11 downto 0 );
	signal FF_DONE			: std_logic;
	signal FF_DSO_CNT		: std_logic_Vector( 15 downto 0 );	

	signal rms_out			: std_logic_vector( 15 downto 0 );
	
	signal SHDisc_Dly_Sel 	: std_logic_Vector(  8 downto 0 );


	signal Hot_Swap_En		: std_logic;
	signal hc_rate			: std_logic_Vector( 20 downto 0 );
	signal hc_threshold		: std_logic_Vector( 15 downto 0 );
	signal Hi_Cnt			: std_logic;
	signal HV_Enable 		: std_logic_vector( 1 downto 0 );

	signal IMR			: std_logic;
	signal INT_EN			: std_logic_vector( 1 downto 0 );				-- Interrupt Enable
	signal iDSP_D 			: Std_logic_vector( 15 downto 0 );				-- Internal DSP Read Bus
	signal IPD_END			: std_logic;
	
	signal LTIME			: std_logic_vector( 31 downto 0 );
	signal LS_Abort		: std_logic;
	signal LS_Map_En		: std_logic;
	
	
	signal ms100_tc		: std_logic;								-- 100ms Timer
	signal MEAS_DONE		: std_logic;
	
	signal NPS			: std_logic_vector( 20 downto 0 );	-- Output CPA
	signal NET_CPS			: std_logic_Vector( 31 downto 0 );
	signal NET_NPS			: std_logic_vector( 31 downto 0 );

	signal OFFSET			: std_logiC_vector( 15 downto 0 );			-- 11/5/99
	
	signal PRESET			: std_logic_Vector( 31 downto 0 );
	signal PRESET_MODE		: std_logic_vector(  3 downto 0 );		-- 0 = None, 1 = Clock, 2 = Live
	signal Preset_Out		: std_logic;


	signal PS_SEL			: std_logic_Vector(  1 downto 0 );
	signal PA_Reset		: std_logic;
	signal PA_PSlope		: std_logic;
	signal PA_NSlope		: std_logic;


	signal Reset_Width		: std_logic_Vector( 15 downto 0 );	
	signal Reset_Time		: std_logic_vector( 31 downto 0 );
	signal RMS_Thr			: std_logic_Vector( 15 downto 0 );	-- RMS Threshold
	signal ROI_LU_DATA		: std_logic_Vector( 15 downto 0 );
	signal ROI_SUM			: std_logic_Vector( 31 downto 0 );
	signal rtem_ini  		: std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
	signal rtem_state		: std_logic_Vector(  3 downto 0 );
	signal RTEM_SEL 		: std_logic_vector(  7 downto 0 );
	signal RTEM_CNT_100		: std_logic_Vector( 31 downto 0 );
	signal RTEM_CNT_MAX		: std_logic_Vector( 31 downto 0 );
	signal rtem_hi_cnt 		: std_logic;
	signal rtem_Det_sat 	: std_logic;
	signal RTEM_RED_TMP 	: std_Logic;
	signal RTEM_GRN_TMP 	: std_Logic;
	signal RTEM_TEST_REG	: std_logic_vector( 1 downto 0 );

	signal RTEM_CNT		: std_logic_Vector( 31 downto 0 );
	
	signal SCA_LU_DATA		: std_logic_Vector(  7 downto 0 );
	signal SHDisc_MDly		: std_logic_vector(  8 downto 0 );	-- SHDisc Measued Delay

	signal TP_SEL			: std_logic_vector( 15 downto 0 );
	signal Time_Enable		: std_logic;
	signal TEST_REG		: std_logic_Vector( 31 downto 0 );
	signal TMP_LO			: std_logic_vector( 47 downto 0 );
	signal TMP_HI			: std_logic_vector( 47 downto 0 );
	signal TC_SEL 			: std_logic_vector( 12 downto 0 );	
	signal tlevel			: std_logic_vector( 63 downto 0 );

	signal WE_DCD			: std_logic_vector( 127 downto 0 );-- Number of DSP Decodes

	signal DSP_DATA_EN		: std_logic;
	signal iDSP_D_MUX		: std_logic_Vector( 15 downto 0 );
	signal STAT_OE			: std_Logic;
	signal Status_Vec 		: std_logic_Vector( 15 downto 0 );
	signal Status_OE		: std_logic;
	
	signal VIDEO_ABORT		: std_logic;
	
	signal Preset_Done_Cnt	: std_logic_vector( 15 downto 0 );

	signal TMP_SENSE_VEC	: std_logic_Vector( 1 downto 0 );

	signal DSO_TRIG_VEC		: std_logic_vector( 5 downto 0 );
	
	signal PA_Reset_Gen		: std_logic;

	signal rtd_test		: std_logic;	
     -----------------------------------------------------------------------------------
     -- Start of Component Declarations ------------------------------------------------
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- MEGAFUNCTION WIZARD Component Declarations -------------------------------------
     -----------------------------------------------------------------------------------
     -----------------------------------------------------------------------------------
	component altclklock
		generic(
			inclock_period			: NATURAL;
			clock0_boost			: NATURAL;
			clock1_boost			: NATURAL;
			operation_mode			: STRING;
			intended_device_family	: STRING;
			valid_lock_cycles		: NATURAL;
			invalid_lock_cycles		: NATURAL;
			valid_lock_multiplier	: NATURAL;
			invalid_lock_multiplier	: NATURAL;
			clock0_divide			: NATURAL;
			clock1_divide			: NATURAL;
			outclock_phase_shift	: NATURAL;
			lpm_type				: STRING );
		port(
			inclock				: IN STD_LOGIC;
			clock0				: buffer STD_LOGIC;
			clock1				: buffer STD_LOGIC );
	end component;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component AD9240 is
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK20		: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			PCT_50		: in		std_logic;
			FDisc		: in		std_logic;
			PA_Reset		: in		std_logic;
			OTR			: in		std_logic;					-- Out of Range
			ADout		: in		std_logic_vector( 13 downto 0 );	-- A/D Data Output
			fgain		: in		std_logic_Vector( 15 downto 0 );
			raw_ad		: buffer	std_Logic_Vector( 15 downto 0 );
			CLK_FAD		: buffer	std_logic;					-- Clock to A/D Converter
			Reset_Gen		: buffer	std_Logic;
			Dout			: buffer	std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
			Max			: buffer	std_logic_Vector( 13 downto 0 );
			Min			: buffer	std_logic_vector( 13 downto 0 ) );
	end component AD9240;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component ads8321 is 
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Master Clock					
			AD_Dout		: in		std_logic;					-- A/D Data Output from ADS8321
			AD_Start		: in		std_logic;					-- Start Command from DSP
			AD_CS		: buffer	std_logic;					-- Chip Select to ADS8321
			AD_CLK		: buffer	std_logic;	 				-- clock to ADS8321
			AD_BUSY		: buffer	std_Logic;					-- Busy Indication
			ADATA		: buffer	std_logic_Vector( 15 downto 0 ));	-- ADS8321 Parallel Data
	end component ads8321;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component BIT_DAC is 
		port( 
		     IMR            : in      std_logic;				 	-- Master Reset
	     	CLK20          : in      std_logic;                     	-- Master Clock
			BIT_EN		: in		std_Logic;					-- Bit DAC Generation Enable
			Time_Enable	: in		std_logic;
		     peak1     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 1 Value
	     	peak2     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 2 Value
		     cps       	: in      std_logic_vector( 15 downto 0 );	-- CPS Value
	     	pk2dly    	: in      std_logic_vector( 15 downto 0 );   -- Peak2 Delay Ver 523
			LD_BIT_DAC	: buffer	std_logic;					-- Load BIT DAC
			BIT_DAC_DATA	: buffer	std_logic_vector( 15 downto 0 ) );	-- BIT DAC Data
		end component BIT_DAC;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component DAC_INT
		port(
			IMR			: in		std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Mater Clock
			WE_ADR_DISC1	: in		std_logic;					-- Disc1 DAC Load Strobe from DSP
			WE_ADR_DISC2	: in		std_logic;					-- Disc2 DAC Load Strobe from DSP
			WE_ADR_EVCH	: in		std_logic;					-- ev/ch DAC Load Strobe from DSP
			WE_ADR_BIT	: in		std_logic;					-- BIT DAC Load Strobe from DSP
			BIT_DAC_LD	: in		std_logic;					-- BIT DAC Generation Load Stobe
			BIT_DAC_DATA	: in		stD_logic_Vector( 11 downto 0 );	-- BIT DAC Generation Data
			DISC1		: in		std_logic_Vector( 11 downto 0 );	-- Disc1 data
			DISC2		: in		std_logic_Vector( 11 downto 0 );	-- Disc2 Data
			EVCH 		: in		std_logic_Vector( 11 downto 0 );	-- ev/ch data
			BIT   		: in		std_logic_Vector( 11 downto 0 );	-- bit dac data
			DA_OFS		: buffer	std_logic;					-- DAC Frame Sync
			DA_CLK		: buffer	std_logic;					-- DAC Clock
			DA_DOUT		: buffer	std_logic;					-- DAC Data
			DA_UPD		: buffer	std_logic );					-- DAC Load
		end component DAC_INT;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component DSPINT 
	     port( 	
			IMR     		: in      std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- master Clock
			clk40 		: in		std_logic;					-- Master 40Mhz clock
			DSP_WR		: in		std_logic;					-- DSP Write Strobe
			DSP_STB		: in		std_logic_vector(   3 downto  0 );	-- DSP Data Select Strobes
			DSP_A		: in		std_logic_Vector(  15 downto 13 );	-- DSP Address bits 15-13
			DSP_A1		: in		std_logic_Vector(   7 downto  0 ); -- DSP Address bits 7-0
			DSP_D		: in		std_logic_vector(  15 downto  0 );	-- DSP Data Bus
			FF_FF		: in		std_logic;
			FF_DONE		: in		std_logic;
			DSO_ENABLE	: in		std_logic;
			int_en		: in		std_logic;
			ms100_tc		: in		std_logic;
			STAT_OE		: buffer	std_logic;					-- FIFO Status Output Enable
			FF_OE		: buffer	std_logic;					-- FIRO Output Enable
			FF_REN		: buffer	std_logic;					-- FIFO Read Enable
			FF_RCK		: buffer	std_logic;					-- FIFO Read Clock
			DSP_READ		: buffer	std_logic;					-- DSP Read Signal, Enable tri-state buffers
			DSP_WD		: buffer	std_Logic_Vector(  15 downto 0 );	-- Registerd DSP Write Data Bus
			DSP_IRQ		: buffer	std_logic;		-- HV_DIS(1)
			WE_DCD		: buffer 	std_logic_Vector( 127 downto 0 )); -- DSP Write Decode
     	end component DSPINT;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component FIFO 
		port( 
			IMR				: in		std_logic;					-- Master Reset
			CLK20			: in		std_logic;					-- Master Clock
			FF_FF			: in		std_logic;					-- FIFO Full Flag
			LS_MAP_EN			: in		std_Logic;
			WE_FIFO_HI		: in		std_logic;					-- FIFO Write Hi
			BIT_ENABLE		: in		std_logic;					-- BIT Mode Enable
			DSO_Enable		: in		std_logic;					-- DSO Mode Enable
			Time_Enable		: in		std_logic;					-- Acquisition Enabled
			Blevel_En			: in		std_logic;					-- Baseline Histogram Mode
			Meas_Done			: in		std_logic;					-- Measurement Done - Normal pulse processing
			Preset_out		: in		std_logic;					-- Preset Done	- Normal pulse processing
			Blevel_Upd		: in		std_logic;					-- Baseline Level Update - during Normal Pulse Processing
			WE_DSO_START		: in		std_logic;					-- DSO Start
			PBusy			: in		std_logic;					-- Pulse Busy	- DSO
			WE_PRESET_DONE_CNT	: in		std_logic;
			DSO_TRIG_VEC 		: in		std_logic_Vector( 5 downto 0 );
			DSO_Trig			: in		std_logic_vector(  3 downto 0 );	-- DSO Trigger Select
			DSO_INT			: in		std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
			TEST_REG			: in		std_logic_Vector( 17 downto 0 );
			CPEAK			: in		std_logic_vector( 11 downto 0 );	-- Current Peak Value
			BLEVEL			: in		std_logic_vector(  7 downto 0 );	-- Baseline Level
			Debug			: in		std_logic_vector( 15 downto 0 );	-- DSO Data Port
			Preset_DOne_Cnt	: buffer	std_logic_Vector( 15 downto 0 );
			FF_DONE			: buffer	std_logic;					-- FIFO Write Done
			FF_CNT			: buffer	std_logic_Vector( 15 downto 0 );
			FF_WR			: buffer	std_logic;					-- FIFO Write Signal
			FF_WDATA			: buffer	std_logic_vector( 17 downto 0 ) );	-- FIFO Data
	end component FIFO;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component fir_block 
		port(
			imr				: in		std_logic;					-- Master Reset
			clk20			: in		std_Logic;					-- Master 20Mhz Clock
			clk40			: in		std_logic;					-- Master 40Mhz clock
			ADisc_en			: in		std_logic;					-- Adaptive Disc Enable
			OTR				: in		std_logic;
			AD_CLR			: in		std_logic;
			ad_data_in		: in		std_logic_vector( 13 downto 0 );
			fad_data			: in		std_logic_Vector( 15 downto 0 );
			ad_data			: in		std_logic_vector( 15 downto 0 );
			blank_mode		: in		std_logic_vector(  1 downto 0 );	-- Beam Blanking Mode
			blank_Delay		: in		std_logic_Vector( 15 downto 0 );	-- Beam Blanking Delay
			blank_Width		: in		std_Logic_Vector( 15 downto 0 );	-- Beam Blanking Width
			CLIP_MIN			: in		std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
			CLIP_MAX			: in		std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
			debug_sel			: in		std_logic_Vector(  5 downto 0 );	-- Debug Select Code
			decay			: in		std_logic_vector( 15 downto 0 );	-- ADisc Decay
			DSCA_POL			: in		std_logic;					-- Digital SCA Polarity
			DSCA_PW			: in		std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse Width
			Disc_en			: in		std_logic_Vector(  5 downto 0 );	-- Discriminator enabled
			DSP_RAMA			: in		std_logic_Vector( 15 downto 0 );	-- DSP Address Bus
			DSP_WD			: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
			EDX_XFER			: in		std_Logic;					-- XFer flag for all/ROI data
			fgain			: in      std_logic_Vector( 15 downto 0 );	-- Fine Gain
			fir_dly_len		: in		std_logic_Vector(  3 downto 0 );	-- Flat-Top Selection
			fir_offset_inc		: in		std_logic_Vector( 11 downto 0 );	-- Additional Settling Time for PUR
			hc_rate			: in		std_logic_Vector( 20 downto 0 );	-- High Count Rate Threshold
			hc_threshold		: in		std_logic_Vector( 15 downto 0 );	-- High Count Rate Time Threshold
			IPD_END			: in		std_logic;
			memory_access		: in		std_logic;					-- Memory Access Flag
			offset			: in      std_logic_Vector( 15 downto 0 );	-- Offset (Zero)
			PA_Reset			: in		std_Logic;
			Preset_Mode		: in		std_logic_vector(  3 downto 0 );	-- Acquisition Preset Mode
			Preset			: in		std_logic_Vector( 31 downto 0 );	-- Acquisition Preset Value
			PS_SEL			: in		std_logic_Vector(  1 downto 0 );	-- Peak Shift Select (BLR)
			RMS_Thr			: in		std_logic_Vector( 15 downto 0 );	-- RMS Threshold
			rtd_test			: in		std_logic;
			SCA_EN			: in		std_logic;					-- SCA Enable bit
			SHDisc_Dly_Sel 	: in		std_logic_Vector(  8 downto 0 );	-- Super High Speed Discriminator Delay
			SHDisc_In			: in		std_logic;					-- Super High Speed Discriminator Input
			TC_SEL			: in		std_logic_vector(  3 downto 0 );	-- Time Const Select
			Time_Clr			: in		std_logic;					-- Time Clear
			Time_Start		: in		std_logic;					-- Time Start
			Time_Stop			: in		std_logic;					-- Time Stop
			tlevel 			: in		std_logic_vector( 63 downto 0 );	-- Threshold Level
			WE_FIR_MR			: in		std_logic;					-- Filter Master Reset
			WE_DSP_SCA		: in		std_logic;					-- Write to SCA Directly (test)
			WE_SCA_LU			: in		std_logic;					-- Write to SCA Look-up memory
			WE_ROI			: in		std_logic;					-- Write to ROI look-up Memory
			Video_Abort		: in		std_logic;
			BLEVEL			: buffer	std_logic_vector(  7 downto 0 );	-- Baseline Level 
			BLR_PBusy			: buffer	std_logic;
			blevel_upd		: buffer	std_logic;					-- Baseline level Update (FIFO)
			cpeak   			: buffer  std_Logic_Vector( 11 downto 0 );	-- Converted Peak
			CPS 				: buffer	std_logic_Vector( 20 downto 0 );	-- Input Count Rate
			CTIME			: buffer	std_logic_Vector( 31 downto 0 );	-- CLock Time
			DiscCnts			: buffer	std_logic_Vector( 39 downto 0 );	-- Discriminator Counts
			DSO_TRIG_VEC		: buffer	std_logic_vector(  5 downto 0 );	-- DSO Trigger Vector
			FDisc			: buffer	std_logic;
			LTIME			: buffer	std_logic_Vector( 31 downto 0 );	-- Live Time
			NPS				: buffer	std_logic_Vector( 20 downto 0 );	-- Stored Count Rate
			Meas_Done			: buffer	std_logic;					-- Converted Peak Done (FIFO)
			ms100_tc			: buffer	std_logic;					-- 100ms Timer (RTEM)
			LS_Abort			: buffer	std_logic;
			LS_Map_En			: buffer	std_Logic;
			net_cps			: buffer	std_logic_Vector( 31 downto 0 );	-- Net Input Counts
			net_nps			: buffer	std_logic_Vector( 31 downto 0 );	-- Net Stored Counts
			Preset_Out		: buffer	std_logic;					-- Preset Done (FIFO)
			ROI_LU			: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
			ROI_SUM			: buffer	std_logic_vector( 31 downto 0 );
			SCA				: buffer	std_logic_vector(  7 downto 0 );	-- SCA Output
			sca_lu_data		: buffer	std_logic_vector(  7 downto 0 );	-- SCA LookUp Data
			SHDisc_MDly		: buffer	std_logic_vector(  8 downto 0 );	-- SHDisc Measued Delay
			Time_Enable		: buffer	std_logic;					-- Acquisition enable (FIFO)
			debug			: buffer	std_logic_Vector( 15 downto 0 );	-- Debug Bus
			rms_out			: buffer	std_logic_vector( 15 downto 0 ));		
	end component fir_block;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component hotswap is
		port(
			imr 		: in		std_logic;
			clk20	: in		std_logic;
			ms100_tc	: in		std_logic;
			Sense	: in		std_logic;		-- TMP_SENSE(1)
			DC_ON	: buffer	std_logic );		-- HV_DIS(1)
	end component hotswap;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component LTC1595 
		port( 
		     IMR            : in      std_logic;				     -- Master Reset
			CLK20		: in		std_logic;
			Data_Ld		: in		std_logic;			-- Strobe to Load the Data
			Data			: in		std_logic_vector( 15 downto 0 );
			DAC_LD		: buffer	std_logic;			-- Signal indicating data Load is Done
			DAC_CLK		: buffer	std_logic;			-- DAC clock
			DAC_DATA		: buffer	std_logic );			-- DAC Data
	end component LTC1595;
     -----------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component PA_Reset_Det is 
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK			: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			Fir_Mr		: in		std_logic;
			We_Clr_AD		: in		std_logic;
			Dout			: in		std_logic_vector( 15 downto 0 );	-- A/D Data Output
			ms100tc		: in		std_logic;
			CPS			: in		std_logic_Vector( 20 downto 0 );
			hc_rate		: in		std_logic_Vector( 20 downto 0 );
			hc_threshold	: in		std_logic_Vector( 15 downto 0 );
			det_sat		: buffer	std_logic;
			Hi_Cnt		: buffer	std_logic;
			AD_CLR		: buffer	std_logic;
			Reset		: buffer	std_logic;					-- PreAmp Reset
			NSlope_Cmp_out	: buffer	std_logic;
			pslope_cmp_out	: buffer 	std_logic;
			Reset_time	: buffer	std_logic_vector( 31 downto 0 );
			Reset_Width	: buffer	std_logic_Vector( 15 downto 0 ) );
	end component PA_Reset_Det;
	-------------------------------------------------------------------------------


     -----------------------------------------------------------------------------------
	component rtemctrl 
		port( 
			imr       	: in      std_logic;				     	-- Master Reset
			clk20       	: in      std_logic;                         	-- Master Clock
			RTEM_SEL		: in		std_logic_vector( 1 downto 0 );
			ms100_tc		: in		std_logic;
			WD_EN		: in		std_logic;
			WD_MR		: in		std_logic;
			rtem_ini  	: in		std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
			CNT_100		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			CNT_MAX		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
			Det_Sat		: in		std_logic;
			CMD_IN		: in		std_Logic;						-- COmmand to Move In
			CMD_OUT		: in		std_logic;						-- Command to Move Out
			CMD_MID_IN	: in		std_logic;
			CMD_MID_OUT	: in		std_logic;	
			CMD_HC_MR		: in		std_logic;						-- High Count Master Reset
			RTEM_RED		: in		std_logic;						-- Input Status for In/Moving in
			RTEM_GRN		: in		std_logic;						-- Input Status for Out/Moving Out
			RTEM_HS		: in		std_logic;
			RTEM_ANALYZE	: buffer	std_logic;						-- Output to Move RTEM In
			RTEM_RETRACT	: buffer	std_logic;						-- Output to Move RTEM Out
			RTEM_CNT		: buffer	std_logic_Vector( 31 downto 0 );
			ST_RTEM   	: buffer	std_logic_vector(  3 downto 0 ) );		-- RTEM Status
	end component rtemctrl;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	Component TMP04 
		port( 
			IMR            : in      std_logic;				     -- Master Reset
		     CLK20          : in      std_logic;                         -- Master Clock
     		TMP_SENSE      : in      std_logic;                         -- Pipelined   
		     TMP_LO         : buffer  std_logic_vector( 15 downto 0 );	-- Temperature Low
		     TMP_HI		: buffer	std_logic_vector( 15 downto 0 ));	-- Temperature Hi
	end component TMP04;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
     -- End of Component Declarations --------------------------------------------------
     -----------------------------------------------------------------------------------
begin
     -----------------------------------------------------------------------------------
	-- Altera PLL megafunctions used to take the reference input clock
	-- and generate two internal clocks based on the input reference clock
	CLK_GEN0 : altclklock
		generic map(
			inclock_period 		=> 50000,
			clock0_boost 			=> 1,
			clock1_boost 			=> 2,
			operation_mode 		=> "NORMAL",
			intended_device_family	=> "APEX20KE",
			valid_lock_cycles 		=> 1,
			invalid_lock_cycles 	=> 5,
			valid_lock_multiplier 	=> 1,
			invalid_lock_multiplier 	=> 5,
			clock0_divide 			=> 1,
			clock1_divide 			=> 1,
			outclock_phase_shift 	=> 0,
			lpm_type 				=> "altclklock" )
		port map(
			inclock 				=> clk20_IN(1),	-- clk1p 		(pin 31 )
			clock0 				=> clk20,		-- clk1out 	(pin 23)
			clock1 				=> CLK40 );		-- Internal Node

     -----------------------------------------------------------------------------------
	-- Start of EDAX Defined Component INSTANTIATIONS ---------------------------------
     -----------------------------------------------------------------------------------
	     -----------------------------------------------------------------------------------
		-- Fast A/D Converter Interface
		-- Documented 7/24/03 - MCS
		-- Input to A/D is inverted, so invert it here to correct
	
		-- Ver 4x28
		ad_data_in <= not( adata ) when ( AD_Invert = '0' ) else adata;	-- Normally invert, othersise dont

		U_AD9240 : AD9240
			port map(
				IMR					=> IMR,					-- Master Reset
				clk20				=> clk20,					-- Master Clock
				PCT_50				=> AD9240_PCT_50,
				FDisc				=> FDisc,
				PA_Reset				=> PA_Reset,
				OTR					=> OTR,					-- A/D Out-of-Range
				Adout				=> ad_data_in,				-- not( adata ),				
				fgain				=> fgain,
				CLK_FAD				=> CLK_FAD,				-- Convert Clock to Fast A/D (10Mhz)
				Reset_Gen				=> PA_RESET_GEN,
				Raw_ad				=> debug_AD, 
				Dout					=> AD_DATA,				-- Converted Data	(20Mhz Rate)
				Max					=> AD_Max,
				Min					=> AD_Min );
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- BIT DAC Data Generator
		-- Documented 7/24/03 - MCS
		U_BITDAC : BIT_DAC
			port map(
				IMR					=> IMR,					-- Master Reset
				CLK20				=> clk20,					-- 20Mhz Clock
				BIT_EN				=> BIT_DAC_EN,				-- Bit DAC Generation Enable
				Time_Enable			=> Time_Enable,
			     peak1     			=> BIT_Peak1,				-- Peak 1 Size
		  		peak2     			=> BIT_Peak2,				-- Peak 2 Size
			     cps       			=> BIT_CPS,				-- CPS Time
	     		pk2dly    			=> BIT_Pk2D,				-- Peak2 Delay Time
				LD_BIT_DAC			=> BIT_DAC_LD,				-- Load Strobe
				BIT_DAC_DATA			=> BIT_DAC_DATA );			-- Generated Data
				
		ad_data_mux	<= bit_Dac_data	when ( CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) 	= PA_BITD ) else 
					   x"8000"		when ( AD_CLR 		= '1' 	) else
					  ad_data;

		fad_data_Mux	<= bit_Dac_data	when ( CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) 	= PA_BITD ) else 
					   x"8000"		when ( AD_CLR 		= '1' 	) else
					   ad_data;



	     -----------------------------------------------------------------------------------
		
	     -----------------------------------------------------------------------------------
		-- BIT A/D Interface
		-- Documented 7/24/03 - MCS
		U_ADS8321 : ads8321
			port map(
				IMR					=> IMR,					-- Master Reset
				clk20				=> clk20,					-- Master Clock
				AD_Dout				=> AD_Dout,				-- A/D Input Data from ADS8321
				AD_Start				=> WE_DCD( iADR_BIT_ADC ),  	-- Start Command from DSP
				AD_CS				=> AD_CS,					-- Chip Select to ADS8321
				AD_CLK				=> AD_CLK,				-- Clock to ADS8321
				AD_BUSY				=> AD_BUSY,				-- Busy Indication
				ADATA				=> BIT_ADATA );			-- ADS8321 Parallel Data
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- DAC8420 Interface
		-- Documented 7/28/03 - MCS
		U_DAC_INT : DAC_INT
			port map(
				IMR					=> IMR,					-- Master Reset
				CLK20				=> clk20,					-- Master Clock
				WE_ADR_DISC1			=> WE_DCD( iADR_DISC1 ),		-- Disc1 DAC Load Strobe from DSP
				WE_ADR_DISC2			=> WE_DCD( iADR_DISC2 ),		-- Disc2 DaC Load Strobe	from DSP
				WE_ADR_EVCH			=> Ld_EvCh,				-- eV/Ch DAC Load Strobe	from DSP
				WE_ADR_BIT			=> WE_DCD( iADR_BIT ),		-- Bit DAC Load Strobe from DSP
				BIT_DAC_LD			=> BIT_DAC_LD,				-- BIT DAC Generation Load Strobe
				BIT_DAC_DATA			=> BIT_DAC_DATA( 15 downto 4 ),-- BIT DAC Data
				DISC1				=> DISC1,					-- Disc 1 DAC Value
				DISC2				=> DISC2,					-- Disc 2 DAC Value
				EVCH 				=> EvChDac,				-- Ev/Ch DaC Value
				BIT   				=> BIT,					-- BIT DAC Value
				DA_OFS				=> DA_OFS,				-- DAC Busy output
				DA_CLK				=> DA_CLK,				-- DAC Clock
				DA_DOUT				=> DA_DOUT,				-- DAC Data
				DA_UPD				=> DA_UPD );				-- DAC Load Strobe
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- DSP Interface (TI TMS320C32 )
		-- Documented 7/28/03 - MCS
		U_DSPINT : DSPINT
			port map(
				IMR       			=> IMR,			-- Master Reset
				CLK20				=> clk20,			-- Master clock
				clk40				=> clk40,			-- Master 40Mhz clock
				DSP_WR				=> DSP_WR,		-- DSP Write Strobe
				DSP_STB				=> DSP_STB,		-- DSP Dataselect Strobes
				DSP_A				=> DSP_A,			-- DSP Address Bus -bits 15:13
				DSP_A1				=> DSP_A1,		-- DSP Address bus -bits 7:0
				DSP_D				=> DSP_D,			-- DSP DAta Bus
				ff_ff				=> ff_ff,
				ff_done				=> ff_done,
				DSO_ENABLE			=> CWORD( CW_DSO_EN ),
				int_en				=> Int_En(0),
				ms100_tc				=> ms100_tc,
				STAT_OE				=> STAT_OE,		-- FIFO Status Output Enable
				FF_OE				=> FF_OE,			-- FIFO Output Enable
				FF_REN				=> FF_RD,			-- FIFO Read Enable
				FF_RCK				=> FF_RCK,		-- FIFO Read Clock
				DSP_READ				=> DSP_READ,		-- DSP Read signal, enable tri-state buffers
				DSP_WD				=> DSP_WD,		-- Registered DSP Write DAta Bus
				DSP_IRQ				=> DSP_IRQ,
				WE_DCD				=> WE_DCD );		-- DSP Decoded Vector
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- FIFO Interface
		-- Documented 7/28/03 - MCS
		U_FIFO : FIFO
			port map(
				IMR					=> IMR,						-- Master Reset
				CLK20				=> clk20,						-- Master Clock
				LS_MAP_EN				=> LS_MAP_EN,
				FF_FF				=> FF_FF,						-- FIFO FUll Flag
				WE_FIFO_HI			=> WE_DCD( iADR_FIFO_HI ),		-- DSO Write to FIFO
				BIT_ENABLE			=> CWORD( CW_BIT_ENABLE ),		-- BIT Mode Enable
				DSO_Enable			=> CWORD( CW_DSO_EN ),					-- DSO Mode Enable
				Time_Enable			=> Time_Enable,				-- TIme Enable
				Blevel_en				=> CWORD( CW_BLEVEL_EN ),		-- Baseline Histogram Mode
				Meas_Done				=> MEas_Done,					-- Measurement Done Ver 4x17
				Preset_out			=> Preset_out,					-- Preset TIme Done
				Blevel_Upd			=> blevel_upd,					-- Baseine Level Update
				WE_DSO_START			=> WE_DCD( iADR_DSO_START ),		-- Start DSO Mode
				PBusy				=> BLR_PBusy,					-- Used for DSO
				WE_Preset_Done_Cnt		=> WE_DCD( iADR_Preset_Done_Cnt ),	
				DSO_TRIG_VEC			=> DSO_TRIG_VEC,				-- DSO Trigger Vector
				DSO_Trig				=> DSO_Trig,					-- DSO Trigger Code
				DSO_INT				=> DSO_INT,					-- DSO Sample Interval
				TEST_REG				=> TEST_REG( 17 downto 0 ),		-- Test Register used for DSP-FIFO Write
				CPEAK				=> CPeak,					-- Converted Peak Value	ver 4x17
				BLEVEL				=> Blevel,					-- Baseline Level
				Debug				=> Debug,						-- DSO data mux
				Preset_DOne_Cnt		=> Preset_Done_Cnt,
				FF_DONE				=> FF_DONE,					-- FIFO Sequence Done
				FF_CNT				=> FF_DSO_CNT,
				FF_WR				=> FF_WR,						-- FIFO Write Signal
				FF_WDATA				=> FF_WData );					-- FIFO Data Signal
	     -----------------------------------------------------------------------------------


		IPD_END		<= not( RT_OUT(1) );
		VIDEO_ABORT	<= not( RT_OUT(0) ) when ( Cword( CW_VIDEO_THR_MODE ) = '1' ) else '0';
	
	     -----------------------------------------------------------------------------------
		Gen_Fir : if( Fir_Gen = 1 ) generate
			UDPP : Fir_block 
				port map(
					imr				=> imr,          		  	-- Master Reset
					clk20			=> clk20,					-- Master 20Mhz Clock
					clk40			=> clk40,        			-- Master 40Mhz clock
					ADisc_en			=> CWord( CW_ADisc_En ),		-- Adaptive Disc Enable
					ad_data_in		=> ad_data_in,
					fad_Data			=> fad_Data_mux,
					ad_data			=> ad_data_mux,
					otr				=> otr,
					AD_CLR			=> AD_CLR,
					blank_mode		=> Blank_Mode,				-- Beam Blanking Mode
					blank_Delay		=> Blank_Delay,			-- Beam Blanking Delay
					blank_Width		=> blank_Width,			-- Beam Blanking Width
					CLIP_MIN			=> CLIP_MIN,				-- Minimum CLIP Value
					CLIP_MAX			=> CLIP_MAX,				-- Maximum CLIP Value
					debug_sel			=> dso_sel,				-- Debug Select Code
					decay			=> decay,					-- ADisc Decay
					DSCA_POL			=> CWORD( CW_DSCA_POL ),				-- Digital SCA Polarity
					DSCA_PW			=> DSCA_PW,				-- Digital SCA Pulse Width
					Disc_en			=> Disc_En,				-- Discriminator enabled
					DSP_RAMA			=> DSP_RAMA,				-- DSP Address Bus
					DSP_WD			=> DSP_WD,				-- DSP Data Bus
					EDX_XFER			=> CWORD( CW_EDX_XFER ),		-- XFer flag for all/ROI data
					fgain			=> fgain,					-- Fine Gain
					fir_dly_len		=> fir_dly_len,			-- Flat-Top Selection
					fir_offset_inc		=> fir_offset_inc,			-- Additional Settling Time for PUR
					hc_rate			=> hc_rate,				-- High Count Rate Threshold
					hc_threshold		=> hc_threshold,			-- High Count Rate Time Threshold
					IPD_END			=> IPD_End,				-- Interpixel Delay End
					memory_access		=> Cword( CW_MEMORY_ACCESS ),	-- Memory Access Flag
					offset			=> offset,				-- Offset (Zero)
					PA_Reset			=> PA_Reset,				-- PreAmp Reset
					Preset_Mode		=> Preset_Mode,			-- Acquisition Preset Mode
					Preset			=> Preset,				-- Acquisition Preset Value
					PS_SEL			=> PS_SEL,				-- Peak Shift Select (BLR)
					RMS_Thr			=> RMS_Thr,				-- RMS Threshold
					rtd_test			=> rtd_test,
					SCA_EN			=> CWORD( CW_SCA_EN ),		-- SCA Enable bit
					SHDisc_Dly_Sel 	=> SHDisc_Dly_Sel,			-- Super High Speed Discriminator Delay
					SHDisc_In			=> SHDisc,				-- Super High Speed Discriminator Input
					TC_SEL			=> TC_SEL( 3 downto 0 ),		-- Time Const Select
					Time_Clr			=> WE_DCD( iADR_TIME_CLR ),	-- Time Clear
					Time_Start		=> WE_DCD( iADR_TIME_START ),	-- Time Start
					Time_Stop			=> WE_DCD( iADR_TIME_STOP ),	-- Time Stop
					tlevel 			=> tlevel,				-- Threshold Level
					WE_FIR_MR			=> WE_DCD( iADR_FIR_MR ),	-- Filter Master Reset
					WE_DSP_SCA		=> WE_DCD( iADR_SCA ),		-- Write to SCA Directly (test)
					WE_SCA_LU			=> WE_DCD( iADR_SCA_LU ),	-- Write to SCA Look-up memory
					WE_ROI			=> WE_DCD( iADR_ROI_LU ),	-- Write to ROI look-up Memory
					Video_Abort		=> Video_Abort,			-- Video Threshold Abort
					BLEVEL			=> Blevel,				-- Baseline Level 
					BLR_PBusy			=> BLR_PBusy,
					blevel_upd		=> blevel_upd,				-- Baseline level Update (FIFO)
					cpeak   			=> CPEAK,					-- Converted Peak
					CPS 				=> CPS,					-- Input Count Rate
					CTIME			=> CTIME,					-- CLock Time
					DiscCnts			=> DiscCnts,				-- Discriminator Counts
					DSO_TRIG_VEC		=> DSO_TRIG_VEC,			-- DSO Trigger Vector
					FDisc			=> FDisc,
					LTIME			=> LTIME,					-- Live Time
					Meas_Done			=> Meas_Done,				-- Converted Peak Done (FIFO)
					ms100_tc			=> ms100_tc,				-- 100ms Timer (RTEM)
					LS_Abort			=> LS_Abort,
					LS_Map_En			=> LS_Map_En,
					NPS				=> NPS, 					-- Stored Count Rate
					net_cps			=> net_cps,				-- Net Input Counts
					net_nps			=> net_nps,				-- Net Stored Counts
					Preset_Out		=> Preset_Out,				-- Preset Done (FIFO)
					ROI_LU			=> ROI_LU_DATA,			-- ROI Lookup Data
					ROI_SUM			=> ROI_SUM,
					SCA				=> SCA,					-- SCA Output
					sca_lu_data		=> sca_lu_data,			-- SCA LookUp Data
					SHDisc_MDly		=> SHDisc_MDly,			-- Super High Speed Measured Delay
					Time_Enable		=> Time_Enable,			-- Acquisition enable (FIFO)
					rms_out			=> rms_out,
					debug			=> DEBUG );				-- WDS Status Move Flag
			end generate;
		-- FIR BLock		
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		U_HOTSWAP : hotswap 
			port map(
				imr				=> imr,
				clk20			=> clk20,
				ms100_tc			=> ms100_tc,
				sense			=> TMP_SENSE(1),		-- Sense Input
				DC_ON			=> Hot_Swap_En );		-- Relay Enable

		IHV_DIS(1)	<= PA_RESET_GEN when ( Act_Reset = '1' ) else HOT_SWAP_EN;
					
		IHV_DIS(0)	<= '0';	-- Always Enabled ( or Never Disabled )
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- Interface to the LTC1595 SDAC used for the Coarse Gain
		--	Documented 7/28/03 - MCS
		U_LTC1595	: LTC1595
			port map(
			     IMR           		=> IMR,
				CLK20			=> clk20,
				Data_Ld			=> WE_DCD( iADR_CGain ),
				Data				=> CGain,
				DAC_LD			=> CG_LD,
				DAC_CLK			=> CG_CLK,
				DAC_DATA			=> CG_Data );
	     -----------------------------------------------------------------------------------

		------------------------------------------------------------------------------
		-- Next Monitir A/D Data stream for reset
		-- Monitor A/D Data to determine when the Reset Occurs, also maintain Min and Max Ramp Values
		U_PA_Reset_Det : PA_Reset_Det
			port map(
				IMR				=> IMR,					-- Master Reset
				CLK				=> clk20,					-- Master Clock
				Fir_Mr			=> WE_DCD( iADR_Reset_Mr ),	-- Filter Reset
				We_Clr_AD			=> WE_DCD( iADR_FIR_MR ),	-- Reset_Mr,
				Dout				=> fad_Data_mux, 			-- adata_mux,				-- A/D Input Data
				ms100tc			=> ms100_tc,				-- 100ms Timer
				cps				=> cps,
				hc_rate			=> hc_rate,				-- High Count Rate
				hc_threshold		=> hc_threshold,			-- High Count Threshold
				Det_Sat			=> Det_Sat,				-- Detector Saturated
				Hi_Cnt			=> Hi_Cnt,				-- High Count Rate Detected
				AD_CLR			=> ad_clr,				-- A/D Input Clamp
				Reset			=> PA_Reset,				-- Reset Detection
				NSlope_Cmp_out		=> PA_NSlope,
				pslope_cmp_out		=> PA_PSlope,
				Reset_Time		=> Reset_Time,				-- Measured Reset Time
				Reset_Width		=> Reset_Width );     		-- Reset Duration
		------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- RTEM/UMS-II Control
		--	Documented 7/28/03 - MCS
		GEN_RTEM : if( RTEM_GEN = 1 ) generate
			-- RTEM_SEL(3) = AutoRetract Disable
			rtem_hi_cnt 	<= '0' when ( RTEM_SEL(3) = '1' ) else Hi_Cnt;
			
			rtem_Det_sat 	<= '0' when ( RTEM_SEL(3) = '1' ) else 
	   					   Det_Sat;

			U_RTEM : rtemctrl 
				port map(
					IMR				=> IMR,		-- Master Reset
					CLK20			=> clk20,		-- Master Clock
					RTEM_SEL			=> RTEM_SEL( 1 downto 0),
					ms100_tc			=> ms100_tc,
					WD_EN			=> RTEM_SEL(4),
					WD_MR			=> WE_DCD( iADR_RTEM_WD_MR ),
					rtem_ini  		=> RTEM_INI,
					CNT_100			=> RTEM_CNT_100,
					CNT_MAX			=> RTEM_CNT_MAX,
				    	Hi_Cnt    		=> rtem_hi_cnt,
					Det_Sat			=> rtem_Det_sat,
					CMD_IN			=> WE_DCD( iADR_RTEM_IN ),
					CMD_OUT			=> WE_DCD( iADR_RTEM_OUT ),
					CMD_MID_IN		=> WE_DCD( iADR_RTEM_CMD_MID_In ),
					CMD_MID_OUT		=> WE_DCD( iADR_RTEM_CMD_MID_OUT ),
					CMD_HC_MR			=> WE_DCD( iADR_RTEM_HCMR ),
					RTEM_RED			=> RTEM_RED_TMP,				-- Input Status for In/Moving in
					RTEM_GRN			=> RTEM_GRN_TMP,				-- Input Status for Out/Moving Out
					RTEM_HS			=> RTEM_TILT,					-- Hard Stop Active Low	( Disabled )
					RTEM_ANALYZE		=> RTEM_ANALYZE,			-- Output to Move RTEM In
					RTEM_RETRACT		=> RTEM_RETRACT,			-- Output to Move RTEM Out
					RTEM_CNT			=> RTEM_Cnt,
					ST_RTEM   		=> rtem_state );	 			-- RTEM Status

			-- RTEM_RED and RTEM_GRN are normally closed ( or low )
			-- WHen they come in contact with the limit swith they open or transition high
			-- do to disable them must force them low all the time.
			RTEM_RED_TMP <= RTEM_RED when ( RTEM_TEST_REG(0) = '0' ) else '0';

			RTEM_GRN_TMP <= RTEM_GRN when ( RTEM_TEST_REG(1) = '0' ) else '0';
		end generate;
	     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------
		-- Temperature Sensor interface to the Analog Devices TMP04
		-- Instance 0 is for the 0n-board
		-- Instance 1 is for the 204 PreAmp
		-- Documentd 7/28/03
		-- The TMP_SENSE from the PreAmp is inverted, so invert it here to correct the Thi/Tlo
		TMP_SENSE_VEC(0) <= TMP_SENSE(0);
		TMP_SENSE_VEC(1) <= not( TMP_SENSE(1) );
		GEN_TEMP : if( TEMP_GEN = 1 ) generate
			TMP_GEN_LOOP : for i in 0 to 1 generate
			     U_TMP04 : TMP04
					port map(
		     	        	IMR            => IMR,							-- Master Reset
		          	     CLK20          => clk20,							-- Master Clock
		     	     	TMP_SENSE      => TMP_SENSE_VEC(i),					-- Temperature In
			               TMP_LO         => TMP_LO( (16*i)+15 downto 16*i),		-- Temperature PW Lo
			               TMP_HI		=> TMP_HI( (16*i)+15 downto 16*i) );	-- Tempeature PW Hi
			end generate;
		end generate;
	     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- END of EDAX Defined Component INSTANTIATIONS -----------------------------------
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- Start of ALTERA LPM PRIMATIVE INSTANTIATIONS -----------------------------------
     -----------------------------------------------------------------------------------

	     -----------------------------------------------------------------------------------------

	CWORD_LO_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_CW ),
			data			=> DSP_WD,
			q			=> CWORD( 15 downto 0 ) );

	DSCA_PW_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_DSCA_PW  ),
			data			=> DSP_WD,
			q			=> DSCA_PW );

	RMS_THR_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RMS_THR ),
			data			=> DSP_WD( 15 downto 0 ),
			q			=> RMS_Thr );
			
	-----------------------------------------------------------------------			
	two_loop : for i in 0 to 1 generate
		PRESET_LO_FF : LPM_FF
			generic map(
				LPM_WIDTH		=> 16,
				LPM_TYPE		=> "LPM_FF" )
			port map(
				aclr			=> IMR,						-- Master Reset
				clock		=> clk20,					-- DSP Clock
				enable		=> WE_DCD( iADR_PRESET_LO+i ),
				data			=> DSP_WD,
				q			=> PRESET( (16*i)+15 downto 16*i ) );
				
		TEST_REG_FF_LO : LPM_FF
			generic map(
				LPM_WIDTH		=> 16,
				LPM_TYPE		=> "LPM_FF" )
			port map(
				aclr			=> IMR,
				clock		=> clk20,					-- DSP Clock
				enable		=> WE_DCD( iADR_FIFO_LO+i ),
				data			=> DSP_WD,
				q			=> TEST_REG( (16*i)+15 downto 16*i ) );


		RTEM_CNT_100_FF : LPM_FF
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 16 )
			port map(
				aclr			=> imr,
				clock		=> clk20,
				enable		=> we_dcd( iADR_RTEM_CNT_100_L+i ),
				data			=> dsp_wd,
				q			=> RTEM_CNT_100( (16*i)+15 downto 16*i ) );

		RTEM_CNT_MAX_FF : LPM_FF
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 16 )
			port map(
				aclr			=> imr,
				clock		=> clk20,
				enable		=> we_dcd( iADR_RTEM_CNT_MAX_L+i ),
				data			=> dsp_wd,
				q			=> RTEM_CNT_MAX( (16*i)+15 downto 16*i ) );
				
	end generate;
	-----------------------------------------------------------------------			

	-----------------------------------------------------------------------			
	Four_Loop : for i in 0 to 3 Generate
		tlevel_ff : lpm_ff
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 16 )
			port map(
				aclr			=> imr,
				clock		=> clk20,
				enable		=> WE_DCD( iADR_DLEVEL0+i ),
				data			=> DSP_WD,
				q			=> TLEVEL( (16*i)+15 downto 16*i) );
	end generate;
	-----------------------------------------------------------------------			
	
	PRESET_MODE_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH 	=> 4)
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_PRESET_MODE ),
			data			=> DSP_WD( 3 downto 0 ),
			q			=> PRESET_MODE );

	DSP_RAMA_CNTR : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,					-- Asynchronous Clear
			clock		=> clk20,					-- Master Clock
			enable		=> WE_DCD( iADR_RAMA ),		-- Synchronous Load
			data			=> DSP_WD,				-- Load Data
			q			=> DSP_RAMA );				-- COunter Output
			
	TP_SEL_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,					-- DSP Write Clock
			enable		=> WE_DCD( iADR_TP_SEL ),
			data			=> DSP_WD,
			q			=> TP_SEL );

	rtem_ini_ff : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16,
			LPM_AVALUE 	=> "4882",
			LPM_PVALUE 	=> "0001001100010010" )			-- 0x1312 (1882) Power-On Value
		port map(
			aset			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_INI ),
			data			=> DSP_WD,
			q			=> RTEM_INI );
	
	hc_rate_lo_ff : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_hc_rate_lo ),
			data			=> DSP_WD,
			q			=> hc_Rate( 15 downto 0 ) );
			
	hc_rate_hi_ff : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 5 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_hc_rate_hi ),
			data			=> DSP_WD( 4 downto 0 ),
			q			=> hc_Rate( 20 downto 16 ) );

	hc_threshold_ff : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_hc_threshold ),
			data			=> DSP_WD,
			q			=> hc_threshold );

	DISC1_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_DISC1 ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> DISC1 );
			
	DISC2_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_DISC2 ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> DISC2 );
			
	EVCH_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_EVCH ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> EVCH );


	BIT_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_BIT ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> BIT );

	CGain_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_CGain ),
			data			=> DSP_WD,
			q			=> CGain );

	CLIP_MIN_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_CLIP_MIN ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> CLip_Min );

	CLIP_MAX_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_CLIP_Max ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> CLip_Max );

	Disc_En_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 6 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			Enable		=> WE_DCD( iAdr_Disc_En),
			data			=> DSP_WD( 5 downto 0 ),
			q			=> Disc_En );

	TC_SEL_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 13 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> we_Dcd( iADR_TC_SEL ),
			data			=> dsp_wd( 12 downto 0 ),
			q			=> TC_SEL );
			
	decay_ff : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_decay ),
			data			=> DSP_WD,
			q			=> decay );

	FGAIN_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> we_dcd( iADR_FGain ),
			data			=> DSP_WD,
			q			=> fgain );
	
	OFFSET_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_Offset ),
			data			=> DSP_WD,
			q			=> offset );

	BIT_PEAK1_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_BIT_PEAK1 ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> BIT_PEAK1 );
			
	BIT_PEAK2_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 12 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_BIT_PEAK2 ),
			data			=> DSP_WD( 11 downto 0 ),
			q			=> BIT_PEAK2 );
			
	BIT_CPS_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_BIT_CPS ),
			data			=> DSP_WD,
			q			=> BIT_CPS );
	
	BIT_PK2D_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_BIT_PK2D ),
			data			=> DSP_WD,
			q			=> BIT_PK2D );

	fir_dly_len_ff : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			lpm_width		=> 4 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iadr_fir_dly_len ),
			data			=> dsp_wd( 3 downto 0 ),
			q			=> fir_dly_len );

	fir_offset_inc_ff : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			lpm_width		=> 12 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_FIR_DLY_INC ),
			data			=> dsp_wd( 11 downto 0 ),
			q			=> fir_offset_inc );
		
						
	DSO_TRIG_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 4 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iadr_dso_trig ),
			data			=> dsp_wd( 3 downto 0 ),
			q			=> dso_trig );

	DSO_INT_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_DSO_INT ),
			data			=> dsp_wd,
			q			=> DSO_INT );

	SHDisc_Dly_Sel_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 9 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_SHDisc_Dly_Sel ),
			data			=> DSP_WD( 8 downto 0 ),
			q			=> SHDisc_Dly_Sel );

	Blank_Mode_ff : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_Blank_Mode ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> Blank_Mode );
				
	Blank_Delay_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_Blank_Delay ),
			data			=> DSP_WD,
			q			=> Blank_Delay );
				
	Blank_width_ff : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_Blank_Width),
			data			=> DSP_WD,
			q			=> Blank_width );

	Disc_Evch_ff : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_Disc_Evch ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> DISCEVCH );

	RTEM_SEL_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 8 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_SEL ),
			data			=> DSP_WD( 7 downto 0 ),
			q			=> RTEM_SEL );
		  
	RTEM_TEST_REG_FF : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_RTEM_TEST_REG ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> RTEM_TEST_REG );
			
	PS_SEL_REG_FF : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_PS_SEL ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> PS_SEL );
		
	INT_EN_REG_FF : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_INT_EN ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> INT_EN );

	Det_Type_ff : lpm_ff
		generic map(
			LPM_WIDTH		=> 4 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_DET_TYPE ),
			data			=> DSP_WD( 3 downto 0 ),
			q			=> Det_Type );
     ----------------------------------------------------------------------------------------------
	-- END of ALTERA LPM PRIMATIVE INSTANTIATIONS ------------------------------------------------
     ----------------------------------------------------------------------------------------------


	IMR			<= not( OMR );

	-- Control Word Parameters -------------------------------------------------------------------
	
	-- PreAmp Control Word ----------------------------------------------------------------------
	BIT_DAC_EN 	<= '1' when ( CWORD( CW_BIT_ENABLE ) = '0' ) and ( CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) = PA_BITA ) else
				   '1' when ( CWORD( CW_BIT_ENABLE ) = '0' ) And ( CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) = PA_BITD ) else 
				   '0';

	-- Input Steering Mux
	with CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) select
		PA_MODE( 2 downto 0 ) <= 
				   "001" when PA_DET0,			-- DET1
				   "010" when PA_DET1,			-- DET2 ( AC Coupled )
				   "100" when others; 			-- BIT DAC Analog and Digital

	-- Active Reset Enable  Ver 4x28
	with Conv_Integer( Det_Type ) select
		Act_Reset		<= '1' when DET_UNICORN,
					   '1' when DET_AP40,
					   '0' when DET_AP10,
					   '0' when others;

	-- 1 = 2X Gain, 0 = 1X Gain Ver 4x28
	with Conv_Integer( Det_Type ) select
		PA_Mode(3) 	<= '0' when DET_204NFR,			-- 204 Preamp with Non FR ( don't throw x2 gain ) 4x2A
				        '1' when DET_204FR,			-- 204 Preamp with FR 
					   '1' when DET_UNICORN,
					   '1' when DET_AP40,
					   '0' when DET_AP10,			-- Ver 2x30 Disable 2X Gain Switch
					   '0' when others;	-- Includes DET_AP10
--	PA_Mode(3)	<= CWord(CW_GAIN_2X);	

	-- When to Invert the Input ( due to inverted Ramp )	-- Ver 4x28
	with Conv_Integer( Det_Type ) select
		AD_Invert <= '1' when DET_AP10,			-- should be 1
				   '0' when others;

	-- 50% Gain increase ( in AD9240 otherwise only 25% )
	with Conv_Integer( Det_Type ) select
		AD9240_PCT_50	<= '1' when DET_AP10,
          		        '0' when others;
	-- Ver 4x28
	Disc_Inv(0) 	<= not( AD_Invert );		-- 1 = Invert		-- Ver 4x2B ( confirmed U10-1 with oscolloscope )
	Disc_Inv(1)	<= not( Disc_Inv(0) );		-- Disc_Inv(1) always not Disc_Inv(0)
	
	
	-- WDS Mode, invert the OHV_ENABLE signal	
	HV_Enable		<= OHV_ENABLE;

	rtd_test 		<= '1' when ( TP_SEL( 15 downto 12 ) = x"F" ) else '0';

	-- FIFO Interface ----------------------------------------------------------------------------
	-- FIFO Master Reset -----------------------------------------------------
	FF_RS  	<= '0' when ( IMR 				= '1' ) else
		        '0' when ( WE_DCD( iADR_FIFO_MR ) = '1' ) else '1';
	
	-- FIFO Interface ----------------------------------------------------------------------------

	-- Real Time Bus to the Scan Generator ---------------------------	
	RT_IN(0)		<= '0';			-- Unused, when 0 - Output is tristate due to open collector
	RT_IN(1)		<= Time_Enable;	
---------------------------
	-- Real Time Bus to the Scan Generator ------------------------------------------------------

	-- DSP Read Back Mux -------------------------------------------------------------------------
	-- The two additional DSP Bits to output the FIFO Flag Bits when the FIFO Data is read
	DSP_DATA_EN	<= '1' when ( DSP_READ = '1' ) or ( STAT_OE = '1' ) else '0';
	
				-- Status, generated data
	status_oe <= '1' when ( conv_integer( DSP_A1 ) = iADR_STAT ) else '0';
	
	Status_Vec <= 	'0'					-- Bit 15		-- Ver 3565
				& OTR				-- Bit 14
				& RTEM_RED			-- Bit 13
				& RTEM_GRN			-- Bit 12
				& RTEM_State(3)		-- Bit 11
				& RTEM_State(2)		-- Bit 10
				& RTEM_State(1)		-- Bit 9
				& RTEM_State(0)		-- Bit 8
				& '0'				-- Bit 7
				& not( DA_OFS )		-- Bit 6
				& HV_Enable(1)			-- Bit 5
				& HV_Enable(0)			-- Bit 4
				& Time_Enable 			-- Bit 3
				& AD_BUSY				-- Bit 2
				& not( FF_FF )			-- Bit 1
				& not( FF_EF );		-- Bit 0

	DSP_D	<= iDSP_D_MUX 		when ( DSP_DATA_EN = '1' ) else 
	        	   "ZZZZZZZZZZZZZZZZ";

	iDSP_D_Mux	<= Status_Vec 	 when ( STAT_OE = '1' ) or ( Status_OE = '1' ) else
				   iDSP_D;

	with CONV_INTEGER( DSP_A1 ) select
	    iDSP_D <= CONV_STD_LOGIC_VECTOR( Ver, 16 ) 					when iADR_ID,
				TEST_REG( 15 downto 0 )							when iADR_FIFO_LO,
				TEST_REG( 31 downto 16 )							when iADR_FIFO_HI,
				CWORD( 15 downto 0 ) 							when iADR_CW,
				DSCA_PW										when iADR_DSCA_PW,
				PRESET( 15 downto 0 )							when iADR_PRESET_LO,
				PRESET( 31 downto 16 )							when iADR_PRESET_HI,
				ZEROES( 15 downto 4 ) & PRESET_MODE				when iADR_PRESET_MODE,
				DSP_RAMA										when iADR_RAMA,
				TP_SEL										when iADR_TP_SEL,
				RTEM_INI										when iADR_RTEM_INI,
				HC_Rate( 15 downto 0 )							when iADR_RTEM_HC_RATE_LO,
				ZEROES( 15 downto 5 ) & HC_Rate( 20 downto 16 ) 		when iADR_RTEM_HC_RATE_HI,
				HC_Threshold									when iADR_RTEM_HC_THRESHOLD,
				ZEROES( 15 downto 12 ) & DISC1					when iADR_DISC1,
				ZEROES( 15 downto 12 ) & DISC2					when iADR_DISC2,
				ZEROES( 15 downto 12 ) & EVCH 					when iADR_EVCH, 
				ZEROES( 15 downto 12 ) & BIT						when iADR_BIT, 
				CGain										when iADR_CGain,
				Zeroes( 15 downto 6 ) & Disc_En					when iADR_Disc_En,
				fgain										when iadr_fgain,
				offset										when iadr_offset,
				zeroes(  15 downto 13 ) & tc_sel					when iADR_TC_SEL,
				tlevel(  15 downto  0 )							when iADR_DLEVEL0,
				TLEVEL(  31 downto 16 ) 							when iADR_DLEVEL1,
				TLEVEL(  47 downto 32 ) 							when iADR_DLEVEL2,
				TLEVEL(  63 downto 48 ) 							when iADR_DLEVEL3,
				ZEROES(  15 downto 12 ) & CLIP_MIN			 		when iADR_CLIP_MIN,
				ZEROES(  15 downto 12 ) & CLIP_MAX			 		when iADR_CLIP_MAX,
				decay(   15 downto  0 )							when iADR_decay,
				Zeroes( 15 downto 2 ) & INT_EN 					when iADR_INT_EN,
			   	TMP_LO( 15 downto  0 )							when iADR_TLO,
				TMP_HI( 15 downto  0 )							when iADR_THI,
			   	TMP_LO( 31 downto 16 )							when iADR_DU0_TLO,
				TMP_HI( 31 downto 16 )							when iADR_DU0_THI,
--				SN_REG( 15 downto  0 )							when iADR_SN0,
--				SN_REG( 31 downto 16 )							when iADR_SN1,
--				SN_REG( 47 downto 32 )							when iADR_SN2,
--				SN_REG( 63 downto 48 )							when iADR_SN3,
				Zeroes( 15 downto 12 ) & BIT_Peak1					when iADR_BIT_PEAK1,
				Zeroes( 15 downto 12 ) & BIT_Peak2					when iADR_BIT_Peak2,
				BIT_CPS										when iADR_BIT_CPS,
				bIT_PK2D										when iADR_BIT_PK2D,
				x"00" & SCA_LU_DATA								when iADR_SCA_LU,
				
				ROI_LU_DATA									when iADR_ROI_LU,
  				CPS( 15 downto 0 ) 								when iADR_IN_CPSL,
				ZEROES( 15 downto 5 ) & CPS( 20 downto 16 ) 			when iADR_IN_CPSH,
				NPS( 15 downto 0 ) 								when iADR_OUT_CPSL,
				zeroes( 15 downto 5 ) & NPS( 20 downto 16 ) 			when iADR_OUT_CPSH,								
				NET_CPS( 15 downto  0 ) 							when iADR_NET_CPSL,
				NET_CPS( 31 downto 16 ) 							when iADR_NET_CPSH,
				NET_NPS( 15 downto  0 ) 							when iADR_NET_NPSL,
				NET_NPS( 31 downto 16 ) 							when iADR_NET_NPSH,
				LTIME( 15 downto  0 ) 							when iADR_LTIME_LO,
				LTIME( 31 downto 16 ) 							when iADR_LTIME_HI,
				CTIME( 15 downto  0 ) 							when iADR_CTIME_LO,
				CTIME( 31 downto 16 ) 							when iADR_CTIME_HI,
				ROI_SUM( 15 downto 0 )							when iADR_ROI_SUML,
				ROI_SUM( 31 downto 16 )							when iADR_ROI_SUMH,				
				"00" & AD_MIN									when iADR_RAMP_LO,	-- Ver > 3580
				"00" & AD_MAX									when iADR_RAMP_HI,	-- Ver > 3580
--				AD_MIN(15) & AD_MIN(15) & AD_MIN( 15 downto 2 )		when iADR_RAMP_LO,
--				AD_MAX(15) & AD_MAX( 15 downto 1 )					when iADR_RAMP_HI,
				Reset_Width									when iADR_Reset_Width,
				Reset_Time( 15 downto  0 )						when iADR_PA_TIME_LO,
				ReseT_Time( 31 downto 16 )						when iADR_PA_TIME_HI,
				BIT_ADATA										when iADR_BIT_ADC,
				debug_ad										when iADR_FADC,
				Zeroes( 15 downto 9 ) & SHDisc_Dly_Sel				when iADR_SHDisc_Dly_Sel,	
				zeroes( 15 downto 4 ) & dso_trig					when iADR_DSO_TRIG,
				DSO_INT										when iADR_DSO_INT,
				zeroes( 15 downto 4 ) & fir_dly_len				when iadr_fir_dly_len,
				zeroes( 15 downto 12 ) & fir_offset_inc				when iadr_fir_dly_inc,
				Zeroes( 15 downto 8 ) & RTEM_SEL 					when iADR_RTEM_SEL,	
				RTEM_CNT_100( 15 downto  0 )						when iADR_RTEM_CNT_100_L,
				RTEM_CNT_100( 31 downto 16 )						when iADR_RTEM_CNT_100_H,
				RTEM_CNT_MAX( 15 downto  0 )						when iADR_RTEM_CNT_MAX_L,
				RTEM_CNT_MAX( 31 downto 16 )						when iADR_RTEM_CNT_MAX_H,
				RTEM_CNt( 15 downto 0 )							when iADR_RTEM_Cnt_L,
				RTEM_Cnt( 31 downto 16 ) 						when iADR_RTEM_CnT_H,
				Zeroes( 15 downto 8 ) & DiscCnts(  7 downto  0 )  	when iADR_SH_Disc_Cnts,
				Zeroes( 15 downto 8 ) & DiscCnts( 15 downto  8 )  	when iADR_H_Disc_Cnts,
				Zeroes( 15 downto 8 ) & DiscCnts( 23 downto 16 )  	when iADR_M_Disc_Cnts,
				Zeroes( 15 downto 8 ) & DiscCnts( 31 downto 24 )  	when iADR_L_Disc_Cnts,
				Zeroes( 15 downto 8 ) & DiscCnts( 39 downto 32 )  	when iADR_E_Disc_Cnts,
				Zeroes( 15 downto 2 ) & Blank_Mode					when iADR_Blank_Mode,
				Blank_Delay									when iADR_BLANK_DELAY,
				Blank_Width									when iADR_BLANK_WIDTH,
				Zeroes( 15 downto 2 ) & DISCEVCH					when iADR_Disc_Evch,
				Zeroes( 15 downto 2 ) & PS_SEL					when iADR_PS_SEL,
				Preset_Done_Cnt								when iADR_Preset_Done_Cnt,
				rms_out 										when iADR_RMS_OUT,
				x"0" & "000" & SHDisc_MDly						when iADR_AUTO_SHDISC,
				RMS_THR										when iADR_RMS_THR,
				FF_DSO_CNT									when iADR_DSO_FF_CNT,
				x"00"															-- 15:8
				& RTEM_STATE														-- 7:4
				& RTEM_GRN														-- 3
				& RTEM_RED														-- 2
				& RTEM_TEST_REG( 1 downto 0 )						when iADR_RTEM_TEST_REG,	-- 1:0
				x"000" & Det_Type								when iADR_DET_TYPE,		
				X"1971" when others;


	----- TP Output Mux --------------------------------------------------------------------------
	dso_sel	<= tp_sel( 5 downto 0 );

	ATP_SEL 	<= tp_sel( 11 downto 8 );

	
	with conv_integer( TP_SEL( 15 downto 12 )) select
	     TP <= 
			OTR    									-- Bit 7
			& DSP_IRQ									-- Bit 6
			& DSP_STB( 3 downto 0 )						-- Bits 5 downto 2
			& WE_DCD( iADR_Reset_Mr )					-- Bit 1 was 
			& WE_DCD( iADR_FIR_MR )			when 0,		-- Bit 0


			EVCH( 2 downto 0 )							-- 7:5
			& WE_DCD( iADR_EVCH )						-- 4
			& DA_OFS									-- 3
			& DA_CLK									-- 2
			& DA_DOUT									-- 1
			& DA_UPD 						when 1,		-- 0	
						

			TIME_ENABLE								-- 7
			& MEAS_DONE								-- 6
			& DSP_IRQ									-- 5
			& INT_EN(0)								-- 4
			& FF_OE									-- 3
			& FF_RD									-- 2
			& FF_EF									-- 1
			& FF_WR						when 2,		-- 0

			-- THis Decode is used for the FIFO Interface Debugging
			FF_FF 									-- 7
			& FF_EF									-- 6
			& FF_RS									-- 5
			& FF_OE									-- 4
			& FF_RD									-- 3	
			& FF_WR									-- 2
			& FF_RCK									-- 1
			& DSP_READ					when 3,		-- 0
			-- THis Decode is used for the FIFO Interface Debugging
		
			-- This decode is used for the ADS8321 Interface Debugging		
			BIT_ADATA( 2 downto 0 )						-- 7:5
			& AD_BUSY									-- 4
			& WE_DCD( iADR_BIT_ADC )    					-- 3
			& AD_Dout									-- 2
			& AD_CS									-- 1
			& AD_CLK						when 4,   	-- 0
			-- This decode is used for the ADS8321 Interface Debugging		

			"00" 									-- 7:6
			& RTEM_SEL(1)								-- 5 WDS Mode Decode = 1
			& RTEM_SEL(0)								-- 4 WDS Mode Decode = 0
			& TMP_SENSE(1)								-- 3 Actve Low Move Flag
			& '0' 									-- 2
			& '0'									-- 1 Active High Move Flag
			& TIME_ENABLE				when 5,			-- 0 EDX Analysis
			
			Preset_Out								-- 7
			& RT_IN(0)								-- 6 EDX_ANAL
			& LS_Abort								-- 5
			& RT_OUT(1)								-- 4 IPD_END
			& RT_OUT(0)								-- 3 VIDEO_ABORT
			& IPD_END									-- 2
			& VIDEO_ABORT								-- 1
			& TIme_Enable					when 6,		-- 0

			x"0"										-- 7:4
			& IHV_DIS(1)								-- 3
			& Hot_Swap_En								-- 2
			& Act_Reset								-- 1
			& PA_Reset_Gen					When 7,		-- 0

			FF_WDATA( 17 downto 16 )					-- 7:6	FIFO Key
			& FF_WDATA( 3  downto  0 )					-- 5:1
			& Meas_Done
			& FF_WR						when 8,	-- 0
			

		
	
			-- This decode is used for the DAC8420 Interface Debugging		
			BIT_DAC_EN								-- 7
			& BIT_DAC_LD								-- 6
			& BIT_DAC_DATA( 1 downto 0 )					-- 5: 4
			& DA_CLK									-- 3
			& DA_DOUT									-- 2
			& DA_UPD									-- 1
			& DA_OFS						when 9,		-- 0
			-- This decode is used for the DAC8420 Interface Debugging		

			
			Debug( 7 downto 0 )				when 10,
			Debug( 15 downto 8 ) 			when 11,
						

			"00"
			& ad_clr								-- 5
			& WE_DCD( iADR_Reset_Mr )				-- 4
			& WE_DCD( iADR_FIR_MR )					-- 3
			& PA_Reset							-- 2
			& PA_NSLOPE							-- 1
			& PA_PSLOPE					when 15, 	-- 0


		X"12" 		when others;

	---- TP Output Mux ------------------------------------------------------------
	
	-- Ver 4007 ----------------------------------------------------------------------------------
	-- EvCh Settings
	--	DISCEVCH Code: Setting
	--			0	2.5
	--			1	5.0
	-- 			2	10.0
	--			3	20.0
	clock_proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			Disc_EvCh_LD 	<= '0';
			EvCh_Ld		<= '0';
			EvChDac		<= x"000";
			Disc_evch		<= "00";
			Ld_EvCh		<= '0';
		elsif(( CLk20'event ) and ( Clk20 = '1' )) then

			Disc_EVCH_LD 	<= WE_DCD( iADR_Disc_Evch );
			EvCh_Ld		<= WE_DCD( iADR_EVCH );
			
			if( Disc_EVCH_LD = '1' ) then
				Ld_EvCh <= '1';
				case conv_integer( DET_TYPE ) is
					when DET_UNICORN => 
						case DISCEVCH is
							when "00" 	=> EvChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "11";	-- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "10"; 	-- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 2047, 12 ); Disc_evch <= "01";	-- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(    0, 12 ); Disc_evch <= "00"; 	-- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;
					when DET_AP40 => 
						case DISCEVCH is
							when "00" 	=> EvChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "11";	-- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "10"; 	-- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 2047, 12 ); Disc_evch <= "01";	-- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(    0, 12 ); Disc_evch <= "00"; 	-- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;
					when DET_AP10 => 
						case DISCEVCH is
							when "00" 	=> EvChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "11";	-- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 4095, 12 ); Disc_evch <= "10"; 	-- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 2047, 12 ); Disc_evch <= "01";	-- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(    0, 12 ); Disc_evch <= "00"; 	-- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;

					when DET_204FR =>
						case DISCEVCH is
							when "00" 	=> EVChDac <= conv_Std_logic_Vector( 3584, 12 ); Disc_evch <= "11"; -- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 2650, 12 ); Disc_evch <= "10"; -- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 1536, 12 ); Disc_evch <= "01"; -- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(  512, 12 ); Disc_evch <= "00"; -- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;
					when DET_204NFR =>
						case DISCEVCH is
							when "00" 	=> EVChDac <= conv_Std_logic_Vector( 3584, 12 ); Disc_evch <= "11"; -- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 2650, 12 ); Disc_evch <= "10"; -- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 1536, 12 ); Disc_evch <= "01"; -- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(  512, 12 ); Disc_evch <= "00"; -- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;
					when others	=>
						case DISCEVCH is
							when "00" 	=> EVChDac <= conv_Std_logic_Vector( 3584, 12 ); Disc_evch <= "11"; -- 2.5eV/Ch
							when "01" 	=> EVChDac <= conv_Std_logic_Vector( 2650, 12 ); Disc_evch <= "10"; -- 5.0 eV/Ch
							when "10" 	=> EVChDac <= conv_Std_logic_Vector( 1536, 12 ); Disc_evch <= "01"; -- 10.0 eV/Ch
							when "11" 	=> EVChDac <= conv_Std_logic_Vector(  512, 12 ); Disc_evch <= "00"; -- 20.0 eV/Ch
							when others 	=> EvChDac <= x"000"; Disc_evch <= "00";
						end case;
				end case;
			elsif( EvCh_LD = '1' ) then
				Ld_EvCh <= '1';
				EvChDac <= EvCh;			
			else
				LD_EvCh <= '0';
			end if;
		end if;
	end process;		
	-- Ver 4007 ----------------------------------------------------------------------------------



----------------------------------------------------------------------------------------
end BEHAVIORAL;          -- EP99840.VHD
----------------------------------------------------------------------------------------


