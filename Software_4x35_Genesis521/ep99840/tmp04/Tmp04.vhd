---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	tmp04.VHD
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--             Module to read the Temperature for the Analog Devices tmp04 
--             Pulse Width Modulated Temperature Sensor
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		February 08, 2001 - MCS:
--			Clock change to 20Mhz for EDI-II+
--		************************************************************************************************
--		September 22, 2000 - MCS:
--			Final Release
--		************************************************************************************************
--		February 17, 2000 - MCS
--			Updates
--		November 29, 1999 - MCS
--			Updates 
--		October 18, 1999 - MCS
--			Toplevel Module changed to 4035.009.99770
--		June 24, 1999 - MCS
--			Module has been debuggged and implemented in the EDI-II Board
--		May, 27, 1999 - MCS
--			Module has been debugged, but it must be cleaned up and documented
--		May 20, 1999 - MCS
--			Debugged using GLUEFPGA on the EDI-II Board.
--		May 12, 1999 - MCS
--			Added some PCI-DSP Handshaking
--		April 12, 1999 - MCS
--			Updated for final configuration
--		March 29, 1999 - MCS
--			Update and verify pin definitions to schematic
--		Perviously debugged on the PCI Scan Generator Board (4035.065.20000 )
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

---------------------------------------------------------------------------------------------------
entity tmp04 is port( 
	IMR            : in      std_logic;				     -- Master Reset
     CLK20          : in      std_logic;                         -- Master Clock
     TMP_SENSE      : in      std_logic;                         -- Pipelined   
     TMP_LO         : out  	std_logic_vector( 15 downto 0 );	-- Temperature Low
     TMP_HI		: out	std_logic_vector( 15 downto 0 ));	-- Temperature Hi
end tmp04;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of tmp04 is
     -- Constant Declarations

     -- Combinatorial Signals
     signal TMP_CLR           : std_logic;
     signal LD_LO             : std_logic;
     signal LD_HI             : std_logic;
     -- Registered Signals
     signal TMP_VEC           : std_logic_vector(  1 downto 0 );
     signal TMP_CNT           : std_logic_vector( 20 downto 0 );

begin
     TMP_CLR    <= '1' when (( TMP_VEC = "10" ) or  ( TMP_VEC = "01" )) else '0';
     LD_HI      <= '1' when ( TMP_VEC = "10" ) else '0';
     LD_LO      <= '1' when ( TMP_VEC = "01" ) else '0';

     -- Counter to measure the Pulse Widths
	U1 : LPM_COUNTER 
			generic map( 
                    LPM_WIDTH => 21 )
 			port map( 
                    clock     => CLK20,
                    aclr      => IMR, 
                    sclr      => TMP_CLR, 
                    q         => TMP_CNT );

     -- Register to hold the Low Pulse Width
     U2 : LPM_FF
               generic map( 
                    LPM_WIDTH => 16 )
               port map(
                    clock     => CLK20,
                    aclr      => IMR,
                    enable    => LD_LO,
                    data      => TMP_CNT( 20 downto 5 ),
                    q         => TMP_LO );

     -- Register to hold the High Pulse Width
     U3 : LPM_FF
               generic map( 
                    LPM_WIDTH => 16 )
               port map(
                    clock     => CLK20,
                    aclr      => IMR,
                    enable    => LD_HI,
                    data      => TMP_CNT( 20 downto 5 ),
                    q         => TMP_HI );

     ----------------------------------------------------------------------------------------------
     AD_WR_PROC : process( CLK20, IMR )
     begin
          if( IMR = '1' ) then
               TMP_VEC        <= "00";
          elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
               TMP_VEC        <= TMP_VEC(0) & TMP_SENSE;
         end if;
     end process;
     ----------------------------------------------------------------------------------------------
          
---------------------------------------------------------------------------------------------------
end behavioral;			-- tmp04.VHD
---------------------------------------------------------------------------------------------------

