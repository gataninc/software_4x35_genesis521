-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FIR_MEM.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the delay lines to be used for the DPP-II. The delay
--		lines are broken down into the individual sections to generate all 8 time
--		constant's FIRs
--
--		It outputs 10 different version of delays from the input data (adata)
--		data1a	1us from adata
--		data1b	1us from data1a
--		data2	2us from data1b
--		data4	4us from data2
--		data8	8us from data4
--		data16	16us from data8
--		data32	32us	from data16
--		data64	64us from data32
--		data128	128us from data128
--
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal IMR		: std_Logic := '1';					-- Master Reset
	signal CLK20		: std_logic := '0';					-- 5Mhz Clock ( from 20Mhz )
	signal TMP_SENSE	: std_logic;
	signal TMP_LO		: std_logic_Vector( 15 downto 0 );
	signal TMP_HI		: std_logic_vector( 15 downto 0 );
	
	---------------------------------------------------------------------------------------------------
	component TMP04 
		port( 
			IMR            : in      std_logic;				     -- Master Reset
		     CLK20          : in      std_logic;                         -- Master Clock
		     TMP_SENSE      : in      std_logic;                         -- Pipelined   
		     TMP_LO         : out  	std_logic_vector( 15 downto 0 );	-- Temperature Low
		     TMP_HI		: out	std_logic_vector( 15 downto 0 ));	-- Temperature Hi
	end component TMP04;
	---------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------

begin
	IMR 		<= '0' after 800 ns;
	CLK20	<= not( CLK20 ) after 25 ns;
	
	U : tmp04
		port map(
			IMR			=> IMR,
			CLK20		=> CLK20,
			TMP_SENSE		=> TMP_SENSE,
			TMP_LO		=> TMP_LO,
			TMP_HI		=> TMP_HI );
			
	tmp_sense_proc : process
	begin
		TMP_SENSE <= '1';
		wait for 10 us;
		forever_loop : loop
			TMP_SENSE <= '0';
			wait for 14 ms;
			TMP_SENSE <= '1';
			wait for 15 ms;
		end loop;
	end process;
     --------------------------------------------------------------------------

	
-------------------------------------------------------------------------------
end test_bench;
-------------------------------------------------------------------------------

