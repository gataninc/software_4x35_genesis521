onerror {resume}
quietly WaveActivateNextPane {}
add wave -noupdate -format Logic -radix binary /tb/tmp_sense
add wave -noupdate -format Literal -radix hexadecimal /tb/tmp_lo
add wave -noupdate -format Literal -radix hexadecimal /tb/tmp_hi
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {39706000 ps} {74519000 ps} {5741000 ps} {757018 ps}
WaveRestoreZoom {465726 ps} {1778226 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
