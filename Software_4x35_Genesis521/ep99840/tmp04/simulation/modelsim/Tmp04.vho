-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 10:41:52"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	Tmp04 IS
    PORT (
	CLK20 : IN std_logic;
	IMR : IN std_logic;
	TMP_SENSE : IN std_logic;
	TMP_LO : OUT std_logic_vector(15 DOWNTO 0);
	TMP_HI : OUT std_logic_vector(15 DOWNTO 0)
	);
END Tmp04;

ARCHITECTURE structure OF Tmp04 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_TMP_SENSE : std_logic;
SIGNAL ww_TMP_LO : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_TMP_HI : std_logic_vector(15 DOWNTO 0);
SIGNAL U1_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL CLK20_apadio : std_logic;
SIGNAL IMR_apadio : std_logic;
SIGNAL TMP_SENSE_apadio : std_logic;
SIGNAL TMP_LO_a15_a_apadio : std_logic;
SIGNAL TMP_LO_a14_a_apadio : std_logic;
SIGNAL TMP_LO_a13_a_apadio : std_logic;
SIGNAL TMP_LO_a12_a_apadio : std_logic;
SIGNAL TMP_LO_a11_a_apadio : std_logic;
SIGNAL TMP_LO_a10_a_apadio : std_logic;
SIGNAL TMP_LO_a9_a_apadio : std_logic;
SIGNAL TMP_LO_a8_a_apadio : std_logic;
SIGNAL TMP_LO_a7_a_apadio : std_logic;
SIGNAL TMP_LO_a6_a_apadio : std_logic;
SIGNAL TMP_LO_a5_a_apadio : std_logic;
SIGNAL TMP_LO_a4_a_apadio : std_logic;
SIGNAL TMP_LO_a3_a_apadio : std_logic;
SIGNAL TMP_LO_a2_a_apadio : std_logic;
SIGNAL TMP_LO_a1_a_apadio : std_logic;
SIGNAL TMP_LO_a0_a_apadio : std_logic;
SIGNAL TMP_HI_a15_a_apadio : std_logic;
SIGNAL TMP_HI_a14_a_apadio : std_logic;
SIGNAL TMP_HI_a13_a_apadio : std_logic;
SIGNAL TMP_HI_a12_a_apadio : std_logic;
SIGNAL TMP_HI_a11_a_apadio : std_logic;
SIGNAL TMP_HI_a10_a_apadio : std_logic;
SIGNAL TMP_HI_a9_a_apadio : std_logic;
SIGNAL TMP_HI_a8_a_apadio : std_logic;
SIGNAL TMP_HI_a7_a_apadio : std_logic;
SIGNAL TMP_HI_a6_a_apadio : std_logic;
SIGNAL TMP_HI_a5_a_apadio : std_logic;
SIGNAL TMP_HI_a4_a_apadio : std_logic;
SIGNAL TMP_HI_a3_a_apadio : std_logic;
SIGNAL TMP_HI_a2_a_apadio : std_logic;
SIGNAL TMP_HI_a1_a_apadio : std_logic;
SIGNAL TMP_HI_a0_a_apadio : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL TMP_SENSE_acombout : std_logic;
SIGNAL TMP_VEC_a0_a : std_logic;
SIGNAL TMP_VEC_a1_a : std_logic;
SIGNAL rtl_a0 : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL U1_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL U1_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL reduce_nor_11 : std_logic;
SIGNAL U2_adffs_a15_a : std_logic;
SIGNAL U2_adffs_a14_a : std_logic;
SIGNAL U2_adffs_a13_a : std_logic;
SIGNAL U2_adffs_a12_a : std_logic;
SIGNAL U2_adffs_a11_a : std_logic;
SIGNAL U2_adffs_a10_a : std_logic;
SIGNAL U2_adffs_a9_a : std_logic;
SIGNAL U2_adffs_a8_a : std_logic;
SIGNAL U2_adffs_a7_a : std_logic;
SIGNAL U2_adffs_a6_a : std_logic;
SIGNAL U2_adffs_a5_a : std_logic;
SIGNAL U2_adffs_a4_a : std_logic;
SIGNAL U2_adffs_a3_a : std_logic;
SIGNAL U2_adffs_a2_a : std_logic;
SIGNAL U2_adffs_a1_a : std_logic;
SIGNAL U2_adffs_a0_a : std_logic;
SIGNAL reduce_nor_9 : std_logic;
SIGNAL U3_adffs_a15_a : std_logic;
SIGNAL U3_adffs_a14_a : std_logic;
SIGNAL U3_adffs_a13_a : std_logic;
SIGNAL U3_adffs_a12_a : std_logic;
SIGNAL U3_adffs_a11_a : std_logic;
SIGNAL U3_adffs_a10_a : std_logic;
SIGNAL U3_adffs_a9_a : std_logic;
SIGNAL U3_adffs_a8_a : std_logic;
SIGNAL U3_adffs_a7_a : std_logic;
SIGNAL U3_adffs_a6_a : std_logic;
SIGNAL U3_adffs_a5_a : std_logic;
SIGNAL U3_adffs_a4_a : std_logic;
SIGNAL U3_adffs_a3_a : std_logic;
SIGNAL U3_adffs_a2_a : std_logic;
SIGNAL U3_adffs_a1_a : std_logic;
SIGNAL U3_adffs_a0_a : std_logic;

BEGIN

ww_CLK20 <= CLK20;
ww_IMR <= IMR;
ww_TMP_SENSE <= TMP_SENSE;
TMP_LO <= ww_TMP_LO;
TMP_HI <= ww_TMP_HI;

CLK20_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

TMP_SENSE_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TMP_SENSE,
	combout => TMP_SENSE_acombout);

TMP_VEC_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- TMP_VEC_a0_a = DFFE(TMP_SENSE_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => TMP_SENSE_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => TMP_VEC_a0_a);

TMP_VEC_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- TMP_VEC_a1_a = DFFE(TMP_VEC_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => TMP_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => TMP_VEC_a1_a);

rtl_a0_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a0 = TMP_VEC_a0_a $ TMP_VEC_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TMP_VEC_a0_a,
	datad => TMP_VEC_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a0);

U1_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY()

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	cout => U1_awysi_counter_acounter_cell_a0_a_aCOUT);

U1_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a1_a $ U1_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a0_a_aCOUT # !U1_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a1_a,
	cin => U1_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a1_a,
	cout => U1_awysi_counter_acounter_cell_a1_a_aCOUT);

U1_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a2_a $ !U1_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a2_a & !U1_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a2_a,
	cin => U1_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a2_a,
	cout => U1_awysi_counter_acounter_cell_a2_a_aCOUT);

U1_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a3_a $ U1_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a2_a_aCOUT # !U1_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a3_a,
	cin => U1_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a3_a,
	cout => U1_awysi_counter_acounter_cell_a3_a_aCOUT);

U1_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a4_a $ !U1_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a4_a & !U1_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a4_a,
	cin => U1_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a4_a,
	cout => U1_awysi_counter_acounter_cell_a4_a_aCOUT);

U1_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a5_a $ U1_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a4_a_aCOUT # !U1_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a5_a,
	cin => U1_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a5_a,
	cout => U1_awysi_counter_acounter_cell_a5_a_aCOUT);

U1_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a6_a $ !U1_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a6_a & !U1_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a6_a,
	cin => U1_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a6_a,
	cout => U1_awysi_counter_acounter_cell_a6_a_aCOUT);

U1_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a7_a $ U1_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a6_a_aCOUT # !U1_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a7_a,
	cin => U1_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a7_a,
	cout => U1_awysi_counter_acounter_cell_a7_a_aCOUT);

U1_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a8_a $ !U1_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a8_a & !U1_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a8_a,
	cin => U1_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a8_a,
	cout => U1_awysi_counter_acounter_cell_a8_a_aCOUT);

U1_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a9_a $ U1_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a8_a_aCOUT # !U1_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a9_a,
	cin => U1_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a9_a,
	cout => U1_awysi_counter_acounter_cell_a9_a_aCOUT);

U1_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a10_a $ !U1_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a10_a & !U1_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a10_a,
	cin => U1_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a10_a,
	cout => U1_awysi_counter_acounter_cell_a10_a_aCOUT);

U1_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a11_a $ U1_awysi_counter_acounter_cell_a10_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a10_a_aCOUT # !U1_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a11_a,
	cin => U1_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a11_a,
	cout => U1_awysi_counter_acounter_cell_a11_a_aCOUT);

U1_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a12_a $ !U1_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a12_a & !U1_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a12_a,
	cin => U1_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a12_a,
	cout => U1_awysi_counter_acounter_cell_a12_a_aCOUT);

U1_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a13_a $ U1_awysi_counter_acounter_cell_a12_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a12_a_aCOUT # !U1_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a13_a,
	cin => U1_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a13_a,
	cout => U1_awysi_counter_acounter_cell_a13_a_aCOUT);

U1_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a14_a $ !U1_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a14_a & !U1_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a14_a,
	cin => U1_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a14_a,
	cout => U1_awysi_counter_acounter_cell_a14_a_aCOUT);

U1_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a15_a $ U1_awysi_counter_acounter_cell_a14_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a14_a_aCOUT # !U1_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a15_a,
	cin => U1_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a15_a,
	cout => U1_awysi_counter_acounter_cell_a15_a_aCOUT);

U1_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a16_a $ !U1_awysi_counter_acounter_cell_a15_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a16_a & !U1_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a16_a,
	cin => U1_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a16_a,
	cout => U1_awysi_counter_acounter_cell_a16_a_aCOUT);

U1_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a17_a $ U1_awysi_counter_acounter_cell_a16_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a16_a_aCOUT # !U1_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a17_a,
	cin => U1_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a17_a,
	cout => U1_awysi_counter_acounter_cell_a17_a_aCOUT);

U1_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a18_a $ !U1_awysi_counter_acounter_cell_a17_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(U1_awysi_counter_asload_path_a18_a & !U1_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a18_a,
	cin => U1_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a18_a,
	cout => U1_awysi_counter_acounter_cell_a18_a_aCOUT);

U1_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a19_a $ U1_awysi_counter_acounter_cell_a18_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U1_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!U1_awysi_counter_acounter_cell_a18_a_aCOUT # !U1_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => U1_awysi_counter_asload_path_a19_a,
	cin => U1_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a19_a,
	cout => U1_awysi_counter_acounter_cell_a19_a_aCOUT);

U1_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- U1_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(rtl_a0) & U1_awysi_counter_asload_path_a20_a $ !U1_awysi_counter_acounter_cell_a19_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => U1_awysi_counter_asload_path_a20_a,
	cin => U1_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => U1_awysi_counter_asload_path_a20_a);

reduce_nor_11_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_11 = TMP_VEC_a0_a & !TMP_VEC_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TMP_VEC_a0_a,
	datad => TMP_VEC_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_11);

U2_adffs_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a15_a = DFFE(U1_awysi_counter_asload_path_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a15_a);

U2_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a14_a = DFFE(U1_awysi_counter_asload_path_a19_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a14_a);

U2_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a13_a = DFFE(U1_awysi_counter_asload_path_a18_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a13_a);

U2_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a12_a = DFFE(U1_awysi_counter_asload_path_a17_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a12_a);

U2_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a11_a = DFFE(U1_awysi_counter_asload_path_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a11_a);

U2_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a10_a = DFFE(U1_awysi_counter_asload_path_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a10_a);

U2_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a9_a = DFFE(U1_awysi_counter_asload_path_a14_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a9_a);

U2_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a8_a = DFFE(U1_awysi_counter_asload_path_a13_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a8_a);

U2_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a7_a = DFFE(U1_awysi_counter_asload_path_a12_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a7_a);

U2_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a6_a = DFFE(U1_awysi_counter_asload_path_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a6_a);

U2_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a5_a = DFFE(U1_awysi_counter_asload_path_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a5_a);

U2_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a4_a = DFFE(U1_awysi_counter_asload_path_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a4_a);

U2_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a3_a = DFFE(U1_awysi_counter_asload_path_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a3_a);

U2_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a2_a = DFFE(U1_awysi_counter_asload_path_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a2_a);

U2_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a1_a = DFFE(U1_awysi_counter_asload_path_a6_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a1_a);

U2_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- U2_adffs_a0_a = DFFE(U1_awysi_counter_asload_path_a5_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_11,
	devclrn => devclrn,
	devpor => devpor,
	regout => U2_adffs_a0_a);

reduce_nor_9_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_9 = !TMP_VEC_a0_a & TMP_VEC_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TMP_VEC_a0_a,
	datad => TMP_VEC_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_9);

U3_adffs_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a15_a = DFFE(U1_awysi_counter_asload_path_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a15_a);

U3_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a14_a = DFFE(U1_awysi_counter_asload_path_a19_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a14_a);

U3_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a13_a = DFFE(U1_awysi_counter_asload_path_a18_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a13_a);

U3_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a12_a = DFFE(U1_awysi_counter_asload_path_a17_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a12_a);

U3_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a11_a = DFFE(U1_awysi_counter_asload_path_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a11_a);

U3_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a10_a = DFFE(U1_awysi_counter_asload_path_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a10_a);

U3_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a9_a = DFFE(U1_awysi_counter_asload_path_a14_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a9_a);

U3_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a8_a = DFFE(U1_awysi_counter_asload_path_a13_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a8_a);

U3_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a7_a = DFFE(U1_awysi_counter_asload_path_a12_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a7_a);

U3_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a6_a = DFFE(U1_awysi_counter_asload_path_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a6_a);

U3_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a5_a = DFFE(U1_awysi_counter_asload_path_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a5_a);

U3_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a4_a = DFFE(U1_awysi_counter_asload_path_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a4_a);

U3_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a3_a = DFFE(U1_awysi_counter_asload_path_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => U1_awysi_counter_asload_path_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a3_a);

U3_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a2_a = DFFE(U1_awysi_counter_asload_path_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a2_a);

U3_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a1_a = DFFE(U1_awysi_counter_asload_path_a6_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a1_a);

U3_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- U3_adffs_a0_a = DFFE(U1_awysi_counter_asload_path_a5_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => U1_awysi_counter_asload_path_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_9,
	devclrn => devclrn,
	devpor => devpor,
	regout => U3_adffs_a0_a);

TMP_LO_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(15));

TMP_LO_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(14));

TMP_LO_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(13));

TMP_LO_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(12));

TMP_LO_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(11));

TMP_LO_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(10));

TMP_LO_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(9));

TMP_LO_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(8));

TMP_LO_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(7));

TMP_LO_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(6));

TMP_LO_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(5));

TMP_LO_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(4));

TMP_LO_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(3));

TMP_LO_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(2));

TMP_LO_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(1));

TMP_LO_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U2_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_LO(0));

TMP_HI_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(15));

TMP_HI_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(14));

TMP_HI_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(13));

TMP_HI_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(12));

TMP_HI_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(11));

TMP_HI_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(10));

TMP_HI_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(9));

TMP_HI_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(8));

TMP_HI_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(7));

TMP_HI_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(6));

TMP_HI_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(5));

TMP_HI_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(4));

TMP_HI_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(3));

TMP_HI_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(2));

TMP_HI_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(1));

TMP_HI_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => U3_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_TMP_HI(0));
END structure;


