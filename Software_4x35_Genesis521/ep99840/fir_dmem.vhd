-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_dmem.VHD
--
-------------------------------------------------------------------------------
--
--	DO NOT USE LEONARDO TO SYNTHESISE WITH QUARTUS II 2.0
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the delay lines to be used for the DPP-II. The delay
--		lines are broken down into the individual sections to generate all 8 time
--		constant's FIRs
--
--		It outputs 10 different version of delays from the input data (adata)
--		data1a	1us from adata
--		data1b	1us from data1a
--		data2	2us from data1b
--		data4	4us from data2
--		data8	8us from data4
--		data16	16us from data8
--		data32	32us	from data16
--		data64	64us from data32
--		data128	128us from data128
--
--   History:
-------------------------------------------------------------------------------
-- Total Bits: Width*(17+2)-1
--		Width = 16: Total Width = 303
--		Width = 14: Total Width = 265
-- Total Bits: Width*(19+2)-1
--		Width = 16: Total Width = 303
--		Width = 14: Total Width = 265

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_dmem is 
	port( 
		clk			: in		std_logic;
     	imr            : in      std_logic;                    -- Master Reset
		adata		: in		std_logic_Vector( 14 downto 0 );
		Dout			: out	std_logic_Vector( 134 downto 0 ) );
end fir_dmem;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_dmem is
	constant Width				: integer := 15;

	constant diff0				: integer := 6;		-- bits 3 (0.4us)
	constant diff1				: integer := 14;		-- bits 4 (0.8us)
	constant diff2				: integer := 94;		-- bits 7 (1.6us)
	constant diff3				: integer := 126;		-- bits 7 (6.4us)

	constant Bit0				: integer := 3;
	constant Bit1				: integer := 4;
	constant Bit2				: integer := 7;
	constant bit3				: integer := 7;


	signal radr0				: std_logic_vector( Bit3-1 downto 0 );
	signal radr1 				: std_logic_vector( Bit3-1 downto 0 );
	signal radr2 				: std_logic_vector( Bit3-1 downto 0 );
	signal radr3 				: std_logic_vector( Bit3-1 downto 0 );
	signal wadr				: std_logic_vector( Bit3-1 downto 0 );	-- Used for All
	signal Vcc				: std_logic;

	
	signal dly_data			: std_logic_Vector( 89 downto 0 );
	
begin
	Dout(  14 downto   0 ) 	<= dly_data(  14 downto   0 );		-- (0) A/D Data High
	Dout(  44 downto  15 ) 	<= dly_data(  44 downto  15 );		-- (2) 0.4us

	Dout(  59 downto  45 ) 	<= dly_data(  14 downto   0 );		-- (0) A/D Data Med
	Dout(  89 downto  60 ) 	<= dly_data(  59 downto  30 );		-- (4) (0.8us total )

	Dout( 104 downto  90 ) 	<= dly_data(  14 downto   0 );		-- (0) A/D Data Low
	Dout( 134 downto 105 ) 	<= dly_data(  89 downto  60 );		-- (12) 6.4us overall


	Vcc 		<= '1';
     -------------------------------------------------------------------------------


	wadr_cntr : lpm_counter
		generic map(
			LPM_WIDTH		=> bit3,
			LPM_DIRECTION	=> "UP",
			LPM_TYPE		=> "LPM_COUNTER" )
		port map(
			aclr			=> IMR,
			clock		=> clk,
			q			=> wadr );

	radr0	<= wadr - conv_std_logic_Vector( diff0, bit3 );
	radr1	<= wadr - conv_std_logic_Vector( diff1, bit3 );
	radr2	<= wadr - conv_std_logic_Vector( diff2, bit3 );
	radr3	<= wadr - conv_std_logic_Vector( diff3, bit3 );

     --------------------------------------------------------------------------
	data0_ff : lpm_Ff
		generic map(
			LPM_WIDTH		=> Width,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> imr,
			clock		=> clk,
			data			=> adata,
			q			=> dly_data( Width-1 downto 0 ) );

     --------------------------------------------------------------------------
	-- 200ns Delay Llines (x4) - OK
	-- Index 1,2,3,4
     --------------------------------------------------------------------------

	DLY1a : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Width,			-- Data Width
			LPM_WIDTHAD			=> Bit0,
			LPM_TYPE				=> "LPM_RAM_DP",
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT1.MIF" )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			wraddress				=> wadr ( Bit0-1 downto 0 ),
			rdaddress 			=> radr0( Bit0-1 downto 0 ), 
			data					=> dly_data( 14 downto  0 ),
			q					=> dly_data( 29 downto 15 ) );

	DLY1b : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Width,	-- Data Width
			LPM_WIDTHAD			=> Bit0,
			LPM_TYPE				=> "LPM_RAM_DP",
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT1.MIF" )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			wraddress				=> wadr ( Bit0-1 downto 0 ),
			rdaddress 			=> radr0( Bit0-1 downto 0 ), 
			data					=> dly_data( 29 downto 15 ),
			q					=> dly_data( 44 downto 30 ) );


     --------------------------------------------------------------------------
	-- 400ns Delay Llines (x2) - OK
	-- Index 5,6
     --------------------------------------------------------------------------
	DLY2 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Width,	-- Data Width
			LPM_WIDTHAD			=> Bit1,
			LPM_TYPE				=> "LPM_RAM_DP",
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT2.MIF" )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			wraddress 			=> wadr(  Bit1-1 downto 0 ), 
			rdaddress				=> radr1( bit1-1 downto 0 ),
			data					=> dly_data( 44 downto 30 ),
			q					=> dly_data( 59 downto 45 ) );
     --------------------------------------------------------------------------
	-- 800ns Delay Llines (x2)
     --------------------------------------------------------------------------
	DLY3 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Width,	-- Data Width
			LPM_WIDTHAD			=> Bit2,
			LPM_TYPE				=> "LPM_RAM_DP",
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT3.MIF" )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			wraddress 			=> wadr(  Bit2-1 downto 0 ), 
			rdaddress				=> radr2( Bit2-1 downto 0 ), 
			data					=> dly_data( 59 downto 45 ),
			q					=> dly_data( 74 downto 60 ) );
			
     --------------------------------------------------------------------------
	-- 1.6us Delay Llines (x2)
     --------------------------------------------------------------------------
	DLY4 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Width,	-- Data Width
			LPM_WIDTHAD			=> Bit3,
			LPM_TYPE				=> "LPM_RAM_DP",
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT4.MIF"  )
		PORT map(
			Wren					=> Vcc,
			wrclock				=> clk,
			rdclock 				=> clk,
			wraddress 			=> wadr(  Bit3-1 downto 0 ), 
			rdaddress				=> radr3( Bit3-1 downto 0 ), 
			data					=> dly_data( 74 downto 60 ),
			q					=> dly_data( 89 downto 75 ) );

-------------------------------------------------------------------------------
end behavioral;               -- fir_dmem
-------------------------------------------------------------------------------