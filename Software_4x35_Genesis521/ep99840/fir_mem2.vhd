-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_mem2.VHD
--
-------------------------------------------------------------------------------
--
--	DO NOT USE LEONARDO TO SYNTHESISE WITH QUARTUS II 2.0
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the delay lines to be used for the DPP-II. The delay
--		lines are broken down into the individual sections to generate all 8 time
--		constant's FIRs
--
--		It outputs 10 different version of delays from the input data (adata)
--		data1a	1us from adata
--		data1b	1us from data1a
--		data2	2us from data1b
--		data4	4us from data2
--		data8	8us from data4
--		data16	16us from data8
--		data32	32us	from data16
--		data64	64us from data32
--		data128	128us from data128
--
--   History:
-------------------------------------------------------------------------------
-- Total Bits: Width*(17+2)-1
--		Width = 16: Total Width = 303
--		Width = 14: Total Width = 265
-- Total Bits: Width*(19+2)-1
--		Width = 16: Total Width = 303
--		Width = 14: Total Width = 265

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_mem2 is 
	port( 
		clk			: in		std_logic;
     	imr            : in      std_logic;                    -- Master Reset
		TC_SEL		: in		std_logic_vector(   3 downto 0 );
		adata		: in		std_logic_Vector(  14 downto 0 );
		Peak_time		: out	std_logic_vector(  11 downto 0 );
		Pur_time		: out	std_logic_vector(  11 downto 0 );
		Dout			: out	std_logic_Vector(  74 downto 0 ) );
end fir_mem2;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_mem2 is
	constant Width				: integer := 15;
	-- Diff = PT / 0.2 - 2
	constant diff0				: integer := 0+2;		-- bits 1 (0.2us)
	constant diff1				: integer := 0+6;		-- bits 2 (0.4us)
	constant diff2				: integer := 0+14;		-- bits 3 (0.8us)
	constant diff3				: integer := 0+30;		-- bits 4 (1.6us)
	constant diff4				: integer := 0+62;		-- bits 5 (3.2us)
	constant diff5				: integer := 0+126;		-- bits 6 (6.4us)
	constant diff6				: integer := 0+254;		-- bits 7 (12.8us)
	constant diff7				: integer := 0+510;		-- bits 8 (25.6us)
	constant diff8				: integer := 0+1022;	-- bits 9 (51.2us)

--	constant diff0				: integer := 1+2;		-- bits 1 (0.2us)
--	constant diff1				: integer := 1+6;		-- bits 2 (0.4us)
--	constant diff2				: integer := 1+14;		-- bits 3 (0.8us)
--	constant diff3				: integer := 1+30;		-- bits 4 (1.6us)
--	constant diff4				: integer := 1+62;		-- bits 5 (3.2us)
--	constant diff5				: integer := 1+126;		-- bits 6 (6.4us)
--	constant diff6				: integer := 1+254;		-- bits 7 (12.8us)
--	constant diff7				: integer := 1+510;		-- bits 8 (25.6us)
--	constant diff8				: integer := 1+1022;	-- bits 9 (51.2us)

	constant Peak_Time0 		: std_logic_vector( 11 downto 0 ) := x"008";		--  0.4
	constant Peak_Time1 		: std_logic_vector( 11 downto 0 ) := x"010";		--  0.8
	constant Peak_Time2 		: std_logic_vector( 11 downto 0 ) := x"020";		--  1.6
	constant Peak_Time3 		: std_logic_vector( 11 downto 0 ) := x"040";		--  3.2
	constant Peak_Time4 		: std_logic_vector( 11 downto 0 ) := x"080";		--  6.4
	constant Peak_Time5 		: std_logic_vector( 11 downto 0 ) := x"100";		-- 12.8
	constant Peak_Time6 		: std_logic_vector( 11 downto 0 ) := x"200";		-- 25.6
	constant Peak_Time7 		: std_logic_vector( 11 downto 0 ) := x"400";		-- 51.2
	constant Peak_Time8 		: std_logic_vector( 11 downto 0 ) := x"800";		-- 102.4

--	constant Peak_Time0 		: std_logic_vector( 11 downto 0 ) := x"007";		--  0.4
--	constant Peak_Time1 		: std_logic_vector( 11 downto 0 ) := x"00F";		--  0.8
--	constant Peak_Time2 		: std_logic_vector( 11 downto 0 ) := x"01F";		--  1.6
--	constant Peak_Time3 		: std_logic_vector( 11 downto 0 ) := x"03F";		--  3.2
--	constant Peak_Time4 		: std_logic_vector( 11 downto 0 ) := x"07F";		--  6.4
--	constant Peak_Time5 		: std_logic_vector( 11 downto 0 ) := x"0FF";		-- 12.8
--	constant Peak_Time6 		: std_logic_vector( 11 downto 0 ) := x"1FF";		-- 25.6
--	constant Peak_Time7 		: std_logic_vector( 11 downto 0 ) := x"3FF";		-- 51.2
--	constant Peak_Time8 		: std_logic_vector( 11 downto 0 ) := x"7FF";		-- 102.4


	-- Pur Time = ( PT / 0.05 ) / 4
	constant Pur_Time0 			: std_logic_vector( 11 downto 0 ) := x"002";		--  0.4
	constant Pur_Time1 			: std_logic_vector( 11 downto 0 ) := x"004";		--  0.8
	constant Pur_Time2 			: std_logic_vector( 11 downto 0 ) := x"008";		--  1.6
	constant Pur_Time3 			: std_logic_vector( 11 downto 0 ) := x"010";		--  3.2
	constant Pur_Time4 			: std_logic_vector( 11 downto 0 ) := x"020";		--  6.4
	constant Pur_Time5 			: std_logic_vector( 11 downto 0 ) := x"040";		-- 12.8
	constant Pur_Time6 			: std_logic_vector( 11 downto 0 ) := x"080";		-- 25.6
	constant Pur_Time7 			: std_logic_vector( 11 downto 0 ) := x"100";		-- 51.2
	constant Pur_Time8 			: std_logic_vector( 11 downto 0 ) := x"200";		-- 102.4

	
	signal radr				: std_logic_vector(  9 downto 0 );
	signal wadr 				: std_logic_vector(  9 downto 0 );	-- Used for All
	signal adr_diff			: std_logic_vector(  9 downto 0 );
	signal Vcc				: std_logic;
	signal gnd				: std_logic;
	signal dly_data			: std_logic_vector( 74 downto 0 );
	
begin
	Vcc		<= '1';
	gnd		<= '0';
	Dout		<= dly_data;
	

	with( ConV_Integer( TC_SEL ) ) select
		adr_diff <= 	conv_std_logic_vector( diff0, 10 ) when 0,
					conv_std_logic_vector( diff1, 10 ) when 1,
					conv_std_logic_vector( diff2, 10 ) when 2,
					conv_std_logic_vector( diff3, 10 ) when 3,
					conv_std_logic_vector( diff4, 10 ) when 4,
					conv_std_logic_vector( diff5, 10 ) when 5,
					conv_std_logic_vector( diff6, 10 ) when 6,
					conv_std_logic_vector( diff7, 10 ) when 7,
					conv_std_logic_vector( diff8, 10 ) when others;

     --------------------------------------------------------------------------
	-- Delay Line Memory Address Counters
	wadr_cntr : lpm_counter
		generic map(
			LPM_WIDTH				=> 10,
			LPM_DIRECTION			=> "UP",
			LPM_TYPE				=> "LPM_COUNTER" )
		port map(	
			aclr					=> IMR,
			clock				=> clk,
			q					=> wadr );

					
	radr_Add_sub : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> 10,
			LPM_REPRESENTATION 		=> "UNSIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			cin					=> Vcc,					-- Carry In
			Add_Sub				=> Gnd,					-- Subtract
			dataa				=> wadr,
			datab				=> adr_diff,
			result				=> radr );
	-- Delay Line Memory Address Counters
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- First Data element (0 Delay ) -----------------------------------------
	data0_ff : lpm_Ff
		generic map(
			LPM_WIDTH				=> Width,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> adata,
			q					=> dly_data( Width-1 downto 0 ) );
	-- First Data element (0 Delay ) -----------------------------------------

		
     --------------------------------------------------------------------------
	-- 200ns Delay Llines (x4) - OK
	-- Index 1,2,3,4
     --------------------------------------------------------------------------

	DLY_GEN1 : for i in 0 to 3 generate
		DLY1 : lpm_ram_dp
			generic map(
				LPM_WIDTH				=> Width,	-- Data Width
				LPM_WIDTHAD			=> 10,
				LPM_TYPE				=> "LPM_RAM_DP",
				LPM_INDATA			=> "REGISTERED",
				LPM_OUTDATA			=> "REGISTERED",
				LPM_RDADDRESS_CONTROL	=> "REGISTERED",
				LPM_WRADDRESS_CONTROL	=> "REGISTERED",
				LPM_FILE				=> "INIT1.MIF" )
			PORT map(
				Wren					=> Vcc,
				wrclock				=> clk,
				rdclock 				=> clk,
				rdaddress 			=> radr, 
				wraddress				=> wadr,
				data					=> dly_data( (Width*(i+1))-1 downto  Width*(i+0) ),	
				q					=> dly_data( (Width*(i+2))-1 downto  Width*(i+1) )  );
	end generate;
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then 
			Peak_time <= x"000";
			Pur_Time 	<= x"000";
		elsif(( clk'event ) and ( clk = '1' )) then
			case Conv_Integer( TC_SEL ) is
				when 0		=> Peak_time <= Peak_time0;	Pur_time <= Pur_Time0;
				when 1		=> Peak_time <= Peak_time1;	Pur_time <= Pur_Time1;
				when 2		=> Peak_time <= Peak_time2;	Pur_time <= Pur_Time2;
				when 3		=> Peak_time <= Peak_time3;	Pur_time <= Pur_Time3;
				when 4		=> Peak_time <= Peak_time4;	Pur_time <= Pur_Time4;
				when 5		=> Peak_time <= Peak_time5;	Pur_time <= Pur_Time5;
				when 6		=> Peak_time <= Peak_time6;	Pur_time <= Pur_Time6;
				when 7		=> Peak_time <= Peak_time7;	Pur_time <= Pur_Time7;
				when others	=> Peak_time <= Peak_time8;	Pur_time <= Pur_Time8;
			end case;
		end if;
	end process;
	-------------------------------------------------------------------------------
	
-------------------------------------------------------------------------------
end behavioral;               -- fir_mem2
-------------------------------------------------------------------------------