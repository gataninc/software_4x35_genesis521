onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_start
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_cs
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_clk
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_busy
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_dout
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
TreeUpdate [SetDefaultTree]
WaveRestoreZoom {834309 ps} {5368634 ps}
configure wave -namecolwidth 120
configure wave -valuecolwidth 53
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
