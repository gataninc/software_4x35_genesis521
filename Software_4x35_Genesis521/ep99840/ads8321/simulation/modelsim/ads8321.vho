-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:16:02"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	ads8321 IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	AD_Dout : IN std_logic;
	AD_Start : IN std_logic;
	AD_CS : OUT std_logic;
	AD_CLK : OUT std_logic;
	AD_BUSY : OUT std_logic;
	ADATA : OUT std_logic_vector(15 DOWNTO 0)
	);
END ads8321;

ARCHITECTURE structure OF ads8321 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_AD_Dout : std_logic;
SIGNAL ww_AD_Start : std_logic;
SIGNAL ww_AD_CS : std_logic;
SIGNAL ww_AD_CLK : std_logic;
SIGNAL ww_AD_BUSY : std_logic;
SIGNAL ww_ADATA : std_logic_vector(15 DOWNTO 0);
SIGNAL BIT_CNT_a4_a : std_logic;
SIGNAL add_a182 : std_logic;
SIGNAL add_a189 : std_logic;
SIGNAL add_a204 : std_logic;
SIGNAL add_a202 : std_logic;
SIGNAL Sample : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL clk_cnt_a0_a : std_logic;
SIGNAL Equal_a47 : std_logic;
SIGNAL clk_cnt_a1_a : std_logic;
SIGNAL BIT_CNT_a3_a : std_logic;
SIGNAL add_a197 : std_logic;
SIGNAL BIT_CNT_a0_a : std_logic;
SIGNAL add_a199 : std_logic;
SIGNAL add_a192 : std_logic;
SIGNAL BIT_CNT_a1_a : std_logic;
SIGNAL add_a194 : std_logic;
SIGNAL add_a187 : std_logic;
SIGNAL BIT_CNT_a2_a : std_logic;
SIGNAL AD_CS_a76 : std_logic;
SIGNAL AD_CS_a77 : std_logic;
SIGNAL Data_a138 : std_logic;
SIGNAL AD_Dout_acombout : std_logic;
SIGNAL Data : std_logic;
SIGNAL AD_CLK_PRoc_a0 : std_logic;
SIGNAL AD_Start_acombout : std_logic;
SIGNAL AD_CS_areg0 : std_logic;
SIGNAL AD_CLK_areg0 : std_logic;
SIGNAL iADATA_a0_a : std_logic;
SIGNAL iADATA_a1_a : std_logic;
SIGNAL adata_ff_adffs_a0_a : std_logic;
SIGNAL iADATA_a2_a : std_logic;
SIGNAL adata_ff_adffs_a1_a : std_logic;
SIGNAL iADATA_a3_a : std_logic;
SIGNAL adata_ff_adffs_a2_a : std_logic;
SIGNAL iADATA_a4_a : std_logic;
SIGNAL adata_ff_adffs_a3_a : std_logic;
SIGNAL iADATA_a5_a : std_logic;
SIGNAL adata_ff_adffs_a4_a : std_logic;
SIGNAL iADATA_a6_a : std_logic;
SIGNAL adata_ff_adffs_a5_a : std_logic;
SIGNAL iADATA_a7_a : std_logic;
SIGNAL adata_ff_adffs_a6_a : std_logic;
SIGNAL iADATA_a8_a : std_logic;
SIGNAL adata_ff_adffs_a7_a : std_logic;
SIGNAL iADATA_a9_a : std_logic;
SIGNAL adata_ff_adffs_a8_a : std_logic;
SIGNAL iADATA_a10_a : std_logic;
SIGNAL adata_ff_adffs_a9_a : std_logic;
SIGNAL iADATA_a11_a : std_logic;
SIGNAL adata_ff_adffs_a10_a : std_logic;
SIGNAL iADATA_a12_a : std_logic;
SIGNAL adata_ff_adffs_a11_a : std_logic;
SIGNAL iADATA_a13_a : std_logic;
SIGNAL adata_ff_adffs_a12_a : std_logic;
SIGNAL iADATA_a14_a : std_logic;
SIGNAL adata_ff_adffs_a13_a : std_logic;
SIGNAL iADATA_a15_a : std_logic;
SIGNAL adata_ff_adffs_a14_a : std_logic;
SIGNAL iADATA_a16_a : std_logic;
SIGNAL adata_ff_adffs_a15_a : std_logic;
SIGNAL ALT_INV_AD_CS_areg0 : std_logic;
SIGNAL ALT_INV_AD_CLK_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_AD_Dout <= AD_Dout;
ww_AD_Start <= AD_Start;
AD_CS <= ww_AD_CS;
AD_CLK <= ww_AD_CLK;
AD_BUSY <= ww_AD_BUSY;
ADATA <= ww_ADATA;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_AD_CS_areg0 <= NOT AD_CS_areg0;
ALT_INV_AD_CLK_areg0 <= NOT AD_CLK_areg0;

BIT_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a4_a = DFFE(!add_a182 & (Data), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Equal_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a182,
	datac => Data,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Equal_a47,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a4_a);

add_a182_I : apex20ke_lcell
-- Equation(s):
-- add_a182 = add_a204 $ !BIT_CNT_a4_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => BIT_CNT_a4_a,
	cin => add_a204,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a182);

add_a187_I : apex20ke_lcell
-- Equation(s):
-- add_a187 = BIT_CNT_a2_a $ (add_a194)
-- add_a189 = CARRY(BIT_CNT_a2_a # !add_a194)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AAF",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a2_a,
	cin => add_a194,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a187,
	cout => add_a189);

add_a202_I : apex20ke_lcell
-- Equation(s):
-- add_a202 = BIT_CNT_a3_a $ !add_a189
-- add_a204 = CARRY(!BIT_CNT_a3_a & !add_a189)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a3_a,
	cin => add_a189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a202,
	cout => add_a204);

Sample_aI : apex20ke_lcell
-- Equation(s):
-- Sample = DFFE(AD_Start_acombout # Sample & (AD_Dout_acombout # !Equal_a47), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC4",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a47,
	datab => Sample,
	datac => AD_Dout_acombout,
	datad => AD_Start_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Sample);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

clk_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- clk_cnt_a0_a = DFFE(AD_CS_areg0 & !clk_cnt_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => AD_CS_areg0,
	datad => clk_cnt_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => clk_cnt_a0_a);

Equal_a47_I : apex20ke_lcell
-- Equation(s):
-- Equal_a47 = clk_cnt_a1_a & (clk_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clk_cnt_a1_a,
	datac => clk_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a47);

clk_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- clk_cnt_a1_a = DFFE(AD_CS_areg0 & (clk_cnt_a0_a $ clk_cnt_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => clk_cnt_a0_a,
	datac => AD_CS_areg0,
	datad => clk_cnt_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => clk_cnt_a1_a);

BIT_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a3_a = DFFE(add_a202 & (Data), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Equal_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a202,
	datac => Data,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Equal_a47,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a3_a);

add_a197_I : apex20ke_lcell
-- Equation(s):
-- add_a197 = !BIT_CNT_a0_a
-- add_a199 = CARRY(BIT_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a197,
	cout => add_a199);

BIT_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a0_a = DFFE(add_a197 & Data, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Equal_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add_a197,
	datad => Data,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Equal_a47,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a0_a);

add_a192_I : apex20ke_lcell
-- Equation(s):
-- add_a192 = BIT_CNT_a1_a $ !add_a199
-- add_a194 = CARRY(!BIT_CNT_a1_a & !add_a199)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C303",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BIT_CNT_a1_a,
	cin => add_a199,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a192,
	cout => add_a194);

BIT_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a1_a = DFFE(Data & add_a192, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Equal_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Data,
	datad => add_a192,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Equal_a47,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a1_a);

BIT_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BIT_CNT_a2_a = DFFE(Data & add_a187, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Equal_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Data,
	datad => add_a187,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Equal_a47,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_CNT_a2_a);

AD_CS_a76_I : apex20ke_lcell
-- Equation(s):
-- AD_CS_a76 = !BIT_CNT_a2_a & !BIT_CNT_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BIT_CNT_a2_a,
	datad => BIT_CNT_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AD_CS_a76);

AD_CS_a77_I : apex20ke_lcell
-- Equation(s):
-- AD_CS_a77 = BIT_CNT_a4_a & !BIT_CNT_a3_a & !BIT_CNT_a0_a & AD_CS_a76

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_CNT_a4_a,
	datab => BIT_CNT_a3_a,
	datac => BIT_CNT_a0_a,
	datad => AD_CS_a76,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AD_CS_a77);

Data_a138_I : apex20ke_lcell
-- Equation(s):
-- Data_a138 = Data & (!AD_CS_a77 # !clk_cnt_a1_a # !clk_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4CCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clk_cnt_a0_a,
	datab => Data,
	datac => clk_cnt_a1_a,
	datad => AD_CS_a77,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Data_a138);

AD_Dout_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_Dout,
	combout => AD_Dout_acombout);

Data_aI : apex20ke_lcell
-- Equation(s):
-- Data = DFFE(Data_a138 # Sample & Equal_a47 & !AD_Dout_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sample,
	datab => Equal_a47,
	datac => Data_a138,
	datad => AD_Dout_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Data);

AD_CLK_PRoc_a0_I : apex20ke_lcell
-- Equation(s):
-- AD_CLK_PRoc_a0 = clk_cnt_a0_a & Data & clk_cnt_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => clk_cnt_a0_a,
	datac => Data,
	datad => clk_cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => AD_CLK_PRoc_a0);

AD_Start_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_Start,
	combout => AD_Start_acombout);

AD_CS_areg0_I : apex20ke_lcell
-- Equation(s):
-- AD_CS_areg0 = DFFE(AD_Start_acombout # AD_CS_areg0 & (!AD_CS_a77 # !AD_CLK_PRoc_a0), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF2A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => AD_CS_areg0,
	datab => AD_CLK_PRoc_a0,
	datac => AD_CS_a77,
	datad => AD_Start_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AD_CS_areg0);

AD_CLK_areg0_I : apex20ke_lcell
-- Equation(s):
-- AD_CLK_areg0 = DFFE(AD_CS_areg0 & (clk_cnt_a0_a $ !clk_cnt_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A500",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clk_cnt_a0_a,
	datac => clk_cnt_a1_a,
	datad => AD_CS_areg0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AD_CLK_areg0);

iADATA_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a0_a = DFFE(AD_Dout_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => AD_Dout_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a0_a);

iADATA_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a1_a = DFFE(iADATA_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a1_a);

adata_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a0_a = DFFE(iADATA_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a0_a);

iADATA_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a2_a = DFFE(iADATA_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a2_a);

adata_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a1_a = DFFE(iADATA_a2_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a1_a);

iADATA_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a3_a = DFFE(iADATA_a2_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a3_a);

adata_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a2_a = DFFE(iADATA_a3_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a2_a);

iADATA_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a4_a = DFFE(iADATA_a3_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a4_a);

adata_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a3_a = DFFE(iADATA_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a3_a);

iADATA_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a5_a = DFFE(iADATA_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a5_a);

adata_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a4_a = DFFE(iADATA_a5_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a4_a);

iADATA_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a6_a = DFFE(iADATA_a5_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a6_a);

adata_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a5_a = DFFE(iADATA_a6_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a5_a);

iADATA_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a7_a = DFFE(iADATA_a6_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a7_a);

adata_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a6_a = DFFE(iADATA_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a6_a);

iADATA_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a8_a = DFFE(iADATA_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a8_a);

adata_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a7_a = DFFE(iADATA_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a7_a);

iADATA_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a9_a = DFFE(iADATA_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a9_a);

adata_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a8_a = DFFE(iADATA_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a8_a);

iADATA_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a10_a = DFFE(iADATA_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a10_a);

adata_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a9_a = DFFE(iADATA_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a9_a);

iADATA_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a11_a = DFFE(iADATA_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a11_a);

adata_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a10_a = DFFE(iADATA_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a10_a);

iADATA_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a12_a = DFFE(iADATA_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a12_a);

adata_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a11_a = DFFE(iADATA_a12_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a11_a);

iADATA_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a13_a = DFFE(iADATA_a12_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a13_a);

adata_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a12_a = DFFE(iADATA_a13_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iADATA_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a12_a);

iADATA_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a14_a = DFFE(iADATA_a13_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a14_a);

adata_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a13_a = DFFE(iADATA_a14_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a13_a);

iADATA_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a15_a = DFFE(iADATA_a14_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a15_a);

adata_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a14_a = DFFE(iADATA_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a14_a);

iADATA_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- iADATA_a16_a = DFFE(iADATA_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , AD_CLK_PRoc_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => AD_CLK_PRoc_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iADATA_a16_a);

adata_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_ff_adffs_a15_a = DFFE(!iADATA_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !AD_CS_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => iADATA_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_ff_adffs_a15_a);

AD_CS_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AD_CS);

AD_CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_AD_CLK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AD_CLK);

AD_BUSY_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => AD_CS_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AD_BUSY);

ADATA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(0));

ADATA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(1));

ADATA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(2));

ADATA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(3));

ADATA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(4));

ADATA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(5));

ADATA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(6));

ADATA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(7));

ADATA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(8));

ADATA_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(9));

ADATA_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(10));

ADATA_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(11));

ADATA_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(12));

ADATA_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(13));

ADATA_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(14));

ADATA_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => adata_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ADATA(15));
END structure;


