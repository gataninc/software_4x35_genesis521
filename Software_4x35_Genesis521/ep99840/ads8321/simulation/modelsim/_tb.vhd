---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ads8321.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--   History:       <date> - <Author>
--		February 8, 2001 - MCS
--			Timing change clock to 20Mhz from 10Mhz
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal IMR		: std_logic := '1';
	signal clK20		: std_logic := '0';
	signal AD_Dout		: std_logic;
	signal AD_Start 	: std_logic := '0';
	signal AD_CS		: std_logic;
	signal AD_CLK		: std_logic;	
	signal AD_BUSY		: std_logic;
	signal ADATA		: std_logic_Vector( 15 downto 0 );
---------------------------------------------------------------------------------------------------
	component ads8321  
		port( 
			IMR		: in		std_Logic;	-- Master Clock
			CLK20	: in		std_logic;
			AD_Dout	: in		std_logic;	-- A/D Data Output
			AD_Start	: in		std_logic;
			AD_CS	: out	std_logic;	-- A/D Chip Select
			AD_CLK	: out	std_logic;	-- A/D Clock ( 2Mhz )
			AD_BUSY	: out	std_Logic;
			ADATA	: out	std_logic_Vector( 15 downto 0 ) );
	end component ads8321;
	---------------------------------------------------------------------------------------------------

begin
     ----------------------------------------------------------------------------------------------
	U : ads8321
		port map(
			IMR		=> IMR,
			CLK20	=> CLK20,
			AD_Dout	=> AD_Dout,
			AD_Start	=> AD_Start,
			ad_cs	=> ad_cs,
			ad_clk	=> ad_clk,
			ad_busy	=> ad_busy,
			adata	=> adata );

	imr	<= '0' after 350 ns;
	CLK20 <= not( CLK20 ) after 25 ns;

	
	CLK_PROC : Process
	begin
		AD_Start <= '0';
		wait for 1 us;
		wait until(( CLK20'Event ) and ( CLK20 = '1' ));
		AD_Start <= '1';
		wait until(( CLK20'Event ) and ( CLK20 = '1' ));
		AD_Start <= '0';
		wait ;
	end process;
	
	AD_PROC : process
	begin
		AD_DOut <= 'H';
		wait until (( AD_CS'Event ) and ( AD_CS = '0' ));
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		AD_DOut <= '0';
--		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		for i in 0 to 13 loop
			wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
			AD_DOut <= '1';
		end loop;
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		AD_DOut <= '0';
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		AD_DOut <= '0';
		wait until (( AD_CLK'Event ) and ( AD_CLK = '0' ));
		AD_DOut <= 'H';
		waIT ;
	END PROCESS;
		
---------------------------------------------------------------------------------------------------
end test_bench;			-- ads8321.VHD
---------------------------------------------------------------------------------------------------

