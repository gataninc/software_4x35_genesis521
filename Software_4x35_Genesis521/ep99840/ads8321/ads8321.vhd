---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ads8321.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--   History:       <date> - <Author>
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--			Fits better not using Leonardo in this version
--		February 8, 2001 - MCS
--			Timing change clock to 20Mhz from 10Mhz
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

---------------------------------------------------------------------------------------------------
entity ads8321 is 
	port( 
		IMR		: in		std_Logic;	-- Master Clock
		CLK20	: in		std_logic;
		AD_Dout	: in		std_logic;	-- A/D Data Output
		AD_Start	: in		std_logic;
		AD_CS	: buffer	std_logic;	-- A/D Chip Select
		AD_CLK	: buffer	std_logic;	-- A/D Clock ( 2Mhz )
		AD_BUSY	: buffer	std_Logic;
		ADATA	: buffer	std_logic_Vector( 15 downto 0 ) );
end ads8321;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of ads8321 is

	constant BIT_CNT_MAX		: integer := 16;

	signal iADATA				: std_logic_Vector( 16 downto 0 );
	signal BIT_CNT 			: integer range 0 to BIT_CNT_MAX;
	signal Sample				: std_Logic;
	signal Data				: std_logic;

	signal iadata_vec			: std_logic_vector( 15 downto 0 );
	signal clk_cnt				: std_logic_vector( 1 downto 0 );
	signal clk_cnt_tc			: std_logic;
	
begin
				
	clk_Cnt_tc	<= '1' when ( clk_cnt = "11" ) else '0';
	
	iadata_vec	<= not( iADATA(16)) & iADATA( 15 downto 1 );
	AD_BUSY		<= not( AD_CS );
	
	adata_ff : lpm_ff
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> AD_CS,
			data			=> iadata_vec,
			q			=> adata );

     ----------------------------------------------------------------------------------------------
	AD_CLK_PRoc : process( CLK20, IMR )
	begin
		if( IMR = '1' ) then
			AD_CS	<= '1';
			AD_CLK	<= '1';
			clk_cnt	<= "00";
			Sample	<= '0';
			data		<= '0';
			BIT_CNT	<= bit_cnt_max;
			iADATA	<= '0' & X"0000";
			
		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then	
			-- Start signal from the DSP activates immediately and asserts the A/D busy
			if( AD_Start = '1' )
				then AD_CS <= '0';
			elsif(( clk_cnt_tc = '1' ) and ( data = '1' ) and ( bit_cnt = 0 ))
				then AD_CS <= '1';
			end if;
			
			-- when A/D is not busy, force clk_cnt inactive, otherwise always incremnet 
			if( AD_CS = '1' )
				then CLK_CNT <= "00";
				else CLK_CNT <= CLK_CNT + "01";
			end if;
			
			-- Decode of the states when the AD_CLK is low, otherwise keep it high
			if(( AD_CS = '0' ) and (( CLK_CNT = "11" ) or ( CLK_CNT = "00" )))
				then AD_CLK <= '0';
				else AD_CLK <= '1';
			end if;
			
			-- From the Start, as soon as the CS goes avtive the A/D starts sampling the analog signal.
			-- The sampling state remains until the A/D outputs a logic low
			if( AD_Start = '1' ) 
				then Sample <= '1';
			elsif(( CLK_CNT_TC = '1' ) and ( AD_Dout = '0' ))
				then Sample <= '0';
			end if;

			-- Once the A/D outputs a logic low during the sampling phase, the next phase is the data phase
			-- where 16 bits are shifted out
			if(( clk_cnt_tc = '1' ) and ( Sample = '1' ) and ( AD_Dout = '0' ))
				then data <= '1';
			elsif(( clk_cnt_tc = '1' ) and ( Bit_Cnt = 0 ))
				then data <= '0';
			end if;
				
			-- keep track of how many bits are shifted out
			if(( clk_cnt_tc = '1' ) and ( data = '0' ))
				then bit_cnt <= bit_cnt_max;
			elsif(( clk_Cnt_tc = '1' ) and ( data = '1' ))
				then bit_cnt <= bit_cnt - 1;
			end if;
			
			-- add the bit to the lsb of the "barrel" shift register
			if(( clk_Cnt_tc = '1' ) and ( data = '1' )) 
				then iadata <= iadata( 15 downto 0 ) & AD_Dout;
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;			-- ads8321.VHD
---------------------------------------------------------------------------------------------------

