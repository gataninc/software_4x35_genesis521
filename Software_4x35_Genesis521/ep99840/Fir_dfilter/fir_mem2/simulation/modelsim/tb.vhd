	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal tc_sel		: std_logic_vector( 3 downto 0 );
	signal adata		: std_logic_Vector( 15 downto 0 );
	signal peak_time	: std_logic_Vector( 11 downto 0 );
	signal pur_time	: std_logic_Vector( 11 downto 0 );	
	signal dout		: std_logic_vector( 74 downto 0 );
	
	signal dout0		: std_logic_Vector( 14 downto 0 );
	signal dout1		: std_logic_Vector( 14 downto 0 );
	signal dout2		: std_logic_Vector( 14 downto 0 );
	signal dout3		: std_logic_Vector( 14 downto 0 );
	signal dout4		: std_logic_Vector( 14 downto 0 );

	-------------------------------------------------------------------------------
	component fir_mem2 is 
		port( 
			clk			: in		std_logic;
	     	imr            : in      std_logic;                    -- Master Reset
			TC_SEL		: in		std_logic_vector(   3 downto 0 );
			adata		: in		std_logic_Vector(  14 downto 0 );
			Peak_time		: out	std_logic_vector(  11 downto 0 );
			Pur_time		: out	std_logic_vector(  11 downto 0 );
			Dout			: out	std_logic_Vector(  74 downto 0 ) );
	end component fir_mem2;
	-------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk		<= not( clk ) after 25 ns;
	
	adata_proc : process
	begin
		adata	<= x"0000";
		tc_sel	<= x"0";
		forever_loop : loop
			wait for 1 us;
			adata <= adata + "0010";
			wait until (( Dout4'event ) and ( Dout4 = adata ));
			wait for 2 us;
			tc_sel <= tc_sel + x"1";
		end loop;
	end process;
			
		
		-------------------------------------------------------------------------------
	U : fir_mem2
		port map( 
			clk			=> clk,
	     	imr            => imr,
			TC_SEL		=> tc_sel,
			adata		=> adata( 14 downto 0 ),
			Peak_time		=> Peak_Time,
			Pur_time		=> Pur_time,
			Dout			=> Dout );
	-------------------------------------------------------------------------------
	clk_proc : process( clk )
	begin
		if(( clk'event ) and ( clk = '1' )) then
			dout0	<= dout(  14 downto  0 );
			dout1	<= dout(  29 downto 15 );
			dout2	<= dout(  44 downto 30 );
			dout3	<= dout(  59 downto 45 );
			dout4	<= dout(  74 downto 60 );
		end if;
	end process;
-------------------------------------------------------------------------------
end test_bench;               -- fir_mem2
-------------------------------------------------------------------------------