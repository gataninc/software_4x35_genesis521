-- Copyright (C) 1991-2004 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 4.0 Build 190 1/28/2004 SJ Full Version"

-- DATE "02/25/2004 17:16:04"

-- 
-- Device: Altera EP20K160EQC208-1 Package PQFP208
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_mem2 IS
    PORT (
	TC_SEL : IN std_logic_vector(3 DOWNTO 0);
	clk : IN std_logic;
	imr : IN std_logic;
	adata : IN std_logic_vector(14 DOWNTO 0);
	Peak_time : OUT std_logic_vector(11 DOWNTO 0);
	Pur_time : OUT std_logic_vector(11 DOWNTO 0);
	Dout : OUT std_logic_vector(74 DOWNTO 0)
	);
END fir_mem2;

ARCHITECTURE structure OF fir_mem2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_TC_SEL : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_clk : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_adata : std_logic_vector(14 DOWNTO 0);
SIGNAL ww_Peak_time : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Pur_time : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Dout : std_logic_vector(74 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL TC_SEL_a3_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL Peak_time_a11_a_areg0 : std_logic;
SIGNAL TC_SEL_a2_a_acombout : std_logic;
SIGNAL TC_SEL_a0_a_acombout : std_logic;
SIGNAL TC_SEL_a1_a_acombout : std_logic;
SIGNAL Peak_time_a10_a_areg0 : std_logic;
SIGNAL Peak_time_a9_a_areg0 : std_logic;
SIGNAL Peak_time_a8_a_areg0 : std_logic;
SIGNAL Peak_time_a7_a_areg0 : std_logic;
SIGNAL i_a106 : std_logic;
SIGNAL Peak_time_a6_a_areg0 : std_logic;
SIGNAL Peak_time_a5_a_areg0 : std_logic;
SIGNAL i_a107 : std_logic;
SIGNAL Peak_time_a4_a_areg0 : std_logic;
SIGNAL adr_diff_a2_a_a8 : std_logic;
SIGNAL Peak_time_a3_a_areg0 : std_logic;
SIGNAL adata_a14_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a14_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 : std_logic;
SIGNAL adr_diff_a4_a_a13 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 : std_logic;
SIGNAL adr_diff_a6_a_a15 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 : std_logic;
SIGNAL adr_diff_a7_a_a27 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 : std_logic;
SIGNAL adr_diff_a8_a_a17 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL radr_Add_sub_aadder_aunreg_res_node_a9_a_a7 : std_logic;
SIGNAL radr_Add_sub_aadder_aunreg_res_node_a9_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a14_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a14_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a14_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a14_a : std_logic;
SIGNAL adata_a13_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a13_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a13_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a13_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a13_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a13_a : std_logic;
SIGNAL adata_a12_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a12_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a12_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a12_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a12_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a12_a : std_logic;
SIGNAL adata_a11_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a11_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a11_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a11_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a11_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a11_a : std_logic;
SIGNAL adata_a10_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a10_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a10_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a10_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a10_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a10_a : std_logic;
SIGNAL adata_a9_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a9_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a9_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a9_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a9_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a9_a : std_logic;
SIGNAL adata_a8_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a8_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a8_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a8_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a8_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a8_a : std_logic;
SIGNAL adata_a7_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a7_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a7_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a7_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a7_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a7_a : std_logic;
SIGNAL adata_a6_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a6_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a6_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a6_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a6_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a6_a : std_logic;
SIGNAL adata_a5_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a5_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a5_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a5_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a5_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a5_a : std_logic;
SIGNAL adata_a4_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a4_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a4_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a4_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a4_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a4_a : std_logic;
SIGNAL adata_a3_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a3_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a3_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a3_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a3_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a3_a : std_logic;
SIGNAL adata_a2_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a2_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a2_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a2_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a2_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a2_a : std_logic;
SIGNAL adata_a1_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a1_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a1_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a1_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a1_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a1_a : std_logic;
SIGNAL adata_a0_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a0_a : std_logic;
SIGNAL DLY_GEN1_0_DLY1_asram_aq_a0_a : std_logic;
SIGNAL DLY_GEN1_1_DLY1_asram_aq_a0_a : std_logic;
SIGNAL DLY_GEN1_2_DLY1_asram_aq_a0_a : std_logic;
SIGNAL DLY_GEN1_3_DLY1_asram_aq_a0_a : std_logic;

BEGIN

ww_TC_SEL <= TC_SEL;
ww_clk <= clk;
ww_imr <= imr;
ww_adata <= adata;
Peak_time <= ww_Peak_time;
Pur_time <= ww_Pur_time;
Dout <= ww_Dout;

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a9_a & wadr_cntr_awysi_counter_asload_path_a8_a & wadr_cntr_awysi_counter_asload_path_a7_a & 
wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a
& wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 & 
DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 & DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

TC_SEL_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TC_SEL(3),
	combout => TC_SEL_a3_a_acombout);

clk_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

Peak_time_a11_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a11_a_areg0 = DFFE(TC_SEL_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => TC_SEL_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a11_a_areg0);

TC_SEL_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TC_SEL(2),
	combout => TC_SEL_a2_a_acombout);

TC_SEL_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TC_SEL(0),
	combout => TC_SEL_a0_a_acombout);

TC_SEL_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TC_SEL(1),
	combout => TC_SEL_a1_a_acombout);

Peak_time_a10_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a10_a_areg0 = DFFE(TC_SEL_a2_a_acombout & !TC_SEL_a3_a_acombout & TC_SEL_a0_a_acombout & TC_SEL_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a2_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a10_a_areg0);

Peak_time_a9_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a9_a_areg0 = DFFE(TC_SEL_a1_a_acombout & !TC_SEL_a3_a_acombout & !TC_SEL_a0_a_acombout & TC_SEL_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a1_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a9_a_areg0);

Peak_time_a8_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a8_a_areg0 = DFFE(!TC_SEL_a1_a_acombout & !TC_SEL_a3_a_acombout & TC_SEL_a0_a_acombout & TC_SEL_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a1_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a8_a_areg0);

Peak_time_a7_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a7_a_areg0 = DFFE(TC_SEL_a2_a_acombout & !TC_SEL_a3_a_acombout & !TC_SEL_a0_a_acombout & !TC_SEL_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a2_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a7_a_areg0);

i_a106_I : apex20ke_lcell 
-- Equation(s):
-- i_a106 = !TC_SEL_a3_a_acombout & !TC_SEL_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TC_SEL_a3_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a106);

Peak_time_a6_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a6_a_areg0 = DFFE(TC_SEL_a1_a_acombout & TC_SEL_a0_a_acombout & i_a106, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => TC_SEL_a1_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => i_a106,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a6_a_areg0);

Peak_time_a5_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a5_a_areg0 = DFFE(TC_SEL_a1_a_acombout & !TC_SEL_a0_a_acombout & i_a106, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => TC_SEL_a1_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => i_a106,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a5_a_areg0);

i_a107_I : apex20ke_lcell 
-- Equation(s):
-- i_a107 = !TC_SEL_a1_a_acombout & i_a106

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => TC_SEL_a1_a_acombout,
	datad => i_a106,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a107);

Peak_time_a4_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a4_a_areg0 = DFFE(TC_SEL_a0_a_acombout & i_a107, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AA00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a0_a_acombout,
	datad => i_a107,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a4_a_areg0);

adr_diff_a2_a_a8_I : apex20ke_lcell 
-- Equation(s):
-- adr_diff_a2_a_a8 = !TC_SEL_a0_a_acombout & i_a107

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a0_a_acombout,
	datad => i_a107,
	devclrn => devclrn,
	devpor => devpor,
	combout => adr_diff_a2_a_a8);

Peak_time_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- Peak_time_a3_a_areg0 = DFFE(adr_diff_a2_a_a8, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adr_diff_a2_a_a8,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Peak_time_a3_a_areg0);

adata_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(14),
	combout => adata_a14_a_acombout);

data0_ff_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a14_a = DFFE(adata_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a14_a);

wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(wadr_cntr_awysi_counter_asload_path_a1_a $ wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(wadr_cntr_awysi_counter_asload_path_a2_a $ !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a2_a & !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(wadr_cntr_awysi_counter_asload_path_a3_a $ wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a3_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a4_a = DFFE(wadr_cntr_awysi_counter_asload_path_a4_a $ !wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a4_a & !wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a4_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a5_a = DFFE(wadr_cntr_awysi_counter_asload_path_a5_a $ wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a5_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a6_a = DFFE(wadr_cntr_awysi_counter_asload_path_a6_a $ !wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a6_a & !wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a6_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a7_a = DFFE(wadr_cntr_awysi_counter_asload_path_a7_a $ wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a7_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a7_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a8_a = DFFE(wadr_cntr_awysi_counter_asload_path_a8_a $ !wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a8_a & !wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a8_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a8_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a9_a = DFFE(wadr_cntr_awysi_counter_asload_path_a9_a $ wadr_cntr_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a9_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a9_a);

radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a = wadr_cntr_awysi_counter_asload_path_a1_a
-- radr_Add_sub_aadder_aresult_node_acout_a1_a = CARRY(wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "CCCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a1_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a0_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a0 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a0);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a = adr_diff_a2_a_a8 $ wadr_cntr_awysi_counter_asload_path_a2_a $ !radr_Add_sub_aadder_aresult_node_acout_a1_a
-- radr_Add_sub_aadder_aresult_node_acout_a2_a = CARRY(adr_diff_a2_a_a8 & !wadr_cntr_awysi_counter_asload_path_a2_a & !radr_Add_sub_aadder_aresult_node_acout_a1_a # !adr_diff_a2_a_a8 & (!radr_Add_sub_aadder_aresult_node_acout_a1_a # !wadr_cntr_awysi_counter_asload_path_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6917",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => adr_diff_a2_a_a8,
	datab => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a2_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a1_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a1 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a1);

radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a = i_a107 $ wadr_cntr_awysi_counter_asload_path_a3_a $ radr_Add_sub_aadder_aresult_node_acout_a2_a
-- radr_Add_sub_aadder_aresult_node_acout_a3_a = CARRY(i_a107 & (wadr_cntr_awysi_counter_asload_path_a3_a # !radr_Add_sub_aadder_aresult_node_acout_a2_a) # !i_a107 & wadr_cntr_awysi_counter_asload_path_a3_a & !radr_Add_sub_aadder_aresult_node_acout_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "968E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a107,
	datab => wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a3_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a2_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a2 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a2);

adr_diff_a4_a_a13_I : apex20ke_lcell 
-- Equation(s):
-- adr_diff_a4_a_a13 = TC_SEL_a3_a_acombout # TC_SEL_a2_a_acombout # TC_SEL_a1_a_acombout & TC_SEL_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a1_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => adr_diff_a4_a_a13);

radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a = adr_diff_a4_a_a13 $ wadr_cntr_awysi_counter_asload_path_a4_a $ radr_Add_sub_aadder_aresult_node_acout_a3_a
-- radr_Add_sub_aadder_aresult_node_acout_a4_a = CARRY(adr_diff_a4_a_a13 & (!radr_Add_sub_aadder_aresult_node_acout_a3_a # !wadr_cntr_awysi_counter_asload_path_a4_a) # !adr_diff_a4_a_a13 & !wadr_cntr_awysi_counter_asload_path_a4_a & !radr_Add_sub_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => adr_diff_a4_a_a13,
	datab => wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a4_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a3_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a3 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a3);

radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a = i_a106 $ wadr_cntr_awysi_counter_asload_path_a5_a $ radr_Add_sub_aadder_aresult_node_acout_a4_a
-- radr_Add_sub_aadder_aresult_node_acout_a5_a = CARRY(i_a106 & (wadr_cntr_awysi_counter_asload_path_a5_a # !radr_Add_sub_aadder_aresult_node_acout_a4_a) # !i_a106 & wadr_cntr_awysi_counter_asload_path_a5_a & !radr_Add_sub_aadder_aresult_node_acout_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "968E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a106,
	datab => wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a5_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a4_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a4 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a4);

adr_diff_a6_a_a15_I : apex20ke_lcell 
-- Equation(s):
-- adr_diff_a6_a_a15 = TC_SEL_a3_a_acombout # TC_SEL_a2_a_acombout & (TC_SEL_a0_a_acombout # TC_SEL_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCEC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a0_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a2_a_acombout,
	datad => TC_SEL_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => adr_diff_a6_a_a15);

radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a = adr_diff_a6_a_a15 $ wadr_cntr_awysi_counter_asload_path_a6_a $ radr_Add_sub_aadder_aresult_node_acout_a5_a
-- radr_Add_sub_aadder_aresult_node_acout_a6_a = CARRY(adr_diff_a6_a_a15 & (!radr_Add_sub_aadder_aresult_node_acout_a5_a # !wadr_cntr_awysi_counter_asload_path_a6_a) # !adr_diff_a6_a_a15 & !wadr_cntr_awysi_counter_asload_path_a6_a & !radr_Add_sub_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => adr_diff_a6_a_a15,
	datab => wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a6_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a5_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a5 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a5);

adr_diff_a7_a_a27_I : apex20ke_lcell 
-- Equation(s):
-- adr_diff_a7_a_a27 = TC_SEL_a3_a_acombout # TC_SEL_a1_a_acombout & TC_SEL_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a1_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => adr_diff_a7_a_a27);

radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a = adr_diff_a7_a_a27 $ wadr_cntr_awysi_counter_asload_path_a7_a $ !radr_Add_sub_aadder_aresult_node_acout_a6_a
-- radr_Add_sub_aadder_aresult_node_acout_a7_a = CARRY(adr_diff_a7_a_a27 & wadr_cntr_awysi_counter_asload_path_a7_a & !radr_Add_sub_aadder_aresult_node_acout_a6_a # !adr_diff_a7_a_a27 & (wadr_cntr_awysi_counter_asload_path_a7_a # !radr_Add_sub_aadder_aresult_node_acout_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => adr_diff_a7_a_a27,
	datab => wadr_cntr_awysi_counter_asload_path_a7_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a7_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a6_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a6 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a6);

adr_diff_a8_a_a17_I : apex20ke_lcell 
-- Equation(s):
-- adr_diff_a8_a_a17 = TC_SEL_a3_a_acombout # TC_SEL_a1_a_acombout & TC_SEL_a0_a_acombout & TC_SEL_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => TC_SEL_a1_a_acombout,
	datab => TC_SEL_a3_a_acombout,
	datac => TC_SEL_a0_a_acombout,
	datad => TC_SEL_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => adr_diff_a8_a_a17);

radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a = adr_diff_a8_a_a17 $ wadr_cntr_awysi_counter_asload_path_a8_a $ radr_Add_sub_aadder_aresult_node_acout_a7_a
-- radr_Add_sub_aadder_aresult_node_acout_a8_a = CARRY(adr_diff_a8_a_a17 & (!radr_Add_sub_aadder_aresult_node_acout_a7_a # !wadr_cntr_awysi_counter_asload_path_a8_a) # !adr_diff_a8_a_a17 & !wadr_cntr_awysi_counter_asload_path_a8_a & !radr_Add_sub_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => adr_diff_a8_a_a17,
	datab => wadr_cntr_awysi_counter_asload_path_a8_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a8_a);

DLY_GEN1_3_DLY1_asram_aq_a14_a_a7_I : apex20ke_lcell 
-- Equation(s):
-- DLY_GEN1_3_DLY1_asram_aq_a14_a_a7 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY_GEN1_3_DLY1_asram_aq_a14_a_a7);

radr_Add_sub_aadder_aunreg_res_node_a9_a_a7_I : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aunreg_res_node_a9_a_a7 = radr_Add_sub_aadder_aresult_node_acout_a8_a $ !wadr_cntr_awysi_counter_asload_path_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a9_a,
	cin => radr_Add_sub_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aunreg_res_node_a9_a_a7);

radr_Add_sub_aadder_aunreg_res_node_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aunreg_res_node_a9_a = TC_SEL_a3_a_acombout $ radr_Add_sub_aadder_aunreg_res_node_a9_a_a7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => TC_SEL_a3_a_acombout,
	datad => radr_Add_sub_aadder_aunreg_res_node_a9_a_a7,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aunreg_res_node_a9_a);

radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51_I : apex20ke_lcell 
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51 = !radr_Add_sub_aadder_aunreg_res_node_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aunreg_res_node_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a9_a_a51);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a14_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a14_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a14_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a14_a);

adata_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(13),
	combout => adata_a13_a_acombout);

data0_ff_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a13_a = DFFE(adata_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a13_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a13_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a13_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a13_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a13_a);

adata_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(12),
	combout => adata_a12_a_acombout);

data0_ff_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a12_a = DFFE(adata_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a12_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a12_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a12_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a12_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a12_a);

adata_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(11),
	combout => adata_a11_a_acombout);

data0_ff_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a11_a = DFFE(adata_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a11_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a11_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a11_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a11_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a11_a);

adata_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(10),
	combout => adata_a10_a_acombout);

data0_ff_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a10_a = DFFE(adata_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a10_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a10_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a10_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a10_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a10_a);

adata_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(9),
	combout => adata_a9_a_acombout);

data0_ff_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a9_a = DFFE(adata_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a9_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a9_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a9_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a9_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a9_a);

adata_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(8),
	combout => adata_a8_a_acombout);

data0_ff_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a8_a = DFFE(adata_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a8_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a8_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a8_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a8_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a8_a);

adata_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(7),
	combout => adata_a7_a_acombout);

data0_ff_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a7_a = DFFE(adata_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a7_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a7_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a7_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a7_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a7_a);

adata_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(6),
	combout => adata_a6_a_acombout);

data0_ff_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a6_a = DFFE(adata_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a6_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a6_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a6_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a6_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a6_a);

adata_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(5),
	combout => adata_a5_a_acombout);

data0_ff_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a5_a = DFFE(adata_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a5_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a5_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a5_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a5_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a5_a);

adata_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(4),
	combout => adata_a4_a_acombout);

data0_ff_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a4_a = DFFE(adata_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a4_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a4_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a4_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a4_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a4_a);

adata_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(3),
	combout => adata_a3_a_acombout);

data0_ff_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a3_a = DFFE(adata_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a3_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a3_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a3_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a3_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a3_a);

adata_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(2),
	combout => adata_a2_a_acombout);

data0_ff_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a2_a = DFFE(adata_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a2_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a2_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a2_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a2_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a2_a);

adata_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(1),
	combout => adata_a1_a_acombout);

data0_ff_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a1_a = DFFE(adata_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a1_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a1_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a1_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a1_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a1_a);

adata_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(0),
	combout => adata_a0_a_acombout);

data0_ff_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a0_a = DFFE(adata_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a0_a);

DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_0_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_0_DLY1_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY_GEN1_0_DLY1_asram_aq_a0_a);

DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_1_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_1_DLY1_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY_GEN1_1_DLY1_asram_aq_a0_a);

DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_2_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_2_DLY1_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY_GEN1_2_DLY1_asram_aq_a0_a);

DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY_GEN1_3_DLY1|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 1024,
	logical_ram_width => 15,
	address_width => 10,
	first_address => 0,
	last_address => 1023,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY_GEN1_3_DLY1_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY_GEN1_3_DLY1_asram_aq_a0_a);

Peak_time_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(11));

Peak_time_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(10));

Peak_time_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(9));

Peak_time_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(8));

Peak_time_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(7));

Peak_time_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(6));

Peak_time_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(5));

Peak_time_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(4));

Peak_time_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(3));

Peak_time_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(2));

Peak_time_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(1));

Peak_time_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_time(0));

Pur_time_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(11));

Pur_time_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(10));

Pur_time_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(9));

Pur_time_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(8));

Pur_time_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(7));

Pur_time_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(6));

Pur_time_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(5));

Pur_time_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(4));

Pur_time_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(3));

Pur_time_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(2));

Pur_time_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Peak_time_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(1));

Pur_time_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_time(0));

Dout_a74_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(74));

Dout_a73_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(73));

Dout_a72_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(72));

Dout_a71_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(71));

Dout_a70_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(70));

Dout_a69_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(69));

Dout_a68_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(68));

Dout_a67_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(67));

Dout_a66_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(66));

Dout_a65_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(65));

Dout_a64_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(64));

Dout_a63_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(63));

Dout_a62_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(62));

Dout_a61_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(61));

Dout_a60_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_3_DLY1_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(60));

Dout_a59_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(59));

Dout_a58_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(58));

Dout_a57_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(57));

Dout_a56_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(56));

Dout_a55_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(55));

Dout_a54_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(54));

Dout_a53_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(53));

Dout_a52_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(52));

Dout_a51_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(51));

Dout_a50_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(50));

Dout_a49_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(49));

Dout_a48_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(48));

Dout_a47_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(47));

Dout_a46_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(46));

Dout_a45_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_2_DLY1_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(45));

Dout_a44_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(44));

Dout_a43_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(43));

Dout_a42_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(42));

Dout_a41_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(41));

Dout_a40_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(40));

Dout_a39_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(39));

Dout_a38_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(38));

Dout_a37_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(37));

Dout_a36_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(36));

Dout_a35_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(35));

Dout_a34_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(34));

Dout_a33_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(33));

Dout_a32_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(32));

Dout_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(31));

Dout_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_1_DLY1_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(30));

Dout_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(29));

Dout_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(28));

Dout_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(27));

Dout_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(26));

Dout_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(25));

Dout_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(24));

Dout_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(23));

Dout_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(22));

Dout_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(21));

Dout_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(20));

Dout_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(19));

Dout_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(18));

Dout_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(17));

Dout_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(16));

Dout_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY_GEN1_0_DLY1_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(15));

Dout_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(14));

Dout_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(13));

Dout_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(12));

Dout_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(11));

Dout_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(10));

Dout_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(9));

Dout_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(8));

Dout_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(7));

Dout_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(6));

Dout_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(5));

Dout_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(4));

Dout_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(3));

Dout_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(2));

Dout_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(1));

Dout_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(0));
END structure;


