onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Literal -radix hexadecimal /tb/tc_sel
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
add wave -noupdate -format Literal -radix hexadecimal /tb/peak_time
add wave -noupdate -format Literal -radix hexadecimal /tb/pur_time
add wave -noupdate -format Literal -radix hexadecimal /tb/dout
add wave -noupdate -format Literal -radix hexadecimal /tb/dout0
add wave -noupdate -format Literal -radix hexadecimal /tb/dout1
add wave -noupdate -format Literal -radix hexadecimal /tb/dout2
add wave -noupdate -format Literal -radix hexadecimal /tb/dout3
add wave -noupdate -format Literal -radix hexadecimal /tb/dout4
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {1070 ns} {1325 ns} {1576 ns} {1822 ns} {2075 ns}
WaveRestoreZoom {619 ns} {2884 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
