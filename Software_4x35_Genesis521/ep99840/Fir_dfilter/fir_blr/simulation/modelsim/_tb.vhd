-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	APP_BLOCK.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is

end tb;

architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal AD_MR		: std_logic := '1';
	signal pbusy		: std_logic;
	signal PDone_In	: std_logic;
	signal Pur_In		: std_logic;
	signal tc_sel		: std_logic_vector(  3 downto 0 );
	signal tlevel		: std_logic_vector( 15 downto 0 );
	signal PS_SEL		: std_logic_vector( 1 downto 0 );
	signal fir_in		: std_logic_Vector( 23 downto 0 );
	signal PDone_Out	: std_logic;
	signal Pur_Out		: std_logic;
	signal pbusy_out	: std_logic;
	signal fir_out		: std_logic_Vector( 15 downto 0 );
	signal blevel_upd	: std_logic;
	signal blevel		: std_logic_Vector( 7 downto 0 );


	-------------------------------------------------------------------------------
	component fir_blr is 
		port(
			imr			: in		std_logic;
			clk			: in		std_logic;
			AD_MR		: in		std_logic;
			pbusy		: in		std_logic;
			PDone_In		: in		std_logic;
			Pur_In		: in		std_logic;
			tc_sel		: in		std_logic_vector(  3 downto 0 );
			tlevel		: in		std_logic_vector( 15 downto 0 );
			PS_SEL		: in		std_logic_vector( 1 downto 0 );
			fir_in		: in		std_logic_Vector( 23 downto 0 );
			PDone_Out		: out	std_logic;
			Pur_Out		: out	std_logic;
			pbusy_out		: out	std_logic;
			fir_out		: out	std_logic_Vector( 15 downto 0 );
			blevel_upd	: out	std_logic;
			blevel		: out	std_logic_Vector( 7 downto 0 ) );
	end component fir_blr;
	-------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;
	AD_MR <= '0' after 2 us;
	clk <= not( clk ) after 25 ns;
	
	U : fir_blr
		port map(
			imr 			=> imr,
			clk			=> clk,
			AD_MR		=> AD_MR,
			pbusy		=> pbusy,
			PDone_In		=> PDone_In,
			Pur_In		=> Pur_In,
			tc_sel		=> Tc_Sel,
			tlevel		=> Tlevel,
			PS_SEL		=> PS_Sel,
			fir_in		=> fir_in,
			PDone_Out		=> PDone_Out,
			Pur_Out		=> Pur_Out,
			pbusy_out		=> PBusy_Out,
			fir_out		=> fir_Out,
			blevel_upd	=> Blevel_Upd,
			blevel		=> Blevel );

------------------------------------------------------------------------------------
end test_bench;               -- Fir_Block
------------------------------------------------------------------------------------