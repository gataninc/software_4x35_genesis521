-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	APP_BLOCK.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
-------------------------------------------------------------------------------
	component fir_blr_acc is 
	port(
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_Logic;					-- Master Clock
			sclr			: in		std_logic;
			Div_Fact		: in		std_logic_Vector( 3 downto 0 );
			DataIn		: in		std_logic_vector( 7 downto 0 );
			DataOut		: out	std_logic_Vector( 7 downto 0 ) );
	end fir_blr_acc;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_blr_acc is
	signal In_Vec 		: std_logic_Vector( 15 downto 0 );
	signal Acc	 	: std_logic_Vector( 15 downto 0 );
	signal Add		: std_logic_Vector( 15 downto 0 );
	signal Add_Mux		: std_logic_Vector( 15 downto 0 );
begin
	------------------------------------------------------------------------------------------
	-- Sample Averaging Circuit
	IN_Vec_Gen : for i in 0 to 7 generate
		in_vec( 8 + i ) <= DataIn(7);
	end generate;
	
	in_vec( 7 downto 0 ) <= DataIn;
	
	blevel_adder : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_DIRECTION			=> "ADD",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			dataa				=> in_vec,
			datab				=> Acc,
			result				=> Add );

	add_mux <= in_vec when ( sclr = '1' ) else Add;

	blevel_acc_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> 16,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> IMR,
			clock				=> CLK,
			data					=> add_mux,
			q					=> Acc );
	
	-------------------------------------------------------------------------------
	clk_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			dataout <= x"0000";
		elsif(( clk'event ) and ( clk = '1' )) then
			case conv_integer( Div_Fact ) is
				when 0 		=> DataOut <= Acc(  8 downto 1 );
				when 1 		=> DataOut <= Acc(  9 downto 2 );
				when 2 		=> DataOut <= Acc( 10 downto 3 );
				when 3 		=> DataOut <= Acc( 11 downto 4 );
				when 4 		=> DataOut <= Acc( 12 downto 5 );
				when 5 		=> DataOut <= Acc( 13 downto 6 );
				when 6 		=> DataOut <= Acc( 14 downto 7 );
				when others 	=> DataOut <= Acc( 15 downto 8 );
			end case;
		end if;
	end process;
	-------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- Fir_Block
------------------------------------------------------------------------------------