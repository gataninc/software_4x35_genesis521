-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:34:00"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_blr IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	AD_MR : IN std_logic;
	pbusy : IN std_logic;
	PDone_In : IN std_logic;
	Pur_In : IN std_logic;
	tc_sel : IN std_logic_vector(3 DOWNTO 0);
	tlevel : IN std_logic_vector(15 DOWNTO 0);
	PS_SEL : IN std_logic_vector(1 DOWNTO 0);
	fir_in : IN std_logic_vector(23 DOWNTO 0);
	offset : IN std_logic_vector(15 DOWNTO 0);
	PDone_Out : OUT std_logic;
	Pur_Out : OUT std_logic;
	pbusy_out : OUT std_logic;
	fir_out : OUT std_logic_vector(15 DOWNTO 0);
	blevel_upd : OUT std_logic;
	blevel : OUT std_logic_vector(7 DOWNTO 0)
	);
END fir_blr;

ARCHITECTURE structure OF fir_blr IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_AD_MR : std_logic;
SIGNAL ww_pbusy : std_logic;
SIGNAL ww_PDone_In : std_logic;
SIGNAL ww_Pur_In : std_logic;
SIGNAL ww_tc_sel : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PS_SEL : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_fir_in : std_logic_vector(23 DOWNTO 0);
SIGNAL ww_offset : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PDone_Out : std_logic;
SIGNAL ww_Pur_Out : std_logic;
SIGNAL ww_pbusy_out : std_logic;
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_blevel_upd : std_logic;
SIGNAL ww_blevel : std_logic_vector(7 DOWNTO 0);
SIGNAL \fir_out_ff|dffs[0]~185COMB\ : std_logic;
SIGNAL \clk_cnt[1]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[14]~1044\ : std_logic;
SIGNAL \pbusy_str~701\ : std_logic;
SIGNAL \pbusy_str~705\ : std_logic;
SIGNAL \pbusy_str~706\ : std_logic;
SIGNAL \pbusy_str~707\ : std_logic;
SIGNAL \pbusy_str_cnt[1]\ : std_logic;
SIGNAL \pbusy_str~708\ : std_logic;
SIGNAL \pbusy_str~709\ : std_logic;
SIGNAL \pbusy_str~713\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~518\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~189COMB\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~522\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~526\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~530\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~534\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~538\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~582\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~586\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[13]~1045\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[14]\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~193COMB\ : std_logic;
SIGNAL \clk_cnt[2]\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~590\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[12]~1046\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~197COMB\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[15]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[18]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[19]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[22]\ : std_logic;
SIGNAL \Equal~384\ : std_logic;
SIGNAL \Equal~389\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~594\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[7]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[11]~1047\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~201COMB\ : std_logic;
SIGNAL \ps_mux~2948\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[10]~1048\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~205COMB\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~602\ : std_logic;
SIGNAL \ps_mux~2949\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[9]~1049\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~209COMB\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~606\ : std_logic;
SIGNAL \ps_mux~2951\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[8]~1050\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~213COMB\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~610\ : std_logic;
SIGNAL \ps_mux~2953\ : std_logic;
SIGNAL \fir_acc_ff|dffs[15]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[7]~1051\ : std_logic;
SIGNAL \ps_mux~2955\ : std_logic;
SIGNAL \fir_acc_ff|dffs[14]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[6]~1052\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[7]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[13]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[5]~1053\ : std_logic;
SIGNAL \fir_acc_ff|dffs[12]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[4]~1054\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[5]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[11]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[3]~1055\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[4]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[10]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[2]~1056\ : std_logic;
SIGNAL \fir_acc_ff|dffs[9]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[1]~1057\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[0]~1058\ : std_logic;
SIGNAL \ps_mux~2970\ : std_logic;
SIGNAL \ps_mux~2971\ : std_logic;
SIGNAL \fir_acc_ff|dffs[7]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[6]\ : std_logic;
SIGNAL \ps_mux[6]~2974\ : std_logic;
SIGNAL \fir_acc_ff|dffs[5]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[4]\ : std_logic;
SIGNAL \ps_mux[4]~2978\ : std_logic;
SIGNAL \ps_mux[3]~2980\ : std_logic;
SIGNAL \fir_acc_ff|dffs[2]\ : std_logic;
SIGNAL \ps_mux[1]~2982\ : std_logic;
SIGNAL \ps_mux[0]~2983\ : std_logic;
SIGNAL \offset[7]~combout\ : std_logic;
SIGNAL \offset[6]~combout\ : std_logic;
SIGNAL \tlevel[14]~combout\ : std_logic;
SIGNAL \offset[5]~combout\ : std_logic;
SIGNAL \offset[4]~combout\ : std_logic;
SIGNAL \offset[3]~combout\ : std_logic;
SIGNAL \offset[2]~combout\ : std_logic;
SIGNAL \offset[1]~combout\ : std_logic;
SIGNAL \offset[0]~combout\ : std_logic;
SIGNAL \tlevel[7]~combout\ : std_logic;
SIGNAL \tlevel[5]~combout\ : std_logic;
SIGNAL \tlevel[4]~combout\ : std_logic;
SIGNAL \PDone_In~combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \imr~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[24]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[24]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[24]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[24]\ : std_logic;
SIGNAL \pdone_out_vec[0]\ : std_logic;
SIGNAL \pdone_out_vec[1]\ : std_logic;
SIGNAL \pdone_out_vec[2]\ : std_logic;
SIGNAL \pdone_out_vec[3]\ : std_logic;
SIGNAL \PDone_Out~reg0\ : std_logic;
SIGNAL \Pur_In~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[25]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[25]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[25]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[25]\ : std_logic;
SIGNAL \pbusy~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[26]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[26]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[26]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[26]\ : std_logic;
SIGNAL \fir_in[23]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[23]\ : std_logic;
SIGNAL \tlevel[15]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[15]\ : std_logic;
SIGNAL \fir_in[22]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[22]\ : std_logic;
SIGNAL \tlevel[13]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[13]\ : std_logic;
SIGNAL \tlevel[12]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[12]\ : std_logic;
SIGNAL \tlevel[11]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[11]\ : std_logic;
SIGNAL \tlevel[10]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[10]\ : std_logic;
SIGNAL \tlevel[9]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[9]\ : std_logic;
SIGNAL \tlevel[8]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[8]\ : std_logic;
SIGNAL \fir_in[15]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[15]\ : std_logic;
SIGNAL \tlevel[6]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[6]\ : std_logic;
SIGNAL \fir_in[13]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[13]\ : std_logic;
SIGNAL \fir_in[12]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[12]\ : std_logic;
SIGNAL \tlevel[3]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[3]\ : std_logic;
SIGNAL \tlevel[2]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[2]\ : std_logic;
SIGNAL \tlevel[1]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[1]\ : std_logic;
SIGNAL \tlevel[0]~combout\ : std_logic;
SIGNAL \tlevel_reg_ff|dffs[0]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[0]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[1]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[2]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[3]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[4]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|lcarry[14]\ : std_logic;
SIGNAL \tlevel_cmp|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[27]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[27]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[27]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[27]\ : std_logic;
SIGNAL \tc_sel[0]~combout\ : std_logic;
SIGNAL \tc_sel[2]~combout\ : std_logic;
SIGNAL \tc_sel[1]~combout\ : std_logic;
SIGNAL \pbusy_str_mux[1]~81\ : std_logic;
SIGNAL \pbusy_str~710\ : std_logic;
SIGNAL \tc_sel[3]~combout\ : std_logic;
SIGNAL \pbusy_str_mux[3]~82\ : std_logic;
SIGNAL \pbusy_str~711\ : std_logic;
SIGNAL \pbusy_str_cnt[0]\ : std_logic;
SIGNAL \pbusy_str_cnt[0]~82\ : std_logic;
SIGNAL \pbusy_str_cnt[1]~97\ : std_logic;
SIGNAL \pbusy_str_cnt[2]\ : std_logic;
SIGNAL \pbusy_str_mux[2]~80\ : std_logic;
SIGNAL \pbusy_str~712\ : std_logic;
SIGNAL \pbusy_str~714\ : std_logic;
SIGNAL \pbusy_str_cnt[2]~94\ : std_logic;
SIGNAL \pbusy_str_cnt[3]\ : std_logic;
SIGNAL \pbusy_str_cnt[3]~100\ : std_logic;
SIGNAL \pbusy_str_cnt[4]\ : std_logic;
SIGNAL \pbusy_str_cnt[4]~103\ : std_logic;
SIGNAL \pbusy_str_cnt[5]~88\ : std_logic;
SIGNAL \pbusy_str_cnt[6]~91\ : std_logic;
SIGNAL \pbusy_str_cnt[7]\ : std_logic;
SIGNAL \pbusy_str_cnt[7]~79\ : std_logic;
SIGNAL \pbusy_str_cnt[8]\ : std_logic;
SIGNAL \pbusy_str~717\ : std_logic;
SIGNAL \pbusy_str~718\ : std_logic;
SIGNAL \pbusy_gen_vec[0]\ : std_logic;
SIGNAL \pbusy_gen_vec[1]\ : std_logic;
SIGNAL \pbusy_str~719\ : std_logic;
SIGNAL \pbusy_str_mux[4]~83\ : std_logic;
SIGNAL \pbusy_str~720\ : std_logic;
SIGNAL \pbusy_str~715\ : std_logic;
SIGNAL \pbusy_str~716\ : std_logic;
SIGNAL \pbusy_str~721\ : std_logic;
SIGNAL \pbusy_str_cnt[6]\ : std_logic;
SIGNAL \clk_cnt_max[1]~231\ : std_logic;
SIGNAL \pbusy_str~726\ : std_logic;
SIGNAL \pbusy_str~703\ : std_logic;
SIGNAL \pbusy_str~698\ : std_logic;
SIGNAL \pbusy_str~699\ : std_logic;
SIGNAL \pbusy_str_mux[5]~79\ : std_logic;
SIGNAL \pbusy_str_cnt[5]\ : std_logic;
SIGNAL \pbusy_str~700\ : std_logic;
SIGNAL \pbusy_str~704\ : std_logic;
SIGNAL pbusy_str : std_logic;
SIGNAL \pbusy_out_comb~33\ : std_logic;
SIGNAL \pbusy_out~reg0\ : std_logic;
SIGNAL \offset[8]~combout\ : std_logic;
SIGNAL \PS_SEL[0]~combout\ : std_logic;
SIGNAL \PS_SEL_REG_FF|dffs[0]\ : std_logic;
SIGNAL \PS_SEL[1]~combout\ : std_logic;
SIGNAL \PS_SEL_REG_FF|dffs[1]\ : std_logic;
SIGNAL \pbusy_out_comb~2\ : std_logic;
SIGNAL \ps_mux[23]~2942\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[22]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[22]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[22]\ : std_logic;
SIGNAL \fir_in[21]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[21]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[21]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[21]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[21]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[21]\ : std_logic;
SIGNAL \fir_in[20]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[20]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[20]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[20]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[20]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[20]\ : std_logic;
SIGNAL \fir_in[19]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[19]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[19]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[19]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[19]\ : std_logic;
SIGNAL \fir_in[18]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[18]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[18]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[18]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[18]\ : std_logic;
SIGNAL \fir_in[17]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[17]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[17]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[17]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[17]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[17]\ : std_logic;
SIGNAL \fir_in[16]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[16]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[16]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[16]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[16]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[16]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[15]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[15]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[15]\ : std_logic;
SIGNAL \fir_in[14]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[14]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[14]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[14]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[14]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[14]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[13]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[13]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[13]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[13]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[12]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[12]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[12]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[12]\ : std_logic;
SIGNAL \fir_in[11]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[11]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[11]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[11]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[11]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[11]\ : std_logic;
SIGNAL \fir_in[10]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[10]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[10]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[10]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[10]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[10]\ : std_logic;
SIGNAL \fir_in[9]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[9]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[9]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[9]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[9]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[9]\ : std_logic;
SIGNAL \fir_in[8]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[8]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[8]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[8]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[8]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[8]\ : std_logic;
SIGNAL \fir_in[7]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[7]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[7]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[7]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[7]\ : std_logic;
SIGNAL \fir_in[6]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[6]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[6]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[6]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[6]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[6]\ : std_logic;
SIGNAL \fir_in[5]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[5]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[5]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[5]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[5]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[5]\ : std_logic;
SIGNAL \fir_in[4]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[4]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[4]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[4]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[4]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[4]\ : std_logic;
SIGNAL \fir_in[3]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[3]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[3]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[3]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[3]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[3]\ : std_logic;
SIGNAL \fir_in[2]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[2]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[2]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[2]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[2]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[2]\ : std_logic;
SIGNAL \fir_in[1]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[1]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[1]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[1]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[1]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[1]\ : std_logic;
SIGNAL \fir_in[0]~combout\ : std_logic;
SIGNAL \u_reg:0:u_reg_i|dffs[0]\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[0]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[0]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[0]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[0]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[0]~220\ : std_logic;
SIGNAL \fir_diff_ff|dffs[1]~217\ : std_logic;
SIGNAL \fir_diff_ff|dffs[2]~214\ : std_logic;
SIGNAL \fir_diff_ff|dffs[3]~211\ : std_logic;
SIGNAL \fir_diff_ff|dffs[4]~208\ : std_logic;
SIGNAL \fir_diff_ff|dffs[5]~205\ : std_logic;
SIGNAL \fir_diff_ff|dffs[6]~199\ : std_logic;
SIGNAL \fir_diff_ff|dffs[7]~202\ : std_logic;
SIGNAL \fir_diff_ff|dffs[8]~151\ : std_logic;
SIGNAL \fir_diff_ff|dffs[9]~154\ : std_logic;
SIGNAL \fir_diff_ff|dffs[10]~157\ : std_logic;
SIGNAL \fir_diff_ff|dffs[11]~160\ : std_logic;
SIGNAL \fir_diff_ff|dffs[12]~163\ : std_logic;
SIGNAL \fir_diff_ff|dffs[13]~166\ : std_logic;
SIGNAL \fir_diff_ff|dffs[14]~169\ : std_logic;
SIGNAL \fir_diff_ff|dffs[15]~172\ : std_logic;
SIGNAL \fir_diff_ff|dffs[16]~175\ : std_logic;
SIGNAL \fir_diff_ff|dffs[17]~178\ : std_logic;
SIGNAL \fir_diff_ff|dffs[18]~181\ : std_logic;
SIGNAL \fir_diff_ff|dffs[19]~184\ : std_logic;
SIGNAL \fir_diff_ff|dffs[20]~187\ : std_logic;
SIGNAL \fir_diff_ff|dffs[21]~190\ : std_logic;
SIGNAL \fir_diff_ff|dffs[22]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[21]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[20]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[19]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[18]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[17]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[16]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[15]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[14]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[13]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[12]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[11]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[10]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[9]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[8]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[7]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[6]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[5]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[4]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[3]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[2]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[1]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[0]\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~612\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~608\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~604\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~600\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~596\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~592\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~584\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~588\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~520\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~524\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~528\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~532\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~536\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~540\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~544\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~548\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~552\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~556\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~560\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~564\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~568\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~572\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~574\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~570\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~566\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~562\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~558\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~554\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~550\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~546\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~542\ : std_logic;
SIGNAL \AD_MR~combout\ : std_logic;
SIGNAL \fir_acc_ff|dffs[24]~287\ : std_logic;
SIGNAL \fir_acc_ff|dffs[25]~290\ : std_logic;
SIGNAL \fir_acc_ff|dffs[26]~293\ : std_logic;
SIGNAL \fir_acc_ff|dffs[27]~296\ : std_logic;
SIGNAL \fir_acc_ff|dffs[28]~299\ : std_logic;
SIGNAL \fir_acc_ff|dffs[29]~302\ : std_logic;
SIGNAL \fir_acc_ff|dffs[30]~305\ : std_logic;
SIGNAL \fir_acc_ff|dffs[31]~308\ : std_logic;
SIGNAL \fir_acc_ff|dffs[32]~311\ : std_logic;
SIGNAL \fir_acc_ff|dffs[33]~314\ : std_logic;
SIGNAL \fir_acc_ff|dffs[34]~317\ : std_logic;
SIGNAL \fir_acc_ff|dffs[35]~320\ : std_logic;
SIGNAL \fir_acc_ff|dffs[36]~323\ : std_logic;
SIGNAL \fir_acc_ff|dffs[37]~326\ : std_logic;
SIGNAL \fir_acc_ff|dffs[38]\ : std_logic;
SIGNAL \ps_mux~2943\ : std_logic;
SIGNAL \ps_mux~2945\ : std_logic;
SIGNAL \fir_acc_ff|dffs[37]\ : std_logic;
SIGNAL \ps_mux~2944\ : std_logic;
SIGNAL \ps_mux~2946\ : std_logic;
SIGNAL \fir_acc_ff|dffs[36]\ : std_logic;
SIGNAL \ps_mux~2947\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~598\ : std_logic;
SIGNAL \Equal~386\ : std_logic;
SIGNAL \u_reg:1:u_reg_i|dffs[23]\ : std_logic;
SIGNAL \u_reg:2:u_reg_i|dffs[23]\ : std_logic;
SIGNAL \u_reg:3:u_reg_i|dffs[23]\ : std_logic;
SIGNAL \fir_in_del1_ff|dffs[23]\ : std_logic;
SIGNAL \fir_diff_ff|dffs[22]~193\ : std_logic;
SIGNAL \fir_diff_ff|dffs[23]\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~576\ : std_logic;
SIGNAL \fir_sum1_as|adder|result_node|cs_buffer[16]~578\ : std_logic;
SIGNAL \fir_acc_ff|dffs[38]~329\ : std_logic;
SIGNAL \fir_acc_ff|dffs[39]\ : std_logic;
SIGNAL \ps_mux~2950\ : std_logic;
SIGNAL \ps_mux~2952\ : std_logic;
SIGNAL \ps_mux~2954\ : std_logic;
SIGNAL \ps_mux~2956\ : std_logic;
SIGNAL \ps_mux~2957\ : std_logic;
SIGNAL \fir_acc_ff|dffs[34]\ : std_logic;
SIGNAL \ps_mux~2959\ : std_logic;
SIGNAL \fir_acc_ff|dffs[29]\ : std_logic;
SIGNAL \ps_mux~2960\ : std_logic;
SIGNAL \fir_acc_ff|dffs[33]\ : std_logic;
SIGNAL \ps_mux~2961\ : std_logic;
SIGNAL \fir_acc_ff|dffs[28]\ : std_logic;
SIGNAL \ps_mux~2962\ : std_logic;
SIGNAL \fir_acc_ff|dffs[32]\ : std_logic;
SIGNAL \ps_mux~2963\ : std_logic;
SIGNAL \fir_acc_ff|dffs[27]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[35]\ : std_logic;
SIGNAL \ps_mux~2964\ : std_logic;
SIGNAL \fir_acc_ff|dffs[31]\ : std_logic;
SIGNAL \ps_mux~2965\ : std_logic;
SIGNAL \fir_acc_ff|dffs[26]\ : std_logic;
SIGNAL \ps_mux~2966\ : std_logic;
SIGNAL \ps_mux~2958\ : std_logic;
SIGNAL \ps_mux~2967\ : std_logic;
SIGNAL \fir_acc_ff|dffs[25]\ : std_logic;
SIGNAL \ps_mux~2968\ : std_logic;
SIGNAL \ps_mux~2969\ : std_logic;
SIGNAL \ps_mux[7]~2972\ : std_logic;
SIGNAL \ps_mux[7]~2973\ : std_logic;
SIGNAL \fir_acc_ff|dffs[30]\ : std_logic;
SIGNAL \ps_mux[6]~2975\ : std_logic;
SIGNAL \ps_mux[5]~2976\ : std_logic;
SIGNAL \ps_mux[5]~2977\ : std_logic;
SIGNAL \ps_mux[4]~2979\ : std_logic;
SIGNAL \ps_mux[2]~2981\ : std_logic;
SIGNAL \fir_acc_ff|dffs[0]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[0]~404\ : std_logic;
SIGNAL \fir_acc_ff|dffs[1]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[1]~401\ : std_logic;
SIGNAL \fir_acc_ff|dffs[2]~398\ : std_logic;
SIGNAL \fir_acc_ff|dffs[3]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[3]~395\ : std_logic;
SIGNAL \fir_acc_ff|dffs[4]~392\ : std_logic;
SIGNAL \fir_acc_ff|dffs[5]~389\ : std_logic;
SIGNAL \fir_acc_ff|dffs[6]~386\ : std_logic;
SIGNAL \fir_acc_ff|dffs[7]~383\ : std_logic;
SIGNAL \fir_acc_ff|dffs[8]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[8]~380\ : std_logic;
SIGNAL \fir_acc_ff|dffs[9]~377\ : std_logic;
SIGNAL \fir_acc_ff|dffs[10]~374\ : std_logic;
SIGNAL \fir_acc_ff|dffs[11]~371\ : std_logic;
SIGNAL \fir_acc_ff|dffs[12]~368\ : std_logic;
SIGNAL \fir_acc_ff|dffs[13]~365\ : std_logic;
SIGNAL \fir_acc_ff|dffs[14]~362\ : std_logic;
SIGNAL \fir_acc_ff|dffs[15]~359\ : std_logic;
SIGNAL \fir_acc_ff|dffs[16]~356\ : std_logic;
SIGNAL \fir_acc_ff|dffs[17]~353\ : std_logic;
SIGNAL \fir_acc_ff|dffs[18]~350\ : std_logic;
SIGNAL \fir_acc_ff|dffs[19]~347\ : std_logic;
SIGNAL \fir_acc_ff|dffs[20]~344\ : std_logic;
SIGNAL \fir_acc_ff|dffs[21]~341\ : std_logic;
SIGNAL \fir_acc_ff|dffs[22]~335\ : std_logic;
SIGNAL \fir_acc_ff|dffs[23]~338\ : std_logic;
SIGNAL \fir_acc_ff|dffs[24]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[23]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[22]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[21]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[20]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[19]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[18]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[17]\ : std_logic;
SIGNAL \fir_acc_ff|dffs[16]\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~215\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~211\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~207\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~203\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~199\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~195\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~191\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~187\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]\ : std_logic;
SIGNAL \offset[9]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[0]~138\ : std_logic;
SIGNAL \fir_out_ff|dffs[1]\ : std_logic;
SIGNAL \offset[10]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[1]~141\ : std_logic;
SIGNAL \fir_out_ff|dffs[2]\ : std_logic;
SIGNAL \offset[11]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[2]~144\ : std_logic;
SIGNAL \fir_out_ff|dffs[3]\ : std_logic;
SIGNAL \offset[12]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[3]~147\ : std_logic;
SIGNAL \fir_out_ff|dffs[4]\ : std_logic;
SIGNAL \offset[13]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[4]~150\ : std_logic;
SIGNAL \fir_out_ff|dffs[5]\ : std_logic;
SIGNAL \offset[14]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[5]~153\ : std_logic;
SIGNAL \fir_out_ff|dffs[6]\ : std_logic;
SIGNAL \offset[15]~combout\ : std_logic;
SIGNAL \fir_out_ff|dffs[6]~156\ : std_logic;
SIGNAL \fir_out_ff|dffs[7]\ : std_logic;
SIGNAL \fir_out_ff|dffs[7]~159\ : std_logic;
SIGNAL \fir_out_ff|dffs[8]\ : std_logic;
SIGNAL \fir_out_ff|dffs[8]~162\ : std_logic;
SIGNAL \fir_out_ff|dffs[9]\ : std_logic;
SIGNAL \fir_out_ff|dffs[9]~165\ : std_logic;
SIGNAL \fir_out_ff|dffs[10]\ : std_logic;
SIGNAL \fir_out_ff|dffs[10]~168\ : std_logic;
SIGNAL \fir_out_ff|dffs[11]\ : std_logic;
SIGNAL \fir_out_ff|dffs[11]~171\ : std_logic;
SIGNAL \fir_out_ff|dffs[12]\ : std_logic;
SIGNAL \fir_out_ff|dffs[12]~174\ : std_logic;
SIGNAL \fir_out_ff|dffs[13]\ : std_logic;
SIGNAL \fir_out_ff|dffs[13]~177\ : std_logic;
SIGNAL \fir_out_ff|dffs[14]\ : std_logic;
SIGNAL \fir_out_ff|dffs[14]~180\ : std_logic;
SIGNAL \fir_out_ff|dffs[15]\ : std_logic;
SIGNAL \clk_cnt[0]~100\ : std_logic;
SIGNAL \clk_cnt[1]~103\ : std_logic;
SIGNAL \clk_cnt[2]~121\ : std_logic;
SIGNAL \clk_cnt[3]\ : std_logic;
SIGNAL \clk_cnt[3]~130\ : std_logic;
SIGNAL \clk_cnt[4]\ : std_logic;
SIGNAL \clk_cnt[4]~127\ : std_logic;
SIGNAL \clk_cnt[5]\ : std_logic;
SIGNAL \clk_cnt[5]~118\ : std_logic;
SIGNAL \clk_cnt[6]\ : std_logic;
SIGNAL \clk_cnt[6]~112\ : std_logic;
SIGNAL \clk_cnt[7]\ : std_logic;
SIGNAL \clk_cnt[7]~106\ : std_logic;
SIGNAL \clk_cnt[8]\ : std_logic;
SIGNAL \clk_cnt[8]~124\ : std_logic;
SIGNAL \clk_cnt[9]\ : std_logic;
SIGNAL \clk_cnt[9]~115\ : std_logic;
SIGNAL \clk_cnt[10]\ : std_logic;
SIGNAL \Equal~379\ : std_logic;
SIGNAL \Equal~378\ : std_logic;
SIGNAL \clk_cnt_max[2]~226\ : std_logic;
SIGNAL \clk_cnt_max[5]~230\ : std_logic;
SIGNAL \clk_cnt_max[8]~229\ : std_logic;
SIGNAL \clk_cnt_max[3]~227\ : std_logic;
SIGNAL \clk_cnt_max[4]~228\ : std_logic;
SIGNAL \Equal~392\ : std_logic;
SIGNAL \Equal~393\ : std_logic;
SIGNAL \Equal~388\ : std_logic;
SIGNAL \Equal~374\ : std_logic;
SIGNAL \clk_cnt[0]\ : std_logic;
SIGNAL \Equal~371\ : std_logic;
SIGNAL \clk_cnt_max[1]~224\ : std_logic;
SIGNAL \Equal~372\ : std_logic;
SIGNAL \blevel_upd~reg0\ : std_logic;
SIGNAL \blevel_ff|dffs[0]\ : std_logic;
SIGNAL \blevel_ff|dffs[1]\ : std_logic;
SIGNAL \blevel_ff|dffs[2]\ : std_logic;
SIGNAL \blevel_ff|dffs[3]\ : std_logic;
SIGNAL \blevel_ff|dffs[4]\ : std_logic;
SIGNAL \blevel_ff|dffs[5]\ : std_logic;
SIGNAL \blevel_ff|dffs[6]\ : std_logic;
SIGNAL \blevel_ff|dffs[7]\ : std_logic;
SIGNAL ALT_INV_pbusy_str : std_logic;
SIGNAL \ALT_INV_pbusy_out_comb~2\ : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_AD_MR <= AD_MR;
ww_pbusy <= pbusy;
ww_PDone_In <= PDone_In;
ww_Pur_In <= Pur_In;
ww_tc_sel <= tc_sel;
ww_tlevel <= tlevel;
ww_PS_SEL <= PS_SEL;
ww_fir_in <= fir_in;
ww_offset <= offset;
PDone_Out <= ww_PDone_Out;
Pur_Out <= ww_Pur_Out;
pbusy_out <= ww_pbusy_out;
fir_out <= ww_fir_out;
blevel_upd <= ww_blevel_upd;
blevel <= ww_blevel;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_pbusy_str <= NOT pbusy_str;
\ALT_INV_pbusy_out_comb~2\ <= NOT \pbusy_out_comb~2\;

\clk_cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[1]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[1]\ $ (\clk_cnt[0]~100\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[1]~103\ = CARRY(!\clk_cnt[0]~100\ # !\clk_cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[1]\,
	cin => \clk_cnt[0]~100\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[1]\,
	cout => \clk_cnt[1]~103\);

\pbusy_str~701_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~701\ = pbusy_str & !\clk_cnt_max[1]~224\ & \pbusy_str_cnt[6]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pbusy_str,
	datac => \clk_cnt_max[1]~224\,
	datad => \pbusy_str_cnt[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~701\);

\pbusy_str~705_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~705\ = \pbusy_str_cnt[8]\ # !\tc_sel[1]~combout\ # !\tc_sel[0]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[8]\,
	datac => \tc_sel[0]~combout\,
	datad => \tc_sel[1]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~705\);

\pbusy_str~706_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~706\ = pbusy_str & \pbusy_str~705\ & (\pbusy_str_cnt[7]\ # \pbusy_str_cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pbusy_str,
	datab => \pbusy_str_cnt[7]\,
	datac => \pbusy_str_cnt[0]\,
	datad => \pbusy_str~705\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~706\);

\pbusy_str~707_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~707\ = !\pbusy_str_cnt[5]\ & (\pbusy_str_mux[5]~79\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[5]\,
	datad => \pbusy_str_mux[5]~79\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~707\);

\pbusy_str_cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[1]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[1]\ $ (\pbusy_str_cnt[0]~82\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[1]~97\ = CARRY(!\pbusy_str_cnt[0]~82\ # !\pbusy_str_cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[1]\,
	cin => \pbusy_str_cnt[0]~82\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[1]\,
	cout => \pbusy_str_cnt[1]~97\);

\pbusy_str~708_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~708\ = !\pbusy_str_cnt[1]\ & (!\pbusy_str_mux[1]~81\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[1]\,
	datad => \pbusy_str_mux[1]~81\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~708\);

\pbusy_str~709_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~709\ = \pbusy_str~706\ # pbusy_str & (\pbusy_str~707\ # \pbusy_str~708\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pbusy_str,
	datab => \pbusy_str~707\,
	datac => \pbusy_str~706\,
	datad => \pbusy_str~708\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~709\);

\pbusy_str~713_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~713\ = pbusy_str & \pbusy_str_cnt[4]\ & !\pbusy_str_mux[4]~83\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pbusy_str,
	datac => \pbusy_str_cnt[4]\,
	datad => \pbusy_str_mux[4]~83\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~713\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~518_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~518\ = \fir_acc_ff|dffs[24]\ $ \fir_diff_ff|dffs[8]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~588\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~520\ = CARRY(\fir_acc_ff|dffs[24]\ & (\fir_diff_ff|dffs[8]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~588\) # !\fir_acc_ff|dffs[24]\ & \fir_diff_ff|dffs[8]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~588\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[24]\,
	datab => \fir_diff_ff|dffs[8]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~588\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~518\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~520\);

\offset[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(7),
	combout => \offset[7]~combout\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~522_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~522\ = \fir_acc_ff|dffs[25]\ $ \fir_diff_ff|dffs[9]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~520\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~524\ = CARRY(\fir_acc_ff|dffs[25]\ & !\fir_diff_ff|dffs[9]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~520\ # !\fir_acc_ff|dffs[25]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~520\ # 
-- !\fir_diff_ff|dffs[9]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[25]\,
	datab => \fir_diff_ff|dffs[9]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~520\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~522\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~524\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~526_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~526\ = \fir_acc_ff|dffs[26]\ $ \fir_diff_ff|dffs[10]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~524\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~528\ = CARRY(\fir_acc_ff|dffs[26]\ & (\fir_diff_ff|dffs[10]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~524\) # !\fir_acc_ff|dffs[26]\ & \fir_diff_ff|dffs[10]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~524\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[26]\,
	datab => \fir_diff_ff|dffs[10]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~524\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~526\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~528\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~530_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~530\ = \fir_acc_ff|dffs[27]\ $ \fir_diff_ff|dffs[11]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~528\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~532\ = CARRY(\fir_acc_ff|dffs[27]\ & !\fir_diff_ff|dffs[11]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~528\ # !\fir_acc_ff|dffs[27]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~528\ # 
-- !\fir_diff_ff|dffs[11]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[27]\,
	datab => \fir_diff_ff|dffs[11]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~528\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~530\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~532\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~534_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~534\ = \fir_acc_ff|dffs[28]\ $ \fir_diff_ff|dffs[12]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~532\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~536\ = CARRY(\fir_acc_ff|dffs[28]\ & (\fir_diff_ff|dffs[12]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~532\) # !\fir_acc_ff|dffs[28]\ & \fir_diff_ff|dffs[12]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~532\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[28]\,
	datab => \fir_diff_ff|dffs[12]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~532\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~534\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~536\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~538_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~538\ = \fir_acc_ff|dffs[29]\ $ \fir_diff_ff|dffs[13]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~536\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~540\ = CARRY(\fir_acc_ff|dffs[29]\ & !\fir_diff_ff|dffs[13]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~536\ # !\fir_acc_ff|dffs[29]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~536\ # 
-- !\fir_diff_ff|dffs[13]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[29]\,
	datab => \fir_diff_ff|dffs[13]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~536\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~538\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~540\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~582_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~582\ = \fir_acc_ff|dffs[22]\ $ \fir_diff_ff|dffs[6]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~592\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~584\ = CARRY(\fir_acc_ff|dffs[22]\ & (\fir_diff_ff|dffs[6]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~592\) # !\fir_acc_ff|dffs[22]\ & \fir_diff_ff|dffs[6]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~592\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[22]\,
	datab => \fir_diff_ff|dffs[6]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~592\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~582\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~584\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~586_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~586\ = \fir_acc_ff|dffs[23]\ $ \fir_diff_ff|dffs[7]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~584\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~588\ = CARRY(\fir_acc_ff|dffs[23]\ & !\fir_diff_ff|dffs[7]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~584\ # !\fir_acc_ff|dffs[23]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~584\ # 
-- !\fir_diff_ff|dffs[7]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[23]\,
	datab => \fir_diff_ff|dffs[7]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~584\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~586\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~588\);

\tlevel_reg_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[14]\ = DFFE(\tlevel[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[14]\);

\offset[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(6),
	combout => \offset[6]~combout\);

\clk_cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[2]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[2]\ $ (!\clk_cnt[1]~103\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[2]~121\ = CARRY(\clk_cnt[2]\ & (!\clk_cnt[1]~103\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[2]\,
	cin => \clk_cnt[1]~103\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[2]\,
	cout => \clk_cnt[2]~121\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~590_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~590\ = \fir_acc_ff|dffs[21]\ $ \fir_diff_ff|dffs[5]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~596\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~592\ = CARRY(\fir_acc_ff|dffs[21]\ & !\fir_diff_ff|dffs[5]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~596\ # !\fir_acc_ff|dffs[21]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~596\ # 
-- !\fir_diff_ff|dffs[5]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[21]\,
	datab => \fir_diff_ff|dffs[5]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~596\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~590\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~592\);

\tlevel[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(14),
	combout => \tlevel[14]~combout\);

\offset[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(5),
	combout => \offset[5]~combout\);

\fir_in_del1_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[15]\ = DFFE(\u_reg:3:u_reg_i|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[15]\);

\fir_in_del1_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[18]\ = DFFE(\u_reg:3:u_reg_i|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[18]\);

\fir_in_del1_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[19]\ = DFFE(\u_reg:3:u_reg_i|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[19]\);

\fir_in_del1_ff|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[22]\ = DFFE(\u_reg:3:u_reg_i|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[22]\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~594_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~594\ = \fir_acc_ff|dffs[20]\ $ \fir_diff_ff|dffs[4]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~600\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~596\ = CARRY(\fir_acc_ff|dffs[20]\ & (\fir_diff_ff|dffs[4]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~600\) # !\fir_acc_ff|dffs[20]\ & \fir_diff_ff|dffs[4]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~600\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[20]\,
	datab => \fir_diff_ff|dffs[4]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~600\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~594\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~596\);

\fir_in_del1_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[7]\ = DFFE(\u_reg:3:u_reg_i|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[7]\);

\offset[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(4),
	combout => \offset[4]~combout\);

\ps_mux~2948_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2948\ = !\pbusy_out_comb~2\ & (\ps_mux~2943\ # \ps_mux~2944\ & \fir_acc_ff|dffs[35]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2944\,
	datab => \fir_acc_ff|dffs[35]\,
	datac => \ps_mux~2943\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2948\);

\offset[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(3),
	combout => \offset[3]~combout\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~602_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~602\ = \fir_acc_ff|dffs[18]\ $ \fir_diff_ff|dffs[2]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~608\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~604\ = CARRY(\fir_acc_ff|dffs[18]\ & (\fir_diff_ff|dffs[2]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~608\) # !\fir_acc_ff|dffs[18]\ & \fir_diff_ff|dffs[2]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~608\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[18]\,
	datab => \fir_diff_ff|dffs[2]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~608\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~602\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~604\);

\ps_mux~2949_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2949\ = \PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\ & \fir_acc_ff|dffs[34]\ # !\PS_SEL_REG_FF|dffs[0]\ & (\fir_acc_ff|dffs[38]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[34]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \fir_acc_ff|dffs[38]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2949\);

\offset[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(2),
	combout => \offset[2]~combout\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~606_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~606\ = \fir_acc_ff|dffs[17]\ $ \fir_diff_ff|dffs[1]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~612\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~608\ = CARRY(\fir_acc_ff|dffs[17]\ & !\fir_diff_ff|dffs[1]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~612\ # !\fir_acc_ff|dffs[17]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~612\ # 
-- !\fir_diff_ff|dffs[1]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[17]\,
	datab => \fir_diff_ff|dffs[1]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~612\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~606\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~608\);

\ps_mux~2951_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2951\ = \PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\ & (\fir_acc_ff|dffs[33]\) # !\PS_SEL_REG_FF|dffs[0]\ & \fir_acc_ff|dffs[37]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[37]\,
	datab => \fir_acc_ff|dffs[33]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \PS_SEL_REG_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2951\);

\offset[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(1),
	combout => \offset[1]~combout\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~610_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~610\ = \fir_acc_ff|dffs[16]\ $ \fir_diff_ff|dffs[0]\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~612\ = CARRY(\fir_acc_ff|dffs[16]\ & \fir_diff_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[16]\,
	datab => \fir_diff_ff|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~610\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~612\);

\ps_mux~2953_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2953\ = \PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\ & \fir_acc_ff|dffs[32]\ # !\PS_SEL_REG_FF|dffs[0]\ & (\fir_acc_ff|dffs[36]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[32]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \fir_acc_ff|dffs[36]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2953\);

\fir_acc_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[15]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[15]\ $ \ps_mux~2956\ $ !\fir_acc_ff|dffs[14]~362\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[15]~359\ = CARRY(\fir_acc_ff|dffs[15]\ & \ps_mux~2956\ & !\fir_acc_ff|dffs[14]~362\ # !\fir_acc_ff|dffs[15]\ & (\ps_mux~2956\ # !\fir_acc_ff|dffs[14]~362\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[15]\,
	datab => \ps_mux~2956\,
	cin => \fir_acc_ff|dffs[14]~362\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[15]\,
	cout => \fir_acc_ff|dffs[15]~359\);

\offset[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(0),
	combout => \offset[0]~combout\);

\ps_mux~2955_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2955\ = \PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\ & \fir_acc_ff|dffs[31]\ # !\PS_SEL_REG_FF|dffs[0]\ & (\fir_acc_ff|dffs[35]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[31]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \fir_acc_ff|dffs[35]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2955\);

\fir_acc_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[14]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[14]\ $ \ps_mux~2959\ $ \fir_acc_ff|dffs[13]~365\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[14]~362\ = CARRY(\fir_acc_ff|dffs[14]\ & (!\fir_acc_ff|dffs[13]~365\ # !\ps_mux~2959\) # !\fir_acc_ff|dffs[14]\ & !\ps_mux~2959\ & !\fir_acc_ff|dffs[13]~365\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[14]\,
	datab => \ps_mux~2959\,
	cin => \fir_acc_ff|dffs[13]~365\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[14]\,
	cout => \fir_acc_ff|dffs[14]~362\);

\tlevel_reg_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[7]\ = DFFE(\tlevel[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[7]\);

\fir_acc_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[13]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[13]\ $ \ps_mux~2961\ $ !\fir_acc_ff|dffs[12]~368\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[13]~365\ = CARRY(\fir_acc_ff|dffs[13]\ & \ps_mux~2961\ & !\fir_acc_ff|dffs[12]~368\ # !\fir_acc_ff|dffs[13]\ & (\ps_mux~2961\ # !\fir_acc_ff|dffs[12]~368\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[13]\,
	datab => \ps_mux~2961\,
	cin => \fir_acc_ff|dffs[12]~368\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[13]\,
	cout => \fir_acc_ff|dffs[13]~365\);

\tlevel[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(7),
	combout => \tlevel[7]~combout\);

\fir_acc_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[12]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[12]\ $ \ps_mux~2963\ $ \fir_acc_ff|dffs[11]~371\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[12]~368\ = CARRY(\fir_acc_ff|dffs[12]\ & (!\fir_acc_ff|dffs[11]~371\ # !\ps_mux~2963\) # !\fir_acc_ff|dffs[12]\ & !\ps_mux~2963\ & !\fir_acc_ff|dffs[11]~371\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[12]\,
	datab => \ps_mux~2963\,
	cin => \fir_acc_ff|dffs[11]~371\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[12]\,
	cout => \fir_acc_ff|dffs[12]~368\);

\tlevel_reg_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[5]\ = DFFE(\tlevel[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[5]\);

\fir_acc_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[11]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[11]\ $ \ps_mux~2965\ $ !\fir_acc_ff|dffs[10]~374\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[11]~371\ = CARRY(\fir_acc_ff|dffs[11]\ & \ps_mux~2965\ & !\fir_acc_ff|dffs[10]~374\ # !\fir_acc_ff|dffs[11]\ & (\ps_mux~2965\ # !\fir_acc_ff|dffs[10]~374\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[11]\,
	datab => \ps_mux~2965\,
	cin => \fir_acc_ff|dffs[10]~374\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[11]\,
	cout => \fir_acc_ff|dffs[11]~371\);

\tlevel_reg_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[4]\ = DFFE(\tlevel[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[4]\);

\tlevel[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(5),
	combout => \tlevel[5]~combout\);

\fir_acc_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[10]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[10]\ $ \ps_mux~2967\ $ \fir_acc_ff|dffs[9]~377\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[10]~374\ = CARRY(\fir_acc_ff|dffs[10]\ & (!\fir_acc_ff|dffs[9]~377\ # !\ps_mux~2967\) # !\fir_acc_ff|dffs[10]\ & !\ps_mux~2967\ & !\fir_acc_ff|dffs[9]~377\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[10]\,
	datab => \ps_mux~2967\,
	cin => \fir_acc_ff|dffs[9]~377\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[10]\,
	cout => \fir_acc_ff|dffs[10]~374\);

\tlevel[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(4),
	combout => \tlevel[4]~combout\);

\fir_acc_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[9]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[9]\ $ \ps_mux~2969\ $ !\fir_acc_ff|dffs[8]~380\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[9]~377\ = CARRY(\fir_acc_ff|dffs[9]\ & \ps_mux~2969\ & !\fir_acc_ff|dffs[8]~380\ # !\fir_acc_ff|dffs[9]\ & (\ps_mux~2969\ # !\fir_acc_ff|dffs[8]~380\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[9]\,
	datab => \ps_mux~2969\,
	cin => \fir_acc_ff|dffs[8]~380\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[9]\,
	cout => \fir_acc_ff|dffs[9]~377\);

\ps_mux~2970_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2970\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[24]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[32]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C480",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \fir_acc_ff|dffs[24]\,
	datad => \fir_acc_ff|dffs[32]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2970\);

\ps_mux~2971_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2971\ = !\pbusy_out_comb~2\ & (\ps_mux~2970\ # \ps_mux~2958\ & \fir_acc_ff|dffs[28]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2970\,
	datab => \ps_mux~2958\,
	datac => \fir_acc_ff|dffs[28]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2971\);

\fir_acc_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[7]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[7]\ $ \ps_mux[7]~2973\ $ !\fir_acc_ff|dffs[6]~386\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[7]~383\ = CARRY(\fir_acc_ff|dffs[7]\ & \ps_mux[7]~2973\ & !\fir_acc_ff|dffs[6]~386\ # !\fir_acc_ff|dffs[7]\ & (\ps_mux[7]~2973\ # !\fir_acc_ff|dffs[6]~386\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[7]\,
	datab => \ps_mux[7]~2973\,
	cin => \fir_acc_ff|dffs[6]~386\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[7]\,
	cout => \fir_acc_ff|dffs[7]~383\);

\fir_acc_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[6]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[6]\ $ \ps_mux[6]~2975\ $ \fir_acc_ff|dffs[5]~389\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[6]~386\ = CARRY(\fir_acc_ff|dffs[6]\ & (!\fir_acc_ff|dffs[5]~389\ # !\ps_mux[6]~2975\) # !\fir_acc_ff|dffs[6]\ & !\ps_mux[6]~2975\ & !\fir_acc_ff|dffs[5]~389\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[6]\,
	datab => \ps_mux[6]~2975\,
	cin => \fir_acc_ff|dffs[5]~389\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[6]\,
	cout => \fir_acc_ff|dffs[6]~386\);

\ps_mux[6]~2974_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[6]~2974\ = \fir_acc_ff|dffs[26]\ & \PS_SEL_REG_FF|dffs[1]\ & !\PS_SEL_REG_FF|dffs[0]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \fir_acc_ff|dffs[26]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \PS_SEL_REG_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[6]~2974\);

\fir_acc_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[5]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[5]\ $ \ps_mux[5]~2977\ $ !\fir_acc_ff|dffs[4]~392\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[5]~389\ = CARRY(\fir_acc_ff|dffs[5]\ & \ps_mux[5]~2977\ & !\fir_acc_ff|dffs[4]~392\ # !\fir_acc_ff|dffs[5]\ & (\ps_mux[5]~2977\ # !\fir_acc_ff|dffs[4]~392\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[5]\,
	datab => \ps_mux[5]~2977\,
	cin => \fir_acc_ff|dffs[4]~392\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[5]\,
	cout => \fir_acc_ff|dffs[5]~389\);

\fir_acc_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[4]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[4]\ $ \ps_mux[4]~2979\ $ \fir_acc_ff|dffs[3]~395\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[4]~392\ = CARRY(\fir_acc_ff|dffs[4]\ & (!\fir_acc_ff|dffs[3]~395\ # !\ps_mux[4]~2979\) # !\fir_acc_ff|dffs[4]\ & !\ps_mux[4]~2979\ & !\fir_acc_ff|dffs[3]~395\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[4]\,
	datab => \ps_mux[4]~2979\,
	cin => \fir_acc_ff|dffs[3]~395\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[4]\,
	cout => \fir_acc_ff|dffs[4]~392\);

\ps_mux[4]~2978_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[4]~2978\ = \PS_SEL_REG_FF|dffs[1]\ & (!\PS_SEL_REG_FF|dffs[0]\ & \fir_acc_ff|dffs[24]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	datad => \fir_acc_ff|dffs[24]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[4]~2978\);

\ps_mux[3]~2980_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[3]~2980\ = \fir_acc_ff|dffs[27]\ & !\PS_SEL_REG_FF|dffs[1]\ & \PS_SEL_REG_FF|dffs[0]\ & !\pbusy_out_comb~2\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[27]\,
	datab => \PS_SEL_REG_FF|dffs[1]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[3]~2980\);

\fir_acc_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[2]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_acc_ff|dffs[2]\ $ \ps_mux[2]~2981\ $ \fir_acc_ff|dffs[1]~401\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[2]~398\ = CARRY(\fir_acc_ff|dffs[2]\ & (!\fir_acc_ff|dffs[1]~401\ # !\ps_mux[2]~2981\) # !\fir_acc_ff|dffs[2]\ & !\ps_mux[2]~2981\ & !\fir_acc_ff|dffs[1]~401\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[2]\,
	datab => \ps_mux[2]~2981\,
	cin => \fir_acc_ff|dffs[1]~401\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[2]\,
	cout => \fir_acc_ff|dffs[2]~398\);

\ps_mux[1]~2982_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[1]~2982\ = \fir_acc_ff|dffs[25]\ & !\PS_SEL_REG_FF|dffs[1]\ & \PS_SEL_REG_FF|dffs[0]\ & !\pbusy_out_comb~2\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[25]\,
	datab => \PS_SEL_REG_FF|dffs[1]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[1]~2982\);

\ps_mux[0]~2983_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[0]~2983\ = \fir_acc_ff|dffs[24]\ & !\PS_SEL_REG_FF|dffs[1]\ & \PS_SEL_REG_FF|dffs[0]\ & !\pbusy_out_comb~2\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[24]\,
	datab => \PS_SEL_REG_FF|dffs[1]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[0]~2983\);

\PDone_In~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PDone_In,
	combout => \PDone_In~combout\);

\clk~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => \clk~combout\);

\imr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => \imr~combout\);

\u_reg:0:u_reg_i|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[24]\ = DFFE(\PDone_In~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \PDone_In~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[24]\);

\u_reg:1:u_reg_i|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[24]\ = DFFE(\u_reg:0:u_reg_i|dffs[24]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[24]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[24]\);

\u_reg:2:u_reg_i|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[24]\ = DFFE(\u_reg:1:u_reg_i|dffs[24]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[24]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[24]\);

\u_reg:3:u_reg_i|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[24]\ = DFFE(\u_reg:2:u_reg_i|dffs[24]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[24]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[24]\);

\pdone_out_vec[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \pdone_out_vec[0]\ = DFFE(\u_reg:3:u_reg_i|dffs[24]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[24]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pdone_out_vec[0]\);

\pdone_out_vec[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \pdone_out_vec[1]\ = DFFE(\pdone_out_vec[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pdone_out_vec[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pdone_out_vec[1]\);

\pdone_out_vec[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \pdone_out_vec[2]\ = DFFE(\pdone_out_vec[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pdone_out_vec[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pdone_out_vec[2]\);

\pdone_out_vec[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \pdone_out_vec[3]\ = DFFE(\pdone_out_vec[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pdone_out_vec[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pdone_out_vec[3]\);

\PDone_Out~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \PDone_Out~reg0\ = DFFE(\pdone_out_vec[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pdone_out_vec[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PDone_Out~reg0\);

\Pur_In~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_In,
	combout => \Pur_In~combout\);

\u_reg:0:u_reg_i|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[25]\ = DFFE(\Pur_In~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Pur_In~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[25]\);

\u_reg:1:u_reg_i|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[25]\ = DFFE(\u_reg:0:u_reg_i|dffs[25]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[25]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[25]\);

\u_reg:2:u_reg_i|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[25]\ = DFFE(\u_reg:1:u_reg_i|dffs[25]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[25]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[25]\);

\u_reg:3:u_reg_i|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[25]\ = DFFE(\u_reg:2:u_reg_i|dffs[25]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:2:u_reg_i|dffs[25]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[25]\);

\pbusy~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_pbusy,
	combout => \pbusy~combout\);

\u_reg:0:u_reg_i|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[26]\ = DFFE(\pbusy~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pbusy~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[26]\);

\u_reg:1:u_reg_i|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[26]\ = DFFE(\u_reg:0:u_reg_i|dffs[26]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[26]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[26]\);

\u_reg:2:u_reg_i|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[26]\ = DFFE(\u_reg:1:u_reg_i|dffs[26]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:1:u_reg_i|dffs[26]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[26]\);

\u_reg:3:u_reg_i|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[26]\ = DFFE(\u_reg:2:u_reg_i|dffs[26]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[26]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[26]\);

\fir_in[23]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(23),
	combout => \fir_in[23]~combout\);

\u_reg:0:u_reg_i|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[23]\ = DFFE(\fir_in[23]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[23]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[23]\);

\tlevel[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(15),
	combout => \tlevel[15]~combout\);

\tlevel_reg_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[15]\ = DFFE(\tlevel[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[15]\);

\fir_in[22]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(22),
	combout => \fir_in[22]~combout\);

\u_reg:0:u_reg_i|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[22]\ = DFFE(\fir_in[22]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[22]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[22]\);

\tlevel[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(13),
	combout => \tlevel[13]~combout\);

\tlevel_reg_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[13]\ = DFFE(\tlevel[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[13]\);

\tlevel[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(12),
	combout => \tlevel[12]~combout\);

\tlevel_reg_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[12]\ = DFFE(\tlevel[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[12]\);

\tlevel[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(11),
	combout => \tlevel[11]~combout\);

\tlevel_reg_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[11]\ = DFFE(\tlevel[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[11]\);

\tlevel[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(10),
	combout => \tlevel[10]~combout\);

\tlevel_reg_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[10]\ = DFFE(\tlevel[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[10]\);

\tlevel[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(9),
	combout => \tlevel[9]~combout\);

\tlevel_reg_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[9]\ = DFFE(\tlevel[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[9]\);

\tlevel[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(8),
	combout => \tlevel[8]~combout\);

\tlevel_reg_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[8]\ = DFFE(\tlevel[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[8]\);

\fir_in[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(15),
	combout => \fir_in[15]~combout\);

\u_reg:0:u_reg_i|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[15]\ = DFFE(\fir_in[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[15]\);

\tlevel[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(6),
	combout => \tlevel[6]~combout\);

\tlevel_reg_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[6]\ = DFFE(\tlevel[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[6]\);

\fir_in[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(13),
	combout => \fir_in[13]~combout\);

\u_reg:0:u_reg_i|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[13]\ = DFFE(\fir_in[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[13]\);

\fir_in[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(12),
	combout => \fir_in[12]~combout\);

\u_reg:0:u_reg_i|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[12]\ = DFFE(\fir_in[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[12]\);

\tlevel[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(3),
	combout => \tlevel[3]~combout\);

\tlevel_reg_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[3]\ = DFFE(\tlevel[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[3]\);

\tlevel[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(2),
	combout => \tlevel[2]~combout\);

\tlevel_reg_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[2]\ = DFFE(\tlevel[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[2]\);

\tlevel[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(1),
	combout => \tlevel[1]~combout\);

\tlevel_reg_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[1]\ = DFFE(\tlevel[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[1]\);

\tlevel[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(0),
	combout => \tlevel[0]~combout\);

\tlevel_reg_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_reg_ff|dffs[0]\ = DFFE(\tlevel[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \tlevel_reg_ff|dffs[0]\);

\tlevel_cmp|comparator|cmp_end|lcarry[0]~1058_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[0]\ = CARRY(\u_reg:0:u_reg_i|dffs[8]\ & !\tlevel_reg_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[8]\,
	datab => \tlevel_reg_ff|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[0]~1058\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[0]\);

\tlevel_cmp|comparator|cmp_end|lcarry[1]~1057_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[1]\ = CARRY(\u_reg:0:u_reg_i|dffs[9]\ & \tlevel_reg_ff|dffs[1]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[0]\ # !\u_reg:0:u_reg_i|dffs[9]\ & (\tlevel_reg_ff|dffs[1]\ # !\tlevel_cmp|comparator|cmp_end|lcarry[0]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[9]\,
	datab => \tlevel_reg_ff|dffs[1]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[1]~1057\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[1]\);

\tlevel_cmp|comparator|cmp_end|lcarry[2]~1056_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[2]\ = CARRY(\u_reg:0:u_reg_i|dffs[10]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[1]\ # !\tlevel_reg_ff|dffs[2]\) # !\u_reg:0:u_reg_i|dffs[10]\ & !\tlevel_reg_ff|dffs[2]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[10]\,
	datab => \tlevel_reg_ff|dffs[2]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[2]~1056\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[2]\);

\tlevel_cmp|comparator|cmp_end|lcarry[3]~1055_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[3]\ = CARRY(\u_reg:0:u_reg_i|dffs[11]\ & \tlevel_reg_ff|dffs[3]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[2]\ # !\u_reg:0:u_reg_i|dffs[11]\ & (\tlevel_reg_ff|dffs[3]\ # !\tlevel_cmp|comparator|cmp_end|lcarry[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[11]\,
	datab => \tlevel_reg_ff|dffs[3]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[3]~1055\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[3]\);

\tlevel_cmp|comparator|cmp_end|lcarry[4]~1054_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[4]\ = CARRY(\tlevel_reg_ff|dffs[4]\ & \u_reg:0:u_reg_i|dffs[12]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[3]\ # !\tlevel_reg_ff|dffs[4]\ & (\u_reg:0:u_reg_i|dffs[12]\ # !\tlevel_cmp|comparator|cmp_end|lcarry[3]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[4]\,
	datab => \u_reg:0:u_reg_i|dffs[12]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[4]~1054\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[4]\);

\tlevel_cmp|comparator|cmp_end|lcarry[5]~1053_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[5]\ = CARRY(\tlevel_reg_ff|dffs[5]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[4]\ # !\u_reg:0:u_reg_i|dffs[13]\) # !\tlevel_reg_ff|dffs[5]\ & !\u_reg:0:u_reg_i|dffs[13]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[4]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[5]\,
	datab => \u_reg:0:u_reg_i|dffs[13]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[5]~1053\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[5]\);

\tlevel_cmp|comparator|cmp_end|lcarry[6]~1052_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[6]\ = CARRY(\u_reg:0:u_reg_i|dffs[14]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[5]\ # !\tlevel_reg_ff|dffs[6]\) # !\u_reg:0:u_reg_i|dffs[14]\ & !\tlevel_reg_ff|dffs[6]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[14]\,
	datab => \tlevel_reg_ff|dffs[6]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[6]~1052\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[6]\);

\tlevel_cmp|comparator|cmp_end|lcarry[7]~1051_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[7]\ = CARRY(\tlevel_reg_ff|dffs[7]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[6]\ # !\u_reg:0:u_reg_i|dffs[15]\) # !\tlevel_reg_ff|dffs[7]\ & !\u_reg:0:u_reg_i|dffs[15]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[6]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[7]\,
	datab => \u_reg:0:u_reg_i|dffs[15]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[7]~1051\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[7]\);

\tlevel_cmp|comparator|cmp_end|lcarry[8]~1050_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[8]\ = CARRY(\u_reg:0:u_reg_i|dffs[16]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[7]\ # !\tlevel_reg_ff|dffs[8]\) # !\u_reg:0:u_reg_i|dffs[16]\ & !\tlevel_reg_ff|dffs[8]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[16]\,
	datab => \tlevel_reg_ff|dffs[8]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[8]~1050\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[8]\);

\tlevel_cmp|comparator|cmp_end|lcarry[9]~1049_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[9]\ = CARRY(\u_reg:0:u_reg_i|dffs[17]\ & \tlevel_reg_ff|dffs[9]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[8]\ # !\u_reg:0:u_reg_i|dffs[17]\ & (\tlevel_reg_ff|dffs[9]\ # !\tlevel_cmp|comparator|cmp_end|lcarry[8]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[17]\,
	datab => \tlevel_reg_ff|dffs[9]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[9]~1049\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[9]\);

\tlevel_cmp|comparator|cmp_end|lcarry[10]~1048_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[10]\ = CARRY(\u_reg:0:u_reg_i|dffs[18]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[9]\ # !\tlevel_reg_ff|dffs[10]\) # !\u_reg:0:u_reg_i|dffs[18]\ & !\tlevel_reg_ff|dffs[10]\ & 
-- !\tlevel_cmp|comparator|cmp_end|lcarry[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[18]\,
	datab => \tlevel_reg_ff|dffs[10]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[10]~1048\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[10]\);

\tlevel_cmp|comparator|cmp_end|lcarry[11]~1047_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[11]\ = CARRY(\u_reg:0:u_reg_i|dffs[19]\ & \tlevel_reg_ff|dffs[11]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[10]\ # !\u_reg:0:u_reg_i|dffs[19]\ & (\tlevel_reg_ff|dffs[11]\ # 
-- !\tlevel_cmp|comparator|cmp_end|lcarry[10]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[19]\,
	datab => \tlevel_reg_ff|dffs[11]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[11]~1047\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[11]\);

\tlevel_cmp|comparator|cmp_end|lcarry[12]~1046_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[12]\ = CARRY(\u_reg:0:u_reg_i|dffs[20]\ & (!\tlevel_cmp|comparator|cmp_end|lcarry[11]\ # !\tlevel_reg_ff|dffs[12]\) # !\u_reg:0:u_reg_i|dffs[20]\ & !\tlevel_reg_ff|dffs[12]\ & 
-- !\tlevel_cmp|comparator|cmp_end|lcarry[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[20]\,
	datab => \tlevel_reg_ff|dffs[12]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[12]~1046\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[12]\);

\tlevel_cmp|comparator|cmp_end|lcarry[13]~1045_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[13]\ = CARRY(\u_reg:0:u_reg_i|dffs[21]\ & \tlevel_reg_ff|dffs[13]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[12]\ # !\u_reg:0:u_reg_i|dffs[21]\ & (\tlevel_reg_ff|dffs[13]\ # 
-- !\tlevel_cmp|comparator|cmp_end|lcarry[12]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:0:u_reg_i|dffs[21]\,
	datab => \tlevel_reg_ff|dffs[13]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[13]~1045\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[13]\);

\tlevel_cmp|comparator|cmp_end|lcarry[14]~1044_I\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|lcarry[14]\ = CARRY(\tlevel_reg_ff|dffs[14]\ & \u_reg:0:u_reg_i|dffs[22]\ & !\tlevel_cmp|comparator|cmp_end|lcarry[13]\ # !\tlevel_reg_ff|dffs[14]\ & (\u_reg:0:u_reg_i|dffs[22]\ # 
-- !\tlevel_cmp|comparator|cmp_end|lcarry[13]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_reg_ff|dffs[14]\,
	datab => \u_reg:0:u_reg_i|dffs[22]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|lcarry[14]~1044\,
	cout => \tlevel_cmp|comparator|cmp_end|lcarry[14]\);

\tlevel_cmp|comparator|cmp_end|agb_out_node\ : apex20ke_lcell
-- Equation(s):
-- \tlevel_cmp|comparator|cmp_end|agb_out\ = \u_reg:0:u_reg_i|dffs[23]\ & \tlevel_cmp|comparator|cmp_end|lcarry[14]\ & \tlevel_reg_ff|dffs[15]\ # !\u_reg:0:u_reg_i|dffs[23]\ & (\tlevel_cmp|comparator|cmp_end|lcarry[14]\ # \tlevel_reg_ff|dffs[15]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F330",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \u_reg:0:u_reg_i|dffs[23]\,
	datad => \tlevel_reg_ff|dffs[15]\,
	cin => \tlevel_cmp|comparator|cmp_end|lcarry[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \tlevel_cmp|comparator|cmp_end|agb_out\);

\u_reg:0:u_reg_i|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[27]\ = DFFE(\tlevel_cmp|comparator|cmp_end|agb_out\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \tlevel_cmp|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[27]\);

\u_reg:1:u_reg_i|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[27]\ = DFFE(\u_reg:0:u_reg_i|dffs[27]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[27]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[27]\);

\u_reg:2:u_reg_i|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[27]\ = DFFE(\u_reg:1:u_reg_i|dffs[27]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[27]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[27]\);

\u_reg:3:u_reg_i|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[27]\ = DFFE(\u_reg:2:u_reg_i|dffs[27]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[27]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[27]\);

\tc_sel[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tc_sel(0),
	combout => \tc_sel[0]~combout\);

\tc_sel[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tc_sel(2),
	combout => \tc_sel[2]~combout\);

\tc_sel[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tc_sel(1),
	combout => \tc_sel[1]~combout\);

\pbusy_str_mux[1]~81_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_mux[1]~81\ = \tc_sel[3]~combout\ # \tc_sel[1]~combout\ # \tc_sel[0]~combout\ & \tc_sel[2]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \tc_sel[0]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[1]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str_mux[1]~81\);

\pbusy_str~710_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~710\ = \pbusy_str_cnt[1]\ & (pbusy_str & \pbusy_str_mux[1]~81\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[1]\,
	datac => pbusy_str,
	datad => \pbusy_str_mux[1]~81\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~710\);

\tc_sel[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tc_sel(3),
	combout => \tc_sel[3]~combout\);

\pbusy_str_mux[3]~82_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_mux[3]~82\ = \tc_sel[1]~combout\ & !\tc_sel[3]~combout\ & (\tc_sel[2]~combout\ $ \tc_sel[0]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0220",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[1]~combout\,
	datab => \tc_sel[3]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str_mux[3]~82\);

\pbusy_str~711_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~711\ = !\pbusy_str_cnt[3]\ & (pbusy_str & \pbusy_str_mux[3]~82\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[3]\,
	datac => pbusy_str,
	datad => \pbusy_str_mux[3]~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~711\);

\pbusy_str_cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[0]\ = DFFE(GLOBAL(pbusy_str) & !\pbusy_str_cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[0]~82\ = CARRY(\pbusy_str_cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[0]\,
	cout => \pbusy_str_cnt[0]~82\);

\pbusy_str_cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[2]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[2]\ $ !\pbusy_str_cnt[1]~97\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[2]~94\ = CARRY(\pbusy_str_cnt[2]\ & !\pbusy_str_cnt[1]~97\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[2]\,
	cin => \pbusy_str_cnt[1]~97\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[2]\,
	cout => \pbusy_str_cnt[2]~94\);

\pbusy_str_mux[2]~80_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_mux[2]~80\ = !\tc_sel[3]~combout\ & (\tc_sel[0]~combout\ & !\tc_sel[1]~combout\ & \tc_sel[2]~combout\ # !\tc_sel[0]~combout\ & \tc_sel[1]~combout\ & !\tc_sel[2]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0210",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[0]~combout\,
	datab => \tc_sel[3]~combout\,
	datac => \tc_sel[1]~combout\,
	datad => \tc_sel[2]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str_mux[2]~80\);

\pbusy_str~712_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~712\ = pbusy_str & !\pbusy_str_cnt[2]\ & \pbusy_str_mux[2]~80\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pbusy_str,
	datac => \pbusy_str_cnt[2]\,
	datad => \pbusy_str_mux[2]~80\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~712\);

\pbusy_str~714_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~714\ = \pbusy_str~713\ # \pbusy_str~710\ # \pbusy_str~711\ # \pbusy_str~712\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str~713\,
	datab => \pbusy_str~710\,
	datac => \pbusy_str~711\,
	datad => \pbusy_str~712\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~714\);

\pbusy_str_cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[3]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[3]\ $ \pbusy_str_cnt[2]~94\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[3]~100\ = CARRY(!\pbusy_str_cnt[2]~94\ # !\pbusy_str_cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[3]\,
	cin => \pbusy_str_cnt[2]~94\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[3]\,
	cout => \pbusy_str_cnt[3]~100\);

\pbusy_str_cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[4]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[4]\ $ !\pbusy_str_cnt[3]~100\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[4]~103\ = CARRY(\pbusy_str_cnt[4]\ & !\pbusy_str_cnt[3]~100\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[4]\,
	cin => \pbusy_str_cnt[3]~100\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[4]\,
	cout => \pbusy_str_cnt[4]~103\);

\pbusy_str_cnt[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[5]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[5]\ $ (\pbusy_str_cnt[4]~103\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[5]~88\ = CARRY(!\pbusy_str_cnt[4]~103\ # !\pbusy_str_cnt[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[5]\,
	cin => \pbusy_str_cnt[4]~103\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[5]\,
	cout => \pbusy_str_cnt[5]~88\);

\pbusy_str_cnt[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[6]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[6]\ $ (!\pbusy_str_cnt[5]~88\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[6]~91\ = CARRY(\pbusy_str_cnt[6]\ & (!\pbusy_str_cnt[5]~88\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[6]\,
	cin => \pbusy_str_cnt[5]~88\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[6]\,
	cout => \pbusy_str_cnt[6]~91\);

\pbusy_str_cnt[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[7]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[7]\ $ \pbusy_str_cnt[6]~91\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \pbusy_str_cnt[7]~79\ = CARRY(!\pbusy_str_cnt[6]~91\ # !\pbusy_str_cnt[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str_cnt[7]\,
	cin => \pbusy_str_cnt[6]~91\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[7]\,
	cout => \pbusy_str_cnt[7]~79\);

\pbusy_str_cnt[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_cnt[8]\ = DFFE(GLOBAL(pbusy_str) & \pbusy_str_cnt[7]~79\ $ !\pbusy_str_cnt[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pbusy_str_cnt[8]\,
	cin => \pbusy_str_cnt[7]~79\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => ALT_INV_pbusy_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_str_cnt[8]\);

\pbusy_str~717_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~717\ = \pbusy_str_cnt[8]\ & !\tc_sel[3]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \pbusy_str_cnt[8]\,
	datad => \tc_sel[3]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~717\);

\pbusy_str~718_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~718\ = pbusy_str & (\pbusy_str~717\ # \tc_sel[2]~combout\ & \pbusy_str_cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA80",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pbusy_str,
	datab => \tc_sel[2]~combout\,
	datac => \pbusy_str_cnt[0]\,
	datad => \pbusy_str~717\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~718\);

\pbusy_gen_vec[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_gen_vec[0]\ = DFFE(\tlevel_cmp|comparator|cmp_end|agb_out\ # \u_reg:3:u_reg_i|dffs[26]\ # \pbusy~combout\ # \u_reg:3:u_reg_i|dffs[27]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tlevel_cmp|comparator|cmp_end|agb_out\,
	datab => \u_reg:3:u_reg_i|dffs[26]\,
	datac => \pbusy~combout\,
	datad => \u_reg:3:u_reg_i|dffs[27]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_gen_vec[0]\);

\pbusy_gen_vec[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_gen_vec[1]\ = DFFE(\pbusy_gen_vec[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pbusy_gen_vec[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_gen_vec[1]\);

\pbusy_str~719_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~719\ = \pbusy_gen_vec[1]\ & (!\pbusy_gen_vec[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_gen_vec[1]\,
	datad => \pbusy_gen_vec[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~719\);

\pbusy_str_mux[4]~83_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_mux[4]~83\ = !\tc_sel[3]~combout\ & \tc_sel[2]~combout\ & (\tc_sel[1]~combout\ $ !\tc_sel[0]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[1]~combout\,
	datab => \tc_sel[3]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str_mux[4]~83\);

\pbusy_str~720_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~720\ = \pbusy_str~719\ # !\pbusy_str_cnt[4]\ & pbusy_str & \pbusy_str_mux[4]~83\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DCCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[4]\,
	datab => \pbusy_str~719\,
	datac => pbusy_str,
	datad => \pbusy_str_mux[4]~83\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~720\);

\pbusy_str~715_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~715\ = \pbusy_str_cnt[7]\ & !\tc_sel[2]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \pbusy_str_cnt[7]\,
	datad => \tc_sel[2]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~715\);

\pbusy_str~716_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~716\ = pbusy_str & (\pbusy_str~715\ # \pbusy_str_cnt[3]\ & !\pbusy_str_mux[3]~82\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0E0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str_cnt[3]\,
	datab => \pbusy_str~715\,
	datac => pbusy_str,
	datad => \pbusy_str_mux[3]~82\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~716\);

\pbusy_str~721_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~721\ = \pbusy_str~718\ # \pbusy_str~720\ # \pbusy_str~716\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \pbusy_str~718\,
	datac => \pbusy_str~720\,
	datad => \pbusy_str~716\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~721\);

\clk_cnt_max[1]~224_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[1]~224\ = !\tc_sel[3]~combout\ & \tc_sel[2]~combout\ & \tc_sel[1]~combout\ & !\tc_sel[0]~combout\
-- \clk_cnt_max[1]~231\ = !\tc_sel[3]~combout\ & \tc_sel[2]~combout\ & \tc_sel[1]~combout\ & !\tc_sel[0]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \tc_sel[2]~combout\,
	datac => \tc_sel[1]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[1]~224\,
	cascout => \clk_cnt_max[1]~231\);

\pbusy_str~726_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~726\ = (pbusy_str & !\pbusy_str_cnt[6]\) & CASCADE(\clk_cnt_max[1]~231\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pbusy_str,
	datad => \pbusy_str_cnt[6]\,
	cascin => \clk_cnt_max[1]~231\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~726\);

\pbusy_str~703_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~703\ = \pbusy_str~726\ # pbusy_str & \pbusy_str_cnt[2]\ & !\pbusy_str_mux[2]~80\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pbusy_str,
	datab => \pbusy_str_cnt[2]\,
	datac => \pbusy_str~726\,
	datad => \pbusy_str_mux[2]~80\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~703\);

\pbusy_str~698_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~698\ = \tc_sel[1]~combout\ & !\pbusy_str_cnt[0]\ & !\pbusy_str_cnt[7]\ & \tc_sel[0]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[1]~combout\,
	datab => \pbusy_str_cnt[0]\,
	datac => \pbusy_str_cnt[7]\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~698\);

\pbusy_str~699_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~699\ = pbusy_str & !\pbusy_str_cnt[8]\ & (\tc_sel[3]~combout\ # \pbusy_str~698\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2220",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pbusy_str,
	datab => \pbusy_str_cnt[8]\,
	datac => \tc_sel[3]~combout\,
	datad => \pbusy_str~698\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~699\);

\pbusy_str_mux[5]~79_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str_mux[5]~79\ = \tc_sel[3]~combout\ # \tc_sel[0]~combout\ & \tc_sel[2]~combout\ & !\tc_sel[1]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[0]~combout\,
	datab => \tc_sel[3]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[1]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str_mux[5]~79\);

\pbusy_str~700_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~700\ = pbusy_str & !\pbusy_str_mux[5]~79\ & \pbusy_str_cnt[5]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pbusy_str,
	datac => \pbusy_str_mux[5]~79\,
	datad => \pbusy_str_cnt[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~700\);

\pbusy_str~704_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_str~704\ = \pbusy_str~701\ # \pbusy_str~703\ # \pbusy_str~699\ # \pbusy_str~700\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str~701\,
	datab => \pbusy_str~703\,
	datac => \pbusy_str~699\,
	datad => \pbusy_str~700\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_str~704\);

\pbusy_str~I\ : apex20ke_lcell
-- Equation(s):
-- pbusy_str = DFFE(\pbusy_str~709\ # \pbusy_str~714\ # \pbusy_str~721\ # \pbusy_str~704\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_str~709\,
	datab => \pbusy_str~714\,
	datac => \pbusy_str~721\,
	datad => \pbusy_str~704\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pbusy_str);

\pbusy_out_comb~33_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_out_comb~33\ = \pbusy_gen_vec[1]\ # pbusy_str # \pbusy_gen_vec[0]\ # \tlevel_cmp|comparator|cmp_end|agb_out\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_gen_vec[1]\,
	datab => pbusy_str,
	datac => \pbusy_gen_vec[0]\,
	datad => \tlevel_cmp|comparator|cmp_end|agb_out\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_out_comb~33\);

\pbusy_out~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_out~reg0\ = DFFE(\u_reg:3:u_reg_i|dffs[26]\ # \u_reg:3:u_reg_i|dffs[27]\ # \pbusy~combout\ # \pbusy_out_comb~33\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[26]\,
	datab => \u_reg:3:u_reg_i|dffs[27]\,
	datac => \pbusy~combout\,
	datad => \pbusy_out_comb~33\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pbusy_out~reg0\);

\offset[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(8),
	combout => \offset[8]~combout\);

\PS_SEL[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PS_SEL(0),
	combout => \PS_SEL[0]~combout\);

\PS_SEL_REG_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \PS_SEL_REG_FF|dffs[0]\ = DFFE(\PS_SEL[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \PS_SEL[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PS_SEL_REG_FF|dffs[0]\);

\PS_SEL[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PS_SEL(1),
	combout => \PS_SEL[1]~combout\);

\PS_SEL_REG_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \PS_SEL_REG_FF|dffs[1]\ = DFFE(\PS_SEL[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \PS_SEL[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \PS_SEL_REG_FF|dffs[1]\);

\pbusy_out_comb~2_I\ : apex20ke_lcell
-- Equation(s):
-- \pbusy_out_comb~2\ = \pbusy_out_comb~33\ # \u_reg:3:u_reg_i|dffs[27]\ # \u_reg:3:u_reg_i|dffs[26]\ # \pbusy~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_out_comb~33\,
	datab => \u_reg:3:u_reg_i|dffs[27]\,
	datac => \u_reg:3:u_reg_i|dffs[26]\,
	datad => \pbusy~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \pbusy_out_comb~2\);

\ps_mux[23]~2942_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[23]~2942\ = \fir_acc_ff|dffs[39]\ & !\pbusy_out_comb~2\ & (\PS_SEL_REG_FF|dffs[0]\ # \PS_SEL_REG_FF|dffs[1]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[39]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[23]~2942\);

\u_reg:1:u_reg_i|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[22]\ = DFFE(\u_reg:0:u_reg_i|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[22]\);

\u_reg:2:u_reg_i|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[22]\ = DFFE(\u_reg:1:u_reg_i|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[22]\);

\u_reg:3:u_reg_i|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[22]\ = DFFE(\u_reg:2:u_reg_i|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[22]\);

\fir_in[21]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(21),
	combout => \fir_in[21]~combout\);

\u_reg:0:u_reg_i|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[21]\ = DFFE(\fir_in[21]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[21]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[21]\);

\u_reg:1:u_reg_i|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[21]\ = DFFE(\u_reg:0:u_reg_i|dffs[21]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[21]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[21]\);

\u_reg:2:u_reg_i|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[21]\ = DFFE(\u_reg:1:u_reg_i|dffs[21]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[21]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[21]\);

\u_reg:3:u_reg_i|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[21]\ = DFFE(\u_reg:2:u_reg_i|dffs[21]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[21]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[21]\);

\fir_in_del1_ff|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[21]\ = DFFE(\u_reg:3:u_reg_i|dffs[21]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[21]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[21]\);

\fir_in[20]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(20),
	combout => \fir_in[20]~combout\);

\u_reg:0:u_reg_i|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[20]\ = DFFE(\fir_in[20]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[20]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[20]\);

\u_reg:1:u_reg_i|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[20]\ = DFFE(\u_reg:0:u_reg_i|dffs[20]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[20]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[20]\);

\u_reg:2:u_reg_i|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[20]\ = DFFE(\u_reg:1:u_reg_i|dffs[20]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[20]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[20]\);

\u_reg:3:u_reg_i|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[20]\ = DFFE(\u_reg:2:u_reg_i|dffs[20]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[20]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[20]\);

\fir_in_del1_ff|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[20]\ = DFFE(\u_reg:3:u_reg_i|dffs[20]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[20]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[20]\);

\fir_in[19]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(19),
	combout => \fir_in[19]~combout\);

\u_reg:0:u_reg_i|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[19]\ = DFFE(\fir_in[19]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[19]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[19]\);

\u_reg:1:u_reg_i|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[19]\ = DFFE(\u_reg:0:u_reg_i|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[19]\);

\u_reg:2:u_reg_i|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[19]\ = DFFE(\u_reg:1:u_reg_i|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:1:u_reg_i|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[19]\);

\u_reg:3:u_reg_i|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[19]\ = DFFE(\u_reg:2:u_reg_i|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[19]\);

\fir_in[18]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(18),
	combout => \fir_in[18]~combout\);

\u_reg:0:u_reg_i|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[18]\ = DFFE(\fir_in[18]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[18]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[18]\);

\u_reg:1:u_reg_i|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[18]\ = DFFE(\u_reg:0:u_reg_i|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[18]\);

\u_reg:2:u_reg_i|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[18]\ = DFFE(\u_reg:1:u_reg_i|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[18]\);

\u_reg:3:u_reg_i|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[18]\ = DFFE(\u_reg:2:u_reg_i|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[18]\);

\fir_in[17]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(17),
	combout => \fir_in[17]~combout\);

\u_reg:0:u_reg_i|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[17]\ = DFFE(\fir_in[17]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[17]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[17]\);

\u_reg:1:u_reg_i|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[17]\ = DFFE(\u_reg:0:u_reg_i|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[17]\);

\u_reg:2:u_reg_i|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[17]\ = DFFE(\u_reg:1:u_reg_i|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[17]\);

\u_reg:3:u_reg_i|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[17]\ = DFFE(\u_reg:2:u_reg_i|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[17]\);

\fir_in_del1_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[17]\ = DFFE(\u_reg:3:u_reg_i|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[17]\);

\fir_in[16]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(16),
	combout => \fir_in[16]~combout\);

\u_reg:0:u_reg_i|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[16]\ = DFFE(\fir_in[16]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[16]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[16]\);

\u_reg:1:u_reg_i|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[16]\ = DFFE(\u_reg:0:u_reg_i|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:0:u_reg_i|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[16]\);

\u_reg:2:u_reg_i|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[16]\ = DFFE(\u_reg:1:u_reg_i|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[16]\);

\u_reg:3:u_reg_i|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[16]\ = DFFE(\u_reg:2:u_reg_i|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[16]\);

\fir_in_del1_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[16]\ = DFFE(\u_reg:3:u_reg_i|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[16]\);

\u_reg:1:u_reg_i|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[15]\ = DFFE(\u_reg:0:u_reg_i|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[15]\);

\u_reg:2:u_reg_i|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[15]\ = DFFE(\u_reg:1:u_reg_i|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[15]\);

\u_reg:3:u_reg_i|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[15]\ = DFFE(\u_reg:2:u_reg_i|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[15]\);

\fir_in[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(14),
	combout => \fir_in[14]~combout\);

\u_reg:0:u_reg_i|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[14]\ = DFFE(\fir_in[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[14]\);

\u_reg:1:u_reg_i|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[14]\ = DFFE(\u_reg:0:u_reg_i|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[14]\);

\u_reg:2:u_reg_i|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[14]\ = DFFE(\u_reg:1:u_reg_i|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[14]\);

\u_reg:3:u_reg_i|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[14]\ = DFFE(\u_reg:2:u_reg_i|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[14]\);

\fir_in_del1_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[14]\ = DFFE(\u_reg:3:u_reg_i|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[14]\);

\u_reg:1:u_reg_i|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[13]\ = DFFE(\u_reg:0:u_reg_i|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[13]\);

\u_reg:2:u_reg_i|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[13]\ = DFFE(\u_reg:1:u_reg_i|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[13]\);

\u_reg:3:u_reg_i|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[13]\ = DFFE(\u_reg:2:u_reg_i|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[13]\);

\fir_in_del1_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[13]\ = DFFE(\u_reg:3:u_reg_i|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[13]\);

\u_reg:1:u_reg_i|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[12]\ = DFFE(\u_reg:0:u_reg_i|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[12]\);

\u_reg:2:u_reg_i|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[12]\ = DFFE(\u_reg:1:u_reg_i|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[12]\);

\u_reg:3:u_reg_i|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[12]\ = DFFE(\u_reg:2:u_reg_i|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[12]\);

\fir_in_del1_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[12]\ = DFFE(\u_reg:3:u_reg_i|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[12]\);

\fir_in[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(11),
	combout => \fir_in[11]~combout\);

\u_reg:0:u_reg_i|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[11]\ = DFFE(\fir_in[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[11]\);

\u_reg:1:u_reg_i|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[11]\ = DFFE(\u_reg:0:u_reg_i|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[11]\);

\u_reg:2:u_reg_i|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[11]\ = DFFE(\u_reg:1:u_reg_i|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[11]\);

\u_reg:3:u_reg_i|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[11]\ = DFFE(\u_reg:2:u_reg_i|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[11]\);

\fir_in_del1_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[11]\ = DFFE(\u_reg:3:u_reg_i|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[11]\);

\fir_in[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(10),
	combout => \fir_in[10]~combout\);

\u_reg:0:u_reg_i|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[10]\ = DFFE(\fir_in[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[10]\);

\u_reg:1:u_reg_i|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[10]\ = DFFE(\u_reg:0:u_reg_i|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[10]\);

\u_reg:2:u_reg_i|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[10]\ = DFFE(\u_reg:1:u_reg_i|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[10]\);

\u_reg:3:u_reg_i|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[10]\ = DFFE(\u_reg:2:u_reg_i|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[10]\);

\fir_in_del1_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[10]\ = DFFE(\u_reg:3:u_reg_i|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[10]\);

\fir_in[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(9),
	combout => \fir_in[9]~combout\);

\u_reg:0:u_reg_i|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[9]\ = DFFE(\fir_in[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[9]\);

\u_reg:1:u_reg_i|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[9]\ = DFFE(\u_reg:0:u_reg_i|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[9]\);

\u_reg:2:u_reg_i|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[9]\ = DFFE(\u_reg:1:u_reg_i|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[9]\);

\u_reg:3:u_reg_i|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[9]\ = DFFE(\u_reg:2:u_reg_i|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[9]\);

\fir_in_del1_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[9]\ = DFFE(\u_reg:3:u_reg_i|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[9]\);

\fir_in[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(8),
	combout => \fir_in[8]~combout\);

\u_reg:0:u_reg_i|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[8]\ = DFFE(\fir_in[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[8]\);

\u_reg:1:u_reg_i|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[8]\ = DFFE(\u_reg:0:u_reg_i|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[8]\);

\u_reg:2:u_reg_i|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[8]\ = DFFE(\u_reg:1:u_reg_i|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[8]\);

\u_reg:3:u_reg_i|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[8]\ = DFFE(\u_reg:2:u_reg_i|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[8]\);

\fir_in_del1_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[8]\ = DFFE(\u_reg:3:u_reg_i|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[8]\);

\fir_in[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(7),
	combout => \fir_in[7]~combout\);

\u_reg:0:u_reg_i|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[7]\ = DFFE(\fir_in[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[7]\);

\u_reg:1:u_reg_i|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[7]\ = DFFE(\u_reg:0:u_reg_i|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[7]\);

\u_reg:2:u_reg_i|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[7]\ = DFFE(\u_reg:1:u_reg_i|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[7]\);

\u_reg:3:u_reg_i|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[7]\ = DFFE(\u_reg:2:u_reg_i|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[7]\);

\fir_in[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(6),
	combout => \fir_in[6]~combout\);

\u_reg:0:u_reg_i|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[6]\ = DFFE(\fir_in[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[6]\);

\u_reg:1:u_reg_i|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[6]\ = DFFE(\u_reg:0:u_reg_i|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[6]\);

\u_reg:2:u_reg_i|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[6]\ = DFFE(\u_reg:1:u_reg_i|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[6]\);

\u_reg:3:u_reg_i|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[6]\ = DFFE(\u_reg:2:u_reg_i|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:2:u_reg_i|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[6]\);

\fir_in_del1_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[6]\ = DFFE(\u_reg:3:u_reg_i|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[6]\);

\fir_in[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(5),
	combout => \fir_in[5]~combout\);

\u_reg:0:u_reg_i|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[5]\ = DFFE(\fir_in[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[5]\);

\u_reg:1:u_reg_i|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[5]\ = DFFE(\u_reg:0:u_reg_i|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[5]\);

\u_reg:2:u_reg_i|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[5]\ = DFFE(\u_reg:1:u_reg_i|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:1:u_reg_i|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[5]\);

\u_reg:3:u_reg_i|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[5]\ = DFFE(\u_reg:2:u_reg_i|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[5]\);

\fir_in_del1_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[5]\ = DFFE(\u_reg:3:u_reg_i|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[5]\);

\fir_in[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(4),
	combout => \fir_in[4]~combout\);

\u_reg:0:u_reg_i|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[4]\ = DFFE(\fir_in[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[4]\);

\u_reg:1:u_reg_i|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[4]\ = DFFE(\u_reg:0:u_reg_i|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:0:u_reg_i|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[4]\);

\u_reg:2:u_reg_i|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[4]\ = DFFE(\u_reg:1:u_reg_i|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[4]\);

\u_reg:3:u_reg_i|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[4]\ = DFFE(\u_reg:2:u_reg_i|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[4]\);

\fir_in_del1_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[4]\ = DFFE(\u_reg:3:u_reg_i|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[4]\);

\fir_in[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(3),
	combout => \fir_in[3]~combout\);

\u_reg:0:u_reg_i|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[3]\ = DFFE(\fir_in[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[3]\);

\u_reg:1:u_reg_i|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[3]\ = DFFE(\u_reg:0:u_reg_i|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[3]\);

\u_reg:2:u_reg_i|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[3]\ = DFFE(\u_reg:1:u_reg_i|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[3]\);

\u_reg:3:u_reg_i|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[3]\ = DFFE(\u_reg:2:u_reg_i|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:2:u_reg_i|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[3]\);

\fir_in_del1_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[3]\ = DFFE(\u_reg:3:u_reg_i|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[3]\);

\fir_in[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(2),
	combout => \fir_in[2]~combout\);

\u_reg:0:u_reg_i|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[2]\ = DFFE(\fir_in[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[2]\);

\u_reg:1:u_reg_i|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[2]\ = DFFE(\u_reg:0:u_reg_i|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[2]\);

\u_reg:2:u_reg_i|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[2]\ = DFFE(\u_reg:1:u_reg_i|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[2]\);

\u_reg:3:u_reg_i|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[2]\ = DFFE(\u_reg:2:u_reg_i|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[2]\);

\fir_in_del1_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[2]\ = DFFE(\u_reg:3:u_reg_i|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[2]\);

\fir_in[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(1),
	combout => \fir_in[1]~combout\);

\u_reg:0:u_reg_i|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[1]\ = DFFE(\fir_in[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[1]\);

\u_reg:1:u_reg_i|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[1]\ = DFFE(\u_reg:0:u_reg_i|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[1]\);

\u_reg:2:u_reg_i|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[1]\ = DFFE(\u_reg:1:u_reg_i|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[1]\);

\u_reg:3:u_reg_i|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[1]\ = DFFE(\u_reg:2:u_reg_i|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[1]\);

\fir_in_del1_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[1]\ = DFFE(\u_reg:3:u_reg_i|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[1]\);

\fir_in[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(0),
	combout => \fir_in[0]~combout\);

\u_reg:0:u_reg_i|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:0:u_reg_i|dffs[0]\ = DFFE(\fir_in[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_in[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:0:u_reg_i|dffs[0]\);

\u_reg:1:u_reg_i|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[0]\ = DFFE(\u_reg:0:u_reg_i|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[0]\);

\u_reg:2:u_reg_i|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[0]\ = DFFE(\u_reg:1:u_reg_i|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[0]\);

\u_reg:3:u_reg_i|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[0]\ = DFFE(\u_reg:2:u_reg_i|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[0]\);

\fir_in_del1_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[0]\ = DFFE(\u_reg:3:u_reg_i|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \u_reg:3:u_reg_i|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[0]\);

\fir_diff_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[0]\ = DFFE(\u_reg:3:u_reg_i|dffs[0]\ $ \fir_in_del1_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[0]~220\ = CARRY(\u_reg:3:u_reg_i|dffs[0]\ # !\fir_in_del1_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[0]\,
	datab => \fir_in_del1_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[0]\,
	cout => \fir_diff_ff|dffs[0]~220\);

\fir_diff_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[1]\ = DFFE(\u_reg:3:u_reg_i|dffs[1]\ $ \fir_in_del1_ff|dffs[1]\ $ !\fir_diff_ff|dffs[0]~220\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[1]~217\ = CARRY(\u_reg:3:u_reg_i|dffs[1]\ & \fir_in_del1_ff|dffs[1]\ & !\fir_diff_ff|dffs[0]~220\ # !\u_reg:3:u_reg_i|dffs[1]\ & (\fir_in_del1_ff|dffs[1]\ # !\fir_diff_ff|dffs[0]~220\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[1]\,
	datab => \fir_in_del1_ff|dffs[1]\,
	cin => \fir_diff_ff|dffs[0]~220\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[1]\,
	cout => \fir_diff_ff|dffs[1]~217\);

\fir_diff_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[2]\ = DFFE(\u_reg:3:u_reg_i|dffs[2]\ $ \fir_in_del1_ff|dffs[2]\ $ \fir_diff_ff|dffs[1]~217\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[2]~214\ = CARRY(\u_reg:3:u_reg_i|dffs[2]\ & (!\fir_diff_ff|dffs[1]~217\ # !\fir_in_del1_ff|dffs[2]\) # !\u_reg:3:u_reg_i|dffs[2]\ & !\fir_in_del1_ff|dffs[2]\ & !\fir_diff_ff|dffs[1]~217\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[2]\,
	datab => \fir_in_del1_ff|dffs[2]\,
	cin => \fir_diff_ff|dffs[1]~217\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[2]\,
	cout => \fir_diff_ff|dffs[2]~214\);

\fir_diff_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[3]\ = DFFE(\u_reg:3:u_reg_i|dffs[3]\ $ \fir_in_del1_ff|dffs[3]\ $ !\fir_diff_ff|dffs[2]~214\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[3]~211\ = CARRY(\u_reg:3:u_reg_i|dffs[3]\ & \fir_in_del1_ff|dffs[3]\ & !\fir_diff_ff|dffs[2]~214\ # !\u_reg:3:u_reg_i|dffs[3]\ & (\fir_in_del1_ff|dffs[3]\ # !\fir_diff_ff|dffs[2]~214\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[3]\,
	datab => \fir_in_del1_ff|dffs[3]\,
	cin => \fir_diff_ff|dffs[2]~214\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[3]\,
	cout => \fir_diff_ff|dffs[3]~211\);

\fir_diff_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[4]\ = DFFE(\u_reg:3:u_reg_i|dffs[4]\ $ \fir_in_del1_ff|dffs[4]\ $ \fir_diff_ff|dffs[3]~211\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[4]~208\ = CARRY(\u_reg:3:u_reg_i|dffs[4]\ & (!\fir_diff_ff|dffs[3]~211\ # !\fir_in_del1_ff|dffs[4]\) # !\u_reg:3:u_reg_i|dffs[4]\ & !\fir_in_del1_ff|dffs[4]\ & !\fir_diff_ff|dffs[3]~211\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[4]\,
	datab => \fir_in_del1_ff|dffs[4]\,
	cin => \fir_diff_ff|dffs[3]~211\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[4]\,
	cout => \fir_diff_ff|dffs[4]~208\);

\fir_diff_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[5]\ = DFFE(\u_reg:3:u_reg_i|dffs[5]\ $ \fir_in_del1_ff|dffs[5]\ $ !\fir_diff_ff|dffs[4]~208\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[5]~205\ = CARRY(\u_reg:3:u_reg_i|dffs[5]\ & \fir_in_del1_ff|dffs[5]\ & !\fir_diff_ff|dffs[4]~208\ # !\u_reg:3:u_reg_i|dffs[5]\ & (\fir_in_del1_ff|dffs[5]\ # !\fir_diff_ff|dffs[4]~208\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[5]\,
	datab => \fir_in_del1_ff|dffs[5]\,
	cin => \fir_diff_ff|dffs[4]~208\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[5]\,
	cout => \fir_diff_ff|dffs[5]~205\);

\fir_diff_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[6]\ = DFFE(\u_reg:3:u_reg_i|dffs[6]\ $ \fir_in_del1_ff|dffs[6]\ $ \fir_diff_ff|dffs[5]~205\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[6]~199\ = CARRY(\u_reg:3:u_reg_i|dffs[6]\ & (!\fir_diff_ff|dffs[5]~205\ # !\fir_in_del1_ff|dffs[6]\) # !\u_reg:3:u_reg_i|dffs[6]\ & !\fir_in_del1_ff|dffs[6]\ & !\fir_diff_ff|dffs[5]~205\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[6]\,
	datab => \fir_in_del1_ff|dffs[6]\,
	cin => \fir_diff_ff|dffs[5]~205\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[6]\,
	cout => \fir_diff_ff|dffs[6]~199\);

\fir_diff_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[7]\ = DFFE(\fir_in_del1_ff|dffs[7]\ $ \u_reg:3:u_reg_i|dffs[7]\ $ !\fir_diff_ff|dffs[6]~199\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[7]~202\ = CARRY(\fir_in_del1_ff|dffs[7]\ & (!\fir_diff_ff|dffs[6]~199\ # !\u_reg:3:u_reg_i|dffs[7]\) # !\fir_in_del1_ff|dffs[7]\ & !\u_reg:3:u_reg_i|dffs[7]\ & !\fir_diff_ff|dffs[6]~199\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_in_del1_ff|dffs[7]\,
	datab => \u_reg:3:u_reg_i|dffs[7]\,
	cin => \fir_diff_ff|dffs[6]~199\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[7]\,
	cout => \fir_diff_ff|dffs[7]~202\);

\fir_diff_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[8]\ = DFFE(\u_reg:3:u_reg_i|dffs[8]\ $ \fir_in_del1_ff|dffs[8]\ $ \fir_diff_ff|dffs[7]~202\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[8]~151\ = CARRY(\u_reg:3:u_reg_i|dffs[8]\ & (!\fir_diff_ff|dffs[7]~202\ # !\fir_in_del1_ff|dffs[8]\) # !\u_reg:3:u_reg_i|dffs[8]\ & !\fir_in_del1_ff|dffs[8]\ & !\fir_diff_ff|dffs[7]~202\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[8]\,
	datab => \fir_in_del1_ff|dffs[8]\,
	cin => \fir_diff_ff|dffs[7]~202\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[8]\,
	cout => \fir_diff_ff|dffs[8]~151\);

\fir_diff_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[9]\ = DFFE(\u_reg:3:u_reg_i|dffs[9]\ $ \fir_in_del1_ff|dffs[9]\ $ !\fir_diff_ff|dffs[8]~151\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[9]~154\ = CARRY(\u_reg:3:u_reg_i|dffs[9]\ & \fir_in_del1_ff|dffs[9]\ & !\fir_diff_ff|dffs[8]~151\ # !\u_reg:3:u_reg_i|dffs[9]\ & (\fir_in_del1_ff|dffs[9]\ # !\fir_diff_ff|dffs[8]~151\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[9]\,
	datab => \fir_in_del1_ff|dffs[9]\,
	cin => \fir_diff_ff|dffs[8]~151\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[9]\,
	cout => \fir_diff_ff|dffs[9]~154\);

\fir_diff_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[10]\ = DFFE(\u_reg:3:u_reg_i|dffs[10]\ $ \fir_in_del1_ff|dffs[10]\ $ \fir_diff_ff|dffs[9]~154\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[10]~157\ = CARRY(\u_reg:3:u_reg_i|dffs[10]\ & (!\fir_diff_ff|dffs[9]~154\ # !\fir_in_del1_ff|dffs[10]\) # !\u_reg:3:u_reg_i|dffs[10]\ & !\fir_in_del1_ff|dffs[10]\ & !\fir_diff_ff|dffs[9]~154\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[10]\,
	datab => \fir_in_del1_ff|dffs[10]\,
	cin => \fir_diff_ff|dffs[9]~154\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[10]\,
	cout => \fir_diff_ff|dffs[10]~157\);

\fir_diff_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[11]\ = DFFE(\u_reg:3:u_reg_i|dffs[11]\ $ \fir_in_del1_ff|dffs[11]\ $ !\fir_diff_ff|dffs[10]~157\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[11]~160\ = CARRY(\u_reg:3:u_reg_i|dffs[11]\ & \fir_in_del1_ff|dffs[11]\ & !\fir_diff_ff|dffs[10]~157\ # !\u_reg:3:u_reg_i|dffs[11]\ & (\fir_in_del1_ff|dffs[11]\ # !\fir_diff_ff|dffs[10]~157\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[11]\,
	datab => \fir_in_del1_ff|dffs[11]\,
	cin => \fir_diff_ff|dffs[10]~157\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[11]\,
	cout => \fir_diff_ff|dffs[11]~160\);

\fir_diff_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[12]\ = DFFE(\u_reg:3:u_reg_i|dffs[12]\ $ \fir_in_del1_ff|dffs[12]\ $ \fir_diff_ff|dffs[11]~160\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[12]~163\ = CARRY(\u_reg:3:u_reg_i|dffs[12]\ & (!\fir_diff_ff|dffs[11]~160\ # !\fir_in_del1_ff|dffs[12]\) # !\u_reg:3:u_reg_i|dffs[12]\ & !\fir_in_del1_ff|dffs[12]\ & !\fir_diff_ff|dffs[11]~160\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[12]\,
	datab => \fir_in_del1_ff|dffs[12]\,
	cin => \fir_diff_ff|dffs[11]~160\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[12]\,
	cout => \fir_diff_ff|dffs[12]~163\);

\fir_diff_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[13]\ = DFFE(\u_reg:3:u_reg_i|dffs[13]\ $ \fir_in_del1_ff|dffs[13]\ $ !\fir_diff_ff|dffs[12]~163\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[13]~166\ = CARRY(\u_reg:3:u_reg_i|dffs[13]\ & \fir_in_del1_ff|dffs[13]\ & !\fir_diff_ff|dffs[12]~163\ # !\u_reg:3:u_reg_i|dffs[13]\ & (\fir_in_del1_ff|dffs[13]\ # !\fir_diff_ff|dffs[12]~163\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[13]\,
	datab => \fir_in_del1_ff|dffs[13]\,
	cin => \fir_diff_ff|dffs[12]~163\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[13]\,
	cout => \fir_diff_ff|dffs[13]~166\);

\fir_diff_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[14]\ = DFFE(\u_reg:3:u_reg_i|dffs[14]\ $ \fir_in_del1_ff|dffs[14]\ $ \fir_diff_ff|dffs[13]~166\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[14]~169\ = CARRY(\u_reg:3:u_reg_i|dffs[14]\ & (!\fir_diff_ff|dffs[13]~166\ # !\fir_in_del1_ff|dffs[14]\) # !\u_reg:3:u_reg_i|dffs[14]\ & !\fir_in_del1_ff|dffs[14]\ & !\fir_diff_ff|dffs[13]~166\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[14]\,
	datab => \fir_in_del1_ff|dffs[14]\,
	cin => \fir_diff_ff|dffs[13]~166\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[14]\,
	cout => \fir_diff_ff|dffs[14]~169\);

\fir_diff_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[15]\ = DFFE(\fir_in_del1_ff|dffs[15]\ $ \u_reg:3:u_reg_i|dffs[15]\ $ !\fir_diff_ff|dffs[14]~169\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[15]~172\ = CARRY(\fir_in_del1_ff|dffs[15]\ & (!\fir_diff_ff|dffs[14]~169\ # !\u_reg:3:u_reg_i|dffs[15]\) # !\fir_in_del1_ff|dffs[15]\ & !\u_reg:3:u_reg_i|dffs[15]\ & !\fir_diff_ff|dffs[14]~169\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_in_del1_ff|dffs[15]\,
	datab => \u_reg:3:u_reg_i|dffs[15]\,
	cin => \fir_diff_ff|dffs[14]~169\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[15]\,
	cout => \fir_diff_ff|dffs[15]~172\);

\fir_diff_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[16]\ = DFFE(\u_reg:3:u_reg_i|dffs[16]\ $ \fir_in_del1_ff|dffs[16]\ $ \fir_diff_ff|dffs[15]~172\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[16]~175\ = CARRY(\u_reg:3:u_reg_i|dffs[16]\ & (!\fir_diff_ff|dffs[15]~172\ # !\fir_in_del1_ff|dffs[16]\) # !\u_reg:3:u_reg_i|dffs[16]\ & !\fir_in_del1_ff|dffs[16]\ & !\fir_diff_ff|dffs[15]~172\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[16]\,
	datab => \fir_in_del1_ff|dffs[16]\,
	cin => \fir_diff_ff|dffs[15]~172\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[16]\,
	cout => \fir_diff_ff|dffs[16]~175\);

\fir_diff_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[17]\ = DFFE(\u_reg:3:u_reg_i|dffs[17]\ $ \fir_in_del1_ff|dffs[17]\ $ !\fir_diff_ff|dffs[16]~175\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[17]~178\ = CARRY(\u_reg:3:u_reg_i|dffs[17]\ & \fir_in_del1_ff|dffs[17]\ & !\fir_diff_ff|dffs[16]~175\ # !\u_reg:3:u_reg_i|dffs[17]\ & (\fir_in_del1_ff|dffs[17]\ # !\fir_diff_ff|dffs[16]~175\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[17]\,
	datab => \fir_in_del1_ff|dffs[17]\,
	cin => \fir_diff_ff|dffs[16]~175\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[17]\,
	cout => \fir_diff_ff|dffs[17]~178\);

\fir_diff_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[18]\ = DFFE(\fir_in_del1_ff|dffs[18]\ $ \u_reg:3:u_reg_i|dffs[18]\ $ \fir_diff_ff|dffs[17]~178\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[18]~181\ = CARRY(\fir_in_del1_ff|dffs[18]\ & \u_reg:3:u_reg_i|dffs[18]\ & !\fir_diff_ff|dffs[17]~178\ # !\fir_in_del1_ff|dffs[18]\ & (\u_reg:3:u_reg_i|dffs[18]\ # !\fir_diff_ff|dffs[17]~178\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_in_del1_ff|dffs[18]\,
	datab => \u_reg:3:u_reg_i|dffs[18]\,
	cin => \fir_diff_ff|dffs[17]~178\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[18]\,
	cout => \fir_diff_ff|dffs[18]~181\);

\fir_diff_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[19]\ = DFFE(\fir_in_del1_ff|dffs[19]\ $ \u_reg:3:u_reg_i|dffs[19]\ $ !\fir_diff_ff|dffs[18]~181\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[19]~184\ = CARRY(\fir_in_del1_ff|dffs[19]\ & (!\fir_diff_ff|dffs[18]~181\ # !\u_reg:3:u_reg_i|dffs[19]\) # !\fir_in_del1_ff|dffs[19]\ & !\u_reg:3:u_reg_i|dffs[19]\ & !\fir_diff_ff|dffs[18]~181\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_in_del1_ff|dffs[19]\,
	datab => \u_reg:3:u_reg_i|dffs[19]\,
	cin => \fir_diff_ff|dffs[18]~181\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[19]\,
	cout => \fir_diff_ff|dffs[19]~184\);

\fir_diff_ff|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[20]\ = DFFE(\u_reg:3:u_reg_i|dffs[20]\ $ \fir_in_del1_ff|dffs[20]\ $ \fir_diff_ff|dffs[19]~184\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[20]~187\ = CARRY(\u_reg:3:u_reg_i|dffs[20]\ & (!\fir_diff_ff|dffs[19]~184\ # !\fir_in_del1_ff|dffs[20]\) # !\u_reg:3:u_reg_i|dffs[20]\ & !\fir_in_del1_ff|dffs[20]\ & !\fir_diff_ff|dffs[19]~184\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[20]\,
	datab => \fir_in_del1_ff|dffs[20]\,
	cin => \fir_diff_ff|dffs[19]~184\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[20]\,
	cout => \fir_diff_ff|dffs[20]~187\);

\fir_diff_ff|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[21]\ = DFFE(\u_reg:3:u_reg_i|dffs[21]\ $ \fir_in_del1_ff|dffs[21]\ $ !\fir_diff_ff|dffs[20]~187\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[21]~190\ = CARRY(\u_reg:3:u_reg_i|dffs[21]\ & \fir_in_del1_ff|dffs[21]\ & !\fir_diff_ff|dffs[20]~187\ # !\u_reg:3:u_reg_i|dffs[21]\ & (\fir_in_del1_ff|dffs[21]\ # !\fir_diff_ff|dffs[20]~187\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \u_reg:3:u_reg_i|dffs[21]\,
	datab => \fir_in_del1_ff|dffs[21]\,
	cin => \fir_diff_ff|dffs[20]~187\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[21]\,
	cout => \fir_diff_ff|dffs[21]~190\);

\fir_diff_ff|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[22]\ = DFFE(\fir_in_del1_ff|dffs[22]\ $ \u_reg:3:u_reg_i|dffs[22]\ $ \fir_diff_ff|dffs[21]~190\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_diff_ff|dffs[22]~193\ = CARRY(\fir_in_del1_ff|dffs[22]\ & \u_reg:3:u_reg_i|dffs[22]\ & !\fir_diff_ff|dffs[21]~190\ # !\fir_in_del1_ff|dffs[22]\ & (\u_reg:3:u_reg_i|dffs[22]\ # !\fir_diff_ff|dffs[21]~190\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_in_del1_ff|dffs[22]\,
	datab => \u_reg:3:u_reg_i|dffs[22]\,
	cin => \fir_diff_ff|dffs[21]~190\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[22]\,
	cout => \fir_diff_ff|dffs[22]~193\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~598_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~598\ = \fir_acc_ff|dffs[19]\ $ \fir_diff_ff|dffs[3]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~604\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~600\ = CARRY(\fir_acc_ff|dffs[19]\ & !\fir_diff_ff|dffs[3]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~604\ # !\fir_acc_ff|dffs[19]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~604\ # 
-- !\fir_diff_ff|dffs[3]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[19]\,
	datab => \fir_diff_ff|dffs[3]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~604\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~598\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~600\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~542_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~542\ = \fir_acc_ff|dffs[30]\ $ \fir_diff_ff|dffs[14]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~540\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~544\ = CARRY(\fir_acc_ff|dffs[30]\ & (\fir_diff_ff|dffs[14]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~540\) # !\fir_acc_ff|dffs[30]\ & \fir_diff_ff|dffs[14]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~540\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[30]\,
	datab => \fir_diff_ff|dffs[14]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~540\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~542\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~544\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~546_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~546\ = \fir_acc_ff|dffs[31]\ $ \fir_diff_ff|dffs[15]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~544\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~548\ = CARRY(\fir_acc_ff|dffs[31]\ & !\fir_diff_ff|dffs[15]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~544\ # !\fir_acc_ff|dffs[31]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~544\ # 
-- !\fir_diff_ff|dffs[15]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[31]\,
	datab => \fir_diff_ff|dffs[15]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~544\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~546\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~548\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~550_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~550\ = \fir_acc_ff|dffs[32]\ $ \fir_diff_ff|dffs[16]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~548\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~552\ = CARRY(\fir_acc_ff|dffs[32]\ & (\fir_diff_ff|dffs[16]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~548\) # !\fir_acc_ff|dffs[32]\ & \fir_diff_ff|dffs[16]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~548\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[32]\,
	datab => \fir_diff_ff|dffs[16]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~548\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~550\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~552\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~554_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~554\ = \fir_acc_ff|dffs[33]\ $ \fir_diff_ff|dffs[17]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~552\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~556\ = CARRY(\fir_acc_ff|dffs[33]\ & !\fir_diff_ff|dffs[17]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~552\ # !\fir_acc_ff|dffs[33]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~552\ # 
-- !\fir_diff_ff|dffs[17]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[33]\,
	datab => \fir_diff_ff|dffs[17]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~552\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~554\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~556\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~558_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~558\ = \fir_acc_ff|dffs[34]\ $ \fir_diff_ff|dffs[18]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~556\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~560\ = CARRY(\fir_acc_ff|dffs[34]\ & (\fir_diff_ff|dffs[18]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~556\) # !\fir_acc_ff|dffs[34]\ & \fir_diff_ff|dffs[18]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~556\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[34]\,
	datab => \fir_diff_ff|dffs[18]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~556\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~558\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~560\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~562_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~562\ = \fir_acc_ff|dffs[35]\ $ \fir_diff_ff|dffs[19]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~560\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~564\ = CARRY(\fir_acc_ff|dffs[35]\ & !\fir_diff_ff|dffs[19]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~560\ # !\fir_acc_ff|dffs[35]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~560\ # 
-- !\fir_diff_ff|dffs[19]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[35]\,
	datab => \fir_diff_ff|dffs[19]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~560\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~562\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~564\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~566_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~566\ = \fir_acc_ff|dffs[36]\ $ \fir_diff_ff|dffs[20]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~564\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~568\ = CARRY(\fir_acc_ff|dffs[36]\ & (\fir_diff_ff|dffs[20]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~564\) # !\fir_acc_ff|dffs[36]\ & \fir_diff_ff|dffs[20]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~564\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[36]\,
	datab => \fir_diff_ff|dffs[20]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~564\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~566\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~568\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~570_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~570\ = \fir_acc_ff|dffs[37]\ $ \fir_diff_ff|dffs[21]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~568\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~572\ = CARRY(\fir_acc_ff|dffs[37]\ & !\fir_diff_ff|dffs[21]\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~568\ # !\fir_acc_ff|dffs[37]\ & (!\fir_sum1_as|adder|result_node|cs_buffer[16]~568\ # 
-- !\fir_diff_ff|dffs[21]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[37]\,
	datab => \fir_diff_ff|dffs[21]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~568\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~570\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~572\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~574_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~574\ = \fir_acc_ff|dffs[38]\ $ \fir_diff_ff|dffs[22]\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~572\
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~576\ = CARRY(\fir_acc_ff|dffs[38]\ & (\fir_diff_ff|dffs[22]\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~572\) # !\fir_acc_ff|dffs[38]\ & \fir_diff_ff|dffs[22]\ & 
-- !\fir_sum1_as|adder|result_node|cs_buffer[16]~572\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[38]\,
	datab => \fir_diff_ff|dffs[22]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~572\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~574\,
	cout => \fir_sum1_as|adder|result_node|cs_buffer[16]~576\);

\AD_MR~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_MR,
	combout => \AD_MR~combout\);

\fir_acc_ff|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[24]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~518\ $ \ps_mux[23]~2942\ $ \fir_acc_ff|dffs[23]~338\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[24]~287\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~518\ & (!\fir_acc_ff|dffs[23]~338\ # !\ps_mux[23]~2942\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~518\ & !\ps_mux[23]~2942\ & !\fir_acc_ff|dffs[23]~338\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~518\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[23]~338\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[24]\,
	cout => \fir_acc_ff|dffs[24]~287\);

\fir_acc_ff|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[25]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~522\ $ \ps_mux[23]~2942\ $ !\fir_acc_ff|dffs[24]~287\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[25]~290\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~522\ & \ps_mux[23]~2942\ & !\fir_acc_ff|dffs[24]~287\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~522\ & (\ps_mux[23]~2942\ # !\fir_acc_ff|dffs[24]~287\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~522\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[24]~287\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[25]\,
	cout => \fir_acc_ff|dffs[25]~290\);

\fir_acc_ff|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[26]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~526\ $ \ps_mux[23]~2942\ $ \fir_acc_ff|dffs[25]~290\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[26]~293\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~526\ & (!\fir_acc_ff|dffs[25]~290\ # !\ps_mux[23]~2942\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~526\ & !\ps_mux[23]~2942\ & !\fir_acc_ff|dffs[25]~290\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~526\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[25]~290\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[26]\,
	cout => \fir_acc_ff|dffs[26]~293\);

\fir_acc_ff|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[27]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~530\ $ \ps_mux[23]~2942\ $ !\fir_acc_ff|dffs[26]~293\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[27]~296\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~530\ & \ps_mux[23]~2942\ & !\fir_acc_ff|dffs[26]~293\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~530\ & (\ps_mux[23]~2942\ # !\fir_acc_ff|dffs[26]~293\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~530\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[26]~293\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[27]\,
	cout => \fir_acc_ff|dffs[27]~296\);

\fir_acc_ff|dffs[28]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[28]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~534\ $ \ps_mux[23]~2942\ $ \fir_acc_ff|dffs[27]~296\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[28]~299\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~534\ & (!\fir_acc_ff|dffs[27]~296\ # !\ps_mux[23]~2942\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~534\ & !\ps_mux[23]~2942\ & !\fir_acc_ff|dffs[27]~296\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~534\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[27]~296\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[28]\,
	cout => \fir_acc_ff|dffs[28]~299\);

\fir_acc_ff|dffs[29]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[29]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~538\ $ \ps_mux[23]~2942\ $ !\fir_acc_ff|dffs[28]~299\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[29]~302\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~538\ & \ps_mux[23]~2942\ & !\fir_acc_ff|dffs[28]~299\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~538\ & (\ps_mux[23]~2942\ # !\fir_acc_ff|dffs[28]~299\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~538\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[28]~299\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[29]\,
	cout => \fir_acc_ff|dffs[29]~302\);

\fir_acc_ff|dffs[30]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[30]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~542\ $ \fir_acc_ff|dffs[29]~302\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[30]~305\ = CARRY(\ps_mux[23]~2942\ & \fir_sum1_as|adder|result_node|cs_buffer[16]~542\ & !\fir_acc_ff|dffs[29]~302\ # !\ps_mux[23]~2942\ & (\fir_sum1_as|adder|result_node|cs_buffer[16]~542\ # !\fir_acc_ff|dffs[29]~302\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~542\,
	cin => \fir_acc_ff|dffs[29]~302\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[30]\,
	cout => \fir_acc_ff|dffs[30]~305\);

\fir_acc_ff|dffs[31]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[31]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~546\ $ !\fir_acc_ff|dffs[30]~305\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[31]~308\ = CARRY(\ps_mux[23]~2942\ & (!\fir_acc_ff|dffs[30]~305\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~546\) # !\ps_mux[23]~2942\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~546\ & !\fir_acc_ff|dffs[30]~305\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~546\,
	cin => \fir_acc_ff|dffs[30]~305\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[31]\,
	cout => \fir_acc_ff|dffs[31]~308\);

\fir_acc_ff|dffs[32]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[32]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~550\ $ \fir_acc_ff|dffs[31]~308\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[32]~311\ = CARRY(\ps_mux[23]~2942\ & \fir_sum1_as|adder|result_node|cs_buffer[16]~550\ & !\fir_acc_ff|dffs[31]~308\ # !\ps_mux[23]~2942\ & (\fir_sum1_as|adder|result_node|cs_buffer[16]~550\ # !\fir_acc_ff|dffs[31]~308\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~550\,
	cin => \fir_acc_ff|dffs[31]~308\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[32]\,
	cout => \fir_acc_ff|dffs[32]~311\);

\fir_acc_ff|dffs[33]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[33]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~554\ $ !\fir_acc_ff|dffs[32]~311\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[33]~314\ = CARRY(\ps_mux[23]~2942\ & (!\fir_acc_ff|dffs[32]~311\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~554\) # !\ps_mux[23]~2942\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~554\ & !\fir_acc_ff|dffs[32]~311\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~554\,
	cin => \fir_acc_ff|dffs[32]~311\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[33]\,
	cout => \fir_acc_ff|dffs[33]~314\);

\fir_acc_ff|dffs[34]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[34]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~558\ $ \fir_acc_ff|dffs[33]~314\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[34]~317\ = CARRY(\ps_mux[23]~2942\ & \fir_sum1_as|adder|result_node|cs_buffer[16]~558\ & !\fir_acc_ff|dffs[33]~314\ # !\ps_mux[23]~2942\ & (\fir_sum1_as|adder|result_node|cs_buffer[16]~558\ # !\fir_acc_ff|dffs[33]~314\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~558\,
	cin => \fir_acc_ff|dffs[33]~314\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[34]\,
	cout => \fir_acc_ff|dffs[34]~317\);

\fir_acc_ff|dffs[35]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[35]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~562\ $ !\fir_acc_ff|dffs[34]~317\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[35]~320\ = CARRY(\ps_mux[23]~2942\ & (!\fir_acc_ff|dffs[34]~317\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~562\) # !\ps_mux[23]~2942\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~562\ & !\fir_acc_ff|dffs[34]~317\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~562\,
	cin => \fir_acc_ff|dffs[34]~317\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[35]\,
	cout => \fir_acc_ff|dffs[35]~320\);

\fir_acc_ff|dffs[36]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[36]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~566\ $ \fir_acc_ff|dffs[35]~320\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[36]~323\ = CARRY(\ps_mux[23]~2942\ & \fir_sum1_as|adder|result_node|cs_buffer[16]~566\ & !\fir_acc_ff|dffs[35]~320\ # !\ps_mux[23]~2942\ & (\fir_sum1_as|adder|result_node|cs_buffer[16]~566\ # !\fir_acc_ff|dffs[35]~320\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~566\,
	cin => \fir_acc_ff|dffs[35]~320\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[36]\,
	cout => \fir_acc_ff|dffs[36]~323\);

\fir_acc_ff|dffs[37]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[37]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~570\ $ !\fir_acc_ff|dffs[36]~323\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[37]~326\ = CARRY(\ps_mux[23]~2942\ & (!\fir_acc_ff|dffs[36]~323\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~570\) # !\ps_mux[23]~2942\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~570\ & !\fir_acc_ff|dffs[36]~323\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~570\,
	cin => \fir_acc_ff|dffs[36]~323\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[37]\,
	cout => \fir_acc_ff|dffs[37]~326\);

\fir_acc_ff|dffs[38]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[38]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~574\ $ \fir_acc_ff|dffs[37]~326\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[38]~329\ = CARRY(\ps_mux[23]~2942\ & \fir_sum1_as|adder|result_node|cs_buffer[16]~574\ & !\fir_acc_ff|dffs[37]~326\ # !\ps_mux[23]~2942\ & (\fir_sum1_as|adder|result_node|cs_buffer[16]~574\ # !\fir_acc_ff|dffs[37]~326\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~574\,
	cin => \fir_acc_ff|dffs[37]~326\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[38]\,
	cout => \fir_acc_ff|dffs[38]~329\);

\ps_mux~2943_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2943\ = \fir_acc_ff|dffs[39]\ & (\PS_SEL_REG_FF|dffs[0]\ $ \PS_SEL_REG_FF|dffs[1]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2828",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[39]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2943\);

\ps_mux~2945_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2945\ = !\pbusy_out_comb~2\ & (\ps_mux~2943\ # \ps_mux~2944\ & \fir_acc_ff|dffs[38]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2944\,
	datab => \fir_acc_ff|dffs[38]\,
	datac => \ps_mux~2943\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2945\);

\ps_mux~2944_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2944\ = \PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2944\);

\ps_mux~2946_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2946\ = !\pbusy_out_comb~2\ & (\ps_mux~2943\ # \fir_acc_ff|dffs[37]\ & \ps_mux~2944\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2943\,
	datab => \fir_acc_ff|dffs[37]\,
	datac => \ps_mux~2944\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2946\);

\ps_mux~2947_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2947\ = !\pbusy_out_comb~2\ & (\ps_mux~2943\ # \ps_mux~2944\ & \fir_acc_ff|dffs[36]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2944\,
	datab => \fir_acc_ff|dffs[36]\,
	datac => \ps_mux~2943\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2947\);

\Equal~386_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~386\ = !\PS_SEL_REG_FF|dffs[1]\ & (\PS_SEL_REG_FF|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PS_SEL_REG_FF|dffs[1]\,
	datad => \PS_SEL_REG_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~386\);

\u_reg:1:u_reg_i|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:1:u_reg_i|dffs[23]\ = DFFE(\u_reg:0:u_reg_i|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:0:u_reg_i|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:1:u_reg_i|dffs[23]\);

\u_reg:2:u_reg_i|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:2:u_reg_i|dffs[23]\ = DFFE(\u_reg:1:u_reg_i|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:1:u_reg_i|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:2:u_reg_i|dffs[23]\);

\u_reg:3:u_reg_i|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \u_reg:3:u_reg_i|dffs[23]\ = DFFE(\u_reg:2:u_reg_i|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:2:u_reg_i|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \u_reg:3:u_reg_i|dffs[23]\);

\fir_in_del1_ff|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_in_del1_ff|dffs[23]\ = DFFE(\u_reg:3:u_reg_i|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \u_reg:3:u_reg_i|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_in_del1_ff|dffs[23]\);

\fir_diff_ff|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_diff_ff|dffs[23]\ = DFFE(\u_reg:3:u_reg_i|dffs[23]\ $ \fir_diff_ff|dffs[22]~193\ $ !\fir_in_del1_ff|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \u_reg:3:u_reg_i|dffs[23]\,
	datad => \fir_in_del1_ff|dffs[23]\,
	cin => \fir_diff_ff|dffs[22]~193\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_diff_ff|dffs[23]\);

\fir_sum1_as|adder|result_node|cs_buffer[16]~578_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_sum1_as|adder|result_node|cs_buffer[16]~578\ = \fir_acc_ff|dffs[39]\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~576\ $ \fir_diff_ff|dffs[23]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C33C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \fir_acc_ff|dffs[39]\,
	datad => \fir_diff_ff|dffs[23]\,
	cin => \fir_sum1_as|adder|result_node|cs_buffer[16]~576\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_sum1_as|adder|result_node|cs_buffer[16]~578\);

\fir_acc_ff|dffs[39]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[39]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[23]~2942\ $ (\fir_acc_ff|dffs[38]~329\ $ !\fir_sum1_as|adder|result_node|cs_buffer[16]~578\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[23]~2942\,
	datad => \fir_sum1_as|adder|result_node|cs_buffer[16]~578\,
	cin => \fir_acc_ff|dffs[38]~329\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[39]\);

\ps_mux~2950_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2950\ = !\pbusy_out_comb~2\ & (\ps_mux~2949\ # \Equal~386\ & \fir_acc_ff|dffs[39]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2949\,
	datab => \Equal~386\,
	datac => \fir_acc_ff|dffs[39]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2950\);

\ps_mux~2952_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2952\ = !\pbusy_out_comb~2\ & (\ps_mux~2951\ # \Equal~386\ & \fir_acc_ff|dffs[39]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2951\,
	datab => \Equal~386\,
	datac => \fir_acc_ff|dffs[39]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2952\);

\ps_mux~2954_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2954\ = !\pbusy_out_comb~2\ & (\ps_mux~2953\ # \Equal~386\ & \fir_acc_ff|dffs[39]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2953\,
	datab => \Equal~386\,
	datac => \fir_acc_ff|dffs[39]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2954\);

\ps_mux~2956_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2956\ = !\pbusy_out_comb~2\ & (\ps_mux~2955\ # \Equal~386\ & \fir_acc_ff|dffs[39]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2955\,
	datab => \Equal~386\,
	datac => \fir_acc_ff|dffs[39]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2956\);

\ps_mux~2957_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2957\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[30]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[38]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8C80",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[30]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \fir_acc_ff|dffs[38]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2957\);

\ps_mux~2959_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2959\ = !\pbusy_out_comb~2\ & (\ps_mux~2957\ # \ps_mux~2958\ & \fir_acc_ff|dffs[34]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2958\,
	datab => \ps_mux~2957\,
	datac => \fir_acc_ff|dffs[34]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2959\);

\ps_mux~2960_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2960\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[29]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[37]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C480",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \fir_acc_ff|dffs[29]\,
	datad => \fir_acc_ff|dffs[37]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2960\);

\ps_mux~2961_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2961\ = !\pbusy_out_comb~2\ & (\ps_mux~2960\ # \ps_mux~2958\ & \fir_acc_ff|dffs[33]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2958\,
	datab => \ps_mux~2960\,
	datac => \fir_acc_ff|dffs[33]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2961\);

\ps_mux~2962_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2962\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[28]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[36]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C480",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \fir_acc_ff|dffs[28]\,
	datad => \fir_acc_ff|dffs[36]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2962\);

\ps_mux~2963_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2963\ = !\pbusy_out_comb~2\ & (\ps_mux~2962\ # \ps_mux~2958\ & \fir_acc_ff|dffs[32]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2958\,
	datab => \ps_mux~2962\,
	datac => \fir_acc_ff|dffs[32]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2963\);

\ps_mux~2964_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2964\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[27]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[35]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C480",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \fir_acc_ff|dffs[27]\,
	datad => \fir_acc_ff|dffs[35]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2964\);

\ps_mux~2965_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2965\ = !\pbusy_out_comb~2\ & (\ps_mux~2964\ # \ps_mux~2958\ & \fir_acc_ff|dffs[31]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2958\,
	datab => \ps_mux~2964\,
	datac => \fir_acc_ff|dffs[31]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2965\);

\ps_mux~2966_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2966\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[26]\ # !\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[34]\))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \PS_SEL_REG_FF|dffs[1]\,
	datab => \fir_acc_ff|dffs[26]\,
	datac => \fir_acc_ff|dffs[34]\,
	datad => \PS_SEL_REG_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2966\);

\ps_mux~2958_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2958\ = !\PS_SEL_REG_FF|dffs[0]\ & \PS_SEL_REG_FF|dffs[1]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3030",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2958\);

\ps_mux~2967_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2967\ = !\pbusy_out_comb~2\ & (\ps_mux~2966\ # \fir_acc_ff|dffs[30]\ & \ps_mux~2958\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[30]\,
	datab => \ps_mux~2966\,
	datac => \ps_mux~2958\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2967\);

\ps_mux~2968_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2968\ = \PS_SEL_REG_FF|dffs[0]\ & (\PS_SEL_REG_FF|dffs[1]\ & (\fir_acc_ff|dffs[25]\) # !\PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[33]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[33]\,
	datab => \fir_acc_ff|dffs[25]\,
	datac => \PS_SEL_REG_FF|dffs[0]\,
	datad => \PS_SEL_REG_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2968\);

\ps_mux~2969_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux~2969\ = !\pbusy_out_comb~2\ & (\ps_mux~2968\ # \ps_mux~2958\ & \fir_acc_ff|dffs[29]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2958\,
	datab => \ps_mux~2968\,
	datac => \fir_acc_ff|dffs[29]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux~2969\);

\ps_mux[7]~2972_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[7]~2972\ = !\PS_SEL_REG_FF|dffs[0]\ & \PS_SEL_REG_FF|dffs[1]\ & \fir_acc_ff|dffs[27]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \fir_acc_ff|dffs[27]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[7]~2972\);

\ps_mux[7]~2973_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[7]~2973\ = !\pbusy_out_comb~2\ & (\ps_mux[7]~2972\ # \fir_acc_ff|dffs[31]\ & \Equal~386\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[31]\,
	datab => \ps_mux[7]~2972\,
	datac => \Equal~386\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[7]~2973\);

\ps_mux[6]~2975_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[6]~2975\ = !\pbusy_out_comb~2\ & (\ps_mux[6]~2974\ # \fir_acc_ff|dffs[30]\ & \Equal~386\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[6]~2974\,
	datab => \fir_acc_ff|dffs[30]\,
	datac => \Equal~386\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[6]~2975\);

\ps_mux[5]~2976_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[5]~2976\ = \fir_acc_ff|dffs[25]\ & !\PS_SEL_REG_FF|dffs[0]\ & \PS_SEL_REG_FF|dffs[1]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[25]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[5]~2976\);

\ps_mux[5]~2977_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[5]~2977\ = !\pbusy_out_comb~2\ & (\ps_mux[5]~2976\ # \Equal~386\ & \fir_acc_ff|dffs[29]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Equal~386\,
	datab => \ps_mux[5]~2976\,
	datac => \fir_acc_ff|dffs[29]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[5]~2977\);

\ps_mux[4]~2979_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[4]~2979\ = !\pbusy_out_comb~2\ & (\ps_mux[4]~2978\ # \Equal~386\ & \fir_acc_ff|dffs[28]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[4]~2978\,
	datab => \Equal~386\,
	datac => \fir_acc_ff|dffs[28]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[4]~2979\);

\ps_mux[2]~2981_I\ : apex20ke_lcell
-- Equation(s):
-- \ps_mux[2]~2981\ = \fir_acc_ff|dffs[26]\ & \PS_SEL_REG_FF|dffs[0]\ & !\PS_SEL_REG_FF|dffs[1]\ & !\pbusy_out_comb~2\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_acc_ff|dffs[26]\,
	datab => \PS_SEL_REG_FF|dffs[0]\,
	datac => \PS_SEL_REG_FF|dffs[1]\,
	datad => \pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \ps_mux[2]~2981\);

\fir_acc_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[0]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[0]~2983\ $ \fir_acc_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[0]~404\ = CARRY(\fir_acc_ff|dffs[0]\ # !\ps_mux[0]~2983\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66DD",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[0]~2983\,
	datab => \fir_acc_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[0]\,
	cout => \fir_acc_ff|dffs[0]~404\);

\fir_acc_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[1]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[1]~2982\ $ \fir_acc_ff|dffs[1]\ $ !\fir_acc_ff|dffs[0]~404\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[1]~401\ = CARRY(\ps_mux[1]~2982\ & (!\fir_acc_ff|dffs[0]~404\ # !\fir_acc_ff|dffs[1]\) # !\ps_mux[1]~2982\ & !\fir_acc_ff|dffs[1]\ & !\fir_acc_ff|dffs[0]~404\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[1]~2982\,
	datab => \fir_acc_ff|dffs[1]\,
	cin => \fir_acc_ff|dffs[0]~404\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[1]\,
	cout => \fir_acc_ff|dffs[1]~401\);

\fir_acc_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[3]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux[3]~2980\ $ \fir_acc_ff|dffs[3]\ $ !\fir_acc_ff|dffs[2]~398\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[3]~395\ = CARRY(\ps_mux[3]~2980\ & (!\fir_acc_ff|dffs[2]~398\ # !\fir_acc_ff|dffs[3]\) # !\ps_mux[3]~2980\ & !\fir_acc_ff|dffs[3]\ & !\fir_acc_ff|dffs[2]~398\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux[3]~2980\,
	datab => \fir_acc_ff|dffs[3]\,
	cin => \fir_acc_ff|dffs[2]~398\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[3]\,
	cout => \fir_acc_ff|dffs[3]~395\);

\fir_acc_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[8]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux~2971\ $ \fir_acc_ff|dffs[8]\ $ \fir_acc_ff|dffs[7]~383\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[8]~380\ = CARRY(\ps_mux~2971\ & \fir_acc_ff|dffs[8]\ & !\fir_acc_ff|dffs[7]~383\ # !\ps_mux~2971\ & (\fir_acc_ff|dffs[8]\ # !\fir_acc_ff|dffs[7]~383\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2971\,
	datab => \fir_acc_ff|dffs[8]\,
	cin => \fir_acc_ff|dffs[7]~383\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[8]\,
	cout => \fir_acc_ff|dffs[8]~380\);

\fir_acc_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[16]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~610\ $ \ps_mux~2954\ $ \fir_acc_ff|dffs[15]~359\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[16]~356\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~610\ & (!\fir_acc_ff|dffs[15]~359\ # !\ps_mux~2954\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~610\ & !\ps_mux~2954\ & !\fir_acc_ff|dffs[15]~359\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~610\,
	datab => \ps_mux~2954\,
	cin => \fir_acc_ff|dffs[15]~359\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[16]\,
	cout => \fir_acc_ff|dffs[16]~356\);

\fir_acc_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[17]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~606\ $ \ps_mux~2952\ $ !\fir_acc_ff|dffs[16]~356\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[17]~353\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~606\ & \ps_mux~2952\ & !\fir_acc_ff|dffs[16]~356\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~606\ & (\ps_mux~2952\ # !\fir_acc_ff|dffs[16]~356\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~606\,
	datab => \ps_mux~2952\,
	cin => \fir_acc_ff|dffs[16]~356\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[17]\,
	cout => \fir_acc_ff|dffs[17]~353\);

\fir_acc_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[18]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~602\ $ \ps_mux~2950\ $ \fir_acc_ff|dffs[17]~353\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[18]~350\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~602\ & (!\fir_acc_ff|dffs[17]~353\ # !\ps_mux~2950\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~602\ & !\ps_mux~2950\ & !\fir_acc_ff|dffs[17]~353\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~602\,
	datab => \ps_mux~2950\,
	cin => \fir_acc_ff|dffs[17]~353\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[18]\,
	cout => \fir_acc_ff|dffs[18]~350\);

\fir_acc_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[19]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \ps_mux~2948\ $ \fir_sum1_as|adder|result_node|cs_buffer[16]~598\ $ !\fir_acc_ff|dffs[18]~350\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[19]~347\ = CARRY(\ps_mux~2948\ & (!\fir_acc_ff|dffs[18]~350\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~598\) # !\ps_mux~2948\ & !\fir_sum1_as|adder|result_node|cs_buffer[16]~598\ & !\fir_acc_ff|dffs[18]~350\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \ps_mux~2948\,
	datab => \fir_sum1_as|adder|result_node|cs_buffer[16]~598\,
	cin => \fir_acc_ff|dffs[18]~350\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[19]\,
	cout => \fir_acc_ff|dffs[19]~347\);

\fir_acc_ff|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[20]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~594\ $ \ps_mux~2947\ $ \fir_acc_ff|dffs[19]~347\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[20]~344\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~594\ & (!\fir_acc_ff|dffs[19]~347\ # !\ps_mux~2947\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~594\ & !\ps_mux~2947\ & !\fir_acc_ff|dffs[19]~347\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~594\,
	datab => \ps_mux~2947\,
	cin => \fir_acc_ff|dffs[19]~347\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[20]\,
	cout => \fir_acc_ff|dffs[20]~344\);

\fir_acc_ff|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[21]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~590\ $ \ps_mux~2946\ $ !\fir_acc_ff|dffs[20]~344\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[21]~341\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~590\ & \ps_mux~2946\ & !\fir_acc_ff|dffs[20]~344\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~590\ & (\ps_mux~2946\ # !\fir_acc_ff|dffs[20]~344\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~590\,
	datab => \ps_mux~2946\,
	cin => \fir_acc_ff|dffs[20]~344\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[21]\,
	cout => \fir_acc_ff|dffs[21]~341\);

\fir_acc_ff|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[22]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~582\ $ \ps_mux~2945\ $ \fir_acc_ff|dffs[21]~341\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[22]~335\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~582\ & (!\fir_acc_ff|dffs[21]~341\ # !\ps_mux~2945\) # !\fir_sum1_as|adder|result_node|cs_buffer[16]~582\ & !\ps_mux~2945\ & !\fir_acc_ff|dffs[21]~341\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~582\,
	datab => \ps_mux~2945\,
	cin => \fir_acc_ff|dffs[21]~341\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[22]\,
	cout => \fir_acc_ff|dffs[22]~335\);

\fir_acc_ff|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_acc_ff|dffs[23]\ = DFFE(!GLOBAL(\AD_MR~combout\) & \fir_sum1_as|adder|result_node|cs_buffer[16]~586\ $ \ps_mux[23]~2942\ $ !\fir_acc_ff|dffs[22]~335\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_acc_ff|dffs[23]~338\ = CARRY(\fir_sum1_as|adder|result_node|cs_buffer[16]~586\ & \ps_mux[23]~2942\ & !\fir_acc_ff|dffs[22]~335\ # !\fir_sum1_as|adder|result_node|cs_buffer[16]~586\ & (\ps_mux[23]~2942\ # !\fir_acc_ff|dffs[22]~335\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \fir_sum1_as|adder|result_node|cs_buffer[16]~586\,
	datab => \ps_mux[23]~2942\,
	cin => \fir_acc_ff|dffs[22]~335\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \AD_MR~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_acc_ff|dffs[23]\,
	cout => \fir_acc_ff|dffs[23]~338\);

\fir_out_ff|dffs[0]~215_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~215\ = CARRY(\offset[0]~combout\ & \fir_acc_ff|dffs[16]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0088",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[0]~combout\,
	datab => \fir_acc_ff|dffs[16]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~213COMB\,
	cout => \fir_out_ff|dffs[0]~215\);

\fir_out_ff|dffs[0]~211_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~211\ = CARRY(\offset[1]~combout\ & !\fir_acc_ff|dffs[17]\ & !\fir_out_ff|dffs[0]~215\ # !\offset[1]~combout\ & (!\fir_out_ff|dffs[0]~215\ # !\fir_acc_ff|dffs[17]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[1]~combout\,
	datab => \fir_acc_ff|dffs[17]\,
	cin => \fir_out_ff|dffs[0]~215\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~209COMB\,
	cout => \fir_out_ff|dffs[0]~211\);

\fir_out_ff|dffs[0]~207_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~207\ = CARRY(\offset[2]~combout\ & (\fir_acc_ff|dffs[18]\ # !\fir_out_ff|dffs[0]~211\) # !\offset[2]~combout\ & \fir_acc_ff|dffs[18]\ & !\fir_out_ff|dffs[0]~211\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[2]~combout\,
	datab => \fir_acc_ff|dffs[18]\,
	cin => \fir_out_ff|dffs[0]~211\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~205COMB\,
	cout => \fir_out_ff|dffs[0]~207\);

\fir_out_ff|dffs[0]~203_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~203\ = CARRY(\offset[3]~combout\ & !\fir_acc_ff|dffs[19]\ & !\fir_out_ff|dffs[0]~207\ # !\offset[3]~combout\ & (!\fir_out_ff|dffs[0]~207\ # !\fir_acc_ff|dffs[19]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[3]~combout\,
	datab => \fir_acc_ff|dffs[19]\,
	cin => \fir_out_ff|dffs[0]~207\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~201COMB\,
	cout => \fir_out_ff|dffs[0]~203\);

\fir_out_ff|dffs[0]~199_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~199\ = CARRY(\offset[4]~combout\ & (\fir_acc_ff|dffs[20]\ # !\fir_out_ff|dffs[0]~203\) # !\offset[4]~combout\ & \fir_acc_ff|dffs[20]\ & !\fir_out_ff|dffs[0]~203\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[4]~combout\,
	datab => \fir_acc_ff|dffs[20]\,
	cin => \fir_out_ff|dffs[0]~203\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~197COMB\,
	cout => \fir_out_ff|dffs[0]~199\);

\fir_out_ff|dffs[0]~195_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~195\ = CARRY(\offset[5]~combout\ & !\fir_acc_ff|dffs[21]\ & !\fir_out_ff|dffs[0]~199\ # !\offset[5]~combout\ & (!\fir_out_ff|dffs[0]~199\ # !\fir_acc_ff|dffs[21]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[5]~combout\,
	datab => \fir_acc_ff|dffs[21]\,
	cin => \fir_out_ff|dffs[0]~199\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~193COMB\,
	cout => \fir_out_ff|dffs[0]~195\);

\fir_out_ff|dffs[0]~191_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~191\ = CARRY(\offset[6]~combout\ & (\fir_acc_ff|dffs[22]\ # !\fir_out_ff|dffs[0]~195\) # !\offset[6]~combout\ & \fir_acc_ff|dffs[22]\ & !\fir_out_ff|dffs[0]~195\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[6]~combout\,
	datab => \fir_acc_ff|dffs[22]\,
	cin => \fir_out_ff|dffs[0]~195\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~189COMB\,
	cout => \fir_out_ff|dffs[0]~191\);

\fir_out_ff|dffs[0]~187_I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]~187\ = CARRY(\offset[7]~combout\ & !\fir_acc_ff|dffs[23]\ & !\fir_out_ff|dffs[0]~191\ # !\offset[7]~combout\ & (!\fir_out_ff|dffs[0]~191\ # !\fir_acc_ff|dffs[23]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[7]~combout\,
	datab => \fir_acc_ff|dffs[23]\,
	cin => \fir_out_ff|dffs[0]~191\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \fir_out_ff|dffs[0]~185COMB\,
	cout => \fir_out_ff|dffs[0]~187\);

\fir_out_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[0]\ = DFFE(\offset[8]~combout\ $ \fir_acc_ff|dffs[24]\ $ !\fir_out_ff|dffs[0]~187\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[0]~138\ = CARRY(\offset[8]~combout\ & (\fir_acc_ff|dffs[24]\ # !\fir_out_ff|dffs[0]~187\) # !\offset[8]~combout\ & \fir_acc_ff|dffs[24]\ & !\fir_out_ff|dffs[0]~187\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[8]~combout\,
	datab => \fir_acc_ff|dffs[24]\,
	cin => \fir_out_ff|dffs[0]~187\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[0]\,
	cout => \fir_out_ff|dffs[0]~138\);

\offset[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(9),
	combout => \offset[9]~combout\);

\fir_out_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[1]\ = DFFE(\offset[9]~combout\ $ \fir_acc_ff|dffs[25]\ $ \fir_out_ff|dffs[0]~138\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[1]~141\ = CARRY(\offset[9]~combout\ & !\fir_acc_ff|dffs[25]\ & !\fir_out_ff|dffs[0]~138\ # !\offset[9]~combout\ & (!\fir_out_ff|dffs[0]~138\ # !\fir_acc_ff|dffs[25]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[9]~combout\,
	datab => \fir_acc_ff|dffs[25]\,
	cin => \fir_out_ff|dffs[0]~138\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[1]\,
	cout => \fir_out_ff|dffs[1]~141\);

\offset[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(10),
	combout => \offset[10]~combout\);

\fir_out_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[2]\ = DFFE(\offset[10]~combout\ $ \fir_acc_ff|dffs[26]\ $ !\fir_out_ff|dffs[1]~141\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[2]~144\ = CARRY(\offset[10]~combout\ & (\fir_acc_ff|dffs[26]\ # !\fir_out_ff|dffs[1]~141\) # !\offset[10]~combout\ & \fir_acc_ff|dffs[26]\ & !\fir_out_ff|dffs[1]~141\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[10]~combout\,
	datab => \fir_acc_ff|dffs[26]\,
	cin => \fir_out_ff|dffs[1]~141\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[2]\,
	cout => \fir_out_ff|dffs[2]~144\);

\offset[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(11),
	combout => \offset[11]~combout\);

\fir_out_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[3]\ = DFFE(\offset[11]~combout\ $ \fir_acc_ff|dffs[27]\ $ \fir_out_ff|dffs[2]~144\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[3]~147\ = CARRY(\offset[11]~combout\ & !\fir_acc_ff|dffs[27]\ & !\fir_out_ff|dffs[2]~144\ # !\offset[11]~combout\ & (!\fir_out_ff|dffs[2]~144\ # !\fir_acc_ff|dffs[27]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[11]~combout\,
	datab => \fir_acc_ff|dffs[27]\,
	cin => \fir_out_ff|dffs[2]~144\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[3]\,
	cout => \fir_out_ff|dffs[3]~147\);

\offset[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(12),
	combout => \offset[12]~combout\);

\fir_out_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[4]\ = DFFE(\offset[12]~combout\ $ \fir_acc_ff|dffs[28]\ $ !\fir_out_ff|dffs[3]~147\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[4]~150\ = CARRY(\offset[12]~combout\ & (\fir_acc_ff|dffs[28]\ # !\fir_out_ff|dffs[3]~147\) # !\offset[12]~combout\ & \fir_acc_ff|dffs[28]\ & !\fir_out_ff|dffs[3]~147\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[12]~combout\,
	datab => \fir_acc_ff|dffs[28]\,
	cin => \fir_out_ff|dffs[3]~147\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[4]\,
	cout => \fir_out_ff|dffs[4]~150\);

\offset[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(13),
	combout => \offset[13]~combout\);

\fir_out_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[5]\ = DFFE(\offset[13]~combout\ $ \fir_acc_ff|dffs[29]\ $ \fir_out_ff|dffs[4]~150\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[5]~153\ = CARRY(\offset[13]~combout\ & !\fir_acc_ff|dffs[29]\ & !\fir_out_ff|dffs[4]~150\ # !\offset[13]~combout\ & (!\fir_out_ff|dffs[4]~150\ # !\fir_acc_ff|dffs[29]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[13]~combout\,
	datab => \fir_acc_ff|dffs[29]\,
	cin => \fir_out_ff|dffs[4]~150\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[5]\,
	cout => \fir_out_ff|dffs[5]~153\);

\offset[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(14),
	combout => \offset[14]~combout\);

\fir_out_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[6]\ = DFFE(\offset[14]~combout\ $ \fir_acc_ff|dffs[30]\ $ !\fir_out_ff|dffs[5]~153\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[6]~156\ = CARRY(\offset[14]~combout\ & (\fir_acc_ff|dffs[30]\ # !\fir_out_ff|dffs[5]~153\) # !\offset[14]~combout\ & \fir_acc_ff|dffs[30]\ & !\fir_out_ff|dffs[5]~153\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[14]~combout\,
	datab => \fir_acc_ff|dffs[30]\,
	cin => \fir_out_ff|dffs[5]~153\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[6]\,
	cout => \fir_out_ff|dffs[6]~156\);

\offset[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_offset(15),
	combout => \offset[15]~combout\);

\fir_out_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[7]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[31]\ $ \fir_out_ff|dffs[6]~156\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[7]~159\ = CARRY(\offset[15]~combout\ & !\fir_acc_ff|dffs[31]\ & !\fir_out_ff|dffs[6]~156\ # !\offset[15]~combout\ & (!\fir_out_ff|dffs[6]~156\ # !\fir_acc_ff|dffs[31]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[31]\,
	cin => \fir_out_ff|dffs[6]~156\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[7]\,
	cout => \fir_out_ff|dffs[7]~159\);

\fir_out_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[8]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[32]\ $ !\fir_out_ff|dffs[7]~159\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[8]~162\ = CARRY(\offset[15]~combout\ & (\fir_acc_ff|dffs[32]\ # !\fir_out_ff|dffs[7]~159\) # !\offset[15]~combout\ & \fir_acc_ff|dffs[32]\ & !\fir_out_ff|dffs[7]~159\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[32]\,
	cin => \fir_out_ff|dffs[7]~159\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[8]\,
	cout => \fir_out_ff|dffs[8]~162\);

\fir_out_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[9]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[33]\ $ \fir_out_ff|dffs[8]~162\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[9]~165\ = CARRY(\offset[15]~combout\ & !\fir_acc_ff|dffs[33]\ & !\fir_out_ff|dffs[8]~162\ # !\offset[15]~combout\ & (!\fir_out_ff|dffs[8]~162\ # !\fir_acc_ff|dffs[33]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[33]\,
	cin => \fir_out_ff|dffs[8]~162\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[9]\,
	cout => \fir_out_ff|dffs[9]~165\);

\fir_out_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[10]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[34]\ $ !\fir_out_ff|dffs[9]~165\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[10]~168\ = CARRY(\offset[15]~combout\ & (\fir_acc_ff|dffs[34]\ # !\fir_out_ff|dffs[9]~165\) # !\offset[15]~combout\ & \fir_acc_ff|dffs[34]\ & !\fir_out_ff|dffs[9]~165\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[34]\,
	cin => \fir_out_ff|dffs[9]~165\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[10]\,
	cout => \fir_out_ff|dffs[10]~168\);

\fir_out_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[11]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[35]\ $ \fir_out_ff|dffs[10]~168\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[11]~171\ = CARRY(\offset[15]~combout\ & !\fir_acc_ff|dffs[35]\ & !\fir_out_ff|dffs[10]~168\ # !\offset[15]~combout\ & (!\fir_out_ff|dffs[10]~168\ # !\fir_acc_ff|dffs[35]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[35]\,
	cin => \fir_out_ff|dffs[10]~168\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[11]\,
	cout => \fir_out_ff|dffs[11]~171\);

\fir_out_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[12]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[36]\ $ !\fir_out_ff|dffs[11]~171\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[12]~174\ = CARRY(\offset[15]~combout\ & (\fir_acc_ff|dffs[36]\ # !\fir_out_ff|dffs[11]~171\) # !\offset[15]~combout\ & \fir_acc_ff|dffs[36]\ & !\fir_out_ff|dffs[11]~171\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[36]\,
	cin => \fir_out_ff|dffs[11]~171\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[12]\,
	cout => \fir_out_ff|dffs[12]~174\);

\fir_out_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[13]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[37]\ $ \fir_out_ff|dffs[12]~174\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[13]~177\ = CARRY(\offset[15]~combout\ & !\fir_acc_ff|dffs[37]\ & !\fir_out_ff|dffs[12]~174\ # !\offset[15]~combout\ & (!\fir_out_ff|dffs[12]~174\ # !\fir_acc_ff|dffs[37]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[37]\,
	cin => \fir_out_ff|dffs[12]~174\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[13]\,
	cout => \fir_out_ff|dffs[13]~177\);

\fir_out_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[14]\ = DFFE(\offset[15]~combout\ $ \fir_acc_ff|dffs[38]\ $ !\fir_out_ff|dffs[13]~177\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \fir_out_ff|dffs[14]~180\ = CARRY(\offset[15]~combout\ & (\fir_acc_ff|dffs[38]\ # !\fir_out_ff|dffs[13]~177\) # !\offset[15]~combout\ & \fir_acc_ff|dffs[38]\ & !\fir_out_ff|dffs[13]~177\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datab => \fir_acc_ff|dffs[38]\,
	cin => \fir_out_ff|dffs[13]~177\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[14]\,
	cout => \fir_out_ff|dffs[14]~180\);

\fir_out_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \fir_out_ff|dffs[15]\ = DFFE(\offset[15]~combout\ $ (\fir_out_ff|dffs[14]~180\ $ \fir_acc_ff|dffs[39]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \offset[15]~combout\,
	datad => \fir_acc_ff|dffs[39]\,
	cin => \fir_out_ff|dffs[14]~180\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \fir_out_ff|dffs[15]\);

\clk_cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[0]\ = DFFE(!GLOBAL(\Equal~374\) & \pbusy_out_comb~2\ $ !\clk_cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[0]~100\ = CARRY(!\pbusy_out_comb~2\ & \clk_cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9944",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \pbusy_out_comb~2\,
	datab => \clk_cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[0]\,
	cout => \clk_cnt[0]~100\);

\clk_cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[3]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[3]\ $ \clk_cnt[2]~121\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[3]~130\ = CARRY(!\clk_cnt[2]~121\ # !\clk_cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[3]\,
	cin => \clk_cnt[2]~121\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[3]\,
	cout => \clk_cnt[3]~130\);

\clk_cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[4]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[4]\ $ !\clk_cnt[3]~130\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[4]~127\ = CARRY(\clk_cnt[4]\ & !\clk_cnt[3]~130\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[4]\,
	cin => \clk_cnt[3]~130\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[4]\,
	cout => \clk_cnt[4]~127\);

\clk_cnt[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[5]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[5]\ $ \clk_cnt[4]~127\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[5]~118\ = CARRY(!\clk_cnt[4]~127\ # !\clk_cnt[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[5]\,
	cin => \clk_cnt[4]~127\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[5]\,
	cout => \clk_cnt[5]~118\);

\clk_cnt[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[6]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[6]\ $ !\clk_cnt[5]~118\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[6]~112\ = CARRY(\clk_cnt[6]\ & !\clk_cnt[5]~118\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[6]\,
	cin => \clk_cnt[5]~118\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[6]\,
	cout => \clk_cnt[6]~112\);

\clk_cnt[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[7]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[7]\ $ \clk_cnt[6]~112\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[7]~106\ = CARRY(!\clk_cnt[6]~112\ # !\clk_cnt[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[7]\,
	cin => \clk_cnt[6]~112\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[7]\,
	cout => \clk_cnt[7]~106\);

\clk_cnt[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[8]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[8]\ $ !\clk_cnt[7]~106\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[8]~124\ = CARRY(\clk_cnt[8]\ & !\clk_cnt[7]~106\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[8]\,
	cin => \clk_cnt[7]~106\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[8]\,
	cout => \clk_cnt[8]~124\);

\clk_cnt[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[9]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[9]\ $ \clk_cnt[8]~124\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \clk_cnt[9]~115\ = CARRY(!\clk_cnt[8]~124\ # !\clk_cnt[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \clk_cnt[9]\,
	cin => \clk_cnt[8]~124\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[9]\,
	cout => \clk_cnt[9]~115\);

\clk_cnt[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt[10]\ = DFFE(!GLOBAL(\Equal~374\) & \clk_cnt[9]~115\ $ !\clk_cnt[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \clk_cnt[10]\,
	cin => \clk_cnt[9]~115\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \Equal~374\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \clk_cnt[10]\);

\Equal~379_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~379\ = \clk_cnt[9]\ $ \tc_sel[3]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \clk_cnt[9]\,
	datad => \tc_sel[3]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~379\);

\Equal~378_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~378\ = \clk_cnt[6]\ $ (\tc_sel[3]~combout\ # \tc_sel[2]~combout\ & \tc_sel[0]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3666",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \clk_cnt[6]\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~378\);

\clk_cnt_max[2]~226_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[2]~226\ = \tc_sel[3]~combout\ # \tc_sel[0]~combout\ # \tc_sel[1]~combout\ $ \tc_sel[2]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFBE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \tc_sel[1]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[2]~226\);

\clk_cnt_max[5]~230_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[5]~230\ = \tc_sel[3]~combout\ # \tc_sel[2]~combout\ & (\tc_sel[0]~combout\ # !\tc_sel[1]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FABA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \tc_sel[1]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[5]~230\);

\clk_cnt_max[8]~229_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[8]~229\ = \tc_sel[3]~combout\ # \tc_sel[0]~combout\ & \tc_sel[2]~combout\ & \tc_sel[1]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF80",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[0]~combout\,
	datab => \tc_sel[2]~combout\,
	datac => \tc_sel[1]~combout\,
	datad => \tc_sel[3]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[8]~229\);

\clk_cnt_max[3]~227_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[3]~227\ = \tc_sel[3]~combout\ # \tc_sel[1]~combout\ & (\tc_sel[0]~combout\ # !\tc_sel[2]~combout\) # !\tc_sel[1]~combout\ & \tc_sel[2]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEBE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \tc_sel[1]~combout\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[0]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[3]~227\);

\clk_cnt_max[4]~228_I\ : apex20ke_lcell
-- Equation(s):
-- \clk_cnt_max[4]~228\ = \tc_sel[2]~combout\ # \tc_sel[3]~combout\ # \tc_sel[0]~combout\ & \tc_sel[1]~combout\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[0]~combout\,
	datab => \tc_sel[2]~combout\,
	datac => \tc_sel[1]~combout\,
	datad => \tc_sel[3]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clk_cnt_max[4]~228\);

\Equal~384_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~392\ = \clk_cnt[4]\ & \clk_cnt_max[4]~228\ & (\clk_cnt_max[3]~227\ $ !\clk_cnt[3]\) # !\clk_cnt[4]\ & !\clk_cnt_max[4]~228\ & (\clk_cnt_max[3]~227\ $ !\clk_cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[4]\,
	datab => \clk_cnt_max[3]~227\,
	datac => \clk_cnt[3]\,
	datad => \clk_cnt_max[4]~228\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~384\,
	cascout => \Equal~392\);

\Equal~389_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~393\ = (\clk_cnt[8]\ & \clk_cnt_max[8]~229\ & (\clk_cnt_max[5]~230\ $ !\clk_cnt[5]\) # !\clk_cnt[8]\ & !\clk_cnt_max[8]~229\ & (\clk_cnt_max[5]~230\ $ !\clk_cnt[5]\)) & CASCADE(\Equal~392\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[8]\,
	datab => \clk_cnt_max[5]~230\,
	datac => \clk_cnt[5]\,
	datad => \clk_cnt_max[8]~229\,
	cascin => \Equal~392\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~389\,
	cascout => \Equal~393\);

\Equal~388_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~388\ = (!\Equal~379\ & !\Equal~378\ & (\clk_cnt[2]\ $ !\clk_cnt_max[2]~226\)) & CASCADE(\Equal~393\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0201",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[2]\,
	datab => \Equal~379\,
	datac => \Equal~378\,
	datad => \clk_cnt_max[2]~226\,
	cascin => \Equal~393\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~388\);

\Equal~374_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~374\ = \Equal~372\ & !\clk_cnt[10]\ & \Equal~388\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Equal~372\,
	datac => \clk_cnt[10]\,
	datad => \Equal~388\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~374\);

\Equal~371_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~371\ = \clk_cnt[7]\ $ (\tc_sel[3]~combout\ # \tc_sel[2]~combout\ & \tc_sel[1]~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3666",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \tc_sel[3]~combout\,
	datab => \clk_cnt[7]\,
	datac => \tc_sel[2]~combout\,
	datad => \tc_sel[1]~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~371\);

\Equal~372_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~372\ = !\Equal~371\ & (\clk_cnt[1]\ & \clk_cnt[0]\ & !\clk_cnt_max[1]~224\ # !\clk_cnt[1]\ & !\clk_cnt[0]\ & \clk_cnt_max[1]~224\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0108",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \clk_cnt[1]\,
	datab => \clk_cnt[0]\,
	datac => \Equal~371\,
	datad => \clk_cnt_max[1]~224\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~372\);

\blevel_upd~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_upd~reg0\ = DFFE(\Equal~372\ & !\clk_cnt[10]\ & \Equal~388\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Equal~372\,
	datac => \clk_cnt[10]\,
	datad => \Equal~388\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_upd~reg0\);

\blevel_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[0]\ = DFFE(\fir_acc_ff|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[0]\);

\blevel_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[1]\ = DFFE(\fir_acc_ff|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_acc_ff|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[1]\);

\blevel_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[2]\ = DFFE(\fir_acc_ff|dffs[24]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[24]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[2]\);

\blevel_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[3]\ = DFFE(\fir_acc_ff|dffs[25]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[25]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[3]\);

\blevel_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[4]\ = DFFE(\fir_acc_ff|dffs[26]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[26]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[4]\);

\blevel_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[5]\ = DFFE(\fir_acc_ff|dffs[27]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[27]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[5]\);

\blevel_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[6]\ = DFFE(\fir_acc_ff|dffs[28]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \fir_acc_ff|dffs[28]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[6]\);

\blevel_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \blevel_ff|dffs[7]\ = DFFE(\fir_acc_ff|dffs[29]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , !\pbusy_out_comb~2\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \fir_acc_ff|dffs[29]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \ALT_INV_pbusy_out_comb~2\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \blevel_ff|dffs[7]\);

\PDone_Out~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \PDone_Out~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PDone_Out);

\Pur_Out~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \u_reg:3:u_reg_i|dffs[25]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur_Out);

\pbusy_out~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \pbusy_out~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_pbusy_out);

\fir_out[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(0));

\fir_out[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(1));

\fir_out[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(2));

\fir_out[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(3));

\fir_out[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(4));

\fir_out[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(5));

\fir_out[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(6));

\fir_out[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(7));

\fir_out[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(8));

\fir_out[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(9));

\fir_out[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(10));

\fir_out[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(11));

\fir_out[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(12));

\fir_out[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(13));

\fir_out[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(14));

\fir_out[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \fir_out_ff|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(15));

\blevel_upd~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_upd~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel_upd);

\blevel[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(0));

\blevel[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(1));

\blevel[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(2));

\blevel[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(3));

\blevel[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(4));

\blevel[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(5));

\blevel[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(6));

\blevel[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \blevel_ff|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_blevel(7));
END structure;


