onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/pbusy
add wave -noupdate -format Logic -radix binary /tb/ad_mr
add wave -noupdate -format Literal -radix hexadecimal /tb/acc_fact
add wave -noupdate -format Logic -radix symbolic /tb/blevel_upd
add wave -noupdate -format logic -radix hexadecimal /tb/blevel_dly
add wave -noupdate -format Literal -radix hexadecimal /tb/blevel_pre
add wave -noupdate -format Literal -radix hexadecimal /tb/blevel_acc
add wave -noupdate -format Literal -radix hexadecimal /tb/blevel
add wave -noupdate -format Literal -radix hexadecimal /tb/pre_level
add wave -noupdate -format Literal -radix hexadecimal /tb/blevel_dly2
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {13713 ns} {34834 ns} {3088 ns}
WaveRestoreZoom {1651 ns} {11220 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
