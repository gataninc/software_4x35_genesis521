-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	APP_BLOCK.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is

end tb;

architecture test_bench of tb is
	signal imr 		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal pbusy		: std_logic;
	signal blevel_pre 	: std_logic_Vector( 15 downto 0 ) := x"0000";
	signal blr_dly		: std_logic_Vector( 11 downto 0 ) := x"004";
	signal blevel		: std_logic_vector( 15 downto 0 );
	signal acc_fact	: std_logic_Vector(  3 downto 0 );
	signal blevel_upd	: std_logic;
	signal blevel_dly 	: std_logic_Vector( 15 downto 0 );
	signal blevel_diff 	: std_logic_Vector( 15 downto 0 );
	signal blevel_acc	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly1 	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly2	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly3	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly4	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly5	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly6	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly7	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly8	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly9	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly10	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly11 	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly12	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly13	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly14	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly15	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly16	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly17	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly18	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly19	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly20	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly21 	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly22	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly23	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly24	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly25	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly26	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly27	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly28	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly29	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly30	: std_logic_Vector( 15 downto 0 );
	signal blevel_dly31 	: std_logic_Vector( 15 downto 0 );

	-------------------------------------------------------------------------------
	component fir_blr is 
		port(
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_Logic;					-- Master Clock
			pbusy		: in		std_Logic;
			acc_fact		: in		std_logic_vector( 3 downto 0 );
			blevel_Pre	: in		std_logic_Vector( 15 downto 0 );
			blevel_upd	: out	std_logic;
			BLEVEL		: out	std_logic_vector( 15 downto 0 ) );
	end component fir_blr;
	-------------------------------------------------------------------------------
begin
--	U : fir_blr
--		port map(
--			imr 			=> imr,
--			clk			=> clk,
--			pbusy		=> pbusy,
---			acc_fact		=> acc_fact,
--	--		blevel_pre	=> blevel_pre,
--	-		blevel_upd	=> blevel_upd,
--			blevel		=> blevel );
			
--	imr 			<= '0' after 725 ns;
	imr 			<= '0' after 11 us;
	clk			<= not( clk ) after 100 ns;	-- 5Mhz
	pbusy 		<= '0';
	bl_pre_proc : process
	begin
		blevel_pre	<= x"0000";
		wait for 20 us;
		blevel_pre	<= x"0005";
		wait for 10 us;
		wait for 400 ns;
		blevel_pre	<= blevel_pre + 1;
		wait for 400 ns;
		blevel_pre	<= blevel_pre + 2;
		wait for 400 ns;
		blevel_pre	<= blevel_pre - 1;
		wait for 400 ns;
		blevel_pre	<= blevel_pre - 2;
		wait for 400 ns;
		blevel_pre	<= blevel_pre + 5;
		wait for 400 ns;
		blevel_pre	<= blevel_pre - 3;
		wait for 400 ns;
		blevel_pre	<= blevel_pre + 1;
		wait ;
	
	end process;
	
	bl_proc : process
	begin
		wait for 20 us;
		acc_fact 		<= x"0";
		for i in 0 to 15 loop
			for j in 0 to i+1 loop
				wait for 5 us;
			end loop;
			acc_fact <= acc_Fact + 1;
		end loop;
	end process;
	
		
	blevel_diff 	<= blevel_pre - blevel_dly;
	blevel		<= x"0" & blevel_acc( 15 downto 4 );
	clock_proc : process( clk )
	begin
		if(( clk'event ) and ( clk = '1' )) then
			blevel_dly31 <= blevel_pre;
			blevel_dly30 <= blevel_dly31;
			blevel_dly29 <= blevel_dly30;
			blevel_dly28 <= blevel_dly29;
			blevel_dly27 <= blevel_dly28;
			blevel_dly26 <= blevel_dly27;
			blevel_dly25 <= blevel_dly26;
			blevel_dly24 <= blevel_dly25;
			blevel_dly23 <= blevel_dly24;
			blevel_dly22 <= blevel_dly23;
			blevel_dly21 <= blevel_dly22;
			blevel_dly20 <= blevel_dly21;
			blevel_dly19 <= blevel_dly20;
			blevel_dly18 <= blevel_dly19;
			blevel_dly17 <= blevel_dly18;
			blevel_dly16 <= blevel_dly17;
			blevel_dly15 <= blevel_dly16;
			blevel_dly14 <= blevel_dly15;
			blevel_dly13 <= blevel_dly14;
			blevel_dly12 <= blevel_dly13;
			blevel_dly11 <= blevel_dly12;
			blevel_dly10 <= blevel_dly11;
			blevel_dly9  <= blevel_dly10;
			blevel_dly8  <= blevel_dly9;
			blevel_dly7  <= blevel_dly8;
			blevel_dly6  <= blevel_dly7;
			blevel_dly5  <= blevel_dly6;
			blevel_dly4  <= blevel_dly5;
			blevel_dly3  <= blevel_dly4;
			blevel_dly2  <= blevel_dly3;
			blevel_dly1  <= blevel_dly2;
			blevel_dly   <= blevel_dly1;
		
		end if;
	end process;
	
	clock_mr_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			blevel_acc <= x"0000";
		elsif(( CLk'event ) and ( CLk = '1' )) then
			blevel_acc <= blevel_acc + blevel_diff;
		end if;
	end process;
------------------------------------------------------------------------------------
end test_bench;               -- Fir_Block
------------------------------------------------------------------------------------