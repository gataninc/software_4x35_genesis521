 ------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_gen
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_blr is 
	port( 
		imr			: in		std_logic;
		clk			: in		std_logic;
		AD_MR		: in		std_logic;
		pbusy		: in		std_logic;
		PDone_In		: in		std_logic;
		Pur_In		: in		std_logic;
		tc_sel		: in		std_logic_vector(  3 downto 0 );
		tlevel		: in		std_logic_vector( 15 downto 0 );
		PS_SEL		: in		std_logic_vector( 1 downto 0 );
		fir_in		: in		std_logic_Vector( 23 downto 0 );
		offset		: in		std_logic_vector( 15 downto 0 );
		PDone_Out		: buffer	std_logic;
		Pur_Out		: buffer	std_logic;
		pbusy_out		: buffer	std_logic;
		fir_out		: buffer	std_logic_Vector( 15 downto 0 );
		blevel_upd	: buffer	std_logic;
		blevel		: buffer	std_logic_Vector( 7 downto 0 ) );
end fir_blr;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_blr is
	constant BLR_WIDTH			: integer := 40;	-- was 40;

	constant pbusy_str_cnt_max8	: integer := 288;
	constant pbusy_str_cnt_max7	: integer := 144;
	constant pbusy_str_cnt_max6	: integer := 72;
	constant pbusy_str_cnt_max5	: integer := 36;
	constant pbusy_str_cnt_max4	: integer := 18;
	constant pbusy_str_cnt_max3	: integer :=  9;
	constant pbusy_str_cnt_max2	: integer :=  4;
	constant pbusy_str_cnt_max1	: integer :=  2;
	constant pbusy_str_cnt_max0	: integer :=  2;
	constant ONE				: std_logic_vector( 15 downto 0 ) := x"0001";


	
	signal fir_in_del0		: std_logic_vector(  23 downto 0 );
	signal fir_in_del1		: std_logic_vector(  23 downto 0 );
	signal fir_in_diff		: std_logic_vector(  23 downto 0 );
	signal fir_diff		: std_logic_vector(  23 downto 0 );
	
	signal fir_diff_vec		: std_logic_Vector(  BLR_WIDTH-1 downto 0 );
	signal fir_sum1 		: std_logic_Vector(  BLR_WIDTH-1 downto 0 );
	signal ps_mux			: std_logic_Vector(  BLR_WIDTH-1 downto 0 );
	signal fir_sum2		: std_logic_Vector(  BLR_WIDTH-1 downto 0 );
	signal fir_acc			: std_logic_Vector(  BLR_WIDTH-1 downto 0 );
	signal PS_Mult			: std_logic_Vector( (3*BLR_WIDTH)-1 downto 0 );
	signal pbusy_del		: std_logic;
	signal pbusy_int		: std_logic;
	signal pbusy_int_out	: std_logic;
	signal clk_cnt			: integer range 0 to 2047;
	signal clk_cnt_max		: integer range 0 to 2047;


	signal pbusy_str_tc		: std_logic;
	signal pbusy_gen_vec	: std_logic_Vector( 1 downto 0 );
	signal pbusy_str		: std_logic;
	signal pbusy_gen		: std_logic;
	signal pbusy_str_cnt	: integer range 0 to pbusy_str_cnt_max8;
	signal pbusy_str_mux	: integer range 0 to pbusy_str_cnt_max8;
	signal pdone_out_vec	: std_logic_Vector( 3 downto 0 );
	signal pbusy_out_comb	: std_logic;
	signal blevel_upd_comb	: std_logic;
	signal blevel_cen		: std_logic;
	signal tlevel_reg		: std_logic_vector( 15 downto 0 );
	signal fir_in_reg		: std_logic_vector( 15 downto 0 );
	signal PS_SEL_Reg		: std_logic_vector( 1 downto 0 );
	signal PDone_Out_Bit	: std_logic;
	signal fir_in_vector	: std_logic_vector ( 139 downto 0 );
	signal offset_ext 		: std_logic_vector( 23 downto 0 );
	signal fir_offset		: std_logic_vector( 23 downto 0 );

begin
	--Adjust BLM Histogram Sample time set equal to Amp Time
	with conv_integer( TC_Sel ) select
		clk_cnt_max <=    3 when 0,				-- 0.4
					   7 when 1,				-- 0.8
					  15 when 2,				-- 1.6
					  31 when 3,				-- 3.2
					  63 when 4,				-- 6.4
					 127 when 5,				-- 12.8
					 144 when 6,				-- 25.6
					 511 when 7,				-- 51.2
					1023 when others;			-- 102.4


	tlevel_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> tlevel,
			q				=> tlevel_reg );
	
	fir_in_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_in( 23 downto 23-15 ),
			q				=> fir_in_reg );

	PS_SEL_REG_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 2,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> PS_SEL,
			q				=> PS_SEL_REG );
	
	----------------------------------------------------------------------------------------------------
	-- Compare the input data against the threshold level to generate an internal pulse busy
	tlevel_cmp : lpm_compare
		generic map(
			LPM_WIDTH				=> 16,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_TYPE				=> "LPM_COMPARE" )
		port map(
			dataa				=> fir_in_reg, -- fir_in( 23 downto 23-15 ),
			datab				=> tlevel_Reg,
			agb					=> pbusy_int );
	----------------------------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------------
	fir_in_vector( 27 downto 0 ) <= pbusy_int & pbusy & pur_in & pdone_in & fir_in;
		
	u_reg : for i in 0 to 3 generate
		u_reg_i : lpm_ff
			generic map(
				LPM_WIDTH	=> 28,
				LPM_TYPE	=> "LPM_FF" )
			port map(
				aclr		=> imr,
				clock	=> clk,
				data		=> fir_in_vector(  (i   *28)+27 downto  i   *28),
				q		=> fir_in_vector( ((i+1)*28)+27 downto (i+1)*28 ) );
	end generate;

	pbusy_int_out 	<= fir_in_vector(139);			
	pbusy_del		<= fir_in_vector(138);
	pur_out		<= fir_in_vector(137);
	PDone_Out_Bit	<= fir_in_vector(136);
	fir_in_del0 	<= fir_in_vector(135 downto 112 );
	----------------------------------------------------------------------------------------------------
	
	----------------------------------------------------------------------------------------------------
	-- Pipeline Stages to calculate the "Difference" to then be accumulated
	fir_in_del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 24,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_in_del0,
			q				=> fir_in_del1 );
			
	fir_diff_add_sub : lpm_add_sub
		generic map( 
			LPM_WIDTH			=> 24,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> fir_in_del0,
			datab			=> fir_in_del1,
			result			=> fir_in_diff );
			
	fir_diff_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 24,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_in_diff,
			q				=> fir_diff );

	-- Now Shift the FIR_DIFF up to keep fractional values
	fir_diff_vec( BLR_WIDTH-1 downto BLR_WIDTH-1-23 ) <= fir_diff;
	fir_diff_Vec_gen : for i in 0 to BLR_WIDTH-1-24 generate
		fir_diff_vec(i) <= '0';
	end generate;
	-- Pipeline Stages to calculate the "Difference" to then be accumulated
	----------------------------------------------------------------------------------------------------


	------------------------------------------------------------------------
	-- Multiplier to form "Decay time"
--	PS_Multiply : LPM_MULT			
--		generic map(
--			LPM_WIDTHA		=> 16,
--			LPM_WIDTHB		=> 16,
--			LPM_WIDTHS  		=> 32,	
--		     LPM_WIDTHP		=> 32,
--		     LPM_REPRESENTATION	=> "SIGNED",
--			lpm_hint 			=> "MAXIMIZE_SPEED=1",
--			LPM_TYPE			=> "LPM_MULT" )
--		port map(
--			dataa			=> fir_Acc( 31 downto 16 ),
--			datab			=> PS,
--			result			=> PS_Mult );
	PS_Mult( (1*BLR_WIDTH)+3 downto (1*BLR_WIDTH) ) 	<= x"0";			-- Med
	PS_Mult( (2*BLR_WIDTH)+7 downto (2*BLR_WIDTH) ) 	<= x"00";			-- High

	PS_Mult_All_Gen : for j in 0 to 2 generate
		-- Sign Extend BLR
		PS_Mult_GenA : for i in (4*j)+16 to BLR_WIDTH-1 generate
			PS_Mult((BLR_WIDTH*j)+i) <= fir_Acc( BLR_WIDTH-1 );
		end generate;

		-- Copy "fir_out" into lower bits of PS_MULT
		PS_Mult( ((BLR_WIDTH+4)*j)+15 downto ((BLR_WIDTH+4)*j) ) <= fir_acc( BLR_WIDTH-1 downto BLR_WIDTH-1-15 );
	end generate;

	-- If Pulse is Busy - No Correction
	PS_Mux_Gen : for i in 0 to BLR_WIDTH-1 generate
		PS_Mux(i) <= '0' 					when ( pbusy_out_comb = '1' ) else
				   PS_Mult(i) 				when ( PS_SEL_REG = "01" ) else 
				   PS_Mult(i+(1*BLR_WIDTH))	when ( PS_SEL_REG = "10" ) else 
				   PS_Mult(i+(2*BLR_WIDTH))	when ( PS_SEL_REG = "11" ) else 				
				   '0';
	end generate;
	-- Multiplier to form "Decay time"
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- Baseline Correction Adder/Subtractor/Accumulator
	fir_sum1_as : lpm_add_sub
		generic map( 
			LPM_WIDTH			=> BLR_WIDTH,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',
			dataa			=> Fir_Acc,
			datab			=> fir_diff_Vec,
			result			=> fir_sum1 );
	
	fir_sum2_as : lpm_add_sub
		generic map( 
			LPM_WIDTH			=> BLR_WIDTH,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> fir_sum1,
			datab			=> PS_Mux,
			result			=> fir_sum2 );
	
	fir_acc_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> BLR_WIDTH,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> AD_MR,
			data				=> fir_sum2,
			q				=> fir_acc );
	-- Baseline Correction Adder/Subtractor/Accumulator
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- One channel subtraction to eliminate a residual offset
	blevel_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 8,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			enable			=> blevel_cen,
--			dataa			=> fir_sum2( BLR_WIDTH-1-8 downto BLR_WIDTH-1-15 ),	-- 10eV/Ch
--			dataa			=> fir_sum2( BLR_WIDTH-2-8 downto BLR_WIDTH-2-15 ),	-- 5eV/Ch
			data				=> fir_acc( BLR_WIDTH-3-8 downto BLR_WIDTH-3-15 ),	-- 2.5eV/Ch
--			data				=> fir_sum2( BLR_WIDTH-3-8 downto BLR_WIDTH-3-15 ),	-- 2.5eV/Ch
			q				=> blevel );
	------------------------------------------------------------------------

	------------------------------------------------------------------------
	-- Ver 4004....
	-- Sign Extend the offset to 24 bits
	offset_ext_gen_a : for i in 16 to 23 generate
		offset_Ext(i) <= offset( 15 );
	end generate;
	offset_ext_gen_b : for i in 0 to 15 generate
		offset_Ext(i) <= offset(i);
	end generate;
	-- Sign Extend the offset to 24 bits
	------------------------------------------------------------------------
	
	------------------------------------------------------------------------
	-- Add the offset to what would be the FIR_OUT which is 40 bits wide	
	fir_out_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 24,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			Cin				=> '0',
			dataa			=> fir_acc( BLR_WIDTH-1 downto BLR_WIDTH-1-23 ),
--			dataa			=> fir_sum2( BLR_WIDTH-1 downto BLR_WIDTH-1-23 ),
			datab			=> offset_ext,
			result			=> fir_offset );
	-- Add the offset to what would be the FIR_OUT which is 40 bits wide	
	------------------------------------------------------------------------
	
	------------------------------------------------------------------------
	-- Now keep only the MSB 16 bits for the output
	fir_out_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_offset( 15+8 downto 8 ),
--			data				=> fir_sum2( BLR_WIDTH-1 downto BLR_WIDTH-1-15 ),	-- 10eV/Ch
			q				=> fir_out );
	-- Now keep only the MSB 16 bits for the output
	------------------------------------------------------------------------
			
	------------------------------------------------------------------------
	pbusy_Gen <= '0' when ( ( pbusy_int 	= '0' ) 
					and ( pbusy_int_out = '0' ) 
					and ( pbusy 		= '0' ) 
					and ( pbusy_del 	= '0' )) else '1';

	pbusy_out_comb <= '1' when (( pbusy_gen 	  = '1' ) 
					or ( pbusy_gen_vec(0) = '1' )
					or ( pbusy_gen_vec(1) = '1' )
					or ( pbusy_str 	  = '1' )) else '0';	

	blevel_cen 		<= not( pbusy_out_comb );

	blevel_upd_comb 	<= '1' when ( clk_cnt = clk_cnt_max ) else '0';

	-- Stretch the Pulse Busy Indication depending on the Amp Time
	with Conv_Integer( TC_Sel ) select
		pbusy_str_mux <= pbusy_str_cnt_max0	when 0,
		                     pbusy_str_cnt_max1	when 1,
		                     pbusy_str_cnt_max2	when 2,
		                     pbusy_str_cnt_max3	when 3,
		                     pbusy_str_cnt_max4	when 4,
		                     pbusy_str_cnt_max5	when 5,
		                     pbusy_str_cnt_max6	when 6,
		                     pbusy_str_cnt_max7	when 7,
		                     pbusy_str_cnt_max8	when others;
		
	pbusy_str_tc 	<= '1' when ( pbusy_str_cnt = pbusy_str_mux  ) else '0';

     -------------------------------------------------------------------------------
	clk_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			pdone_out_vec	<= "0000";
			pDONE_oUT		<= '0';
			pbusy_Gen_Vec 	<= "00";
			pbusy_str		<= '0';
			pbusy_str_cnt	<= 0;
			clk_cnt 		<= 0;
			blevel_upd 	<= '0';
			PBusy_Out		<= '0';
			
		elsif(( clk'event ) and ( clk = '1' )) then
			pdone_out_vec	<= pdone_out_vec( 2 downto 0 ) & PDone_Out_Bit;
			pdone_out		<= pdone_out_vec(3);
		
			pbusy_Gen_Vec <= pbusy_Gen_Vec(0) & pbusy_Gen;
			
			if( pbusy_gen_vec = "10" )
				then pbusy_str <= '1';
			elsif( pbusy_str_tc = '1' )
				then pbusy_str <= '0';
			end if;
		
		
			if( pbusy_str = '0' )
				then pbusy_str_Cnt <= 0;
				else pbusy_str_cnt <= pbusy_str_Cnt + 1;
			end if;
			
			if( blevel_upd_comb = '1' )
				then clk_cnt <= 0;
			elsif( pbusy_out_comb = '0' )
				then clk_cnt <= clk_cnt + 1;
			end if;
	
			blevel_upd 	<= blevel_upd_comb;
			pbusy_out 	<= pbusy_out_comb;
			
		end if;
	end process;
     -------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- fir_blr
------------------------------------------------------------------------------------
			
