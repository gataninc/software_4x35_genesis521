-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_disc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the Medium and Low Speed FIR data 
--		It includes the Delay Lines with the appropriate address
--		counters.
--	
--		It also generates the "Threshold Level" comparison for the
--		input to the Pulse Steering Logic
--
--		fir = ( adata - 2*adata_del1 ) + adata_del2
--
--		High Speed 	= 0.4us
--		Medium Speed 	= 0.8us
--		Low Speed 	= 6.4us
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_disc is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		AD_MR		: in		std_logic;
		PA_Reset		: in		std_logic;
		adata		: in		std_logic_Vector( 47 downto 0 );
		DSelect		: in		std_logic_vector(  1 downto 0 );					-- 0 = High 1 = Medium, 2 = Low
		Disc_En		: in		std_logic;
		tlevel		: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
		Level_Out		: buffer	std_logic;					-- Discriminator Level Exceeded	
		Level_Cmp		: buffer	std_logic;
		fir_out		: buffer	std_logic_Vector( 15 downto 0 ) );	-- FIR Output ( for Debug )
end fir_disc;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_disc is
	constant Low_LSB 		: integer := 2;		-- was 1;	-- was 3
	constant Med_LSB 		: integer := 3;		-- was 2;	-- was 4
	constant Hi_LSB  		: integer := 5;		-- was 4

	Constant Din_MSB		: integer := 15;
	constant Acc_Bits		: integer := 27;
	constant Slope_Vec_Max	: integer := 5;	-- was 6;
	constant TLEVEL_EXT_LSB 	: integer := 4;

	signal adata_del 		: std_logic_Vector( 15 downto 0 );
	signal adata_Del1		: std_logic_Vector( 15 downto 0 );
	signal adata_del2		: std_logic_Vector( 15 downto 0 );
	signal adata_reg		: std_logic_Vector( 47 downto 0 );

	signal add_vec1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal add_vec2		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_vec1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total2a		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total2		: std_logic_Vector( Acc_Bits-1 downto 0 );
	
	signal fir_Data		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal fir_data_sub		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal tlevel_ext0		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal tlevel_ext1		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal tlevel_ext2		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal tlevel_extm		: std_logic_vector( Acc_Bits-1 downto 0 );


	signal wadr			: std_logic_vector( 2 downto 0 );
	signal radr			: std_logic_Vector( 2 downto 0 );

	signal fir_data_Del1	: std_logic_vector( Acc_Bits-1 downto 0 );
	signal fir_data_Del3 	: std_logic_vector( Acc_Bits-1 downto 0 );
	signal fir_data_DelM 	: std_logic_vector( Acc_Bits-1 downto 0 );
	
	signal iPSlope			: std_logic;
	signal pslope_vec		: std_logic_vector( slope_Vec_max-1 downto 0 );
	signal PSlope			: std_logic;
	signal PSlope_Del		: std_logic;
	signal dselect_reg		: std_logic_vector( 1 downto 0 );
	signal tlevel_reg		: std_logic_vector( 15 downto 0 );

begin
	dselect_reg_ff: lpm_ff
		generic map(
			LPM_WIDTH			=> 2,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			data				=> dselect,
			q				=> dselect_reg );
	
	tlevel_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			data				=> tlevel,
			q				=> tlevel_reg );

	adata_Reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 48 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> adata,
			q				=> adata_reg );
		
	adata_del		<= adata_reg( 15 downto  0 );
	adata_del1	<= adata_reg( 31 downto 16 );
	adata_del2	<= adata_reg( 47 downto 32 );

	-------------------------------------------------------------------------------
	-- Create the three Vectors for the Filter Generation 
	-- Gain = X2
	sext1_gen : for i in Din_MSB+1 to acc_bits-1 generate
		add_vec1(i) <= adata_del(Din_MSB);
		add_vec2(i) <= adata_del2(Din_MSB);
	end generate;
	
	add_Vec1( Din_MSB downto 0 ) <= adata_del;
	add_vec2( Din_MSB downto 0 ) <= adata_del2;

	sext2_gen : for i in Din_MSB+2 to acc_bits-1 generate
		sub_vec1(i) <= adata_Del1(Din_MSB);
	end generate;
	sub_vec1( Din_MSB+1 downto 0 ) <= adata_del1 & '0';
	-- Create the three Vectors for the Filter Generation
	--------------------------------------------------------------------------

	--------------------------------------------------------------------------
	-- FIR - First Stage 
	sub1 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTIOn		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> add_Vec1,			-- adata
			datab			=> sub_vec1,			-- 2Xadata_del1
			result			=> sub_total1 );		-- dataa-datab
	-- FIR - First Stage 
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- FIR - Second Stage 
	add1 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',
			dataa			=> sub_total1,		-- ( dataa-datab)-datab_del3
			datab			=> add_vec2,		-- adata_del2
			result			=> sub_total2a );	-- (( dataa-datab)-datab_del3 ) + datac_del3
			
	add1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			data				=> sub_total2a,
			q				=> sub_total2 );
	-- FIR - Second Stage 
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- FIR - Third Stage 
	-- Accumulate the previous sub_total with the FIR value	
	add2a : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTIOn		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',
			dataa			=> sub_total2,
			datab			=> fir_data,
			result			=> fir_data_sub );

	add2b_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			sclr				=> AD_MR,	
			clock			=> CLK,
			data				=> fir_data_sub,
			q				=> fir_data );
	-- FIR - Third Stage 
     --------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Sign Extend Threshold Level for High and Medium Speed Threshold Levels
	tlevel_ext_gen0a : for i in Low_LSB+16 to Acc_Bits-1 	generate tlevel_ext0(i)			<= TLevel_Reg(15);	end generate;
	tlevel_ext_gen1a : for i in Med_LSB+16 to Acc_Bits-1 	generate tlevel_ext1(i) 			<= TLevel_Reg(15);	end generate;
	tlevel_ext_gen2a : for i in Hi_LSB+16  to Acc_Bits-1 	generate tlevel_ext2(i) 			<= TLevel_Reg(15);	end generate;

	
	tlevel_ext_gen0b : for i in 0 to 15 				generate tlevel_ext0(i+Low_LSB)	<= TLevel_Reg(i); end generate;
	tlevel_ext_gen1b : for i in 0 to 15 				generate tlevel_ext1(i+Med_LSB)	<= TLevel_Reg(i); end generate;
	tlevel_ext_gen2b : for i in 0 to 15 				generate tlevel_ext2(i+Hi_LSB)	<= TLevel_Reg(i); end generate;
		
	tlevel_ext_gen0c : for i in 0 to Low_LSB-1 			generate tlevel_ext0(i) 			<= '0';		end generate;	
	tlevel_ext_gen1c : for i in 0 to Med_LSB-1 			generate tlevel_ext1(i) 			<= '0';		end generate;	
	tlevel_ext_gen2c : for i in 0 to Hi_LSB-1  			generate tlevel_ext2(i) 			<= '0';		end generate;	
	-- Sign Extend Threshold Level for High and Medium Speed Threshold Levels
	---------------------------------------------------------------------------
	with Conv_Integer( DSelect_Reg ) select
		tlevel_extm <= tlevel_ext0 when 0,				-- High
					tlevel_ext1 when 1,				-- Med
					tlevel_ext2 when others;			-- Low

	tlevel_cmp : lpm_compare
		generic map(
			lpm_width			=> Acc_Bits,
			lpm_representation	=> "SIGNED",
			lpm_type			=> "LPM_COMPARE",
			lpm_pipeline		=> 1 )		
		port map(
			aclr				=> imr,
			clock			=> clk,
			dataa			=> fir_data,
			datab			=> tlevel_extm, 
			agb				=> Level_Cmp );		
     --------------------------------------------------------------------------
		
     --------------------------------------------------------------------------
	-- Slope Determination ---------------------------------------------------
	wadr_cntr : lpm_Counter
		generic map(
			lpm_width				=> 3,
			lpm_type				=> "LPM_COUNTER" )
		port map(
			aclr					=> imr,
			clock				=> clk,
			q					=> wadr );
	
	radr <= wadr - "010";

	fir_data_Del1_ff : lpm_ff
		generic map(
			LPM_WIDTH				=> Acc_Bits,
			LPM_TYPE				=> "LPM_FF" )
		port map(
			aclr					=> imr,
			clock				=> clk,
			data					=> fir_data,
			q					=> fir_data_del1 );

	Dly1 : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Acc_Bits,	-- Data Width
			LPM_WIDTHAD			=> 3,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT.MIF",
			LPM_TYPE				=> "LPM_RAM_DP" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress				=> radr,
			wraddress 			=> wadr,
			data					=> fir_data,
			q					=> fir_data_del3  );		
			
	with Conv_Integer( DSelect_Reg ) select
		Fir_data_DelM <= fir_data_Del1 when 0,		-- High
					  fir_data_Del1 when 1,		-- Medium
					  fir_data_Del3 when others;	-- Low

	PSlope_Gen_Comp : LPM_COMPARE
		generic map(
			LPM_WIDTH				=> Acc_Bits,
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_TYPE				=> "LPM_COMPARE",
			LPM_PIPELINE			=> 1 )
		port map	(
			aclr					=> imr,
			clock				=> clk,			
			dataa				=> fir_data,
			datab				=> fir_data_delm,
			agb					=> ipslope );		
	-- Slope Determination ---------------------------------------------------
     --------------------------------------------------------------------------

	clock_proc : process( imr, clk )
	
	begin 
		if( imr = '1' ) then
			for i in 0 to slope_vec_max-1 loop
				pslope_vec(i) <= '0';
			end loop;
			PSlope		<= '0';
			PSlope_Del	<= '0';
			Level_out		<= '0';
			Fir_Out		<= x"0000";
			
		elsif(( clk'event ) and ( clk = '1' )) then
			pslope_vec <= pslope_vec( slope_vec_max-2 downto 0 ) & ipslope;
			
			
			if(   ( DSelect_Reg(1) = '0' ) and ( PSlope_Vec( 1 downto 0 ) = "11"    ) and ( ipslope = '1' )) then PSlope <= '1';
			elsif(( DSelect_Reg(1) = '1' ) and ( PSlope_Vec( 4 downto 0 ) = "11111" ) and ( ipslope = '1' )) then PSlope <= '1';
			elsif(( DSelect_Reg(1) = '0' ) and ( PSlope_Vec( 1 downto 0 ) = "00"    ) and ( ipslope = '0' )) then PSlope <= '0';
			elsif(( DSelect_Reg(1) = '1' ) and ( PSlope_Vec( 4 downto 0 ) = "00000" ) and ( ipslope = '0' )) then PSlope <= '0';
			end if;
										
			PSlope_Del <= PSlope;
			
			if(( PA_Reset = '0' ) and ( Disc_En = '1' ) and ( Level_Cmp = '1' ) and ( Pslope = '0' ) and ( PSlope_Del = '1' ))
				then LeveL_out <= '1';
				else LeveL_out <= '0';
			end if;

			if( PA_Reset = '1' )
				then fir_out <= x"0000";
			else
				case Conv_Integer( DSelect_Reg ) is
					when 0 	  => Fir_out <= FIr_data( 15+Low_LSB downto Low_LSB );		-- High
					when 1 	  => Fir_out <= FIr_data( 15+Med_LSB downto Med_LSB );		-- Medium
					when others => Fir_out <= FIr_data( 15+Hi_LSB  downto Hi_LSB );		-- Low
				end case;
			end if;
		end if;
	end process;
     --------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- fir_disc
------------------------------------------------------------------------------------

