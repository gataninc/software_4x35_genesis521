vcom -reportprogress 300 -work work {fir_disc.vho}
vcom -reportprogress 200 -work work {fir_dmem.vhd}
vcom -reportprogress 300 -work work {_tb.vhd}
vsim -sdftyp /U=fir_disc_vhd.sdo work.tb 
do wave.do
run 45us