
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity fir_dmem is
	port ( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		adata		: in		std_logic_Vector( 14 downto 0 );
		Disc_Mem		: out 	std_logic_Vector( 44 downto 0 ) );
end fir_dmem;

architecture behavoiral of fir_dmem is

	type mem_type 	is array( 0 to 7 ) of std_logic_Vector( 14 downto 0 );

	signal mem1 	: mem_type;
	signal mem2 	: mem_type;
	signal wadr 	: std_logic_vector( 2 downto 0 );
	signal radr 	: std_logic_vector( 2 downto 0 );
	
begin
	radr					<= wadr - 8;		-- 400ns rise

     -------------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			wadr 	<= "000";
			disc_mem	<= conv_std_logic_vector( 0, 45 );
			
			for i in 0 to 7 loop
				mem1(i) <= conv_std_logic_vector( 0, 15 );
				mem2(i) <= conv_std_logic_vector( 0, 15 );
			end loop;
		elsif(( clk'event ) and ( clk = '1' )) then
			wadr 					<= wadr + 1;
			disc_mem( 14 downto  0 ) 	<= adata;
			
			mem1( conv_integer( wadr)) 	<= adata;
			disc_mem( 29 downto 15 ) 	<= mem1( conv_integer( radr ));
			
			mem2( conv_integer( wadr ))	<= mem1( conv_integer( radr ));
			disc_mem( 44 downto 30 )		<= mem2( conv_integer( radr ));
		end if;
	end process;
     -------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavoiral;               -- fir_mdisc
------------------------------------------------------------------------------------
