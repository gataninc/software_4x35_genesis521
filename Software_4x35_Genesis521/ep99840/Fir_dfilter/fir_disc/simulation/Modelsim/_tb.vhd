
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is

	signal imr			: std_logic := '1';					-- Master Reset
	signal clk			: std_logic := '0';					-- MAster Clock
	signal AD_MR			: std_logic;
	signal adata			: std_logic_Vector( 15 downto 0 );
	signal Disc_Mem		: std_logic_Vector( 44 downto 0 );
	signal DSelect 		: std_logic_vector(  1 downto 0 );
	signal Disc_En			: std_logic;
	signal tlevel			: std_logic_Vector( 15 downto 0 );
	signal fir_Data		: std_logic_vector( 15 downto 0 );
	signal Disc_in			: std_logic;
	signal Disc_Cmp		: std_logic;
	signal PA_Reset		: stD_logic;
	-------------------------------------------------------------------------------
	component fir_dmem 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			adata		: in		std_logic_Vector( 14 downto 0 );
			Disc_Mem		: out 	std_logic_Vector( 44 downto 0 ) );
	end component fir_dmem;
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	component fir_disc
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			AD_MR		: in		std_logic;
			PA_Reset		: in		std_logic;
			adata		: in		std_logic_Vector( 44 downto 0 );
			DSelect		: in		std_logic_vector(  1 downto 0 );					-- 0 = High 1 = Medium, 2 = Low
			Disc_En		: in		std_logic;
			tlevel		: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
			Level_Out		: out	std_logic;					-- Discriminator Level Exceeded	
			Level_Cmp		: out	std_logic;
			fir_out		: out	std_logic_Vector( 15 downto 0 ) );	-- FIR Output ( for Debug )
	end component;
	-------------------------------------------------------------------------------
begin
	-------------------------------------------------------------------------------
	-- Medium and Low FIR Generation ( 2us and 8us digital FIR Generation )
	tlevel 		<= x"0010";
	DSelect		<= "00";
	Disc_En		<= '1';
	IMR 			<= '0' after 800 ns;
	CLK 			<= not( CLK ) after 25 ns;
	PA_Reset		<= '0';
	
	UM : fir_dmem
		port map(
			imr					=> imr,
			clk					=> clk,
			adata				=> adata( 14 downto 0 ),
			disc_mem				=> disc_mem );

	U : fir_disc 
		port map(
			imr				=> imr,								-- Master Clock
			clk				=> clk,								-- Master Reset
			AD_MR			=> AD_MR,								-- A/D Master Reset (Fir Reset )
			PA_Reset			=> PA_Reset,
			DSelect			=> DSelect,
			adata			=> disc_mem,					-- A/D Input 
			Disc_En			=> Disc_En,					-- Discriminator Enable
			tlevel			=> tlevel,					-- Threhsold Level
			Level_Out			=> Disc_In,					-- Discrminator Output
			level_Cmp			=> disc_Cmp,
			fir_out			=> fir_data );


--	PA_Reset_Proc : process
--	begin
--		PA_Reset <= '0';
--		wait for 30 us;
--		PA_Reset <= '1';
--		wait for 15 us;
--		PA_Reset <= '0';
--	end process;
	
	main_proc : process
	begin
		AD_MR	<= '0';
		adata	<=  x"0000";
		wait until (( IMR'Event ) and ( IMR = '0' ));
		wait for 1 us;
		wait until(( clk'event ) and ( clk = '1' ));
		AD_MR	<= '1';
		wait until(( clk'event ) and ( clk = '1' ));
		AD_MR	<= '0';
		
		adata	<= x"0000";

		wait for 10 us;
		AD_MR 	<= '0';

		wait until(( clk'event ) and ( clk = '1' ));
		adata <= adata + x"0123";
		wait for 20 us;
		
		wait until(( clk'event ) and ( clk = '1' ));
		adata <= adata + x"0123";
		wait for 20 us;

		wait until(( clk'event ) and ( clk = '1' ));
		adata <= adata + x"0123";
		wait for 4 us;
		
		wait until(( clk'event ) and ( clk = '1' ));
		adata <= adata + x"0123";
		wait for 20 us;
		
		wait until(( clk'event ) and ( clk = '1' ));
		adata <= x"2000";
		wait for 20 us;
		
		forever_loop : loop
			wait until(( clk'event ) and ( clk = '1' ));
			adata <= adata + x"0123";
			wait for 5 us;
		end loop;
	end process;
	
	
------------------------------------------------------------------------------------
end test_bench;               -- fir_mdisc
------------------------------------------------------------------------------------
