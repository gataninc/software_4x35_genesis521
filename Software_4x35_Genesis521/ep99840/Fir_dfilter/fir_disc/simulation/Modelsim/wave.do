onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk
add wave -noupdate -format Logic -radix hexadecimal /tb/ad_mr
add wave -noupdate -format Logic -radix hexadecimal /tb/pa_reset
add wave -noupdate -format Logic -radix binary /tb/disc_en
add wave -noupdate -format Literal -radix hexadecimal /tb/dselect
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
add wave -noupdate -format Literal -radix hexadecimal /tb/disc_mem
add wave -noupdate -format Literal -radix hexadecimal /tb/tlevel
add wave -noupdate -format Logic -radix binary /tb/disc_in
add wave -noupdate -format Logic -radix binary /tb/disc_cmp
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.125 /tb/fir_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {12483 ns}
WaveRestoreZoom {11616 ns} {13467 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 78
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
