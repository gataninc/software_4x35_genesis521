-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:40:52"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_disc IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	AD_MR : IN std_logic;
	PA_Reset : IN std_logic;
	adata : IN std_logic_vector(47 DOWNTO 0);
	DSelect : IN std_logic_vector(1 DOWNTO 0);
	Disc_En : IN std_logic;
	tlevel : IN std_logic_vector(15 DOWNTO 0);
	Level_Out : OUT std_logic;
	Level_Cmp : OUT std_logic;
	fir_out : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_disc;

ARCHITECTURE structure OF fir_disc IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_AD_MR : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_adata : std_logic_vector(47 DOWNTO 0);
SIGNAL ww_DSelect : std_logic_vector(1 DOWNTO 0);
SIGNAL ww_Disc_En : std_logic;
SIGNAL ww_tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Level_Out : std_logic;
SIGNAL ww_Level_Cmp : std_logic;
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a26_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a26_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a24_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a24_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a25_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a25_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a22_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a22_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a23_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a23_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a20_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a20_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a21_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a21_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a18_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a18_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a19_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a19_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a16_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a16_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a17_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a17_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly1_asram_asegment_a0_a_a19_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a26_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a25_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a24_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a23_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a22_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a18_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a21_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a20_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a17_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a16_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3 : std_logic;
SIGNAL PSlope_a190 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4 : std_logic;
SIGNAL add1_ff_adffs_a3_a : std_logic;
SIGNAL add1_ff_adffs_a2_a : std_logic;
SIGNAL add1_ff_adffs_a6_a : std_logic;
SIGNAL add1_ff_adffs_a7_a : std_logic;
SIGNAL add1_ff_adffs_a8_a : std_logic;
SIGNAL add1_ff_adffs_a9_a : std_logic;
SIGNAL add1_ff_adffs_a12_a : std_logic;
SIGNAL add1_ff_adffs_a13_a : std_logic;
SIGNAL add1_ff_adffs_a14_a : std_logic;
SIGNAL add1_ff_adffs_a15_a : std_logic;
SIGNAL add1_ff_adffs_a16_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5 : std_logic;
SIGNAL adata_Reg_ff_adffs_a37_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a35_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a34_a : std_logic;
SIGNAL add1_ff_adffs_a1_a : std_logic;
SIGNAL add2b_ff_adffs_a0_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a38_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a36_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a39_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a40_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a41_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a42_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a43_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a44_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a45_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a46_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a47_a : std_logic;
SIGNAL fir_out_a0_a_a1576 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_aagb_out : std_logic;
SIGNAL adata_Reg_ff_adffs_a20_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a18_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a17_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a33_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390 : std_logic;
SIGNAL adata_Reg_ff_adffs_a21_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a19_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a22_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a23_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a24_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a25_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a26_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a27_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a13_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a14_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a30_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a31_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a26_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL tlevel_extm_a18_a_a1393 : std_logic;
SIGNAL tlevel_extm_a19_a_a1394 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_reg_ff_adffs_a11_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL adata_Reg_ff_adffs_a16_a : std_logic;
SIGNAL adata_Reg_ff_adffs_a32_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_reg_ff_adffs_a9_a : std_logic;
SIGNAL tlevel_extm_a14_a_a1399 : std_logic;
SIGNAL tlevel_extm_a14_a_a1400 : std_logic;
SIGNAL tlevel_reg_ff_adffs_a10_a : std_logic;
SIGNAL tlevel_extm_a15_a_a1401 : std_logic;
SIGNAL tlevel_extm_a15_a_a1402 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL tlevel_reg_ff_adffs_a7_a : std_logic;
SIGNAL tlevel_extm_a12_a_a1403 : std_logic;
SIGNAL tlevel_extm_a12_a_a1404 : std_logic;
SIGNAL tlevel_extm_a13_a_a1405 : std_logic;
SIGNAL tlevel_extm_a13_a_a1406 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_reg_ff_adffs_a5_a : std_logic;
SIGNAL tlevel_reg_ff_adffs_a6_a : std_logic;
SIGNAL tlevel_extm_a11_a_a1409 : std_logic;
SIGNAL tlevel_extm_a11_a_a1410 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a18_a : std_logic;
SIGNAL Dly1_asram_aq_a19_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a19_a : std_logic;
SIGNAL fir_data_DelM_a19_a_a274 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_reg_ff_adffs_a3_a : std_logic;
SIGNAL tlevel_extm_a8_a_a1411 : std_logic;
SIGNAL tlevel_extm_a8_a_a1412 : std_logic;
SIGNAL tlevel_reg_ff_adffs_a4_a : std_logic;
SIGNAL tlevel_extm_a9_a_a1413 : std_logic;
SIGNAL tlevel_extm_a9_a_a1414 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL tlevel_extm_a6_a_a1415 : std_logic;
SIGNAL tlevel_extm_a6_a_a1416 : std_logic;
SIGNAL tlevel_extm_a7_a_a1417 : std_logic;
SIGNAL tlevel_extm_a7_a_a1418 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a386 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139 : std_logic;
SIGNAL Dly1_asram_aq_a12_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a12_a : std_logic;
SIGNAL fir_data_DelM_a12_a_a279 : std_logic;
SIGNAL Dly1_asram_aq_a13_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a13_a : std_logic;
SIGNAL fir_data_DelM_a13_a_a280 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a387 : std_logic;
SIGNAL Dly1_asram_aq_a6_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a6_a : std_logic;
SIGNAL fir_data_DelM_a6_a_a285 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a597 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a5_a : std_logic;
SIGNAL Dly1_asram_aq_a5_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_a_a00006 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a388 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a598 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a_a389 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a599 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609 : std_logic;
SIGNAL Dly1_asram_aq_a4_a : std_logic;
SIGNAL fir_data_DelM_a4_a_a291 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a4_a : std_logic;
SIGNAL fir_data_DelM_a4_a_a292 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a600 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610 : std_logic;
SIGNAL Dly1_asram_aq_a3_a : std_logic;
SIGNAL fir_data_DelM_a3_a_a295 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a3_a : std_logic;
SIGNAL fir_data_DelM_a3_a_a296 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a601 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611 : std_logic;
SIGNAL Dly1_asram_aq_a2_a : std_logic;
SIGNAL fir_data_DelM_a2_a_a299 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a2_a : std_logic;
SIGNAL fir_data_DelM_a2_a_a300 : std_logic;
SIGNAL Dly1_asram_aq_a1_a : std_logic;
SIGNAL fir_data_DelM_a1_a_a301 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a1_a : std_logic;
SIGNAL fir_data_DelM_a1_a_a302 : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a0_a : std_logic;
SIGNAL Dly1_asram_aq_a0_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641 : std_logic;
SIGNAL adata_a37_a_acombout : std_logic;
SIGNAL adata_a35_a_acombout : std_logic;
SIGNAL adata_a34_a_acombout : std_logic;
SIGNAL adata_a38_a_acombout : std_logic;
SIGNAL adata_a36_a_acombout : std_logic;
SIGNAL adata_a39_a_acombout : std_logic;
SIGNAL adata_a40_a_acombout : std_logic;
SIGNAL adata_a41_a_acombout : std_logic;
SIGNAL adata_a42_a_acombout : std_logic;
SIGNAL adata_a43_a_acombout : std_logic;
SIGNAL adata_a44_a_acombout : std_logic;
SIGNAL adata_a45_a_acombout : std_logic;
SIGNAL adata_a46_a_acombout : std_logic;
SIGNAL adata_a47_a_acombout : std_logic;
SIGNAL adata_a20_a_acombout : std_logic;
SIGNAL adata_a18_a_acombout : std_logic;
SIGNAL adata_a17_a_acombout : std_logic;
SIGNAL adata_a33_a_acombout : std_logic;
SIGNAL adata_a21_a_acombout : std_logic;
SIGNAL adata_a19_a_acombout : std_logic;
SIGNAL adata_a22_a_acombout : std_logic;
SIGNAL adata_a23_a_acombout : std_logic;
SIGNAL adata_a24_a_acombout : std_logic;
SIGNAL adata_a25_a_acombout : std_logic;
SIGNAL adata_a26_a_acombout : std_logic;
SIGNAL adata_a27_a_acombout : std_logic;
SIGNAL adata_a13_a_acombout : std_logic;
SIGNAL adata_a14_a_acombout : std_logic;
SIGNAL adata_a30_a_acombout : std_logic;
SIGNAL adata_a31_a_acombout : std_logic;
SIGNAL tlevel_a11_a_acombout : std_logic;
SIGNAL adata_a16_a_acombout : std_logic;
SIGNAL adata_a32_a_acombout : std_logic;
SIGNAL tlevel_a9_a_acombout : std_logic;
SIGNAL tlevel_a10_a_acombout : std_logic;
SIGNAL tlevel_a7_a_acombout : std_logic;
SIGNAL tlevel_a5_a_acombout : std_logic;
SIGNAL tlevel_a6_a_acombout : std_logic;
SIGNAL tlevel_a3_a_acombout : std_logic;
SIGNAL tlevel_a4_a_acombout : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL DSelect_a1_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL dselect_reg_ff_adffs_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL Dly1_asram_asegment_a0_a_a26_a_a1 : std_logic;
SIGNAL add_a28 : std_logic;
SIGNAL Dly1_asram_aq_a26_a : std_logic;
SIGNAL adata_a15_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a15_a : std_logic;
SIGNAL adata_a29_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a29_a : std_logic;
SIGNAL adata_a28_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a28_a : std_logic;
SIGNAL adata_a12_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a12_a : std_logic;
SIGNAL adata_a11_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a11_a : std_logic;
SIGNAL adata_a10_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a10_a : std_logic;
SIGNAL adata_a9_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a9_a : std_logic;
SIGNAL adata_a8_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a8_a : std_logic;
SIGNAL adata_a7_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a7_a : std_logic;
SIGNAL adata_a6_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a6_a : std_logic;
SIGNAL adata_a5_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a5_a : std_logic;
SIGNAL adata_a4_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a4_a : std_logic;
SIGNAL adata_a3_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a3_a : std_logic;
SIGNAL adata_a2_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a2_a : std_logic;
SIGNAL adata_a1_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a1_a : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a599 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a543 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a539 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a551 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a535 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a547 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a555 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a559 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a563 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a567 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a571 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a575 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a579 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a583 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a587 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a591 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a593 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a589 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a585 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a581 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a577 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a573 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a569 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a565 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a561 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a557 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a553 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a545 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a533 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a549 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a537 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a541 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a597 : std_logic;
SIGNAL adata_a0_a_acombout : std_logic;
SIGNAL adata_Reg_ff_adffs_a0_a : std_logic;
SIGNAL add1_ff_adffs_a0_a_a178 : std_logic;
SIGNAL add1_ff_adffs_a1_a_a175 : std_logic;
SIGNAL add1_ff_adffs_a2_a_a130 : std_logic;
SIGNAL add1_ff_adffs_a3_a_a127 : std_logic;
SIGNAL add1_ff_adffs_a4_a_a136 : std_logic;
SIGNAL add1_ff_adffs_a5_a_a124 : std_logic;
SIGNAL add1_ff_adffs_a6_a_a133 : std_logic;
SIGNAL add1_ff_adffs_a7_a_a139 : std_logic;
SIGNAL add1_ff_adffs_a8_a_a142 : std_logic;
SIGNAL add1_ff_adffs_a9_a_a145 : std_logic;
SIGNAL add1_ff_adffs_a10_a_a148 : std_logic;
SIGNAL add1_ff_adffs_a11_a_a151 : std_logic;
SIGNAL add1_ff_adffs_a12_a_a154 : std_logic;
SIGNAL add1_ff_adffs_a13_a_a157 : std_logic;
SIGNAL add1_ff_adffs_a14_a_a160 : std_logic;
SIGNAL add1_ff_adffs_a15_a_a163 : std_logic;
SIGNAL add1_ff_adffs_a16_a_a166 : std_logic;
SIGNAL add1_ff_adffs_a17_a_a169 : std_logic;
SIGNAL add1_ff_adffs_a26_a : std_logic;
SIGNAL add1_ff_adffs_a17_a : std_logic;
SIGNAL add1_ff_adffs_a11_a : std_logic;
SIGNAL add1_ff_adffs_a10_a : std_logic;
SIGNAL add1_ff_adffs_a5_a : std_logic;
SIGNAL add1_ff_adffs_a4_a : std_logic;
SIGNAL add1_ff_adffs_a0_a : std_logic;
SIGNAL AD_MR_acombout : std_logic;
SIGNAL add2b_ff_adffs_a0_a_a336 : std_logic;
SIGNAL add2b_ff_adffs_a1_a : std_logic;
SIGNAL add2b_ff_adffs_a1_a_a324 : std_logic;
SIGNAL add2b_ff_adffs_a2_a : std_logic;
SIGNAL add2b_ff_adffs_a2_a_a273 : std_logic;
SIGNAL add2b_ff_adffs_a3_a : std_logic;
SIGNAL add2b_ff_adffs_a3_a_a270 : std_logic;
SIGNAL add2b_ff_adffs_a4_a_a279 : std_logic;
SIGNAL add2b_ff_adffs_a5_a_a267 : std_logic;
SIGNAL add2b_ff_adffs_a6_a : std_logic;
SIGNAL add2b_ff_adffs_a6_a_a276 : std_logic;
SIGNAL add2b_ff_adffs_a7_a : std_logic;
SIGNAL add2b_ff_adffs_a7_a_a282 : std_logic;
SIGNAL add2b_ff_adffs_a8_a : std_logic;
SIGNAL add2b_ff_adffs_a8_a_a285 : std_logic;
SIGNAL add2b_ff_adffs_a9_a : std_logic;
SIGNAL add2b_ff_adffs_a9_a_a288 : std_logic;
SIGNAL add2b_ff_adffs_a10_a_a291 : std_logic;
SIGNAL add2b_ff_adffs_a11_a_a294 : std_logic;
SIGNAL add2b_ff_adffs_a12_a : std_logic;
SIGNAL add2b_ff_adffs_a12_a_a297 : std_logic;
SIGNAL add2b_ff_adffs_a13_a : std_logic;
SIGNAL add2b_ff_adffs_a13_a_a300 : std_logic;
SIGNAL add2b_ff_adffs_a14_a : std_logic;
SIGNAL add2b_ff_adffs_a14_a_a303 : std_logic;
SIGNAL add2b_ff_adffs_a15_a : std_logic;
SIGNAL add2b_ff_adffs_a15_a_a306 : std_logic;
SIGNAL add2b_ff_adffs_a16_a : std_logic;
SIGNAL add2b_ff_adffs_a16_a_a309 : std_logic;
SIGNAL add2b_ff_adffs_a17_a_a312 : std_logic;
SIGNAL add2b_ff_adffs_a18_a_a315 : std_logic;
SIGNAL add2b_ff_adffs_a19_a_a318 : std_logic;
SIGNAL add2b_ff_adffs_a20_a_a321 : std_logic;
SIGNAL add2b_ff_adffs_a21_a : std_logic;
SIGNAL add2b_ff_adffs_a21_a_a345 : std_logic;
SIGNAL add2b_ff_adffs_a22_a : std_logic;
SIGNAL add2b_ff_adffs_a22_a_a342 : std_logic;
SIGNAL add2b_ff_adffs_a23_a : std_logic;
SIGNAL add2b_ff_adffs_a23_a_a339 : std_logic;
SIGNAL add2b_ff_adffs_a24_a : std_logic;
SIGNAL add2b_ff_adffs_a24_a_a333 : std_logic;
SIGNAL add2b_ff_adffs_a25_a : std_logic;
SIGNAL add2b_ff_adffs_a25_a_a330 : std_logic;
SIGNAL add2b_ff_adffs_a26_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out : std_logic;
SIGNAL Dly1_asram_aq_a25_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a25_a : std_logic;
SIGNAL fir_data_DelM_a25_a_a268 : std_logic;
SIGNAL Dly1_asram_aq_a24_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a24_a : std_logic;
SIGNAL fir_data_DelM_a24_a_a267 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL Dly1_asram_aq_a23_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a23_a : std_logic;
SIGNAL fir_data_DelM_a23_a_a270 : std_logic;
SIGNAL Dly1_asram_aq_a22_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a22_a : std_logic;
SIGNAL fir_data_DelM_a22_a_a269 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL add2b_ff_adffs_a19_a : std_logic;
SIGNAL add2b_ff_adffs_a18_a : std_logic;
SIGNAL Dly1_asram_aq_a18_a : std_logic;
SIGNAL fir_data_DelM_a18_a_a273 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL add2b_ff_adffs_a20_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a21_a : std_logic;
SIGNAL Dly1_asram_aq_a21_a : std_logic;
SIGNAL fir_data_DelM_a21_a_a272 : std_logic;
SIGNAL Dly1_asram_aq_a20_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a20_a : std_logic;
SIGNAL fir_data_DelM_a20_a_a271 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL Dly1_asram_aq_a17_a : std_logic;
SIGNAL add2b_ff_adffs_a17_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a17_a : std_logic;
SIGNAL fir_data_DelM_a17_a_a276 : std_logic;
SIGNAL Dly1_asram_aq_a16_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a16_a : std_logic;
SIGNAL fir_data_DelM_a16_a_a275 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL Dly1_asram_aq_a15_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a15_a : std_logic;
SIGNAL fir_data_DelM_a15_a_a278 : std_logic;
SIGNAL Dly1_asram_aq_a14_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a14_a : std_logic;
SIGNAL fir_data_DelM_a14_a_a277 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL Dly1_asram_aq_a11_a : std_logic;
SIGNAL add2b_ff_adffs_a11_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a11_a : std_logic;
SIGNAL fir_data_DelM_a11_a_a282 : std_logic;
SIGNAL Dly1_asram_aq_a10_a : std_logic;
SIGNAL add2b_ff_adffs_a10_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a10_a : std_logic;
SIGNAL fir_data_DelM_a10_a_a281 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL Dly1_asram_aq_a7_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a7_a : std_logic;
SIGNAL fir_data_DelM_a7_a_a286 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL Dly1_asram_aq_a9_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a9_a : std_logic;
SIGNAL fir_data_DelM_a9_a_a284 : std_logic;
SIGNAL Dly1_asram_aq_a8_a : std_logic;
SIGNAL fir_data_Del1_ff_adffs_a8_a : std_logic;
SIGNAL fir_data_DelM_a8_a_a283 : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a : std_logic;
SIGNAL PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL pslope_vec_a0_a : std_logic;
SIGNAL pslope_vec_a1_a : std_logic;
SIGNAL PSlope_a193 : std_logic;
SIGNAL PSlope_a191 : std_logic;
SIGNAL pslope_vec_a2_a : std_logic;
SIGNAL pslope_vec_a3_a : std_logic;
SIGNAL PSlope_a188 : std_logic;
SIGNAL pslope_vec_a4_a : std_logic;
SIGNAL PSlope_a189 : std_logic;
SIGNAL PSlope_a192 : std_logic;
SIGNAL PSlope : std_logic;
SIGNAL PSlope_Del : std_logic;
SIGNAL tlevel_a15_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a15_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL tlevel_a12_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a12_a : std_logic;
SIGNAL DSelect_a0_a_acombout : std_logic;
SIGNAL dselect_reg_ff_adffs_a0_a : std_logic;
SIGNAL tlevel_a14_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a14_a : std_logic;
SIGNAL tlevel_extm_a17_a_a1397 : std_logic;
SIGNAL tlevel_extm_a17_a_a1398 : std_logic;
SIGNAL tlevel_a13_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a13_a : std_logic;
SIGNAL tlevel_extm_a16_a_a1395 : std_logic;
SIGNAL tlevel_extm_a16_a_a1396 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL tlevel_a8_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a8_a : std_logic;
SIGNAL tlevel_extm_a10_a_a1407 : std_logic;
SIGNAL tlevel_extm_a10_a_a1408 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL add2b_ff_adffs_a5_a : std_logic;
SIGNAL tlevel_a0_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a0_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140 : std_logic;
SIGNAL tlevel_a2_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a2_a : std_logic;
SIGNAL tlevel_a1_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a1_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_a0_a_a_a00006 : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a : std_logic;
SIGNAL tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out : std_logic;
SIGNAL Disc_En_acombout : std_logic;
SIGNAL clock_proc_a52 : std_logic;
SIGNAL Level_Out_areg0 : std_logic;
SIGNAL fir_out_a0_a_a1541 : std_logic;
SIGNAL fir_out_a0_a_areg0 : std_logic;
SIGNAL add2b_ff_adffs_a4_a : std_logic;
SIGNAL fir_out_a1_a_a1543 : std_logic;
SIGNAL fir_out_a1_a_areg0 : std_logic;
SIGNAL fir_out_a2_a_a1545 : std_logic;
SIGNAL fir_out_a2_a_areg0 : std_logic;
SIGNAL fir_out_a3_a_a1547 : std_logic;
SIGNAL fir_out_a3_a_areg0 : std_logic;
SIGNAL fir_out_a4_a_a1549 : std_logic;
SIGNAL fir_out_a4_a_areg0 : std_logic;
SIGNAL fir_out_a5_a_a1551 : std_logic;
SIGNAL fir_out_a5_a_areg0 : std_logic;
SIGNAL fir_out_a6_a_a1553 : std_logic;
SIGNAL fir_out_a6_a_areg0 : std_logic;
SIGNAL fir_out_a7_a_a1555 : std_logic;
SIGNAL fir_out_a7_a_areg0 : std_logic;
SIGNAL fir_out_a8_a_a1557 : std_logic;
SIGNAL fir_out_a8_a_areg0 : std_logic;
SIGNAL fir_out_a9_a_a1559 : std_logic;
SIGNAL fir_out_a9_a_areg0 : std_logic;
SIGNAL fir_out_a10_a_a1561 : std_logic;
SIGNAL fir_out_a10_a_areg0 : std_logic;
SIGNAL fir_out_a11_a_a1563 : std_logic;
SIGNAL fir_out_a11_a_areg0 : std_logic;
SIGNAL fir_out_a12_a_a1565 : std_logic;
SIGNAL fir_out_a12_a_areg0 : std_logic;
SIGNAL fir_out_a13_a_a1567 : std_logic;
SIGNAL fir_out_a13_a_areg0 : std_logic;
SIGNAL fir_out_a14_a_a1569 : std_logic;
SIGNAL fir_out_a14_a_areg0 : std_logic;
SIGNAL fir_out_a0_a_a1580 : std_logic;
SIGNAL fir_out_a15_a_a1578 : std_logic;
SIGNAL fir_out_a15_a_areg0 : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_AD_MR <= AD_MR;
ww_PA_Reset <= PA_Reset;
ww_adata <= adata;
ww_DSelect <= DSelect;
ww_Disc_En <= Disc_En;
ww_tlevel <= tlevel;
Level_Out <= ww_Level_Out;
Level_Cmp <= ww_Level_Cmp;
fir_out <= ww_fir_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

Dly1_asram_asegment_a0_a_a26_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a26_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a24_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a24_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a25_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a25_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a22_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a22_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a23_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a23_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a20_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a20_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a21_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a21_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a18_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a18_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a19_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a19_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a16_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a16_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a17_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a17_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly1_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a28 & Dly1_asram_asegment_a0_a_a26_a_a1 & wadr_cntr_awysi_counter_asload_path_a0_a);

tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out & tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out);

PSlope_a190_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a190 = pslope_vec_a3_a & pslope_vec_a2_a & pslope_vec_a4_a # !dselect_reg_ff_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8F0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pslope_vec_a3_a,
	datab => pslope_vec_a2_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => pslope_vec_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a190);

tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out & tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out, GLOBAL(clk_acombout), 
-- !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

add1_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a3_a = DFFE(adata_Reg_ff_adffs_a35_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a537 $ add1_ff_adffs_a2_a_a130, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a3_a_a127 = CARRY(adata_Reg_ff_adffs_a35_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a537 & !add1_ff_adffs_a2_a_a130 # !adata_Reg_ff_adffs_a35_a & (!add1_ff_adffs_a2_a_a130 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a537))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a35_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a537,
	cin => add1_ff_adffs_a2_a_a130,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a3_a,
	cout => add1_ff_adffs_a3_a_a127);

add1_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a2_a = DFFE(adata_Reg_ff_adffs_a34_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a541 $ !add1_ff_adffs_a1_a_a175, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a2_a_a130 = CARRY(adata_Reg_ff_adffs_a34_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a541 # !add1_ff_adffs_a1_a_a175) # !adata_Reg_ff_adffs_a34_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a541 & !add1_ff_adffs_a1_a_a175)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a34_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a541,
	cin => add1_ff_adffs_a1_a_a175,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a2_a,
	cout => add1_ff_adffs_a2_a_a130);

add1_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a6_a = DFFE(adata_Reg_ff_adffs_a38_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a545 $ !add1_ff_adffs_a5_a_a124, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a6_a_a133 = CARRY(adata_Reg_ff_adffs_a38_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a545 # !add1_ff_adffs_a5_a_a124) # !adata_Reg_ff_adffs_a38_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a545 & !add1_ff_adffs_a5_a_a124)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a38_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a545,
	cin => add1_ff_adffs_a5_a_a124,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a6_a,
	cout => add1_ff_adffs_a6_a_a133);

add1_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a7_a = DFFE(adata_Reg_ff_adffs_a39_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a553 $ add1_ff_adffs_a6_a_a133, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a7_a_a139 = CARRY(adata_Reg_ff_adffs_a39_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a553 & !add1_ff_adffs_a6_a_a133 # !adata_Reg_ff_adffs_a39_a & (!add1_ff_adffs_a6_a_a133 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a553))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a39_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a553,
	cin => add1_ff_adffs_a6_a_a133,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a7_a,
	cout => add1_ff_adffs_a7_a_a139);

add1_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a8_a = DFFE(adata_Reg_ff_adffs_a40_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a557 $ !add1_ff_adffs_a7_a_a139, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a8_a_a142 = CARRY(adata_Reg_ff_adffs_a40_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a557 # !add1_ff_adffs_a7_a_a139) # !adata_Reg_ff_adffs_a40_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a557 & !add1_ff_adffs_a7_a_a139)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a40_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a557,
	cin => add1_ff_adffs_a7_a_a139,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a8_a,
	cout => add1_ff_adffs_a8_a_a142);

add1_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a9_a = DFFE(adata_Reg_ff_adffs_a41_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a561 $ add1_ff_adffs_a8_a_a142, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a9_a_a145 = CARRY(adata_Reg_ff_adffs_a41_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a561 & !add1_ff_adffs_a8_a_a142 # !adata_Reg_ff_adffs_a41_a & (!add1_ff_adffs_a8_a_a142 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a561))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a41_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a561,
	cin => add1_ff_adffs_a8_a_a142,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a9_a,
	cout => add1_ff_adffs_a9_a_a145);

add1_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a12_a = DFFE(adata_Reg_ff_adffs_a44_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a573 $ !add1_ff_adffs_a11_a_a151, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a12_a_a154 = CARRY(adata_Reg_ff_adffs_a44_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a573 # !add1_ff_adffs_a11_a_a151) # !adata_Reg_ff_adffs_a44_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a573 & !add1_ff_adffs_a11_a_a151)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a44_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a573,
	cin => add1_ff_adffs_a11_a_a151,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a12_a,
	cout => add1_ff_adffs_a12_a_a154);

add1_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a13_a = DFFE(adata_Reg_ff_adffs_a45_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a577 $ add1_ff_adffs_a12_a_a154, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a13_a_a157 = CARRY(adata_Reg_ff_adffs_a45_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a577 & !add1_ff_adffs_a12_a_a154 # !adata_Reg_ff_adffs_a45_a & (!add1_ff_adffs_a12_a_a154 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a577))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a45_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a577,
	cin => add1_ff_adffs_a12_a_a154,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a13_a,
	cout => add1_ff_adffs_a13_a_a157);

add1_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a14_a = DFFE(adata_Reg_ff_adffs_a46_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a581 $ !add1_ff_adffs_a13_a_a157, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a14_a_a160 = CARRY(adata_Reg_ff_adffs_a46_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a581 # !add1_ff_adffs_a13_a_a157) # !adata_Reg_ff_adffs_a46_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a581 & !add1_ff_adffs_a13_a_a157)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a46_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a581,
	cin => add1_ff_adffs_a13_a_a157,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a14_a,
	cout => add1_ff_adffs_a14_a_a160);

add1_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a15_a = DFFE(adata_Reg_ff_adffs_a47_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a585 $ add1_ff_adffs_a14_a_a160, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a15_a_a163 = CARRY(adata_Reg_ff_adffs_a47_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a585 & !add1_ff_adffs_a14_a_a160 # !adata_Reg_ff_adffs_a47_a & (!add1_ff_adffs_a14_a_a160 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a585))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a47_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a585,
	cin => add1_ff_adffs_a14_a_a160,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a15_a,
	cout => add1_ff_adffs_a15_a_a163);

add1_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a16_a = DFFE(adata_Reg_ff_adffs_a47_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a589 $ !add1_ff_adffs_a15_a_a163, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a16_a_a166 = CARRY(adata_Reg_ff_adffs_a47_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a589 # !add1_ff_adffs_a15_a_a163) # !adata_Reg_ff_adffs_a47_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a589 & !add1_ff_adffs_a15_a_a163)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a47_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a589,
	cin => add1_ff_adffs_a15_a_a163,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a16_a,
	cout => add1_ff_adffs_a16_a_a166);

tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = add2b_ff_adffs_a24_a & (tlevel_reg_ff_adffs_a15_a & add2b_ff_adffs_a25_a) # !add2b_ff_adffs_a24_a & (!tlevel_reg_ff_adffs_a15_a & !add2b_ff_adffs_a25_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A005",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a24_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => add2b_ff_adffs_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out & tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out, GLOBAL(clk_acombout), 
-- !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

adata_Reg_ff_adffs_a37_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a37_a = DFFE(adata_a37_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a37_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a37_a);

adata_Reg_ff_adffs_a35_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a35_a = DFFE(adata_a35_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a35_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a35_a);

adata_Reg_ff_adffs_a34_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a34_a = DFFE(adata_a34_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a34_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a34_a);

add1_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a1_a = DFFE(adata_Reg_ff_adffs_a33_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a597 $ add1_ff_adffs_a0_a_a178, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a1_a_a175 = CARRY(adata_Reg_ff_adffs_a33_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a597 & !add1_ff_adffs_a0_a_a178 # !adata_Reg_ff_adffs_a33_a & (!add1_ff_adffs_a0_a_a178 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a597))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a33_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a597,
	cin => add1_ff_adffs_a0_a_a178,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a1_a,
	cout => add1_ff_adffs_a1_a_a175);

add2b_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a0_a = DFFE(!GLOBAL(AD_MR_acombout) & tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390 $ add1_ff_adffs_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a0_a_a336 = CARRY(tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390 & add1_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390,
	datab => add1_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a0_a,
	cout => add2b_ff_adffs_a0_a_a336);

adata_Reg_ff_adffs_a38_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a38_a = DFFE(adata_a38_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a38_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a38_a);

adata_Reg_ff_adffs_a36_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a36_a = DFFE(adata_a36_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a36_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a36_a);

adata_Reg_ff_adffs_a39_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a39_a = DFFE(adata_a39_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a39_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a39_a);

adata_Reg_ff_adffs_a40_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a40_a = DFFE(adata_a40_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a40_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a40_a);

adata_Reg_ff_adffs_a41_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a41_a = DFFE(adata_a41_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a41_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a41_a);

adata_Reg_ff_adffs_a42_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a42_a = DFFE(adata_a42_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a42_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a42_a);

adata_Reg_ff_adffs_a43_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a43_a = DFFE(adata_a43_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a43_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a43_a);

adata_Reg_ff_adffs_a44_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a44_a = DFFE(adata_a44_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a44_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a44_a);

adata_Reg_ff_adffs_a45_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a45_a = DFFE(adata_a45_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a45_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a45_a);

adata_Reg_ff_adffs_a46_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a46_a = DFFE(adata_a46_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a46_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a46_a);

adata_Reg_ff_adffs_a47_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a47_a = DFFE(adata_a47_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a47_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a47_a);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out & PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out, 
-- GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out = tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out # tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out & tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out,
	datac => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out,
	datad => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out & tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out,
	datac => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out & tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out, GLOBAL(clk_acombout), 
-- !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

adata_Reg_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a20_a = DFFE(adata_a20_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a20_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a20_a);

adata_a37_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(37),
	combout => adata_a37_a_acombout);

adata_Reg_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a18_a = DFFE(adata_a18_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a18_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a18_a);

adata_a35_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(35),
	combout => adata_a35_a_acombout);

adata_Reg_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a17_a = DFFE(adata_a17_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a17_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a17_a);

adata_a34_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(34),
	combout => adata_a34_a_acombout);

adata_Reg_ff_adffs_a33_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a33_a = DFFE(adata_a33_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a33_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a33_a);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390 = add2b_ff_adffs_a0_a
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a = CARRY(add2b_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAAA",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390,
	cout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a);

adata_Reg_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a21_a = DFFE(adata_a21_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a21_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a21_a);

adata_a38_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(38),
	combout => adata_a38_a_acombout);

adata_Reg_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a19_a = DFFE(adata_a19_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a19_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a19_a);

adata_a36_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(36),
	combout => adata_a36_a_acombout);

adata_Reg_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a22_a = DFFE(adata_a22_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a22_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a22_a);

adata_a39_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(39),
	combout => adata_a39_a_acombout);

adata_Reg_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a23_a = DFFE(adata_a23_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a23_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a23_a);

adata_a40_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(40),
	combout => adata_a40_a_acombout);

adata_Reg_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a24_a = DFFE(adata_a24_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a24_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a24_a);

adata_a41_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(41),
	combout => adata_a41_a_acombout);

adata_Reg_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a25_a = DFFE(adata_a25_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a25_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a25_a);

adata_a42_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(42),
	combout => adata_a42_a_acombout);

adata_Reg_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a26_a = DFFE(adata_a26_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a26_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a26_a);

adata_a43_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(43),
	combout => adata_a43_a_acombout);

adata_Reg_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a27_a = DFFE(adata_a27_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a27_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a27_a);

adata_a44_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(44),
	combout => adata_a44_a_acombout);

adata_Reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a13_a = DFFE(adata_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a13_a);

adata_a45_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(45),
	combout => adata_a45_a_acombout);

adata_Reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a14_a = DFFE(adata_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a14_a);

adata_a46_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(46),
	combout => adata_a46_a_acombout);

adata_Reg_ff_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a30_a = DFFE(adata_a30_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a30_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a30_a);

adata_a47_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(47),
	combout => adata_a47_a_acombout);

adata_Reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a31_a = DFFE(adata_a31_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a31_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a31_a);

fir_data_Del1_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a26_a = DFFE(add2b_ff_adffs_a26_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a26_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a26_a);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out & PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out, 
-- GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out_a31 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out = !tlevel_reg_ff_adffs_a15_a & (add2b_ff_adffs_a21_a # add2b_ff_adffs_a20_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3232",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out = add2b_ff_adffs_a21_a & tlevel_reg_ff_adffs_a15_a & add2b_ff_adffs_a20_a # !add2b_ff_adffs_a21_a & !tlevel_reg_ff_adffs_a15_a & !add2b_ff_adffs_a20_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8181",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out);

tlevel_extm_a18_a_a1393_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a18_a_a1393 = dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a13_a) # !dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA50",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => tlevel_reg_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a18_a_a1393);

tlevel_extm_a19_a_a1394_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a19_a_a1394 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a14_a # !dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D8D8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a14_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a19_a_a1394);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out = tlevel_extm_a19_a_a1394 & add2b_ff_adffs_a19_a & add2b_ff_adffs_a18_a & !tlevel_extm_a18_a_a1393 # !tlevel_extm_a19_a_a1394 & (add2b_ff_adffs_a19_a # add2b_ff_adffs_a18_a & 
-- !tlevel_extm_a18_a_a1393)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "44D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a19_a_a1394,
	datab => add2b_ff_adffs_a19_a,
	datac => add2b_ff_adffs_a18_a,
	datad => tlevel_extm_a18_a_a1393,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out = tlevel_extm_a19_a_a1394 & add2b_ff_adffs_a19_a & (add2b_ff_adffs_a18_a $ !tlevel_extm_a18_a_a1393) # !tlevel_extm_a19_a_a1394 & !add2b_ff_adffs_a19_a & (add2b_ff_adffs_a18_a $ 
-- !tlevel_extm_a18_a_a1393)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a19_a_a1394,
	datab => add2b_ff_adffs_a19_a,
	datac => add2b_ff_adffs_a18_a,
	datad => tlevel_extm_a18_a_a1393,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out);

tlevel_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a11_a = DFFE(tlevel_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a11_a);

tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out = tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out # tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out & tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out,
	datac => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out,
	datad => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out & tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

adata_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(20),
	combout => adata_a20_a_acombout);

adata_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(18),
	combout => adata_a18_a_acombout);

adata_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(17),
	combout => adata_a17_a_acombout);

adata_Reg_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a16_a = DFFE(adata_a16_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a16_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a16_a);

adata_a33_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(33),
	combout => adata_a33_a_acombout);

adata_Reg_ff_adffs_a32_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a32_a = DFFE(adata_a32_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a32_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a32_a);

adata_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(21),
	combout => adata_a21_a_acombout);

adata_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(19),
	combout => adata_a19_a_acombout);

adata_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(22),
	combout => adata_a22_a_acombout);

adata_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(23),
	combout => adata_a23_a_acombout);

adata_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(24),
	combout => adata_a24_a_acombout);

adata_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(25),
	combout => adata_a25_a_acombout);

adata_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(26),
	combout => adata_a26_a_acombout);

adata_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(27),
	combout => adata_a27_a_acombout);

adata_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(13),
	combout => adata_a13_a_acombout);

adata_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(14),
	combout => adata_a14_a_acombout);

adata_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(30),
	combout => adata_a30_a_acombout);

adata_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(31),
	combout => adata_a31_a_acombout);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out & PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out & PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out, 
-- GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out);

tlevel_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(11),
	combout => tlevel_a11_a_acombout);

tlevel_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a9_a = DFFE(tlevel_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a9_a);

tlevel_extm_a14_a_a1399_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a14_a_a1399 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a11_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a11_a,
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a14_a_a1399);

tlevel_extm_a14_a_a1400_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a14_a_a1400 = dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a9_a) # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a14_a_a1399)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a9_a,
	datad => tlevel_extm_a14_a_a1399,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a14_a_a1400);

tlevel_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a10_a = DFFE(tlevel_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a10_a);

tlevel_extm_a15_a_a1401_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a15_a_a1401 = dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a12_a) # !dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a13_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a13_a,
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a15_a_a1401);

tlevel_extm_a15_a_a1402_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a15_a_a1402 = dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a10_a) # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a15_a_a1401)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a10_a,
	datad => tlevel_extm_a15_a_a1401,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a15_a_a1402);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out = tlevel_extm_a15_a_a1402 & add2b_ff_adffs_a14_a & add2b_ff_adffs_a15_a & !tlevel_extm_a14_a_a1400 # !tlevel_extm_a15_a_a1402 & (add2b_ff_adffs_a15_a # add2b_ff_adffs_a14_a & 
-- !tlevel_extm_a14_a_a1400)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "50D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a15_a_a1402,
	datab => add2b_ff_adffs_a14_a,
	datac => add2b_ff_adffs_a15_a,
	datad => tlevel_extm_a14_a_a1400,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out = tlevel_extm_a15_a_a1402 & add2b_ff_adffs_a15_a & (add2b_ff_adffs_a14_a $ !tlevel_extm_a14_a_a1400) # !tlevel_extm_a15_a_a1402 & !add2b_ff_adffs_a15_a & (add2b_ff_adffs_a14_a $ 
-- !tlevel_extm_a14_a_a1400)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a15_a_a1402,
	datab => add2b_ff_adffs_a14_a,
	datac => add2b_ff_adffs_a15_a,
	datad => tlevel_extm_a14_a_a1400,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out);

tlevel_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a7_a = DFFE(tlevel_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a7_a);

tlevel_extm_a12_a_a1403_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a12_a_a1403 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a9_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a9_a,
	datac => tlevel_reg_ff_adffs_a10_a,
	datad => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a12_a_a1403);

tlevel_extm_a12_a_a1404_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a12_a_a1404 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a7_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a12_a_a1403)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a7_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a12_a_a1403,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a12_a_a1404);

tlevel_extm_a13_a_a1405_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a13_a_a1405 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a10_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B8B8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a10_a,
	datab => dselect_reg_ff_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a13_a_a1405);

tlevel_extm_a13_a_a1406_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a13_a_a1406 = dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a8_a) # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a13_a_a1405)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a8_a,
	datad => tlevel_extm_a13_a_a1405,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a13_a_a1406);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out = tlevel_extm_a13_a_a1406 & add2b_ff_adffs_a12_a & add2b_ff_adffs_a13_a & !tlevel_extm_a12_a_a1404 # !tlevel_extm_a13_a_a1406 & (add2b_ff_adffs_a13_a # add2b_ff_adffs_a12_a & 
-- !tlevel_extm_a12_a_a1404)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a12_a,
	datab => tlevel_extm_a13_a_a1406,
	datac => add2b_ff_adffs_a13_a,
	datad => tlevel_extm_a12_a_a1404,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out = add2b_ff_adffs_a12_a & tlevel_extm_a12_a_a1404 & (tlevel_extm_a13_a_a1406 $ !add2b_ff_adffs_a13_a) # !add2b_ff_adffs_a12_a & !tlevel_extm_a12_a_a1404 & (tlevel_extm_a13_a_a1406 $ 
-- !add2b_ff_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a12_a,
	datab => tlevel_extm_a13_a_a1406,
	datac => add2b_ff_adffs_a13_a,
	datad => tlevel_extm_a12_a_a1404,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out);

tlevel_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a5_a = DFFE(tlevel_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a5_a);

tlevel_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a6_a = DFFE(tlevel_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a6_a);

tlevel_extm_a11_a_a1409_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a11_a_a1409 = dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a8_a) # !dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a9_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a11_a_a1409);

tlevel_extm_a11_a_a1410_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a11_a_a1410 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a6_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a11_a_a1409)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a6_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a11_a_a1409,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a11_a_a1410);

tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out = tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out # tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out & tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F8F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out,
	datab => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out,
	datac => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out & (tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

adata_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(16),
	combout => adata_a16_a_acombout);

adata_a32_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(32),
	combout => adata_a32_a_acombout);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out = add2b_ff_adffs_a21_a & fir_data_DelM_a21_a_a272 & (add2b_ff_adffs_a20_a $ !fir_data_DelM_a20_a_a271) # !add2b_ff_adffs_a21_a & !fir_data_DelM_a21_a_a272 & (add2b_ff_adffs_a20_a $ 
-- !fir_data_DelM_a20_a_a271)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => add2b_ff_adffs_a20_a,
	datac => fir_data_DelM_a21_a_a272,
	datad => fir_data_DelM_a20_a_a271,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out);

fir_data_Del1_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a18_a = DFFE(add2b_ff_adffs_a18_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a18_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a18_a);

Dly1_asram_asegment_a0_a_a19_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 19,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a19_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a19_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a19_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a19_a_modesel,
	dataout => Dly1_asram_aq_a19_a);

fir_data_Del1_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a19_a = DFFE(add2b_ff_adffs_a19_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a19_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a19_a);

fir_data_DelM_a19_a_a274_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a19_a_a274 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a19_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a19_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a19_a,
	datad => fir_data_Del1_ff_adffs_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a19_a_a274);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out = fir_data_DelM_a19_a_a274 & add2b_ff_adffs_a19_a & (add2b_ff_adffs_a18_a $ !fir_data_DelM_a18_a_a273) # !fir_data_DelM_a19_a_a274 & !add2b_ff_adffs_a19_a & (add2b_ff_adffs_a18_a $ 
-- !fir_data_DelM_a18_a_a273)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a19_a_a274,
	datab => add2b_ff_adffs_a19_a,
	datac => add2b_ff_adffs_a18_a,
	datad => fir_data_DelM_a18_a_a273,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out = add2b_ff_adffs_a17_a & (add2b_ff_adffs_a16_a & !fir_data_DelM_a16_a_a275 # !fir_data_DelM_a17_a_a276) # !add2b_ff_adffs_a17_a & add2b_ff_adffs_a16_a & !fir_data_DelM_a17_a_a276 & 
-- !fir_data_DelM_a16_a_a275

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A8E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a16_a,
	datac => fir_data_DelM_a17_a_a276,
	datad => fir_data_DelM_a16_a_a275,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out & (PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

tlevel_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(9),
	combout => tlevel_a9_a_acombout);

tlevel_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(10),
	combout => tlevel_a10_a_acombout);

tlevel_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(7),
	combout => tlevel_a7_a_acombout);

tlevel_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(5),
	combout => tlevel_a5_a_acombout);

tlevel_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(6),
	combout => tlevel_a6_a_acombout);

tlevel_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a3_a = DFFE(tlevel_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a3_a);

tlevel_extm_a8_a_a1411_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a8_a_a1411 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a5_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a5_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a8_a_a1411);

tlevel_extm_a8_a_a1412_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a8_a_a1412 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a3_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a8_a_a1411)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a3_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a8_a_a1411,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a8_a_a1412);

tlevel_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a4_a = DFFE(tlevel_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a4_a);

tlevel_extm_a9_a_a1413_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a9_a_a1413 = dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a6_a) # !dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a7_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a9_a_a1413);

tlevel_extm_a9_a_a1414_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a9_a_a1414 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a4_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a9_a_a1413)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a4_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a9_a_a1413,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a9_a_a1414);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out = add2b_ff_adffs_a9_a & (add2b_ff_adffs_a8_a & !tlevel_extm_a8_a_a1412 # !tlevel_extm_a9_a_a1414) # !add2b_ff_adffs_a9_a & !tlevel_extm_a9_a_a1414 & add2b_ff_adffs_a8_a & !tlevel_extm_a8_a_a1412

-- pragma translate_off
GENERIC MAP (
	lut_mask => "22B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => tlevel_extm_a9_a_a1414,
	datac => add2b_ff_adffs_a8_a,
	datad => tlevel_extm_a8_a_a1412,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out = add2b_ff_adffs_a9_a & tlevel_extm_a9_a_a1414 & (add2b_ff_adffs_a8_a $ !tlevel_extm_a8_a_a1412) # !add2b_ff_adffs_a9_a & !tlevel_extm_a9_a_a1414 & (add2b_ff_adffs_a8_a $ !tlevel_extm_a8_a_a1412)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => tlevel_extm_a9_a_a1414,
	datac => add2b_ff_adffs_a8_a,
	datad => tlevel_extm_a8_a_a1412,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out);

tlevel_extm_a6_a_a1415_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a6_a_a1415 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a3_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a3_a,
	datab => tlevel_reg_ff_adffs_a4_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a6_a_a1415);

tlevel_extm_a6_a_a1416_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a6_a_a1416 = dselect_reg_ff_adffs_a1_a & (tlevel_reg_ff_adffs_a1_a) # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a6_a_a1415)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a6_a_a1415,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a6_a_a1416);

tlevel_extm_a7_a_a1417_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a7_a_a1417 = dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a4_a) # !dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a5_a,
	datab => tlevel_reg_ff_adffs_a4_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a7_a_a1417);

tlevel_extm_a7_a_a1418_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a7_a_a1418 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a2_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a7_a_a1417)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a7_a_a1417,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a7_a_a1418);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out = tlevel_extm_a7_a_a1418 & add2b_ff_adffs_a6_a & add2b_ff_adffs_a7_a & !tlevel_extm_a6_a_a1416 # !tlevel_extm_a7_a_a1418 & (add2b_ff_adffs_a7_a # add2b_ff_adffs_a6_a & !tlevel_extm_a6_a_a1416)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => tlevel_extm_a7_a_a1418,
	datac => add2b_ff_adffs_a7_a,
	datad => tlevel_extm_a6_a_a1416,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out = add2b_ff_adffs_a6_a & tlevel_extm_a6_a_a1416 & (tlevel_extm_a7_a_a1418 $ !add2b_ff_adffs_a7_a) # !add2b_ff_adffs_a6_a & !tlevel_extm_a6_a_a1416 & (tlevel_extm_a7_a_a1418 $ !add2b_ff_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => tlevel_extm_a7_a_a1418,
	datac => add2b_ff_adffs_a7_a,
	datad => tlevel_extm_a6_a_a1416,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139 = !dselect_reg_ff_adffs_a1_a & (dselect_reg_ff_adffs_a0_a & !tlevel_reg_ff_adffs_a2_a # !dselect_reg_ff_adffs_a0_a & (!tlevel_reg_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0027",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => tlevel_reg_ff_adffs_a3_a,
	datad => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139);

Dly1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a12_a_modesel,
	dataout => Dly1_asram_aq_a12_a);

fir_data_Del1_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a12_a = DFFE(add2b_ff_adffs_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a12_a);

fir_data_DelM_a12_a_a279_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a12_a_a279 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a12_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a12_a,
	datad => fir_data_Del1_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a12_a_a279);

Dly1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a13_a_modesel,
	dataout => Dly1_asram_aq_a13_a);

fir_data_Del1_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a13_a = DFFE(add2b_ff_adffs_a13_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a13_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a13_a);

fir_data_DelM_a13_a_a280_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a13_a_a280 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a13_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a13_a,
	datad => fir_data_Del1_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a13_a_a280);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out = fir_data_DelM_a13_a_a280 & add2b_ff_adffs_a12_a & add2b_ff_adffs_a13_a & !fir_data_DelM_a12_a_a279 # !fir_data_DelM_a13_a_a280 & (add2b_ff_adffs_a13_a # add2b_ff_adffs_a12_a & 
-- !fir_data_DelM_a12_a_a279)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "50D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a13_a_a280,
	datab => add2b_ff_adffs_a12_a,
	datac => add2b_ff_adffs_a13_a,
	datad => fir_data_DelM_a12_a_a279,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out = fir_data_DelM_a13_a_a280 & add2b_ff_adffs_a13_a & (add2b_ff_adffs_a12_a $ !fir_data_DelM_a12_a_a279) # !fir_data_DelM_a13_a_a280 & !add2b_ff_adffs_a13_a & (add2b_ff_adffs_a12_a $ 
-- !fir_data_DelM_a12_a_a279)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a13_a_a280,
	datab => add2b_ff_adffs_a12_a,
	datac => add2b_ff_adffs_a13_a,
	datad => fir_data_DelM_a12_a_a279,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out = PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out & (PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aaeb_out);

tlevel_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(3),
	combout => tlevel_a3_a_acombout);

tlevel_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(4),
	combout => tlevel_a4_a_acombout);

Dly1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a6_a_modesel,
	dataout => Dly1_asram_aq_a6_a);

fir_data_Del1_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a6_a = DFFE(add2b_ff_adffs_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a6_a);

fir_data_DelM_a6_a_a285_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a6_a_a285 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a6_a) # !dselect_reg_ff_adffs_a1_a & fir_data_Del1_ff_adffs_a6_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => fir_data_Del1_ff_adffs_a6_a,
	datad => Dly1_asram_aq_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a6_a_a285);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out = fir_data_DelM_a6_a_a285 & add2b_ff_adffs_a6_a & (add2b_ff_adffs_a7_a $ !fir_data_DelM_a7_a_a286) # !fir_data_DelM_a6_a_a285 & !add2b_ff_adffs_a6_a & (add2b_ff_adffs_a7_a $ 
-- !fir_data_DelM_a7_a_a286)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a6_a_a285,
	datab => add2b_ff_adffs_a7_a,
	datac => add2b_ff_adffs_a6_a,
	datad => fir_data_DelM_a7_a_a286,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a597_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AA",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608,
	datab => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a597,
	cout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a);

fir_data_Del1_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a5_a = DFFE(add2b_ff_adffs_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a5_a);

Dly1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a5_a_modesel,
	dataout => Dly1_asram_aq_a5_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73 = dselect_reg_ff_adffs_a1_a & (!Dly1_asram_aq_a5_a) # !dselect_reg_ff_adffs_a1_a & !fir_data_Del1_ff_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3535",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_Del1_ff_adffs_a5_a,
	datab => Dly1_asram_aq_a5_a,
	datac => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73);

PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a74 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_a_a00006 = add2b_ff_adffs_a5_a & (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a # PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73) # !add2b_ff_adffs_a5_a & 
-- (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a & PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FAA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datad => PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_node_a73,
	cin => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_a_a00006);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a598_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609,
	datab => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a598,
	cout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608 = add2b_ff_adffs_a4_a & (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625 # !fir_data_DelM_a4_a_a292 & !fir_data_DelM_a4_a_a291) # !add2b_ff_adffs_a4_a & !fir_data_DelM_a4_a_a292 & 
-- !fir_data_DelM_a4_a_a291 & PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F110",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a4_a_a292,
	datab => fir_data_DelM_a4_a_a291,
	datac => add2b_ff_adffs_a4_a,
	datad => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a4_a_a608);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a599_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610,
	datab => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a599,
	cout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609 = add2b_ff_adffs_a3_a & (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631 # !fir_data_DelM_a3_a_a296 & !fir_data_DelM_a3_a_a295) # !add2b_ff_adffs_a3_a & !fir_data_DelM_a3_a_a296 & 
-- !fir_data_DelM_a3_a_a295 & PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F110",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a3_a_a296,
	datab => fir_data_DelM_a3_a_a295,
	datac => add2b_ff_adffs_a3_a,
	datad => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a609);

Dly1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a4_a_modesel,
	dataout => Dly1_asram_aq_a4_a);

fir_data_DelM_a4_a_a291_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a4_a_a291 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dselect_reg_ff_adffs_a1_a,
	datad => Dly1_asram_aq_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a4_a_a291);

fir_data_Del1_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a4_a = DFFE(add2b_ff_adffs_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a4_a);

fir_data_DelM_a4_a_a292_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a4_a_a292 = !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a4_a_a292);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406 = dselect_reg_ff_adffs_a0_a # dselect_reg_ff_adffs_a1_a # !tlevel_reg_ff_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FDFD",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a0_a,
	datab => dselect_reg_ff_adffs_a0_a,
	datac => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a600_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611,
	datab => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a600,
	cout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610 = add2b_ff_adffs_a2_a & (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637 # !fir_data_DelM_a2_a_a299 & !fir_data_DelM_a2_a_a300) # !add2b_ff_adffs_a2_a & !fir_data_DelM_a2_a_a299 & 
-- !fir_data_DelM_a2_a_a300 & PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AB02",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a2_a,
	datab => fir_data_DelM_a2_a_a299,
	datac => fir_data_DelM_a2_a_a300,
	datad => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a610);

Dly1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a3_a_modesel,
	dataout => Dly1_asram_aq_a3_a);

fir_data_DelM_a3_a_a295_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a3_a_a295 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dselect_reg_ff_adffs_a1_a,
	datad => Dly1_asram_aq_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a3_a_a295);

fir_data_Del1_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a3_a = DFFE(add2b_ff_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a3_a);

fir_data_DelM_a3_a_a296_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a3_a_a296 = fir_data_Del1_ff_adffs_a3_a & (!dselect_reg_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_Del1_ff_adffs_a3_a,
	datac => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a3_a_a296);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a601_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614 & tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0088",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614,
	datab => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a601,
	cout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611 = add2b_ff_adffs_a1_a & (PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641 # !fir_data_DelM_a1_a_a301 & !fir_data_DelM_a1_a_a302) # !add2b_ff_adffs_a1_a & !fir_data_DelM_a1_a_a301 & 
-- !fir_data_DelM_a1_a_a302 & PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AB02",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a1_a,
	datab => fir_data_DelM_a1_a_a301,
	datac => fir_data_DelM_a1_a_a302,
	datad => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a611);

Dly1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a2_a_modesel,
	dataout => Dly1_asram_aq_a2_a);

fir_data_DelM_a2_a_a299_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a2_a_a299 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datad => Dly1_asram_aq_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a2_a_a299);

fir_data_Del1_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a2_a = DFFE(add2b_ff_adffs_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a2_a);

fir_data_DelM_a2_a_a300_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a2_a_a300 = fir_data_Del1_ff_adffs_a2_a & (!dselect_reg_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_data_Del1_ff_adffs_a2_a,
	datad => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a2_a_a300);

Dly1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a1_a_modesel,
	dataout => Dly1_asram_aq_a1_a);

fir_data_DelM_a1_a_a301_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a1_a_a301 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dselect_reg_ff_adffs_a1_a,
	datad => Dly1_asram_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a1_a_a301);

fir_data_Del1_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a1_a = DFFE(add2b_ff_adffs_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a1_a);

fir_data_DelM_a1_a_a302_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a1_a_a302 = fir_data_Del1_ff_adffs_a1_a & (!dselect_reg_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_data_Del1_ff_adffs_a1_a,
	datad => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a1_a_a302);

fir_data_Del1_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a0_a = DFFE(tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a0_a);

Dly1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a_a390,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a0_a_modesel,
	dataout => Dly1_asram_aq_a0_a);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614 = dselect_reg_ff_adffs_a1_a & (!Dly1_asram_aq_a0_a) # !dselect_reg_ff_adffs_a1_a & (!fir_data_Del1_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "05AF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => fir_data_Del1_ff_adffs_a0_a,
	datad => Dly1_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a614);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a624 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625 = PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a3_a_a625);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a630 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631 = PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a2_a_a631);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a636 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637 = PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a1_a_a637);

PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a640 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641 = PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a0_a_alcarry_a0_a_a641);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

DSelect_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSelect(1),
	combout => DSelect_a1_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

dselect_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- dselect_reg_ff_adffs_a1_a = DFFE(DSelect_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSelect_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dselect_reg_ff_adffs_a1_a);

wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(wadr_cntr_awysi_counter_asload_path_a1_a $ wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(wadr_cntr_awysi_counter_asload_path_a2_a $ (!wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A5A5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a2_a);

Dly1_asram_asegment_a0_a_a26_a_a1_I : apex20ke_lcell
-- Equation(s):
-- Dly1_asram_asegment_a0_a_a26_a_a1 = !wadr_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dly1_asram_asegment_a0_a_a26_a_a1);

add_a28_I : apex20ke_lcell
-- Equation(s):
-- add_a28 = wadr_cntr_awysi_counter_asload_path_a2_a $ !wadr_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a28);

Dly1_asram_asegment_a0_a_a26_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 26,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a26_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a26_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a26_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a26_a_modesel,
	dataout => Dly1_asram_aq_a26_a);

adata_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(15),
	combout => adata_a15_a_acombout);

adata_Reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a15_a = DFFE(adata_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a15_a);

adata_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(29),
	combout => adata_a29_a_acombout);

adata_Reg_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a29_a = DFFE(adata_a29_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a29_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a29_a);

adata_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(28),
	combout => adata_a28_a_acombout);

adata_Reg_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a28_a = DFFE(adata_a28_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a28_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a28_a);

adata_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(12),
	combout => adata_a12_a_acombout);

adata_Reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a12_a = DFFE(adata_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a12_a);

adata_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(11),
	combout => adata_a11_a_acombout);

adata_Reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a11_a = DFFE(adata_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a11_a);

adata_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(10),
	combout => adata_a10_a_acombout);

adata_Reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a10_a = DFFE(adata_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a10_a);

adata_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(9),
	combout => adata_a9_a_acombout);

adata_Reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a9_a = DFFE(adata_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a9_a);

adata_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(8),
	combout => adata_a8_a_acombout);

adata_Reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a8_a = DFFE(adata_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a8_a);

adata_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(7),
	combout => adata_a7_a_acombout);

adata_Reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a7_a = DFFE(adata_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a7_a);

adata_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(6),
	combout => adata_a6_a_acombout);

adata_Reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a6_a = DFFE(adata_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a6_a);

adata_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(5),
	combout => adata_a5_a_acombout);

adata_Reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a5_a = DFFE(adata_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a5_a);

adata_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(4),
	combout => adata_a4_a_acombout);

adata_Reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a4_a = DFFE(adata_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a4_a);

adata_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(3),
	combout => adata_a3_a_acombout);

adata_Reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a3_a = DFFE(adata_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a3_a);

adata_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(2),
	combout => adata_a2_a_acombout);

adata_Reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a2_a = DFFE(adata_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a2_a);

adata_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(1),
	combout => adata_a1_a_acombout);

adata_Reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a1_a = DFFE(adata_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a1_a);

sub1_aadder_aresult_node_acs_buffer_a1_a_a597_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a597 = adata_Reg_ff_adffs_a16_a $ adata_Reg_ff_adffs_a1_a
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a599 = CARRY(adata_Reg_ff_adffs_a1_a # !adata_Reg_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66DD",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a16_a,
	datab => adata_Reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a597,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a599);

sub1_aadder_aresult_node_acs_buffer_a1_a_a541_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a541 = adata_Reg_ff_adffs_a17_a $ adata_Reg_ff_adffs_a2_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a599
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a543 = CARRY(adata_Reg_ff_adffs_a17_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a599 # !adata_Reg_ff_adffs_a2_a) # !adata_Reg_ff_adffs_a17_a & !adata_Reg_ff_adffs_a2_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a599)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a17_a,
	datab => adata_Reg_ff_adffs_a2_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a599,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a541,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a543);

sub1_aadder_aresult_node_acs_buffer_a1_a_a537_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a537 = adata_Reg_ff_adffs_a18_a $ adata_Reg_ff_adffs_a3_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a543
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a539 = CARRY(adata_Reg_ff_adffs_a18_a & adata_Reg_ff_adffs_a3_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a543 # !adata_Reg_ff_adffs_a18_a & (adata_Reg_ff_adffs_a3_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a543))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a18_a,
	datab => adata_Reg_ff_adffs_a3_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a543,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a537,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a539);

sub1_aadder_aresult_node_acs_buffer_a1_a_a549_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a549 = adata_Reg_ff_adffs_a19_a $ adata_Reg_ff_adffs_a4_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a539
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a551 = CARRY(adata_Reg_ff_adffs_a19_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a539 # !adata_Reg_ff_adffs_a4_a) # !adata_Reg_ff_adffs_a19_a & !adata_Reg_ff_adffs_a4_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a539)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a19_a,
	datab => adata_Reg_ff_adffs_a4_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a539,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a549,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a551);

sub1_aadder_aresult_node_acs_buffer_a1_a_a533_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a533 = adata_Reg_ff_adffs_a20_a $ adata_Reg_ff_adffs_a5_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a551
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a535 = CARRY(adata_Reg_ff_adffs_a20_a & adata_Reg_ff_adffs_a5_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a551 # !adata_Reg_ff_adffs_a20_a & (adata_Reg_ff_adffs_a5_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a551))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a20_a,
	datab => adata_Reg_ff_adffs_a5_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a551,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a533,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a535);

sub1_aadder_aresult_node_acs_buffer_a1_a_a545_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a545 = adata_Reg_ff_adffs_a21_a $ adata_Reg_ff_adffs_a6_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a535
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a547 = CARRY(adata_Reg_ff_adffs_a21_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a535 # !adata_Reg_ff_adffs_a6_a) # !adata_Reg_ff_adffs_a21_a & !adata_Reg_ff_adffs_a6_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a535)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a21_a,
	datab => adata_Reg_ff_adffs_a6_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a535,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a545,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a547);

sub1_aadder_aresult_node_acs_buffer_a1_a_a553_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a553 = adata_Reg_ff_adffs_a22_a $ adata_Reg_ff_adffs_a7_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a547
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a555 = CARRY(adata_Reg_ff_adffs_a22_a & adata_Reg_ff_adffs_a7_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a547 # !adata_Reg_ff_adffs_a22_a & (adata_Reg_ff_adffs_a7_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a547))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a22_a,
	datab => adata_Reg_ff_adffs_a7_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a547,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a553,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a555);

sub1_aadder_aresult_node_acs_buffer_a1_a_a557_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a557 = adata_Reg_ff_adffs_a23_a $ adata_Reg_ff_adffs_a8_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a555
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a559 = CARRY(adata_Reg_ff_adffs_a23_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a555 # !adata_Reg_ff_adffs_a8_a) # !adata_Reg_ff_adffs_a23_a & !adata_Reg_ff_adffs_a8_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a555)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a23_a,
	datab => adata_Reg_ff_adffs_a8_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a555,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a557,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a559);

sub1_aadder_aresult_node_acs_buffer_a1_a_a561_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a561 = adata_Reg_ff_adffs_a24_a $ adata_Reg_ff_adffs_a9_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a559
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a563 = CARRY(adata_Reg_ff_adffs_a24_a & adata_Reg_ff_adffs_a9_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a559 # !adata_Reg_ff_adffs_a24_a & (adata_Reg_ff_adffs_a9_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a559))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a24_a,
	datab => adata_Reg_ff_adffs_a9_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a559,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a561,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a563);

sub1_aadder_aresult_node_acs_buffer_a1_a_a565_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a565 = adata_Reg_ff_adffs_a25_a $ adata_Reg_ff_adffs_a10_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a563
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a567 = CARRY(adata_Reg_ff_adffs_a25_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a563 # !adata_Reg_ff_adffs_a10_a) # !adata_Reg_ff_adffs_a25_a & !adata_Reg_ff_adffs_a10_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a563)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a25_a,
	datab => adata_Reg_ff_adffs_a10_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a563,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a565,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a567);

sub1_aadder_aresult_node_acs_buffer_a1_a_a569_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a569 = adata_Reg_ff_adffs_a26_a $ adata_Reg_ff_adffs_a11_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a567
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a571 = CARRY(adata_Reg_ff_adffs_a26_a & adata_Reg_ff_adffs_a11_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a567 # !adata_Reg_ff_adffs_a26_a & (adata_Reg_ff_adffs_a11_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a567))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a26_a,
	datab => adata_Reg_ff_adffs_a11_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a567,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a569,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a571);

sub1_aadder_aresult_node_acs_buffer_a1_a_a573_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a573 = adata_Reg_ff_adffs_a27_a $ adata_Reg_ff_adffs_a12_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a571
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a575 = CARRY(adata_Reg_ff_adffs_a27_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a571 # !adata_Reg_ff_adffs_a12_a) # !adata_Reg_ff_adffs_a27_a & !adata_Reg_ff_adffs_a12_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a571)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a27_a,
	datab => adata_Reg_ff_adffs_a12_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a571,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a573,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a575);

sub1_aadder_aresult_node_acs_buffer_a1_a_a577_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a577 = adata_Reg_ff_adffs_a13_a $ adata_Reg_ff_adffs_a28_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a575
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a579 = CARRY(adata_Reg_ff_adffs_a13_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a575 # !adata_Reg_ff_adffs_a28_a) # !adata_Reg_ff_adffs_a13_a & !adata_Reg_ff_adffs_a28_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a575)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a13_a,
	datab => adata_Reg_ff_adffs_a28_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a575,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a577,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a579);

sub1_aadder_aresult_node_acs_buffer_a1_a_a581_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a581 = adata_Reg_ff_adffs_a14_a $ adata_Reg_ff_adffs_a29_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a579
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a583 = CARRY(adata_Reg_ff_adffs_a14_a & adata_Reg_ff_adffs_a29_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a579 # !adata_Reg_ff_adffs_a14_a & (adata_Reg_ff_adffs_a29_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a579))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a14_a,
	datab => adata_Reg_ff_adffs_a29_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a579,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a581,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a583);

sub1_aadder_aresult_node_acs_buffer_a1_a_a585_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a585 = adata_Reg_ff_adffs_a30_a $ adata_Reg_ff_adffs_a15_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a583
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a587 = CARRY(adata_Reg_ff_adffs_a30_a & adata_Reg_ff_adffs_a15_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a583 # !adata_Reg_ff_adffs_a30_a & (adata_Reg_ff_adffs_a15_a # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a583))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a30_a,
	datab => adata_Reg_ff_adffs_a15_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a583,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a585,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a587);

sub1_aadder_aresult_node_acs_buffer_a1_a_a589_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a589 = adata_Reg_ff_adffs_a31_a $ adata_Reg_ff_adffs_a15_a $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a587
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a591 = CARRY(adata_Reg_ff_adffs_a31_a & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a587 # !adata_Reg_ff_adffs_a15_a) # !adata_Reg_ff_adffs_a31_a & !adata_Reg_ff_adffs_a15_a & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a587)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a31_a,
	datab => adata_Reg_ff_adffs_a15_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a589,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a591);

sub1_aadder_aresult_node_acs_buffer_a1_a_a593_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a593 = adata_Reg_ff_adffs_a31_a $ (sub1_aadder_aresult_node_acs_buffer_a1_a_a591 $ adata_Reg_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a31_a,
	datad => adata_Reg_ff_adffs_a15_a,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a591,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a593);

adata_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(0),
	combout => adata_a0_a_acombout);

adata_Reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- adata_Reg_ff_adffs_a0_a = DFFE(adata_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => adata_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => adata_Reg_ff_adffs_a0_a);

add1_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a0_a = DFFE(adata_Reg_ff_adffs_a32_a $ adata_Reg_ff_adffs_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a0_a_a178 = CARRY(adata_Reg_ff_adffs_a32_a & adata_Reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a32_a,
	datab => adata_Reg_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a0_a,
	cout => add1_ff_adffs_a0_a_a178);

add1_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a4_a = DFFE(adata_Reg_ff_adffs_a36_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a549 $ !add1_ff_adffs_a3_a_a127, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a4_a_a136 = CARRY(adata_Reg_ff_adffs_a36_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a549 # !add1_ff_adffs_a3_a_a127) # !adata_Reg_ff_adffs_a36_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a549 & !add1_ff_adffs_a3_a_a127)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a36_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a549,
	cin => add1_ff_adffs_a3_a_a127,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a4_a,
	cout => add1_ff_adffs_a4_a_a136);

add1_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a5_a = DFFE(adata_Reg_ff_adffs_a37_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a533 $ add1_ff_adffs_a4_a_a136, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a5_a_a124 = CARRY(adata_Reg_ff_adffs_a37_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a533 & !add1_ff_adffs_a4_a_a136 # !adata_Reg_ff_adffs_a37_a & (!add1_ff_adffs_a4_a_a136 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a533))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a37_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a533,
	cin => add1_ff_adffs_a4_a_a136,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a5_a,
	cout => add1_ff_adffs_a5_a_a124);

add1_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a10_a = DFFE(adata_Reg_ff_adffs_a42_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a565 $ !add1_ff_adffs_a9_a_a145, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a10_a_a148 = CARRY(adata_Reg_ff_adffs_a42_a & (sub1_aadder_aresult_node_acs_buffer_a1_a_a565 # !add1_ff_adffs_a9_a_a145) # !adata_Reg_ff_adffs_a42_a & sub1_aadder_aresult_node_acs_buffer_a1_a_a565 & !add1_ff_adffs_a9_a_a145)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a42_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a565,
	cin => add1_ff_adffs_a9_a_a145,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a10_a,
	cout => add1_ff_adffs_a10_a_a148);

add1_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a11_a = DFFE(adata_Reg_ff_adffs_a43_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a569 $ add1_ff_adffs_a10_a_a148, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a11_a_a151 = CARRY(adata_Reg_ff_adffs_a43_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a569 & !add1_ff_adffs_a10_a_a148 # !adata_Reg_ff_adffs_a43_a & (!add1_ff_adffs_a10_a_a148 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a569))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a43_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a569,
	cin => add1_ff_adffs_a10_a_a148,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a11_a,
	cout => add1_ff_adffs_a11_a_a151);

add1_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a17_a = DFFE(adata_Reg_ff_adffs_a47_a $ sub1_aadder_aresult_node_acs_buffer_a1_a_a593 $ add1_ff_adffs_a16_a_a166, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a17_a_a169 = CARRY(adata_Reg_ff_adffs_a47_a & !sub1_aadder_aresult_node_acs_buffer_a1_a_a593 & !add1_ff_adffs_a16_a_a166 # !adata_Reg_ff_adffs_a47_a & (!add1_ff_adffs_a16_a_a166 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a593))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a47_a,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a593,
	cin => add1_ff_adffs_a16_a_a166,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a17_a,
	cout => add1_ff_adffs_a17_a_a169);

add1_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a26_a = DFFE(adata_Reg_ff_adffs_a47_a $ (add1_ff_adffs_a17_a_a169 $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a593), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => adata_Reg_ff_adffs_a47_a,
	datad => sub1_aadder_aresult_node_acs_buffer_a1_a_a593,
	cin => add1_ff_adffs_a17_a_a169,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a26_a);

AD_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_MR,
	combout => AD_MR_acombout);

add2b_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a1_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a1_a $ add2b_ff_adffs_a1_a $ add2b_ff_adffs_a0_a_a336, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a1_a_a324 = CARRY(add1_ff_adffs_a1_a & !add2b_ff_adffs_a1_a & !add2b_ff_adffs_a0_a_a336 # !add1_ff_adffs_a1_a & (!add2b_ff_adffs_a0_a_a336 # !add2b_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a1_a,
	datab => add2b_ff_adffs_a1_a,
	cin => add2b_ff_adffs_a0_a_a336,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a1_a,
	cout => add2b_ff_adffs_a1_a_a324);

add2b_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a2_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a2_a $ add2b_ff_adffs_a2_a $ !add2b_ff_adffs_a1_a_a324, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a2_a_a273 = CARRY(add1_ff_adffs_a2_a & (add2b_ff_adffs_a2_a # !add2b_ff_adffs_a1_a_a324) # !add1_ff_adffs_a2_a & add2b_ff_adffs_a2_a & !add2b_ff_adffs_a1_a_a324)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a2_a,
	datab => add2b_ff_adffs_a2_a,
	cin => add2b_ff_adffs_a1_a_a324,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a2_a,
	cout => add2b_ff_adffs_a2_a_a273);

add2b_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a3_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a3_a $ add2b_ff_adffs_a3_a $ add2b_ff_adffs_a2_a_a273, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a3_a_a270 = CARRY(add1_ff_adffs_a3_a & !add2b_ff_adffs_a3_a & !add2b_ff_adffs_a2_a_a273 # !add1_ff_adffs_a3_a & (!add2b_ff_adffs_a2_a_a273 # !add2b_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a3_a,
	datab => add2b_ff_adffs_a3_a,
	cin => add2b_ff_adffs_a2_a_a273,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a3_a,
	cout => add2b_ff_adffs_a3_a_a270);

add2b_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a4_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a4_a $ add1_ff_adffs_a4_a $ !add2b_ff_adffs_a3_a_a270, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a4_a_a279 = CARRY(add2b_ff_adffs_a4_a & (add1_ff_adffs_a4_a # !add2b_ff_adffs_a3_a_a270) # !add2b_ff_adffs_a4_a & add1_ff_adffs_a4_a & !add2b_ff_adffs_a3_a_a270)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a4_a,
	datab => add1_ff_adffs_a4_a,
	cin => add2b_ff_adffs_a3_a_a270,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a4_a,
	cout => add2b_ff_adffs_a4_a_a279);

add2b_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a5_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a5_a $ add1_ff_adffs_a5_a $ add2b_ff_adffs_a4_a_a279, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a5_a_a267 = CARRY(add2b_ff_adffs_a5_a & !add1_ff_adffs_a5_a & !add2b_ff_adffs_a4_a_a279 # !add2b_ff_adffs_a5_a & (!add2b_ff_adffs_a4_a_a279 # !add1_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => add1_ff_adffs_a5_a,
	cin => add2b_ff_adffs_a4_a_a279,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a5_a,
	cout => add2b_ff_adffs_a5_a_a267);

add2b_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a6_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a6_a $ add2b_ff_adffs_a6_a $ !add2b_ff_adffs_a5_a_a267, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a6_a_a276 = CARRY(add1_ff_adffs_a6_a & (add2b_ff_adffs_a6_a # !add2b_ff_adffs_a5_a_a267) # !add1_ff_adffs_a6_a & add2b_ff_adffs_a6_a & !add2b_ff_adffs_a5_a_a267)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a6_a,
	datab => add2b_ff_adffs_a6_a,
	cin => add2b_ff_adffs_a5_a_a267,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a6_a,
	cout => add2b_ff_adffs_a6_a_a276);

add2b_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a7_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a7_a $ add2b_ff_adffs_a7_a $ add2b_ff_adffs_a6_a_a276, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a7_a_a282 = CARRY(add1_ff_adffs_a7_a & !add2b_ff_adffs_a7_a & !add2b_ff_adffs_a6_a_a276 # !add1_ff_adffs_a7_a & (!add2b_ff_adffs_a6_a_a276 # !add2b_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a7_a,
	datab => add2b_ff_adffs_a7_a,
	cin => add2b_ff_adffs_a6_a_a276,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a7_a,
	cout => add2b_ff_adffs_a7_a_a282);

add2b_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a8_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a8_a $ add2b_ff_adffs_a8_a $ !add2b_ff_adffs_a7_a_a282, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a8_a_a285 = CARRY(add1_ff_adffs_a8_a & (add2b_ff_adffs_a8_a # !add2b_ff_adffs_a7_a_a282) # !add1_ff_adffs_a8_a & add2b_ff_adffs_a8_a & !add2b_ff_adffs_a7_a_a282)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a8_a,
	datab => add2b_ff_adffs_a8_a,
	cin => add2b_ff_adffs_a7_a_a282,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a8_a,
	cout => add2b_ff_adffs_a8_a_a285);

add2b_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a9_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a9_a $ add2b_ff_adffs_a9_a $ add2b_ff_adffs_a8_a_a285, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a9_a_a288 = CARRY(add1_ff_adffs_a9_a & !add2b_ff_adffs_a9_a & !add2b_ff_adffs_a8_a_a285 # !add1_ff_adffs_a9_a & (!add2b_ff_adffs_a8_a_a285 # !add2b_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a9_a,
	datab => add2b_ff_adffs_a9_a,
	cin => add2b_ff_adffs_a8_a_a285,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a9_a,
	cout => add2b_ff_adffs_a9_a_a288);

add2b_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a10_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a10_a $ add1_ff_adffs_a10_a $ !add2b_ff_adffs_a9_a_a288, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a10_a_a291 = CARRY(add2b_ff_adffs_a10_a & (add1_ff_adffs_a10_a # !add2b_ff_adffs_a9_a_a288) # !add2b_ff_adffs_a10_a & add1_ff_adffs_a10_a & !add2b_ff_adffs_a9_a_a288)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datab => add1_ff_adffs_a10_a,
	cin => add2b_ff_adffs_a9_a_a288,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a10_a,
	cout => add2b_ff_adffs_a10_a_a291);

add2b_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a11_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a11_a $ add1_ff_adffs_a11_a $ add2b_ff_adffs_a10_a_a291, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a11_a_a294 = CARRY(add2b_ff_adffs_a11_a & !add1_ff_adffs_a11_a & !add2b_ff_adffs_a10_a_a291 # !add2b_ff_adffs_a11_a & (!add2b_ff_adffs_a10_a_a291 # !add1_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a11_a,
	datab => add1_ff_adffs_a11_a,
	cin => add2b_ff_adffs_a10_a_a291,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a11_a,
	cout => add2b_ff_adffs_a11_a_a294);

add2b_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a12_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a12_a $ add2b_ff_adffs_a12_a $ !add2b_ff_adffs_a11_a_a294, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a12_a_a297 = CARRY(add1_ff_adffs_a12_a & (add2b_ff_adffs_a12_a # !add2b_ff_adffs_a11_a_a294) # !add1_ff_adffs_a12_a & add2b_ff_adffs_a12_a & !add2b_ff_adffs_a11_a_a294)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a12_a,
	datab => add2b_ff_adffs_a12_a,
	cin => add2b_ff_adffs_a11_a_a294,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a12_a,
	cout => add2b_ff_adffs_a12_a_a297);

add2b_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a13_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a13_a $ add2b_ff_adffs_a13_a $ add2b_ff_adffs_a12_a_a297, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a13_a_a300 = CARRY(add1_ff_adffs_a13_a & !add2b_ff_adffs_a13_a & !add2b_ff_adffs_a12_a_a297 # !add1_ff_adffs_a13_a & (!add2b_ff_adffs_a12_a_a297 # !add2b_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a13_a,
	datab => add2b_ff_adffs_a13_a,
	cin => add2b_ff_adffs_a12_a_a297,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a13_a,
	cout => add2b_ff_adffs_a13_a_a300);

add2b_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a14_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a14_a $ add2b_ff_adffs_a14_a $ !add2b_ff_adffs_a13_a_a300, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a14_a_a303 = CARRY(add1_ff_adffs_a14_a & (add2b_ff_adffs_a14_a # !add2b_ff_adffs_a13_a_a300) # !add1_ff_adffs_a14_a & add2b_ff_adffs_a14_a & !add2b_ff_adffs_a13_a_a300)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a14_a,
	datab => add2b_ff_adffs_a14_a,
	cin => add2b_ff_adffs_a13_a_a300,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a14_a,
	cout => add2b_ff_adffs_a14_a_a303);

add2b_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a15_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a15_a $ add2b_ff_adffs_a15_a $ add2b_ff_adffs_a14_a_a303, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a15_a_a306 = CARRY(add1_ff_adffs_a15_a & !add2b_ff_adffs_a15_a & !add2b_ff_adffs_a14_a_a303 # !add1_ff_adffs_a15_a & (!add2b_ff_adffs_a14_a_a303 # !add2b_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a15_a,
	cin => add2b_ff_adffs_a14_a_a303,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a15_a,
	cout => add2b_ff_adffs_a15_a_a306);

add2b_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a16_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a16_a $ add2b_ff_adffs_a16_a $ !add2b_ff_adffs_a15_a_a306, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a16_a_a309 = CARRY(add1_ff_adffs_a16_a & (add2b_ff_adffs_a16_a # !add2b_ff_adffs_a15_a_a306) # !add1_ff_adffs_a16_a & add2b_ff_adffs_a16_a & !add2b_ff_adffs_a15_a_a306)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a16_a,
	datab => add2b_ff_adffs_a16_a,
	cin => add2b_ff_adffs_a15_a_a306,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a16_a,
	cout => add2b_ff_adffs_a16_a_a309);

add2b_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a17_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a17_a $ add1_ff_adffs_a17_a $ add2b_ff_adffs_a16_a_a309, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a17_a_a312 = CARRY(add2b_ff_adffs_a17_a & !add1_ff_adffs_a17_a & !add2b_ff_adffs_a16_a_a309 # !add2b_ff_adffs_a17_a & (!add2b_ff_adffs_a16_a_a309 # !add1_ff_adffs_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add1_ff_adffs_a17_a,
	cin => add2b_ff_adffs_a16_a_a309,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a17_a,
	cout => add2b_ff_adffs_a17_a_a312);

add2b_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a18_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a18_a $ add1_ff_adffs_a26_a $ !add2b_ff_adffs_a17_a_a312, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a18_a_a315 = CARRY(add2b_ff_adffs_a18_a & (add1_ff_adffs_a26_a # !add2b_ff_adffs_a17_a_a312) # !add2b_ff_adffs_a18_a & add1_ff_adffs_a26_a & !add2b_ff_adffs_a17_a_a312)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datab => add1_ff_adffs_a26_a,
	cin => add2b_ff_adffs_a17_a_a312,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a18_a,
	cout => add2b_ff_adffs_a18_a_a315);

add2b_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a19_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a19_a $ add1_ff_adffs_a26_a $ add2b_ff_adffs_a18_a_a315, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a19_a_a318 = CARRY(add2b_ff_adffs_a19_a & !add1_ff_adffs_a26_a & !add2b_ff_adffs_a18_a_a315 # !add2b_ff_adffs_a19_a & (!add2b_ff_adffs_a18_a_a315 # !add1_ff_adffs_a26_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a19_a,
	datab => add1_ff_adffs_a26_a,
	cin => add2b_ff_adffs_a18_a_a315,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a19_a,
	cout => add2b_ff_adffs_a19_a_a318);

add2b_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a20_a = DFFE(!GLOBAL(AD_MR_acombout) & add2b_ff_adffs_a20_a $ add1_ff_adffs_a26_a $ !add2b_ff_adffs_a19_a_a318, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a20_a_a321 = CARRY(add2b_ff_adffs_a20_a & (add1_ff_adffs_a26_a # !add2b_ff_adffs_a19_a_a318) # !add2b_ff_adffs_a20_a & add1_ff_adffs_a26_a & !add2b_ff_adffs_a19_a_a318)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a20_a,
	datab => add1_ff_adffs_a26_a,
	cin => add2b_ff_adffs_a19_a_a318,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a20_a,
	cout => add2b_ff_adffs_a20_a_a321);

add2b_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a21_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a21_a $ add2b_ff_adffs_a20_a_a321, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a21_a_a345 = CARRY(add1_ff_adffs_a26_a & !add2b_ff_adffs_a21_a & !add2b_ff_adffs_a20_a_a321 # !add1_ff_adffs_a26_a & (!add2b_ff_adffs_a20_a_a321 # !add2b_ff_adffs_a21_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a26_a,
	datab => add2b_ff_adffs_a21_a,
	cin => add2b_ff_adffs_a20_a_a321,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a21_a,
	cout => add2b_ff_adffs_a21_a_a345);

add2b_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a22_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a22_a $ !add2b_ff_adffs_a21_a_a345, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a22_a_a342 = CARRY(add1_ff_adffs_a26_a & (add2b_ff_adffs_a22_a # !add2b_ff_adffs_a21_a_a345) # !add1_ff_adffs_a26_a & add2b_ff_adffs_a22_a & !add2b_ff_adffs_a21_a_a345)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a26_a,
	datab => add2b_ff_adffs_a22_a,
	cin => add2b_ff_adffs_a21_a_a345,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a22_a,
	cout => add2b_ff_adffs_a22_a_a342);

add2b_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a23_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a23_a $ add2b_ff_adffs_a22_a_a342, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a23_a_a339 = CARRY(add1_ff_adffs_a26_a & !add2b_ff_adffs_a23_a & !add2b_ff_adffs_a22_a_a342 # !add1_ff_adffs_a26_a & (!add2b_ff_adffs_a22_a_a342 # !add2b_ff_adffs_a23_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a26_a,
	datab => add2b_ff_adffs_a23_a,
	cin => add2b_ff_adffs_a22_a_a342,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a23_a,
	cout => add2b_ff_adffs_a23_a_a339);

add2b_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a24_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a24_a $ !add2b_ff_adffs_a23_a_a339, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a24_a_a333 = CARRY(add1_ff_adffs_a26_a & (add2b_ff_adffs_a24_a # !add2b_ff_adffs_a23_a_a339) # !add1_ff_adffs_a26_a & add2b_ff_adffs_a24_a & !add2b_ff_adffs_a23_a_a339)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a26_a,
	datab => add2b_ff_adffs_a24_a,
	cin => add2b_ff_adffs_a23_a_a339,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a24_a,
	cout => add2b_ff_adffs_a24_a_a333);

add2b_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a25_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a25_a $ add2b_ff_adffs_a24_a_a333, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a25_a_a330 = CARRY(add1_ff_adffs_a26_a & !add2b_ff_adffs_a25_a & !add2b_ff_adffs_a24_a_a333 # !add1_ff_adffs_a26_a & (!add2b_ff_adffs_a24_a_a333 # !add2b_ff_adffs_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a26_a,
	datab => add2b_ff_adffs_a25_a,
	cin => add2b_ff_adffs_a24_a_a333,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a25_a,
	cout => add2b_ff_adffs_a25_a_a330);

add2b_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a26_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a26_a $ add2b_ff_adffs_a25_a_a330 $ !add2b_ff_adffs_a26_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add1_ff_adffs_a26_a,
	datad => add2b_ff_adffs_a26_a,
	cin => add2b_ff_adffs_a25_a_a330,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a26_a);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out = add2b_ff_adffs_a26_a $ (dselect_reg_ff_adffs_a1_a & (!Dly1_asram_aq_a26_a) # !dselect_reg_ff_adffs_a1_a & !fir_data_Del1_ff_adffs_a26_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E21D",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_Del1_ff_adffs_a26_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a26_a,
	datad => add2b_ff_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out);

Dly1_asram_asegment_a0_a_a25_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 25,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a25_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a25_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a25_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a25_a_modesel,
	dataout => Dly1_asram_aq_a25_a);

fir_data_Del1_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a25_a = DFFE(add2b_ff_adffs_a25_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a25_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a25_a);

fir_data_DelM_a25_a_a268_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a25_a_a268 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a25_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a25_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dly1_asram_aq_a25_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a25_a_a268);

Dly1_asram_asegment_a0_a_a24_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 24,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a24_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a24_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a24_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a24_a_modesel,
	dataout => Dly1_asram_aq_a24_a);

fir_data_Del1_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a24_a = DFFE(add2b_ff_adffs_a24_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a24_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a24_a);

fir_data_DelM_a24_a_a267_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a24_a_a267 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a24_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a24_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dly1_asram_aq_a24_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a24_a_a267);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = add2b_ff_adffs_a25_a & fir_data_DelM_a25_a_a268 & (add2b_ff_adffs_a24_a $ !fir_data_DelM_a24_a_a267) # !add2b_ff_adffs_a25_a & !fir_data_DelM_a25_a_a268 & (add2b_ff_adffs_a24_a $ 
-- !fir_data_DelM_a24_a_a267)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a25_a,
	datab => fir_data_DelM_a25_a_a268,
	datac => add2b_ff_adffs_a24_a,
	datad => fir_data_DelM_a24_a_a267,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out & PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), 
-- , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out = !add2b_ff_adffs_a26_a & (dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a26_a) # !dselect_reg_ff_adffs_a1_a & fir_data_Del1_ff_adffs_a26_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00E2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_Del1_ff_adffs_a26_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a26_a,
	datad => add2b_ff_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out = add2b_ff_adffs_a25_a & (add2b_ff_adffs_a24_a & !fir_data_DelM_a24_a_a267 # !fir_data_DelM_a25_a_a268) # !add2b_ff_adffs_a25_a & !fir_data_DelM_a25_a_a268 & add2b_ff_adffs_a24_a & 
-- !fir_data_DelM_a24_a_a267

-- pragma translate_off
GENERIC MAP (
	lut_mask => "22B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a25_a,
	datab => fir_data_DelM_a25_a_a268,
	datac => add2b_ff_adffs_a24_a,
	datad => fir_data_DelM_a24_a_a267,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out # PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aaeb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_end_aagb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out);

Dly1_asram_asegment_a0_a_a23_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 23,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a23_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a23_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a23_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a23_a_modesel,
	dataout => Dly1_asram_aq_a23_a);

fir_data_Del1_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a23_a = DFFE(add2b_ff_adffs_a23_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a23_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a23_a);

fir_data_DelM_a23_a_a270_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a23_a_a270 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a23_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a23_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a23_a,
	datad => fir_data_Del1_ff_adffs_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a23_a_a270);

Dly1_asram_asegment_a0_a_a22_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 22,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a22_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a22_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a22_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a22_a_modesel,
	dataout => Dly1_asram_aq_a22_a);

fir_data_Del1_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a22_a = DFFE(add2b_ff_adffs_a22_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a22_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a22_a);

fir_data_DelM_a22_a_a269_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a22_a_a269 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a22_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a22_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a22_a,
	datad => fir_data_Del1_ff_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a22_a_a269);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out = fir_data_DelM_a23_a_a270 & add2b_ff_adffs_a22_a & add2b_ff_adffs_a23_a & !fir_data_DelM_a22_a_a269 # !fir_data_DelM_a23_a_a270 & (add2b_ff_adffs_a23_a # add2b_ff_adffs_a22_a & 
-- !fir_data_DelM_a22_a_a269)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a22_a,
	datab => fir_data_DelM_a23_a_a270,
	datac => add2b_ff_adffs_a23_a,
	datad => fir_data_DelM_a22_a_a269,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out = add2b_ff_adffs_a22_a & fir_data_DelM_a22_a_a269 & (fir_data_DelM_a23_a_a270 $ !add2b_ff_adffs_a23_a) # !add2b_ff_adffs_a22_a & !fir_data_DelM_a22_a_a269 & (fir_data_DelM_a23_a_a270 $ 
-- !add2b_ff_adffs_a23_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a22_a,
	datab => fir_data_DelM_a23_a_a270,
	datac => add2b_ff_adffs_a23_a,
	datad => fir_data_DelM_a22_a_a269,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out);

Dly1_asram_asegment_a0_a_a18_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 18,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a18_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a18_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a18_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a18_a_modesel,
	dataout => Dly1_asram_aq_a18_a);

fir_data_DelM_a18_a_a273_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a18_a_a273 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a18_a) # !dselect_reg_ff_adffs_a1_a & fir_data_Del1_ff_adffs_a18_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E2E2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_Del1_ff_adffs_a18_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a18_a_a273);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out = fir_data_DelM_a19_a_a274 & add2b_ff_adffs_a19_a & add2b_ff_adffs_a18_a & !fir_data_DelM_a18_a_a273 # !fir_data_DelM_a19_a_a274 & (add2b_ff_adffs_a19_a # add2b_ff_adffs_a18_a & 
-- !fir_data_DelM_a18_a_a273)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "44D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a19_a_a274,
	datab => add2b_ff_adffs_a19_a,
	datac => add2b_ff_adffs_a18_a,
	datad => fir_data_DelM_a18_a_a273,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out);

fir_data_Del1_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a21_a = DFFE(add2b_ff_adffs_a21_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a21_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a21_a);

Dly1_asram_asegment_a0_a_a21_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 21,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a21_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a21_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a21_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a21_a_modesel,
	dataout => Dly1_asram_aq_a21_a);

fir_data_DelM_a21_a_a272_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a21_a_a272 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a21_a) # !dselect_reg_ff_adffs_a1_a & fir_data_Del1_ff_adffs_a21_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_data_Del1_ff_adffs_a21_a,
	datac => Dly1_asram_aq_a21_a,
	datad => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a21_a_a272);

Dly1_asram_asegment_a0_a_a20_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 20,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a20_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a20_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a20_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a20_a_modesel,
	dataout => Dly1_asram_aq_a20_a);

fir_data_Del1_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a20_a = DFFE(add2b_ff_adffs_a20_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a20_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a20_a);

fir_data_DelM_a20_a_a271_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a20_a_a271 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a20_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a20_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dly1_asram_aq_a20_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a20_a_a271);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out = add2b_ff_adffs_a21_a & (add2b_ff_adffs_a20_a & !fir_data_DelM_a20_a_a271 # !fir_data_DelM_a21_a_a272) # !add2b_ff_adffs_a21_a & add2b_ff_adffs_a20_a & !fir_data_DelM_a21_a_a272 & 
-- !fir_data_DelM_a20_a_a271

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A8E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => add2b_ff_adffs_a20_a,
	datac => fir_data_DelM_a21_a_a272,
	datad => fir_data_DelM_a20_a_a271,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out = PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aaeb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a0_a_aagb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a1_a_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCCC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

Dly1_asram_asegment_a0_a_a17_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 17,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a17_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a17_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a17_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a17_a_modesel,
	dataout => Dly1_asram_aq_a17_a);

fir_data_Del1_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a17_a = DFFE(add2b_ff_adffs_a17_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a17_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a17_a);

fir_data_DelM_a17_a_a276_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a17_a_a276 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a17_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a17_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a17_a,
	datad => fir_data_Del1_ff_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a17_a_a276);

Dly1_asram_asegment_a0_a_a16_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 16,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a16_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a16_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a16_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a16_a_modesel,
	dataout => Dly1_asram_aq_a16_a);

fir_data_Del1_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a16_a = DFFE(add2b_ff_adffs_a16_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a16_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a16_a);

fir_data_DelM_a16_a_a275_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a16_a_a275 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a16_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a16_a,
	datad => fir_data_Del1_ff_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a16_a_a275);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out = add2b_ff_adffs_a17_a & fir_data_DelM_a17_a_a276 & (add2b_ff_adffs_a16_a $ !fir_data_DelM_a16_a_a275) # !add2b_ff_adffs_a17_a & !fir_data_DelM_a17_a_a276 & (add2b_ff_adffs_a16_a $ 
-- !fir_data_DelM_a16_a_a275)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a16_a,
	datac => fir_data_DelM_a17_a_a276,
	datad => fir_data_DelM_a16_a_a275,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out);

Dly1_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a15_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a15_a_modesel,
	dataout => Dly1_asram_aq_a15_a);

fir_data_Del1_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a15_a = DFFE(add2b_ff_adffs_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a15_a);

fir_data_DelM_a15_a_a278_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a15_a_a278 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a15_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dly1_asram_aq_a15_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a15_a_a278);

Dly1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a14_a_modesel,
	dataout => Dly1_asram_aq_a14_a);

fir_data_Del1_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a14_a = DFFE(add2b_ff_adffs_a14_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a14_a);

fir_data_DelM_a14_a_a277_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a14_a_a277 = dselect_reg_ff_adffs_a1_a & Dly1_asram_aq_a14_a # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dly1_asram_aq_a14_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => fir_data_Del1_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a14_a_a277);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out = fir_data_DelM_a15_a_a278 & add2b_ff_adffs_a14_a & add2b_ff_adffs_a15_a & !fir_data_DelM_a14_a_a277 # !fir_data_DelM_a15_a_a278 & (add2b_ff_adffs_a15_a # add2b_ff_adffs_a14_a & 
-- !fir_data_DelM_a14_a_a277)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a14_a,
	datab => fir_data_DelM_a15_a_a278,
	datac => add2b_ff_adffs_a15_a,
	datad => fir_data_DelM_a14_a_a277,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out = add2b_ff_adffs_a14_a & fir_data_DelM_a14_a_a277 & (fir_data_DelM_a15_a_a278 $ !add2b_ff_adffs_a15_a) # !add2b_ff_adffs_a14_a & !fir_data_DelM_a14_a_a277 & (fir_data_DelM_a15_a_a278 $ 
-- !add2b_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a14_a,
	datab => fir_data_DelM_a15_a_a278,
	datac => add2b_ff_adffs_a15_a,
	datad => fir_data_DelM_a14_a_a277,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out = PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a0_a_aagb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aagb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a1_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

Dly1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a11_a_modesel,
	dataout => Dly1_asram_aq_a11_a);

fir_data_Del1_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a11_a = DFFE(add2b_ff_adffs_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a11_a);

fir_data_DelM_a11_a_a282_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a11_a_a282 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a11_a) # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a11_a,
	datad => fir_data_Del1_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a11_a_a282);

Dly1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a10_a_modesel,
	dataout => Dly1_asram_aq_a10_a);

fir_data_Del1_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a10_a = DFFE(add2b_ff_adffs_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a10_a);

fir_data_DelM_a10_a_a281_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a10_a_a281 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a10_a) # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a10_a,
	datad => fir_data_Del1_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a10_a_a281);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out = add2b_ff_adffs_a10_a & fir_data_DelM_a10_a_a281 & (fir_data_DelM_a11_a_a282 $ !add2b_ff_adffs_a11_a) # !add2b_ff_adffs_a10_a & !fir_data_DelM_a10_a_a281 & (fir_data_DelM_a11_a_a282 $ 
-- !add2b_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datab => fir_data_DelM_a11_a_a282,
	datac => add2b_ff_adffs_a11_a,
	datad => fir_data_DelM_a10_a_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out);

Dly1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a7_a_modesel,
	dataout => Dly1_asram_aq_a7_a);

fir_data_Del1_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a7_a = DFFE(add2b_ff_adffs_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => add2b_ff_adffs_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a7_a);

fir_data_DelM_a7_a_a286_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a7_a_a286 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a7_a) # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a7_a,
	datad => fir_data_Del1_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a7_a_a286);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out = add2b_ff_adffs_a7_a & (!fir_data_DelM_a6_a_a285 & add2b_ff_adffs_a6_a # !fir_data_DelM_a7_a_a286) # !add2b_ff_adffs_a7_a & !fir_data_DelM_a6_a_a285 & add2b_ff_adffs_a6_a & 
-- !fir_data_DelM_a7_a_a286

-- pragma translate_off
GENERIC MAP (
	lut_mask => "40DC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_data_DelM_a6_a_a285,
	datab => add2b_ff_adffs_a7_a,
	datac => add2b_ff_adffs_a6_a,
	datad => fir_data_DelM_a7_a_a286,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out);

Dly1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a9_a_modesel,
	dataout => Dly1_asram_aq_a9_a);

fir_data_Del1_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a9_a = DFFE(add2b_ff_adffs_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a9_a);

fir_data_DelM_a9_a_a284_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a9_a_a284 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a9_a) # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a9_a,
	datad => fir_data_Del1_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a9_a_a284);

Dly1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 3,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 7,
	logical_ram_depth => 8,
	logical_ram_name => "lpm_ram_dp:Dly1|altdpram:sram|content",
	logical_ram_width => 27,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly1_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Dly1_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly1_asram_asegment_a0_a_a8_a_modesel,
	dataout => Dly1_asram_aq_a8_a);

fir_data_Del1_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_data_Del1_ff_adffs_a8_a = DFFE(add2b_ff_adffs_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add2b_ff_adffs_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_data_Del1_ff_adffs_a8_a);

fir_data_DelM_a8_a_a283_I : apex20ke_lcell
-- Equation(s):
-- fir_data_DelM_a8_a_a283 = dselect_reg_ff_adffs_a1_a & (Dly1_asram_aq_a8_a) # !dselect_reg_ff_adffs_a1_a & (fir_data_Del1_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datac => Dly1_asram_aq_a8_a,
	datad => fir_data_Del1_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_data_DelM_a8_a_a283);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out = add2b_ff_adffs_a9_a & (add2b_ff_adffs_a8_a & !fir_data_DelM_a8_a_a283 # !fir_data_DelM_a9_a_a284) # !add2b_ff_adffs_a9_a & !fir_data_DelM_a9_a_a284 & add2b_ff_adffs_a8_a & 
-- !fir_data_DelM_a8_a_a283

-- pragma translate_off
GENERIC MAP (
	lut_mask => "22B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => fir_data_DelM_a9_a_a284,
	datac => add2b_ff_adffs_a8_a,
	datad => fir_data_DelM_a8_a_a283,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out = add2b_ff_adffs_a9_a & fir_data_DelM_a9_a_a284 & (add2b_ff_adffs_a8_a $ !fir_data_DelM_a8_a_a283) # !add2b_ff_adffs_a9_a & !fir_data_DelM_a9_a_a284 & (add2b_ff_adffs_a8_a $ 
-- !fir_data_DelM_a8_a_a283)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => fir_data_DelM_a9_a_a284,
	datac => add2b_ff_adffs_a8_a,
	datad => fir_data_DelM_a8_a_a283,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out = PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a0_a_aagb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aagb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a1_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out = fir_data_DelM_a11_a_a282 & add2b_ff_adffs_a10_a & add2b_ff_adffs_a11_a & !fir_data_DelM_a10_a_a281 # !fir_data_DelM_a11_a_a282 & (add2b_ff_adffs_a11_a # add2b_ff_adffs_a10_a & 
-- !fir_data_DelM_a10_a_a281)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30B2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datab => fir_data_DelM_a11_a_a282,
	datac => add2b_ff_adffs_a11_a,
	datad => fir_data_DelM_a10_a_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out # PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out & 
-- PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out,
	datac => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a = CARRY()

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "qfbk_counter",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a0_a_a_a00006,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Gen_Comp_acomparator_acmp_a0_a_aagb_out,
	cout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a);

PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a = CARRY(!PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out & (!PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a # 
-- !PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0013",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => PSlope_Gen_Comp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5,
	cout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a);

PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a = CARRY(PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out # 
-- PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out & !PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CE",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => PSlope_Gen_Comp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4,
	cout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a);

PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a = CARRY(!PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out & (!PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a # 
-- !PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0013",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => PSlope_Gen_Comp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3,
	cout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a);

PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out = PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out # PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out & 
-- !PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out,
	datad => PSlope_Gen_Comp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out,
	cin => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out);

pslope_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a0_a = DFFE(PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a0_a);

pslope_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a1_a = DFFE(pslope_vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a1_a);

PSlope_a193_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a193 = pslope_vec_a0_a # pslope_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pslope_vec_a0_a,
	datad => pslope_vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a193);

PSlope_a191_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a191 = pslope_vec_a0_a & pslope_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pslope_vec_a0_a,
	datad => pslope_vec_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a191);

pslope_vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a2_a = DFFE(pslope_vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a2_a);

pslope_vec_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a3_a = DFFE(pslope_vec_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a3_a);

PSlope_a188_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a188 = pslope_vec_a2_a # pslope_vec_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pslope_vec_a2_a,
	datac => pslope_vec_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a188);

pslope_vec_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a4_a = DFFE(pslope_vec_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pslope_vec_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a4_a);

PSlope_a189_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a189 = dselect_reg_ff_adffs_a1_a & PSlope & (PSlope_a188 # pslope_vec_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => PSlope_a188,
	datac => PSlope,
	datad => pslope_vec_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a189);

PSlope_a192_I : apex20ke_lcell
-- Equation(s):
-- PSlope_a192 = PSlope_a189 # PSlope_a190 & PSlope_a191 & PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF80",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_a190,
	datab => PSlope_a191,
	datac => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out,
	datad => PSlope_a189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_a192);

PSlope_aI : apex20ke_lcell
-- Equation(s):
-- PSlope = DFFE(PSlope_a192 # PSlope & (PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out # PSlope_a193), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFE0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Gen_Comp_acomparator_asub_comptree_agt_cmp_end_aagb_out,
	datab => PSlope_a193,
	datac => PSlope,
	datad => PSlope_a192,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope);

PSlope_Del_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Del = DFFE(PSlope, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Del);

tlevel_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(15),
	combout => tlevel_a15_a_acombout);

tlevel_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a15_a = DFFE(tlevel_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a15_a);

tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out = tlevel_reg_ff_adffs_a15_a $ !add2b_ff_adffs_a26_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => add2b_ff_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out);

tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out_a24 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out = !tlevel_reg_ff_adffs_a15_a & (add2b_ff_adffs_a24_a # add2b_ff_adffs_a25_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a24_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => add2b_ff_adffs_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out);

tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out = tlevel_reg_ff_adffs_a15_a & !add2b_ff_adffs_a26_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => add2b_ff_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out);

tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out = DFFE(tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out # tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out & tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out, 
-- GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aaeb_out,
	datac => tlevel_cmp_acomparator_acmp_end_acomp_acmp_a0_a_aagb_out,
	datad => tlevel_cmp_acomparator_acmp_end_acomp_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out = tlevel_reg_ff_adffs_a15_a & add2b_ff_adffs_a23_a & add2b_ff_adffs_a22_a # !tlevel_reg_ff_adffs_a15_a & !add2b_ff_adffs_a23_a & !add2b_ff_adffs_a22_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C003",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a23_a,
	datad => add2b_ff_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out_a31 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out = !tlevel_reg_ff_adffs_a15_a & (add2b_ff_adffs_a23_a # add2b_ff_adffs_a22_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3330",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a23_a,
	datad => add2b_ff_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out);

tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out # tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out & 
-- tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	datac => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a3_a_acomp_acmp_a2_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

tlevel_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(12),
	combout => tlevel_a12_a_acombout);

tlevel_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a12_a = DFFE(tlevel_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a12_a);

DSelect_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSelect(0),
	combout => DSelect_a0_a_acombout);

dselect_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- dselect_reg_ff_adffs_a0_a = DFFE(DSelect_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSelect_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => dselect_reg_ff_adffs_a0_a);

tlevel_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(14),
	combout => tlevel_a14_a_acombout);

tlevel_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a14_a = DFFE(tlevel_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a14_a);

tlevel_extm_a17_a_a1397_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a17_a_a1397 = dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a14_a) # !dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => tlevel_reg_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a17_a_a1397);

tlevel_extm_a17_a_a1398_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a17_a_a1398 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a12_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a17_a_a1397)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a17_a_a1397,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a17_a_a1398);

tlevel_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(13),
	combout => tlevel_a13_a_acombout);

tlevel_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a13_a = DFFE(tlevel_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a13_a);

tlevel_extm_a16_a_a1395_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a16_a_a1395 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a13_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a13_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a16_a_a1395);

tlevel_extm_a16_a_a1396_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a16_a_a1396 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a11_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a16_a_a1395)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a11_a,
	datac => tlevel_extm_a16_a_a1395,
	datad => dselect_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a16_a_a1396);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out = add2b_ff_adffs_a17_a & tlevel_extm_a17_a_a1398 & (add2b_ff_adffs_a16_a $ !tlevel_extm_a16_a_a1396) # !add2b_ff_adffs_a17_a & !tlevel_extm_a17_a_a1398 & (add2b_ff_adffs_a16_a $ 
-- !tlevel_extm_a16_a_a1396)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a16_a,
	datac => tlevel_extm_a17_a_a1398,
	datad => tlevel_extm_a16_a_a1396,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out = add2b_ff_adffs_a17_a & (add2b_ff_adffs_a16_a & !tlevel_extm_a16_a_a1396 # !tlevel_extm_a17_a_a1398) # !add2b_ff_adffs_a17_a & add2b_ff_adffs_a16_a & !tlevel_extm_a17_a_a1398 & 
-- !tlevel_extm_a16_a_a1396

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A8E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a16_a,
	datac => tlevel_extm_a17_a_a1398,
	datad => tlevel_extm_a16_a_a1396,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out);

tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out # tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out & 
-- tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	datac => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a2_a_acomp_acmp_a2_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

tlevel_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(8),
	combout => tlevel_a8_a_acombout);

tlevel_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a8_a = DFFE(tlevel_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a8_a);

tlevel_extm_a10_a_a1407_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a10_a_a1407 = dselect_reg_ff_adffs_a0_a & tlevel_reg_ff_adffs_a7_a # !dselect_reg_ff_adffs_a0_a & (tlevel_reg_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a7_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a10_a_a1407);

tlevel_extm_a10_a_a1408_I : apex20ke_lcell
-- Equation(s):
-- tlevel_extm_a10_a_a1408 = dselect_reg_ff_adffs_a1_a & tlevel_reg_ff_adffs_a5_a # !dselect_reg_ff_adffs_a1_a & (tlevel_extm_a10_a_a1407)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a5_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_extm_a10_a_a1407,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_extm_a10_a_a1408);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out = tlevel_extm_a11_a_a1410 & add2b_ff_adffs_a11_a & (add2b_ff_adffs_a10_a $ !tlevel_extm_a10_a_a1408) # !tlevel_extm_a11_a_a1410 & !add2b_ff_adffs_a11_a & (add2b_ff_adffs_a10_a $ 
-- !tlevel_extm_a10_a_a1408)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a11_a_a1410,
	datab => add2b_ff_adffs_a10_a,
	datac => add2b_ff_adffs_a11_a,
	datad => tlevel_extm_a10_a_a1408,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out = tlevel_extm_a11_a_a1410 & add2b_ff_adffs_a10_a & add2b_ff_adffs_a11_a & !tlevel_extm_a10_a_a1408 # !tlevel_extm_a11_a_a1410 & (add2b_ff_adffs_a11_a # add2b_ff_adffs_a10_a & 
-- !tlevel_extm_a10_a_a1408)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "50D4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_extm_a11_a_a1410,
	datab => add2b_ff_adffs_a10_a,
	datac => add2b_ff_adffs_a11_a,
	datad => tlevel_extm_a10_a_a1408,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out);

tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out = DFFE(tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out # tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out & 
-- tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_acmp_a0_a_aagb_out,
	datac => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_a1_a_acomp_acmp_a2_a_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out);

tlevel_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(0),
	combout => tlevel_a0_a_acombout);

tlevel_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a0_a = DFFE(tlevel_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a0_a);

tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140 = dselect_reg_ff_adffs_a1_a & !tlevel_reg_ff_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => dselect_reg_ff_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140);

tlevel_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(2),
	combout => tlevel_a2_a_acombout);

tlevel_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a2_a = DFFE(tlevel_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a2_a);

tlevel_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(1),
	combout => tlevel_a1_a_acombout);

tlevel_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a1_a = DFFE(tlevel_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a1_a);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402 = dselect_reg_ff_adffs_a1_a # dselect_reg_ff_adffs_a0_a & (!tlevel_reg_ff_adffs_a1_a) # !dselect_reg_ff_adffs_a0_a & !tlevel_reg_ff_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ABFB",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405 = dselect_reg_ff_adffs_a1_a # dselect_reg_ff_adffs_a0_a & !tlevel_reg_ff_adffs_a0_a # !dselect_reg_ff_adffs_a0_a & (!tlevel_reg_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BABF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a0_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a_a389_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a = CARRY(!add2b_ff_adffs_a1_a & (!tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a1_a,
	cin => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a_a389,
	cout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a388_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a = CARRY(tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406 & (add2b_ff_adffs_a2_a # !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a) # !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406 & add2b_ff_adffs_a2_a & 
-- !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a406,
	datab => add2b_ff_adffs_a2_a,
	cin => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a_a388,
	cout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a387_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a = CARRY(add2b_ff_adffs_a3_a & !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405 & !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a # !add2b_ff_adffs_a3_a & (!tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a # 
-- !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a3_a,
	datab => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a405,
	cin => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a_a387,
	cout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a);

tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a386_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a = CARRY(add2b_ff_adffs_a4_a & (tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402 # !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a) # !add2b_ff_adffs_a4_a & tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402 & 
-- !tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a4_a,
	datab => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a402,
	cin => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a_a386,
	cout => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a);

tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a141 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_a0_a_a_a00006 = add2b_ff_adffs_a5_a & (tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139 # tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a # tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140) # !add2b_ff_adffs_a5_a & 
-- tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a & (tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139 # tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FCE8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a139,
	datab => add2b_ff_adffs_a5_a,
	datad => tlevel_cmp_acomparator_acmp_a0_a_aagb_out_node_a140,
	cin => tlevel_cmp_acomparator_acmp_a0_a_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_a0_a_a_a00006);

tlevel_cmp_acomparator_acmp_a0_a_aagb_out_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a = CARRY()

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "qfbk_counter",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_cmp_acomparator_acmp_a0_a_a_a00006,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_cmp_acomparator_acmp_a0_a_aagb_out,
	cout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a);

tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a = CARRY(!tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out & (!tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a # 
-- !tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0013",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => tlevel_cmp_acomparator_acmp_a1_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a_a5,
	cout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a);

tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a = CARRY(tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out # tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out & 
-- !tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CE",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => tlevel_cmp_acomparator_acmp_a2_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a_a4,
	cout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a);

tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a = CARRY(!tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out & (!tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a # 
-- !tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0013",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_aeq_cmp_end_aaeb_out,
	datab => tlevel_cmp_acomparator_acmp_a3_a_acomp_asub_comptree_asub_comptree_agt_cmp_end_aagb_out,
	cin => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a_a3,
	cout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a);

tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out = tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out # tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out & 
-- !tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FF0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_aeq_cmp_end_aaeb_out,
	datad => tlevel_cmp_acomparator_acmp_end_acomp_asub_comptree_agt_cmp_end_aagb_out,
	cin => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out);

Disc_En_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En,
	combout => Disc_En_acombout);

clock_proc_a52_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a52 = !PSlope & Disc_En_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PSlope,
	datad => Disc_En_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a52);

Level_Out_areg0_I : apex20ke_lcell
-- Equation(s):
-- Level_Out_areg0 = DFFE(!PA_Reset_acombout & PSlope_Del & tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out & clock_proc_a52, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => PSlope_Del,
	datac => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out,
	datad => clock_proc_a52,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Level_Out_areg0);

fir_out_a0_a_a1541_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1541 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a3_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a2_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => add2b_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1541);

fir_out_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & (add2b_ff_adffs_a5_a) # !dselect_reg_ff_adffs_a1_a & fir_out_a0_a_a1541), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00E2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1541,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => add2b_ff_adffs_a5_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a0_a_areg0);

fir_out_a1_a_a1543_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a1_a_a1543 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a4_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a3_a,
	datab => add2b_ff_adffs_a4_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a1_a_a1543);

fir_out_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a1_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a6_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a1_a_a1543)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00D8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => add2b_ff_adffs_a6_a,
	datac => fir_out_a1_a_a1543,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a1_a_areg0);

fir_out_a2_a_a1545_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a2_a_a1545 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a5_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => add2b_ff_adffs_a4_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a2_a_a1545);

fir_out_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a2_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a7_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a2_a_a1545)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0B08",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a7_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => PA_Reset_acombout,
	datad => fir_out_a2_a_a1545,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a2_a_areg0);

fir_out_a3_a_a1547_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a3_a_a1547 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a6_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => add2b_ff_adffs_a6_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a3_a_a1547);

fir_out_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a3_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a8_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a3_a_a1547)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0B08",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a8_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => PA_Reset_acombout,
	datad => fir_out_a3_a_a1547,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a3_a_areg0);

fir_out_a4_a_a1549_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a4_a_a1549 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a7_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a7_a,
	datab => add2b_ff_adffs_a6_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a4_a_a1549);

fir_out_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a4_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a9_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a4_a_a1549)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0B08",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => PA_Reset_acombout,
	datad => fir_out_a4_a_a1549,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a4_a_areg0);

fir_out_a5_a_a1551_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a5_a_a1551 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a8_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a7_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => add2b_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a5_a_a1551);

fir_out_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a5_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a10_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a5_a_a1551)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datab => fir_out_a5_a_a1551,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a5_a_areg0);

fir_out_a6_a_a1553_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a6_a_a1553 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a9_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a8_a,
	datac => add2b_ff_adffs_a9_a,
	datad => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a6_a_a1553);

fir_out_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a6_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & (add2b_ff_adffs_a11_a) # !dselect_reg_ff_adffs_a1_a & fir_out_a6_a_a1553), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a6_a_a1553,
	datab => add2b_ff_adffs_a11_a,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a6_a_areg0);

fir_out_a7_a_a1555_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a7_a_a1555 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a10_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datac => add2b_ff_adffs_a9_a,
	datad => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a7_a_a1555);

fir_out_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a7_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a12_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a7_a_a1555)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a12_a,
	datab => fir_out_a7_a_a1555,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a7_a_areg0);

fir_out_a8_a_a1557_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a8_a_a1557 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a11_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a10_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a0_a,
	datac => add2b_ff_adffs_a10_a,
	datad => add2b_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a8_a_a1557);

fir_out_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a8_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a13_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a8_a_a1557)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a13_a,
	datab => fir_out_a8_a_a1557,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a8_a_areg0);

fir_out_a9_a_a1559_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a9_a_a1559 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a12_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a0_a,
	datac => add2b_ff_adffs_a12_a,
	datad => add2b_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a9_a_a1559);

fir_out_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a9_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a14_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a9_a_a1559)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a14_a,
	datab => fir_out_a9_a_a1559,
	datac => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a9_a_areg0);

fir_out_a10_a_a1561_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a10_a_a1561 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a13_a) # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA50",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a0_a,
	datac => add2b_ff_adffs_a12_a,
	datad => add2b_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a10_a_a1561);

fir_out_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a10_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & (add2b_ff_adffs_a15_a) # !dselect_reg_ff_adffs_a1_a & fir_out_a10_a_a1561), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00E2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a10_a_a1561,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => add2b_ff_adffs_a15_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a10_a_areg0);

fir_out_a11_a_a1563_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a11_a_a1563 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a14_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a14_a,
	datac => dselect_reg_ff_adffs_a0_a,
	datad => add2b_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a11_a_a1563);

fir_out_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a11_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a16_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a11_a_a1563)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00B8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a16_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => fir_out_a11_a_a1563,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a11_a_areg0);

fir_out_a12_a_a1565_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a12_a_a1565 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a15_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a14_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a12_a_a1565);

fir_out_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a12_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a17_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a12_a_a1565)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00B8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => fir_out_a12_a_a1565,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a12_a_areg0);

fir_out_a13_a_a1567_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a13_a_a1567 = dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a16_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a16_a,
	datac => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a13_a_a1567);

fir_out_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a13_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & add2b_ff_adffs_a18_a # !dselect_reg_ff_adffs_a1_a & (fir_out_a13_a_a1567)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00D8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => add2b_ff_adffs_a18_a,
	datac => fir_out_a13_a_a1567,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a13_a_areg0);

fir_out_a14_a_a1569_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a14_a_a1569 = dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a17_a # !dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a16_a,
	datad => dselect_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a14_a_a1569);

fir_out_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a14_a_areg0 = DFFE(!PA_Reset_acombout & (dselect_reg_ff_adffs_a1_a & (add2b_ff_adffs_a19_a) # !dselect_reg_ff_adffs_a1_a & fir_out_a14_a_a1569), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00E2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a14_a_a1569,
	datab => dselect_reg_ff_adffs_a1_a,
	datac => add2b_ff_adffs_a19_a,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a14_a_areg0);

fir_out_a0_a_a1576_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1580 = !dselect_reg_ff_adffs_a1_a & (!PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => dselect_reg_ff_adffs_a1_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1576,
	cascout => fir_out_a0_a_a1580);

fir_out_a15_a_a1578_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a15_a_a1578 = (dselect_reg_ff_adffs_a0_a & (add2b_ff_adffs_a18_a) # !dselect_reg_ff_adffs_a0_a & add2b_ff_adffs_a17_a) & CASCADE(fir_out_a0_a_a1580)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a18_a,
	datad => dselect_reg_ff_adffs_a0_a,
	cascin => fir_out_a0_a_a1580,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a15_a_a1578);

fir_out_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a15_a_areg0 = DFFE(fir_out_a15_a_a1578 # dselect_reg_ff_adffs_a1_a & !PA_Reset_acombout & add2b_ff_adffs_a20_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF20",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => dselect_reg_ff_adffs_a1_a,
	datab => PA_Reset_acombout,
	datac => add2b_ff_adffs_a20_a,
	datad => fir_out_a15_a_a1578,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a15_a_areg0);

Level_Out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Level_Out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_Out);

Level_Cmp_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => tlevel_cmp_acomparator_asub_comptree_agt_cmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_Cmp);

fir_out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(0));

fir_out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(1));

fir_out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(2));

fir_out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(3));

fir_out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(4));

fir_out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(5));

fir_out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(6));

fir_out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(7));

fir_out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(8));

fir_out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(9));

fir_out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(10));

fir_out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(11));

fir_out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(12));

fir_out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(13));

fir_out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(14));

fir_out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(15));
END structure;


