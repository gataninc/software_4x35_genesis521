-- Copyright (C) 1991-2004 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 4.0 Build 190 1/28/2004 SJ Full Version"

-- DATE "02/25/2004 17:07:44"

-- 
-- Device: Altera EP20K60EFC324-1X Package FBGA324
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_dmem IS
    PORT (
	clk : IN std_logic;
	adata : IN std_logic_vector(14 DOWNTO 0);
	imr : IN std_logic;
	Dout : OUT std_logic_vector(134 DOWNTO 0)
	);
END fir_dmem;

ARCHITECTURE structure OF fir_dmem IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_clk : std_logic;
SIGNAL ww_adata : std_logic_vector(14 DOWNTO 0);
SIGNAL ww_imr : std_logic;
SIGNAL ww_Dout : std_logic_vector(134 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY4_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY3_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY2_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1b_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a14_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a14_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a13_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a13_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a12_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a12_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a11_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a11_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a10_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a10_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a9_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a9_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a8_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a8_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a7_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a7_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a6_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a6_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a5_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a5_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a4_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a4_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a3_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a3_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a2_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a2_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a1_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a1_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a0_a_waddr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DLY1a_asram_asegment_a0_a_a0_a_raddr : std_logic_vector(15 DOWNTO 0);
SIGNAL DLY1a_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1a_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY1b_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY2_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY3_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL DLY4_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL adata_a14_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL data0_ff_adffs_a14_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL DLY4_asram_aq_a14_a_a0 : std_logic;
SIGNAL i_a13 : std_logic;
SIGNAL DLY1a_asram_aq_a14_a : std_logic;
SIGNAL DLY1b_asram_aq_a14_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL i_a18 : std_logic;
SIGNAL DLY2_asram_aq_a14_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL i_a23 : std_logic;
SIGNAL i_a15 : std_logic;
SIGNAL i_a28 : std_logic;
SIGNAL i_a20 : std_logic;
SIGNAL i_a63 : std_logic;
SIGNAL DLY3_asram_aq_a14_a : std_logic;
SIGNAL DLY4_asram_aq_a14_a_a1 : std_logic;
SIGNAL i_a33 : std_logic;
SIGNAL DLY4_asram_aq_a14_a : std_logic;
SIGNAL adata_a13_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a13_a : std_logic;
SIGNAL DLY1a_asram_aq_a13_a : std_logic;
SIGNAL DLY1b_asram_aq_a13_a : std_logic;
SIGNAL DLY2_asram_aq_a13_a : std_logic;
SIGNAL DLY3_asram_aq_a13_a : std_logic;
SIGNAL DLY4_asram_aq_a13_a : std_logic;
SIGNAL adata_a12_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a12_a : std_logic;
SIGNAL DLY1a_asram_aq_a12_a : std_logic;
SIGNAL DLY1b_asram_aq_a12_a : std_logic;
SIGNAL DLY2_asram_aq_a12_a : std_logic;
SIGNAL DLY3_asram_aq_a12_a : std_logic;
SIGNAL DLY4_asram_aq_a12_a : std_logic;
SIGNAL adata_a11_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a11_a : std_logic;
SIGNAL DLY1a_asram_aq_a11_a : std_logic;
SIGNAL DLY1b_asram_aq_a11_a : std_logic;
SIGNAL DLY2_asram_aq_a11_a : std_logic;
SIGNAL DLY3_asram_aq_a11_a : std_logic;
SIGNAL DLY4_asram_aq_a11_a : std_logic;
SIGNAL adata_a10_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a10_a : std_logic;
SIGNAL DLY1a_asram_aq_a10_a : std_logic;
SIGNAL DLY1b_asram_aq_a10_a : std_logic;
SIGNAL DLY2_asram_aq_a10_a : std_logic;
SIGNAL DLY3_asram_aq_a10_a : std_logic;
SIGNAL DLY4_asram_aq_a10_a : std_logic;
SIGNAL adata_a9_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a9_a : std_logic;
SIGNAL DLY1a_asram_aq_a9_a : std_logic;
SIGNAL DLY1b_asram_aq_a9_a : std_logic;
SIGNAL DLY2_asram_aq_a9_a : std_logic;
SIGNAL DLY3_asram_aq_a9_a : std_logic;
SIGNAL DLY4_asram_aq_a9_a : std_logic;
SIGNAL adata_a8_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a8_a : std_logic;
SIGNAL DLY1a_asram_aq_a8_a : std_logic;
SIGNAL DLY1b_asram_aq_a8_a : std_logic;
SIGNAL DLY2_asram_aq_a8_a : std_logic;
SIGNAL DLY3_asram_aq_a8_a : std_logic;
SIGNAL DLY4_asram_aq_a8_a : std_logic;
SIGNAL adata_a7_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a7_a : std_logic;
SIGNAL DLY1a_asram_aq_a7_a : std_logic;
SIGNAL DLY1b_asram_aq_a7_a : std_logic;
SIGNAL DLY2_asram_aq_a7_a : std_logic;
SIGNAL DLY3_asram_aq_a7_a : std_logic;
SIGNAL DLY4_asram_aq_a7_a : std_logic;
SIGNAL adata_a6_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a6_a : std_logic;
SIGNAL DLY1a_asram_aq_a6_a : std_logic;
SIGNAL DLY1b_asram_aq_a6_a : std_logic;
SIGNAL DLY2_asram_aq_a6_a : std_logic;
SIGNAL DLY3_asram_aq_a6_a : std_logic;
SIGNAL DLY4_asram_aq_a6_a : std_logic;
SIGNAL adata_a5_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a5_a : std_logic;
SIGNAL DLY1a_asram_aq_a5_a : std_logic;
SIGNAL DLY1b_asram_aq_a5_a : std_logic;
SIGNAL DLY2_asram_aq_a5_a : std_logic;
SIGNAL DLY3_asram_aq_a5_a : std_logic;
SIGNAL DLY4_asram_aq_a5_a : std_logic;
SIGNAL adata_a4_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a4_a : std_logic;
SIGNAL DLY1a_asram_aq_a4_a : std_logic;
SIGNAL DLY1b_asram_aq_a4_a : std_logic;
SIGNAL DLY2_asram_aq_a4_a : std_logic;
SIGNAL DLY3_asram_aq_a4_a : std_logic;
SIGNAL DLY4_asram_aq_a4_a : std_logic;
SIGNAL adata_a3_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a3_a : std_logic;
SIGNAL DLY1a_asram_aq_a3_a : std_logic;
SIGNAL DLY1b_asram_aq_a3_a : std_logic;
SIGNAL DLY2_asram_aq_a3_a : std_logic;
SIGNAL DLY3_asram_aq_a3_a : std_logic;
SIGNAL DLY4_asram_aq_a3_a : std_logic;
SIGNAL adata_a2_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a2_a : std_logic;
SIGNAL DLY1a_asram_aq_a2_a : std_logic;
SIGNAL DLY1b_asram_aq_a2_a : std_logic;
SIGNAL DLY2_asram_aq_a2_a : std_logic;
SIGNAL DLY3_asram_aq_a2_a : std_logic;
SIGNAL DLY4_asram_aq_a2_a : std_logic;
SIGNAL adata_a1_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a1_a : std_logic;
SIGNAL DLY1a_asram_aq_a1_a : std_logic;
SIGNAL DLY1b_asram_aq_a1_a : std_logic;
SIGNAL DLY2_asram_aq_a1_a : std_logic;
SIGNAL DLY3_asram_aq_a1_a : std_logic;
SIGNAL DLY4_asram_aq_a1_a : std_logic;
SIGNAL adata_a0_a_acombout : std_logic;
SIGNAL data0_ff_adffs_a0_a : std_logic;
SIGNAL DLY1a_asram_aq_a0_a : std_logic;
SIGNAL DLY1b_asram_aq_a0_a : std_logic;
SIGNAL DLY2_asram_aq_a0_a : std_logic;
SIGNAL DLY3_asram_aq_a0_a : std_logic;
SIGNAL DLY4_asram_aq_a0_a : std_logic;

BEGIN

ww_clk <= clk;
ww_adata <= adata;
ww_imr <= imr;
Dout <= ww_Dout;

ww_DLY4_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY4_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a33 & DLY4_asram_aq_a14_a_a1 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a6_a & wadr_cntr_awysi_counter_asload_path_a5_a & wadr_cntr_awysi_counter_asload_path_a4_a & 
wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY3_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a63 & i_a28 & i_a23 & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY2_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a18 & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1b_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a14_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a14_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a13_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a13_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a12_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a12_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a11_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a11_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a10_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a10_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a9_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a9_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a8_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a8_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a7_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a7_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a6_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a6_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a5_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a5_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a4_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a4_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a3_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a3_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a2_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a2_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a1_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a1_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a0_a_waddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a0_a);

ww_DLY1a_asram_asegment_a0_a_a0_a_raddr <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & i_a13 & DLY4_asram_aq_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

adata_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(14),
	combout => adata_a14_a_acombout);

clk_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

data0_ff_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a14_a = DFFE(adata_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a14_a);

wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(wadr_cntr_awysi_counter_asload_path_a1_a $ wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(wadr_cntr_awysi_counter_asload_path_a2_a $ !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a2_a & !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

DLY4_asram_aq_a14_a_a0_I : apex20ke_lcell 
-- Equation(s):
-- DLY4_asram_aq_a14_a_a0 = !wadr_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY4_asram_aq_a14_a_a0);

i_a13_I : apex20ke_lcell 
-- Equation(s):
-- i_a13 = wadr_cntr_awysi_counter_asload_path_a2_a $ wadr_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a13);

DLY1a_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY1a_asram_aq_a14_a);

DLY1b_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY1b_asram_aq_a14_a);

wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(wadr_cntr_awysi_counter_asload_path_a3_a $ wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a3_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

i_a18_I : apex20ke_lcell 
-- Equation(s):
-- i_a18 = wadr_cntr_awysi_counter_asload_path_a3_a $ (wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3FC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	datac => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => wadr_cntr_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a18);

DLY2_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY2_asram_aq_a14_a);

wadr_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a4_a = DFFE(wadr_cntr_awysi_counter_asload_path_a4_a $ !wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a4_a & !wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a4_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a5_a = DFFE(wadr_cntr_awysi_counter_asload_path_a5_a $ wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a5_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a6_a = DFFE(wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT $ !wadr_cntr_awysi_counter_asload_path_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a6_a);

i_a23_I : apex20ke_lcell 
-- Equation(s):
-- i_a23 = wadr_cntr_awysi_counter_asload_path_a4_a $ (wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7F80",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a2_a,
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	datac => wadr_cntr_awysi_counter_asload_path_a3_a,
	datad => wadr_cntr_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a23);

i_a15_I : apex20ke_lcell 
-- Equation(s):
-- i_a15 = wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a15);

i_a28_I : apex20ke_lcell 
-- Equation(s):
-- i_a28 = wadr_cntr_awysi_counter_asload_path_a5_a $ (!i_a15 # !wadr_cntr_awysi_counter_asload_path_a3_a # !wadr_cntr_awysi_counter_asload_path_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "870F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a4_a,
	datab => wadr_cntr_awysi_counter_asload_path_a3_a,
	datac => wadr_cntr_awysi_counter_asload_path_a5_a,
	datad => i_a15,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a28);

i_a20_I : apex20ke_lcell 
-- Equation(s):
-- i_a20 = wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & wadr_cntr_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a2_a,
	datac => wadr_cntr_awysi_counter_asload_path_a1_a,
	datad => wadr_cntr_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a20);

i_a63_I : apex20ke_lcell 
-- Equation(s):
-- i_a63 = wadr_cntr_awysi_counter_asload_path_a6_a $ (wadr_cntr_awysi_counter_asload_path_a5_a # wadr_cntr_awysi_counter_asload_path_a4_a & i_a20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1E3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a4_a,
	datab => wadr_cntr_awysi_counter_asload_path_a5_a,
	datac => wadr_cntr_awysi_counter_asload_path_a6_a,
	datad => i_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a63);

DLY3_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY3_asram_aq_a14_a);

DLY4_asram_aq_a14_a_a1_I : apex20ke_lcell 
-- Equation(s):
-- DLY4_asram_aq_a14_a_a1 = !i_a28

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => i_a28,
	devclrn => devclrn,
	devpor => devpor,
	combout => DLY4_asram_aq_a14_a_a1);

i_a33_I : apex20ke_lcell 
-- Equation(s):
-- i_a33 = wadr_cntr_awysi_counter_asload_path_a6_a $ (wadr_cntr_awysi_counter_asload_path_a4_a & wadr_cntr_awysi_counter_asload_path_a5_a & i_a20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "78F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a4_a,
	datab => wadr_cntr_awysi_counter_asload_path_a5_a,
	datac => wadr_cntr_awysi_counter_asload_path_a6_a,
	datad => i_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a33);

DLY4_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a14_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a14_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a14_a_modesel,
	dataout => DLY4_asram_aq_a14_a);

adata_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(13),
	combout => adata_a13_a_acombout);

data0_ff_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a13_a = DFFE(adata_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a13_a);

DLY1a_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY1a_asram_aq_a13_a);

DLY1b_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY1b_asram_aq_a13_a);

DLY2_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY2_asram_aq_a13_a);

DLY3_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY3_asram_aq_a13_a);

DLY4_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a13_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a13_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a13_a_modesel,
	dataout => DLY4_asram_aq_a13_a);

adata_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(12),
	combout => adata_a12_a_acombout);

data0_ff_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a12_a = DFFE(adata_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a12_a);

DLY1a_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY1a_asram_aq_a12_a);

DLY1b_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY1b_asram_aq_a12_a);

DLY2_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY2_asram_aq_a12_a);

DLY3_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY3_asram_aq_a12_a);

DLY4_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a12_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a12_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a12_a_modesel,
	dataout => DLY4_asram_aq_a12_a);

adata_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(11),
	combout => adata_a11_a_acombout);

data0_ff_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a11_a = DFFE(adata_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a11_a);

DLY1a_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY1a_asram_aq_a11_a);

DLY1b_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY1b_asram_aq_a11_a);

DLY2_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY2_asram_aq_a11_a);

DLY3_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY3_asram_aq_a11_a);

DLY4_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a11_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a11_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a11_a_modesel,
	dataout => DLY4_asram_aq_a11_a);

adata_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(10),
	combout => adata_a10_a_acombout);

data0_ff_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a10_a = DFFE(adata_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a10_a);

DLY1a_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY1a_asram_aq_a10_a);

DLY1b_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY1b_asram_aq_a10_a);

DLY2_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY2_asram_aq_a10_a);

DLY3_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY3_asram_aq_a10_a);

DLY4_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a10_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a10_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a10_a_modesel,
	dataout => DLY4_asram_aq_a10_a);

adata_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(9),
	combout => adata_a9_a_acombout);

data0_ff_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a9_a = DFFE(adata_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a9_a);

DLY1a_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY1a_asram_aq_a9_a);

DLY1b_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY1b_asram_aq_a9_a);

DLY2_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY2_asram_aq_a9_a);

DLY3_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY3_asram_aq_a9_a);

DLY4_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a9_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a9_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a9_a_modesel,
	dataout => DLY4_asram_aq_a9_a);

adata_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(8),
	combout => adata_a8_a_acombout);

data0_ff_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a8_a = DFFE(adata_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a8_a);

DLY1a_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY1a_asram_aq_a8_a);

DLY1b_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY1b_asram_aq_a8_a);

DLY2_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY2_asram_aq_a8_a);

DLY3_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY3_asram_aq_a8_a);

DLY4_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a8_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a8_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a8_a_modesel,
	dataout => DLY4_asram_aq_a8_a);

adata_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(7),
	combout => adata_a7_a_acombout);

data0_ff_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a7_a = DFFE(adata_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a7_a);

DLY1a_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY1a_asram_aq_a7_a);

DLY1b_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY1b_asram_aq_a7_a);

DLY2_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY2_asram_aq_a7_a);

DLY3_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY3_asram_aq_a7_a);

DLY4_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a7_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a7_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a7_a_modesel,
	dataout => DLY4_asram_aq_a7_a);

adata_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(6),
	combout => adata_a6_a_acombout);

data0_ff_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a6_a = DFFE(adata_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a6_a);

DLY1a_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY1a_asram_aq_a6_a);

DLY1b_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY1b_asram_aq_a6_a);

DLY2_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY2_asram_aq_a6_a);

DLY3_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY3_asram_aq_a6_a);

DLY4_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a6_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a6_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a6_a_modesel,
	dataout => DLY4_asram_aq_a6_a);

adata_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(5),
	combout => adata_a5_a_acombout);

data0_ff_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a5_a = DFFE(adata_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a5_a);

DLY1a_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY1a_asram_aq_a5_a);

DLY1b_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY1b_asram_aq_a5_a);

DLY2_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY2_asram_aq_a5_a);

DLY3_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY3_asram_aq_a5_a);

DLY4_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a5_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a5_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a5_a_modesel,
	dataout => DLY4_asram_aq_a5_a);

adata_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(4),
	combout => adata_a4_a_acombout);

data0_ff_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a4_a = DFFE(adata_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a4_a);

DLY1a_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY1a_asram_aq_a4_a);

DLY1b_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY1b_asram_aq_a4_a);

DLY2_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY2_asram_aq_a4_a);

DLY3_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY3_asram_aq_a4_a);

DLY4_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a4_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a4_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a4_a_modesel,
	dataout => DLY4_asram_aq_a4_a);

adata_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(3),
	combout => adata_a3_a_acombout);

data0_ff_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a3_a = DFFE(adata_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a3_a);

DLY1a_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY1a_asram_aq_a3_a);

DLY1b_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY1b_asram_aq_a3_a);

DLY2_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY2_asram_aq_a3_a);

DLY3_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY3_asram_aq_a3_a);

DLY4_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a3_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a3_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a3_a_modesel,
	dataout => DLY4_asram_aq_a3_a);

adata_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(2),
	combout => adata_a2_a_acombout);

data0_ff_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a2_a = DFFE(adata_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a2_a);

DLY1a_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY1a_asram_aq_a2_a);

DLY1b_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY1b_asram_aq_a2_a);

DLY2_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY2_asram_aq_a2_a);

DLY3_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY3_asram_aq_a2_a);

DLY4_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a2_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a2_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a2_a_modesel,
	dataout => DLY4_asram_aq_a2_a);

adata_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(1),
	combout => adata_a1_a_acombout);

data0_ff_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a1_a = DFFE(adata_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a1_a);

DLY1a_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY1a_asram_aq_a1_a);

DLY1b_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY1b_asram_aq_a1_a);

DLY2_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY2_asram_aq_a1_a);

DLY3_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY3_asram_aq_a1_a);

DLY4_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a1_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a1_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a1_a_modesel,
	dataout => DLY4_asram_aq_a1_a);

adata_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_adata(0),
	combout => adata_a0_a_acombout);

data0_ff_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- data0_ff_adffs_a0_a = DFFE(adata_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => adata_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => data0_ff_adffs_a0_a);

DLY1a_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1a|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1a_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY1a_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1a_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY1a_asram_aq_a0_a);

DLY1b_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY1b|altdpram:sram|content",
	init_file => "INIT1.MIF",
	logical_ram_depth => 8,
	logical_ram_width => 15,
	address_width => 3,
	first_address => 0,
	last_address => 7,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY1b_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY1b_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY1b_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY1b_asram_aq_a0_a);

DLY2_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY2|altdpram:sram|content",
	init_file => "INIT2.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY2_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY2_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY2_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY2_asram_aq_a0_a);

DLY3_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY3|altdpram:sram|content",
	init_file => "INIT3.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY3_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY3_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY3_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY3_asram_aq_a0_a);

DLY4_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:DLY4|altdpram:sram|content",
	init_file => "INIT4.MIF",
	logical_ram_depth => 128,
	logical_ram_width => 15,
	address_width => 7,
	first_address => 0,
	last_address => 127,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a0_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => ww_DLY4_asram_asegment_a0_a_a0_a_waddr,
	raddr => ww_DLY4_asram_asegment_a0_a_a0_a_raddr,
	devclrn => devclrn,
	devpor => devpor,
	modesel => DLY4_asram_asegment_a0_a_a0_a_modesel,
	dataout => DLY4_asram_aq_a0_a);

Dout_a134_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(134));

Dout_a133_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(133));

Dout_a132_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(132));

Dout_a131_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(131));

Dout_a130_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(130));

Dout_a129_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(129));

Dout_a128_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(128));

Dout_a127_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(127));

Dout_a126_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(126));

Dout_a125_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(125));

Dout_a124_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(124));

Dout_a123_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(123));

Dout_a122_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(122));

Dout_a121_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(121));

Dout_a120_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY4_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(120));

Dout_a119_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(119));

Dout_a118_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(118));

Dout_a117_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(117));

Dout_a116_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(116));

Dout_a115_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(115));

Dout_a114_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(114));

Dout_a113_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(113));

Dout_a112_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(112));

Dout_a111_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(111));

Dout_a110_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(110));

Dout_a109_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(109));

Dout_a108_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(108));

Dout_a107_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(107));

Dout_a106_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(106));

Dout_a105_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY3_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(105));

Dout_a104_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(104));

Dout_a103_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(103));

Dout_a102_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(102));

Dout_a101_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(101));

Dout_a100_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(100));

Dout_a99_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(99));

Dout_a98_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(98));

Dout_a97_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(97));

Dout_a96_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(96));

Dout_a95_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(95));

Dout_a94_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(94));

Dout_a93_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(93));

Dout_a92_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(92));

Dout_a91_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(91));

Dout_a90_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(90));

Dout_a89_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(89));

Dout_a88_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(88));

Dout_a87_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(87));

Dout_a86_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(86));

Dout_a85_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(85));

Dout_a84_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(84));

Dout_a83_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(83));

Dout_a82_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(82));

Dout_a81_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(81));

Dout_a80_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(80));

Dout_a79_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(79));

Dout_a78_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(78));

Dout_a77_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(77));

Dout_a76_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(76));

Dout_a75_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY2_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(75));

Dout_a74_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(74));

Dout_a73_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(73));

Dout_a72_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(72));

Dout_a71_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(71));

Dout_a70_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(70));

Dout_a69_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(69));

Dout_a68_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(68));

Dout_a67_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(67));

Dout_a66_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(66));

Dout_a65_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(65));

Dout_a64_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(64));

Dout_a63_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(63));

Dout_a62_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(62));

Dout_a61_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(61));

Dout_a60_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(60));

Dout_a59_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(59));

Dout_a58_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(58));

Dout_a57_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(57));

Dout_a56_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(56));

Dout_a55_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(55));

Dout_a54_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(54));

Dout_a53_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(53));

Dout_a52_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(52));

Dout_a51_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(51));

Dout_a50_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(50));

Dout_a49_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(49));

Dout_a48_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(48));

Dout_a47_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(47));

Dout_a46_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(46));

Dout_a45_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(45));

Dout_a44_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(44));

Dout_a43_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(43));

Dout_a42_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(42));

Dout_a41_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(41));

Dout_a40_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(40));

Dout_a39_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(39));

Dout_a38_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(38));

Dout_a37_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(37));

Dout_a36_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(36));

Dout_a35_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(35));

Dout_a34_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(34));

Dout_a33_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(33));

Dout_a32_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(32));

Dout_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(31));

Dout_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1b_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(30));

Dout_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(29));

Dout_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(28));

Dout_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(27));

Dout_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(26));

Dout_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(25));

Dout_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(24));

Dout_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(23));

Dout_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(22));

Dout_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(21));

Dout_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(20));

Dout_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(19));

Dout_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(18));

Dout_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(17));

Dout_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(16));

Dout_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DLY1a_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(15));

Dout_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(14));

Dout_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(13));

Dout_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(12));

Dout_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(11));

Dout_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(10));

Dout_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(9));

Dout_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(8));

Dout_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(7));

Dout_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(6));

Dout_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(5));

Dout_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(4));

Dout_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(3));

Dout_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(2));

Dout_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(1));

Dout_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => data0_ff_adffs_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Dout(0));
END structure;


