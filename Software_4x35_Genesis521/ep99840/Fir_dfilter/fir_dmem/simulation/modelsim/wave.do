onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
add wave -noupdate -format Literal -radix hexadecimal /tb/dout
add wave -noupdate -format Literal -radix hexadecimal /tb/disc1
add wave -noupdate -format Literal -radix hexadecimal /tb/disc2
add wave -noupdate -format Literal -radix hexadecimal /tb/disc3
add wave -noupdate -format Literal -radix hexadecimal /tb/disc4
add wave -noupdate -format Literal -radix hexadecimal /tb/disc5
add wave -noupdate -format Literal -radix hexadecimal /tb/disc6
add wave -noupdate -format Literal -radix hexadecimal /tb/disc7
add wave -noupdate -format Literal -radix hexadecimal /tb/disc8
add wave -noupdate -format Literal -radix hexadecimal /tb/disc9
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {15180 ns}
WaveRestoreZoom {1999 ns} {22345 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 78
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
