	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr 	: std_logic := '1';
	signal clk	: std_logic := '0';
	signal adata	: std_logic_Vector( 15 downto 0 ) := x"0000";
	signal dout	: std_logic_Vector( 134 downto 0 );
	
	signal disc1	: std_logic_vector( 14 downto 0 );
	signal disc2	: std_logic_vector( 14 downto 0 );
	signal disc3	: std_logic_vector( 14 downto 0 );
	signal disc4	: std_logic_vector( 14 downto 0 );
	signal disc5	: std_logic_vector( 14 downto 0 );
	signal disc6	: std_logic_vector( 14 downto 0 );
	signal disc7	: std_logic_vector( 14 downto 0 );
	signal disc8	: std_logic_vector( 14 downto 0 );
	signal disc9	: std_logic_vector( 14 downto 0 );

	-------------------------------------------------------------------------------
	component fir_dmem is 
		port( 
			clk			: in		std_logic;
	     	imr            : in      std_logic;                    -- Master Reset
			adata		: in		std_logic_Vector( 14 downto 0 );
			Dout			: out	std_logic_Vector( 134 downto 0 ) );
	end component fir_dmem;
	-------------------------------------------------------------------------------

begin
	imr	<= '0' after 555 ns;
	clk	<= not( clk ) after 25 ns;
	adata	<= x"1234" after 1 us;

	U : fir_dmem
		port map(
			imr		=> imr,
			clk		=> clk,
			adata	=> adata( 14 downto 0 ),
			dout		=> dout );
				
	clk_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			disc1	<= conv_std_logic_vector( 0, 15 );
			disc2	<= conv_std_logic_vector( 0, 15 );
			disc3	<= conv_std_logic_vector( 0, 15 );
			disc4	<= conv_std_logic_vector( 0, 15 );
			disc5	<= conv_std_logic_vector( 0, 15 );
			disc6	<= conv_std_logic_vector( 0, 15 );
			disc7	<= conv_std_logic_vector( 0, 15 );
			disc8	<= conv_std_logic_vector( 0, 15 );
			disc9	<= conv_std_logic_vector( 0, 15 );
		elsif(( clk'event ) and ( clk = '1' )) then
			disc1	<= dout(   14 downto   0 );
			disc2	<= dout(   29 downto  15 );
			disc3	<= dout(   44 downto  30 );
			disc4	<= dout(   59 downto  45 );
			disc5	<= dout(   74 downto  60 );
			disc6	<= dout(   89 downto  75 );
			disc7	<= dout(  104 downto  90 );
			disc8	<= dout(  119 downto 105 );
			disc9	<= dout(  134 downto 120 );
		end if;
	end process;

-------------------------------------------------------------------------------
end test_bench;               -- fir_dmem
-------------------------------------------------------------------------------