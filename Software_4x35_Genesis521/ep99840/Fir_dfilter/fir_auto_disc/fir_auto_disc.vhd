-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_auto_disc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module monitors the filtered data ( High, Medium, Low, BLM )
--		and attempts to identify the threshold level
--		by looking for the most negative peaks, and applying the abs function
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_auto_disc is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		ADisc_En		: in		std_logic;
		Decay		: in		std_logic_Vector( 15 downto 0 );
		tlevel		: in 	std_logic_Vector( 15 downto 0 );	-- User Threshold Lavel
		fir_out		: in 	std_logic_Vector( 15 downto 0 );	-- FIR Output 
		atlevel		: buffer	std_logic_vector( 15 downto 0 ) );	-- AUto Threshold Level
end fir_auto_disc;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_auto_disc is

	signal decay_vec	: std_logic_vector( 31 downto 0 );
	signal decay_sum	: std_logic_vector( 31 downto 0 );

	signal fir_out_vec	: std_logic_vector( 31 downto 0 );
	signal fir_min		: std_logic_vector( 31 downto 0 );
	signal fir_min_mux	: std_logic_vector( 31 downto 0 );
	signal fir_min_cmp 	: std_logic;
	signal abs_min		: std_logic_vector( 15 downto 0 );
	signal vcc		: std_logic;
	signal Gnd		: std_logic;
	signal ADisc_Dis	: std_logic;
	signal atlevel_add	: std_logic_Vector( 15 downto 0 );
begin
	vcc	<= '1';
	Gnd	<= '0';
	---------------------------------------------------------------------------
	-- Minimum Determination
	fir_out_Vec_ff : lpm_ff
		generic map(
			lpm_WIDTH			=> 16,
			lpm_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_out,
			q				=> fir_out_vec( 31 downto 16 ) );
	fir_out_vec( 15 downto 0 ) <= x"0000";
--	fir_out_vec 	<= fir_out & x"0000";

	fir_min_compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 32, 
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> fir_out_vec,
			datab			=> fir_min,
			alb				=> fir_min_cmp );
	-- Minimum Determination
	---------------------------------------------------------------------------
	
	
	---------------------------------------------------------------------------
	-- Minimum Detected value 
	fir_min_mux	<= fir_out_vec when ( fir_min_cmp = '1' ) else decay_sum;
	
	ADisc_Dis <= not( ADisc_En );
	
	fir_min_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 32,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> ADisc_Dis,
			data				=> fir_min_mux,
			q				=> fir_min );
	-- Minimum Detected value 
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- add a fractional number to the minimum to decay down to 0
	decay_vec	<= x"0000" & decay;
	
	decay_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',					-- No Carry In
			dataa			=> fir_min,
			datab			=> decay_vec,
			result			=> decay_sum );
	-- add a fractional number to the minimum to decay down to 0
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Absolute Value of the Minimum
	fir_min_abs : lpm_abs
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_ABS" )
		port map(
			data				=> fir_min( 31 downto 16 ),
			result			=> abs_min );			
	-- Absolute Value of the Minimum
	---------------------------------------------------------------------------

	---------------------------------------------------------------------------
	-- Add the "tlevel" to the abs(min)
	atlevel_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',				-- No Carry In
			dataa			=> abs_min,
			datab			=> tlevel,
			result			=> atlevel_add );
			
	atlevel_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> atlevel_add,
			q				=> atlevel );
			
	-- Add the "tlevel" to the abs(min)
	---------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- fir_auto_disc
------------------------------------------------------------------------------------

