-- Copyright (C) 1991-2002 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 2.1 Build 189 09/05/2002 Service Pack 1 SJ Full Version"

-- DATE "07/30/2003 11:05:41"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for MODELSIM-ALTERA (VHDL OUTPUT FROM QUARTUS II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_adisc IS
    PORT (
	clk : IN std_logic;
	imr : IN std_logic;
	tlevel : IN std_logic_vector(15 DOWNTO 0);
	fir_out : IN std_logic_vector(15 DOWNTO 0);
	Decay : IN std_logic_vector(15 DOWNTO 0);
	atlevel : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_adisc;

ARCHITECTURE structure OF fir_adisc IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_clk : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Decay : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_atlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL tlevel_a15_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a15_a : std_logic;
SIGNAL fir_out_a15_a_acombout : std_logic;
SIGNAL fir_out_a14_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a30_a_a181 : std_logic;
SIGNAL fir_out_a13_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a29_a_a179 : std_logic;
SIGNAL fir_out_a12_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a28_a_a177 : std_logic;
SIGNAL fir_out_a11_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a27_a_a175 : std_logic;
SIGNAL fir_out_a10_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a26_a_a173 : std_logic;
SIGNAL fir_out_a7_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a23_a_a195 : std_logic;
SIGNAL fir_out_a6_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a22_a_a193 : std_logic;
SIGNAL fir_out_a5_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a21_a_a191 : std_logic;
SIGNAL fir_out_a4_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a20_a_a189 : std_logic;
SIGNAL fir_out_a3_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a19_a_a187 : std_logic;
SIGNAL fir_out_a2_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a18_a_a185 : std_logic;
SIGNAL fir_out_a1_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a17_a_a183 : std_logic;
SIGNAL fir_out_a0_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a16_a_a167 : std_logic;
SIGNAL Decay_a15_a_acombout : std_logic;
SIGNAL Decay_a14_a_acombout : std_logic;
SIGNAL Decay_a13_a_acombout : std_logic;
SIGNAL Decay_a12_a_acombout : std_logic;
SIGNAL Decay_a11_a_acombout : std_logic;
SIGNAL Decay_a10_a_acombout : std_logic;
SIGNAL Decay_a9_a_acombout : std_logic;
SIGNAL Decay_a8_a_acombout : std_logic;
SIGNAL Decay_a7_a_acombout : std_logic;
SIGNAL Decay_a6_a_acombout : std_logic;
SIGNAL Decay_a5_a_acombout : std_logic;
SIGNAL Decay_a4_a_acombout : std_logic;
SIGNAL Decay_a3_a_acombout : std_logic;
SIGNAL Decay_a2_a_acombout : std_logic;
SIGNAL Decay_a1_a_acombout : std_logic;
SIGNAL Decay_a0_a_acombout : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL fir_min_ff_adffs_a0_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL fir_min_ff_adffs_a1_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL fir_min_ff_adffs_a2_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL fir_min_ff_adffs_a3_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL fir_min_ff_adffs_a4_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL fir_min_ff_adffs_a5_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL fir_min_ff_adffs_a6_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL fir_min_ff_adffs_a7_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL fir_min_ff_adffs_a8_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a9_a : std_logic;
SIGNAL fir_min_ff_adffs_a9_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a9_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a10_a : std_logic;
SIGNAL fir_min_ff_adffs_a10_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a10_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a11_a : std_logic;
SIGNAL fir_min_ff_adffs_a11_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a11_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a12_a : std_logic;
SIGNAL fir_min_ff_adffs_a12_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a12_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a13_a : std_logic;
SIGNAL fir_min_ff_adffs_a13_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a13_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a14_a : std_logic;
SIGNAL fir_min_ff_adffs_a14_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a14_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a15_a : std_logic;
SIGNAL fir_min_ff_adffs_a15_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a15_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a16_a : std_logic;
SIGNAL fir_min_ff_adffs_a16_a_a168 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a17_a : std_logic;
SIGNAL fir_min_ff_adffs_a17_a_a184 : std_logic;
SIGNAL fir_min_mux_a17_a_a23 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a18_a : std_logic;
SIGNAL fir_min_ff_adffs_a18_a_a186 : std_logic;
SIGNAL fir_min_mux_a18_a_a20 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a19_a : std_logic;
SIGNAL fir_min_ff_adffs_a19_a_a188 : std_logic;
SIGNAL fir_min_mux_a19_a_a17 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a20_a : std_logic;
SIGNAL fir_min_ff_adffs_a20_a_a190 : std_logic;
SIGNAL fir_min_mux_a20_a_a14 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a20_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a21_a : std_logic;
SIGNAL fir_min_ff_adffs_a21_a_a192 : std_logic;
SIGNAL fir_min_mux_a21_a_a11 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a21_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a22_a : std_logic;
SIGNAL fir_min_ff_adffs_a22_a_a194 : std_logic;
SIGNAL fir_min_mux_a22_a_a8 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a22_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a23_a : std_logic;
SIGNAL fir_min_ff_adffs_a23_a_a196 : std_logic;
SIGNAL fir_min_mux_a23_a_a5 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a23_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a24_a : std_logic;
SIGNAL fir_min_ff_adffs_a24_a_a170 : std_logic;
SIGNAL fir_out_a8_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a24_a_a169 : std_logic;
SIGNAL fir_min_mux_a24_a_a44 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a24_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a25_a : std_logic;
SIGNAL fir_min_ff_adffs_a25_a_a172 : std_logic;
SIGNAL fir_out_a9_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a25_a_a171 : std_logic;
SIGNAL fir_min_mux_a25_a_a41 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a25_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a26_a : std_logic;
SIGNAL fir_min_ff_adffs_a26_a_a174 : std_logic;
SIGNAL fir_min_mux_a26_a_a38 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a26_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a27_a : std_logic;
SIGNAL fir_min_ff_adffs_a27_a_a176 : std_logic;
SIGNAL fir_min_mux_a27_a_a35 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a27_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a28_a : std_logic;
SIGNAL fir_min_ff_adffs_a28_a_a178 : std_logic;
SIGNAL fir_min_mux_a28_a_a32 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a28_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a29_a : std_logic;
SIGNAL fir_min_ff_adffs_a29_a_a180 : std_logic;
SIGNAL fir_min_mux_a29_a_a29 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a29_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a30_a : std_logic;
SIGNAL fir_min_ff_adffs_a30_a_a182 : std_logic;
SIGNAL fir_min_mux_a30_a_a26 : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a_a14 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL fir_min_ff_adffs_a31_a_a197 : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a30_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a31_a : std_logic;
SIGNAL fir_min_ff_adffs_a31_a_a198 : std_logic;
SIGNAL fir_min_mux_a31_a_a2 : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a_a63 : std_logic;
SIGNAL fir_min_abs_alcarry_a1_a : std_logic;
SIGNAL fir_min_abs_alcarry_a1_a_a39 : std_logic;
SIGNAL fir_min_abs_alcarry_a2_a : std_logic;
SIGNAL fir_min_abs_alcarry_a2_a_a35 : std_logic;
SIGNAL fir_min_abs_alcarry_a3_a : std_logic;
SIGNAL fir_min_abs_alcarry_a3_a_a31 : std_logic;
SIGNAL fir_min_abs_alcarry_a4_a : std_logic;
SIGNAL fir_min_abs_alcarry_a4_a_a27 : std_logic;
SIGNAL fir_min_abs_alcarry_a5_a : std_logic;
SIGNAL fir_min_abs_alcarry_a5_a_a23 : std_logic;
SIGNAL fir_min_abs_alcarry_a6_a : std_logic;
SIGNAL fir_min_abs_alcarry_a6_a_a19 : std_logic;
SIGNAL fir_min_abs_alcarry_a7_a : std_logic;
SIGNAL fir_min_abs_alcarry_a7_a_a15 : std_logic;
SIGNAL fir_min_abs_alcarry_a8_a : std_logic;
SIGNAL fir_min_abs_alcarry_a8_a_a59 : std_logic;
SIGNAL fir_min_abs_alcarry_a9_a : std_logic;
SIGNAL fir_min_abs_alcarry_a9_a_a55 : std_logic;
SIGNAL fir_min_abs_alcarry_a10_a : std_logic;
SIGNAL fir_min_abs_alcarry_a10_a_a51 : std_logic;
SIGNAL fir_min_abs_alcarry_a11_a : std_logic;
SIGNAL fir_min_abs_alcarry_a11_a_a47 : std_logic;
SIGNAL fir_min_abs_alcarry_a12_a : std_logic;
SIGNAL fir_min_abs_alcarry_a12_a_a43 : std_logic;
SIGNAL fir_min_abs_a2_a12 : std_logic;
SIGNAL fir_min_abs_a2_a13 : std_logic;
SIGNAL fir_min_abs_a_a00001 : std_logic;
SIGNAL tlevel_a14_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a14_a : std_logic;
SIGNAL fir_min_abs_a7_a13 : std_logic;
SIGNAL fir_min_abs_a6_a13 : std_logic;
SIGNAL fir_min_abs_alcarry_a12_a_a44 : std_logic;
SIGNAL fir_min_abs_a6_a12 : std_logic;
SIGNAL tlevel_a13_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a13_a : std_logic;
SIGNAL fir_min_abs_alcarry_a11_a_a48 : std_logic;
SIGNAL fir_min_abs_a6_a11 : std_logic;
SIGNAL tlevel_a12_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a12_a : std_logic;
SIGNAL tlevel_a11_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a11_a : std_logic;
SIGNAL fir_min_abs_alcarry_a10_a_a52 : std_logic;
SIGNAL fir_min_abs_a6_a10 : std_logic;
SIGNAL tlevel_a10_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a10_a : std_logic;
SIGNAL fir_min_abs_alcarry_a9_a_a56 : std_logic;
SIGNAL fir_min_abs_a6_a9 : std_logic;
SIGNAL fir_min_abs_alcarry_a8_a_a60 : std_logic;
SIGNAL fir_min_abs_a6_a8 : std_logic;
SIGNAL tlevel_a9_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a9_a : std_logic;
SIGNAL tlevel_a8_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a8_a : std_logic;
SIGNAL fir_min_abs_alcarry_a7_a_a16 : std_logic;
SIGNAL fir_min_abs_a6_a7 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a4 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a67 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a58 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a49 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a40 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a31 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_a12_a22 : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL tlevel_a7_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a7_a : std_logic;
SIGNAL fir_min_abs_alcarry_a6_a_a20 : std_logic;
SIGNAL fir_min_abs_a6_a6 : std_logic;
SIGNAL tlevel_a6_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a6_a : std_logic;
SIGNAL fir_min_abs_alcarry_a5_a_a24 : std_logic;
SIGNAL fir_min_abs_a6_a5 : std_logic;
SIGNAL tlevel_a5_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a5_a : std_logic;
SIGNAL fir_min_abs_alcarry_a4_a_a28 : std_logic;
SIGNAL fir_min_abs_a6_a4 : std_logic;
SIGNAL tlevel_a4_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a4_a : std_logic;
SIGNAL fir_min_abs_alcarry_a3_a_a32 : std_logic;
SIGNAL fir_min_abs_a6_a3 : std_logic;
SIGNAL tlevel_a3_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a3_a : std_logic;
SIGNAL fir_min_abs_alcarry_a2_a_a36 : std_logic;
SIGNAL fir_min_abs_a6_a2 : std_logic;
SIGNAL tlevel_a2_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a2_a : std_logic;
SIGNAL fir_min_abs_alcarry_a1_a_a40 : std_logic;
SIGNAL fir_min_abs_a6_a1 : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a_a64 : std_logic;
SIGNAL fir_min_abs_a6_a0 : std_logic;
SIGNAL tlevel_a1_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a1_a : std_logic;
SIGNAL fir_min_abs_a_a00000 : std_logic;
SIGNAL tlevel_a0_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL NOT_fir_min_compare_acomparator_acmp_end_aagb_out : std_logic;

BEGIN

ww_clk <= clk;
ww_imr <= imr;
ww_tlevel <= tlevel;
ww_fir_out <= fir_out;
ww_Decay <= Decay;
atlevel <= ww_atlevel;
NOT_fir_min_compare_acomparator_acmp_end_aagb_out <= NOT fir_min_compare_acomparator_acmp_end_aagb_out;

tlevel_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(15),
	combout => tlevel_a15_a_acombout);

atlevel_add_sub_adatab_node_a15_a_a52 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a15_a = tlevel_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a15_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a15_a);

fir_out_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(15),
	combout => fir_out_a15_a_acombout);

fir_out_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(14),
	combout => fir_out_a14_a_acombout);

clk_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

fir_min_ff_adffs_a30_a_a181_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a30_a_a181 = DFFE(fir_out_a14_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a14_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a30_a_a181);

fir_out_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(13),
	combout => fir_out_a13_a_acombout);

fir_min_ff_adffs_a29_a_a179_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a29_a_a179 = DFFE(fir_out_a13_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a13_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a29_a_a179);

fir_out_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(12),
	combout => fir_out_a12_a_acombout);

fir_min_ff_adffs_a28_a_a177_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a28_a_a177 = DFFE(fir_out_a12_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a12_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a28_a_a177);

fir_out_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(11),
	combout => fir_out_a11_a_acombout);

fir_min_ff_adffs_a27_a_a175_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a27_a_a175 = DFFE(fir_out_a11_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a11_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a27_a_a175);

fir_out_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(10),
	combout => fir_out_a10_a_acombout);

fir_min_ff_adffs_a26_a_a173_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a26_a_a173 = DFFE(fir_out_a10_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a10_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a26_a_a173);

fir_out_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(7),
	combout => fir_out_a7_a_acombout);

fir_min_ff_adffs_a23_a_a195_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a23_a_a195 = DFFE(fir_out_a7_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a7_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a23_a_a195);

fir_out_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(6),
	combout => fir_out_a6_a_acombout);

fir_min_ff_adffs_a22_a_a193_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a22_a_a193 = DFFE(fir_out_a6_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a6_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a22_a_a193);

fir_out_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(5),
	combout => fir_out_a5_a_acombout);

fir_min_ff_adffs_a21_a_a191_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a21_a_a191 = DFFE(fir_out_a5_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a5_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a21_a_a191);

fir_out_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(4),
	combout => fir_out_a4_a_acombout);

fir_min_ff_adffs_a20_a_a189_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a20_a_a189 = DFFE(fir_out_a4_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a4_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a20_a_a189);

fir_out_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(3),
	combout => fir_out_a3_a_acombout);

fir_min_ff_adffs_a19_a_a187_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a19_a_a187 = DFFE(fir_out_a3_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a3_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a19_a_a187);

fir_out_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(2),
	combout => fir_out_a2_a_acombout);

fir_min_ff_adffs_a18_a_a185_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a18_a_a185 = DFFE(fir_out_a2_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a2_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a18_a_a185);

fir_out_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(1),
	combout => fir_out_a1_a_acombout);

fir_min_ff_adffs_a17_a_a183_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a17_a_a183 = DFFE(fir_out_a1_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a1_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a17_a_a183);

fir_out_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(0),
	combout => fir_out_a0_a_acombout);

fir_min_ff_adffs_a16_a_a167_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a16_a_a167 = DFFE(fir_out_a0_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a0_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a16_a_a167);

Decay_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(15),
	combout => Decay_a15_a_acombout);

Decay_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(14),
	combout => Decay_a14_a_acombout);

Decay_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(13),
	combout => Decay_a13_a_acombout);

Decay_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(12),
	combout => Decay_a12_a_acombout);

Decay_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(11),
	combout => Decay_a11_a_acombout);

Decay_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(10),
	combout => Decay_a10_a_acombout);

Decay_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(9),
	combout => Decay_a9_a_acombout);

Decay_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(8),
	combout => Decay_a8_a_acombout);

Decay_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(7),
	combout => Decay_a7_a_acombout);

Decay_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(6),
	combout => Decay_a6_a_acombout);

Decay_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(5),
	combout => Decay_a5_a_acombout);

Decay_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(4),
	combout => Decay_a4_a_acombout);

Decay_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(3),
	combout => Decay_a3_a_acombout);

Decay_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(2),
	combout => Decay_a2_a_acombout);

Decay_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(1),
	combout => Decay_a1_a_acombout);

Decay_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(0),
	combout => Decay_a0_a_acombout);

decay_add_sub_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a0_a = fir_min_ff_adffs_a0_a $ Decay_a0_a_acombout
-- decay_add_sub_aadder_aresult_node_acout_a0_a = CARRY(fir_min_ff_adffs_a0_a & Decay_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a0_a,
	datab => Decay_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a0_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a0_a);

fir_min_ff_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a0_a = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- fir_min_compare_acomparator_acmp_end_alcarry_a0_a = CARRY(fir_min_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "CCF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => NOT_fir_min_compare_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a0_a,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a0_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a1_a = Decay_a1_a_acombout $ fir_min_ff_adffs_a1_a $ decay_add_sub_aadder_aresult_node_acout_a0_a
-- decay_add_sub_aadder_aresult_node_acout_a1_a = CARRY(Decay_a1_a_acombout & !fir_min_ff_adffs_a1_a & !decay_add_sub_aadder_aresult_node_acout_a0_a # !Decay_a1_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a0_a # !fir_min_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a1_a_acombout,
	datab => fir_min_ff_adffs_a1_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a1_a);

fir_min_ff_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a1_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a1_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a1_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a2_a = fir_min_ff_adffs_a2_a $ Decay_a2_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a1_a
-- decay_add_sub_aadder_aresult_node_acout_a2_a = CARRY(fir_min_ff_adffs_a2_a & (Decay_a2_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a1_a) # !fir_min_ff_adffs_a2_a & Decay_a2_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a2_a,
	datab => Decay_a2_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a2_a);

fir_min_ff_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a2_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a2_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a2_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a3_a = Decay_a3_a_acombout $ fir_min_ff_adffs_a3_a $ decay_add_sub_aadder_aresult_node_acout_a2_a
-- decay_add_sub_aadder_aresult_node_acout_a3_a = CARRY(Decay_a3_a_acombout & !fir_min_ff_adffs_a3_a & !decay_add_sub_aadder_aresult_node_acout_a2_a # !Decay_a3_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a2_a # !fir_min_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a3_a_acombout,
	datab => fir_min_ff_adffs_a3_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a3_a);

fir_min_ff_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a3_a = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_compare_acomparator_acmp_end_aagb_out,
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a3_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a4_a = fir_min_ff_adffs_a4_a $ Decay_a4_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a3_a
-- decay_add_sub_aadder_aresult_node_acout_a4_a = CARRY(fir_min_ff_adffs_a4_a & (Decay_a4_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a3_a) # !fir_min_ff_adffs_a4_a & Decay_a4_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a4_a,
	datab => Decay_a4_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a4_a);

fir_min_ff_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a4_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a4_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a4_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a5_a = fir_min_ff_adffs_a5_a $ Decay_a5_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a4_a
-- decay_add_sub_aadder_aresult_node_acout_a5_a = CARRY(fir_min_ff_adffs_a5_a & !Decay_a5_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a4_a # !fir_min_ff_adffs_a5_a & (!decay_add_sub_aadder_aresult_node_acout_a4_a # !Decay_a5_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a5_a,
	datab => Decay_a5_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a5_a);

fir_min_ff_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a5_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a5_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a5_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a6_a = fir_min_ff_adffs_a6_a $ Decay_a6_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a5_a
-- decay_add_sub_aadder_aresult_node_acout_a6_a = CARRY(fir_min_ff_adffs_a6_a & (Decay_a6_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a5_a) # !fir_min_ff_adffs_a6_a & Decay_a6_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a6_a,
	datab => Decay_a6_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a6_a);

fir_min_ff_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a6_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a6_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a6_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a7_a = Decay_a7_a_acombout $ fir_min_ff_adffs_a7_a $ decay_add_sub_aadder_aresult_node_acout_a6_a
-- decay_add_sub_aadder_aresult_node_acout_a7_a = CARRY(Decay_a7_a_acombout & !fir_min_ff_adffs_a7_a & !decay_add_sub_aadder_aresult_node_acout_a6_a # !Decay_a7_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a6_a # !fir_min_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a7_a_acombout,
	datab => fir_min_ff_adffs_a7_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a7_a);

fir_min_ff_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a7_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a7_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a7_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a8_a = Decay_a8_a_acombout $ fir_min_ff_adffs_a8_a $ !decay_add_sub_aadder_aresult_node_acout_a7_a
-- decay_add_sub_aadder_aresult_node_acout_a8_a = CARRY(Decay_a8_a_acombout & (fir_min_ff_adffs_a8_a # !decay_add_sub_aadder_aresult_node_acout_a7_a) # !Decay_a8_a_acombout & fir_min_ff_adffs_a8_a & !decay_add_sub_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a8_a_acombout,
	datab => fir_min_ff_adffs_a8_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a8_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a8_a);

fir_min_ff_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a8_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a8_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a8_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a8_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a9_a = fir_min_ff_adffs_a9_a $ Decay_a9_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a8_a
-- decay_add_sub_aadder_aresult_node_acout_a9_a = CARRY(fir_min_ff_adffs_a9_a & !Decay_a9_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a8_a # !fir_min_ff_adffs_a9_a & (!decay_add_sub_aadder_aresult_node_acout_a8_a # !Decay_a9_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a9_a,
	datab => Decay_a9_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a9_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a9_a);

fir_min_ff_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a9_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a9_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a9_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a9_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a10_a = fir_min_ff_adffs_a10_a $ Decay_a10_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a9_a
-- decay_add_sub_aadder_aresult_node_acout_a10_a = CARRY(fir_min_ff_adffs_a10_a & (Decay_a10_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a9_a) # !fir_min_ff_adffs_a10_a & Decay_a10_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a10_a,
	datab => Decay_a10_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a10_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a10_a);

fir_min_ff_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a10_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a10_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a10_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a10_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a11_a = Decay_a11_a_acombout $ fir_min_ff_adffs_a11_a $ decay_add_sub_aadder_aresult_node_acout_a10_a
-- decay_add_sub_aadder_aresult_node_acout_a11_a = CARRY(Decay_a11_a_acombout & !fir_min_ff_adffs_a11_a & !decay_add_sub_aadder_aresult_node_acout_a10_a # !Decay_a11_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a10_a # !fir_min_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a11_a_acombout,
	datab => fir_min_ff_adffs_a11_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a11_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a11_a);

fir_min_ff_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a11_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a11_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a11_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a11_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a12_a = fir_min_ff_adffs_a12_a $ Decay_a12_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a11_a
-- decay_add_sub_aadder_aresult_node_acout_a12_a = CARRY(fir_min_ff_adffs_a12_a & (Decay_a12_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a11_a) # !fir_min_ff_adffs_a12_a & Decay_a12_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a12_a,
	datab => Decay_a12_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a12_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a12_a);

fir_min_ff_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a12_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a12_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a12_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a12_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a13_a = Decay_a13_a_acombout $ fir_min_ff_adffs_a13_a $ decay_add_sub_aadder_aresult_node_acout_a12_a
-- decay_add_sub_aadder_aresult_node_acout_a13_a = CARRY(Decay_a13_a_acombout & !fir_min_ff_adffs_a13_a & !decay_add_sub_aadder_aresult_node_acout_a12_a # !Decay_a13_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a12_a # !fir_min_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a13_a_acombout,
	datab => fir_min_ff_adffs_a13_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a13_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a13_a);

fir_min_ff_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a13_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a13_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a13_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a13_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a14_a = Decay_a14_a_acombout $ fir_min_ff_adffs_a14_a $ !decay_add_sub_aadder_aresult_node_acout_a13_a
-- decay_add_sub_aadder_aresult_node_acout_a14_a = CARRY(Decay_a14_a_acombout & (fir_min_ff_adffs_a14_a # !decay_add_sub_aadder_aresult_node_acout_a13_a) # !Decay_a14_a_acombout & fir_min_ff_adffs_a14_a & !decay_add_sub_aadder_aresult_node_acout_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a14_a_acombout,
	datab => fir_min_ff_adffs_a14_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a14_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a14_a);

fir_min_ff_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a14_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a14_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a14_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a14_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a15_a = Decay_a15_a_acombout $ fir_min_ff_adffs_a15_a $ decay_add_sub_aadder_aresult_node_acout_a14_a
-- decay_add_sub_aadder_aresult_node_acout_a15_a = CARRY(Decay_a15_a_acombout & !fir_min_ff_adffs_a15_a & !decay_add_sub_aadder_aresult_node_acout_a14_a # !Decay_a15_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a14_a # !fir_min_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a15_a_acombout,
	datab => fir_min_ff_adffs_a15_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a15_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a15_a);

fir_min_ff_adffs_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a15_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a15_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a15_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a15_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a16_a = decay_add_sub_aadder_aresult_node_acout_a15_a $ (!fir_min_ff_adffs_a16_a_a167 & !fir_min_ff_adffs_a16_a_a168)
-- decay_add_sub_aadder_aresult_node_acout_a16_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a15_a & (fir_min_ff_adffs_a16_a_a167 # fir_min_ff_adffs_a16_a_a168))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "E10E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a16_a_a167,
	datab => fir_min_ff_adffs_a16_a_a168,
	cin => decay_add_sub_aadder_aresult_node_acout_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a16_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a16_a);

fir_min_ff_adffs_a16_a_a168_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a16_a_a168 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a16_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a16_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a16_a_a168);

decay_add_sub_aadder_aresult_node_acs_buffer_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a17_a = fir_min_mux_a17_a_a23 $ decay_add_sub_aadder_aresult_node_acout_a16_a
-- decay_add_sub_aadder_aresult_node_acout_a17_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a16_a # !fir_min_mux_a17_a_a23)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a17_a_a23,
	cin => decay_add_sub_aadder_aresult_node_acout_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a17_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a17_a);

fir_min_ff_adffs_a17_a_a184_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a17_a_a184 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a17_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a17_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a17_a_a184);

fir_min_mux_a17_a_a23_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a17_a_a23 = fir_min_ff_adffs_a17_a_a183 # fir_min_ff_adffs_a17_a_a184

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a17_a_a183,
	datad => fir_min_ff_adffs_a17_a_a184,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a17_a_a23);

decay_add_sub_aadder_aresult_node_acs_buffer_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a18_a = fir_min_mux_a18_a_a20 $ !decay_add_sub_aadder_aresult_node_acout_a17_a
-- decay_add_sub_aadder_aresult_node_acout_a18_a = CARRY(fir_min_mux_a18_a_a20 & !decay_add_sub_aadder_aresult_node_acout_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a18_a_a20,
	cin => decay_add_sub_aadder_aresult_node_acout_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a18_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a18_a);

fir_min_ff_adffs_a18_a_a186_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a18_a_a186 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a18_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a18_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a18_a_a186);

fir_min_mux_a18_a_a20_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a18_a_a20 = fir_min_ff_adffs_a18_a_a185 # fir_min_ff_adffs_a18_a_a186

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a18_a_a185,
	datad => fir_min_ff_adffs_a18_a_a186,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a18_a_a20);

decay_add_sub_aadder_aresult_node_acs_buffer_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a19_a = fir_min_mux_a19_a_a17 $ decay_add_sub_aadder_aresult_node_acout_a18_a
-- decay_add_sub_aadder_aresult_node_acout_a19_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a18_a # !fir_min_mux_a19_a_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a19_a_a17,
	cin => decay_add_sub_aadder_aresult_node_acout_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a19_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a19_a);

fir_min_ff_adffs_a19_a_a188_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a19_a_a188 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a19_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a19_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a19_a_a188);

fir_min_mux_a19_a_a17_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a19_a_a17 = fir_min_ff_adffs_a19_a_a187 # fir_min_ff_adffs_a19_a_a188

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a19_a_a187,
	datad => fir_min_ff_adffs_a19_a_a188,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a19_a_a17);

decay_add_sub_aadder_aresult_node_acs_buffer_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a20_a = fir_min_mux_a20_a_a14 $ !decay_add_sub_aadder_aresult_node_acout_a19_a
-- decay_add_sub_aadder_aresult_node_acout_a20_a = CARRY(fir_min_mux_a20_a_a14 & !decay_add_sub_aadder_aresult_node_acout_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a20_a_a14,
	cin => decay_add_sub_aadder_aresult_node_acout_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a20_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a20_a);

fir_min_ff_adffs_a20_a_a190_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a20_a_a190 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a20_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a20_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a20_a_a190);

fir_min_mux_a20_a_a14_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a20_a_a14 = fir_min_ff_adffs_a20_a_a189 # fir_min_ff_adffs_a20_a_a190

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a20_a_a189,
	datad => fir_min_ff_adffs_a20_a_a190,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a20_a_a14);

decay_add_sub_aadder_aresult_node_acs_buffer_a21_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a21_a = fir_min_mux_a21_a_a11 $ decay_add_sub_aadder_aresult_node_acout_a20_a
-- decay_add_sub_aadder_aresult_node_acout_a21_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a20_a # !fir_min_mux_a21_a_a11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a21_a_a11,
	cin => decay_add_sub_aadder_aresult_node_acout_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a21_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a21_a);

fir_min_ff_adffs_a21_a_a192_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a21_a_a192 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a21_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a21_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a21_a_a192);

fir_min_mux_a21_a_a11_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a21_a_a11 = fir_min_ff_adffs_a21_a_a191 # fir_min_ff_adffs_a21_a_a192

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a21_a_a191,
	datad => fir_min_ff_adffs_a21_a_a192,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a21_a_a11);

decay_add_sub_aadder_aresult_node_acs_buffer_a22_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a22_a = fir_min_mux_a22_a_a8 $ !decay_add_sub_aadder_aresult_node_acout_a21_a
-- decay_add_sub_aadder_aresult_node_acout_a22_a = CARRY(fir_min_mux_a22_a_a8 & !decay_add_sub_aadder_aresult_node_acout_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a22_a_a8,
	cin => decay_add_sub_aadder_aresult_node_acout_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a22_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a22_a);

fir_min_ff_adffs_a22_a_a194_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a22_a_a194 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a22_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a22_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a22_a_a194);

fir_min_mux_a22_a_a8_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a22_a_a8 = fir_min_ff_adffs_a22_a_a193 # fir_min_ff_adffs_a22_a_a194

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a22_a_a193,
	datad => fir_min_ff_adffs_a22_a_a194,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a22_a_a8);

decay_add_sub_aadder_aresult_node_acs_buffer_a23_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a23_a = fir_min_mux_a23_a_a5 $ decay_add_sub_aadder_aresult_node_acout_a22_a
-- decay_add_sub_aadder_aresult_node_acout_a23_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a22_a # !fir_min_mux_a23_a_a5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a23_a_a5,
	cin => decay_add_sub_aadder_aresult_node_acout_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a23_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a23_a);

fir_min_ff_adffs_a23_a_a196_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a23_a_a196 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a23_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a23_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a23_a_a196);

fir_min_mux_a23_a_a5_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a23_a_a5 = fir_min_ff_adffs_a23_a_a195 # fir_min_ff_adffs_a23_a_a196

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a23_a_a195,
	datad => fir_min_ff_adffs_a23_a_a196,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a23_a_a5);

decay_add_sub_aadder_aresult_node_acs_buffer_a24_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a24_a = fir_min_mux_a24_a_a44 $ !decay_add_sub_aadder_aresult_node_acout_a23_a
-- decay_add_sub_aadder_aresult_node_acout_a24_a = CARRY(fir_min_mux_a24_a_a44 & !decay_add_sub_aadder_aresult_node_acout_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a24_a_a44,
	cin => decay_add_sub_aadder_aresult_node_acout_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a24_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a24_a);

fir_min_ff_adffs_a24_a_a170_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a24_a_a170 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a24_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a24_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a24_a_a170);

fir_out_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(8),
	combout => fir_out_a8_a_acombout);

fir_min_ff_adffs_a24_a_a169_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a24_a_a169 = DFFE(fir_out_a8_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a8_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a24_a_a169);

fir_min_mux_a24_a_a44_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a24_a_a44 = fir_min_ff_adffs_a24_a_a170 # fir_min_ff_adffs_a24_a_a169

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a24_a_a170,
	datad => fir_min_ff_adffs_a24_a_a169,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a24_a_a44);

decay_add_sub_aadder_aresult_node_acs_buffer_a25_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a25_a = fir_min_mux_a25_a_a41 $ decay_add_sub_aadder_aresult_node_acout_a24_a
-- decay_add_sub_aadder_aresult_node_acout_a25_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a24_a # !fir_min_mux_a25_a_a41)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a25_a_a41,
	cin => decay_add_sub_aadder_aresult_node_acout_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a25_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a25_a);

fir_min_ff_adffs_a25_a_a172_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a25_a_a172 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a25_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a25_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a25_a_a172);

fir_out_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(9),
	combout => fir_out_a9_a_acombout);

fir_min_ff_adffs_a25_a_a171_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a25_a_a171 = DFFE(fir_out_a9_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a9_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a25_a_a171);

fir_min_mux_a25_a_a41_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a25_a_a41 = fir_min_ff_adffs_a25_a_a172 # fir_min_ff_adffs_a25_a_a171

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a25_a_a172,
	datad => fir_min_ff_adffs_a25_a_a171,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a25_a_a41);

decay_add_sub_aadder_aresult_node_acs_buffer_a26_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a26_a = fir_min_mux_a26_a_a38 $ !decay_add_sub_aadder_aresult_node_acout_a25_a
-- decay_add_sub_aadder_aresult_node_acout_a26_a = CARRY(fir_min_mux_a26_a_a38 & !decay_add_sub_aadder_aresult_node_acout_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a26_a_a38,
	cin => decay_add_sub_aadder_aresult_node_acout_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a26_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a26_a);

fir_min_ff_adffs_a26_a_a174_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a26_a_a174 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a26_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a26_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a26_a_a174);

fir_min_mux_a26_a_a38_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a26_a_a38 = fir_min_ff_adffs_a26_a_a173 # fir_min_ff_adffs_a26_a_a174

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a26_a_a173,
	datad => fir_min_ff_adffs_a26_a_a174,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a26_a_a38);

decay_add_sub_aadder_aresult_node_acs_buffer_a27_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a27_a = fir_min_mux_a27_a_a35 $ decay_add_sub_aadder_aresult_node_acout_a26_a
-- decay_add_sub_aadder_aresult_node_acout_a27_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a26_a # !fir_min_mux_a27_a_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a27_a_a35,
	cin => decay_add_sub_aadder_aresult_node_acout_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a27_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a27_a);

fir_min_ff_adffs_a27_a_a176_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a27_a_a176 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a27_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a27_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a27_a_a176);

fir_min_mux_a27_a_a35_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a27_a_a35 = fir_min_ff_adffs_a27_a_a175 # fir_min_ff_adffs_a27_a_a176

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a27_a_a175,
	datad => fir_min_ff_adffs_a27_a_a176,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a27_a_a35);

decay_add_sub_aadder_aresult_node_acs_buffer_a28_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a28_a = fir_min_mux_a28_a_a32 $ !decay_add_sub_aadder_aresult_node_acout_a27_a
-- decay_add_sub_aadder_aresult_node_acout_a28_a = CARRY(fir_min_mux_a28_a_a32 & !decay_add_sub_aadder_aresult_node_acout_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a28_a_a32,
	cin => decay_add_sub_aadder_aresult_node_acout_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a28_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a28_a);

fir_min_ff_adffs_a28_a_a178_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a28_a_a178 = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a28_a & GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => decay_add_sub_aadder_aresult_node_acs_buffer_a28_a,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a28_a_a178);

fir_min_mux_a28_a_a32_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a28_a_a32 = fir_min_ff_adffs_a28_a_a177 # fir_min_ff_adffs_a28_a_a178

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a28_a_a177,
	datad => fir_min_ff_adffs_a28_a_a178,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a28_a_a32);

decay_add_sub_aadder_aresult_node_acs_buffer_a29_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a29_a = fir_min_mux_a29_a_a29 $ decay_add_sub_aadder_aresult_node_acout_a28_a
-- decay_add_sub_aadder_aresult_node_acout_a29_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a28_a # !fir_min_mux_a29_a_a29)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a29_a_a29,
	cin => decay_add_sub_aadder_aresult_node_acout_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a29_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a29_a);

fir_min_ff_adffs_a29_a_a180_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a29_a_a180 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a29_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a29_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a29_a_a180);

fir_min_mux_a29_a_a29_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a29_a_a29 = fir_min_ff_adffs_a29_a_a179 # fir_min_ff_adffs_a29_a_a180

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a29_a_a179,
	datad => fir_min_ff_adffs_a29_a_a180,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a29_a_a29);

decay_add_sub_aadder_aresult_node_acs_buffer_a30_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a30_a = fir_min_mux_a30_a_a26 $ !decay_add_sub_aadder_aresult_node_acout_a29_a
-- decay_add_sub_aadder_aresult_node_acout_a30_a = CARRY(fir_min_mux_a30_a_a26 & !decay_add_sub_aadder_aresult_node_acout_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a30_a_a26,
	cin => decay_add_sub_aadder_aresult_node_acout_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a30_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a30_a);

fir_min_ff_adffs_a30_a_a182_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a30_a_a182 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a30_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a30_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a30_a_a182);

fir_min_mux_a30_a_a26_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a30_a_a26 = fir_min_ff_adffs_a30_a_a181 # fir_min_ff_adffs_a30_a_a182

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a30_a_a181,
	datad => fir_min_ff_adffs_a30_a_a182,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a30_a_a26);

fir_min_abs_alcarry_a0_a_a14_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a0_a_a14 = fir_min_ff_adffs_a16_a_a167 # fir_min_ff_adffs_a16_a_a168
-- fir_min_abs_alcarry_a0_a = CARRY(!fir_min_ff_adffs_a16_a_a167 & !fir_min_ff_adffs_a16_a_a168)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "EE11",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a16_a_a167,
	datab => fir_min_ff_adffs_a16_a_a168,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a0_a_a14,
	cout => fir_min_abs_alcarry_a0_a);

fir_min_compare_acomparator_acmp_end_alcarry_a1_a_a164 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a1_a = CARRY(!fir_min_ff_adffs_a1_a & !fir_min_compare_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a1_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a1_a);

fir_min_compare_acomparator_acmp_end_alcarry_a2_a_a163 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a2_a = CARRY(fir_min_ff_adffs_a2_a # !fir_min_compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a2_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a2_a);

fir_min_compare_acomparator_acmp_end_alcarry_a3_a_a162 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a3_a = CARRY(!fir_min_ff_adffs_a3_a & !fir_min_compare_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a3_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a3_a);

fir_min_compare_acomparator_acmp_end_alcarry_a4_a_a161 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a4_a = CARRY(fir_min_ff_adffs_a4_a # !fir_min_compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a4_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a4_a);

fir_min_compare_acomparator_acmp_end_alcarry_a5_a_a160 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a5_a = CARRY(!fir_min_ff_adffs_a5_a & !fir_min_compare_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a5_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a5_a);

fir_min_compare_acomparator_acmp_end_alcarry_a6_a_a159 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a6_a = CARRY(fir_min_ff_adffs_a6_a # !fir_min_compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a6_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a6_a);

fir_min_compare_acomparator_acmp_end_alcarry_a7_a_a158 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a7_a = CARRY(!fir_min_ff_adffs_a7_a & !fir_min_compare_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a7_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a7_a);

fir_min_compare_acomparator_acmp_end_alcarry_a8_a_a157 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a8_a = CARRY(fir_min_ff_adffs_a8_a # !fir_min_compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a8_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a8_a);

fir_min_compare_acomparator_acmp_end_alcarry_a9_a_a156 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a9_a = CARRY(!fir_min_ff_adffs_a9_a & !fir_min_compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a9_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a9_a);

fir_min_compare_acomparator_acmp_end_alcarry_a10_a_a155 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a10_a = CARRY(fir_min_ff_adffs_a10_a # !fir_min_compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a10_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a10_a);

fir_min_compare_acomparator_acmp_end_alcarry_a11_a_a154 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a11_a = CARRY(!fir_min_ff_adffs_a11_a & !fir_min_compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a11_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a11_a);

fir_min_compare_acomparator_acmp_end_alcarry_a12_a_a153 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a12_a = CARRY(fir_min_ff_adffs_a12_a # !fir_min_compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a12_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a12_a);

fir_min_compare_acomparator_acmp_end_alcarry_a13_a_a152 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a13_a = CARRY(!fir_min_ff_adffs_a13_a & !fir_min_compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a13_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a13_a);

fir_min_compare_acomparator_acmp_end_alcarry_a14_a_a151 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a14_a = CARRY(fir_min_ff_adffs_a14_a # !fir_min_compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a14_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a14_a);

fir_min_compare_acomparator_acmp_end_alcarry_a15_a_a150 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a15_a = CARRY(!fir_min_ff_adffs_a15_a & !fir_min_compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a15_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a15_a);

fir_min_compare_acomparator_acmp_end_alcarry_a16_a_a149 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a16_a = CARRY(fir_out_a0_a_acombout & fir_min_abs_alcarry_a0_a_a14 & !fir_min_compare_acomparator_acmp_end_alcarry_a15_a # !fir_out_a0_a_acombout & (fir_min_abs_alcarry_a0_a_a14 # !fir_min_compare_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_acombout,
	datab => fir_min_abs_alcarry_a0_a_a14,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a16_a);

fir_min_compare_acomparator_acmp_end_alcarry_a17_a_a148 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a17_a = CARRY(fir_out_a1_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a16_a # !fir_min_mux_a17_a_a23) # !fir_out_a1_a_acombout & !fir_min_mux_a17_a_a23 & !fir_min_compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a1_a_acombout,
	datab => fir_min_mux_a17_a_a23,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a17_a);

fir_min_compare_acomparator_acmp_end_alcarry_a18_a_a147 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a18_a = CARRY(fir_min_mux_a18_a_a20 & (!fir_min_compare_acomparator_acmp_end_alcarry_a17_a # !fir_out_a2_a_acombout) # !fir_min_mux_a18_a_a20 & !fir_out_a2_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a18_a_a20,
	datab => fir_out_a2_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a18_a);

fir_min_compare_acomparator_acmp_end_alcarry_a19_a_a146 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a19_a = CARRY(fir_min_mux_a19_a_a17 & fir_out_a3_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a18_a # !fir_min_mux_a19_a_a17 & (fir_out_a3_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a19_a_a17,
	datab => fir_out_a3_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a19_a);

fir_min_compare_acomparator_acmp_end_alcarry_a20_a_a145 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a20_a = CARRY(fir_min_mux_a20_a_a14 & (!fir_min_compare_acomparator_acmp_end_alcarry_a19_a # !fir_out_a4_a_acombout) # !fir_min_mux_a20_a_a14 & !fir_out_a4_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a20_a_a14,
	datab => fir_out_a4_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a20_a);

fir_min_compare_acomparator_acmp_end_alcarry_a21_a_a144 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a21_a = CARRY(fir_out_a5_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a20_a # !fir_min_mux_a21_a_a11) # !fir_out_a5_a_acombout & !fir_min_mux_a21_a_a11 & !fir_min_compare_acomparator_acmp_end_alcarry_a20_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a5_a_acombout,
	datab => fir_min_mux_a21_a_a11,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a21_a);

fir_min_compare_acomparator_acmp_end_alcarry_a22_a_a143 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a22_a = CARRY(fir_min_mux_a22_a_a8 & (!fir_min_compare_acomparator_acmp_end_alcarry_a21_a # !fir_out_a6_a_acombout) # !fir_min_mux_a22_a_a8 & !fir_out_a6_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a22_a_a8,
	datab => fir_out_a6_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a22_a);

fir_min_compare_acomparator_acmp_end_alcarry_a23_a_a142 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a23_a = CARRY(fir_out_a7_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a22_a # !fir_min_mux_a23_a_a5) # !fir_out_a7_a_acombout & !fir_min_mux_a23_a_a5 & !fir_min_compare_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a7_a_acombout,
	datab => fir_min_mux_a23_a_a5,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a23_a);

fir_min_compare_acomparator_acmp_end_alcarry_a24_a_a141 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a24_a = CARRY(fir_out_a8_a_acombout & fir_min_mux_a24_a_a44 & !fir_min_compare_acomparator_acmp_end_alcarry_a23_a # !fir_out_a8_a_acombout & (fir_min_mux_a24_a_a44 # !fir_min_compare_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a8_a_acombout,
	datab => fir_min_mux_a24_a_a44,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a24_a);

fir_min_compare_acomparator_acmp_end_alcarry_a25_a_a140 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a25_a = CARRY(fir_min_mux_a25_a_a41 & fir_out_a9_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a24_a # !fir_min_mux_a25_a_a41 & (fir_out_a9_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a25_a_a41,
	datab => fir_out_a9_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a25_a);

fir_min_compare_acomparator_acmp_end_alcarry_a26_a_a139 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a26_a = CARRY(fir_out_a10_a_acombout & fir_min_mux_a26_a_a38 & !fir_min_compare_acomparator_acmp_end_alcarry_a25_a # !fir_out_a10_a_acombout & (fir_min_mux_a26_a_a38 # !fir_min_compare_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a10_a_acombout,
	datab => fir_min_mux_a26_a_a38,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a26_a);

fir_min_compare_acomparator_acmp_end_alcarry_a27_a_a138 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a27_a = CARRY(fir_min_mux_a27_a_a35 & fir_out_a11_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a26_a # !fir_min_mux_a27_a_a35 & (fir_out_a11_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a27_a_a35,
	datab => fir_out_a11_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a27_a);

fir_min_compare_acomparator_acmp_end_alcarry_a28_a_a137 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a28_a = CARRY(fir_out_a12_a_acombout & fir_min_mux_a28_a_a32 & !fir_min_compare_acomparator_acmp_end_alcarry_a27_a # !fir_out_a12_a_acombout & (fir_min_mux_a28_a_a32 # !fir_min_compare_acomparator_acmp_end_alcarry_a27_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a12_a_acombout,
	datab => fir_min_mux_a28_a_a32,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a28_a);

fir_min_compare_acomparator_acmp_end_alcarry_a29_a_a136 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a29_a = CARRY(fir_min_mux_a29_a_a29 & fir_out_a13_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a28_a # !fir_min_mux_a29_a_a29 & (fir_out_a13_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a29_a_a29,
	datab => fir_out_a13_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a29_a);

fir_min_compare_acomparator_acmp_end_alcarry_a30_a_a135 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a30_a = CARRY(fir_min_mux_a30_a_a26 & (!fir_min_compare_acomparator_acmp_end_alcarry_a29_a # !fir_out_a14_a_acombout) # !fir_min_mux_a30_a_a26 & !fir_out_a14_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a30_a_a26,
	datab => fir_out_a14_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a30_a);

fir_min_compare_acomparator_acmp_end_a9 : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_aagb_out = fir_out_a15_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a30_a & fir_min_mux_a31_a_a2 # !fir_out_a15_a_acombout & (fir_min_mux_a31_a_a2 # !fir_min_compare_acomparator_acmp_end_alcarry_a30_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3F03",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_out_a15_a_acombout,
	datad => fir_min_mux_a31_a_a2,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_compare_acomparator_acmp_end_aagb_out);

fir_min_ff_adffs_a31_a_a197_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a31_a_a197 = DFFE(fir_out_a15_a_acombout & !GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_out_a15_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a31_a_a197);

decay_add_sub_aadder_aresult_node_acs_buffer_a31_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a31_a = fir_min_mux_a31_a_a2 $ decay_add_sub_aadder_aresult_node_acout_a30_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a31_a_a2,
	cin => decay_add_sub_aadder_aresult_node_acout_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a31_a);

fir_min_ff_adffs_a31_a_a198_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a31_a_a198 = DFFE(GLOBAL(fir_min_compare_acomparator_acmp_end_aagb_out) & decay_add_sub_aadder_aresult_node_acs_buffer_a31_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a31_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a31_a_a198);

fir_min_mux_a31_a_a2_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a31_a_a2 = fir_min_ff_adffs_a31_a_a197 # fir_min_ff_adffs_a31_a_a198

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a31_a_a197,
	datad => fir_min_ff_adffs_a31_a_a198,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a31_a_a2);

fir_min_abs_alcarry_a0_a_a63_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a0_a_a64 = fir_min_abs_alcarry_a0_a
-- fir_min_abs_alcarry_a0_a_a63 = CARRY(!fir_min_abs_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a0_a_a64,
	cout => fir_min_abs_alcarry_a0_a_a63);

fir_min_abs_alcarry_a1_a_a5 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a1_a = CARRY(!fir_min_mux_a17_a_a23 & !fir_min_abs_alcarry_a0_a_a63)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a17_a_a23,
	cin => fir_min_abs_alcarry_a0_a_a63,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a1_a);

fir_min_abs_alcarry_a1_a_a39_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a1_a_a40 = fir_min_abs_alcarry_a1_a
-- fir_min_abs_alcarry_a1_a_a39 = CARRY(!fir_min_abs_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a1_a_a40,
	cout => fir_min_abs_alcarry_a1_a_a39);

fir_min_abs_alcarry_a2_a_a4 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a2_a = CARRY(!fir_min_mux_a18_a_a20 & !fir_min_abs_alcarry_a1_a_a39)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a18_a_a20,
	cin => fir_min_abs_alcarry_a1_a_a39,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a2_a);

fir_min_abs_alcarry_a2_a_a35_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a2_a_a36 = fir_min_abs_alcarry_a2_a
-- fir_min_abs_alcarry_a2_a_a35 = CARRY(!fir_min_abs_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a2_a_a36,
	cout => fir_min_abs_alcarry_a2_a_a35);

fir_min_abs_alcarry_a3_a_a3 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a3_a = CARRY(!fir_min_mux_a19_a_a17 & !fir_min_abs_alcarry_a2_a_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a19_a_a17,
	cin => fir_min_abs_alcarry_a2_a_a35,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a3_a);

fir_min_abs_alcarry_a3_a_a31_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a3_a_a32 = fir_min_abs_alcarry_a3_a
-- fir_min_abs_alcarry_a3_a_a31 = CARRY(!fir_min_abs_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a3_a_a32,
	cout => fir_min_abs_alcarry_a3_a_a31);

fir_min_abs_alcarry_a4_a_a2 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a4_a = CARRY(!fir_min_mux_a20_a_a14 & !fir_min_abs_alcarry_a3_a_a31)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a20_a_a14,
	cin => fir_min_abs_alcarry_a3_a_a31,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a4_a);

fir_min_abs_alcarry_a4_a_a27_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a4_a_a28 = fir_min_abs_alcarry_a4_a
-- fir_min_abs_alcarry_a4_a_a27 = CARRY(!fir_min_abs_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a4_a_a28,
	cout => fir_min_abs_alcarry_a4_a_a27);

fir_min_abs_alcarry_a5_a_a1 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a5_a = CARRY(!fir_min_mux_a21_a_a11 & !fir_min_abs_alcarry_a4_a_a27)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a21_a_a11,
	cin => fir_min_abs_alcarry_a4_a_a27,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a5_a);

fir_min_abs_alcarry_a5_a_a23_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a5_a_a24 = fir_min_abs_alcarry_a5_a
-- fir_min_abs_alcarry_a5_a_a23 = CARRY(!fir_min_abs_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a5_a_a24,
	cout => fir_min_abs_alcarry_a5_a_a23);

fir_min_abs_alcarry_a6_a_a0 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a6_a = CARRY(!fir_min_mux_a22_a_a8 & !fir_min_abs_alcarry_a5_a_a23)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a22_a_a8,
	cin => fir_min_abs_alcarry_a5_a_a23,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a6_a);

fir_min_abs_alcarry_a6_a_a19_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a6_a_a20 = fir_min_abs_alcarry_a6_a
-- fir_min_abs_alcarry_a6_a_a19 = CARRY(!fir_min_abs_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a6_a_a20,
	cout => fir_min_abs_alcarry_a6_a_a19);

fir_min_abs_alcarry_a7_a_a12 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a7_a = CARRY(!fir_min_mux_a23_a_a5 & !fir_min_abs_alcarry_a6_a_a19)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a23_a_a5,
	cin => fir_min_abs_alcarry_a6_a_a19,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a7_a);

fir_min_abs_alcarry_a7_a_a15_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a7_a_a16 = fir_min_abs_alcarry_a7_a
-- fir_min_abs_alcarry_a7_a_a15 = CARRY(!fir_min_abs_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a7_a_a16,
	cout => fir_min_abs_alcarry_a7_a_a15);

fir_min_abs_alcarry_a8_a_a11 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a8_a = CARRY(!fir_min_mux_a24_a_a44 & !fir_min_abs_alcarry_a7_a_a15)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a24_a_a44,
	cin => fir_min_abs_alcarry_a7_a_a15,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a8_a);

fir_min_abs_alcarry_a8_a_a59_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a8_a_a60 = fir_min_abs_alcarry_a8_a
-- fir_min_abs_alcarry_a8_a_a59 = CARRY(!fir_min_abs_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a8_a_a60,
	cout => fir_min_abs_alcarry_a8_a_a59);

fir_min_abs_alcarry_a9_a_a10 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a9_a = CARRY(!fir_min_mux_a25_a_a41 & !fir_min_abs_alcarry_a8_a_a59)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a25_a_a41,
	cin => fir_min_abs_alcarry_a8_a_a59,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a9_a);

fir_min_abs_alcarry_a9_a_a55_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a9_a_a56 = fir_min_abs_alcarry_a9_a
-- fir_min_abs_alcarry_a9_a_a55 = CARRY(!fir_min_abs_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a9_a_a56,
	cout => fir_min_abs_alcarry_a9_a_a55);

fir_min_abs_alcarry_a10_a_a9 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a10_a = CARRY(!fir_min_mux_a26_a_a38 & !fir_min_abs_alcarry_a9_a_a55)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a26_a_a38,
	cin => fir_min_abs_alcarry_a9_a_a55,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a10_a);

fir_min_abs_alcarry_a10_a_a51_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a10_a_a52 = fir_min_abs_alcarry_a10_a
-- fir_min_abs_alcarry_a10_a_a51 = CARRY(!fir_min_abs_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a10_a_a52,
	cout => fir_min_abs_alcarry_a10_a_a51);

fir_min_abs_alcarry_a11_a_a8 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a11_a = CARRY(!fir_min_mux_a27_a_a35 & !fir_min_abs_alcarry_a10_a_a51)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a27_a_a35,
	cin => fir_min_abs_alcarry_a10_a_a51,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a11_a);

fir_min_abs_alcarry_a11_a_a47_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a11_a_a48 = fir_min_abs_alcarry_a11_a
-- fir_min_abs_alcarry_a11_a_a47 = CARRY(!fir_min_abs_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a11_a_a48,
	cout => fir_min_abs_alcarry_a11_a_a47);

fir_min_abs_alcarry_a12_a_a7 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a12_a = CARRY(!fir_min_mux_a28_a_a32 & !fir_min_abs_alcarry_a11_a_a47)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a28_a_a32,
	cin => fir_min_abs_alcarry_a11_a_a47,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_abs_alcarry_a12_a);

fir_min_abs_alcarry_a12_a_a43_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a12_a_a44 = fir_min_abs_alcarry_a12_a
-- fir_min_abs_alcarry_a12_a_a43 = CARRY(!fir_min_abs_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a12_a_a44,
	cout => fir_min_abs_alcarry_a12_a_a43);

fir_min_abs_a2_a12_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a2_a12 = !fir_min_abs_alcarry_a12_a_a43 & !fir_min_mux_a29_a_a29

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => fir_min_mux_a29_a_a29,
	cin => fir_min_abs_alcarry_a12_a_a43,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a2_a12);

fir_min_abs_a2_a13_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a2_a13 = !fir_min_mux_a30_a_a26 & fir_min_abs_a2_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_mux_a30_a_a26,
	datad => fir_min_abs_a2_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a2_a13);

fir_min_abs_a10 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a_a00001 = fir_min_mux_a31_a_a2 & fir_min_abs_a2_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_a2_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a_a00001);

tlevel_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(14),
	combout => tlevel_a14_a_acombout);

atlevel_add_sub_adatab_node_a14_a_a53 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a14_a = tlevel_a14_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a14_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a14_a);

fir_min_abs_a7_a13_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a7_a13 = fir_min_mux_a30_a_a26 & !fir_min_mux_a31_a_a2

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_mux_a30_a_a26,
	datad => fir_min_mux_a31_a_a2,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a7_a13);

fir_min_abs_a6_a13_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a13 = fir_min_abs_a7_a13 # fir_min_mux_a31_a_a2 & (fir_min_mux_a30_a_a26 $ !fir_min_abs_a2_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECDC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a30_a_a26,
	datab => fir_min_abs_a7_a13,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_a2_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a13);

fir_min_abs_a6_a12_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a12 = fir_min_mux_a29_a_a29 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a12_a_a44)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AA5A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a29_a_a29,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a12_a_a44,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a12);

tlevel_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(13),
	combout => tlevel_a13_a_acombout);

atlevel_add_sub_adatab_node_a13_a_a54 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a13_a = tlevel_a13_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a13_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a13_a);

fir_min_abs_a6_a11_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a11 = fir_min_mux_a28_a_a32 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a11_a_a48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a28_a_a32,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a11_a_a48,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a11);

tlevel_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(12),
	combout => tlevel_a12_a_acombout);

atlevel_add_sub_adatab_node_a12_a_a55 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a12_a = tlevel_a12_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a12_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a12_a);

tlevel_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(11),
	combout => tlevel_a11_a_acombout);

atlevel_add_sub_adatab_node_a11_a_a56 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a11_a = tlevel_a11_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a11_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a11_a);

fir_min_abs_a6_a10_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a10 = fir_min_mux_a27_a_a35 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a10_a_a52)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F05A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a31_a_a2,
	datac => fir_min_mux_a27_a_a35,
	datad => fir_min_abs_alcarry_a10_a_a52,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a10);

tlevel_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(10),
	combout => tlevel_a10_a_acombout);

atlevel_add_sub_adatab_node_a10_a_a57 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a10_a = tlevel_a10_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a10_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a10_a);

fir_min_abs_a6_a9_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a9 = fir_min_mux_a26_a_a38 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a9_a_a56)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a26_a_a38,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a9_a_a56,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a9);

fir_min_abs_a6_a8_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a8 = fir_min_mux_a25_a_a41 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a8_a_a60)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a25_a_a41,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a8_a_a60,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a8);

tlevel_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(9),
	combout => tlevel_a9_a_acombout);

atlevel_add_sub_adatab_node_a9_a_a58 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a9_a = tlevel_a9_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a9_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a9_a);

tlevel_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(8),
	combout => tlevel_a8_a_acombout);

atlevel_add_sub_adatab_node_a8_a_a59 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a8_a = tlevel_a8_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a8_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a8_a);

fir_min_abs_a6_a7_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a7 = fir_min_mux_a24_a_a44 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a7_a_a16)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F05A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a31_a_a2,
	datac => fir_min_mux_a24_a_a44,
	datad => fir_min_abs_alcarry_a7_a_a16,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a7);

atlevel_add_sub_aadder1_0_a1_a_a4_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a4 = atlevel_add_sub_adatab_node_a8_a & fir_min_abs_a6_a7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => atlevel_add_sub_adatab_node_a8_a,
	datad => fir_min_abs_a6_a7,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a4);

atlevel_add_sub_aadder1_0_a1_a_a12_a67_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a67 = fir_min_abs_a6_a8 & (atlevel_add_sub_adatab_node_a9_a # atlevel_add_sub_aadder1_0_a1_a_a4) # !fir_min_abs_a6_a8 & atlevel_add_sub_adatab_node_a9_a & atlevel_add_sub_aadder1_0_a1_a_a4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a8,
	datac => atlevel_add_sub_adatab_node_a9_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a4,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a67);

atlevel_add_sub_aadder1_0_a1_a_a12_a58_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a58 = atlevel_add_sub_adatab_node_a10_a & (fir_min_abs_a6_a9 # atlevel_add_sub_aadder1_0_a1_a_a12_a67) # !atlevel_add_sub_adatab_node_a10_a & fir_min_abs_a6_a9 & atlevel_add_sub_aadder1_0_a1_a_a12_a67

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a10_a,
	datac => fir_min_abs_a6_a9,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a67,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a58);

atlevel_add_sub_aadder1_0_a1_a_a12_a49_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a49 = atlevel_add_sub_adatab_node_a11_a & (fir_min_abs_a6_a10 # atlevel_add_sub_aadder1_0_a1_a_a12_a58) # !atlevel_add_sub_adatab_node_a11_a & fir_min_abs_a6_a10 & atlevel_add_sub_aadder1_0_a1_a_a12_a58

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a11_a,
	datac => fir_min_abs_a6_a10,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a58,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a49);

atlevel_add_sub_aadder1_0_a1_a_a12_a40_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a40 = fir_min_abs_a6_a11 & (atlevel_add_sub_adatab_node_a12_a # atlevel_add_sub_aadder1_0_a1_a_a12_a49) # !fir_min_abs_a6_a11 & atlevel_add_sub_adatab_node_a12_a & atlevel_add_sub_aadder1_0_a1_a_a12_a49

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a11,
	datac => atlevel_add_sub_adatab_node_a12_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a49,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a40);

atlevel_add_sub_aadder1_0_a1_a_a12_a31_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a31 = fir_min_abs_a6_a12 & (atlevel_add_sub_adatab_node_a13_a # atlevel_add_sub_aadder1_0_a1_a_a12_a40) # !fir_min_abs_a6_a12 & atlevel_add_sub_adatab_node_a13_a & atlevel_add_sub_aadder1_0_a1_a_a12_a40

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a12,
	datac => atlevel_add_sub_adatab_node_a13_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a40,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a31);

atlevel_add_sub_aadder1_0_a1_a_a12_a22_I : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_a12_a22 = atlevel_add_sub_adatab_node_a14_a & (fir_min_abs_a6_a13 # atlevel_add_sub_aadder1_0_a1_a_a12_a31) # !atlevel_add_sub_adatab_node_a14_a & fir_min_abs_a6_a13 & atlevel_add_sub_aadder1_0_a1_a_a12_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a14_a,
	datac => fir_min_abs_a6_a13,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a31,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_0_a1_a_a12_a22);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a = DFFE(atlevel_add_sub_adatab_node_a15_a $ fir_min_abs_a_a00001 $ atlevel_add_sub_aadder1_0_a1_a_a12_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_adatab_node_a15_a,
	datac => fir_min_abs_a_a00001,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a = DFFE(fir_min_abs_a6_a13 $ atlevel_add_sub_adatab_node_a14_a $ atlevel_add_sub_aadder1_0_a1_a_a12_a31, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a13,
	datac => atlevel_add_sub_adatab_node_a14_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a31,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a = DFFE(fir_min_abs_a6_a12 $ atlevel_add_sub_adatab_node_a13_a $ atlevel_add_sub_aadder1_0_a1_a_a12_a40, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a12,
	datac => atlevel_add_sub_adatab_node_a13_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a40,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a = DFFE(fir_min_abs_a6_a11 $ atlevel_add_sub_adatab_node_a12_a $ atlevel_add_sub_aadder1_0_a1_a_a12_a49, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a11,
	datac => atlevel_add_sub_adatab_node_a12_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a49,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a = DFFE(fir_min_abs_a6_a10 $ atlevel_add_sub_adatab_node_a11_a $ atlevel_add_sub_aadder1_0_a1_a_a12_a58, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_abs_a6_a10,
	datac => atlevel_add_sub_adatab_node_a11_a,
	datad => atlevel_add_sub_aadder1_0_a1_a_a12_a58,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a = DFFE(atlevel_add_sub_adatab_node_a10_a $ atlevel_add_sub_aadder1_0_a1_a_a12_a67 $ fir_min_abs_a6_a9, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a10_a,
	datac => atlevel_add_sub_aadder1_0_a1_a_a12_a67,
	datad => fir_min_abs_a6_a9,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a = DFFE(fir_min_abs_a6_a8 $ atlevel_add_sub_aadder1_0_a1_a_a4 $ atlevel_add_sub_adatab_node_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_abs_a6_a8,
	datac => atlevel_add_sub_aadder1_0_a1_a_a4,
	datad => atlevel_add_sub_adatab_node_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a);

tlevel_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(7),
	combout => tlevel_a7_a_acombout);

atlevel_add_sub_adatab_node_a7_a_a44 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a7_a = tlevel_a7_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a7_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a7_a);

fir_min_abs_a6_a6_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a6 = fir_min_mux_a23_a_a5 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a6_a_a20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F05A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a31_a_a2,
	datac => fir_min_mux_a23_a_a5,
	datad => fir_min_abs_alcarry_a6_a_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a6);

tlevel_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(6),
	combout => tlevel_a6_a_acombout);

atlevel_add_sub_adatab_node_a6_a_a45 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a6_a = tlevel_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => tlevel_a6_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a6_a);

fir_min_abs_a6_a5_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a5 = fir_min_mux_a22_a_a8 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a5_a_a24)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F05A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a31_a_a2,
	datac => fir_min_mux_a22_a_a8,
	datad => fir_min_abs_alcarry_a5_a_a24,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a5);

tlevel_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(5),
	combout => tlevel_a5_a_acombout);

atlevel_add_sub_adatab_node_a5_a_a46 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a5_a = tlevel_a5_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a5_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a5_a);

fir_min_abs_a6_a4_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a4 = fir_min_mux_a21_a_a11 $ (!fir_min_abs_alcarry_a4_a_a28 & fir_min_mux_a31_a_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A5AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a21_a_a11,
	datac => fir_min_abs_alcarry_a4_a_a28,
	datad => fir_min_mux_a31_a_a2,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a4);

tlevel_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(4),
	combout => tlevel_a4_a_acombout);

atlevel_add_sub_adatab_node_a4_a_a47 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a4_a = tlevel_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a4_a);

fir_min_abs_a6_a3_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a3 = fir_min_mux_a20_a_a14 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a3_a_a32)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a20_a_a14,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a3_a_a32,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a3);

tlevel_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(3),
	combout => tlevel_a3_a_acombout);

atlevel_add_sub_adatab_node_a3_a_a48 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a3_a = tlevel_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a3_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a3_a);

fir_min_abs_a6_a2_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a2 = fir_min_mux_a19_a_a17 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a2_a_a36)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F03C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a31_a_a2,
	datac => fir_min_mux_a19_a_a17,
	datad => fir_min_abs_alcarry_a2_a_a36,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a2);

tlevel_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(2),
	combout => tlevel_a2_a_acombout);

atlevel_add_sub_adatab_node_a2_a_a49 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a2_a = tlevel_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => tlevel_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a2_a);

fir_min_abs_a6_a1_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a1 = fir_min_mux_a18_a_a20 $ (!fir_min_abs_alcarry_a1_a_a40 & fir_min_mux_a31_a_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A5AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a18_a_a20,
	datac => fir_min_abs_alcarry_a1_a_a40,
	datad => fir_min_mux_a31_a_a2,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a1);

fir_min_abs_a6_a0_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a6_a0 = fir_min_mux_a17_a_a23 $ (fir_min_mux_a31_a_a2 & !fir_min_abs_alcarry_a0_a_a64)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_mux_a17_a_a23,
	datac => fir_min_mux_a31_a_a2,
	datad => fir_min_abs_alcarry_a0_a_a64,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a6_a0);

tlevel_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(1),
	combout => tlevel_a1_a_acombout);

atlevel_add_sub_adatab_node_a1_a_a50 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a1_a = tlevel_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a1_a);

fir_min_abs_a_a00000_a1 : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_a_a00000 = fir_min_abs_alcarry_a0_a_a14

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => fir_min_abs_alcarry_a0_a_a14,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_a_a00000);

tlevel_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(0),
	combout => tlevel_a0_a_acombout);

atlevel_add_sub_adatab_node_a0_a_a51 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a0_a = tlevel_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a0_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a = DFFE(fir_min_abs_a_a00000 $ atlevel_add_sub_adatab_node_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a = CARRY(fir_min_abs_a_a00000 & atlevel_add_sub_adatab_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_abs_a_a00000,
	datab => atlevel_add_sub_adatab_node_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a = DFFE(fir_min_abs_a6_a0 $ atlevel_add_sub_adatab_node_a1_a $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a = CARRY(fir_min_abs_a6_a0 & !atlevel_add_sub_adatab_node_a1_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a # !fir_min_abs_a6_a0 & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a # !atlevel_add_sub_adatab_node_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_abs_a6_a0,
	datab => atlevel_add_sub_adatab_node_a1_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a = DFFE(atlevel_add_sub_adatab_node_a2_a $ fir_min_abs_a6_a1 $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a = CARRY(atlevel_add_sub_adatab_node_a2_a & (fir_min_abs_a6_a1 # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a) # !atlevel_add_sub_adatab_node_a2_a & fir_min_abs_a6_a1 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a2_a,
	datab => fir_min_abs_a6_a1,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a = DFFE(atlevel_add_sub_adatab_node_a3_a $ fir_min_abs_a6_a2 $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a = CARRY(atlevel_add_sub_adatab_node_a3_a & !fir_min_abs_a6_a2 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a # !atlevel_add_sub_adatab_node_a3_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a # !fir_min_abs_a6_a2))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a3_a,
	datab => fir_min_abs_a6_a2,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a = DFFE(atlevel_add_sub_adatab_node_a4_a $ fir_min_abs_a6_a3 $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a = CARRY(atlevel_add_sub_adatab_node_a4_a & (fir_min_abs_a6_a3 # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a) # !atlevel_add_sub_adatab_node_a4_a & fir_min_abs_a6_a3 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a4_a,
	datab => fir_min_abs_a6_a3,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a = DFFE(atlevel_add_sub_adatab_node_a5_a $ fir_min_abs_a6_a4 $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a = CARRY(atlevel_add_sub_adatab_node_a5_a & !fir_min_abs_a6_a4 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a # !atlevel_add_sub_adatab_node_a5_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a # !fir_min_abs_a6_a4))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a5_a,
	datab => fir_min_abs_a6_a4,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a = DFFE(atlevel_add_sub_adatab_node_a6_a $ fir_min_abs_a6_a5 $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a = CARRY(atlevel_add_sub_adatab_node_a6_a & (fir_min_abs_a6_a5 # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a) # !atlevel_add_sub_adatab_node_a6_a & fir_min_abs_a6_a5 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a6_a,
	datab => fir_min_abs_a6_a5,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a = DFFE(atlevel_add_sub_adatab_node_a7_a $ fir_min_abs_a6_a6 $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a = CARRY(atlevel_add_sub_adatab_node_a7_a & !fir_min_abs_a6_a6 & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a # !atlevel_add_sub_adatab_node_a7_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a # !fir_min_abs_a6_a6))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a7_a,
	datab => fir_min_abs_a6_a6,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a = DFFE(!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a = DFFE(fir_min_abs_a6_a7 $ atlevel_add_sub_adatab_node_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_abs_a6_a7,
	datad => atlevel_add_sub_adatab_node_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a = atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a = CARRY(atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a & atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a,
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a = atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a);

atlevel_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(15));

atlevel_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(14));

atlevel_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(13));

atlevel_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(12));

atlevel_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(11));

atlevel_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(10));

atlevel_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(9));

atlevel_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(8));

atlevel_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(7));

atlevel_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(6));

atlevel_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(5));

atlevel_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(4));

atlevel_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(3));

atlevel_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(2));

atlevel_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(1));

atlevel_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(0));
END structure;


