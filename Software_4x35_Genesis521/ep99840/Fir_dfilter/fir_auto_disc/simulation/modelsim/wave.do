onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/ADisc_En
add wave -noupdate -format Literal -radix hexadecimal /tb/tlevel
add wave -noupdate -format Literal -radix hexadecimal /tb/atlevel
add wave -noupdate -format Analog-Step -height 200 -radix decimal /tb/fir_out
add wave -noupdate -format Analog-Step -height 200 -radix decimal /tb/atlevel
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {3764361 ns} {349378 ns}
WaveRestoreZoom {0 ns} {315 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
