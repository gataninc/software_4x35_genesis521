-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 16:14:26"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_auto_disc IS
    PORT (
	clk : IN std_logic;
	imr : IN std_logic;
	tlevel : IN std_logic_vector(15 DOWNTO 0);
	ADisc_En : IN std_logic;
	fir_out : IN std_logic_vector(15 DOWNTO 0);
	Decay : IN std_logic_vector(15 DOWNTO 0);
	atlevel : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_auto_disc;

ARCHITECTURE structure OF fir_auto_disc IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_clk : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_ADisc_En : std_logic;
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Decay : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_atlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a30_a_a616 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a29_a_a617 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a28_a_a618 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a27_a_a619 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a26_a_a620 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a25_a_a621 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a24_a_a622 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a23_a_a623 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a22_a_a624 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a21_a_a625 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a20_a_a626 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a19_a_a627 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a18_a_a628 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a17_a_a629 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a16_a_a630 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a15_a_a631 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a14_a_a632 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a13_a_a633 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a12_a_a634 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a11_a_a635 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a10_a_a636 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a9_a_a637 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a8_a_a638 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a7_a_a639 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a6_a_a640 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a5_a_a641 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a4_a_a642 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a3_a_a643 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a2_a_a644 : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a1_a_a645 : std_logic;
SIGNAL clk_apadio : std_logic;
SIGNAL imr_apadio : std_logic;
SIGNAL tlevel_a7_a_apadio : std_logic;
SIGNAL tlevel_a6_a_apadio : std_logic;
SIGNAL tlevel_a5_a_apadio : std_logic;
SIGNAL tlevel_a4_a_apadio : std_logic;
SIGNAL tlevel_a3_a_apadio : std_logic;
SIGNAL tlevel_a2_a_apadio : std_logic;
SIGNAL tlevel_a1_a_apadio : std_logic;
SIGNAL tlevel_a0_a_apadio : std_logic;
SIGNAL tlevel_a15_a_apadio : std_logic;
SIGNAL tlevel_a14_a_apadio : std_logic;
SIGNAL tlevel_a13_a_apadio : std_logic;
SIGNAL tlevel_a12_a_apadio : std_logic;
SIGNAL tlevel_a11_a_apadio : std_logic;
SIGNAL tlevel_a10_a_apadio : std_logic;
SIGNAL tlevel_a9_a_apadio : std_logic;
SIGNAL tlevel_a8_a_apadio : std_logic;
SIGNAL ADisc_En_apadio : std_logic;
SIGNAL fir_out_a7_a_apadio : std_logic;
SIGNAL fir_out_a15_a_apadio : std_logic;
SIGNAL fir_out_a6_a_apadio : std_logic;
SIGNAL fir_out_a5_a_apadio : std_logic;
SIGNAL fir_out_a4_a_apadio : std_logic;
SIGNAL fir_out_a3_a_apadio : std_logic;
SIGNAL fir_out_a2_a_apadio : std_logic;
SIGNAL fir_out_a1_a_apadio : std_logic;
SIGNAL fir_out_a14_a_apadio : std_logic;
SIGNAL fir_out_a13_a_apadio : std_logic;
SIGNAL fir_out_a12_a_apadio : std_logic;
SIGNAL fir_out_a11_a_apadio : std_logic;
SIGNAL fir_out_a10_a_apadio : std_logic;
SIGNAL fir_out_a9_a_apadio : std_logic;
SIGNAL fir_out_a8_a_apadio : std_logic;
SIGNAL fir_out_a0_a_apadio : std_logic;
SIGNAL Decay_a15_a_apadio : std_logic;
SIGNAL Decay_a14_a_apadio : std_logic;
SIGNAL Decay_a13_a_apadio : std_logic;
SIGNAL Decay_a12_a_apadio : std_logic;
SIGNAL Decay_a11_a_apadio : std_logic;
SIGNAL Decay_a10_a_apadio : std_logic;
SIGNAL Decay_a9_a_apadio : std_logic;
SIGNAL Decay_a8_a_apadio : std_logic;
SIGNAL Decay_a7_a_apadio : std_logic;
SIGNAL Decay_a6_a_apadio : std_logic;
SIGNAL Decay_a5_a_apadio : std_logic;
SIGNAL Decay_a4_a_apadio : std_logic;
SIGNAL Decay_a3_a_apadio : std_logic;
SIGNAL Decay_a2_a_apadio : std_logic;
SIGNAL Decay_a1_a_apadio : std_logic;
SIGNAL Decay_a0_a_apadio : std_logic;
SIGNAL atlevel_a15_a_apadio : std_logic;
SIGNAL atlevel_a14_a_apadio : std_logic;
SIGNAL atlevel_a13_a_apadio : std_logic;
SIGNAL atlevel_a12_a_apadio : std_logic;
SIGNAL atlevel_a11_a_apadio : std_logic;
SIGNAL atlevel_a10_a_apadio : std_logic;
SIGNAL atlevel_a9_a_apadio : std_logic;
SIGNAL atlevel_a8_a_apadio : std_logic;
SIGNAL atlevel_a7_a_apadio : std_logic;
SIGNAL atlevel_a6_a_apadio : std_logic;
SIGNAL atlevel_a5_a_apadio : std_logic;
SIGNAL atlevel_a4_a_apadio : std_logic;
SIGNAL atlevel_a3_a_apadio : std_logic;
SIGNAL atlevel_a2_a_apadio : std_logic;
SIGNAL atlevel_a1_a_apadio : std_logic;
SIGNAL atlevel_a0_a_apadio : std_logic;
SIGNAL tlevel_a15_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a15_a : std_logic;
SIGNAL fir_out_a14_a_acombout : std_logic;
SIGNAL fir_out_a15_a_acombout : std_logic;
SIGNAL ADisc_En_acombout : std_logic;
SIGNAL fir_out_a13_a_acombout : std_logic;
SIGNAL fir_out_a12_a_acombout : std_logic;
SIGNAL fir_out_a11_a_acombout : std_logic;
SIGNAL fir_out_a10_a_acombout : std_logic;
SIGNAL fir_out_a7_a_acombout : std_logic;
SIGNAL fir_out_a6_a_acombout : std_logic;
SIGNAL fir_out_a2_a_acombout : std_logic;
SIGNAL fir_out_a1_a_acombout : std_logic;
SIGNAL Decay_a15_a_acombout : std_logic;
SIGNAL Decay_a14_a_acombout : std_logic;
SIGNAL Decay_a13_a_acombout : std_logic;
SIGNAL Decay_a12_a_acombout : std_logic;
SIGNAL Decay_a11_a_acombout : std_logic;
SIGNAL Decay_a10_a_acombout : std_logic;
SIGNAL Decay_a9_a_acombout : std_logic;
SIGNAL Decay_a8_a_acombout : std_logic;
SIGNAL Decay_a7_a_acombout : std_logic;
SIGNAL Decay_a6_a_acombout : std_logic;
SIGNAL Decay_a5_a_acombout : std_logic;
SIGNAL Decay_a4_a_acombout : std_logic;
SIGNAL Decay_a3_a_acombout : std_logic;
SIGNAL Decay_a2_a_acombout : std_logic;
SIGNAL Decay_a1_a_acombout : std_logic;
SIGNAL Decay_a0_a_acombout : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL fir_min_ff_anxd_a15_a_a783 : std_logic;
SIGNAL fir_min_ff_adffs_a0_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL fir_min_ff_adffs_a1_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL fir_min_ff_adffs_a2_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL fir_min_ff_adffs_a3_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL fir_min_ff_adffs_a4_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL fir_min_ff_adffs_a5_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL fir_min_ff_adffs_a6_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL fir_min_ff_adffs_a7_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL fir_min_ff_adffs_a8_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a9_a : std_logic;
SIGNAL fir_min_ff_adffs_a9_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a9_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a10_a : std_logic;
SIGNAL fir_min_ff_adffs_a10_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a10_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a11_a : std_logic;
SIGNAL fir_min_ff_adffs_a11_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a11_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a12_a : std_logic;
SIGNAL fir_min_ff_adffs_a12_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a12_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a13_a : std_logic;
SIGNAL fir_min_ff_adffs_a13_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a13_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a14_a : std_logic;
SIGNAL fir_min_ff_adffs_a14_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a14_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a15_a : std_logic;
SIGNAL fir_min_ff_adffs_a15_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a15_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a16_a : std_logic;
SIGNAL fir_out_a0_a_acombout : std_logic;
SIGNAL fir_min_mux_a16_a_a19 : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a_a0 : std_logic;
SIGNAL fir_min_ff_adffs_a16_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a17_a : std_logic;
SIGNAL fir_min_ff_adffs_a17_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a18_a : std_logic;
SIGNAL fir_min_ff_adffs_a18_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a19_a : std_logic;
SIGNAL fir_out_a3_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a19_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a20_a : std_logic;
SIGNAL fir_out_a4_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a20_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a20_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a21_a : std_logic;
SIGNAL fir_out_a5_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a21_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a21_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a22_a : std_logic;
SIGNAL fir_min_ff_adffs_a22_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a22_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a23_a : std_logic;
SIGNAL fir_min_ff_adffs_a23_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a23_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a24_a : std_logic;
SIGNAL fir_out_a8_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a24_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a24_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a25_a : std_logic;
SIGNAL fir_out_a9_a_acombout : std_logic;
SIGNAL fir_min_ff_adffs_a25_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a25_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a26_a : std_logic;
SIGNAL fir_min_ff_adffs_a26_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a26_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a27_a : std_logic;
SIGNAL fir_min_ff_adffs_a27_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a27_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a28_a : std_logic;
SIGNAL fir_min_ff_adffs_a28_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a28_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a29_a : std_logic;
SIGNAL fir_min_ff_adffs_a29_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a29_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acout_a30_a : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a31_a : std_logic;
SIGNAL fir_min_ff_adffs_a31_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL fir_min_compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL decay_add_sub_aadder_aresult_node_acs_buffer_a30_a : std_logic;
SIGNAL fir_min_ff_adffs_a30_a : std_logic;
SIGNAL fir_min_abs_alcarry_a0_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a1_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a2_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a3_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a4_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a5_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a6_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a7_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a8_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a9_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a10_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a11_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a12_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a13_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a14_a_aCOUT : std_logic;
SIGNAL fir_min_abs_alcarry_a15_a : std_logic;
SIGNAL tlevel_a14_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a14_a : std_logic;
SIGNAL fir_min_abs_alcarry_a14_a : std_logic;
SIGNAL tlevel_a13_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a13_a : std_logic;
SIGNAL fir_min_abs_alcarry_a13_a : std_logic;
SIGNAL tlevel_a12_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a12_a : std_logic;
SIGNAL fir_min_abs_alcarry_a12_a : std_logic;
SIGNAL tlevel_a11_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a11_a : std_logic;
SIGNAL fir_min_abs_alcarry_a11_a : std_logic;
SIGNAL tlevel_a10_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a10_a : std_logic;
SIGNAL fir_min_abs_alcarry_a10_a : std_logic;
SIGNAL tlevel_a9_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a9_a : std_logic;
SIGNAL fir_min_abs_alcarry_a9_a : std_logic;
SIGNAL tlevel_a8_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a8_a : std_logic;
SIGNAL fir_min_abs_alcarry_a8_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL tlevel_a7_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a7_a : std_logic;
SIGNAL fir_min_abs_alcarry_a7_a : std_logic;
SIGNAL tlevel_a6_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a6_a : std_logic;
SIGNAL fir_min_abs_alcarry_a6_a : std_logic;
SIGNAL tlevel_a5_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a5_a : std_logic;
SIGNAL fir_min_abs_alcarry_a5_a : std_logic;
SIGNAL tlevel_a4_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a4_a : std_logic;
SIGNAL fir_min_abs_alcarry_a4_a : std_logic;
SIGNAL tlevel_a3_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a3_a : std_logic;
SIGNAL fir_min_abs_alcarry_a3_a : std_logic;
SIGNAL tlevel_a2_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a2_a : std_logic;
SIGNAL fir_min_abs_alcarry_a2_a : std_logic;
SIGNAL tlevel_a1_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a1_a : std_logic;
SIGNAL fir_min_abs_alcarry_a1_a : std_logic;
SIGNAL tlevel_a0_a_acombout : std_logic;
SIGNAL atlevel_add_sub_adatab_node_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL NOT_fir_min_ff_anxd_a15_a_a783 : std_logic;
SIGNAL NOT_ADisc_En_acombout : std_logic;

BEGIN

ww_clk <= clk;
ww_imr <= imr;
ww_tlevel <= tlevel;
ww_ADisc_En <= ADisc_En;
ww_fir_out <= fir_out;
ww_Decay <= Decay;
atlevel <= ww_atlevel;
NOT_fir_min_ff_anxd_a15_a_a783 <= NOT fir_min_ff_anxd_a15_a_a783;
NOT_ADisc_En_acombout <= NOT ADisc_En_acombout;

tlevel_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(15),
	combout => tlevel_a15_a_acombout);

atlevel_add_sub_adatab_node_a15_a_a71 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a15_a = tlevel_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a15_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a15_a);

fir_out_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(14),
	combout => fir_out_a14_a_acombout);

fir_out_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(15),
	combout => fir_out_a15_a_acombout);

ADisc_En_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ADisc_En,
	combout => ADisc_En_acombout);

fir_out_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(13),
	combout => fir_out_a13_a_acombout);

fir_out_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(12),
	combout => fir_out_a12_a_acombout);

fir_out_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(11),
	combout => fir_out_a11_a_acombout);

fir_out_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(10),
	combout => fir_out_a10_a_acombout);

fir_out_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(7),
	combout => fir_out_a7_a_acombout);

fir_out_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(6),
	combout => fir_out_a6_a_acombout);

fir_out_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(2),
	combout => fir_out_a2_a_acombout);

fir_out_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(1),
	combout => fir_out_a1_a_acombout);

Decay_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(15),
	combout => Decay_a15_a_acombout);

Decay_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(14),
	combout => Decay_a14_a_acombout);

Decay_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(13),
	combout => Decay_a13_a_acombout);

Decay_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(12),
	combout => Decay_a12_a_acombout);

Decay_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(11),
	combout => Decay_a11_a_acombout);

Decay_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(10),
	combout => Decay_a10_a_acombout);

Decay_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(9),
	combout => Decay_a9_a_acombout);

Decay_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(8),
	combout => Decay_a8_a_acombout);

Decay_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(7),
	combout => Decay_a7_a_acombout);

Decay_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(6),
	combout => Decay_a6_a_acombout);

Decay_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(5),
	combout => Decay_a5_a_acombout);

Decay_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(4),
	combout => Decay_a4_a_acombout);

Decay_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(3),
	combout => Decay_a3_a_acombout);

Decay_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(2),
	combout => Decay_a2_a_acombout);

Decay_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(1),
	combout => Decay_a1_a_acombout);

Decay_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Decay(0),
	combout => Decay_a0_a_acombout);

decay_add_sub_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a0_a = fir_min_ff_adffs_a0_a $ Decay_a0_a_acombout
-- decay_add_sub_aadder_aresult_node_acout_a0_a = CARRY(fir_min_ff_adffs_a0_a & Decay_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a0_a,
	datab => Decay_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a0_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a0_a);

clk_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

fir_min_ff_anxd_a15_a_a783_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_anxd_a15_a_a783 = ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_ff_anxd_a15_a_a783);

fir_min_ff_adffs_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a0_a = DFFE(GLOBAL(fir_min_ff_anxd_a15_a_a783) & decay_add_sub_aadder_aresult_node_acs_buffer_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- fir_min_compare_acomparator_acmp_end_alcarry_a0_a = CARRY(fir_min_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "CCF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => NOT_fir_min_ff_anxd_a15_a_a783,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a0_a,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a0_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a1_a = fir_min_ff_adffs_a1_a $ Decay_a1_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a0_a
-- decay_add_sub_aadder_aresult_node_acout_a1_a = CARRY(fir_min_ff_adffs_a1_a & !Decay_a1_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a0_a # !fir_min_ff_adffs_a1_a & (!decay_add_sub_aadder_aresult_node_acout_a0_a # !Decay_a1_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a1_a,
	datab => Decay_a1_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a1_a);

fir_min_ff_adffs_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a1_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a1_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a1_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a1_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a2_a = fir_min_ff_adffs_a2_a $ Decay_a2_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a1_a
-- decay_add_sub_aadder_aresult_node_acout_a2_a = CARRY(fir_min_ff_adffs_a2_a & (Decay_a2_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a1_a) # !fir_min_ff_adffs_a2_a & Decay_a2_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a2_a,
	datab => Decay_a2_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a2_a);

fir_min_ff_adffs_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a2_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a2_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a2_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a2_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a3_a = Decay_a3_a_acombout $ fir_min_ff_adffs_a3_a $ decay_add_sub_aadder_aresult_node_acout_a2_a
-- decay_add_sub_aadder_aresult_node_acout_a3_a = CARRY(Decay_a3_a_acombout & !fir_min_ff_adffs_a3_a & !decay_add_sub_aadder_aresult_node_acout_a2_a # !Decay_a3_a_acombout & (!decay_add_sub_aadder_aresult_node_acout_a2_a # !fir_min_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a3_a_acombout,
	datab => fir_min_ff_adffs_a3_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a3_a);

fir_min_ff_adffs_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a3_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a3_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a3_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a3_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a4_a = Decay_a4_a_acombout $ fir_min_ff_adffs_a4_a $ !decay_add_sub_aadder_aresult_node_acout_a3_a
-- decay_add_sub_aadder_aresult_node_acout_a4_a = CARRY(Decay_a4_a_acombout & (fir_min_ff_adffs_a4_a # !decay_add_sub_aadder_aresult_node_acout_a3_a) # !Decay_a4_a_acombout & fir_min_ff_adffs_a4_a & !decay_add_sub_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a4_a_acombout,
	datab => fir_min_ff_adffs_a4_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a4_a);

fir_min_ff_adffs_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a4_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a4_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a4_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a4_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a5_a = fir_min_ff_adffs_a5_a $ Decay_a5_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a4_a
-- decay_add_sub_aadder_aresult_node_acout_a5_a = CARRY(fir_min_ff_adffs_a5_a & !Decay_a5_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a4_a # !fir_min_ff_adffs_a5_a & (!decay_add_sub_aadder_aresult_node_acout_a4_a # !Decay_a5_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a5_a,
	datab => Decay_a5_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a5_a);

fir_min_ff_adffs_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a5_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a5_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a5_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a5_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a6_a = fir_min_ff_adffs_a6_a $ Decay_a6_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a5_a
-- decay_add_sub_aadder_aresult_node_acout_a6_a = CARRY(fir_min_ff_adffs_a6_a & (Decay_a6_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a5_a) # !fir_min_ff_adffs_a6_a & Decay_a6_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a6_a,
	datab => Decay_a6_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a6_a);

fir_min_ff_adffs_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a6_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a6_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a6_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a6_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a7_a = fir_min_ff_adffs_a7_a $ Decay_a7_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a6_a
-- decay_add_sub_aadder_aresult_node_acout_a7_a = CARRY(fir_min_ff_adffs_a7_a & !Decay_a7_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a6_a # !fir_min_ff_adffs_a7_a & (!decay_add_sub_aadder_aresult_node_acout_a6_a # !Decay_a7_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a7_a,
	datab => Decay_a7_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a7_a);

fir_min_ff_adffs_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a7_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a7_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a7_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a7_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a8_a = fir_min_ff_adffs_a8_a $ Decay_a8_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a7_a
-- decay_add_sub_aadder_aresult_node_acout_a8_a = CARRY(fir_min_ff_adffs_a8_a & (Decay_a8_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a7_a) # !fir_min_ff_adffs_a8_a & Decay_a8_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a8_a,
	datab => Decay_a8_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a8_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a8_a);

fir_min_ff_adffs_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a8_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a8_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a8_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a8_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a9_a = fir_min_ff_adffs_a9_a $ Decay_a9_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a8_a
-- decay_add_sub_aadder_aresult_node_acout_a9_a = CARRY(fir_min_ff_adffs_a9_a & !Decay_a9_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a8_a # !fir_min_ff_adffs_a9_a & (!decay_add_sub_aadder_aresult_node_acout_a8_a # !Decay_a9_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a9_a,
	datab => Decay_a9_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a9_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a9_a);

fir_min_ff_adffs_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a9_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a9_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a9_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a9_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a10_a = Decay_a10_a_acombout $ fir_min_ff_adffs_a10_a $ !decay_add_sub_aadder_aresult_node_acout_a9_a
-- decay_add_sub_aadder_aresult_node_acout_a10_a = CARRY(Decay_a10_a_acombout & (fir_min_ff_adffs_a10_a # !decay_add_sub_aadder_aresult_node_acout_a9_a) # !Decay_a10_a_acombout & fir_min_ff_adffs_a10_a & !decay_add_sub_aadder_aresult_node_acout_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Decay_a10_a_acombout,
	datab => fir_min_ff_adffs_a10_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a10_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a10_a);

fir_min_ff_adffs_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a10_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a10_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a10_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a10_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a11_a = fir_min_ff_adffs_a11_a $ Decay_a11_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a10_a
-- decay_add_sub_aadder_aresult_node_acout_a11_a = CARRY(fir_min_ff_adffs_a11_a & !Decay_a11_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a10_a # !fir_min_ff_adffs_a11_a & (!decay_add_sub_aadder_aresult_node_acout_a10_a # !Decay_a11_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a11_a,
	datab => Decay_a11_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a11_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a11_a);

fir_min_ff_adffs_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a11_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a11_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a11_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a11_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a12_a = fir_min_ff_adffs_a12_a $ Decay_a12_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a11_a
-- decay_add_sub_aadder_aresult_node_acout_a12_a = CARRY(fir_min_ff_adffs_a12_a & (Decay_a12_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a11_a) # !fir_min_ff_adffs_a12_a & Decay_a12_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a12_a,
	datab => Decay_a12_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a12_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a12_a);

fir_min_ff_adffs_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a12_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a12_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a12_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a12_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a13_a = fir_min_ff_adffs_a13_a $ Decay_a13_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a12_a
-- decay_add_sub_aadder_aresult_node_acout_a13_a = CARRY(fir_min_ff_adffs_a13_a & !Decay_a13_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a12_a # !fir_min_ff_adffs_a13_a & (!decay_add_sub_aadder_aresult_node_acout_a12_a # !Decay_a13_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a13_a,
	datab => Decay_a13_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a13_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a13_a);

fir_min_ff_adffs_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a13_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a13_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a13_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a13_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a14_a = fir_min_ff_adffs_a14_a $ Decay_a14_a_acombout $ !decay_add_sub_aadder_aresult_node_acout_a13_a
-- decay_add_sub_aadder_aresult_node_acout_a14_a = CARRY(fir_min_ff_adffs_a14_a & (Decay_a14_a_acombout # !decay_add_sub_aadder_aresult_node_acout_a13_a) # !fir_min_ff_adffs_a14_a & Decay_a14_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a14_a,
	datab => Decay_a14_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a14_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a14_a);

fir_min_ff_adffs_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a14_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a14_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a14_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a14_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a15_a = fir_min_ff_adffs_a15_a $ Decay_a15_a_acombout $ decay_add_sub_aadder_aresult_node_acout_a14_a
-- decay_add_sub_aadder_aresult_node_acout_a15_a = CARRY(fir_min_ff_adffs_a15_a & !Decay_a15_a_acombout & !decay_add_sub_aadder_aresult_node_acout_a14_a # !fir_min_ff_adffs_a15_a & (!decay_add_sub_aadder_aresult_node_acout_a14_a # !Decay_a15_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a15_a,
	datab => Decay_a15_a_acombout,
	cin => decay_add_sub_aadder_aresult_node_acout_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a15_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a15_a);

fir_min_ff_adffs_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a15_a = DFFE(decay_add_sub_aadder_aresult_node_acs_buffer_a15_a & ADisc_En_acombout & !fir_min_compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => decay_add_sub_aadder_aresult_node_acs_buffer_a15_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a15_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a16_a = fir_min_ff_adffs_a16_a $ !decay_add_sub_aadder_aresult_node_acout_a15_a
-- decay_add_sub_aadder_aresult_node_acout_a16_a = CARRY(fir_min_ff_adffs_a16_a & !decay_add_sub_aadder_aresult_node_acout_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a16_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a16_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a16_a);

fir_out_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(0),
	combout => fir_out_a0_a_acombout);

fir_min_mux_a16_a_a19_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_mux_a16_a_a19 = decay_add_sub_aadder_aresult_node_acs_buffer_a16_a & (fir_out_a0_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out) # !decay_add_sub_aadder_aresult_node_acs_buffer_a16_a & fir_out_a0_a_acombout & fir_min_compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a16_a,
	datac => fir_out_a0_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_mux_a16_a_a19);

fir_min_abs_alcarry_a0_a_a0_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a0_a_a0 = !fir_min_ff_adffs_a16_a & fir_min_ff_adffs_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => fir_min_ff_adffs_a16_a,
	datad => fir_min_ff_adffs_a31_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a0_a_a0);

fir_min_ff_adffs_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a16_a = DFFE(GLOBAL(ADisc_En_acombout) & fir_min_mux_a16_a_a19, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- fir_min_abs_alcarry_a0_a_aCOUT = CARRY(fir_min_abs_alcarry_a0_a_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "AACC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_mux_a16_a_a19,
	datab => fir_min_abs_alcarry_a0_a_a0,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => NOT_ADisc_En_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a16_a,
	cout => fir_min_abs_alcarry_a0_a_aCOUT);

decay_add_sub_aadder_aresult_node_acs_buffer_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a17_a = fir_min_ff_adffs_a17_a $ decay_add_sub_aadder_aresult_node_acout_a16_a
-- decay_add_sub_aadder_aresult_node_acout_a17_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a16_a # !fir_min_ff_adffs_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a17_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a17_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a17_a);

fir_min_ff_adffs_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a17_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a1_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a17_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a1_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a17_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a17_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a18_a = fir_min_ff_adffs_a18_a $ !decay_add_sub_aadder_aresult_node_acout_a17_a
-- decay_add_sub_aadder_aresult_node_acout_a18_a = CARRY(fir_min_ff_adffs_a18_a & !decay_add_sub_aadder_aresult_node_acout_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a18_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a18_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a18_a);

fir_min_ff_adffs_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a18_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a2_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a18_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a2_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a18_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a18_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a19_a = fir_min_ff_adffs_a19_a $ decay_add_sub_aadder_aresult_node_acout_a18_a
-- decay_add_sub_aadder_aresult_node_acout_a19_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a18_a # !fir_min_ff_adffs_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a19_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a19_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a19_a);

fir_out_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(3),
	combout => fir_out_a3_a_acombout);

fir_min_ff_adffs_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a19_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a3_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a19_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => decay_add_sub_aadder_aresult_node_acs_buffer_a19_a,
	datab => fir_out_a3_a_acombout,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a19_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a20_a = fir_min_ff_adffs_a20_a $ !decay_add_sub_aadder_aresult_node_acout_a19_a
-- decay_add_sub_aadder_aresult_node_acout_a20_a = CARRY(fir_min_ff_adffs_a20_a & !decay_add_sub_aadder_aresult_node_acout_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a20_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a20_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a20_a);

fir_out_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(4),
	combout => fir_out_a4_a_acombout);

fir_min_ff_adffs_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a20_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a4_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a20_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ADisc_En_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a20_a,
	datac => fir_out_a4_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a20_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a21_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a21_a = fir_min_ff_adffs_a21_a $ decay_add_sub_aadder_aresult_node_acout_a20_a
-- decay_add_sub_aadder_aresult_node_acout_a21_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a20_a # !fir_min_ff_adffs_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a21_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a21_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a21_a);

fir_out_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(5),
	combout => fir_out_a5_a_acombout);

fir_min_ff_adffs_a21_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a21_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a5_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a21_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => decay_add_sub_aadder_aresult_node_acs_buffer_a21_a,
	datab => fir_out_a5_a_acombout,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a21_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a22_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a22_a = fir_min_ff_adffs_a22_a $ !decay_add_sub_aadder_aresult_node_acout_a21_a
-- decay_add_sub_aadder_aresult_node_acout_a22_a = CARRY(fir_min_ff_adffs_a22_a & !decay_add_sub_aadder_aresult_node_acout_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a22_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a22_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a22_a);

fir_min_ff_adffs_a22_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a22_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a6_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a22_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a6_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a22_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a22_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a23_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a23_a = fir_min_ff_adffs_a23_a $ decay_add_sub_aadder_aresult_node_acout_a22_a
-- decay_add_sub_aadder_aresult_node_acout_a23_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a22_a # !fir_min_ff_adffs_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a23_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a23_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a23_a);

fir_min_ff_adffs_a23_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a23_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a7_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a23_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a7_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a23_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a23_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a24_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a24_a = fir_min_ff_adffs_a24_a $ !decay_add_sub_aadder_aresult_node_acout_a23_a
-- decay_add_sub_aadder_aresult_node_acout_a24_a = CARRY(fir_min_ff_adffs_a24_a & !decay_add_sub_aadder_aresult_node_acout_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a24_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a24_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a24_a);

fir_out_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(8),
	combout => fir_out_a8_a_acombout);

fir_min_ff_adffs_a24_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a24_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a8_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a24_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ADisc_En_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a24_a,
	datac => fir_out_a8_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a24_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a25_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a25_a = fir_min_ff_adffs_a25_a $ decay_add_sub_aadder_aresult_node_acout_a24_a
-- decay_add_sub_aadder_aresult_node_acout_a25_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a24_a # !fir_min_ff_adffs_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a25_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a25_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a25_a);

fir_out_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(9),
	combout => fir_out_a9_a_acombout);

fir_min_ff_adffs_a25_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a25_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a9_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a25_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ADisc_En_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a25_a,
	datac => fir_out_a9_a_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a25_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a26_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a26_a = fir_min_ff_adffs_a26_a $ !decay_add_sub_aadder_aresult_node_acout_a25_a
-- decay_add_sub_aadder_aresult_node_acout_a26_a = CARRY(fir_min_ff_adffs_a26_a & !decay_add_sub_aadder_aresult_node_acout_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a26_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a26_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a26_a);

fir_min_ff_adffs_a26_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a26_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a10_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a26_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A280",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ADisc_En_acombout,
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datac => fir_out_a10_a_acombout,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a26_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a26_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a27_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a27_a = fir_min_ff_adffs_a27_a $ decay_add_sub_aadder_aresult_node_acout_a26_a
-- decay_add_sub_aadder_aresult_node_acout_a27_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a26_a # !fir_min_ff_adffs_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a27_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a27_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a27_a);

fir_min_ff_adffs_a27_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a27_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a11_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a27_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a11_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a27_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a27_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a28_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a28_a = fir_min_ff_adffs_a28_a $ !decay_add_sub_aadder_aresult_node_acout_a27_a
-- decay_add_sub_aadder_aresult_node_acout_a28_a = CARRY(fir_min_ff_adffs_a28_a & !decay_add_sub_aadder_aresult_node_acout_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a28_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a28_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a28_a);

fir_min_ff_adffs_a28_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a28_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a12_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a28_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a12_a_acombout,
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datac => ADisc_En_acombout,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a28_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a28_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a29_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a29_a = fir_min_ff_adffs_a29_a $ decay_add_sub_aadder_aresult_node_acout_a28_a
-- decay_add_sub_aadder_aresult_node_acout_a29_a = CARRY(!decay_add_sub_aadder_aresult_node_acout_a28_a # !fir_min_ff_adffs_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a29_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a29_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a29_a);

fir_min_ff_adffs_a29_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a29_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a13_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a29_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a13_a_acombout,
	datab => decay_add_sub_aadder_aresult_node_acs_buffer_a29_a,
	datac => ADisc_En_acombout,
	datad => fir_min_compare_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a29_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a30_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a30_a = fir_min_ff_adffs_a30_a $ !decay_add_sub_aadder_aresult_node_acout_a29_a
-- decay_add_sub_aadder_aresult_node_acout_a30_a = CARRY(fir_min_ff_adffs_a30_a & !decay_add_sub_aadder_aresult_node_acout_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a30_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a30_a,
	cout => decay_add_sub_aadder_aresult_node_acout_a30_a);

decay_add_sub_aadder_aresult_node_acs_buffer_a31_a_aI : apex20ke_lcell 
-- Equation(s):
-- decay_add_sub_aadder_aresult_node_acs_buffer_a31_a = decay_add_sub_aadder_aresult_node_acout_a30_a $ fir_min_ff_adffs_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => fir_min_ff_adffs_a31_a,
	cin => decay_add_sub_aadder_aresult_node_acout_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => decay_add_sub_aadder_aresult_node_acs_buffer_a31_a);

fir_min_ff_adffs_a31_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a31_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a15_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a31_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8A80",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ADisc_En_acombout,
	datab => fir_out_a15_a_acombout,
	datac => fir_min_compare_acomparator_acmp_end_aagb_out,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a31_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a31_a);

fir_min_compare_acomparator_acmp_end_alcarry_a1_a_a645_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a1_a = CARRY(!fir_min_ff_adffs_a1_a & !fir_min_compare_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a1_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a1_a);

fir_min_compare_acomparator_acmp_end_alcarry_a2_a_a644_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a2_a = CARRY(fir_min_ff_adffs_a2_a # !fir_min_compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a2_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a2_a);

fir_min_compare_acomparator_acmp_end_alcarry_a3_a_a643_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a3_a = CARRY(!fir_min_ff_adffs_a3_a & !fir_min_compare_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a3_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a3_a);

fir_min_compare_acomparator_acmp_end_alcarry_a4_a_a642_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a4_a = CARRY(fir_min_ff_adffs_a4_a # !fir_min_compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a4_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a4_a);

fir_min_compare_acomparator_acmp_end_alcarry_a5_a_a641_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a5_a = CARRY(!fir_min_ff_adffs_a5_a & !fir_min_compare_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a5_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a5_a);

fir_min_compare_acomparator_acmp_end_alcarry_a6_a_a640_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a6_a = CARRY(fir_min_ff_adffs_a6_a # !fir_min_compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a6_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a6_a);

fir_min_compare_acomparator_acmp_end_alcarry_a7_a_a639_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a7_a = CARRY(!fir_min_ff_adffs_a7_a & !fir_min_compare_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a7_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a7_a);

fir_min_compare_acomparator_acmp_end_alcarry_a8_a_a638_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a8_a = CARRY(fir_min_ff_adffs_a8_a # !fir_min_compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a8_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a8_a);

fir_min_compare_acomparator_acmp_end_alcarry_a9_a_a637_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a9_a = CARRY(!fir_min_ff_adffs_a9_a & !fir_min_compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a9_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a9_a);

fir_min_compare_acomparator_acmp_end_alcarry_a10_a_a636_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a10_a = CARRY(fir_min_ff_adffs_a10_a # !fir_min_compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a10_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a10_a);

fir_min_compare_acomparator_acmp_end_alcarry_a11_a_a635_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a11_a = CARRY(!fir_min_ff_adffs_a11_a & !fir_min_compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a11_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a11_a);

fir_min_compare_acomparator_acmp_end_alcarry_a12_a_a634_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a12_a = CARRY(fir_min_ff_adffs_a12_a # !fir_min_compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a12_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a12_a);

fir_min_compare_acomparator_acmp_end_alcarry_a13_a_a633_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a13_a = CARRY(!fir_min_ff_adffs_a13_a & !fir_min_compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a13_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a13_a);

fir_min_compare_acomparator_acmp_end_alcarry_a14_a_a632_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a14_a = CARRY(fir_min_ff_adffs_a14_a # !fir_min_compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a14_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a14_a);

fir_min_compare_acomparator_acmp_end_alcarry_a15_a_a631_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a15_a = CARRY(!fir_min_ff_adffs_a15_a & !fir_min_compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => fir_min_ff_adffs_a15_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a15_a);

fir_min_compare_acomparator_acmp_end_alcarry_a16_a_a630_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a16_a = CARRY(fir_out_a0_a_acombout & fir_min_ff_adffs_a16_a & !fir_min_compare_acomparator_acmp_end_alcarry_a15_a # !fir_out_a0_a_acombout & (fir_min_ff_adffs_a16_a # !fir_min_compare_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_acombout,
	datab => fir_min_ff_adffs_a16_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a16_a);

fir_min_compare_acomparator_acmp_end_alcarry_a17_a_a629_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a17_a = CARRY(fir_out_a1_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a16_a # !fir_min_ff_adffs_a17_a) # !fir_out_a1_a_acombout & !fir_min_ff_adffs_a17_a & !fir_min_compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a1_a_acombout,
	datab => fir_min_ff_adffs_a17_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a17_a);

fir_min_compare_acomparator_acmp_end_alcarry_a18_a_a628_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a18_a = CARRY(fir_out_a2_a_acombout & fir_min_ff_adffs_a18_a & !fir_min_compare_acomparator_acmp_end_alcarry_a17_a # !fir_out_a2_a_acombout & (fir_min_ff_adffs_a18_a # !fir_min_compare_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a2_a_acombout,
	datab => fir_min_ff_adffs_a18_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a18_a);

fir_min_compare_acomparator_acmp_end_alcarry_a19_a_a627_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a19_a = CARRY(fir_out_a3_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a18_a # !fir_min_ff_adffs_a19_a) # !fir_out_a3_a_acombout & !fir_min_ff_adffs_a19_a & !fir_min_compare_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a3_a_acombout,
	datab => fir_min_ff_adffs_a19_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a19_a);

fir_min_compare_acomparator_acmp_end_alcarry_a20_a_a626_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a20_a = CARRY(fir_out_a4_a_acombout & fir_min_ff_adffs_a20_a & !fir_min_compare_acomparator_acmp_end_alcarry_a19_a # !fir_out_a4_a_acombout & (fir_min_ff_adffs_a20_a # !fir_min_compare_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a4_a_acombout,
	datab => fir_min_ff_adffs_a20_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a20_a);

fir_min_compare_acomparator_acmp_end_alcarry_a21_a_a625_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a21_a = CARRY(fir_min_ff_adffs_a21_a & fir_out_a5_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a20_a # !fir_min_ff_adffs_a21_a & (fir_out_a5_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a21_a,
	datab => fir_out_a5_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a21_a);

fir_min_compare_acomparator_acmp_end_alcarry_a22_a_a624_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a22_a = CARRY(fir_out_a6_a_acombout & fir_min_ff_adffs_a22_a & !fir_min_compare_acomparator_acmp_end_alcarry_a21_a # !fir_out_a6_a_acombout & (fir_min_ff_adffs_a22_a # !fir_min_compare_acomparator_acmp_end_alcarry_a21_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a6_a_acombout,
	datab => fir_min_ff_adffs_a22_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a22_a);

fir_min_compare_acomparator_acmp_end_alcarry_a23_a_a623_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a23_a = CARRY(fir_min_ff_adffs_a23_a & fir_out_a7_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a22_a # !fir_min_ff_adffs_a23_a & (fir_out_a7_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a23_a,
	datab => fir_out_a7_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a23_a);

fir_min_compare_acomparator_acmp_end_alcarry_a24_a_a622_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a24_a = CARRY(fir_min_ff_adffs_a24_a & (!fir_min_compare_acomparator_acmp_end_alcarry_a23_a # !fir_out_a8_a_acombout) # !fir_min_ff_adffs_a24_a & !fir_out_a8_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a24_a,
	datab => fir_out_a8_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a24_a);

fir_min_compare_acomparator_acmp_end_alcarry_a25_a_a621_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a25_a = CARRY(fir_out_a9_a_acombout & (!fir_min_compare_acomparator_acmp_end_alcarry_a24_a # !fir_min_ff_adffs_a25_a) # !fir_out_a9_a_acombout & !fir_min_ff_adffs_a25_a & !fir_min_compare_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a9_a_acombout,
	datab => fir_min_ff_adffs_a25_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a25_a);

fir_min_compare_acomparator_acmp_end_alcarry_a26_a_a620_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a26_a = CARRY(fir_out_a10_a_acombout & fir_min_ff_adffs_a26_a & !fir_min_compare_acomparator_acmp_end_alcarry_a25_a # !fir_out_a10_a_acombout & (fir_min_ff_adffs_a26_a # !fir_min_compare_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a10_a_acombout,
	datab => fir_min_ff_adffs_a26_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a26_a);

fir_min_compare_acomparator_acmp_end_alcarry_a27_a_a619_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a27_a = CARRY(fir_min_ff_adffs_a27_a & fir_out_a11_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a26_a # !fir_min_ff_adffs_a27_a & (fir_out_a11_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a27_a,
	datab => fir_out_a11_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a27_a);

fir_min_compare_acomparator_acmp_end_alcarry_a28_a_a618_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a28_a = CARRY(fir_min_ff_adffs_a28_a & (!fir_min_compare_acomparator_acmp_end_alcarry_a27_a # !fir_out_a12_a_acombout) # !fir_min_ff_adffs_a28_a & !fir_out_a12_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a28_a,
	datab => fir_out_a12_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a28_a);

fir_min_compare_acomparator_acmp_end_alcarry_a29_a_a617_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a29_a = CARRY(fir_min_ff_adffs_a29_a & fir_out_a13_a_acombout & !fir_min_compare_acomparator_acmp_end_alcarry_a28_a # !fir_min_ff_adffs_a29_a & (fir_out_a13_a_acombout # !fir_min_compare_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a29_a,
	datab => fir_out_a13_a_acombout,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a29_a);

fir_min_compare_acomparator_acmp_end_alcarry_a30_a_a616_I : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_alcarry_a30_a = CARRY(fir_out_a14_a_acombout & fir_min_ff_adffs_a30_a & !fir_min_compare_acomparator_acmp_end_alcarry_a29_a # !fir_out_a14_a_acombout & (fir_min_ff_adffs_a30_a # !fir_min_compare_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a14_a_acombout,
	datab => fir_min_ff_adffs_a30_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => fir_min_compare_acomparator_acmp_end_alcarry_a30_a);

fir_min_compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- fir_min_compare_acomparator_acmp_end_aagb_out = fir_out_a15_a_acombout & (fir_min_compare_acomparator_acmp_end_alcarry_a30_a # !fir_min_ff_adffs_a31_a) # !fir_out_a15_a_acombout & fir_min_compare_acomparator_acmp_end_alcarry_a30_a & !fir_min_ff_adffs_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C0FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => fir_out_a15_a_acombout,
	datad => fir_min_ff_adffs_a31_a,
	cin => fir_min_compare_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_compare_acomparator_acmp_end_aagb_out);

fir_min_ff_adffs_a30_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_ff_adffs_a30_a = DFFE(ADisc_En_acombout & (fir_min_compare_acomparator_acmp_end_aagb_out & fir_out_a14_a_acombout # !fir_min_compare_acomparator_acmp_end_aagb_out & decay_add_sub_aadder_aresult_node_acs_buffer_a30_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a14_a_acombout,
	datab => fir_min_compare_acomparator_acmp_end_aagb_out,
	datac => ADisc_En_acombout,
	datad => decay_add_sub_aadder_aresult_node_acs_buffer_a30_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => fir_min_ff_adffs_a30_a);

fir_min_abs_alcarry_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a1_a = fir_min_ff_adffs_a17_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a0_a_aCOUT
-- fir_min_abs_alcarry_a1_a_aCOUT = CARRY(fir_min_ff_adffs_a17_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a0_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a17_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a0_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a1_a,
	cout => fir_min_abs_alcarry_a1_a_aCOUT);

fir_min_abs_alcarry_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a2_a = fir_min_ff_adffs_a18_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a1_a_aCOUT
-- fir_min_abs_alcarry_a2_a_aCOUT = CARRY(!fir_min_abs_alcarry_a1_a_aCOUT & (fir_min_ff_adffs_a18_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a18_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a1_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a2_a,
	cout => fir_min_abs_alcarry_a2_a_aCOUT);

fir_min_abs_alcarry_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a3_a = fir_min_ff_adffs_a19_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a2_a_aCOUT
-- fir_min_abs_alcarry_a3_a_aCOUT = CARRY(fir_min_ff_adffs_a19_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a2_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a19_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a2_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a3_a,
	cout => fir_min_abs_alcarry_a3_a_aCOUT);

fir_min_abs_alcarry_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a4_a = fir_min_ff_adffs_a20_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a3_a_aCOUT
-- fir_min_abs_alcarry_a4_a_aCOUT = CARRY(!fir_min_abs_alcarry_a3_a_aCOUT & (fir_min_ff_adffs_a20_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a20_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a3_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a4_a,
	cout => fir_min_abs_alcarry_a4_a_aCOUT);

fir_min_abs_alcarry_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a5_a = fir_min_ff_adffs_a21_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a4_a_aCOUT
-- fir_min_abs_alcarry_a5_a_aCOUT = CARRY(fir_min_ff_adffs_a21_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a4_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a21_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a4_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a5_a,
	cout => fir_min_abs_alcarry_a5_a_aCOUT);

fir_min_abs_alcarry_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a6_a = fir_min_ff_adffs_a22_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a5_a_aCOUT
-- fir_min_abs_alcarry_a6_a_aCOUT = CARRY(!fir_min_abs_alcarry_a5_a_aCOUT & (fir_min_ff_adffs_a22_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a22_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a5_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a6_a,
	cout => fir_min_abs_alcarry_a6_a_aCOUT);

fir_min_abs_alcarry_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a7_a = fir_min_ff_adffs_a23_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a6_a_aCOUT
-- fir_min_abs_alcarry_a7_a_aCOUT = CARRY(fir_min_ff_adffs_a23_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a6_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a23_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a6_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a7_a,
	cout => fir_min_abs_alcarry_a7_a_aCOUT);

fir_min_abs_alcarry_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a8_a = fir_min_ff_adffs_a24_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a7_a_aCOUT
-- fir_min_abs_alcarry_a8_a_aCOUT = CARRY(!fir_min_abs_alcarry_a7_a_aCOUT & (fir_min_ff_adffs_a24_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a24_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a7_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a8_a,
	cout => fir_min_abs_alcarry_a8_a_aCOUT);

fir_min_abs_alcarry_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a9_a = fir_min_ff_adffs_a25_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a8_a_aCOUT
-- fir_min_abs_alcarry_a9_a_aCOUT = CARRY(fir_min_ff_adffs_a25_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a8_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a25_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a8_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a9_a,
	cout => fir_min_abs_alcarry_a9_a_aCOUT);

fir_min_abs_alcarry_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a10_a = fir_min_ff_adffs_a26_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a9_a_aCOUT
-- fir_min_abs_alcarry_a10_a_aCOUT = CARRY(!fir_min_abs_alcarry_a9_a_aCOUT & (fir_min_ff_adffs_a26_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a26_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a9_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a10_a,
	cout => fir_min_abs_alcarry_a10_a_aCOUT);

fir_min_abs_alcarry_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a11_a = fir_min_ff_adffs_a27_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a10_a_aCOUT
-- fir_min_abs_alcarry_a11_a_aCOUT = CARRY(fir_min_ff_adffs_a27_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a10_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a27_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a10_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a11_a,
	cout => fir_min_abs_alcarry_a11_a_aCOUT);

fir_min_abs_alcarry_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a12_a = fir_min_ff_adffs_a28_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a11_a_aCOUT
-- fir_min_abs_alcarry_a12_a_aCOUT = CARRY(!fir_min_abs_alcarry_a11_a_aCOUT & (fir_min_ff_adffs_a28_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a28_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a11_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a12_a,
	cout => fir_min_abs_alcarry_a12_a_aCOUT);

fir_min_abs_alcarry_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a13_a = fir_min_ff_adffs_a29_a $ fir_min_ff_adffs_a31_a $ fir_min_abs_alcarry_a12_a_aCOUT
-- fir_min_abs_alcarry_a13_a_aCOUT = CARRY(fir_min_ff_adffs_a29_a $ !fir_min_ff_adffs_a31_a # !fir_min_abs_alcarry_a12_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a29_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a12_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a13_a,
	cout => fir_min_abs_alcarry_a13_a_aCOUT);

fir_min_abs_alcarry_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a14_a = fir_min_ff_adffs_a30_a $ fir_min_ff_adffs_a31_a $ !fir_min_abs_alcarry_a13_a_aCOUT
-- fir_min_abs_alcarry_a14_a_aCOUT = CARRY(!fir_min_abs_alcarry_a13_a_aCOUT & (fir_min_ff_adffs_a30_a $ fir_min_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_min_ff_adffs_a30_a,
	datab => fir_min_ff_adffs_a31_a,
	cin => fir_min_abs_alcarry_a13_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a14_a,
	cout => fir_min_abs_alcarry_a14_a_aCOUT);

fir_min_abs_alcarry_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- fir_min_abs_alcarry_a15_a = fir_min_abs_alcarry_a14_a_aCOUT

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => fir_min_abs_alcarry_a14_a_aCOUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => fir_min_abs_alcarry_a15_a);

tlevel_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(14),
	combout => tlevel_a14_a_acombout);

atlevel_add_sub_adatab_node_a14_a_a72 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a14_a = tlevel_a14_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a14_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a14_a);

tlevel_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(13),
	combout => tlevel_a13_a_acombout);

atlevel_add_sub_adatab_node_a13_a_a73 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a13_a = tlevel_a13_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a13_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a13_a);

tlevel_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(12),
	combout => tlevel_a12_a_acombout);

atlevel_add_sub_adatab_node_a12_a_a74 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a12_a = tlevel_a12_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a12_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a12_a);

tlevel_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(11),
	combout => tlevel_a11_a_acombout);

atlevel_add_sub_adatab_node_a11_a_a75 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a11_a = tlevel_a11_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a11_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a11_a);

tlevel_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(10),
	combout => tlevel_a10_a_acombout);

atlevel_add_sub_adatab_node_a10_a_a76 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a10_a = tlevel_a10_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a10_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a10_a);

tlevel_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(9),
	combout => tlevel_a9_a_acombout);

atlevel_add_sub_adatab_node_a9_a_a77 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a9_a = tlevel_a9_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a9_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a9_a);

tlevel_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(8),
	combout => tlevel_a8_a_acombout);

atlevel_add_sub_adatab_node_a8_a_a78 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a8_a = tlevel_a8_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => tlevel_a8_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a8_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a = DFFE(atlevel_add_sub_adatab_node_a8_a $ fir_min_abs_alcarry_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a = CARRY(atlevel_add_sub_adatab_node_a8_a & fir_min_abs_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a8_a,
	datab => fir_min_abs_alcarry_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a = DFFE(atlevel_add_sub_adatab_node_a9_a $ fir_min_abs_alcarry_a9_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a = CARRY(atlevel_add_sub_adatab_node_a9_a & !fir_min_abs_alcarry_a9_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a # !atlevel_add_sub_adatab_node_a9_a & (!atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a # !fir_min_abs_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a9_a,
	datab => fir_min_abs_alcarry_a9_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a = DFFE(atlevel_add_sub_adatab_node_a10_a $ fir_min_abs_alcarry_a10_a $ !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a = CARRY(atlevel_add_sub_adatab_node_a10_a & (fir_min_abs_alcarry_a10_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a) # !atlevel_add_sub_adatab_node_a10_a & fir_min_abs_alcarry_a10_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a10_a,
	datab => fir_min_abs_alcarry_a10_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a = DFFE(atlevel_add_sub_adatab_node_a11_a $ fir_min_abs_alcarry_a11_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a = CARRY(atlevel_add_sub_adatab_node_a11_a & !fir_min_abs_alcarry_a11_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a # !atlevel_add_sub_adatab_node_a11_a & (!atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a # !fir_min_abs_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a11_a,
	datab => fir_min_abs_alcarry_a11_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a = DFFE(atlevel_add_sub_adatab_node_a12_a $ fir_min_abs_alcarry_a12_a $ !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a = CARRY(atlevel_add_sub_adatab_node_a12_a & (fir_min_abs_alcarry_a12_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a) # !atlevel_add_sub_adatab_node_a12_a & fir_min_abs_alcarry_a12_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a12_a,
	datab => fir_min_abs_alcarry_a12_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a = DFFE(atlevel_add_sub_adatab_node_a13_a $ fir_min_abs_alcarry_a13_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a = CARRY(atlevel_add_sub_adatab_node_a13_a & !fir_min_abs_alcarry_a13_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a # !atlevel_add_sub_adatab_node_a13_a & (!atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a # !fir_min_abs_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a13_a,
	datab => fir_min_abs_alcarry_a13_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a = DFFE(atlevel_add_sub_adatab_node_a14_a $ fir_min_abs_alcarry_a14_a $ !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a6_a = CARRY(atlevel_add_sub_adatab_node_a14_a & (fir_min_abs_alcarry_a14_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a) # !atlevel_add_sub_adatab_node_a14_a & fir_min_abs_alcarry_a14_a & !atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a14_a,
	datab => fir_min_abs_alcarry_a14_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a6_a);

atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a = DFFE(atlevel_add_sub_adatab_node_a15_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a6_a $ fir_min_abs_alcarry_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a15_a,
	datad => fir_min_abs_alcarry_a15_a,
	cin => atlevel_add_sub_aadder1_0_a1_a_aresult_node_acout_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a);

tlevel_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(7),
	combout => tlevel_a7_a_acombout);

atlevel_add_sub_adatab_node_a7_a_a63 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a7_a = tlevel_a7_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a7_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a7_a);

tlevel_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(6),
	combout => tlevel_a6_a_acombout);

atlevel_add_sub_adatab_node_a6_a_a64 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a6_a = tlevel_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a6_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a6_a);

tlevel_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(5),
	combout => tlevel_a5_a_acombout);

atlevel_add_sub_adatab_node_a5_a_a65 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a5_a = tlevel_a5_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a5_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a5_a);

tlevel_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(4),
	combout => tlevel_a4_a_acombout);

atlevel_add_sub_adatab_node_a4_a_a66 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a4_a = tlevel_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a4_a);

tlevel_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(3),
	combout => tlevel_a3_a_acombout);

atlevel_add_sub_adatab_node_a3_a_a67 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a3_a = tlevel_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a3_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a3_a);

tlevel_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(2),
	combout => tlevel_a2_a_acombout);

atlevel_add_sub_adatab_node_a2_a_a68 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a2_a = tlevel_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a2_a);

tlevel_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(1),
	combout => tlevel_a1_a_acombout);

atlevel_add_sub_adatab_node_a1_a_a69 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a1_a = tlevel_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a1_a);

tlevel_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(0),
	combout => tlevel_a0_a_acombout);

atlevel_add_sub_adatab_node_a0_a_a70 : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_adatab_node_a0_a = tlevel_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_adatab_node_a0_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a = DFFE(atlevel_add_sub_adatab_node_a0_a $ fir_min_ff_adffs_a16_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a = CARRY(atlevel_add_sub_adatab_node_a0_a & fir_min_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a0_a,
	datab => fir_min_ff_adffs_a16_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a = DFFE(atlevel_add_sub_adatab_node_a1_a $ fir_min_abs_alcarry_a1_a $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a = CARRY(atlevel_add_sub_adatab_node_a1_a & !fir_min_abs_alcarry_a1_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a # !atlevel_add_sub_adatab_node_a1_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a # !fir_min_abs_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a1_a,
	datab => fir_min_abs_alcarry_a1_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a = DFFE(atlevel_add_sub_adatab_node_a2_a $ fir_min_abs_alcarry_a2_a $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a = CARRY(atlevel_add_sub_adatab_node_a2_a & (fir_min_abs_alcarry_a2_a # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a) # !atlevel_add_sub_adatab_node_a2_a & fir_min_abs_alcarry_a2_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a2_a,
	datab => fir_min_abs_alcarry_a2_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a = DFFE(atlevel_add_sub_adatab_node_a3_a $ fir_min_abs_alcarry_a3_a $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a = CARRY(atlevel_add_sub_adatab_node_a3_a & !fir_min_abs_alcarry_a3_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a # !atlevel_add_sub_adatab_node_a3_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a # !fir_min_abs_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a3_a,
	datab => fir_min_abs_alcarry_a3_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a = DFFE(atlevel_add_sub_adatab_node_a4_a $ fir_min_abs_alcarry_a4_a $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a = CARRY(atlevel_add_sub_adatab_node_a4_a & (fir_min_abs_alcarry_a4_a # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a) # !atlevel_add_sub_adatab_node_a4_a & fir_min_abs_alcarry_a4_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a4_a,
	datab => fir_min_abs_alcarry_a4_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a = DFFE(atlevel_add_sub_adatab_node_a5_a $ fir_min_abs_alcarry_a5_a $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a = CARRY(atlevel_add_sub_adatab_node_a5_a & !fir_min_abs_alcarry_a5_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a # !atlevel_add_sub_adatab_node_a5_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a # !fir_min_abs_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a5_a,
	datab => fir_min_abs_alcarry_a5_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a = DFFE(atlevel_add_sub_adatab_node_a6_a $ fir_min_abs_alcarry_a6_a $ !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a = CARRY(atlevel_add_sub_adatab_node_a6_a & (fir_min_abs_alcarry_a6_a # !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a) # !atlevel_add_sub_adatab_node_a6_a & fir_min_abs_alcarry_a6_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a6_a,
	datab => fir_min_abs_alcarry_a6_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a = DFFE(atlevel_add_sub_adatab_node_a7_a $ fir_min_abs_alcarry_a7_a $ atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a = CARRY(atlevel_add_sub_adatab_node_a7_a & !fir_min_abs_alcarry_a7_a & !atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a # !atlevel_add_sub_adatab_node_a7_a & (!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a # !fir_min_abs_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_adatab_node_a7_a,
	datab => fir_min_abs_alcarry_a7_a,
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cout => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a);

atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a = DFFE(!atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	cin => atlevel_add_sub_aadder1_a0_a_aresult_node_acout_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a = atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a = CARRY(atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a & atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a,
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a $ atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a = CARRY(!atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a # !atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a = atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a $ !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a = CARRY(atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a & !atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a,
	cout => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a);

atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a = atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a $ atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => atlevel_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a,
	cin => atlevel_add_sub_aadder1_a1_a_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a);

atlevel_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(15));

atlevel_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(14));

atlevel_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(13));

atlevel_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(12));

atlevel_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(11));

atlevel_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(10));

atlevel_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(9));

atlevel_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(8));

atlevel_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(7));

atlevel_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(6));

atlevel_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(5));

atlevel_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(4));

atlevel_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(3));

atlevel_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(2));

atlevel_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(1));

atlevel_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => atlevel_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_atlevel(0));
END structure;


