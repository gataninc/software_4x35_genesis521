# vcom -reportprogress 300 -work work {disc.vhd}
vcom -reportprogress 300 -work work {fir_auto_disc.vho}
vcom -reportprogress 300 -work work {tb.vhd}
vsim -sdftyp /U=fir_auto_disc_vhd.sdo work.tb
do wave.do
run 300us