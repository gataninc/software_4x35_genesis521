-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_adisc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module monitors the filtered data ( High, Medium, Low, BLM )
--		and attempts to identify the threshold level
--		by looking for the most negative peaks, and applying the abs function
--	History
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_Bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal ADisc_en	: std_logic := '0';
	signal tlevel		: std_logic_vector( 15 downto 0 );
	signal fir_out		: std_logic_vector( 15 downto 0 );
	signal atlevel		: std_logic_vector( 15 downto 0 );
	signal iatlevel	: std_logic_vector( 15 downto 0 );
	signal decay		: std_logic_vector( 15 downto 0 );
	-------------------------------------------------------------------------------
	component fir_auto_Disc
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			ADisc_En		: in		std_logic;
			Decay		: in		std_logic_Vector( 15 downto 0 );
			tlevel		: in 	std_logic_Vector( 15 downto 0 );	-- User Threshold Lavel
			fir_out		: in 	std_logic_Vector( 15 downto 0 );	-- FIR Output 
			atlevel		: out	std_logic_vector( 15 downto 0 ) );	-- AUto Threshold Level
	end component fir_auto_disc;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component Disc 
	     port( 
	          imr : in std_logic;
     	     clk : in std_logic;
          	dout : out std_logic_Vector( 15 downto 0 ) );
	end component Disc;
	-------------------------------------------------------------------------------

begin
	imr 		<= '0' after 624 ns;
	clk		<= not( clk ) after 25 ns;
	decay	<= x"0100";

	d : disc
		port map(
			imr		=> imr,
			clk		=> clk,
			dout		=> fir_out );

	U : fir_auto_disc 
		port map(
			imr		=> imr,
			clk		=> clk,
			ADisc_En	=> ADisc_En,
			decay	=> decay,
			fir_out	=> fir_out,
			tlevel	=> tlevel,
			atlevel	=> iatlevel );


	tlevel_proc : process
	
	begin
		tlevel <= x"0000";
		ADisc_EN	<= '0';
		wait for 1 us;
		for i in 0 to 15 loop
			wait until (( clk'event ) and ( clk = '1' ));
			tlevel(i) <= '1';
			wait until (( clk'event ) and ( clk = '1' ));
			wait until (( clk'event ) and ( clk = '1' ));
			wait until (( clk'event ) and ( clk = '1' ));
			tlevel(i) <= '0';
			wait until (( clk'event ) and ( clk = '1' ));
			wait until (( clk'event ) and ( clk = '1' ));
			wait until (( clk'event ) and ( clk = '1' ));
		end loop;
		tlevel 	<= x"000A";
		ADisc_En 	<= '1';
		wait ;
			
				
	end process;
    --------------------------------------------------------------------------------
	clk_proc : process( clk )
	begin
		if(( clk'event ) and ( clk = '0' )) then
			atlevel	<= iatlevel;
		end if;
	end process;
    --------------------------------------------------------------------------------
	
------------------------------------------------------------------------------------
end test_Bench;               -- fir_adisc
------------------------------------------------------------------------------------

