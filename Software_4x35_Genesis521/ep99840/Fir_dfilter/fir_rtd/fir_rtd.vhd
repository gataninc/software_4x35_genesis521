-------------------------------------------------------------------------------
-- EDAX Inc
-- 91 McKee Drive
-- Mahwah, NJ 07430
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
     use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
     use ieee.std_logic_arith.all;

entity fir_rtd is
	port(
		imr		: in		std_logic;
		clk		: in		std_logic;
		din		: in		std_logic_vector( 15 downto 0 );
		rt_en	: buffer	std_logic;
		rt_done	: buffer	std_logic;
		rt_pur	: buffer	std_logic;
		rtime	: buffer	std_logic_vector( 11 downto 0 ) );
end fir_rtd;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_rtd is
	constant Max_RTime	: integer := 600 / 50;

	signal rt_en_del	: std_logic;
	signal din_del1 	: std_logic_vector( 15 downto 0 );
	signal din_del2 	: std_logic_vector( 15 downto 0 );
	signal rtime_Cnt	: std_logic_Vector( 11 downto 0 );
	signal pslope		: std_logic;
	signal pslope_Vec	: std_Logic_Vector( 2 downto 0 );
	signal Din_Diff	: std_logic_vector( 15 downto 0 );
	
	signal Max_Cnt		: integer range 0 to Max_Rtime;
	signal Max_Cnt_Tc	: std_logic;
	signal Max_Cnt_En	: std_logic;
	
begin
	Din_Diff	<= Din - Din_Del2;
	Max_Cnt_Tc <= '1' when ( Max_Cnt = Max_RTime ) else '0';

	pslope <= '1' when (( Din >= Din_Del2 ) and ( Din_Diff > 40 )) else '0';
     --------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			rt_en		<= '0';
			rt_en_del		<= '0';
			rt_done		<= '0';
			rt_pur		<= '0';
			din_del1		<= x"0000";
			din_del2		<= x"0000";
			rtime_Cnt		<= x"000";
			rtime		<= x"000";
			pslope_Vec	<= "000";
		elsif(( clk'event ) and ( clk = '1' )) then
			din_del1 	<= din;
			din_del2	<= din_del1;
		
			rt_en_del	<= rt_en;
			Pslope_Vec <= Pslope_Vec( 1 downto 0) & pslope;
		
		
			if( conv_integer( pslope_Vec( 1 downto 0 ) ) = 0 )
				then rt_en <= '0';
				else rt_en <= '1';
			end if;
			
			if( rt_en = '0' )
				then rtime_Cnt <= x"000";
				else rtime_Cnt <= rtime_cnt + 1;
			end if;
			
			if(( rt_en = '0' ) and ( rt_en_del = '1' )) 
				then rtime <= rtime_cnt;
			end if;	
			
			if(( rt_en = '0' ) and ( rt_en_Del = '1' ) and ( Max_Cnt_en = '1' ))
				then rt_done <= '1';
				else rt_done <= '0';
			end if;
			
			if(( rt_en = '0' ) and ( rt_en_del = '1' ) and ( max_Cnt_en = '0' ))
				then rt_pur <= '1';
				else rt_pur <= '0';
			end if;
			
			-- Logic to Limit Max Risetime
			if(( rt_en = '1' ) and( Rt_en_del = '0' ))
				then Max_Cnt_En <= '1';
			elsif( max_Cnt_Tc = '1' )
				then Max_CNt_En <= '0';
			end if;
			
			if(( rt_en = '1' ) and ( Rt_En_Del = '0' ))
				then Max_Cnt <= 0;
			elsif( Max_Cnt_En = '1' )
				then Max_Cnt <= Max_Cnt + 1;
			end if;
		end if;
	end process;
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end behavioral;      -- fir_rtd
-------------------------------------------------------------------------------
