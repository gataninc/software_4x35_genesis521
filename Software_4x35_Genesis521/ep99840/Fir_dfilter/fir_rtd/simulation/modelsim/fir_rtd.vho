-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "06/01/2006 15:02:11"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_rtd IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	din : IN std_logic_vector(15 DOWNTO 0);
	rt_en : OUT std_logic;
	rt_done : OUT std_logic;
	rt_pur : OUT std_logic;
	rtime : OUT std_logic_vector(11 DOWNTO 0)
	);
END fir_rtd;

ARCHITECTURE structure OF fir_rtd IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_rt_en : std_logic;
SIGNAL ww_rt_done : std_logic;
SIGNAL ww_rt_pur : std_logic;
SIGNAL ww_rtime : std_logic_vector(11 DOWNTO 0);
SIGNAL \add~604\ : std_logic;
SIGNAL \add~608\ : std_logic;
SIGNAL \add~628\ : std_logic;
SIGNAL \add~632\ : std_logic;
SIGNAL \add~636\ : std_logic;
SIGNAL \LessThan~520\ : std_logic;
SIGNAL \add~640\ : std_logic;
SIGNAL \Max_Cnt[2]\ : std_logic;
SIGNAL \din_del2[8]\ : std_logic;
SIGNAL \din_del2[9]\ : std_logic;
SIGNAL \din_del2[10]\ : std_logic;
SIGNAL \din_del2[2]\ : std_logic;
SIGNAL \din_del2[12]\ : std_logic;
SIGNAL \din_del2[13]\ : std_logic;
SIGNAL \din_del2[15]\ : std_logic;
SIGNAL \LessThan~527\ : std_logic;
SIGNAL \din_del1[8]\ : std_logic;
SIGNAL \din_del1[9]\ : std_logic;
SIGNAL \din_del1[10]\ : std_logic;
SIGNAL \din_del1[2]\ : std_logic;
SIGNAL \din_del1[12]\ : std_logic;
SIGNAL \din_del1[13]\ : std_logic;
SIGNAL \din_del1[15]\ : std_logic;
SIGNAL \LessThan~531\ : std_logic;
SIGNAL \LessThan~535\ : std_logic;
SIGNAL \LessThan~539\ : std_logic;
SIGNAL \LessThan~543\ : std_logic;
SIGNAL \LessThan~547\ : std_logic;
SIGNAL \LessThan~551\ : std_logic;
SIGNAL \LessThan~555\ : std_logic;
SIGNAL \LessThan~559\ : std_logic;
SIGNAL \LessThan~563\ : std_logic;
SIGNAL \LessThan~567\ : std_logic;
SIGNAL \LessThan~571\ : std_logic;
SIGNAL \LessThan~575\ : std_logic;
SIGNAL \LessThan~579\ : std_logic;
SIGNAL \LessThan~583\ : std_logic;
SIGNAL \din[15]~combout\ : std_logic;
SIGNAL \din[14]~combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \imr~combout\ : std_logic;
SIGNAL \din_del1[14]\ : std_logic;
SIGNAL \din_del2[14]\ : std_logic;
SIGNAL \din[13]~combout\ : std_logic;
SIGNAL \din[12]~combout\ : std_logic;
SIGNAL \din[11]~combout\ : std_logic;
SIGNAL \din_del1[11]\ : std_logic;
SIGNAL \din_del2[11]\ : std_logic;
SIGNAL \din[10]~combout\ : std_logic;
SIGNAL \din[9]~combout\ : std_logic;
SIGNAL \din[8]~combout\ : std_logic;
SIGNAL \din[7]~combout\ : std_logic;
SIGNAL \din_del1[7]\ : std_logic;
SIGNAL \din_del2[7]\ : std_logic;
SIGNAL \din[6]~combout\ : std_logic;
SIGNAL \din_del1[6]\ : std_logic;
SIGNAL \din_del2[6]\ : std_logic;
SIGNAL \din[5]~combout\ : std_logic;
SIGNAL \din_del1[5]\ : std_logic;
SIGNAL \din_del2[5]\ : std_logic;
SIGNAL \din[4]~combout\ : std_logic;
SIGNAL \din_del1[4]\ : std_logic;
SIGNAL \din_del2[4]\ : std_logic;
SIGNAL \din[3]~combout\ : std_logic;
SIGNAL \din_del1[3]\ : std_logic;
SIGNAL \din_del2[3]\ : std_logic;
SIGNAL \din[2]~combout\ : std_logic;
SIGNAL \din[1]~combout\ : std_logic;
SIGNAL \din[0]~combout\ : std_logic;
SIGNAL \din_del1[0]\ : std_logic;
SIGNAL \din_del2[0]\ : std_logic;
SIGNAL \LessThan~585\ : std_logic;
SIGNAL \LessThan~581\ : std_logic;
SIGNAL \LessThan~577\ : std_logic;
SIGNAL \LessThan~573\ : std_logic;
SIGNAL \LessThan~569\ : std_logic;
SIGNAL \LessThan~565\ : std_logic;
SIGNAL \LessThan~561\ : std_logic;
SIGNAL \LessThan~557\ : std_logic;
SIGNAL \LessThan~553\ : std_logic;
SIGNAL \LessThan~549\ : std_logic;
SIGNAL \LessThan~545\ : std_logic;
SIGNAL \LessThan~541\ : std_logic;
SIGNAL \LessThan~537\ : std_logic;
SIGNAL \LessThan~533\ : std_logic;
SIGNAL \LessThan~529\ : std_logic;
SIGNAL \LessThan~523\ : std_logic;
SIGNAL \din_del1[1]\ : std_logic;
SIGNAL \din_del2[1]\ : std_logic;
SIGNAL \add~630\ : std_logic;
SIGNAL \add~634\ : std_logic;
SIGNAL \add~638\ : std_logic;
SIGNAL \add~626\ : std_logic;
SIGNAL \add~606\ : std_logic;
SIGNAL \add~602\ : std_logic;
SIGNAL \add~594\ : std_logic;
SIGNAL \add~598\ : std_logic;
SIGNAL \add~610\ : std_logic;
SIGNAL \add~614\ : std_logic;
SIGNAL \add~618\ : std_logic;
SIGNAL \add~622\ : std_logic;
SIGNAL \add~642\ : std_logic;
SIGNAL \add~644\ : std_logic;
SIGNAL \add~646\ : std_logic;
SIGNAL \add~648\ : std_logic;
SIGNAL \add~650\ : std_logic;
SIGNAL \add~652\ : std_logic;
SIGNAL \LessThan~522\ : std_logic;
SIGNAL \add~624\ : std_logic;
SIGNAL \add~600\ : std_logic;
SIGNAL \add~592\ : std_logic;
SIGNAL \add~596\ : std_logic;
SIGNAL \LessThan~518\ : std_logic;
SIGNAL \add~620\ : std_logic;
SIGNAL \add~612\ : std_logic;
SIGNAL \add~616\ : std_logic;
SIGNAL \LessThan~519\ : std_logic;
SIGNAL \LessThan~521\ : std_logic;
SIGNAL \pslope_Vec[0]\ : std_logic;
SIGNAL \pslope_Vec[1]\ : std_logic;
SIGNAL \rt_en~reg0\ : std_logic;
SIGNAL rt_en_del : std_logic;
SIGNAL \clock_proc~3\ : std_logic;
SIGNAL \Max_Cnt[0]~57\ : std_logic;
SIGNAL \Max_Cnt[1]\ : std_logic;
SIGNAL \Max_Cnt[0]\ : std_logic;
SIGNAL \Max_Cnt[1]~60\ : std_logic;
SIGNAL \Max_Cnt[2]~63\ : std_logic;
SIGNAL \Max_Cnt[3]\ : std_logic;
SIGNAL \Max_Cnt_En~281\ : std_logic;
SIGNAL Max_Cnt_En : std_logic;
SIGNAL \rt_done~reg0\ : std_logic;
SIGNAL \rt_pur~reg0\ : std_logic;
SIGNAL \rtime_Cnt[0]\ : std_logic;
SIGNAL \clock_proc~0\ : std_logic;
SIGNAL \rtime[0]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[0]~97\ : std_logic;
SIGNAL \rtime_Cnt[1]\ : std_logic;
SIGNAL \rtime[1]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[1]~100\ : std_logic;
SIGNAL \rtime_Cnt[2]\ : std_logic;
SIGNAL \rtime[2]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[2]~103\ : std_logic;
SIGNAL \rtime_Cnt[3]\ : std_logic;
SIGNAL \rtime[3]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[3]~106\ : std_logic;
SIGNAL \rtime_Cnt[4]\ : std_logic;
SIGNAL \rtime[4]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[4]~109\ : std_logic;
SIGNAL \rtime_Cnt[5]\ : std_logic;
SIGNAL \rtime[5]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[5]~112\ : std_logic;
SIGNAL \rtime_Cnt[6]\ : std_logic;
SIGNAL \rtime[6]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[6]~115\ : std_logic;
SIGNAL \rtime_Cnt[7]\ : std_logic;
SIGNAL \rtime[7]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[7]~118\ : std_logic;
SIGNAL \rtime_Cnt[8]\ : std_logic;
SIGNAL \rtime[8]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[8]~121\ : std_logic;
SIGNAL \rtime_Cnt[9]\ : std_logic;
SIGNAL \rtime[9]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[9]~124\ : std_logic;
SIGNAL \rtime_Cnt[10]\ : std_logic;
SIGNAL \rtime[10]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[10]~127\ : std_logic;
SIGNAL \rtime_Cnt[11]\ : std_logic;
SIGNAL \rtime[11]~reg0\ : std_logic;
SIGNAL \ALT_INV_rt_en~reg0\ : std_logic;
SIGNAL \ALT_INV_imr~combout\ : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_din <= din;
rt_en <= ww_rt_en;
rt_done <= ww_rt_done;
rt_pur <= ww_rt_pur;
rtime <= ww_rtime;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_rt_en~reg0\ <= NOT \rt_en~reg0\;
\ALT_INV_imr~combout\ <= NOT \imr~combout\;

\add~604_I\ : apex20ke_lcell
-- Equation(s):
-- \add~604\ = \din_del2[4]\ $ \din[4]~combout\ $ \add~626\
-- \add~606\ = CARRY(\din_del2[4]\ & \din[4]~combout\ & !\add~626\ # !\din_del2[4]\ & (\din[4]~combout\ # !\add~626\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[4]\,
	datab => \din[4]~combout\,
	cin => \add~626\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~604\,
	cout => \add~606\);

\add~608_I\ : apex20ke_lcell
-- Equation(s):
-- \add~608\ = \din_del2[8]\ $ \din[8]~combout\ $ \add~598\
-- \add~610\ = CARRY(\din_del2[8]\ & \din[8]~combout\ & !\add~598\ # !\din_del2[8]\ & (\din[8]~combout\ # !\add~598\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[8]\,
	datab => \din[8]~combout\,
	cin => \add~598\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~608\,
	cout => \add~610\);

\add~628_I\ : apex20ke_lcell
-- Equation(s):
-- \add~628\ = \din[0]~combout\ $ \din_del2[0]\
-- \add~630\ = CARRY(\din[0]~combout\ # !\din_del2[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[0]~combout\,
	datab => \din_del2[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~628\,
	cout => \add~630\);

\add~632_I\ : apex20ke_lcell
-- Equation(s):
-- \add~632\ = \din[1]~combout\ $ \din_del2[1]\ $ !\add~630\
-- \add~634\ = CARRY(\din[1]~combout\ & \din_del2[1]\ & !\add~630\ # !\din[1]~combout\ & (\din_del2[1]\ # !\add~630\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[1]~combout\,
	datab => \din_del2[1]\,
	cin => \add~630\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~632\,
	cout => \add~634\);

\add~636_I\ : apex20ke_lcell
-- Equation(s):
-- \add~636\ = \din_del2[2]\ $ \din[2]~combout\ $ \add~634\
-- \add~638\ = CARRY(\din_del2[2]\ & \din[2]~combout\ & !\add~634\ # !\din_del2[2]\ & (\din[2]~combout\ # !\add~634\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[2]\,
	datab => \din[2]~combout\,
	cin => \add~634\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~636\,
	cout => \add~638\);

\LessThan~520_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~520\ = \add~600\ & (\add~632\ # \add~636\ # \add~628\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FE00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~632\,
	datab => \add~636\,
	datac => \add~628\,
	datad => \add~600\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~520\);

\add~640_I\ : apex20ke_lcell
-- Equation(s):
-- \add~640\ = \din_del2[12]\ $ \din[12]~combout\ $ \add~622\
-- \add~642\ = CARRY(\din_del2[12]\ & \din[12]~combout\ & !\add~622\ # !\din_del2[12]\ & (\din[12]~combout\ # !\add~622\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[12]\,
	datab => \din[12]~combout\,
	cin => \add~622\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~640\,
	cout => \add~642\);

\Max_Cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Cnt[2]\ = DFFE(!GLOBAL(\clock_proc~3\) & \Max_Cnt[2]\ $ (!\Max_Cnt[1]~60\), GLOBAL(\clk~combout\), , , !\imr~combout\)
-- \Max_Cnt[2]~63\ = CARRY(\Max_Cnt[2]\ & (!\Max_Cnt[1]~60\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Cnt[2]\,
	cin => \Max_Cnt[1]~60\,
	clk => \clk~combout\,
	ena => \ALT_INV_imr~combout\,
	sclr => \clock_proc~3\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Cnt[2]\,
	cout => \Max_Cnt[2]~63\);

\din_del2[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[8]\ = DFFE(\din_del1[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[8]\);

\din_del2[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[9]\ = DFFE(\din_del1[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[9]\);

\din_del2[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[10]\ = DFFE(\din_del1[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[10]\);

\din_del2[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[2]\ = DFFE(\din_del1[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[2]\);

\din_del2[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[12]\ = DFFE(\din_del1[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[12]\);

\din_del2[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[13]\ = DFFE(\din_del1[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[13]\);

\din_del2[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[15]\ = DFFE(\din_del1[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[15]\);

\din_del1[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[8]\ = DFFE(\din[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[8]\);

\din_del1[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[9]\ = DFFE(\din[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[9]\);

\din_del1[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[10]\ = DFFE(\din[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[10]\);

\din_del1[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[2]\ = DFFE(\din[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[2]\);

\din_del1[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[12]\ = DFFE(\din[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[12]\);

\din_del1[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[13]\ = DFFE(\din[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[13]\);

\din_del1[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[15]\ = DFFE(\din[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[15]\);

\din[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(15),
	combout => \din[15]~combout\);

\din[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(14),
	combout => \din[14]~combout\);

\clk~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => \clk~combout\);

\imr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => \imr~combout\);

\din_del1[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[14]\ = DFFE(\din[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[14]\);

\din_del2[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[14]\ = DFFE(\din_del1[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[14]\);

\din[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(13),
	combout => \din[13]~combout\);

\din[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(12),
	combout => \din[12]~combout\);

\din[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(11),
	combout => \din[11]~combout\);

\din_del1[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[11]\ = DFFE(\din[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[11]\);

\din_del2[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[11]\ = DFFE(\din_del1[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[11]\);

\din[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(10),
	combout => \din[10]~combout\);

\din[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(9),
	combout => \din[9]~combout\);

\din[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(8),
	combout => \din[8]~combout\);

\din[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(7),
	combout => \din[7]~combout\);

\din_del1[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[7]\ = DFFE(\din[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[7]\);

\din_del2[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[7]\ = DFFE(\din_del1[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[7]\);

\din[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(6),
	combout => \din[6]~combout\);

\din_del1[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[6]\ = DFFE(\din[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[6]\);

\din_del2[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[6]\ = DFFE(\din_del1[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[6]\);

\din[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(5),
	combout => \din[5]~combout\);

\din_del1[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[5]\ = DFFE(\din[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[5]\);

\din_del2[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[5]\ = DFFE(\din_del1[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[5]\);

\din[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(4),
	combout => \din[4]~combout\);

\din_del1[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[4]\ = DFFE(\din[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[4]\);

\din_del2[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[4]\ = DFFE(\din_del1[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[4]\);

\din[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(3),
	combout => \din[3]~combout\);

\din_del1[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[3]\ = DFFE(\din[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[3]\);

\din_del2[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[3]\ = DFFE(\din_del1[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[3]\);

\din[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(2),
	combout => \din[2]~combout\);

\din[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(1),
	combout => \din[1]~combout\);

\din[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(0),
	combout => \din[0]~combout\);

\din_del1[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[0]\ = DFFE(\din[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[0]\);

\din_del2[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[0]\ = DFFE(\din_del1[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[0]\);

\LessThan~585_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~585\ = CARRY(!\din[0]~combout\ & \din_del2[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[0]~combout\,
	datab => \din_del2[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~583\,
	cout => \LessThan~585\);

\LessThan~581_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~581\ = CARRY(\din_del2[1]\ & \din[1]~combout\ & !\LessThan~585\ # !\din_del2[1]\ & (\din[1]~combout\ # !\LessThan~585\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[1]\,
	datab => \din[1]~combout\,
	cin => \LessThan~585\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~579\,
	cout => \LessThan~581\);

\LessThan~577_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~577\ = CARRY(\din_del2[2]\ & (!\LessThan~581\ # !\din[2]~combout\) # !\din_del2[2]\ & !\din[2]~combout\ & !\LessThan~581\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[2]\,
	datab => \din[2]~combout\,
	cin => \LessThan~581\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~575\,
	cout => \LessThan~577\);

\LessThan~573_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~573\ = CARRY(\din[3]~combout\ & (!\LessThan~577\ # !\din_del2[3]\) # !\din[3]~combout\ & !\din_del2[3]\ & !\LessThan~577\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[3]~combout\,
	datab => \din_del2[3]\,
	cin => \LessThan~577\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~571\,
	cout => \LessThan~573\);

\LessThan~569_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~569\ = CARRY(\din[4]~combout\ & \din_del2[4]\ & !\LessThan~573\ # !\din[4]~combout\ & (\din_del2[4]\ # !\LessThan~573\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[4]~combout\,
	datab => \din_del2[4]\,
	cin => \LessThan~573\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~567\,
	cout => \LessThan~569\);

\LessThan~565_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~565\ = CARRY(\din[5]~combout\ & (!\LessThan~569\ # !\din_del2[5]\) # !\din[5]~combout\ & !\din_del2[5]\ & !\LessThan~569\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[5]~combout\,
	datab => \din_del2[5]\,
	cin => \LessThan~569\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~563\,
	cout => \LessThan~565\);

\LessThan~561_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~561\ = CARRY(\din[6]~combout\ & \din_del2[6]\ & !\LessThan~565\ # !\din[6]~combout\ & (\din_del2[6]\ # !\LessThan~565\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[6]~combout\,
	datab => \din_del2[6]\,
	cin => \LessThan~565\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~559\,
	cout => \LessThan~561\);

\LessThan~557_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~557\ = CARRY(\din[7]~combout\ & (!\LessThan~561\ # !\din_del2[7]\) # !\din[7]~combout\ & !\din_del2[7]\ & !\LessThan~561\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[7]~combout\,
	datab => \din_del2[7]\,
	cin => \LessThan~561\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~555\,
	cout => \LessThan~557\);

\LessThan~553_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~553\ = CARRY(\din_del2[8]\ & (!\LessThan~557\ # !\din[8]~combout\) # !\din_del2[8]\ & !\din[8]~combout\ & !\LessThan~557\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[8]\,
	datab => \din[8]~combout\,
	cin => \LessThan~557\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~551\,
	cout => \LessThan~553\);

\LessThan~549_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~549\ = CARRY(\din_del2[9]\ & \din[9]~combout\ & !\LessThan~553\ # !\din_del2[9]\ & (\din[9]~combout\ # !\LessThan~553\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[9]\,
	datab => \din[9]~combout\,
	cin => \LessThan~553\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~547\,
	cout => \LessThan~549\);

\LessThan~545_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~545\ = CARRY(\din_del2[10]\ & (!\LessThan~549\ # !\din[10]~combout\) # !\din_del2[10]\ & !\din[10]~combout\ & !\LessThan~549\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[10]\,
	datab => \din[10]~combout\,
	cin => \LessThan~549\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~543\,
	cout => \LessThan~545\);

\LessThan~541_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~541\ = CARRY(\din[11]~combout\ & (!\LessThan~545\ # !\din_del2[11]\) # !\din[11]~combout\ & !\din_del2[11]\ & !\LessThan~545\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[11]~combout\,
	datab => \din_del2[11]\,
	cin => \LessThan~545\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~539\,
	cout => \LessThan~541\);

\LessThan~537_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~537\ = CARRY(\din_del2[12]\ & (!\LessThan~541\ # !\din[12]~combout\) # !\din_del2[12]\ & !\din[12]~combout\ & !\LessThan~541\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[12]\,
	datab => \din[12]~combout\,
	cin => \LessThan~541\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~535\,
	cout => \LessThan~537\);

\LessThan~533_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~533\ = CARRY(\din_del2[13]\ & \din[13]~combout\ & !\LessThan~537\ # !\din_del2[13]\ & (\din[13]~combout\ # !\LessThan~537\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[13]\,
	datab => \din[13]~combout\,
	cin => \LessThan~537\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~531\,
	cout => \LessThan~533\);

\LessThan~529_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~529\ = CARRY(\din[14]~combout\ & \din_del2[14]\ & !\LessThan~533\ # !\din[14]~combout\ & (\din_del2[14]\ # !\LessThan~533\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[14]~combout\,
	datab => \din_del2[14]\,
	cin => \LessThan~533\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~527\,
	cout => \LessThan~529\);

\LessThan~523_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~523\ = \din_del2[15]\ & (\LessThan~529\ # !\din[15]~combout\) # !\din_del2[15]\ & (\LessThan~529\ & !\din[15]~combout\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A0FA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[15]\,
	datad => \din[15]~combout\,
	cin => \LessThan~529\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~523\);

\din_del1[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[1]\ = DFFE(\din[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[1]\);

\din_del2[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[1]\ = DFFE(\din_del1[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[1]\);

\add~624_I\ : apex20ke_lcell
-- Equation(s):
-- \add~624\ = \din_del2[3]\ $ \din[3]~combout\ $ !\add~638\
-- \add~626\ = CARRY(\din_del2[3]\ & (!\add~638\ # !\din[3]~combout\) # !\din_del2[3]\ & !\din[3]~combout\ & !\add~638\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[3]\,
	datab => \din[3]~combout\,
	cin => \add~638\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~624\,
	cout => \add~626\);

\add~600_I\ : apex20ke_lcell
-- Equation(s):
-- \add~600\ = \din[5]~combout\ $ \din_del2[5]\ $ !\add~606\
-- \add~602\ = CARRY(\din[5]~combout\ & \din_del2[5]\ & !\add~606\ # !\din[5]~combout\ & (\din_del2[5]\ # !\add~606\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[5]~combout\,
	datab => \din_del2[5]\,
	cin => \add~606\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~600\,
	cout => \add~602\);

\add~592_I\ : apex20ke_lcell
-- Equation(s):
-- \add~592\ = \din[6]~combout\ $ \din_del2[6]\ $ \add~602\
-- \add~594\ = CARRY(\din[6]~combout\ & (!\add~602\ # !\din_del2[6]\) # !\din[6]~combout\ & !\din_del2[6]\ & !\add~602\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[6]~combout\,
	datab => \din_del2[6]\,
	cin => \add~602\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~592\,
	cout => \add~594\);

\add~596_I\ : apex20ke_lcell
-- Equation(s):
-- \add~596\ = \din[7]~combout\ $ \din_del2[7]\ $ !\add~594\
-- \add~598\ = CARRY(\din[7]~combout\ & \din_del2[7]\ & !\add~594\ # !\din[7]~combout\ & (\din_del2[7]\ # !\add~594\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[7]~combout\,
	datab => \din_del2[7]\,
	cin => \add~594\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~596\,
	cout => \add~598\);

\add~612_I\ : apex20ke_lcell
-- Equation(s):
-- \add~612\ = \din_del2[9]\ $ \din[9]~combout\ $ !\add~610\
-- \add~614\ = CARRY(\din_del2[9]\ & (!\add~610\ # !\din[9]~combout\) # !\din_del2[9]\ & !\din[9]~combout\ & !\add~610\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[9]\,
	datab => \din[9]~combout\,
	cin => \add~610\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~612\,
	cout => \add~614\);

\add~616_I\ : apex20ke_lcell
-- Equation(s):
-- \add~616\ = \din_del2[10]\ $ \din[10]~combout\ $ \add~614\
-- \add~618\ = CARRY(\din_del2[10]\ & \din[10]~combout\ & !\add~614\ # !\din_del2[10]\ & (\din[10]~combout\ # !\add~614\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[10]\,
	datab => \din[10]~combout\,
	cin => \add~614\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~616\,
	cout => \add~618\);

\add~620_I\ : apex20ke_lcell
-- Equation(s):
-- \add~620\ = \din_del2[11]\ $ \din[11]~combout\ $ !\add~618\
-- \add~622\ = CARRY(\din_del2[11]\ & (!\add~618\ # !\din[11]~combout\) # !\din_del2[11]\ & !\din[11]~combout\ & !\add~618\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[11]\,
	datab => \din[11]~combout\,
	cin => \add~618\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~620\,
	cout => \add~622\);

\add~644_I\ : apex20ke_lcell
-- Equation(s):
-- \add~644\ = \din_del2[13]\ $ \din[13]~combout\ $ !\add~642\
-- \add~646\ = CARRY(\din_del2[13]\ & (!\add~642\ # !\din[13]~combout\) # !\din_del2[13]\ & !\din[13]~combout\ & !\add~642\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[13]\,
	datab => \din[13]~combout\,
	cin => \add~642\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~644\,
	cout => \add~646\);

\add~648_I\ : apex20ke_lcell
-- Equation(s):
-- \add~648\ = \din_del2[14]\ $ \din[14]~combout\ $ \add~646\
-- \add~650\ = CARRY(\din_del2[14]\ & \din[14]~combout\ & !\add~646\ # !\din_del2[14]\ & (\din[14]~combout\ # !\add~646\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[14]\,
	datab => \din[14]~combout\,
	cin => \add~646\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~648\,
	cout => \add~650\);

\add~652_I\ : apex20ke_lcell
-- Equation(s):
-- \add~652\ = \din_del2[15]\ $ (\add~650\ $ !\din[15]~combout\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[15]\,
	datad => \din[15]~combout\,
	cin => \add~650\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~652\);

\LessThan~522_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~522\ = \add~640\ # \add~644\ # \add~648\ # \add~652\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~640\,
	datab => \add~644\,
	datac => \add~648\,
	datad => \add~652\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~522\);

\LessThan~518_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~518\ = \add~592\ # \add~596\ # \add~604\ & \add~600\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~604\,
	datab => \add~600\,
	datac => \add~592\,
	datad => \add~596\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~518\);

\LessThan~519_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~519\ = \add~608\ # \add~620\ # \add~612\ # \add~616\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~608\,
	datab => \add~620\,
	datac => \add~612\,
	datad => \add~616\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~519\);

\LessThan~521_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~521\ = \LessThan~518\ # \LessThan~519\ # \LessThan~520\ & \add~624\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \LessThan~520\,
	datab => \add~624\,
	datac => \LessThan~518\,
	datad => \LessThan~519\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~521\);

\pslope_Vec[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \pslope_Vec[0]\ = DFFE(!\LessThan~523\ & (\LessThan~522\ # \LessThan~521\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3330",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \LessThan~523\,
	datac => \LessThan~522\,
	datad => \LessThan~521\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pslope_Vec[0]\);

\pslope_Vec[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \pslope_Vec[1]\ = DFFE(\pslope_Vec[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \pslope_Vec[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pslope_Vec[1]\);

\rt_en~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rt_en~reg0\ = DFFE(\pslope_Vec[0]\ # \pslope_Vec[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \pslope_Vec[0]\,
	datad => \pslope_Vec[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rt_en~reg0\);

\rt_en_del~I\ : apex20ke_lcell
-- Equation(s):
-- rt_en_del = DFFE(\rt_en~reg0\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rt_en~reg0\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rt_en_del);

\clock_proc~3_I\ : apex20ke_lcell
-- Equation(s):
-- \clock_proc~3\ = \rt_en~reg0\ & !rt_en_del

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rt_en~reg0\,
	datad => rt_en_del,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clock_proc~3\);

\Max_Cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Cnt[0]\ = DFFE(!GLOBAL(\clock_proc~3\) & \Max_Cnt[0]\ $ Max_Cnt_En, GLOBAL(\clk~combout\), , , !\imr~combout\)
-- \Max_Cnt[0]~57\ = CARRY(\Max_Cnt[0]\ & Max_Cnt_En)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Cnt[0]\,
	datab => Max_Cnt_En,
	clk => \clk~combout\,
	ena => \ALT_INV_imr~combout\,
	sclr => \clock_proc~3\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Cnt[0]\,
	cout => \Max_Cnt[0]~57\);

\Max_Cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Cnt[1]\ = DFFE(!GLOBAL(\clock_proc~3\) & \Max_Cnt[1]\ $ \Max_Cnt[0]~57\, GLOBAL(\clk~combout\), , , !\imr~combout\)
-- \Max_Cnt[1]~60\ = CARRY(!\Max_Cnt[0]~57\ # !\Max_Cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Max_Cnt[1]\,
	cin => \Max_Cnt[0]~57\,
	clk => \clk~combout\,
	ena => \ALT_INV_imr~combout\,
	sclr => \clock_proc~3\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Cnt[1]\,
	cout => \Max_Cnt[1]~60\);

\Max_Cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Cnt[3]\ = DFFE(!GLOBAL(\clock_proc~3\) & \Max_Cnt[2]~63\ $ \Max_Cnt[3]\, GLOBAL(\clk~combout\), , , !\imr~combout\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Max_Cnt[3]\,
	cin => \Max_Cnt[2]~63\,
	clk => \clk~combout\,
	ena => \ALT_INV_imr~combout\,
	sclr => \clock_proc~3\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Max_Cnt[3]\);

\Max_Cnt_En~281_I\ : apex20ke_lcell
-- Equation(s):
-- \Max_Cnt_En~281\ = \Max_Cnt[1]\ # \Max_Cnt[0]\ # !\Max_Cnt[3]\ # !\Max_Cnt[2]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FDFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Max_Cnt[2]\,
	datab => \Max_Cnt[1]\,
	datac => \Max_Cnt[0]\,
	datad => \Max_Cnt[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Max_Cnt_En~281\);

\Max_Cnt_En~I\ : apex20ke_lcell
-- Equation(s):
-- Max_Cnt_En = DFFE(rt_en_del & (\Max_Cnt_En~281\ & Max_Cnt_En) # !rt_en_del & (\rt_en~reg0\ # \Max_Cnt_En~281\ & Max_Cnt_En), GLOBAL(\clk~combout\), , , !\imr~combout\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F444",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rt_en_del,
	datab => \rt_en~reg0\,
	datac => \Max_Cnt_En~281\,
	datad => Max_Cnt_En,
	clk => \clk~combout\,
	ena => \ALT_INV_imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Max_Cnt_En);

\rt_done~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rt_done~reg0\ = DFFE(!\rt_en~reg0\ & Max_Cnt_En & rt_en_del, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rt_en~reg0\,
	datac => Max_Cnt_En,
	datad => rt_en_del,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rt_done~reg0\);

\rt_pur~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rt_pur~reg0\ = DFFE(!\rt_en~reg0\ & rt_en_del & !Max_Cnt_En, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0030",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rt_en~reg0\,
	datac => rt_en_del,
	datad => Max_Cnt_En,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rt_pur~reg0\);

\rtime_Cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[0]\ = DFFE(GLOBAL(\rt_en~reg0\) & !\rtime_Cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[0]~97\ = CARRY(\rtime_Cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[0]\,
	cout => \rtime_Cnt[0]~97\);

\clock_proc~0_I\ : apex20ke_lcell
-- Equation(s):
-- \clock_proc~0\ = !\rt_en~reg0\ & rt_en_del

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rt_en~reg0\,
	datad => rt_en_del,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clock_proc~0\);

\rtime[0]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[0]~reg0\ = DFFE(\rtime_Cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[0]~reg0\);

\rtime_Cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[1]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[1]\ $ \rtime_Cnt[0]~97\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[1]~100\ = CARRY(!\rtime_Cnt[0]~97\ # !\rtime_Cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[1]\,
	cin => \rtime_Cnt[0]~97\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[1]\,
	cout => \rtime_Cnt[1]~100\);

\rtime[1]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[1]~reg0\ = DFFE(\rtime_Cnt[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[1]~reg0\);

\rtime_Cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[2]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[2]\ $ (!\rtime_Cnt[1]~100\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[2]~103\ = CARRY(\rtime_Cnt[2]\ & (!\rtime_Cnt[1]~100\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \rtime_Cnt[2]\,
	cin => \rtime_Cnt[1]~100\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[2]\,
	cout => \rtime_Cnt[2]~103\);

\rtime[2]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[2]~reg0\ = DFFE(\rtime_Cnt[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[2]~reg0\);

\rtime_Cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[3]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[3]\ $ \rtime_Cnt[2]~103\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[3]~106\ = CARRY(!\rtime_Cnt[2]~103\ # !\rtime_Cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[3]\,
	cin => \rtime_Cnt[2]~103\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[3]\,
	cout => \rtime_Cnt[3]~106\);

\rtime[3]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[3]~reg0\ = DFFE(\rtime_Cnt[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[3]~reg0\);

\rtime_Cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[4]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[4]\ $ (!\rtime_Cnt[3]~106\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[4]~109\ = CARRY(\rtime_Cnt[4]\ & (!\rtime_Cnt[3]~106\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \rtime_Cnt[4]\,
	cin => \rtime_Cnt[3]~106\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[4]\,
	cout => \rtime_Cnt[4]~109\);

\rtime[4]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[4]~reg0\ = DFFE(\rtime_Cnt[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[4]~reg0\);

\rtime_Cnt[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[5]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[5]\ $ \rtime_Cnt[4]~109\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[5]~112\ = CARRY(!\rtime_Cnt[4]~109\ # !\rtime_Cnt[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[5]\,
	cin => \rtime_Cnt[4]~109\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[5]\,
	cout => \rtime_Cnt[5]~112\);

\rtime[5]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[5]~reg0\ = DFFE(\rtime_Cnt[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[5]~reg0\);

\rtime_Cnt[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[6]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[6]\ $ !\rtime_Cnt[5]~112\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[6]~115\ = CARRY(\rtime_Cnt[6]\ & !\rtime_Cnt[5]~112\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[6]\,
	cin => \rtime_Cnt[5]~112\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[6]\,
	cout => \rtime_Cnt[6]~115\);

\rtime[6]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[6]~reg0\ = DFFE(\rtime_Cnt[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[6]~reg0\);

\rtime_Cnt[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[7]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[7]\ $ \rtime_Cnt[6]~115\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[7]~118\ = CARRY(!\rtime_Cnt[6]~115\ # !\rtime_Cnt[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[7]\,
	cin => \rtime_Cnt[6]~115\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[7]\,
	cout => \rtime_Cnt[7]~118\);

\rtime[7]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[7]~reg0\ = DFFE(\rtime_Cnt[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[7]~reg0\);

\rtime_Cnt[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[8]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[8]\ $ (!\rtime_Cnt[7]~118\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[8]~121\ = CARRY(\rtime_Cnt[8]\ & (!\rtime_Cnt[7]~118\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \rtime_Cnt[8]\,
	cin => \rtime_Cnt[7]~118\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[8]\,
	cout => \rtime_Cnt[8]~121\);

\rtime[8]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[8]~reg0\ = DFFE(\rtime_Cnt[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[8]~reg0\);

\rtime_Cnt[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[9]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[9]\ $ (\rtime_Cnt[8]~121\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[9]~124\ = CARRY(!\rtime_Cnt[8]~121\ # !\rtime_Cnt[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \rtime_Cnt[9]\,
	cin => \rtime_Cnt[8]~121\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[9]\,
	cout => \rtime_Cnt[9]~124\);

\rtime[9]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[9]~reg0\ = DFFE(\rtime_Cnt[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[9]~reg0\);

\rtime_Cnt[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[10]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[10]\ $ !\rtime_Cnt[9]~124\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[10]~127\ = CARRY(\rtime_Cnt[10]\ & !\rtime_Cnt[9]~124\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[10]\,
	cin => \rtime_Cnt[9]~124\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[10]\,
	cout => \rtime_Cnt[10]~127\);

\rtime[10]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[10]~reg0\ = DFFE(\rtime_Cnt[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[10]~reg0\);

\rtime_Cnt[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[11]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[10]~127\ $ \rtime_Cnt[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[11]\,
	cin => \rtime_Cnt[10]~127\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[11]\);

\rtime[11]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[11]~reg0\ = DFFE(\rtime_Cnt[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[11]~reg0\);

\rt_en~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rt_en);

\rt_done~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rt_done~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rt_done);

\rt_pur~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rt_pur~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rt_pur);

\rtime[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[0]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(0));

\rtime[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[1]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(1));

\rtime[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[2]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(2));

\rtime[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[3]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(3));

\rtime[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[4]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(4));

\rtime[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[5]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(5));

\rtime[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[6]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(6));

\rtime[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[7]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(7));

\rtime[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[8]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(8));

\rtime[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[9]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(9));

\rtime[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[10]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(10));

\rtime[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[11]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(11));
END structure;


