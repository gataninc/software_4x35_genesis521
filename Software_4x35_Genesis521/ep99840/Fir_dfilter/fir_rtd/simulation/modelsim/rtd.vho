-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "05/31/2006 12:18:28"

-- 
-- Device: Altera EP20K300EQC240-3 Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	rtd IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	din : IN std_logic_vector(15 DOWNTO 0);
	rt_en : OUT std_logic;
	rt_done : OUT std_logic;
	rtime : OUT std_logic_vector(15 DOWNTO 0)
	);
END rtd;

ARCHITECTURE structure OF rtd IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_rt_en : std_logic;
SIGNAL ww_rt_done : std_logic;
SIGNAL ww_rtime : std_logic_vector(15 DOWNTO 0);
SIGNAL \add~639\ : std_logic;
SIGNAL \add~663\ : std_logic;
SIGNAL \add~671\ : std_logic;
SIGNAL \add~679\ : std_logic;
SIGNAL \add~687\ : std_logic;
SIGNAL \din_del2[6]\ : std_logic;
SIGNAL \din_del2[5]\ : std_logic;
SIGNAL \din_del2[8]\ : std_logic;
SIGNAL \din_del2[2]\ : std_logic;
SIGNAL \din_del2[12]\ : std_logic;
SIGNAL \din_del2[13]\ : std_logic;
SIGNAL \LessThan~527\ : std_logic;
SIGNAL \din_del1[6]\ : std_logic;
SIGNAL \din_del1[5]\ : std_logic;
SIGNAL \din_del1[8]\ : std_logic;
SIGNAL \din_del1[2]\ : std_logic;
SIGNAL \din_del1[12]\ : std_logic;
SIGNAL \din_del1[13]\ : std_logic;
SIGNAL \LessThan~531\ : std_logic;
SIGNAL \LessThan~535\ : std_logic;
SIGNAL \LessThan~539\ : std_logic;
SIGNAL \LessThan~543\ : std_logic;
SIGNAL \LessThan~547\ : std_logic;
SIGNAL \LessThan~551\ : std_logic;
SIGNAL \LessThan~555\ : std_logic;
SIGNAL \LessThan~559\ : std_logic;
SIGNAL \LessThan~563\ : std_logic;
SIGNAL \LessThan~567\ : std_logic;
SIGNAL \LessThan~571\ : std_logic;
SIGNAL \LessThan~575\ : std_logic;
SIGNAL \LessThan~579\ : std_logic;
SIGNAL \LessThan~583\ : std_logic;
SIGNAL \din[15]~combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \imr~combout\ : std_logic;
SIGNAL \din_del1[15]\ : std_logic;
SIGNAL \din_del2[15]\ : std_logic;
SIGNAL \din[14]~combout\ : std_logic;
SIGNAL \din[13]~combout\ : std_logic;
SIGNAL \din[12]~combout\ : std_logic;
SIGNAL \din[11]~combout\ : std_logic;
SIGNAL \din_del1[11]\ : std_logic;
SIGNAL \din_del2[11]\ : std_logic;
SIGNAL \din[10]~combout\ : std_logic;
SIGNAL \din_del1[10]\ : std_logic;
SIGNAL \din_del2[10]\ : std_logic;
SIGNAL \din[9]~combout\ : std_logic;
SIGNAL \din_del1[9]\ : std_logic;
SIGNAL \din_del2[9]\ : std_logic;
SIGNAL \din[8]~combout\ : std_logic;
SIGNAL \din[7]~combout\ : std_logic;
SIGNAL \din_del1[7]\ : std_logic;
SIGNAL \din_del2[7]\ : std_logic;
SIGNAL \din[6]~combout\ : std_logic;
SIGNAL \din[5]~combout\ : std_logic;
SIGNAL \din[4]~combout\ : std_logic;
SIGNAL \din_del1[4]\ : std_logic;
SIGNAL \din_del2[4]\ : std_logic;
SIGNAL \din[3]~combout\ : std_logic;
SIGNAL \din_del1[3]\ : std_logic;
SIGNAL \din_del2[3]\ : std_logic;
SIGNAL \din[2]~combout\ : std_logic;
SIGNAL \din[1]~combout\ : std_logic;
SIGNAL \din_del1[1]\ : std_logic;
SIGNAL \din_del2[1]\ : std_logic;
SIGNAL \din[0]~combout\ : std_logic;
SIGNAL \din_del1[0]\ : std_logic;
SIGNAL \din_del2[0]\ : std_logic;
SIGNAL \LessThan~585\ : std_logic;
SIGNAL \LessThan~581\ : std_logic;
SIGNAL \LessThan~577\ : std_logic;
SIGNAL \LessThan~573\ : std_logic;
SIGNAL \LessThan~569\ : std_logic;
SIGNAL \LessThan~565\ : std_logic;
SIGNAL \LessThan~561\ : std_logic;
SIGNAL \LessThan~557\ : std_logic;
SIGNAL \LessThan~553\ : std_logic;
SIGNAL \LessThan~549\ : std_logic;
SIGNAL \LessThan~545\ : std_logic;
SIGNAL \LessThan~541\ : std_logic;
SIGNAL \LessThan~537\ : std_logic;
SIGNAL \LessThan~533\ : std_logic;
SIGNAL \LessThan~529\ : std_logic;
SIGNAL \LessThan~523\ : std_logic;
SIGNAL \add~677\ : std_logic;
SIGNAL \add~681\ : std_logic;
SIGNAL \add~685\ : std_logic;
SIGNAL \add~673\ : std_logic;
SIGNAL \add~653\ : std_logic;
SIGNAL \add~649\ : std_logic;
SIGNAL \add~641\ : std_logic;
SIGNAL \add~645\ : std_logic;
SIGNAL \add~655\ : std_logic;
SIGNAL \add~657\ : std_logic;
SIGNAL \add~659\ : std_logic;
SIGNAL \add~661\ : std_logic;
SIGNAL \add~665\ : std_logic;
SIGNAL \add~667\ : std_logic;
SIGNAL \LessThan~519\ : std_logic;
SIGNAL \add~647\ : std_logic;
SIGNAL \add~651\ : std_logic;
SIGNAL \add~643\ : std_logic;
SIGNAL \LessThan~518\ : std_logic;
SIGNAL \add~675\ : std_logic;
SIGNAL \add~683\ : std_logic;
SIGNAL \LessThan~520\ : std_logic;
SIGNAL \LessThan~521\ : std_logic;
SIGNAL \din_del1[14]\ : std_logic;
SIGNAL \din_del2[14]\ : std_logic;
SIGNAL \add~669\ : std_logic;
SIGNAL \add~689\ : std_logic;
SIGNAL \add~693\ : std_logic;
SIGNAL \add~695\ : std_logic;
SIGNAL \add~691\ : std_logic;
SIGNAL \add~697\ : std_logic;
SIGNAL \add~699\ : std_logic;
SIGNAL \LessThan~522\ : std_logic;
SIGNAL \pslope_Vec[0]\ : std_logic;
SIGNAL \pslope_Vec[1]\ : std_logic;
SIGNAL \rt_en~reg0\ : std_logic;
SIGNAL rt_en_del : std_logic;
SIGNAL \rt_done~reg0\ : std_logic;
SIGNAL \rtime_Cnt[0]\ : std_logic;
SIGNAL \clock_proc~0\ : std_logic;
SIGNAL \rtime[0]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[0]~129\ : std_logic;
SIGNAL \rtime_Cnt[1]\ : std_logic;
SIGNAL \rtime[1]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[1]~132\ : std_logic;
SIGNAL \rtime_Cnt[2]\ : std_logic;
SIGNAL \rtime[2]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[2]~135\ : std_logic;
SIGNAL \rtime_Cnt[3]\ : std_logic;
SIGNAL \rtime[3]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[3]~138\ : std_logic;
SIGNAL \rtime_Cnt[4]\ : std_logic;
SIGNAL \rtime[4]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[4]~141\ : std_logic;
SIGNAL \rtime_Cnt[5]\ : std_logic;
SIGNAL \rtime[5]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[5]~144\ : std_logic;
SIGNAL \rtime_Cnt[6]\ : std_logic;
SIGNAL \rtime[6]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[6]~147\ : std_logic;
SIGNAL \rtime_Cnt[7]\ : std_logic;
SIGNAL \rtime[7]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[7]~150\ : std_logic;
SIGNAL \rtime_Cnt[8]\ : std_logic;
SIGNAL \rtime[8]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[8]~153\ : std_logic;
SIGNAL \rtime_Cnt[9]\ : std_logic;
SIGNAL \rtime[9]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[9]~156\ : std_logic;
SIGNAL \rtime_Cnt[10]\ : std_logic;
SIGNAL \rtime[10]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[10]~159\ : std_logic;
SIGNAL \rtime_Cnt[11]\ : std_logic;
SIGNAL \rtime[11]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[11]~162\ : std_logic;
SIGNAL \rtime_Cnt[12]\ : std_logic;
SIGNAL \rtime[12]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[12]~165\ : std_logic;
SIGNAL \rtime_Cnt[13]\ : std_logic;
SIGNAL \rtime[13]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[13]~168\ : std_logic;
SIGNAL \rtime_Cnt[14]\ : std_logic;
SIGNAL \rtime[14]~reg0\ : std_logic;
SIGNAL \rtime_Cnt[14]~171\ : std_logic;
SIGNAL \rtime_Cnt[15]\ : std_logic;
SIGNAL \rtime[15]~reg0\ : std_logic;
SIGNAL \ALT_INV_rt_en~reg0\ : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_din <= din;
rt_en <= ww_rt_en;
rt_done <= ww_rt_done;
rtime <= ww_rtime;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
\ALT_INV_rt_en~reg0\ <= NOT \rt_en~reg0\;

\add~639_I\ : apex20ke_lcell
-- Equation(s):
-- \add~639\ = \din_del2[6]\ $ \din[6]~combout\ $ \add~649\
-- \add~641\ = CARRY(\din_del2[6]\ & \din[6]~combout\ & !\add~649\ # !\din_del2[6]\ & (\din[6]~combout\ # !\add~649\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[6]\,
	datab => \din[6]~combout\,
	cin => \add~649\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~639\,
	cout => \add~641\);

\add~663_I\ : apex20ke_lcell
-- Equation(s):
-- \add~663\ = \din[10]~combout\ $ \din_del2[10]\ $ \add~661\
-- \add~665\ = CARRY(\din[10]~combout\ & (!\add~661\ # !\din_del2[10]\) # !\din[10]~combout\ & !\din_del2[10]\ & !\add~661\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[10]~combout\,
	datab => \din_del2[10]\,
	cin => \add~661\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~663\,
	cout => \add~665\);

\add~671_I\ : apex20ke_lcell
-- Equation(s):
-- \add~671\ = \din[3]~combout\ $ \din_del2[3]\ $ !\add~685\
-- \add~673\ = CARRY(\din[3]~combout\ & \din_del2[3]\ & !\add~685\ # !\din[3]~combout\ & (\din_del2[3]\ # !\add~685\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[3]~combout\,
	datab => \din_del2[3]\,
	cin => \add~685\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~671\,
	cout => \add~673\);

\add~679_I\ : apex20ke_lcell
-- Equation(s):
-- \add~679\ = \din[1]~combout\ $ \din_del2[1]\ $ !\add~677\
-- \add~681\ = CARRY(\din[1]~combout\ & \din_del2[1]\ & !\add~677\ # !\din[1]~combout\ & (\din_del2[1]\ # !\add~677\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[1]~combout\,
	datab => \din_del2[1]\,
	cin => \add~677\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~679\,
	cout => \add~681\);

\add~687_I\ : apex20ke_lcell
-- Equation(s):
-- \add~687\ = \din_del2[12]\ $ \din[12]~combout\ $ \add~669\
-- \add~689\ = CARRY(\din_del2[12]\ & \din[12]~combout\ & !\add~669\ # !\din_del2[12]\ & (\din[12]~combout\ # !\add~669\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[12]\,
	datab => \din[12]~combout\,
	cin => \add~669\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~687\,
	cout => \add~689\);

\din_del2[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[6]\ = DFFE(\din_del1[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[6]\);

\din_del2[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[5]\ = DFFE(\din_del1[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[5]\);

\din_del2[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[8]\ = DFFE(\din_del1[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[8]\);

\din_del2[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[2]\ = DFFE(\din_del1[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[2]\);

\din_del2[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[12]\ = DFFE(\din_del1[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[12]\);

\din_del2[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[13]\ = DFFE(\din_del1[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[13]\);

\din_del1[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[6]\ = DFFE(\din[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[6]\);

\din_del1[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[5]\ = DFFE(\din[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[5]\);

\din_del1[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[8]\ = DFFE(\din[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[8]\);

\din_del1[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[2]\ = DFFE(\din[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[2]\);

\din_del1[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[12]\ = DFFE(\din[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[12]\);

\din_del1[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[13]\ = DFFE(\din[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[13]\);

\din[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(15),
	combout => \din[15]~combout\);

\clk~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => \clk~combout\);

\imr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => \imr~combout\);

\din_del1[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[15]\ = DFFE(\din[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[15]\);

\din_del2[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[15]\ = DFFE(\din_del1[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[15]\);

\din[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(14),
	combout => \din[14]~combout\);

\din[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(13),
	combout => \din[13]~combout\);

\din[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(12),
	combout => \din[12]~combout\);

\din[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(11),
	combout => \din[11]~combout\);

\din_del1[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[11]\ = DFFE(\din[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[11]\);

\din_del2[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[11]\ = DFFE(\din_del1[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[11]\);

\din[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(10),
	combout => \din[10]~combout\);

\din_del1[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[10]\ = DFFE(\din[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[10]\);

\din_del2[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[10]\ = DFFE(\din_del1[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[10]\);

\din[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(9),
	combout => \din[9]~combout\);

\din_del1[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[9]\ = DFFE(\din[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[9]\);

\din_del2[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[9]\ = DFFE(\din_del1[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[9]\);

\din[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(8),
	combout => \din[8]~combout\);

\din[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(7),
	combout => \din[7]~combout\);

\din_del1[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[7]\ = DFFE(\din[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[7]\);

\din_del2[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[7]\ = DFFE(\din_del1[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[7]\);

\din[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(6),
	combout => \din[6]~combout\);

\din[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(5),
	combout => \din[5]~combout\);

\din[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(4),
	combout => \din[4]~combout\);

\din_del1[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[4]\ = DFFE(\din[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[4]\);

\din_del2[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[4]\ = DFFE(\din_del1[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[4]\);

\din[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(3),
	combout => \din[3]~combout\);

\din_del1[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[3]\ = DFFE(\din[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[3]\);

\din_del2[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[3]\ = DFFE(\din_del1[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[3]\);

\din[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(2),
	combout => \din[2]~combout\);

\din[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(1),
	combout => \din[1]~combout\);

\din_del1[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[1]\ = DFFE(\din[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[1]\);

\din_del2[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[1]\ = DFFE(\din_del1[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[1]\);

\din[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_din(0),
	combout => \din[0]~combout\);

\din_del1[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[0]\ = DFFE(\din[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[0]\);

\din_del2[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[0]\ = DFFE(\din_del1[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din_del1[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[0]\);

\LessThan~585_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~585\ = CARRY(!\din[0]~combout\ & \din_del2[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[0]~combout\,
	datab => \din_del2[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~583\,
	cout => \LessThan~585\);

\LessThan~581_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~581\ = CARRY(\din[1]~combout\ & (!\LessThan~585\ # !\din_del2[1]\) # !\din[1]~combout\ & !\din_del2[1]\ & !\LessThan~585\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[1]~combout\,
	datab => \din_del2[1]\,
	cin => \LessThan~585\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~579\,
	cout => \LessThan~581\);

\LessThan~577_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~577\ = CARRY(\din_del2[2]\ & (!\LessThan~581\ # !\din[2]~combout\) # !\din_del2[2]\ & !\din[2]~combout\ & !\LessThan~581\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[2]\,
	datab => \din[2]~combout\,
	cin => \LessThan~581\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~575\,
	cout => \LessThan~577\);

\LessThan~573_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~573\ = CARRY(\din[3]~combout\ & (!\LessThan~577\ # !\din_del2[3]\) # !\din[3]~combout\ & !\din_del2[3]\ & !\LessThan~577\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[3]~combout\,
	datab => \din_del2[3]\,
	cin => \LessThan~577\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~571\,
	cout => \LessThan~573\);

\LessThan~569_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~569\ = CARRY(\din[4]~combout\ & \din_del2[4]\ & !\LessThan~573\ # !\din[4]~combout\ & (\din_del2[4]\ # !\LessThan~573\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[4]~combout\,
	datab => \din_del2[4]\,
	cin => \LessThan~573\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~567\,
	cout => \LessThan~569\);

\LessThan~565_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~565\ = CARRY(\din_del2[5]\ & \din[5]~combout\ & !\LessThan~569\ # !\din_del2[5]\ & (\din[5]~combout\ # !\LessThan~569\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[5]\,
	datab => \din[5]~combout\,
	cin => \LessThan~569\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~563\,
	cout => \LessThan~565\);

\LessThan~561_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~561\ = CARRY(\din_del2[6]\ & (!\LessThan~565\ # !\din[6]~combout\) # !\din_del2[6]\ & !\din[6]~combout\ & !\LessThan~565\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[6]\,
	datab => \din[6]~combout\,
	cin => \LessThan~565\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~559\,
	cout => \LessThan~561\);

\LessThan~557_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~557\ = CARRY(\din[7]~combout\ & (!\LessThan~561\ # !\din_del2[7]\) # !\din[7]~combout\ & !\din_del2[7]\ & !\LessThan~561\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[7]~combout\,
	datab => \din_del2[7]\,
	cin => \LessThan~561\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~555\,
	cout => \LessThan~557\);

\LessThan~553_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~553\ = CARRY(\din_del2[8]\ & (!\LessThan~557\ # !\din[8]~combout\) # !\din_del2[8]\ & !\din[8]~combout\ & !\LessThan~557\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[8]\,
	datab => \din[8]~combout\,
	cin => \LessThan~557\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~551\,
	cout => \LessThan~553\);

\LessThan~549_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~549\ = CARRY(\din[9]~combout\ & (!\LessThan~553\ # !\din_del2[9]\) # !\din[9]~combout\ & !\din_del2[9]\ & !\LessThan~553\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[9]~combout\,
	datab => \din_del2[9]\,
	cin => \LessThan~553\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~547\,
	cout => \LessThan~549\);

\LessThan~545_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~545\ = CARRY(\din[10]~combout\ & \din_del2[10]\ & !\LessThan~549\ # !\din[10]~combout\ & (\din_del2[10]\ # !\LessThan~549\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[10]~combout\,
	datab => \din_del2[10]\,
	cin => \LessThan~549\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~543\,
	cout => \LessThan~545\);

\LessThan~541_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~541\ = CARRY(\din[11]~combout\ & (!\LessThan~545\ # !\din_del2[11]\) # !\din[11]~combout\ & !\din_del2[11]\ & !\LessThan~545\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[11]~combout\,
	datab => \din_del2[11]\,
	cin => \LessThan~545\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~539\,
	cout => \LessThan~541\);

\LessThan~537_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~537\ = CARRY(\din_del2[12]\ & (!\LessThan~541\ # !\din[12]~combout\) # !\din_del2[12]\ & !\din[12]~combout\ & !\LessThan~541\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[12]\,
	datab => \din[12]~combout\,
	cin => \LessThan~541\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~535\,
	cout => \LessThan~537\);

\LessThan~533_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~533\ = CARRY(\din_del2[13]\ & \din[13]~combout\ & !\LessThan~537\ # !\din_del2[13]\ & (\din[13]~combout\ # !\LessThan~537\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[13]\,
	datab => \din[13]~combout\,
	cin => \LessThan~537\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~531\,
	cout => \LessThan~533\);

\LessThan~529_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~529\ = CARRY(\din_del2[14]\ & (!\LessThan~533\ # !\din[14]~combout\) # !\din_del2[14]\ & !\din[14]~combout\ & !\LessThan~533\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[14]\,
	datab => \din[14]~combout\,
	cin => \LessThan~533\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~527\,
	cout => \LessThan~529\);

\LessThan~523_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~523\ = \din_del2[15]\ & (\LessThan~529\ # !\din[15]~combout\) # !\din_del2[15]\ & \LessThan~529\ & !\din[15]~combout\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C0FC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \din_del2[15]\,
	datad => \din[15]~combout\,
	cin => \LessThan~529\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~523\);

\add~675_I\ : apex20ke_lcell
-- Equation(s):
-- \add~675\ = \din[0]~combout\ $ \din_del2[0]\
-- \add~677\ = CARRY(\din[0]~combout\ # !\din_del2[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[0]~combout\,
	datab => \din_del2[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~675\,
	cout => \add~677\);

\add~683_I\ : apex20ke_lcell
-- Equation(s):
-- \add~683\ = \din_del2[2]\ $ \din[2]~combout\ $ \add~681\
-- \add~685\ = CARRY(\din_del2[2]\ & \din[2]~combout\ & !\add~681\ # !\din_del2[2]\ & (\din[2]~combout\ # !\add~681\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[2]\,
	datab => \din[2]~combout\,
	cin => \add~681\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~683\,
	cout => \add~685\);

\add~651_I\ : apex20ke_lcell
-- Equation(s):
-- \add~651\ = \din_del2[4]\ $ \din[4]~combout\ $ \add~673\
-- \add~653\ = CARRY(\din_del2[4]\ & \din[4]~combout\ & !\add~673\ # !\din_del2[4]\ & (\din[4]~combout\ # !\add~673\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[4]\,
	datab => \din[4]~combout\,
	cin => \add~673\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~651\,
	cout => \add~653\);

\add~647_I\ : apex20ke_lcell
-- Equation(s):
-- \add~647\ = \din_del2[5]\ $ \din[5]~combout\ $ !\add~653\
-- \add~649\ = CARRY(\din_del2[5]\ & (!\add~653\ # !\din[5]~combout\) # !\din_del2[5]\ & !\din[5]~combout\ & !\add~653\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[5]\,
	datab => \din[5]~combout\,
	cin => \add~653\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~647\,
	cout => \add~649\);

\add~643_I\ : apex20ke_lcell
-- Equation(s):
-- \add~643\ = \din[7]~combout\ $ \din_del2[7]\ $ !\add~641\
-- \add~645\ = CARRY(\din[7]~combout\ & \din_del2[7]\ & !\add~641\ # !\din[7]~combout\ & (\din_del2[7]\ # !\add~641\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[7]~combout\,
	datab => \din_del2[7]\,
	cin => \add~641\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~643\,
	cout => \add~645\);

\add~655_I\ : apex20ke_lcell
-- Equation(s):
-- \add~655\ = \din_del2[8]\ $ \din[8]~combout\ $ \add~645\
-- \add~657\ = CARRY(\din_del2[8]\ & \din[8]~combout\ & !\add~645\ # !\din_del2[8]\ & (\din[8]~combout\ # !\add~645\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[8]\,
	datab => \din[8]~combout\,
	cin => \add~645\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~655\,
	cout => \add~657\);

\add~659_I\ : apex20ke_lcell
-- Equation(s):
-- \add~659\ = \din[9]~combout\ $ \din_del2[9]\ $ !\add~657\
-- \add~661\ = CARRY(\din[9]~combout\ & \din_del2[9]\ & !\add~657\ # !\din[9]~combout\ & (\din_del2[9]\ # !\add~657\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[9]~combout\,
	datab => \din_del2[9]\,
	cin => \add~657\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~659\,
	cout => \add~661\);

\add~667_I\ : apex20ke_lcell
-- Equation(s):
-- \add~667\ = \din[11]~combout\ $ \din_del2[11]\ $ !\add~665\
-- \add~669\ = CARRY(\din[11]~combout\ & \din_del2[11]\ & !\add~665\ # !\din[11]~combout\ & (\din_del2[11]\ # !\add~665\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[11]~combout\,
	datab => \din_del2[11]\,
	cin => \add~665\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~667\,
	cout => \add~669\);

\LessThan~519_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~519\ = \add~663\ # \add~655\ # \add~659\ # \add~667\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~663\,
	datab => \add~655\,
	datac => \add~659\,
	datad => \add~667\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~519\);

\LessThan~518_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~518\ = \add~639\ # \add~643\ # \add~647\ & \add~651\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~639\,
	datab => \add~647\,
	datac => \add~651\,
	datad => \add~643\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~518\);

\LessThan~520_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~520\ = \add~647\ & (\add~679\ # \add~675\ # \add~683\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FE00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~679\,
	datab => \add~675\,
	datac => \add~683\,
	datad => \add~647\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~520\);

\LessThan~521_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~521\ = \LessThan~519\ # \LessThan~518\ # \add~671\ & \LessThan~520\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~671\,
	datab => \LessThan~519\,
	datac => \LessThan~518\,
	datad => \LessThan~520\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~521\);

\din_del1[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del1[14]\ = DFFE(\din[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \din[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del1[14]\);

\din_del2[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \din_del2[14]\ = DFFE(\din_del1[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \din_del1[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \din_del2[14]\);

\add~691_I\ : apex20ke_lcell
-- Equation(s):
-- \add~691\ = \din_del2[13]\ $ \din[13]~combout\ $ !\add~689\
-- \add~693\ = CARRY(\din_del2[13]\ & (!\add~689\ # !\din[13]~combout\) # !\din_del2[13]\ & !\din[13]~combout\ & !\add~689\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din_del2[13]\,
	datab => \din[13]~combout\,
	cin => \add~689\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~691\,
	cout => \add~693\);

\add~695_I\ : apex20ke_lcell
-- Equation(s):
-- \add~695\ = \din[14]~combout\ $ \din_del2[14]\ $ \add~693\
-- \add~697\ = CARRY(\din[14]~combout\ & (!\add~693\ # !\din_del2[14]\) # !\din[14]~combout\ & !\din_del2[14]\ & !\add~693\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \din[14]~combout\,
	datab => \din_del2[14]\,
	cin => \add~693\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~695\,
	cout => \add~697\);

\add~699_I\ : apex20ke_lcell
-- Equation(s):
-- \add~699\ = \din_del2[15]\ $ \add~697\ $ !\din[15]~combout\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \din_del2[15]\,
	datad => \din[15]~combout\,
	cin => \add~697\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \add~699\);

\LessThan~522_I\ : apex20ke_lcell
-- Equation(s):
-- \LessThan~522\ = \add~687\ # \add~695\ # \add~691\ # \add~699\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \add~687\,
	datab => \add~695\,
	datac => \add~691\,
	datad => \add~699\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \LessThan~522\);

\pslope_Vec[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \pslope_Vec[0]\ = DFFE(!\LessThan~523\ & (\LessThan~521\ # \LessThan~522\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3330",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \LessThan~523\,
	datac => \LessThan~521\,
	datad => \LessThan~522\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pslope_Vec[0]\);

\pslope_Vec[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \pslope_Vec[1]\ = DFFE(\pslope_Vec[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \pslope_Vec[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \pslope_Vec[1]\);

\rt_en~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rt_en~reg0\ = DFFE(\pslope_Vec[0]\ # \pslope_Vec[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \pslope_Vec[0]\,
	datad => \pslope_Vec[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rt_en~reg0\);

\rt_en_del~I\ : apex20ke_lcell
-- Equation(s):
-- rt_en_del = DFFE(\rt_en~reg0\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rt_en~reg0\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rt_en_del);

\rt_done~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rt_done~reg0\ = DFFE(!\rt_en~reg0\ & rt_en_del, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rt_en~reg0\,
	datad => rt_en_del,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rt_done~reg0\);

\rtime_Cnt[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[0]\ = DFFE(GLOBAL(\rt_en~reg0\) & !\rtime_Cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[0]~129\ = CARRY(\rtime_Cnt[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[0]\,
	cout => \rtime_Cnt[0]~129\);

\clock_proc~0_I\ : apex20ke_lcell
-- Equation(s):
-- \clock_proc~0\ = !\rt_en~reg0\ & rt_en_del

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rt_en~reg0\,
	datad => rt_en_del,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \clock_proc~0\);

\rtime[0]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[0]~reg0\ = DFFE(\rtime_Cnt[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[0]~reg0\);

\rtime_Cnt[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[1]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[1]\ $ \rtime_Cnt[0]~129\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[1]~132\ = CARRY(!\rtime_Cnt[0]~129\ # !\rtime_Cnt[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[1]\,
	cin => \rtime_Cnt[0]~129\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[1]\,
	cout => \rtime_Cnt[1]~132\);

\rtime[1]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[1]~reg0\ = DFFE(\rtime_Cnt[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[1]~reg0\);

\rtime_Cnt[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[2]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[2]\ $ !\rtime_Cnt[1]~132\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[2]~135\ = CARRY(\rtime_Cnt[2]\ & !\rtime_Cnt[1]~132\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[2]\,
	cin => \rtime_Cnt[1]~132\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[2]\,
	cout => \rtime_Cnt[2]~135\);

\rtime[2]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[2]~reg0\ = DFFE(\rtime_Cnt[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[2]~reg0\);

\rtime_Cnt[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[3]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[3]\ $ \rtime_Cnt[2]~135\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[3]~138\ = CARRY(!\rtime_Cnt[2]~135\ # !\rtime_Cnt[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[3]\,
	cin => \rtime_Cnt[2]~135\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[3]\,
	cout => \rtime_Cnt[3]~138\);

\rtime[3]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[3]~reg0\ = DFFE(\rtime_Cnt[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rtime_Cnt[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[3]~reg0\);

\rtime_Cnt[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[4]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[4]\ $ !\rtime_Cnt[3]~138\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[4]~141\ = CARRY(\rtime_Cnt[4]\ & !\rtime_Cnt[3]~138\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[4]\,
	cin => \rtime_Cnt[3]~138\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[4]\,
	cout => \rtime_Cnt[4]~141\);

\rtime[4]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[4]~reg0\ = DFFE(\rtime_Cnt[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[4]~reg0\);

\rtime_Cnt[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[5]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[5]\ $ \rtime_Cnt[4]~141\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[5]~144\ = CARRY(!\rtime_Cnt[4]~141\ # !\rtime_Cnt[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[5]\,
	cin => \rtime_Cnt[4]~141\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[5]\,
	cout => \rtime_Cnt[5]~144\);

\rtime[5]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[5]~reg0\ = DFFE(\rtime_Cnt[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rtime_Cnt[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[5]~reg0\);

\rtime_Cnt[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[6]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[6]\ $ !\rtime_Cnt[5]~144\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[6]~147\ = CARRY(\rtime_Cnt[6]\ & !\rtime_Cnt[5]~144\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[6]\,
	cin => \rtime_Cnt[5]~144\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[6]\,
	cout => \rtime_Cnt[6]~147\);

\rtime[6]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[6]~reg0\ = DFFE(\rtime_Cnt[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \rtime_Cnt[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[6]~reg0\);

\rtime_Cnt[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[7]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[7]\ $ \rtime_Cnt[6]~147\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[7]~150\ = CARRY(!\rtime_Cnt[6]~147\ # !\rtime_Cnt[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[7]\,
	cin => \rtime_Cnt[6]~147\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[7]\,
	cout => \rtime_Cnt[7]~150\);

\rtime[7]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[7]~reg0\ = DFFE(\rtime_Cnt[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[7]~reg0\);

\rtime_Cnt[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[8]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[8]\ $ !\rtime_Cnt[7]~150\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[8]~153\ = CARRY(\rtime_Cnt[8]\ & !\rtime_Cnt[7]~150\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[8]\,
	cin => \rtime_Cnt[7]~150\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[8]\,
	cout => \rtime_Cnt[8]~153\);

\rtime[8]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[8]~reg0\ = DFFE(\rtime_Cnt[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[8]~reg0\);

\rtime_Cnt[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[9]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[9]\ $ \rtime_Cnt[8]~153\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[9]~156\ = CARRY(!\rtime_Cnt[8]~153\ # !\rtime_Cnt[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[9]\,
	cin => \rtime_Cnt[8]~153\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[9]\,
	cout => \rtime_Cnt[9]~156\);

\rtime[9]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[9]~reg0\ = DFFE(\rtime_Cnt[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[9]~reg0\);

\rtime_Cnt[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[10]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[10]\ $ !\rtime_Cnt[9]~156\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[10]~159\ = CARRY(\rtime_Cnt[10]\ & !\rtime_Cnt[9]~156\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[10]\,
	cin => \rtime_Cnt[9]~156\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[10]\,
	cout => \rtime_Cnt[10]~159\);

\rtime[10]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[10]~reg0\ = DFFE(\rtime_Cnt[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[10]~reg0\);

\rtime_Cnt[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[11]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[11]\ $ \rtime_Cnt[10]~159\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[11]~162\ = CARRY(!\rtime_Cnt[10]~159\ # !\rtime_Cnt[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[11]\,
	cin => \rtime_Cnt[10]~159\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[11]\,
	cout => \rtime_Cnt[11]~162\);

\rtime[11]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[11]~reg0\ = DFFE(\rtime_Cnt[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[11]~reg0\);

\rtime_Cnt[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[12]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[12]\ $ !\rtime_Cnt[11]~162\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[12]~165\ = CARRY(\rtime_Cnt[12]\ & !\rtime_Cnt[11]~162\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[12]\,
	cin => \rtime_Cnt[11]~162\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[12]\,
	cout => \rtime_Cnt[12]~165\);

\rtime[12]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[12]~reg0\ = DFFE(\rtime_Cnt[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[12]~reg0\);

\rtime_Cnt[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[13]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[13]\ $ \rtime_Cnt[12]~165\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[13]~168\ = CARRY(!\rtime_Cnt[12]~165\ # !\rtime_Cnt[13]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[13]\,
	cin => \rtime_Cnt[12]~165\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[13]\,
	cout => \rtime_Cnt[13]~168\);

\rtime[13]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[13]~reg0\ = DFFE(\rtime_Cnt[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[13]~reg0\);

\rtime_Cnt[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[14]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[14]\ $ !\rtime_Cnt[13]~168\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \rtime_Cnt[14]~171\ = CARRY(\rtime_Cnt[14]\ & !\rtime_Cnt[13]~168\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \rtime_Cnt[14]\,
	cin => \rtime_Cnt[13]~168\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[14]\,
	cout => \rtime_Cnt[14]~171\);

\rtime[14]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[14]~reg0\ = DFFE(\rtime_Cnt[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[14]~reg0\);

\rtime_Cnt[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \rtime_Cnt[15]\ = DFFE(GLOBAL(\rt_en~reg0\) & \rtime_Cnt[14]~171\ $ \rtime_Cnt[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[15]\,
	cin => \rtime_Cnt[14]~171\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \ALT_INV_rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime_Cnt[15]\);

\rtime[15]~reg0_I\ : apex20ke_lcell
-- Equation(s):
-- \rtime[15]~reg0\ = DFFE(\rtime_Cnt[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , \clock_proc~0\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \rtime_Cnt[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	ena => \clock_proc~0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \rtime[15]~reg0\);

\rt_en~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rt_en~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rt_en);

\rt_done~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rt_done~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rt_done);

\rtime[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[0]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(0));

\rtime[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[1]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(1));

\rtime[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[2]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(2));

\rtime[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[3]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(3));

\rtime[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[4]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(4));

\rtime[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[5]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(5));

\rtime[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[6]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(6));

\rtime[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[7]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(7));

\rtime[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[8]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(8));

\rtime[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[9]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(9));

\rtime[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[10]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(10));

\rtime[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[11]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(11));

\rtime[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[12]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(12));

\rtime[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[13]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(13));

\rtime[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[14]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(14));

\rtime[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \rtime[15]~reg0\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rtime(15));
END structure;


