onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/clk40
add wave -noupdate -format Logic -radix binary /tb/rt_en
add wave -noupdate -format Logic -radix binary /tb/rt_done
add wave -noupdate -format Literal -radix decimal /tb/rtime
add wave -noupdate -format Literal -radix decimal /tb/rise
add wave -noupdate -format Logic -radix binary /tb/u/pslope
add wave -noupdate -format Literal -radix binary /tb/u/pslope_vec
add wave -noupdate -format Analog-Step -height 100 -radix decimal -scale 0.5 /tb/u/din_diff
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.050000000000000003 /tb/dout
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 4} {214497836 ps} 0}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
update
WaveRestoreZoom {213384207 ps} {216155529 ps}
