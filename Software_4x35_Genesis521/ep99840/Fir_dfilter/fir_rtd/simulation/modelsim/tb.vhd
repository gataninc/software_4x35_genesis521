-------------------------------------------------------------------------------
-- EDAX Inc
-- 91 McKee Drive
-- Mahwah, NJ 07430
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
     use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
     use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr 	: std_logic := '1';
	signal clk20 	: std_logic := '0';
	signal clk40 	: std_logic := '0';
	signal dout	: std_logic_vector( 14 downto 0 );
	signal din	: std_logic_vector( 15 downto 0 );
	signal rt_en	: std_logic;
	signal rt_done	: std_logic;
	signal rtime	: std_Logic_Vector( 15 downto 0 );
	signal rise	: time;

	-------------------------------------------------------------------------------
	component rtd is
		port(
			imr		: in		std_logic;
			clk		: in		std_logic;
			din		: in		std_logic_vector( 15 downto 0 );
			rt_en	: buffer	std_logic;
			rt_done	: buffer	std_logic;
			rtime	: buffer	std_logic_vector( 15 downto 0 ) );
	end component rtd;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component Mn20k is 
	     port( 
	          imr 	: in std_logic;
	          clk 	: in std_logic;
	          dout : out std_logic_Vector( 14 downto 0 ) );
	end component Mn20k;
	-------------------------------------------------------------------------------


begin
	imr 		<= '0' after 555 ns;
	clk40	<= not( clk40 ) after 12.5 ns;

	-------------------------------------------------------------------------------
	clk20_gen_proc : process( clk40 )
	begin
		if(( clk40'event ) and ( clk40 = '1' )) then
			clk20 <= not( clk20 );
		end if;
	end process;
	-------------------------------------------------------------------------------

	rise	<= conv_integer( rtime ) * 25 ns;
	
	din  <= dout(14) & dout;
	u : rtd 
		port map(
			imr		=> imr,
			clk		=> clk20,
			din		=> din,
			rt_en	=> rt_en,
			rt_done	=> rt_done,
			rtime	=> rtime );
	
	UR : mn20k
		port map(
			imr		=> imr,
			clk		=> clk20,
			dout		=> dout );


-------------------------------------------------------------------------------
end test_bench;      -- Mn20k
-------------------------------------------------------------------------------
