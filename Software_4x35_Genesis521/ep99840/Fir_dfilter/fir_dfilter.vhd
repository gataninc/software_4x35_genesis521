-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_dfilter.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--   History:
--		6/16/05 - MCS
--			* Stripped out from FIR_BLOCK to generate all the digital discriminators
--			  as well as the BLM and Main Channel for normal Pulsed Reset Processing
--			* Compiled Quartus 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_dfilter is 
	port(
		imr				: in		std_logic;					-- Master Reset
		clk20			: in		std_Logic;					-- Master 20Mhz Clock
		AD_CLR			: in		std_logic;
		ADisc_en			: in		std_logic;					-- Adaptive Disc Enable
		Disc_en			: in		std_logic_Vector(  5 downto 0 );	-- Discriminator enabled
		PA_Reset			: in		std_logic;
		SH_Disc_In		: in		std_logic;
		OTR				: in		std_logic;
		ad_data			: in		std_logic_vector(  15 downto 0 );	-- A/D Input for Discriminators Data
		fad_data			: in		std_logic_vector(  15 downto 0 );	-- A/D Input for Filter
		decay			: in		std_logic_vector(  15 downto 0 );	-- ADisc Decay Factor
		fir_dly_len		: in		std_logic_Vector(   3 downto 0 );	-- Flat-Top Selection
		fir_offset_inc		: in		std_logic_Vector(  11 downto 0 );	-- Additional Settling Time for PUR
		offset			: in		std_logic_Vector(  15 downto 0 );
		PS_SEL			: in		std_logic_Vector(   1 downto 0 );	-- Peak Shift Select (BLR)
		TC_SEL			: in		std_logic_vector(   3 downto 0 );	-- Time Const Select
		tlevel 			: in		std_logic_vector(  63 downto 0 );	-- Threshold Level
		BLR_PDONE			: buffer	std_logic;					-- Peak DOne after BLR
		BLR_PUR			: buffer	std_logic;					-- Pile-UP Reject after BLR
		BLR_PBUSY			: buffer	std_logic;					-- Pulse Busy after BLR
		FDisc			: buffer	std_logic;
		PUR				: buffer	std_logic;
		blevel_upd		: buffer	std_logic;					-- Baseline level Update (FIFO)
		BLEVEL			: buffer	std_logic_vector(   7 downto 0 );	-- Baseline Level 
		Disc_In  			: buffer	std_logic_Vector(   4 downto 0 );	-- Discriminator Input
		rtd_done 			: buffer	std_logic;
		rtd_time			: buffer	std_logic_vector(  11 downto 0 );
		fir_data			: buffer	std_logic_Vector( 103 downto 0 ));	-- Filter Data ( All of it )
end fir_dfilter;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_dfilter is
	

	signal atlevel				: std_logic_vector(  63 downto 0 );

	signal DSelect 			: std_logic_vector(   5 downto  0 );
	signal dly_data 			: std_logic_Vector(  79 downto 0 );
	signal ddly_data			: std_logic_vector( 143 downto 0 );
	
	signal Disc_Cmp			: std_logic_Vector(   4 downto 0 );
	signal Disc_Block 			: std_logic_Vector(   5 downto 1 );

	signal pbusy				: std_logic;
	signal Pur_Time			: std_logic_Vector(  11 downto 0 );	
	signal Peak_time			: std_logic_vector(  11 downto 0 );
	signal PA_Busy				: std_logic;
	signal Peak_Done			: std_logic;

	signal rtd_en			: std_logic;					-- Ver 4x17
	signal rtd_pur			: std_logic;
	
	-------------------------------------------------------------------------------
	component fir_auto_disc is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			ADisc_En		: in		std_logic;
			Decay		: in		std_logic_Vector( 15 downto 0 );
			tlevel		: in 	std_logic_Vector( 15 downto 0 );	-- User Threshold Lavel
			fir_out		: in 	std_logic_Vector( 15 downto 0 );	-- FIR Output 
			atlevel		: buffer	std_logic_vector( 15 downto 0 ) );	-- AUto Threshold Level
	end component fir_auto_disc;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component FIR_BLR is
		port(
			imr			: in		std_logic;
			clk			: in		std_logic;
			AD_MR		: in		std_logic;
			pbusy		: in		std_logic;
			PDone_In		: in		std_logic;
			Pur_In		: in		std_logic;
			tc_sel		: in		std_logic_vector(  3 downto 0 );
			tlevel		: in		std_logic_Vector( 15 downto 0 );
			PS_SEL		: in		std_logic_vector(  1 downto 0 );
			fir_in		: in		std_logic_Vector( 23 downto 0 );
			offset		: in		std_logic_Vector( 15 downto 0 );
			PDone_Out		: buffer	std_logic;
			Pur_Out		: buffer	std_logic;
			pbusy_out		: buffer	std_logic;
			fir_out		: buffer	std_logic_Vector( 15 downto 0 );
			blevel_upd	: buffer	std_logic;
			blevel		: buffer	std_logic_Vector( 7 downto 0 ) );
	end component fir_blr;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_disc is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			AD_MR		: in		std_logic;
			PA_Reset		: in		std_logic;
			adata		: in		std_logic_Vector( 47 downto 0 );
			DSelect		: in		std_logic_vector(  1 downto 0 );					-- 0 = High 1 = Medium, 2 = Low
			Disc_En		: in		std_logic;
			tlevel		: in		std_logic_vector( 15 downto 0 );	-- Discriminator Level
			Level_Out		: buffer	std_logic;					-- Discriminator Level Exceeded	
			Level_Cmp		: buffer	std_logic;
			fir_out		: buffer	std_logic_Vector( 15 downto 0 ) );	-- FIR Output ( for Debug )
	end component fir_disc;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_dmem is 
			port( 
			clk			: in		std_logic;
	     	imr            : in      std_logic;                    -- Master Reset
			adata		: in		std_logic_Vector( 15 downto 0 );
			Dout			: buffer	std_logic_Vector( 143 downto 0 ) );
	end component fir_dmem;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_edisc
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			AD_MR		: in		std_logic;					-- Clear's FIR
			PA_Reset		: in		std_logic;
			Disc_En		: in		std_logic;
			TimeConst		: in		std_logic_Vector(  3 downto 0 );
			data_abc		: in		std_logic_Vector( 47 downto 0 );	-- First TAP
			tlevel		: in		std_logic_vector( 15 downto 0 );
			Level_Cmp		: buffer	std_logic;
			Level_Out		: buffer	std_logic;					-- Discriminator Level Exceeded
			fir_out		: buffer	std_logic_Vector( 15 downto 0 ));	-- FIR Output ( for Debug )
	end component fir_edisc;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_gen2 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			PA_Reset		: in		std_logic;
			AD_MR		: in		std_logic;					-- Clear's FIR
			TimeConst 	: in		std_logic_Vector(  3 downto 0 );
			Offset_Time	: in		std_logic_Vector(  3 downto 0 );
			data_abc		: in		std_logic_Vector( 79 downto 0 );	-- First TAP
			fir_out		: buffer	std_logic_Vector( 23 downto 0 ));	-- FIR Output 
	end component Fir_gen2;
	-------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	component fir_mem2
		port( 
			clk			: in		std_logic;
     		imr            : in      std_logic;                    -- Master Reset
			TC_SEL		: in		std_logic_vector(   3 downto 0 );
			adata		: in		std_logic_Vector(  15 downto 0 );
			Peak_time		: buffer	std_logic_vector(  11 downto 0 );
			Pur_time		: buffer	std_logic_vector(  11 downto 0 );
			Dout			: buffer	std_logic_Vector(  79 downto 0 ) );
	end component fir_mem2; 
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component Fir_PSteer is 
		port( 
			clk            : in      std_logic;			     	-- Master Clock Input
		     imr            : in      std_logic;                    	-- Master Reset
			OTR			: in		std_logic;
			PA_Reset		: in		std_logic;					-- PreAmp Reset
			RTD_PUR		: in		std_logic;
		     Disc_In        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
		     Disc_En        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
			Disc_Cmp		: in		std_logic_Vector(  4 downto 0 );
			Offset_Time	: in		std_logic_vector(  3 downto 0 );	-- Offset TIme
			Offset_Inc	: in		std_logic_vector( 11 downto 0 );		
			Peak_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
			Pur_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
			Peak_Done		: buffer	std_logic;
			BLOCK_OUT		: buffer	std_logic_Vector(  5 downto 1 );	-- BLocking Pulses
			FDISC		: buffer	std_Logic;					-- Fast Disc
			Pur			: buffer	std_logic;					-- Pile-Up Reject
			PBusy		: buffer	std_logic;
			PA_Busy		: buffer	std_logic );
	end component Fir_PSteer;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_rtd is
		port(
			imr		: in		std_logic;
			clk		: in		std_logic;
			din		: in		std_logic_vector( 15 downto 0 );
			rt_en	: buffer	std_logic;
			rt_pur	: buffer	std_logic;
			rt_done	: buffer	std_logic;
			rtime	: buffer	std_logic_vector( 11 downto 0 ) );
	end component fir_rtd;
	-------------------------------------------------------------------------------

begin
	Disc_in(0) 	<= SH_Disc_In;
	Disc_Cmp(0) 	<= '0';
	------------------------------------------------------------------------------
	-- A/D Data for the Discriminators
	-- Documented 8/08/03 - MCS
	dmem_inst : fir_dmem
		port map(
			imr			=> imr,							-- Master Reset
			clk			=> clk20,							-- Master Clock
			adata		=> ad_data,						-- A/D Data
			Dout			=> ddly_data );					-- All A/D Data used for FIR Generation
	------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Energy Channel Generator
	--	Including Delay Elements, BLM Discriminator and Main Energy Channel
	-- Documented 8/08/03 - MCS
	fir_mem2_inst : fir_mem2
		port map(
			imr				=> imr,					-- Master Reset
			clk				=> clk20,					-- Master Clock
			TC_SEL			=> TC_SEL( 3 downto 0 ),		-- Amp Time 
			adata			=> fad_data,				-- Use Filtered A/D Data				
			Peak_time			=> Peak_Time,				-- Peaking Time
			Pur_Time			=> Pur_Time,				-- Pile-UP Rejection Time
			Dout				=> dly_data );				-- All A/D Data used for FIR Generation
	-- Energy Channel Generator
	-------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------
	-- Pulsed Reset or Gaussian Generation		
	-- Discrmiinator Generation and Detection
	-- High, Medium and Low
	-- Documented 8/08/03 - MCS
	DSelect		<= "100100";			

	DisC_Gen : for i in 0 to 2 generate
		fir_disc_inst : fir_disc 
			port map(
				imr		=> imr,							-- Master Clock
				clk		=> clk20,							-- Master Reset
				AD_MR	=> AD_CLR, 						-- Reset Accumulator
				PA_Reset	=> PA_Reset,  						-- PreAmp Busy
				adata	=> ddly_data((48*i)+47 downto 48*i ),	-- Disc A/D Input 
				DSelect	=> DSelect(  ( 2*i)+ 1 downto  2*i ),	-- Discriminator Select
				Disc_En	=> Disc_En(       i+1 ),				-- Discriminator Enable
				tlevel	=> atlevel(  (16*i)+15 downto 16*i ),	-- Threhsold Level
				Level_Out	=> Disc_In(       i+1 ),				-- Discrminator Output (Pulses)
				Level_Cmp	=> Disc_Cmp(  	   i+1 ),				-- Discriminaor Threshold Comparisons
				fir_out	=> fir_data( (16*i)+15 downto 16*i ));	-- Fir Output
	end generate;
	-- Medium FIR Generation ( 1 us digital FIR Generation )
	-------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Energy Discriminator (BLM)
	-- Documented 8/08/03 - MCS
	fir_edisc_inst : fir_edisc
		port map(
		 	imr			=> imr,							-- Master Reset
			clk			=> clk20,							-- Master Clock
			AD_MR		=> AD_CLR,						-- Reset Accumulators
			PA_Reset		=> PA_Busy,						-- PreAmp Busy
			Disc_En		=> Disc_En(4),						-- BLM Enable
			TimeConst		=> TC_Sel,						-- AMp Time
			data_abc		=> dly_data( 47 downto 0 ), 			-- Delayed Data for 1/2 Amp Time
			tlevel		=> atlevel( 63 downto 48 ),			-- BLM Threshold Level
			Level_Cmp		=> Disc_Cmp(4),					-- Threshold Level Comparison
			Level_Out		=> Disc_In(4),						-- BLM Pulse
			fir_out		=> fir_data( 63 downto 48 ) );		-- BLM Fir Data
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Main Fitler Generation
	-- Documented 8/08/03 - MCS
	fir_gen2_inst : fir_gen2
		port map(
			imr			=> imr,							-- Master Reset
			clk			=> clk20,							-- Master Clock
			PA_Reset		=> PA_Reset,						-- PreAmp Reset
			AD_MR		=> AD_CLR, 						-- A/D Clear (Reset Accumulator)
			TimeConst 	=> TC_Sel,						-- Amp Time
			Offset_Time	=> Fir_Dly_Len,					-- Flat-Top Selection
			data_abc		=> dly_data,						-- Delayed data
			fir_out		=> fir_data( 87 downto 64 ) );		-- Filter Ouptut
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	fir_rtd_inst : fir_rtd
		port map(
			imr					=> imr,
			clk					=> clk20,
			din					=> Fad_Data,
			rt_en				=> rtd_en,
			rt_pur				=> rtd_pur,
			rt_Done				=> rtd_done,
			rtime				=> rtd_time );
     -------------------------------------------------------------------------------

	------------------------------------------------------------------------------
	-- Auto-Discriminator Generation
	-- Documented 7/30/03 - MCS
	-- 0 = High
	-- 1 = Med
	-- 2 = Low
	-- 3 = Energy
	adisc_gen : for i in 0 to 3 generate
		fir_auto_Disc_inst : fir_auto_disc
			port map(
				imr		=> imr,							-- Master Reset
				clk		=> clk20,							-- Master Clock
				ADisc_En	=> Adisc_en,						-- Auto_Disc Enable
				Decay	=> decay,							-- Decay Value
				fir_out	=> fir_data((16*i)+15 downto 16*i ),	-- Filter's Output Data
				tlevel	=> tlevel(  (16*i)+15 downto 16*i ),	-- Threshold Input
				atlevel	=> atlevel( (16*i)+15 downto 16*i ));	-- Threshold Output
	end generate;		
	
	------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	-- Based on the Discriminator Edge Detects and the Discriminator Blocks
	-- Generate the First Discriminator Pulse
	fir_psteer_inst : Fir_PSteer 
		port maP(
			CLK			=> clk20,						-- Master Clock 
			IMR			=> IMR,						-- Master REset In
			otr			=> otr,						-- A/D Out of Range
			PA_Reset		=> PA_Reset,					-- PreAmp Reset
			RTD_PUR		=> '0',						-- Always Disabled RTD_PUR,
		     Disc_In        => Disc_In,					-- Discriminator Pulses
		     Disc_En        => Disc_En( 4 downto 0 ),		-- Discriminator Enabled
			Disc_Cmp		=> Disc_Cmp,					-- Discrimator Thresholds
			Offset_Time	=> fir_dly_len,				-- Flat-Top Time
			Offset_Inc	=> fir_offset_inc,				-- Additional Settling Time
			Peak_Time		=> Peak_time,					-- Peaking time
			Pur_Time		=> Pur_Time,					-- Pile-Up Reject Time
			Peak_Done		=> Peak_Done,					-- Peak Done
			BLOCK_OUT		=> Disc_Block( 5 downto 1 ),		-- Block Output (TP)
			FDISC		=> FDisc,						-- First Disc Output (TP)
			PUR			=> Pur,						-- Pile-Up Reject
			PBusy		=> PBusy,						-- Pulse Busy
			PA_Busy		=> PA_Busy );					-- PreAmp Busy
	-------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Baseline Restoratation Generation 
	-- Documented 7/30/03
	fir_blr_inst : FIR_BLR
		port map(
			imr			=> imr,					-- Master Reset
			clk			=> clk20,					-- Master Clock
			AD_MR		=> AD_CLR,				-- Reset Accumulator
			pbusy		=> pbusy,				-- Pulse Busy Flag
			PDone_In		=> Peak_Done,				-- peak Done Flag
			Pur_In		=> Pur,					-- Pile-Up Reject Flag
			tc_sel		=> tc_sel,				-- AmpTime Select
			PS_SEL		=> PS_SEL,				-- BLR Selection (0=Off,Low,Med,High)
			tlevel		=> atlevel( 63 downto 48 ),	-- Threshold Level
			fir_in		=> fir_Data( 87 downto 64 ),	-- Main Channel Filter Data
			offset		=> offset,
			PDone_Out		=> BLR_PDONE,				-- Delayed Peak Done
			Pur_Out		=> BLR_PUR,				-- Delayed Pile-Up Reject
			PBUsy_Out		=> BLR_PBUSY,				-- Delayed Pulse Busy
			fir_out		=> fir_Data( 103 downto 88 ),	-- BLR Corrected Filter Data
			blevel_upd	=> Blevel_Upd,				-- Baseline Level Update
			blevel		=> blevel );				-- Baseline Level
     -------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- fir_dfilter
------------------------------------------------------------------------------------