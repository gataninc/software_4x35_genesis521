
-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_edisc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the Medium and Low Speed FIR data 
--		It includes the Delay Lines with the appropriate address
--		counters.
--	
--		It also generates the "Threshold Level" comparison for the
--		input to the Pulse Steering Logic
--
--		fir = ( adata - 2*adata_del1 ) + adata_del2
--
--		High Speed 	= 0.5us
--		Medium Speed 	= 2.0us
--		Low Speed 	= 8.0us
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
-------------------------------------------------------------------------------
	-- Fir_EDisc has a pipeline delay from the actual peak to the "Peak Detected Output" depending on the Peaking time
	-- This parameter is the "of

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_edisc is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		AD_MR		: in		std_logic;					-- Clear's FIR
		PA_Reset		: in		std_logic;
		Disc_En		: in		std_logic;
		TimeConst		: in		std_logic_Vector(  3 downto 0 );
		data_abc		: in		std_logic_Vector( 47 downto 0 );	-- First TAP
		tlevel		: in		std_logic_vector( 15 downto 0 );
		Level_Cmp		: buffer	std_logic;
		Level_Out		: buffer	std_logic;					-- Discriminator Level Exceeded
		fir_out		: buffer	std_logic_Vector( 15 downto 0 ));	-- FIR Output ( for Debug )
end fir_edisc;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
architecture behavioral of fir_edisc is
	constant Zeroes		: std_logic_Vector( 23 downto 0 ) := x"000000";
	constant Din_MSB 		: integer := 15;
	constant Acc_Bits		: integer := 30;
	constant Mux_start		: integer := 1;	-- was 1 -- was 2; -- was 3;
	
	constant Slope_Vec_Max	: integer := 6;


	constant tc0 			: integer := 0;	--   0.4us
	constant tc1 			: integer := 1;	--   0.8us
	constant tc2			: integer := 0;	--   1.6us
	constant tc3			: integer := 1;	--   3.2us
	constant tc4 			: integer := 2;	--   6.4us
	constant tc5 			: integer := 3;	--  12.8us
	constant tc6 			: integer := 4;	--  25.6us
	constant tc7 			: integer := 5;	--  51.2us
	constant tc8 			: integer := 6;	-- 102.4us

	signal adata_del 		: std_logic_Vector( 15 downto 0 );
	signal adata_Del1		: std_logic_Vector( 15 downto 0 );
	signal adata_del2		: std_logic_Vector( 15 downto 0 );

	signal add_vec1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal add_vec2		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_vec1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total1		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total2a		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal sub_total2		: std_logic_Vector( Acc_Bits-1 downto 0 );
	
	signal fir_Data		: std_logic_vector( Acc_Bits-1 downto 0 );
	signal fir_data_sub		: std_logic_vector( Acc_Bits-1 downto 0 );

	signal iPSlope			: std_logic;
	signal pslope_vec		: std_logic_vector( slope_Vec_max downto 0 );
	signal PSlope_Del		: std_logic;
	signal slope_radr 		: std_logic_vector( 6 downto 0 );
	signal slope_wadr		: std_logic_vector( 6 downto 0 );
	signal slope_diff		: std_logic_vector( 6 downto 0 );

	signal level_cmp_Del_vec	: std_logic_vector( 1 downto 0 );

	signal tlevel0 		: std_logic_Vector( (9*Acc_Bits)-1 downto 0 );
	signal tlevel_Mux 		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal fir_Data_del		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal Enable_Level_Str	: std_logic;
	signal fir_diff		: std_logic_Vector( Acc_Bits-1 downto 0 );
	signal TimeConst_Reg	: std_logic_vector(  3 downto 0 );
	signal Tlevel_Reg		: std_logic_vector( 15 downto 0 );
	signal PSlope_Cmp		: std_logic;
	signal levelcmp		: std_logic;
begin
	TimeConst_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 4,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> TimeConst,
			q				=> TimeConst_Reg );

	tlevel_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> tlevel,
			q				=> tlevel_reg );

	adata_del 	<= data_abc( 15 downto  0 );	-- dataa
	adata_Del1	<= data_abc( 31 downto 16 );	-- databc( 13 downto 0 );
	adata_del2	<= data_abc( 47 downto 32 );	-- databc( 27 downto 14 );
     -------------------------------------------------------------------------------
	-- Create the three Vectors for the Filter Generation
	sext1_gen : for i in Din_MSB+1 to Acc_Bits-1 generate
		add_vec1(i) <= adata_del(Din_MSB);
		add_vec2(i) <= adata_del2(Din_MSB);
	end generate;
	
	add_Vec1( Din_MSB downto 0 ) <= adata_del;
	add_vec2( Din_MSB downto 0 ) <= adata_del2;

	sext2_gen : for i in Din_MSB+2 to Acc_Bits-1 generate
		sub_vec1(i) <= adata_Del1(Din_MSB);
	end generate;
	sub_vec1( Din_MSB+1 downto 0 ) <= adata_del1 & '0';
	-- Create the three Vectors for the Filter Generation
	--------------------------------------------------------------------------
			
	--------------------------------------------------------------------------
	-- FIR - First Stage 
	sub1 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',				-- Sub
			dataa			=> add_Vec1,			-- adata
			datab			=> sub_vec1,			-- 2Xadata_del1
			result			=> sub_total1 );		-- dataa-datab
	-- FIR - First Stage 
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- FIR - Second Stage 
	add1 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',			-- Add
			dataa			=> sub_total1,		-- ( dataa-datab)-datab_del3
			datab			=> add_vec2,		-- adata_del2
			result			=> sub_total2a );	-- (( dataa-datab)-datab_del3 ) + datac_del3
			
	add1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			data				=> sub_total2a,
			q				=> sub_total2 );
	-- FIR - Second Stage 
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- FIR - Third Stage 
	-- Accumulate the previous sub_total with the FIR value	
	add2a : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',			-- Add
			dataa			=> sub_total2,
			datab			=> fir_data,
			result			=> fir_data_sub );

	add2b_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Bits,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			sclr				=> AD_MR,	
			clock			=> CLK,
			data				=> fir_data_sub,
			q				=> fir_data );
	-- FIR - Third Stage 
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- TLevel0 Generator - Sign Extend TLEVEL to ACC_BITS
	tlevel_all_gen : for j in 0 to 8 generate
		tlevel_Gen0a : for i in Mux_Start+16+j to Acc_Bits-1 Generate
			tlevel0((Acc_Bits*j)+i) <= tlevel_Reg(15);
		end generate;
		
		tlevel_Gen0b : for i in 0 to 15 Generate
			tlevel0((Acc_Bits*j)+i+Mux_Start+j) <= tlevel_Reg(i);
		end generate;

		tlevel_Gan0c : for i in 0 to Mux_Start+j-1 Generate
			tlevel0((Acc_Bits*j)+i) <= '0';
		end generate;
	end generate;
	-- TLevel0 Generator
     --------------------------------------------------------------------------

	-- Compare Fir output with the Adjusted Threshold Level"
	tlevel_cmp : lpm_compare
		generic map(
			lpm_width			=> Acc_Bits,
			lpm_representation	=> "SIGNED",
			lpm_type			=> "LPM_COMPARE" )
		port map(
			dataa			=> fir_data,
			datab			=> tlevel_mux, 
			agb				=> LevelCmp );		
	
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Slope Determination
	-- Change to DPRAM with an offset dependent on peaking time
	-- for TimeConst less than  34, use a diff of 4(-2)
	-- for TimeConst less than 100, use a diff of 16(-2)
	-- for all others 		       use a diff of 40hex(-2)
	with conv_integer( TimeConst_Reg ) select
		slope_diff <= "0000000" when 0,		--   0.4
		              "0000000" when 1,		--   0.8
	                   "0000000" when 2,		--   1.6
		              "0000111" when 3,		--   3.2
		              "0000111" when 4,		--   6.2
		              "0000111" when 5,		--  12.8
		              "0000111" when 6,		--  25.6
		              "0111111" when 7,		--  51.2
		              "1111111" when others;	-- 102.4



--	with conv_integer( TimeConst ) select
--		slope_diff <= "000000" when 0,		--   0.4
--		              "000000" when 1,		--   0.8
--	                   "000000" when 2,		--   1.6
--		              "000010" when 3,		--   3.2
--		              "000010" when 4,		--   6.2
--		              "000010" when 5,		--  12.8
--		              "000010" when 6,		--  25.6
--		              "011110" when 7,		--  51.2
--		              "111110" when others;	-- 102.4

	slope_wadr_cntr : lpm_counter
		generic map(
			LPM_WIDTH		=> 7,
			LPM_TYPE		=> "LPM_COUNTER" )
		port map(
			aclr			=> IMR,
			clock		=> CLK,
			q			=> slope_wadr );

	slope_radr <= slope_wadr - slope_Diff;
		
	Slope_Gen_Mem : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Acc_Bits,	-- Data Width
			LPM_WIDTHAD			=> 7,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT.MIF",
			LPM_TYPE				=> "LPM_RAM_DP" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress				=> slope_radr,
			wraddress 			=> slope_wadr,
			data					=> fir_data,
			q					=> fir_data_del  );		-- 1us Overall Delay
		
	pslope_diff_gen : lpm_add_sub
		generic map(
			LPM_WIDTH				=> Acc_Bits,	-- Data Width
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			Cin					=> '1',			-- Cary In
			dataa				=> fir_data,
			datab				=> fir_data_del,
			result				=> fir_diff );

	pslope_Gen_Comp1 : lpm_compare
		generic map(
			LPM_WIDTH				=> Acc_Bits,	-- Data Width
			LPM_REPRESENTATION		=> "SIGNED",
			LPM_TYPE				=> "LPM_COMPARE" )
		port map(
			Dataa				=> fir_data,
			datab				=> fir_data_del,
			agb					=> ipslope );
	-- Slope Determination
	-------------------------------------------------------------------------------
	with Conv_Integer( TimeConst_Reg ) select
		Enable_Level_Str <= '1' when 0,
					     '1' when 1,
					 	'0' when others;
	-------------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin 
		if( imr = '1' ) then
			for i in 0 to slope_vec_max loop
				pslope_vec(i) <= '0';
			end loop;
			PSlope_cmp		<= '0';
			PSlope_Del		<= '0';
			level_cmp_Del_Vec	<= "00";

			Level_out			<= '0';
			level_Cmp			<= '0';
			Fir_Out			<= x"0000";

			for i in 0 to Acc_Bits-1 loop
				tlevel_Mux(i) <= '0';
			end loop;

		elsif(( clk'event ) and ( clk = '1' )) then
			PSlope_Del 		<= PSlope_cmp;
			Level_Cmp			<= levelcmp;
			level_cmp_del_Vec 	<= level_Cmp_del_Vec(0) & levelcmp;
			
			if( ipslope = '1' )
				then pslope_vec 		<= pslope_vec( slope_vec_max-1 downto 0 ) & '1';
				else pslope_vec 		<= pslope_vec( slope_vec_max-1 downto 0 ) & '0';
			end if;
			
			
			if( TimeConst_Reg( 3 downto 1 ) = "000" ) then
				if( PSlope_Vec( 2 downto 0 ) = "111" ) 
					then PSlope_cmp <= '1';
				elsif( PSlope_Vec( 2 downto 0 ) = "000" )
					then PSlope_cmp <= '0';
				end if;
			else
				if( PSlope_Vec( 5 downto 0 ) = "111111" ) 
					then PSlope_cmp <= '1';
				elsif( PSlope_Vec( 5 downto 0 ) = "000000" )
					then PSlope_cmp <= '0';
				end if;
			end if;
			
			if(   ( PA_Reset = '0' ) and ( Disc_En = '1' ) and ( LevelCmp        = '1' ) and ( PSlope_cmp = '0' ) and ( PSLope_Del = '1' ))
				then Level_Out <= '1';
			elsif(( PA_Reset = '0' ) and ( Disc_En = '1' ) and ( Enable_Level_Str = '1' ) and ( Level_Cmp_del_Vec(0) = '1' ) and ( PSlope_cmp = '0' ) and ( PSLope_Del = '1' ))
				then Level_Out <= '1';
			elsif(( PA_Reset = '0' ) and ( Disc_En = '1' ) and ( Enable_Level_Str = '1' ) and ( Level_Cmp_del_Vec(1) = '1' ) and ( PSlope_cmp = '0' ) and ( PSLope_Del = '1' ))
				then Level_Out <= '1';
				else Level_Out <= '0';
			end if;
			
			if( PA_Reset = '1' )
				then Fir_Out <= x"0000";		
			else 
				case Conv_Integer( TimeConst_Reg ) is
					when 0 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc0 downto Mux_Start+tc0 );
					when 1 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc1 downto Mux_Start+tc1 );
					when 2 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc2 downto Mux_Start+tc2 );
					when 3 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc3 downto Mux_Start+tc3 );
					when 4 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc4 downto Mux_Start+tc4 );
					when 5 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc5 downto Mux_Start+tc5 );
					when 6 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc6 downto Mux_Start+tc6 );
					when 7 	  => Fir_Out <= Fir_Data( Mux_Start+15+tc7 downto Mux_Start+tc7 );
					when others => Fir_Out <= Fir_Data( Mux_Start+15+tc8 downto Mux_Start+tc8 );
				end case;
			end if;
		
			case Conv_Integer( timeconst_Reg ) is
				when 0 	  => tlevel_mux <= tlevel0( (1*Acc_Bits)-1 downto 0*Acc_Bits );
				when 1 	  => tlevel_mux <= tlevel0( (2*Acc_Bits)-1 downto 1*Acc_Bits );
				when 2 	  => tlevel_mux <= tlevel0( (3*Acc_Bits)-1 downto 2*Acc_Bits );
				when 3 	  => tlevel_mux <= tlevel0( (4*Acc_Bits)-1 downto 3*Acc_Bits );
				when 4 	  => tlevel_mux <= tlevel0( (5*Acc_Bits)-1 downto 4*Acc_Bits );
				when 5 	  => tlevel_mux <= tlevel0( (6*Acc_Bits)-1 downto 5*Acc_Bits );
				when 6 	  => tlevel_mux <= tlevel0( (7*Acc_Bits)-1 downto 6*Acc_Bits );
				when 7 	  => tlevel_mux <= tlevel0( (8*Acc_Bits)-1 downto 7*Acc_Bits );
				when others => tlevel_mux <= tlevel0( (9*Acc_Bits)-1 downto 8*Acc_Bits );
			end case;			
		end if;
	end process;
     --------------------------------------------------------------------------


------------------------------------------------------------------------------------
end behavioral;               -- fir_edisc
------------------------------------------------------------------------------------

