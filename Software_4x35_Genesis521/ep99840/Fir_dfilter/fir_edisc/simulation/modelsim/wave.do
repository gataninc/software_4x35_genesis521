onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/ad_mr
add wave -noupdate -format Logic -radix binary /tb/pa_Reset
add wave -noupdate -format Logic -radix binary /tb/disc_en
add wave -noupdate -format Logic -radix hexadecimal /tb/timeConst
add wave -noupdate -format Logic -radix hexadecimal /tb/tlevel
add wave -noupdate -format Logic -radix hexadecimal /tb/adata
add wave -noupdate -format Logic -radix hexadecimal /tb/dly_data
add wave -noupdate -format Logic -radix binary /tb/level_cmp
add wave -noupdate -format Logic -radix binary /tb/level_out
add wave -noupdate -format Logic -radix hexadecimal /tb/fir_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {2863 ns} {232426 ns} {5963 ns}
WaveRestoreZoom {0 ns} {11132 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
