-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:50:03"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_edisc IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	AD_MR : IN std_logic;
	PA_Reset : IN std_logic;
	Disc_En : IN std_logic;
	TimeConst : IN std_logic_vector(3 DOWNTO 0);
	data_abc : IN std_logic_vector(47 DOWNTO 0);
	tlevel : IN std_logic_vector(15 DOWNTO 0);
	Level_Cmp : OUT std_logic;
	Level_Out : OUT std_logic;
	fir_out : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_edisc;

ARCHITECTURE structure OF fir_edisc IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_AD_MR : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_Disc_En : std_logic;
SIGNAL ww_TimeConst : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_data_abc : std_logic_vector(47 DOWNTO 0);
SIGNAL ww_tlevel : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Level_Cmp : std_logic;
SIGNAL ww_Level_Out : std_logic;
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a29_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a29_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a28_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a28_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a27_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a27_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a26_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a26_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a25_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a25_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a24_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a24_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a23_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a23_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a22_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a22_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a21_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a21_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a20_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a20_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a19_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a19_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a18_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a18_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a17_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a17_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a16_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a16_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a29_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a28_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a27_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a26_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a25_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a24_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a23_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a22_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a21_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a20_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a19_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a18_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a17_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a16_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Slope_Gen_Mem_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL PSlope_Del : std_logic;
SIGNAL Mux_a2152 : std_logic;
SIGNAL tlevel_Mux_a29_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a28_a_a2073 : std_logic;
SIGNAL add1_ff_adffs_a2_a_a127 : std_logic;
SIGNAL add1_ff_adffs_a2_a : std_logic;
SIGNAL add1_ff_adffs_a1_a_a130 : std_logic;
SIGNAL add1_ff_adffs_a1_a : std_logic;
SIGNAL add1_ff_adffs_a7_a_a133 : std_logic;
SIGNAL add1_ff_adffs_a7_a : std_logic;
SIGNAL add1_ff_adffs_a5_a_a136 : std_logic;
SIGNAL add1_ff_adffs_a5_a : std_logic;
SIGNAL add1_ff_adffs_a4_a_a139 : std_logic;
SIGNAL add1_ff_adffs_a4_a : std_logic;
SIGNAL add1_ff_adffs_a3_a_a142 : std_logic;
SIGNAL add1_ff_adffs_a3_a : std_logic;
SIGNAL add1_ff_adffs_a6_a_a145 : std_logic;
SIGNAL add1_ff_adffs_a6_a : std_logic;
SIGNAL add1_ff_adffs_a8_a_a148 : std_logic;
SIGNAL add1_ff_adffs_a8_a : std_logic;
SIGNAL add1_ff_adffs_a9_a_a151 : std_logic;
SIGNAL add1_ff_adffs_a9_a : std_logic;
SIGNAL add1_ff_adffs_a10_a_a154 : std_logic;
SIGNAL add1_ff_adffs_a10_a : std_logic;
SIGNAL add1_ff_adffs_a11_a_a157 : std_logic;
SIGNAL add1_ff_adffs_a11_a : std_logic;
SIGNAL add1_ff_adffs_a12_a_a160 : std_logic;
SIGNAL add1_ff_adffs_a12_a : std_logic;
SIGNAL add1_ff_adffs_a13_a_a163 : std_logic;
SIGNAL add1_ff_adffs_a13_a : std_logic;
SIGNAL add1_ff_adffs_a14_a_a166 : std_logic;
SIGNAL add1_ff_adffs_a14_a : std_logic;
SIGNAL add1_ff_adffs_a15_a_a169 : std_logic;
SIGNAL add1_ff_adffs_a15_a : std_logic;
SIGNAL add1_ff_adffs_a16_a_a172 : std_logic;
SIGNAL add1_ff_adffs_a16_a : std_logic;
SIGNAL add1_ff_adffs_a17_a_a175 : std_logic;
SIGNAL add1_ff_adffs_a17_a : std_logic;
SIGNAL add1_ff_adffs_a29_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a27_a_a2074 : std_logic;
SIGNAL level_cmp_Del_vec_a1_a : std_logic;
SIGNAL Enable_Level_Str_a18 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a579 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a577 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a583 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a581 : std_logic;
SIGNAL add1_ff_adffs_a0_a_a181 : std_logic;
SIGNAL add1_ff_adffs_a0_a : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a587 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a585 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a591 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a589 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a595 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a593 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a599 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a597 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a603 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a601 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a607 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a605 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a611 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a609 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a615 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a613 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a619 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a617 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a623 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a621 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a627 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a625 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a631 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a629 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a635 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a633 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a639 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a637 : std_logic;
SIGNAL sub1_aadder_aresult_node_acs_buffer_a1_a_a641 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a26_a_a2075 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a_a2080 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a25_a_a2076 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a_a2081 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a24_a_a2077 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a_a2082 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a23_a_a2078 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a_a2083 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a22_a_a2079 : std_logic;
SIGNAL tlevel_Mux_a23_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a_a2084 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a21_a_a2080 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a_a2085 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a20_a_a2081 : std_logic;
SIGNAL tlevel_Mux_a21_a : std_logic;
SIGNAL tlevel_Mux_a22_a_a998 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a_a2086 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a19_a_a2082 : std_logic;
SIGNAL tlevel_Mux_a20_a : std_logic;
SIGNAL tlevel_Mux_a21_a_a1001 : std_logic;
SIGNAL tlevel_Mux_a21_a_a1002 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a_a2087 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a18_a_a2083 : std_logic;
SIGNAL tlevel_Mux_a20_a_a1008 : std_logic;
SIGNAL Mux_a2186 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a_a2088 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a17_a_a2084 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a_a2089 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a16_a_a2085 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a_a2090 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a15_a_a2086 : std_logic;
SIGNAL tlevel_Mux_a17_a_a1017 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a_a2091 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a14_a_a2087 : std_logic;
SIGNAL tlevel_Mux_a15_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a_a2092 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a13_a_a2088 : std_logic;
SIGNAL tlevel_Mux_a14_a : std_logic;
SIGNAL Mux_a2195 : std_logic;
SIGNAL Mux_a2196 : std_logic;
SIGNAL tlevel_Mux_a15_a_a1024 : std_logic;
SIGNAL tlevel_reg_ff_adffs_a6_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a_a2093 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a12_a_a2089 : std_logic;
SIGNAL tlevel_Mux_a13_a : std_logic;
SIGNAL Mux_a2197 : std_logic;
SIGNAL Mux_a2198 : std_logic;
SIGNAL tlevel_Mux_a14_a_a1026 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a_a2094 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a11_a_a2090 : std_logic;
SIGNAL tlevel_Mux_a12_a : std_logic;
SIGNAL Mux_a2199 : std_logic;
SIGNAL Mux_a2200 : std_logic;
SIGNAL tlevel_Mux_a13_a_a1028 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a_a2095 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a10_a_a2091 : std_logic;
SIGNAL tlevel_Mux_a11_a : std_logic;
SIGNAL tlevel_Mux_a12_a_a1030 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a_a2096 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a9_a_a2092 : std_logic;
SIGNAL tlevel_Mux_a10_a : std_logic;
SIGNAL Mux_a2203 : std_logic;
SIGNAL Mux_a2204 : std_logic;
SIGNAL tlevel_Mux_a11_a_a1032 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a_a2097 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a8_a_a2093 : std_logic;
SIGNAL tlevel_Mux_a9_a : std_logic;
SIGNAL tlevel_Mux_a10_a_a1034 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a_a2098 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a7_a_a2094 : std_logic;
SIGNAL Mux_a2207 : std_logic;
SIGNAL Mux_a2208 : std_logic;
SIGNAL tlevel_Mux_a9_a_a1036 : std_logic;
SIGNAL tlevel_reg_ff_adffs_a0_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a_a2099 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a6_a_a2095 : std_logic;
SIGNAL tlevel_Mux_a7_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a_a2100 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a5_a_a2096 : std_logic;
SIGNAL Mux_a2211 : std_logic;
SIGNAL Mux_a2212 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a_a2101 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a4_a_a2097 : std_logic;
SIGNAL tlevel_Mux_a5_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a_a2102 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a3_a_a2098 : std_logic;
SIGNAL Mux_a2215 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a_a2103 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a2_a_a2099 : std_logic;
SIGNAL tlevel_Mux_a3_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a_a2104 : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a1_a_a2100 : std_logic;
SIGNAL tlevel_Mux_a2_a : std_logic;
SIGNAL Mux_a2221 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a_a2105 : std_logic;
SIGNAL tlevel_Mux_a1_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a_a2106 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a_a2107 : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a_a2108 : std_logic;
SIGNAL data_abc_a34_a_acombout : std_logic;
SIGNAL data_abc_a33_a_acombout : std_logic;
SIGNAL data_abc_a39_a_acombout : std_logic;
SIGNAL data_abc_a37_a_acombout : std_logic;
SIGNAL data_abc_a36_a_acombout : std_logic;
SIGNAL data_abc_a35_a_acombout : std_logic;
SIGNAL data_abc_a38_a_acombout : std_logic;
SIGNAL data_abc_a40_a_acombout : std_logic;
SIGNAL data_abc_a41_a_acombout : std_logic;
SIGNAL data_abc_a42_a_acombout : std_logic;
SIGNAL data_abc_a43_a_acombout : std_logic;
SIGNAL data_abc_a44_a_acombout : std_logic;
SIGNAL data_abc_a45_a_acombout : std_logic;
SIGNAL data_abc_a46_a_acombout : std_logic;
SIGNAL data_abc_a47_a_acombout : std_logic;
SIGNAL data_abc_a17_a_acombout : std_logic;
SIGNAL data_abc_a2_a_acombout : std_logic;
SIGNAL data_abc_a16_a_acombout : std_logic;
SIGNAL data_abc_a1_a_acombout : std_logic;
SIGNAL data_abc_a32_a_acombout : std_logic;
SIGNAL data_abc_a0_a_acombout : std_logic;
SIGNAL data_abc_a22_a_acombout : std_logic;
SIGNAL data_abc_a7_a_acombout : std_logic;
SIGNAL data_abc_a20_a_acombout : std_logic;
SIGNAL data_abc_a5_a_acombout : std_logic;
SIGNAL data_abc_a19_a_acombout : std_logic;
SIGNAL data_abc_a4_a_acombout : std_logic;
SIGNAL data_abc_a18_a_acombout : std_logic;
SIGNAL data_abc_a3_a_acombout : std_logic;
SIGNAL data_abc_a21_a_acombout : std_logic;
SIGNAL data_abc_a6_a_acombout : std_logic;
SIGNAL data_abc_a23_a_acombout : std_logic;
SIGNAL data_abc_a8_a_acombout : std_logic;
SIGNAL data_abc_a24_a_acombout : std_logic;
SIGNAL data_abc_a9_a_acombout : std_logic;
SIGNAL data_abc_a25_a_acombout : std_logic;
SIGNAL data_abc_a10_a_acombout : std_logic;
SIGNAL data_abc_a26_a_acombout : std_logic;
SIGNAL data_abc_a11_a_acombout : std_logic;
SIGNAL data_abc_a27_a_acombout : std_logic;
SIGNAL data_abc_a12_a_acombout : std_logic;
SIGNAL data_abc_a28_a_acombout : std_logic;
SIGNAL data_abc_a13_a_acombout : std_logic;
SIGNAL data_abc_a29_a_acombout : std_logic;
SIGNAL data_abc_a14_a_acombout : std_logic;
SIGNAL data_abc_a30_a_acombout : std_logic;
SIGNAL data_abc_a15_a_acombout : std_logic;
SIGNAL data_abc_a31_a_acombout : std_logic;
SIGNAL tlevel_a6_a_acombout : std_logic;
SIGNAL tlevel_a0_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL AD_MR_acombout : std_logic;
SIGNAL add2b_ff_adffs_a0_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101 : std_logic;
SIGNAL add2b_ff_adffs_a0_a_a408 : std_logic;
SIGNAL add2b_ff_adffs_a1_a : std_logic;
SIGNAL add2b_ff_adffs_a1_a_a342 : std_logic;
SIGNAL add2b_ff_adffs_a2_a : std_logic;
SIGNAL add2b_ff_adffs_a2_a_a339 : std_logic;
SIGNAL add2b_ff_adffs_a3_a : std_logic;
SIGNAL add2b_ff_adffs_a3_a_a354 : std_logic;
SIGNAL add2b_ff_adffs_a4_a : std_logic;
SIGNAL add2b_ff_adffs_a4_a_a351 : std_logic;
SIGNAL add2b_ff_adffs_a5_a : std_logic;
SIGNAL add2b_ff_adffs_a5_a_a348 : std_logic;
SIGNAL add2b_ff_adffs_a6_a : std_logic;
SIGNAL add2b_ff_adffs_a6_a_a357 : std_logic;
SIGNAL add2b_ff_adffs_a7_a : std_logic;
SIGNAL add2b_ff_adffs_a7_a_a345 : std_logic;
SIGNAL add2b_ff_adffs_a8_a : std_logic;
SIGNAL add2b_ff_adffs_a8_a_a360 : std_logic;
SIGNAL add2b_ff_adffs_a9_a : std_logic;
SIGNAL add2b_ff_adffs_a9_a_a363 : std_logic;
SIGNAL add2b_ff_adffs_a10_a : std_logic;
SIGNAL add2b_ff_adffs_a10_a_a366 : std_logic;
SIGNAL add2b_ff_adffs_a11_a : std_logic;
SIGNAL add2b_ff_adffs_a11_a_a369 : std_logic;
SIGNAL add2b_ff_adffs_a12_a : std_logic;
SIGNAL add2b_ff_adffs_a12_a_a372 : std_logic;
SIGNAL add2b_ff_adffs_a13_a : std_logic;
SIGNAL add2b_ff_adffs_a13_a_a375 : std_logic;
SIGNAL add2b_ff_adffs_a14_a : std_logic;
SIGNAL add2b_ff_adffs_a14_a_a378 : std_logic;
SIGNAL add2b_ff_adffs_a15_a : std_logic;
SIGNAL add2b_ff_adffs_a15_a_a381 : std_logic;
SIGNAL add2b_ff_adffs_a16_a : std_logic;
SIGNAL add2b_ff_adffs_a16_a_a384 : std_logic;
SIGNAL add2b_ff_adffs_a17_a : std_logic;
SIGNAL add2b_ff_adffs_a17_a_a387 : std_logic;
SIGNAL add2b_ff_adffs_a18_a : std_logic;
SIGNAL add2b_ff_adffs_a18_a_a390 : std_logic;
SIGNAL add2b_ff_adffs_a19_a : std_logic;
SIGNAL add2b_ff_adffs_a19_a_a393 : std_logic;
SIGNAL add2b_ff_adffs_a20_a : std_logic;
SIGNAL add2b_ff_adffs_a20_a_a396 : std_logic;
SIGNAL add2b_ff_adffs_a21_a : std_logic;
SIGNAL add2b_ff_adffs_a21_a_a399 : std_logic;
SIGNAL add2b_ff_adffs_a22_a : std_logic;
SIGNAL add2b_ff_adffs_a22_a_a402 : std_logic;
SIGNAL add2b_ff_adffs_a23_a : std_logic;
SIGNAL add2b_ff_adffs_a23_a_a426 : std_logic;
SIGNAL add2b_ff_adffs_a24_a : std_logic;
SIGNAL add2b_ff_adffs_a24_a_a423 : std_logic;
SIGNAL add2b_ff_adffs_a25_a : std_logic;
SIGNAL add2b_ff_adffs_a25_a_a420 : std_logic;
SIGNAL add2b_ff_adffs_a26_a : std_logic;
SIGNAL add2b_ff_adffs_a26_a_a417 : std_logic;
SIGNAL add2b_ff_adffs_a27_a : std_logic;
SIGNAL add2b_ff_adffs_a27_a_a414 : std_logic;
SIGNAL add2b_ff_adffs_a28_a : std_logic;
SIGNAL add2b_ff_adffs_a28_a_a411 : std_logic;
SIGNAL add2b_ff_adffs_a29_a : std_logic;
SIGNAL TimeConst_a0_a_acombout : std_logic;
SIGNAL TimeConst_FF_adffs_a0_a : std_logic;
SIGNAL TimeConst_a2_a_acombout : std_logic;
SIGNAL TimeConst_FF_adffs_a2_a : std_logic;
SIGNAL TimeConst_a3_a_acombout : std_logic;
SIGNAL TimeConst_FF_adffs_a3_a : std_logic;
SIGNAL tlevel_a15_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a15_a : std_logic;
SIGNAL Mux_a2227 : std_logic;
SIGNAL tlevel_Mux_a22_a_a1041 : std_logic;
SIGNAL fir_out_a0_a_a1952 : std_logic;
SIGNAL tlevel_a14_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a14_a : std_logic;
SIGNAL tlevel_Mux_a22_a_a999 : std_logic;
SIGNAL tlevel_Mux_a22_a : std_logic;
SIGNAL tlevel_a10_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a10_a : std_logic;
SIGNAL tlevel_Mux_a19_a_a1011 : std_logic;
SIGNAL tlevel_a12_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a12_a : std_logic;
SIGNAL tlevel_a11_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a11_a : std_logic;
SIGNAL tlevel_a13_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a13_a : std_logic;
SIGNAL Mux_a2187 : std_logic;
SIGNAL Mux_a2188 : std_logic;
SIGNAL tlevel_Mux_a20_a_a1007 : std_logic;
SIGNAL tlevel_Mux_a19_a : std_logic;
SIGNAL Mux_a2189 : std_logic;
SIGNAL Mux_a2190 : std_logic;
SIGNAL tlevel_a9_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a9_a : std_logic;
SIGNAL tlevel_Mux_a18_a_a1014 : std_logic;
SIGNAL TimeConst_a1_a_acombout : std_logic;
SIGNAL TimeConst_FF_adffs_a1_a : std_logic;
SIGNAL Enable_Level_Str_a21 : std_logic;
SIGNAL tlevel_Mux_a18_a_a1042 : std_logic;
SIGNAL tlevel_Mux_a18_a : std_logic;
SIGNAL Mux_a2191 : std_logic;
SIGNAL Mux_a2192 : std_logic;
SIGNAL tlevel_Mux_a17_a_a1003 : std_logic;
SIGNAL tlevel_Mux_a17_a_a1016 : std_logic;
SIGNAL tlevel_Mux_a17_a : std_logic;
SIGNAL Mux_a2184 : std_logic;
SIGNAL Mux_a2185 : std_logic;
SIGNAL Mux_a2193 : std_logic;
SIGNAL tlevel_a8_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a8_a : std_logic;
SIGNAL Mux_a2194 : std_logic;
SIGNAL tlevel_Mux_a16_a_a1021 : std_logic;
SIGNAL tlevel_Mux_a16_a : std_logic;
SIGNAL tlevel_a7_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a7_a : std_logic;
SIGNAL tlevel_a5_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a5_a : std_logic;
SIGNAL Mux_a2201 : std_logic;
SIGNAL tlevel_a4_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a4_a : std_logic;
SIGNAL Mux_a2202 : std_logic;
SIGNAL Enable_Level_Str_a16 : std_logic;
SIGNAL tlevel_a2_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a2_a : std_logic;
SIGNAL tlevel_a3_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a3_a : std_logic;
SIGNAL tlevel_a1_a_acombout : std_logic;
SIGNAL tlevel_reg_ff_adffs_a1_a : std_logic;
SIGNAL Mux_a2209 : std_logic;
SIGNAL Mux_a2210 : std_logic;
SIGNAL tlevel_Mux_a8_a : std_logic;
SIGNAL Mux_a2205 : std_logic;
SIGNAL Mux_a2206 : std_logic;
SIGNAL fir_out_a0_a_a1956 : std_logic;
SIGNAL Mux_a2225 : std_logic;
SIGNAL tlevel_Mux_a6_a : std_logic;
SIGNAL tlevel_Mux_a4_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL tlevel_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL level_cmp_Del_vec_a0_a : std_logic;
SIGNAL Enable_Level_Str_a20 : std_logic;
SIGNAL Level_Out_a107 : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL slope_wadr_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL slope_diff_a2_a_a54 : std_logic;
SIGNAL add_a113 : std_logic;
SIGNAL add_a115 : std_logic;
SIGNAL add_a117 : std_logic;
SIGNAL add_a119 : std_logic;
SIGNAL add_a121 : std_logic;
SIGNAL slope_diff_a5_a_a55 : std_logic;
SIGNAL add_a123 : std_logic;
SIGNAL add_a125 : std_logic;
SIGNAL add_a127 : std_logic;
SIGNAL add_a129 : std_logic;
SIGNAL add_a131 : std_logic;
SIGNAL add_a133 : std_logic;
SIGNAL add_a135 : std_logic;
SIGNAL add_a137 : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a29_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a28_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a27_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a26_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a25_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a24_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a23_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a22_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a21_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a20_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a19_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a18_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a17_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a16_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a15_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a14_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a13_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a12_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a11_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a10_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a9_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a8_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a7_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a6_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a5_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a4_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a3_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a2_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a1_a : std_logic;
SIGNAL Slope_Gen_Mem_asram_aq_a0_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL pslope_Gen_Comp1_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL pslope_vec_a0_a : std_logic;
SIGNAL pslope_vec_a1_a : std_logic;
SIGNAL pslope_vec_a2_a : std_logic;
SIGNAL pslope_vec_a3_a : std_logic;
SIGNAL pslope_vec_a4_a : std_logic;
SIGNAL pslope_vec_a5_a : std_logic;
SIGNAL PSlope_Cmp_a410 : std_logic;
SIGNAL PSlope_Cmp_a409 : std_logic;
SIGNAL PSlope_Cmp_a411 : std_logic;
SIGNAL PSlope_Cmp_a407 : std_logic;
SIGNAL PSlope_Cmp_a406 : std_logic;
SIGNAL PSlope_Cmp_a408 : std_logic;
SIGNAL PSlope_Cmp : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL Disc_En_acombout : std_logic;
SIGNAL Level_Out_a101 : std_logic;
SIGNAL Level_Out_areg0 : std_logic;
SIGNAL Mux_a2150 : std_logic;
SIGNAL Mux_a2151 : std_logic;
SIGNAL fir_out_a0_a_a1900 : std_logic;
SIGNAL fir_out_a0_a_a1901 : std_logic;
SIGNAL fir_out_a0_a_a1898 : std_logic;
SIGNAL fir_out_a0_a_a1899 : std_logic;
SIGNAL fir_out_a0_a_areg0 : std_logic;
SIGNAL Mux_a2153 : std_logic;
SIGNAL fir_out_a1_a_a1904 : std_logic;
SIGNAL fir_out_a1_a_a1903 : std_logic;
SIGNAL fir_out_a1_a_areg0 : std_logic;
SIGNAL fir_out_a2_a_a1907 : std_logic;
SIGNAL Mux_a2154 : std_logic;
SIGNAL Mux_a2155 : std_logic;
SIGNAL fir_out_a2_a_a1906 : std_logic;
SIGNAL fir_out_a2_a_areg0 : std_logic;
SIGNAL Mux_a2156 : std_logic;
SIGNAL Mux_a2157 : std_logic;
SIGNAL fir_out_a3_a_a1910 : std_logic;
SIGNAL fir_out_a3_a_a1909 : std_logic;
SIGNAL fir_out_a3_a_areg0 : std_logic;
SIGNAL Mux_a2158 : std_logic;
SIGNAL Mux_a2159 : std_logic;
SIGNAL fir_out_a4_a_a1913 : std_logic;
SIGNAL fir_out_a4_a_a1912 : std_logic;
SIGNAL fir_out_a4_a_areg0 : std_logic;
SIGNAL Mux_a2160 : std_logic;
SIGNAL Mux_a2161 : std_logic;
SIGNAL fir_out_a5_a_a1916 : std_logic;
SIGNAL fir_out_a5_a_a1915 : std_logic;
SIGNAL fir_out_a5_a_areg0 : std_logic;
SIGNAL fir_out_a6_a_a1919 : std_logic;
SIGNAL Mux_a2162 : std_logic;
SIGNAL Mux_a2163 : std_logic;
SIGNAL fir_out_a6_a_a1918 : std_logic;
SIGNAL fir_out_a6_a_areg0 : std_logic;
SIGNAL fir_out_a7_a_a1922 : std_logic;
SIGNAL Mux_a2164 : std_logic;
SIGNAL Mux_a2165 : std_logic;
SIGNAL fir_out_a7_a_a1921 : std_logic;
SIGNAL fir_out_a7_a_areg0 : std_logic;
SIGNAL Mux_a2166 : std_logic;
SIGNAL Mux_a2167 : std_logic;
SIGNAL fir_out_a8_a_a1925 : std_logic;
SIGNAL fir_out_a8_a_a1924 : std_logic;
SIGNAL fir_out_a8_a_areg0 : std_logic;
SIGNAL fir_out_a9_a_a1927 : std_logic;
SIGNAL fir_out_a9_a_a1928 : std_logic;
SIGNAL Mux_a2168 : std_logic;
SIGNAL Mux_a2169 : std_logic;
SIGNAL fir_out_a9_a_areg0 : std_logic;
SIGNAL Mux_a2170 : std_logic;
SIGNAL Mux_a2171 : std_logic;
SIGNAL fir_out_a10_a_a1931 : std_logic;
SIGNAL fir_out_a10_a_a1930 : std_logic;
SIGNAL fir_out_a10_a_areg0 : std_logic;
SIGNAL Mux_a2172 : std_logic;
SIGNAL Mux_a2173 : std_logic;
SIGNAL fir_out_a11_a_a1934 : std_logic;
SIGNAL fir_out_a11_a_a1933 : std_logic;
SIGNAL fir_out_a11_a_areg0 : std_logic;
SIGNAL fir_out_a12_a_a1936 : std_logic;
SIGNAL fir_out_a12_a_a1937 : std_logic;
SIGNAL Mux_a2174 : std_logic;
SIGNAL Mux_a2175 : std_logic;
SIGNAL fir_out_a12_a_areg0 : std_logic;
SIGNAL fir_out_a13_a_a1940 : std_logic;
SIGNAL Mux_a2176 : std_logic;
SIGNAL Mux_a2177 : std_logic;
SIGNAL fir_out_a13_a_a1939 : std_logic;
SIGNAL fir_out_a13_a_areg0 : std_logic;
SIGNAL Mux_a2178 : std_logic;
SIGNAL Mux_a2179 : std_logic;
SIGNAL fir_out_a14_a_a1943 : std_logic;
SIGNAL fir_out_a14_a_a1942 : std_logic;
SIGNAL fir_out_a14_a_areg0 : std_logic;
SIGNAL Mux_a2180 : std_logic;
SIGNAL Mux_a2181 : std_logic;
SIGNAL fir_out_a0_a_a1955 : std_logic;
SIGNAL fir_out_a15_a_a1954 : std_logic;
SIGNAL fir_out_a15_a_a1946 : std_logic;
SIGNAL fir_out_a15_a_areg0 : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_AD_MR <= AD_MR;
ww_PA_Reset <= PA_Reset;
ww_Disc_En <= Disc_En;
ww_TimeConst <= TimeConst;
ww_data_abc <= data_abc;
ww_tlevel <= tlevel;
Level_Cmp <= ww_Level_Cmp;
Level_Out <= ww_Level_Out;
fir_out <= ww_fir_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

Slope_Gen_Mem_asram_asegment_a0_a_a29_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a29_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a28_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a28_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a27_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a27_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a26_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a26_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a25_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a25_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a24_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a24_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a23_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a23_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a22_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a22_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a21_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a21_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a20_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a20_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a19_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a19_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a18_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a18_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a17_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a17_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a16_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a16_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

Slope_Gen_Mem_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & slope_wadr_cntr_awysi_counter_asload_path_a6_a & slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_wadr_cntr_awysi_counter_asload_path_a4_a & 
slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_wadr_cntr_awysi_counter_asload_path_a2_a & slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_wadr_cntr_awysi_counter_asload_path_a0_a);

Slope_Gen_Mem_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & add_a137 & add_a133 & add_a129 & add_a125 & add_a121 & add_a117 & add_a113);

PSlope_Del_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Del = DFFE(PSlope_Cmp, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PSlope_Cmp,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Del);

Mux_a2152_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2152 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a5_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA44",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a4_a,
	datac => add2b_ff_adffs_a5_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2152);

tlevel_Mux_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a29_a = DFFE(tlevel_reg_ff_adffs_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tlevel_reg_ff_adffs_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a29_a);

add1_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a2_a = DFFE(data_abc_a34_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a577 $ !add1_ff_adffs_a1_a_a130, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a2_a_a127 = CARRY(data_abc_a34_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a577 # !add1_ff_adffs_a1_a_a130) # !data_abc_a34_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a577 & !add1_ff_adffs_a1_a_a130)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a34_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a577,
	cin => add1_ff_adffs_a1_a_a130,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a2_a,
	cout => add1_ff_adffs_a2_a_a127);

add1_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a1_a = DFFE(data_abc_a33_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a581 $ add1_ff_adffs_a0_a_a181, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a1_a_a130 = CARRY(data_abc_a33_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a581 & !add1_ff_adffs_a0_a_a181 # !data_abc_a33_a_acombout & (!add1_ff_adffs_a0_a_a181 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a581))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a33_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a581,
	cin => add1_ff_adffs_a0_a_a181,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a1_a,
	cout => add1_ff_adffs_a1_a_a130);

add1_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a7_a = DFFE(data_abc_a39_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a585 $ add1_ff_adffs_a6_a_a145, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a7_a_a133 = CARRY(data_abc_a39_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a585 & !add1_ff_adffs_a6_a_a145 # !data_abc_a39_a_acombout & (!add1_ff_adffs_a6_a_a145 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a585))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a39_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a585,
	cin => add1_ff_adffs_a6_a_a145,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a7_a,
	cout => add1_ff_adffs_a7_a_a133);

add1_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a5_a = DFFE(data_abc_a37_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a589 $ add1_ff_adffs_a4_a_a139, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a5_a_a136 = CARRY(data_abc_a37_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a589 & !add1_ff_adffs_a4_a_a139 # !data_abc_a37_a_acombout & (!add1_ff_adffs_a4_a_a139 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a589))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a37_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a589,
	cin => add1_ff_adffs_a4_a_a139,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a5_a,
	cout => add1_ff_adffs_a5_a_a136);

add1_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a4_a = DFFE(data_abc_a36_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a593 $ !add1_ff_adffs_a3_a_a142, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a4_a_a139 = CARRY(data_abc_a36_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a593 # !add1_ff_adffs_a3_a_a142) # !data_abc_a36_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a593 & !add1_ff_adffs_a3_a_a142)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a36_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a593,
	cin => add1_ff_adffs_a3_a_a142,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a4_a,
	cout => add1_ff_adffs_a4_a_a139);

add1_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a3_a = DFFE(data_abc_a35_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a597 $ add1_ff_adffs_a2_a_a127, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a3_a_a142 = CARRY(data_abc_a35_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a597 & !add1_ff_adffs_a2_a_a127 # !data_abc_a35_a_acombout & (!add1_ff_adffs_a2_a_a127 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a597))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a35_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a597,
	cin => add1_ff_adffs_a2_a_a127,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a3_a,
	cout => add1_ff_adffs_a3_a_a142);

add1_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a6_a = DFFE(data_abc_a38_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a601 $ !add1_ff_adffs_a5_a_a136, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a6_a_a145 = CARRY(data_abc_a38_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a601 # !add1_ff_adffs_a5_a_a136) # !data_abc_a38_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a601 & !add1_ff_adffs_a5_a_a136)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a38_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a601,
	cin => add1_ff_adffs_a5_a_a136,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a6_a,
	cout => add1_ff_adffs_a6_a_a145);

add1_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a8_a = DFFE(data_abc_a40_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a605 $ !add1_ff_adffs_a7_a_a133, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a8_a_a148 = CARRY(data_abc_a40_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a605 # !add1_ff_adffs_a7_a_a133) # !data_abc_a40_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a605 & !add1_ff_adffs_a7_a_a133)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a40_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a605,
	cin => add1_ff_adffs_a7_a_a133,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a8_a,
	cout => add1_ff_adffs_a8_a_a148);

add1_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a9_a = DFFE(data_abc_a41_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a609 $ add1_ff_adffs_a8_a_a148, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a9_a_a151 = CARRY(data_abc_a41_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a609 & !add1_ff_adffs_a8_a_a148 # !data_abc_a41_a_acombout & (!add1_ff_adffs_a8_a_a148 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a609))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a41_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a609,
	cin => add1_ff_adffs_a8_a_a148,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a9_a,
	cout => add1_ff_adffs_a9_a_a151);

add1_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a10_a = DFFE(data_abc_a42_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a613 $ !add1_ff_adffs_a9_a_a151, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a10_a_a154 = CARRY(data_abc_a42_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a613 # !add1_ff_adffs_a9_a_a151) # !data_abc_a42_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a613 & !add1_ff_adffs_a9_a_a151)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a42_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a613,
	cin => add1_ff_adffs_a9_a_a151,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a10_a,
	cout => add1_ff_adffs_a10_a_a154);

add1_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a11_a = DFFE(data_abc_a43_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a617 $ add1_ff_adffs_a10_a_a154, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a11_a_a157 = CARRY(data_abc_a43_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a617 & !add1_ff_adffs_a10_a_a154 # !data_abc_a43_a_acombout & (!add1_ff_adffs_a10_a_a154 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a617))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a43_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a617,
	cin => add1_ff_adffs_a10_a_a154,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a11_a,
	cout => add1_ff_adffs_a11_a_a157);

add1_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a12_a = DFFE(data_abc_a44_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a621 $ !add1_ff_adffs_a11_a_a157, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a12_a_a160 = CARRY(data_abc_a44_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a621 # !add1_ff_adffs_a11_a_a157) # !data_abc_a44_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a621 & !add1_ff_adffs_a11_a_a157)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a44_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a621,
	cin => add1_ff_adffs_a11_a_a157,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a12_a,
	cout => add1_ff_adffs_a12_a_a160);

add1_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a13_a = DFFE(data_abc_a45_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a625 $ add1_ff_adffs_a12_a_a160, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a13_a_a163 = CARRY(data_abc_a45_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a625 & !add1_ff_adffs_a12_a_a160 # !data_abc_a45_a_acombout & (!add1_ff_adffs_a12_a_a160 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a625))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a45_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a625,
	cin => add1_ff_adffs_a12_a_a160,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a13_a,
	cout => add1_ff_adffs_a13_a_a163);

add1_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a14_a = DFFE(data_abc_a46_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a629 $ !add1_ff_adffs_a13_a_a163, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a14_a_a166 = CARRY(data_abc_a46_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a629 # !add1_ff_adffs_a13_a_a163) # !data_abc_a46_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a629 & !add1_ff_adffs_a13_a_a163)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a46_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a629,
	cin => add1_ff_adffs_a13_a_a163,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a14_a,
	cout => add1_ff_adffs_a14_a_a166);

add1_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a15_a = DFFE(data_abc_a47_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a633 $ add1_ff_adffs_a14_a_a166, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a15_a_a169 = CARRY(data_abc_a47_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a633 & !add1_ff_adffs_a14_a_a166 # !data_abc_a47_a_acombout & (!add1_ff_adffs_a14_a_a166 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a633))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a47_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a633,
	cin => add1_ff_adffs_a14_a_a166,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a15_a,
	cout => add1_ff_adffs_a15_a_a169);

add1_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a16_a = DFFE(data_abc_a47_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a637 $ !add1_ff_adffs_a15_a_a169, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a16_a_a172 = CARRY(data_abc_a47_a_acombout & (sub1_aadder_aresult_node_acs_buffer_a1_a_a637 # !add1_ff_adffs_a15_a_a169) # !data_abc_a47_a_acombout & sub1_aadder_aresult_node_acs_buffer_a1_a_a637 & !add1_ff_adffs_a15_a_a169)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a47_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a637,
	cin => add1_ff_adffs_a15_a_a169,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a16_a,
	cout => add1_ff_adffs_a16_a_a172);

add1_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a17_a = DFFE(data_abc_a47_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a641 $ add1_ff_adffs_a16_a_a172, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a17_a_a175 = CARRY(data_abc_a47_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a641 & !add1_ff_adffs_a16_a_a172 # !data_abc_a47_a_acombout & (!add1_ff_adffs_a16_a_a172 # !sub1_aadder_aresult_node_acs_buffer_a1_a_a641))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a47_a_acombout,
	datab => sub1_aadder_aresult_node_acs_buffer_a1_a_a641,
	cin => add1_ff_adffs_a16_a_a172,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a17_a,
	cout => add1_ff_adffs_a17_a_a175);

add1_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a29_a = DFFE(data_abc_a47_a_acombout $ (add1_ff_adffs_a17_a_a175 $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a641), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a47_a_acombout,
	datad => sub1_aadder_aresult_node_acs_buffer_a1_a_a641,
	cin => add1_ff_adffs_a17_a_a175,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a29_a);

level_cmp_Del_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- level_cmp_Del_vec_a1_a = DFFE(level_cmp_Del_vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => level_cmp_Del_vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => level_cmp_Del_vec_a1_a);

Enable_Level_Str_a18_I : apex20ke_lcell
-- Equation(s):
-- Enable_Level_Str_a18 = !TimeConst_FF_adffs_a1_a & !TimeConst_FF_adffs_a2_a & !TimeConst_FF_adffs_a3_a
-- Enable_Level_Str_a20 = !TimeConst_FF_adffs_a1_a & !TimeConst_FF_adffs_a2_a & !TimeConst_FF_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0101",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Enable_Level_Str_a18,
	cascout => Enable_Level_Str_a20);

sub1_aadder_aresult_node_acs_buffer_a1_a_a577_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a577 = data_abc_a2_a_acombout $ data_abc_a17_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a583
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a579 = CARRY(data_abc_a2_a_acombout & data_abc_a17_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a583 # !data_abc_a2_a_acombout & (data_abc_a17_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a583))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a2_a_acombout,
	datab => data_abc_a17_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a583,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a577,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a579);

data_abc_a34_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(34),
	combout => data_abc_a34_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a581_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a581 = data_abc_a1_a_acombout $ data_abc_a16_a_acombout
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a583 = CARRY(data_abc_a1_a_acombout # !data_abc_a16_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a1_a_acombout,
	datab => data_abc_a16_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a581,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a583);

data_abc_a33_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(33),
	combout => data_abc_a33_a_acombout);

add1_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- add1_ff_adffs_a0_a = DFFE(data_abc_a0_a_acombout $ data_abc_a32_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add1_ff_adffs_a0_a_a181 = CARRY(data_abc_a0_a_acombout & data_abc_a32_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a0_a_acombout,
	datab => data_abc_a32_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add1_ff_adffs_a0_a,
	cout => add1_ff_adffs_a0_a_a181);

sub1_aadder_aresult_node_acs_buffer_a1_a_a585_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a585 = data_abc_a7_a_acombout $ data_abc_a22_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a603
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a587 = CARRY(data_abc_a7_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a603 # !data_abc_a22_a_acombout) # !data_abc_a7_a_acombout & !data_abc_a22_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a603)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a7_a_acombout,
	datab => data_abc_a22_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a603,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a585,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a587);

data_abc_a39_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(39),
	combout => data_abc_a39_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a589_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a589 = data_abc_a20_a_acombout $ data_abc_a5_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a595
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a591 = CARRY(data_abc_a20_a_acombout & data_abc_a5_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a595 # !data_abc_a20_a_acombout & (data_abc_a5_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a595))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a20_a_acombout,
	datab => data_abc_a5_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a595,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a589,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a591);

data_abc_a37_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(37),
	combout => data_abc_a37_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a593_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a593 = data_abc_a4_a_acombout $ data_abc_a19_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a599
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a595 = CARRY(data_abc_a4_a_acombout & data_abc_a19_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a599 # !data_abc_a4_a_acombout & (data_abc_a19_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a599))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a4_a_acombout,
	datab => data_abc_a19_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a599,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a593,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a595);

data_abc_a36_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(36),
	combout => data_abc_a36_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a597_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a597 = data_abc_a18_a_acombout $ data_abc_a3_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a579
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a599 = CARRY(data_abc_a18_a_acombout & data_abc_a3_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a579 # !data_abc_a18_a_acombout & (data_abc_a3_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a579))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a18_a_acombout,
	datab => data_abc_a3_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a579,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a597,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a599);

data_abc_a35_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(35),
	combout => data_abc_a35_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a601_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a601 = data_abc_a21_a_acombout $ data_abc_a6_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a591
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a603 = CARRY(data_abc_a21_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a591 # !data_abc_a6_a_acombout) # !data_abc_a21_a_acombout & !data_abc_a6_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a591)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a21_a_acombout,
	datab => data_abc_a6_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a591,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a601,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a603);

data_abc_a38_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(38),
	combout => data_abc_a38_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a605_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a605 = data_abc_a8_a_acombout $ data_abc_a23_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a587
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a607 = CARRY(data_abc_a8_a_acombout & data_abc_a23_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a587 # !data_abc_a8_a_acombout & (data_abc_a23_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a587))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a8_a_acombout,
	datab => data_abc_a23_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a605,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a607);

data_abc_a40_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(40),
	combout => data_abc_a40_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a609_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a609 = data_abc_a24_a_acombout $ data_abc_a9_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a607
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a611 = CARRY(data_abc_a24_a_acombout & data_abc_a9_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a607 # !data_abc_a24_a_acombout & (data_abc_a9_a_acombout # !sub1_aadder_aresult_node_acs_buffer_a1_a_a607))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a24_a_acombout,
	datab => data_abc_a9_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a607,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a609,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a611);

data_abc_a41_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(41),
	combout => data_abc_a41_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a613_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a613 = data_abc_a25_a_acombout $ data_abc_a10_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a611
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a615 = CARRY(data_abc_a25_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a611 # !data_abc_a10_a_acombout) # !data_abc_a25_a_acombout & !data_abc_a10_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a611)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a25_a_acombout,
	datab => data_abc_a10_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a613,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a615);

data_abc_a42_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(42),
	combout => data_abc_a42_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a617_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a617 = data_abc_a11_a_acombout $ data_abc_a26_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a615
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a619 = CARRY(data_abc_a11_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a615 # !data_abc_a26_a_acombout) # !data_abc_a11_a_acombout & !data_abc_a26_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a615)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a11_a_acombout,
	datab => data_abc_a26_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a617,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a619);

data_abc_a43_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(43),
	combout => data_abc_a43_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a621_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a621 = data_abc_a27_a_acombout $ data_abc_a12_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a619
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a623 = CARRY(data_abc_a27_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a619 # !data_abc_a12_a_acombout) # !data_abc_a27_a_acombout & !data_abc_a12_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a619)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a27_a_acombout,
	datab => data_abc_a12_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a619,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a621,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a623);

data_abc_a44_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(44),
	combout => data_abc_a44_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a625_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a625 = data_abc_a28_a_acombout $ data_abc_a13_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a623
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a627 = CARRY(data_abc_a28_a_acombout & data_abc_a13_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a623 # !data_abc_a28_a_acombout & (data_abc_a13_a_acombout # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a623))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a28_a_acombout,
	datab => data_abc_a13_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a623,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a625,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a627);

data_abc_a45_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(45),
	combout => data_abc_a45_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a629_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a629 = data_abc_a14_a_acombout $ data_abc_a29_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a627
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a631 = CARRY(data_abc_a14_a_acombout & data_abc_a29_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a627 # !data_abc_a14_a_acombout & (data_abc_a29_a_acombout # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a627))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a14_a_acombout,
	datab => data_abc_a29_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a629,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a631);

data_abc_a46_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(46),
	combout => data_abc_a46_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a633_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a633 = data_abc_a15_a_acombout $ data_abc_a30_a_acombout $ sub1_aadder_aresult_node_acs_buffer_a1_a_a631
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a635 = CARRY(data_abc_a15_a_acombout & (!sub1_aadder_aresult_node_acs_buffer_a1_a_a631 # !data_abc_a30_a_acombout) # !data_abc_a15_a_acombout & !data_abc_a30_a_acombout & 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a631)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a15_a_acombout,
	datab => data_abc_a30_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a631,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a633,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a635);

data_abc_a47_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(47),
	combout => data_abc_a47_a_acombout);

sub1_aadder_aresult_node_acs_buffer_a1_a_a637_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a637 = data_abc_a15_a_acombout $ data_abc_a31_a_acombout $ !sub1_aadder_aresult_node_acs_buffer_a1_a_a635
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a639 = CARRY(data_abc_a15_a_acombout & data_abc_a31_a_acombout & !sub1_aadder_aresult_node_acs_buffer_a1_a_a635 # !data_abc_a15_a_acombout & (data_abc_a31_a_acombout # 
-- !sub1_aadder_aresult_node_acs_buffer_a1_a_a635))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a15_a_acombout,
	datab => data_abc_a31_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a635,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a637,
	cout => sub1_aadder_aresult_node_acs_buffer_a1_a_a639);

sub1_aadder_aresult_node_acs_buffer_a1_a_a641_I : apex20ke_lcell
-- Equation(s):
-- sub1_aadder_aresult_node_acs_buffer_a1_a_a641 = data_abc_a15_a_acombout $ (sub1_aadder_aresult_node_acs_buffer_a1_a_a639 $ data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a15_a_acombout,
	datad => data_abc_a31_a_acombout,
	cin => sub1_aadder_aresult_node_acs_buffer_a1_a_a639,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sub1_aadder_aresult_node_acs_buffer_a1_a_a641);

data_abc_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(17),
	combout => data_abc_a17_a_acombout);

data_abc_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(2),
	combout => data_abc_a2_a_acombout);

data_abc_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(16),
	combout => data_abc_a16_a_acombout);

data_abc_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(1),
	combout => data_abc_a1_a_acombout);

data_abc_a32_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(32),
	combout => data_abc_a32_a_acombout);

data_abc_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(0),
	combout => data_abc_a0_a_acombout);

data_abc_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(22),
	combout => data_abc_a22_a_acombout);

data_abc_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(7),
	combout => data_abc_a7_a_acombout);

data_abc_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(20),
	combout => data_abc_a20_a_acombout);

data_abc_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(5),
	combout => data_abc_a5_a_acombout);

data_abc_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(19),
	combout => data_abc_a19_a_acombout);

data_abc_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(4),
	combout => data_abc_a4_a_acombout);

data_abc_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(18),
	combout => data_abc_a18_a_acombout);

data_abc_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(3),
	combout => data_abc_a3_a_acombout);

data_abc_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(21),
	combout => data_abc_a21_a_acombout);

data_abc_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(6),
	combout => data_abc_a6_a_acombout);

data_abc_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(23),
	combout => data_abc_a23_a_acombout);

data_abc_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(8),
	combout => data_abc_a8_a_acombout);

data_abc_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(24),
	combout => data_abc_a24_a_acombout);

data_abc_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(9),
	combout => data_abc_a9_a_acombout);

data_abc_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(25),
	combout => data_abc_a25_a_acombout);

data_abc_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(10),
	combout => data_abc_a10_a_acombout);

data_abc_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(26),
	combout => data_abc_a26_a_acombout);

data_abc_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(11),
	combout => data_abc_a11_a_acombout);

data_abc_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(27),
	combout => data_abc_a27_a_acombout);

data_abc_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(12),
	combout => data_abc_a12_a_acombout);

data_abc_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(28),
	combout => data_abc_a28_a_acombout);

data_abc_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(13),
	combout => data_abc_a13_a_acombout);

data_abc_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(29),
	combout => data_abc_a29_a_acombout);

data_abc_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(14),
	combout => data_abc_a14_a_acombout);

data_abc_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(30),
	combout => data_abc_a30_a_acombout);

data_abc_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(15),
	combout => data_abc_a15_a_acombout);

data_abc_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(31),
	combout => data_abc_a31_a_acombout);

tlevel_Mux_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a23_a = DFFE(TimeConst_FF_adffs_a3_a & (tlevel_reg_ff_adffs_a14_a) # !TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a23_a);

tlevel_Mux_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a21_a = DFFE(tlevel_Mux_a21_a_a1001 # tlevel_Mux_a21_a_a1002 # tlevel_Mux_a17_a_a1003 & fir_out_a0_a_a1952, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a21_a_a1001,
	datab => tlevel_Mux_a17_a_a1003,
	datac => fir_out_a0_a_a1952,
	datad => tlevel_Mux_a21_a_a1002,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a21_a);

tlevel_Mux_a22_a_a998_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a22_a_a998 = TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a13_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a22_a_a998);

tlevel_Mux_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a20_a = DFFE(tlevel_Mux_a20_a_a1007 # tlevel_Mux_a20_a_a1008 # fir_out_a0_a_a1952 & Mux_a2185, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => Mux_a2185,
	datac => tlevel_Mux_a20_a_a1007,
	datad => tlevel_Mux_a20_a_a1008,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a20_a);

tlevel_Mux_a21_a_a1001_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a21_a_a1001 = !TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a15_a & (!TimeConst_FF_adffs_a1_a # !TimeConst_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0444",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a2_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a21_a_a1001);

tlevel_Mux_a21_a_a1002_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a21_a_a1002 = TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a12_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a21_a_a1002);

tlevel_Mux_a20_a_a1008_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a20_a_a1008 = TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a11_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a20_a_a1008);

tlevel_Mux_a17_a_a1017_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a17_a_a1017 = TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a17_a_a1017);

tlevel_Mux_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a15_a = DFFE(tlevel_Mux_a15_a_a1024 # TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => tlevel_reg_ff_adffs_a6_a,
	datad => tlevel_Mux_a15_a_a1024,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a15_a);

tlevel_Mux_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a14_a = DFFE(tlevel_Mux_a14_a_a1026 # TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_FF_adffs_a3_a,
	datac => tlevel_reg_ff_adffs_a5_a,
	datad => tlevel_Mux_a14_a_a1026,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a14_a);

Mux_a2195_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2195 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a9_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F2C2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a10_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2195);

Mux_a2196_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2196 = Mux_a2195 & (tlevel_reg_ff_adffs_a7_a # !TimeConst_FF_adffs_a1_a) # !Mux_a2195 & (tlevel_reg_ff_adffs_a8_a & TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a7_a,
	datab => tlevel_reg_ff_adffs_a8_a,
	datac => Mux_a2195,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2196);

tlevel_Mux_a15_a_a1024_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a15_a_a1024 = Enable_Level_Str_a16 & (Mux_a2188 # fir_out_a0_a_a1952 & Mux_a2196) # !Enable_Level_Str_a16 & fir_out_a0_a_a1952 & (Mux_a2196)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Enable_Level_Str_a16,
	datab => fir_out_a0_a_a1952,
	datac => Mux_a2188,
	datad => Mux_a2196,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a15_a_a1024);

tlevel_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a6_a = DFFE(tlevel_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a6_a);

tlevel_Mux_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a13_a = DFFE(tlevel_Mux_a13_a_a1028 # tlevel_reg_ff_adffs_a4_a & TimeConst_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a4_a,
	datac => tlevel_Mux_a13_a_a1028,
	datad => TimeConst_FF_adffs_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a13_a);

Mux_a2197_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2197 = TimeConst_FF_adffs_a1_a & (tlevel_reg_ff_adffs_a7_a # TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (!TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CBC8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a7_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2197);

Mux_a2198_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2198 = TimeConst_FF_adffs_a0_a & (Mux_a2197 & (tlevel_reg_ff_adffs_a6_a) # !Mux_a2197 & tlevel_reg_ff_adffs_a8_a) # !TimeConst_FF_adffs_a0_a & (Mux_a2197)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F588",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a8_a,
	datac => tlevel_reg_ff_adffs_a6_a,
	datad => Mux_a2197,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2198);

tlevel_Mux_a14_a_a1026_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a14_a_a1026 = Mux_a2190 & (Enable_Level_Str_a16 # fir_out_a0_a_a1952 & Mux_a2198) # !Mux_a2190 & (fir_out_a0_a_a1952 & Mux_a2198)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2190,
	datab => Enable_Level_Str_a16,
	datac => fir_out_a0_a_a1952,
	datad => Mux_a2198,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a14_a_a1026);

tlevel_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(6),
	combout => tlevel_a6_a_acombout);

tlevel_Mux_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a12_a = DFFE(tlevel_Mux_a12_a_a1030 # TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => tlevel_reg_ff_adffs_a3_a,
	datad => tlevel_Mux_a12_a_a1030,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a12_a);

Mux_a2199_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2199 = TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a # tlevel_reg_ff_adffs_a7_a) # !TimeConst_FF_adffs_a0_a & !TimeConst_FF_adffs_a1_a & (tlevel_reg_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B9A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a7_a,
	datad => tlevel_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2199);

Mux_a2200_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2200 = TimeConst_FF_adffs_a1_a & (Mux_a2199 & (tlevel_reg_ff_adffs_a5_a) # !Mux_a2199 & tlevel_reg_ff_adffs_a6_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2199)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F388",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a6_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a5_a,
	datad => Mux_a2199,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2200);

tlevel_Mux_a13_a_a1028_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a13_a_a1028 = Mux_a2200 & (fir_out_a0_a_a1952 # Enable_Level_Str_a16 & Mux_a2192) # !Mux_a2200 & Enable_Level_Str_a16 & (Mux_a2192)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2200,
	datab => Enable_Level_Str_a16,
	datac => fir_out_a0_a_a1952,
	datad => Mux_a2192,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a13_a_a1028);

tlevel_Mux_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a11_a = DFFE(tlevel_Mux_a11_a_a1032 # tlevel_reg_ff_adffs_a2_a & TimeConst_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_Mux_a11_a_a1032,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a11_a);

tlevel_Mux_a12_a_a1030_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a12_a_a1030 = Enable_Level_Str_a16 & (Mux_a2194 # fir_out_a0_a_a1952 & Mux_a2202) # !Enable_Level_Str_a16 & fir_out_a0_a_a1952 & (Mux_a2202)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Enable_Level_Str_a16,
	datab => fir_out_a0_a_a1952,
	datac => Mux_a2194,
	datad => Mux_a2202,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a12_a_a1030);

tlevel_Mux_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a10_a = DFFE(tlevel_Mux_a10_a_a1034 # TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => tlevel_reg_ff_adffs_a1_a,
	datad => tlevel_Mux_a10_a_a1034,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a10_a);

Mux_a2203_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2203 = TimeConst_FF_adffs_a1_a & TimeConst_FF_adffs_a0_a # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a5_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DC98",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a6_a,
	datad => tlevel_reg_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2203);

Mux_a2204_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2204 = TimeConst_FF_adffs_a1_a & (Mux_a2203 & tlevel_reg_ff_adffs_a3_a # !Mux_a2203 & (tlevel_reg_ff_adffs_a4_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2203)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a3_a,
	datac => tlevel_reg_ff_adffs_a4_a,
	datad => Mux_a2203,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2204);

tlevel_Mux_a11_a_a1032_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a11_a_a1032 = Mux_a2196 & (Enable_Level_Str_a16 # fir_out_a0_a_a1952 & Mux_a2204) # !Mux_a2196 & fir_out_a0_a_a1952 & (Mux_a2204)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2196,
	datab => fir_out_a0_a_a1952,
	datac => Enable_Level_Str_a16,
	datad => Mux_a2204,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a11_a_a1032);

tlevel_Mux_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a9_a = DFFE(tlevel_Mux_a9_a_a1036 # TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => tlevel_Mux_a9_a_a1036,
	datad => tlevel_reg_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a9_a);

tlevel_Mux_a10_a_a1034_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a10_a_a1034 = Enable_Level_Str_a16 & (Mux_a2198 # fir_out_a0_a_a1952 & Mux_a2206) # !Enable_Level_Str_a16 & fir_out_a0_a_a1952 & (Mux_a2206)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Enable_Level_Str_a16,
	datab => fir_out_a0_a_a1952,
	datac => Mux_a2198,
	datad => Mux_a2206,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a10_a_a1034);

Mux_a2207_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2207 = TimeConst_FF_adffs_a1_a & TimeConst_FF_adffs_a0_a # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a3_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DC98",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a4_a,
	datad => tlevel_reg_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2207);

Mux_a2208_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2208 = TimeConst_FF_adffs_a1_a & (Mux_a2207 & tlevel_reg_ff_adffs_a1_a # !Mux_a2207 & (tlevel_reg_ff_adffs_a2_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2207)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2207,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2208);

tlevel_Mux_a9_a_a1036_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a9_a_a1036 = fir_out_a0_a_a1952 & (Mux_a2208 # Enable_Level_Str_a16 & Mux_a2200) # !fir_out_a0_a_a1952 & Enable_Level_Str_a16 & (Mux_a2200)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => Enable_Level_Str_a16,
	datac => Mux_a2208,
	datad => Mux_a2200,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a9_a_a1036);

tlevel_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a0_a = DFFE(tlevel_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a0_a);

tlevel_Mux_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a7_a = DFFE(fir_out_a0_a_a1952 & (Mux_a2212 # Enable_Level_Str_a16 & Mux_a2204) # !fir_out_a0_a_a1952 & Enable_Level_Str_a16 & Mux_a2204, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => Enable_Level_Str_a16,
	datac => Mux_a2204,
	datad => Mux_a2212,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a7_a);

tlevel_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(0),
	combout => tlevel_a0_a_acombout);

Mux_a2211_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2211 = !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0E04",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2211);

Mux_a2212_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2212 = Mux_a2211 # tlevel_reg_ff_adffs_a0_a & TimeConst_FF_adffs_a1_a & !TimeConst_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF08",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a0_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => Mux_a2211,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2212);

tlevel_Mux_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a5_a = DFFE(Mux_a2208 & (Enable_Level_Str_a16 # fir_out_a0_a_a1952 & Mux_a2215) # !Mux_a2208 & fir_out_a0_a_a1952 & Mux_a2215, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2208,
	datab => fir_out_a0_a_a1952,
	datac => Mux_a2215,
	datad => Enable_Level_Str_a16,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a5_a);

Mux_a2215_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2215 = !TimeConst_FF_adffs_a0_a & (!TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0500",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2215);

tlevel_Mux_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a3_a = DFFE(!TimeConst_FF_adffs_a2_a & !TimeConst_FF_adffs_a3_a & (Mux_a2221 # Mux_a2211), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0504",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datab => Mux_a2221,
	datac => TimeConst_FF_adffs_a3_a,
	datad => Mux_a2211,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a3_a);

tlevel_Mux_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a2_a = DFFE(Enable_Level_Str_a18 & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a0_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a1_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E040",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a1_a,
	datac => Enable_Level_Str_a18,
	datad => tlevel_reg_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a2_a);

Mux_a2221_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2221 = !TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2221);

tlevel_Mux_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a1_a = DFFE(!TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a0_a & Enable_Level_Str_a16 & !TimeConst_FF_adffs_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0040",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a0_a,
	datac => Enable_Level_Str_a16,
	datad => TimeConst_FF_adffs_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a1_a);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

AD_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_MR,
	combout => AD_MR_acombout);

add2b_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a0_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a0_a $ tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a0_a_a408 = CARRY(add1_ff_adffs_a0_a & tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a0_a,
	datab => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a0_a,
	cout => add2b_ff_adffs_a0_a_a408);

tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101 = add2b_ff_adffs_a0_a
-- tlevel_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(add2b_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a);

add2b_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a1_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a1_a $ add2b_ff_adffs_a1_a $ add2b_ff_adffs_a0_a_a408, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a1_a_a342 = CARRY(add1_ff_adffs_a1_a & !add2b_ff_adffs_a1_a & !add2b_ff_adffs_a0_a_a408 # !add1_ff_adffs_a1_a & (!add2b_ff_adffs_a0_a_a408 # !add2b_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a1_a,
	datab => add2b_ff_adffs_a1_a,
	cin => add2b_ff_adffs_a0_a_a408,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a1_a,
	cout => add2b_ff_adffs_a1_a_a342);

add2b_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a2_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a2_a $ add2b_ff_adffs_a2_a $ !add2b_ff_adffs_a1_a_a342, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a2_a_a339 = CARRY(add1_ff_adffs_a2_a & (add2b_ff_adffs_a2_a # !add2b_ff_adffs_a1_a_a342) # !add1_ff_adffs_a2_a & add2b_ff_adffs_a2_a & !add2b_ff_adffs_a1_a_a342)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a2_a,
	datab => add2b_ff_adffs_a2_a,
	cin => add2b_ff_adffs_a1_a_a342,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a2_a,
	cout => add2b_ff_adffs_a2_a_a339);

add2b_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a3_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a3_a $ add2b_ff_adffs_a3_a $ add2b_ff_adffs_a2_a_a339, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a3_a_a354 = CARRY(add1_ff_adffs_a3_a & !add2b_ff_adffs_a3_a & !add2b_ff_adffs_a2_a_a339 # !add1_ff_adffs_a3_a & (!add2b_ff_adffs_a2_a_a339 # !add2b_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a3_a,
	datab => add2b_ff_adffs_a3_a,
	cin => add2b_ff_adffs_a2_a_a339,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a3_a,
	cout => add2b_ff_adffs_a3_a_a354);

add2b_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a4_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a4_a $ add2b_ff_adffs_a4_a $ !add2b_ff_adffs_a3_a_a354, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a4_a_a351 = CARRY(add1_ff_adffs_a4_a & (add2b_ff_adffs_a4_a # !add2b_ff_adffs_a3_a_a354) # !add1_ff_adffs_a4_a & add2b_ff_adffs_a4_a & !add2b_ff_adffs_a3_a_a354)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a4_a,
	datab => add2b_ff_adffs_a4_a,
	cin => add2b_ff_adffs_a3_a_a354,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a4_a,
	cout => add2b_ff_adffs_a4_a_a351);

add2b_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a5_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a5_a $ add2b_ff_adffs_a5_a $ add2b_ff_adffs_a4_a_a351, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a5_a_a348 = CARRY(add1_ff_adffs_a5_a & !add2b_ff_adffs_a5_a & !add2b_ff_adffs_a4_a_a351 # !add1_ff_adffs_a5_a & (!add2b_ff_adffs_a4_a_a351 # !add2b_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a5_a,
	datab => add2b_ff_adffs_a5_a,
	cin => add2b_ff_adffs_a4_a_a351,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a5_a,
	cout => add2b_ff_adffs_a5_a_a348);

add2b_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a6_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a6_a $ add2b_ff_adffs_a6_a $ !add2b_ff_adffs_a5_a_a348, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a6_a_a357 = CARRY(add1_ff_adffs_a6_a & (add2b_ff_adffs_a6_a # !add2b_ff_adffs_a5_a_a348) # !add1_ff_adffs_a6_a & add2b_ff_adffs_a6_a & !add2b_ff_adffs_a5_a_a348)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a6_a,
	datab => add2b_ff_adffs_a6_a,
	cin => add2b_ff_adffs_a5_a_a348,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a6_a,
	cout => add2b_ff_adffs_a6_a_a357);

add2b_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a7_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a7_a $ add2b_ff_adffs_a7_a $ add2b_ff_adffs_a6_a_a357, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a7_a_a345 = CARRY(add1_ff_adffs_a7_a & !add2b_ff_adffs_a7_a & !add2b_ff_adffs_a6_a_a357 # !add1_ff_adffs_a7_a & (!add2b_ff_adffs_a6_a_a357 # !add2b_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a7_a,
	datab => add2b_ff_adffs_a7_a,
	cin => add2b_ff_adffs_a6_a_a357,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a7_a,
	cout => add2b_ff_adffs_a7_a_a345);

add2b_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a8_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a8_a $ add2b_ff_adffs_a8_a $ !add2b_ff_adffs_a7_a_a345, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a8_a_a360 = CARRY(add1_ff_adffs_a8_a & (add2b_ff_adffs_a8_a # !add2b_ff_adffs_a7_a_a345) # !add1_ff_adffs_a8_a & add2b_ff_adffs_a8_a & !add2b_ff_adffs_a7_a_a345)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a8_a,
	datab => add2b_ff_adffs_a8_a,
	cin => add2b_ff_adffs_a7_a_a345,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a8_a,
	cout => add2b_ff_adffs_a8_a_a360);

add2b_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a9_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a9_a $ add2b_ff_adffs_a9_a $ add2b_ff_adffs_a8_a_a360, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a9_a_a363 = CARRY(add1_ff_adffs_a9_a & !add2b_ff_adffs_a9_a & !add2b_ff_adffs_a8_a_a360 # !add1_ff_adffs_a9_a & (!add2b_ff_adffs_a8_a_a360 # !add2b_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a9_a,
	datab => add2b_ff_adffs_a9_a,
	cin => add2b_ff_adffs_a8_a_a360,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a9_a,
	cout => add2b_ff_adffs_a9_a_a363);

add2b_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a10_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a10_a $ add2b_ff_adffs_a10_a $ !add2b_ff_adffs_a9_a_a363, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a10_a_a366 = CARRY(add1_ff_adffs_a10_a & (add2b_ff_adffs_a10_a # !add2b_ff_adffs_a9_a_a363) # !add1_ff_adffs_a10_a & add2b_ff_adffs_a10_a & !add2b_ff_adffs_a9_a_a363)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a10_a,
	datab => add2b_ff_adffs_a10_a,
	cin => add2b_ff_adffs_a9_a_a363,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a10_a,
	cout => add2b_ff_adffs_a10_a_a366);

add2b_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a11_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a11_a $ add2b_ff_adffs_a11_a $ add2b_ff_adffs_a10_a_a366, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a11_a_a369 = CARRY(add1_ff_adffs_a11_a & !add2b_ff_adffs_a11_a & !add2b_ff_adffs_a10_a_a366 # !add1_ff_adffs_a11_a & (!add2b_ff_adffs_a10_a_a366 # !add2b_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a11_a,
	datab => add2b_ff_adffs_a11_a,
	cin => add2b_ff_adffs_a10_a_a366,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a11_a,
	cout => add2b_ff_adffs_a11_a_a369);

add2b_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a12_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a12_a $ add2b_ff_adffs_a12_a $ !add2b_ff_adffs_a11_a_a369, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a12_a_a372 = CARRY(add1_ff_adffs_a12_a & (add2b_ff_adffs_a12_a # !add2b_ff_adffs_a11_a_a369) # !add1_ff_adffs_a12_a & add2b_ff_adffs_a12_a & !add2b_ff_adffs_a11_a_a369)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a12_a,
	datab => add2b_ff_adffs_a12_a,
	cin => add2b_ff_adffs_a11_a_a369,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a12_a,
	cout => add2b_ff_adffs_a12_a_a372);

add2b_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a13_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a13_a $ add2b_ff_adffs_a13_a $ add2b_ff_adffs_a12_a_a372, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a13_a_a375 = CARRY(add1_ff_adffs_a13_a & !add2b_ff_adffs_a13_a & !add2b_ff_adffs_a12_a_a372 # !add1_ff_adffs_a13_a & (!add2b_ff_adffs_a12_a_a372 # !add2b_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a13_a,
	datab => add2b_ff_adffs_a13_a,
	cin => add2b_ff_adffs_a12_a_a372,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a13_a,
	cout => add2b_ff_adffs_a13_a_a375);

add2b_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a14_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a14_a $ add2b_ff_adffs_a14_a $ !add2b_ff_adffs_a13_a_a375, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a14_a_a378 = CARRY(add1_ff_adffs_a14_a & (add2b_ff_adffs_a14_a # !add2b_ff_adffs_a13_a_a375) # !add1_ff_adffs_a14_a & add2b_ff_adffs_a14_a & !add2b_ff_adffs_a13_a_a375)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a14_a,
	datab => add2b_ff_adffs_a14_a,
	cin => add2b_ff_adffs_a13_a_a375,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a14_a,
	cout => add2b_ff_adffs_a14_a_a378);

add2b_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a15_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a15_a $ add2b_ff_adffs_a15_a $ add2b_ff_adffs_a14_a_a378, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a15_a_a381 = CARRY(add1_ff_adffs_a15_a & !add2b_ff_adffs_a15_a & !add2b_ff_adffs_a14_a_a378 # !add1_ff_adffs_a15_a & (!add2b_ff_adffs_a14_a_a378 # !add2b_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a15_a,
	cin => add2b_ff_adffs_a14_a_a378,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a15_a,
	cout => add2b_ff_adffs_a15_a_a381);

add2b_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a16_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a16_a $ add2b_ff_adffs_a16_a $ !add2b_ff_adffs_a15_a_a381, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a16_a_a384 = CARRY(add1_ff_adffs_a16_a & (add2b_ff_adffs_a16_a # !add2b_ff_adffs_a15_a_a381) # !add1_ff_adffs_a16_a & add2b_ff_adffs_a16_a & !add2b_ff_adffs_a15_a_a381)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a16_a,
	datab => add2b_ff_adffs_a16_a,
	cin => add2b_ff_adffs_a15_a_a381,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a16_a,
	cout => add2b_ff_adffs_a16_a_a384);

add2b_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a17_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a17_a $ add2b_ff_adffs_a17_a $ add2b_ff_adffs_a16_a_a384, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a17_a_a387 = CARRY(add1_ff_adffs_a17_a & !add2b_ff_adffs_a17_a & !add2b_ff_adffs_a16_a_a384 # !add1_ff_adffs_a17_a & (!add2b_ff_adffs_a16_a_a384 # !add2b_ff_adffs_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a17_a,
	cin => add2b_ff_adffs_a16_a_a384,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a17_a,
	cout => add2b_ff_adffs_a17_a_a387);

add2b_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a18_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a18_a $ !add2b_ff_adffs_a17_a_a387, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a18_a_a390 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a18_a # !add2b_ff_adffs_a17_a_a387) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a18_a & !add2b_ff_adffs_a17_a_a387)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a18_a,
	cin => add2b_ff_adffs_a17_a_a387,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a18_a,
	cout => add2b_ff_adffs_a18_a_a390);

add2b_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a19_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a19_a $ add2b_ff_adffs_a18_a_a390, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a19_a_a393 = CARRY(add1_ff_adffs_a29_a & !add2b_ff_adffs_a19_a & !add2b_ff_adffs_a18_a_a390 # !add1_ff_adffs_a29_a & (!add2b_ff_adffs_a18_a_a390 # !add2b_ff_adffs_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a19_a,
	cin => add2b_ff_adffs_a18_a_a390,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a19_a,
	cout => add2b_ff_adffs_a19_a_a393);

add2b_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a20_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a20_a $ !add2b_ff_adffs_a19_a_a393, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a20_a_a396 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a20_a # !add2b_ff_adffs_a19_a_a393) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a20_a & !add2b_ff_adffs_a19_a_a393)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a20_a,
	cin => add2b_ff_adffs_a19_a_a393,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a20_a,
	cout => add2b_ff_adffs_a20_a_a396);

add2b_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a21_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a21_a $ add2b_ff_adffs_a20_a_a396, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a21_a_a399 = CARRY(add1_ff_adffs_a29_a & !add2b_ff_adffs_a21_a & !add2b_ff_adffs_a20_a_a396 # !add1_ff_adffs_a29_a & (!add2b_ff_adffs_a20_a_a396 # !add2b_ff_adffs_a21_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a21_a,
	cin => add2b_ff_adffs_a20_a_a396,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a21_a,
	cout => add2b_ff_adffs_a21_a_a399);

add2b_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a22_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a22_a $ !add2b_ff_adffs_a21_a_a399, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a22_a_a402 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a22_a # !add2b_ff_adffs_a21_a_a399) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a22_a & !add2b_ff_adffs_a21_a_a399)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a22_a,
	cin => add2b_ff_adffs_a21_a_a399,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a22_a,
	cout => add2b_ff_adffs_a22_a_a402);

add2b_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a23_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a23_a $ add2b_ff_adffs_a22_a_a402, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a23_a_a426 = CARRY(add1_ff_adffs_a29_a & !add2b_ff_adffs_a23_a & !add2b_ff_adffs_a22_a_a402 # !add1_ff_adffs_a29_a & (!add2b_ff_adffs_a22_a_a402 # !add2b_ff_adffs_a23_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a23_a,
	cin => add2b_ff_adffs_a22_a_a402,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a23_a,
	cout => add2b_ff_adffs_a23_a_a426);

add2b_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a24_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a24_a $ !add2b_ff_adffs_a23_a_a426, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a24_a_a423 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a24_a # !add2b_ff_adffs_a23_a_a426) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a24_a & !add2b_ff_adffs_a23_a_a426)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a24_a,
	cin => add2b_ff_adffs_a23_a_a426,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a24_a,
	cout => add2b_ff_adffs_a24_a_a423);

add2b_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a25_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a25_a $ add2b_ff_adffs_a24_a_a423, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a25_a_a420 = CARRY(add1_ff_adffs_a29_a & !add2b_ff_adffs_a25_a & !add2b_ff_adffs_a24_a_a423 # !add1_ff_adffs_a29_a & (!add2b_ff_adffs_a24_a_a423 # !add2b_ff_adffs_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a25_a,
	cin => add2b_ff_adffs_a24_a_a423,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a25_a,
	cout => add2b_ff_adffs_a25_a_a420);

add2b_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a26_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a26_a $ !add2b_ff_adffs_a25_a_a420, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a26_a_a417 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a26_a # !add2b_ff_adffs_a25_a_a420) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a26_a & !add2b_ff_adffs_a25_a_a420)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a26_a,
	cin => add2b_ff_adffs_a25_a_a420,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a26_a,
	cout => add2b_ff_adffs_a26_a_a417);

add2b_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a27_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a27_a $ add2b_ff_adffs_a26_a_a417, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a27_a_a414 = CARRY(add1_ff_adffs_a29_a & !add2b_ff_adffs_a27_a & !add2b_ff_adffs_a26_a_a417 # !add1_ff_adffs_a29_a & (!add2b_ff_adffs_a26_a_a417 # !add2b_ff_adffs_a27_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a27_a,
	cin => add2b_ff_adffs_a26_a_a417,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a27_a,
	cout => add2b_ff_adffs_a27_a_a414);

add2b_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a28_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ add2b_ff_adffs_a28_a $ !add2b_ff_adffs_a27_a_a414, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- add2b_ff_adffs_a28_a_a411 = CARRY(add1_ff_adffs_a29_a & (add2b_ff_adffs_a28_a # !add2b_ff_adffs_a27_a_a414) # !add1_ff_adffs_a29_a & add2b_ff_adffs_a28_a & !add2b_ff_adffs_a27_a_a414)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datab => add2b_ff_adffs_a28_a,
	cin => add2b_ff_adffs_a27_a_a414,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a28_a,
	cout => add2b_ff_adffs_a28_a_a411);

add2b_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- add2b_ff_adffs_a29_a = DFFE(!GLOBAL(AD_MR_acombout) & add1_ff_adffs_a29_a $ (add2b_ff_adffs_a28_a_a411 $ add2b_ff_adffs_a29_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add1_ff_adffs_a29_a,
	datad => add2b_ff_adffs_a29_a,
	cin => add2b_ff_adffs_a28_a_a411,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => add2b_ff_adffs_a29_a);

TimeConst_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(0),
	combout => TimeConst_a0_a_acombout);

TimeConst_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_FF_adffs_a0_a = DFFE(TimeConst_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_FF_adffs_a0_a);

TimeConst_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(2),
	combout => TimeConst_a2_a_acombout);

TimeConst_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_FF_adffs_a2_a = DFFE(TimeConst_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_FF_adffs_a2_a);

TimeConst_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(3),
	combout => TimeConst_a3_a_acombout);

TimeConst_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_FF_adffs_a3_a = DFFE(TimeConst_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_FF_adffs_a3_a);

tlevel_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(15),
	combout => tlevel_a15_a_acombout);

tlevel_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a15_a = DFFE(tlevel_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a15_a);

Mux_a2186_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2227 = !TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2186,
	cascout => Mux_a2227);

tlevel_Mux_a22_a_a1041_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a22_a_a1041 = (!TimeConst_FF_adffs_a2_a # !TimeConst_FF_adffs_a0_a # !TimeConst_FF_adffs_a1_a) & CASCADE(Mux_a2227)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => TimeConst_FF_adffs_a2_a,
	cascin => Mux_a2227,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a22_a_a1041);

fir_out_a0_a_a1952_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1952 = TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a)
-- fir_out_a0_a_a1956 = TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1952,
	cascout => fir_out_a0_a_a1956);

tlevel_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(14),
	combout => tlevel_a14_a_acombout);

tlevel_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a14_a = DFFE(tlevel_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a14_a);

tlevel_Mux_a22_a_a999_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a22_a_a999 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a22_a_a999);

tlevel_Mux_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a22_a = DFFE(tlevel_Mux_a22_a_a998 # tlevel_Mux_a22_a_a1041 # fir_out_a0_a_a1952 & tlevel_Mux_a22_a_a999, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a22_a_a998,
	datab => tlevel_Mux_a22_a_a1041,
	datac => fir_out_a0_a_a1952,
	datad => tlevel_Mux_a22_a_a999,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a22_a);

tlevel_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(10),
	combout => tlevel_a10_a_acombout);

tlevel_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a10_a = DFFE(tlevel_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a10_a);

tlevel_Mux_a19_a_a1011_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a19_a_a1011 = TimeConst_FF_adffs_a3_a & tlevel_reg_ff_adffs_a10_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a19_a_a1011);

tlevel_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(12),
	combout => tlevel_a12_a_acombout);

tlevel_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a12_a = DFFE(tlevel_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a12_a);

tlevel_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(11),
	combout => tlevel_a11_a_acombout);

tlevel_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a11_a = DFFE(tlevel_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a11_a);

tlevel_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(13),
	combout => tlevel_a13_a_acombout);

tlevel_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a13_a = DFFE(tlevel_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a13_a);

Mux_a2187_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2187 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a13_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F4A4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a14_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2187);

Mux_a2188_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2188 = TimeConst_FF_adffs_a1_a & (Mux_a2187 & (tlevel_reg_ff_adffs_a11_a) # !Mux_a2187 & tlevel_reg_ff_adffs_a12_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2187)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F588",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => tlevel_reg_ff_adffs_a11_a,
	datad => Mux_a2187,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2188);

tlevel_Mux_a20_a_a1007_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a20_a_a1007 = tlevel_reg_ff_adffs_a15_a & (!TimeConst_FF_adffs_a3_a & !TimeConst_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => TimeConst_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a20_a_a1007);

tlevel_Mux_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a19_a = DFFE(tlevel_Mux_a19_a_a1011 # tlevel_Mux_a20_a_a1007 # fir_out_a0_a_a1952 & Mux_a2188, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => tlevel_Mux_a19_a_a1011,
	datac => Mux_a2188,
	datad => tlevel_Mux_a20_a_a1007,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a19_a);

Mux_a2189_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2189 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a # tlevel_reg_ff_adffs_a11_a) # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a13_a & !TimeConst_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEA4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a13_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2189);

Mux_a2190_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2190 = TimeConst_FF_adffs_a0_a & (Mux_a2189 & tlevel_reg_ff_adffs_a10_a # !Mux_a2189 & (tlevel_reg_ff_adffs_a12_a)) # !TimeConst_FF_adffs_a0_a & (Mux_a2189)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a10_a,
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => Mux_a2189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2190);

tlevel_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(9),
	combout => tlevel_a9_a_acombout);

tlevel_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a9_a = DFFE(tlevel_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a9_a);

tlevel_Mux_a18_a_a1014_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a18_a_a1014 = TimeConst_FF_adffs_a3_a & (tlevel_reg_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_FF_adffs_a3_a,
	datad => tlevel_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a18_a_a1014);

TimeConst_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(1),
	combout => TimeConst_a1_a_acombout);

TimeConst_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_FF_adffs_a1_a = DFFE(TimeConst_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_FF_adffs_a1_a);

Enable_Level_Str_a16_I : apex20ke_lcell
-- Equation(s):
-- Enable_Level_Str_a16 = !TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a)
-- Enable_Level_Str_a21 = !TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0505",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Enable_Level_Str_a16,
	cascout => Enable_Level_Str_a21);

tlevel_Mux_a18_a_a1042_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a18_a_a1042 = (TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a & (tlevel_reg_ff_adffs_a14_a) # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a15_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a15_a) & CASCADE(Enable_Level_Str_a21)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EC4C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a14_a,
	cascin => Enable_Level_Str_a21,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a18_a_a1042);

tlevel_Mux_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a18_a = DFFE(tlevel_Mux_a18_a_a1014 # tlevel_Mux_a18_a_a1042 # fir_out_a0_a_a1952 & Mux_a2190, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => Mux_a2190,
	datac => tlevel_Mux_a18_a_a1014,
	datad => tlevel_Mux_a18_a_a1042,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a18_a);

Mux_a2191_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2191 = TimeConst_FF_adffs_a1_a & TimeConst_FF_adffs_a0_a # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a11_a # !TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a12_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D9C8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a11_a,
	datad => tlevel_reg_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2191);

Mux_a2192_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2192 = TimeConst_FF_adffs_a1_a & (Mux_a2191 & (tlevel_reg_ff_adffs_a9_a) # !Mux_a2191 & tlevel_reg_ff_adffs_a10_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2191)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F838",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a10_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => Mux_a2191,
	datad => tlevel_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2192);

tlevel_Mux_a17_a_a1003_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a17_a_a1003 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a13_a) # !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a14_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a14_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a17_a_a1003);

tlevel_Mux_a17_a_a1016_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a17_a_a1016 = Enable_Level_Str_a16 & (tlevel_Mux_a17_a_a1003 # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA20",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Enable_Level_Str_a16,
	datab => TimeConst_FF_adffs_a1_a,
	datac => tlevel_reg_ff_adffs_a15_a,
	datad => tlevel_Mux_a17_a_a1003,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a17_a_a1016);

tlevel_Mux_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a17_a = DFFE(tlevel_Mux_a17_a_a1017 # tlevel_Mux_a17_a_a1016 # Mux_a2192 & fir_out_a0_a_a1952, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a17_a_a1017,
	datab => Mux_a2192,
	datac => fir_out_a0_a_a1952,
	datad => tlevel_Mux_a17_a_a1016,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a17_a);

Mux_a2184_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2184 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a # tlevel_reg_ff_adffs_a13_a) # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a15_a & !TimeConst_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEA4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => tlevel_reg_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2184);

Mux_a2185_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2185 = TimeConst_FF_adffs_a0_a & (Mux_a2184 & tlevel_reg_ff_adffs_a12_a # !Mux_a2184 & (tlevel_reg_ff_adffs_a14_a)) # !TimeConst_FF_adffs_a0_a & (Mux_a2184)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a12_a,
	datac => tlevel_reg_ff_adffs_a14_a,
	datad => Mux_a2184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2185);

Mux_a2193_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2193 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a # tlevel_reg_ff_adffs_a9_a) # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a11_a & !TimeConst_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CEC2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a11_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2193);

tlevel_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(8),
	combout => tlevel_a8_a_acombout);

tlevel_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a8_a = DFFE(tlevel_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a8_a);

Mux_a2194_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2194 = TimeConst_FF_adffs_a0_a & (Mux_a2193 & (tlevel_reg_ff_adffs_a8_a) # !Mux_a2193 & tlevel_reg_ff_adffs_a10_a) # !TimeConst_FF_adffs_a0_a & Mux_a2193

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EC64",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => Mux_a2193,
	datac => tlevel_reg_ff_adffs_a10_a,
	datad => tlevel_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2194);

tlevel_Mux_a16_a_a1021_I : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a16_a_a1021 = Enable_Level_Str_a16 & (Mux_a2185 # fir_out_a0_a_a1952 & Mux_a2194) # !Enable_Level_Str_a16 & (fir_out_a0_a_a1952 & Mux_a2194)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Enable_Level_Str_a16,
	datab => Mux_a2185,
	datac => fir_out_a0_a_a1952,
	datad => Mux_a2194,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_Mux_a16_a_a1021);

tlevel_Mux_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a16_a = DFFE(tlevel_Mux_a16_a_a1021 # tlevel_reg_ff_adffs_a7_a & TimeConst_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a7_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => tlevel_Mux_a16_a_a1021,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a16_a);

tlevel_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(7),
	combout => tlevel_a7_a_acombout);

tlevel_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a7_a = DFFE(tlevel_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a7_a);

tlevel_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(5),
	combout => tlevel_a5_a_acombout);

tlevel_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a5_a = DFFE(tlevel_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a5_a);

Mux_a2201_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2201 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a # tlevel_reg_ff_adffs_a5_a) # !TimeConst_FF_adffs_a1_a & !TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BA98",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a7_a,
	datad => tlevel_reg_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2201);

tlevel_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(4),
	combout => tlevel_a4_a_acombout);

tlevel_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a4_a = DFFE(tlevel_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a4_a);

Mux_a2202_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2202 = Mux_a2201 & (tlevel_reg_ff_adffs_a4_a # !TimeConst_FF_adffs_a0_a) # !Mux_a2201 & tlevel_reg_ff_adffs_a6_a & TimeConst_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EC2C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a6_a,
	datab => Mux_a2201,
	datac => TimeConst_FF_adffs_a0_a,
	datad => tlevel_reg_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2202);

tlevel_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(2),
	combout => tlevel_a2_a_acombout);

tlevel_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a2_a = DFFE(tlevel_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a2_a);

tlevel_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(3),
	combout => tlevel_a3_a_acombout);

tlevel_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a3_a = DFFE(tlevel_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a3_a);

tlevel_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_tlevel(1),
	combout => tlevel_a1_a_acombout);

tlevel_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_reg_ff_adffs_a1_a = DFFE(tlevel_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_reg_ff_adffs_a1_a);

Mux_a2209_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2209 = TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a & (tlevel_reg_ff_adffs_a1_a) # !TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F4A4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a3_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => tlevel_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2209);

Mux_a2210_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2210 = TimeConst_FF_adffs_a0_a & (Mux_a2209 & tlevel_reg_ff_adffs_a0_a # !Mux_a2209 & (tlevel_reg_ff_adffs_a2_a)) # !TimeConst_FF_adffs_a0_a & (Mux_a2209)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => Mux_a2209,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2210);

tlevel_Mux_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a8_a = DFFE(fir_out_a0_a_a1952 & (Mux_a2210 # Mux_a2202 & Enable_Level_Str_a16) # !fir_out_a0_a_a1952 & Mux_a2202 & Enable_Level_Str_a16, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1952,
	datab => Mux_a2202,
	datac => Enable_Level_Str_a16,
	datad => Mux_a2210,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a8_a);

Mux_a2205_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2205 = TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a & tlevel_reg_ff_adffs_a3_a # !TimeConst_FF_adffs_a1_a & (tlevel_reg_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EE30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a3_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => tlevel_reg_ff_adffs_a5_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2205);

Mux_a2206_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2206 = TimeConst_FF_adffs_a0_a & (Mux_a2205 & tlevel_reg_ff_adffs_a2_a # !Mux_a2205 & (tlevel_reg_ff_adffs_a4_a)) # !TimeConst_FF_adffs_a0_a & (Mux_a2205)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a2_a,
	datac => tlevel_reg_ff_adffs_a4_a,
	datad => Mux_a2205,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2206);

Mux_a2225_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2225 = (!TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & tlevel_reg_ff_adffs_a0_a # !TimeConst_FF_adffs_a0_a & (tlevel_reg_ff_adffs_a1_a))) & CASCADE(fir_out_a0_a_a1956)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_reg_ff_adffs_a0_a,
	datab => tlevel_reg_ff_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => TimeConst_FF_adffs_a1_a,
	cascin => fir_out_a0_a_a1956,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2225);

tlevel_Mux_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a6_a = DFFE(Mux_a2225 # !TimeConst_FF_adffs_a2_a & Mux_a2206 & !TimeConst_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF04",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datab => Mux_a2206,
	datac => TimeConst_FF_adffs_a3_a,
	datad => Mux_a2225,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a6_a);

tlevel_Mux_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- tlevel_Mux_a4_a = DFFE(!TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a & Mux_a2210), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0500",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => Mux_a2210,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tlevel_Mux_a4_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a1_a_a2100_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(tlevel_Mux_a1_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a0_a # !add2b_ff_adffs_a1_a) # !tlevel_Mux_a1_a & !add2b_ff_adffs_a1_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a1_a,
	datab => add2b_ff_adffs_a1_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a1_a_a2100,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a1_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a2_a_a2099_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(tlevel_Mux_a2_a & add2b_ff_adffs_a2_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a1_a # !tlevel_Mux_a2_a & (add2b_ff_adffs_a2_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a2_a,
	datab => add2b_ff_adffs_a2_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a2_a_a2099,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a2_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a3_a_a2098_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(tlevel_Mux_a3_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a2_a # !add2b_ff_adffs_a3_a) # !tlevel_Mux_a3_a & !add2b_ff_adffs_a3_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a3_a,
	datab => add2b_ff_adffs_a3_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a3_a_a2098,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a3_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a4_a_a2097_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(add2b_ff_adffs_a4_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a3_a # !tlevel_Mux_a4_a) # !add2b_ff_adffs_a4_a & !tlevel_Mux_a4_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a4_a,
	datab => tlevel_Mux_a4_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a4_a_a2097,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a4_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a5_a_a2096_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(tlevel_Mux_a5_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a4_a # !add2b_ff_adffs_a5_a) # !tlevel_Mux_a5_a & !add2b_ff_adffs_a5_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a5_a,
	datab => add2b_ff_adffs_a5_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a5_a_a2096,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a5_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a6_a_a2095_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(add2b_ff_adffs_a6_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a5_a # !tlevel_Mux_a6_a) # !add2b_ff_adffs_a6_a & !tlevel_Mux_a6_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => tlevel_Mux_a6_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a6_a_a2095,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a6_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a7_a_a2094_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(tlevel_Mux_a7_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a6_a # !add2b_ff_adffs_a7_a) # !tlevel_Mux_a7_a & !add2b_ff_adffs_a7_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a7_a,
	datab => add2b_ff_adffs_a7_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a7_a_a2094,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a7_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a8_a_a2093_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(add2b_ff_adffs_a8_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a7_a # !tlevel_Mux_a8_a) # !add2b_ff_adffs_a8_a & !tlevel_Mux_a8_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a8_a,
	datab => tlevel_Mux_a8_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a8_a_a2093,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a8_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a9_a_a2092_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(tlevel_Mux_a9_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a8_a # !add2b_ff_adffs_a9_a) # !tlevel_Mux_a9_a & !add2b_ff_adffs_a9_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a9_a,
	datab => add2b_ff_adffs_a9_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a9_a_a2092,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a9_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a10_a_a2091_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(tlevel_Mux_a10_a & add2b_ff_adffs_a10_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a9_a # !tlevel_Mux_a10_a & (add2b_ff_adffs_a10_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a10_a,
	datab => add2b_ff_adffs_a10_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a10_a_a2091,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a10_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a11_a_a2090_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(tlevel_Mux_a11_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a10_a # !add2b_ff_adffs_a11_a) # !tlevel_Mux_a11_a & !add2b_ff_adffs_a11_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a11_a,
	datab => add2b_ff_adffs_a11_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a11_a_a2090,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a11_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a12_a_a2089_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(tlevel_Mux_a12_a & add2b_ff_adffs_a12_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a11_a # !tlevel_Mux_a12_a & (add2b_ff_adffs_a12_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a12_a,
	datab => add2b_ff_adffs_a12_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a12_a_a2089,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a12_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a13_a_a2088_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(tlevel_Mux_a13_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a12_a # !add2b_ff_adffs_a13_a) # !tlevel_Mux_a13_a & !add2b_ff_adffs_a13_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a13_a,
	datab => add2b_ff_adffs_a13_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a13_a_a2088,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a13_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a14_a_a2087_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(tlevel_Mux_a14_a & add2b_ff_adffs_a14_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a13_a # !tlevel_Mux_a14_a & (add2b_ff_adffs_a14_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a14_a,
	datab => add2b_ff_adffs_a14_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a14_a_a2087,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a14_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a15_a_a2086_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a15_a = CARRY(tlevel_Mux_a15_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a14_a # !add2b_ff_adffs_a15_a) # !tlevel_Mux_a15_a & !add2b_ff_adffs_a15_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a15_a,
	datab => add2b_ff_adffs_a15_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a15_a_a2086,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a15_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a16_a_a2085_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a16_a = CARRY(add2b_ff_adffs_a16_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a15_a # !tlevel_Mux_a16_a) # !add2b_ff_adffs_a16_a & !tlevel_Mux_a16_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a16_a,
	datab => tlevel_Mux_a16_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a16_a_a2085,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a16_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a17_a_a2084_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a17_a = CARRY(add2b_ff_adffs_a17_a & tlevel_Mux_a17_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a16_a # !add2b_ff_adffs_a17_a & (tlevel_Mux_a17_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => tlevel_Mux_a17_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a17_a_a2084,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a17_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a18_a_a2083_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a18_a = CARRY(add2b_ff_adffs_a18_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a17_a # !tlevel_Mux_a18_a) # !add2b_ff_adffs_a18_a & !tlevel_Mux_a18_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datab => tlevel_Mux_a18_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a18_a_a2083,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a18_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a19_a_a2082_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a19_a = CARRY(add2b_ff_adffs_a19_a & tlevel_Mux_a19_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a18_a # !add2b_ff_adffs_a19_a & (tlevel_Mux_a19_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a19_a,
	datab => tlevel_Mux_a19_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a19_a_a2082,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a19_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a20_a_a2081_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a20_a = CARRY(tlevel_Mux_a20_a & add2b_ff_adffs_a20_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a19_a # !tlevel_Mux_a20_a & (add2b_ff_adffs_a20_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a20_a,
	datab => add2b_ff_adffs_a20_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a20_a_a2081,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a20_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a21_a_a2080_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a21_a = CARRY(tlevel_Mux_a21_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a20_a # !add2b_ff_adffs_a21_a) # !tlevel_Mux_a21_a & !add2b_ff_adffs_a21_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a20_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a21_a,
	datab => add2b_ff_adffs_a21_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a21_a_a2080,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a21_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a22_a_a2079_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a22_a = CARRY(add2b_ff_adffs_a22_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a21_a # !tlevel_Mux_a22_a) # !add2b_ff_adffs_a22_a & !tlevel_Mux_a22_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a22_a,
	datab => tlevel_Mux_a22_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a22_a_a2079,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a22_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a23_a_a2078_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a23_a = CARRY(tlevel_Mux_a23_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a22_a # !add2b_ff_adffs_a23_a) # !tlevel_Mux_a23_a & !add2b_ff_adffs_a23_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a23_a,
	datab => add2b_ff_adffs_a23_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a23_a_a2078,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a23_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a24_a_a2077_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a24_a = CARRY(tlevel_Mux_a29_a & add2b_ff_adffs_a24_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a23_a # !tlevel_Mux_a29_a & (add2b_ff_adffs_a24_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datab => add2b_ff_adffs_a24_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a24_a_a2077,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a24_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a25_a_a2076_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a25_a = CARRY(tlevel_Mux_a29_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a24_a # !add2b_ff_adffs_a25_a) # !tlevel_Mux_a29_a & !add2b_ff_adffs_a25_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datab => add2b_ff_adffs_a25_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a25_a_a2076,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a25_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a26_a_a2075_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a26_a = CARRY(tlevel_Mux_a29_a & add2b_ff_adffs_a26_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a25_a # !tlevel_Mux_a29_a & (add2b_ff_adffs_a26_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datab => add2b_ff_adffs_a26_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a26_a_a2075,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a26_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a27_a_a2074_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a27_a = CARRY(tlevel_Mux_a29_a & (!tlevel_cmp_acomparator_acmp_end_alcarry_a26_a # !add2b_ff_adffs_a27_a) # !tlevel_Mux_a29_a & !add2b_ff_adffs_a27_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datab => add2b_ff_adffs_a27_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a27_a_a2074,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a27_a);

tlevel_cmp_acomparator_acmp_end_alcarry_a28_a_a2073_I : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_alcarry_a28_a = CARRY(tlevel_Mux_a29_a & add2b_ff_adffs_a28_a & !tlevel_cmp_acomparator_acmp_end_alcarry_a27_a # !tlevel_Mux_a29_a & (add2b_ff_adffs_a28_a # !tlevel_cmp_acomparator_acmp_end_alcarry_a27_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datab => add2b_ff_adffs_a28_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_alcarry_a28_a_a2073,
	cout => tlevel_cmp_acomparator_acmp_end_alcarry_a28_a);

tlevel_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- tlevel_cmp_acomparator_acmp_end_aagb_out = tlevel_Mux_a29_a & (tlevel_cmp_acomparator_acmp_end_alcarry_a28_a # !add2b_ff_adffs_a29_a) # !tlevel_Mux_a29_a & (tlevel_cmp_acomparator_acmp_end_alcarry_a28_a & !add2b_ff_adffs_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A0FA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_Mux_a29_a,
	datad => add2b_ff_adffs_a29_a,
	cin => tlevel_cmp_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tlevel_cmp_acomparator_acmp_end_aagb_out);

level_cmp_Del_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- level_cmp_Del_vec_a0_a = DFFE(tlevel_cmp_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tlevel_cmp_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => level_cmp_Del_vec_a0_a);

Level_Out_a107_I : apex20ke_lcell
-- Equation(s):
-- Level_Out_a107 = (level_cmp_Del_vec_a1_a # level_cmp_Del_vec_a0_a) & CASCADE(Enable_Level_Str_a20)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => level_cmp_Del_vec_a1_a,
	datad => level_cmp_Del_vec_a0_a,
	cascin => Enable_Level_Str_a20,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Level_Out_a107);

slope_wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!slope_wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(slope_wadr_cntr_awysi_counter_asload_path_a1_a $ slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !slope_wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => slope_wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(slope_wadr_cntr_awysi_counter_asload_path_a2_a $ !slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a2_a & !slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => slope_wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(slope_wadr_cntr_awysi_counter_asload_path_a3_a $ slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !slope_wadr_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => slope_wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a3_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a4_a = DFFE(slope_wadr_cntr_awysi_counter_asload_path_a4_a $ !slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a4_a & !slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => slope_wadr_cntr_awysi_counter_asload_path_a4_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a4_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a5_a = DFFE(slope_wadr_cntr_awysi_counter_asload_path_a5_a $ slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- slope_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !slope_wadr_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => slope_wadr_cntr_awysi_counter_asload_path_a5_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a5_a,
	cout => slope_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

slope_wadr_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- slope_wadr_cntr_awysi_counter_asload_path_a6_a = DFFE(slope_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT $ !slope_wadr_cntr_awysi_counter_asload_path_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => slope_wadr_cntr_awysi_counter_asload_path_a6_a,
	cin => slope_wadr_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => slope_wadr_cntr_awysi_counter_asload_path_a6_a);

slope_diff_a2_a_a54_I : apex20ke_lcell
-- Equation(s):
-- slope_diff_a2_a_a54 = TimeConst_FF_adffs_a2_a # TimeConst_FF_adffs_a3_a # TimeConst_FF_adffs_a0_a & TimeConst_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a2_a,
	datad => TimeConst_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => slope_diff_a2_a_a54);

add_a113_I : apex20ke_lcell
-- Equation(s):
-- add_a113 = slope_wadr_cntr_awysi_counter_asload_path_a0_a $ slope_diff_a2_a_a54
-- add_a115 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a0_a # !slope_diff_a2_a_a54)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a0_a,
	datab => slope_diff_a2_a_a54,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a113,
	cout => add_a115);

add_a117_I : apex20ke_lcell
-- Equation(s):
-- add_a117 = slope_wadr_cntr_awysi_counter_asload_path_a1_a $ slope_diff_a2_a_a54 $ !add_a115
-- add_a119 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a1_a & slope_diff_a2_a_a54 & !add_a115 # !slope_wadr_cntr_awysi_counter_asload_path_a1_a & (slope_diff_a2_a_a54 # !add_a115))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a1_a,
	datab => slope_diff_a2_a_a54,
	cin => add_a115,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a117,
	cout => add_a119);

add_a121_I : apex20ke_lcell
-- Equation(s):
-- add_a121 = slope_wadr_cntr_awysi_counter_asload_path_a2_a $ slope_diff_a2_a_a54 $ add_a119
-- add_a123 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a2_a & (!add_a119 # !slope_diff_a2_a_a54) # !slope_wadr_cntr_awysi_counter_asload_path_a2_a & !slope_diff_a2_a_a54 & !add_a119)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a2_a,
	datab => slope_diff_a2_a_a54,
	cin => add_a119,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a121,
	cout => add_a123);

slope_diff_a5_a_a55_I : apex20ke_lcell
-- Equation(s):
-- slope_diff_a5_a_a55 = TimeConst_FF_adffs_a3_a # TimeConst_FF_adffs_a0_a & TimeConst_FF_adffs_a1_a & TimeConst_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF80",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a2_a,
	datad => TimeConst_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => slope_diff_a5_a_a55);

add_a125_I : apex20ke_lcell
-- Equation(s):
-- add_a125 = slope_wadr_cntr_awysi_counter_asload_path_a3_a $ slope_diff_a5_a_a55 $ !add_a123
-- add_a127 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a3_a & slope_diff_a5_a_a55 & !add_a123 # !slope_wadr_cntr_awysi_counter_asload_path_a3_a & (slope_diff_a5_a_a55 # !add_a123))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a3_a,
	datab => slope_diff_a5_a_a55,
	cin => add_a123,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a125,
	cout => add_a127);

add_a129_I : apex20ke_lcell
-- Equation(s):
-- add_a129 = slope_wadr_cntr_awysi_counter_asload_path_a4_a $ slope_diff_a5_a_a55 $ add_a127
-- add_a131 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a4_a & (!add_a127 # !slope_diff_a5_a_a55) # !slope_wadr_cntr_awysi_counter_asload_path_a4_a & !slope_diff_a5_a_a55 & !add_a127)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a4_a,
	datab => slope_diff_a5_a_a55,
	cin => add_a127,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a129,
	cout => add_a131);

add_a133_I : apex20ke_lcell
-- Equation(s):
-- add_a133 = slope_wadr_cntr_awysi_counter_asload_path_a5_a $ slope_diff_a5_a_a55 $ !add_a131
-- add_a135 = CARRY(slope_wadr_cntr_awysi_counter_asload_path_a5_a & slope_diff_a5_a_a55 & !add_a131 # !slope_wadr_cntr_awysi_counter_asload_path_a5_a & (slope_diff_a5_a_a55 # !add_a131))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a5_a,
	datab => slope_diff_a5_a_a55,
	cin => add_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a133,
	cout => add_a135);

add_a137_I : apex20ke_lcell
-- Equation(s):
-- add_a137 = slope_wadr_cntr_awysi_counter_asload_path_a6_a $ TimeConst_FF_adffs_a3_a $ add_a135

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9696",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => slope_wadr_cntr_awysi_counter_asload_path_a6_a,
	datab => TimeConst_FF_adffs_a3_a,
	cin => add_a135,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a137);

Slope_Gen_Mem_asram_asegment_a0_a_a29_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 29,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a29_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a29_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a29_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a29_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a29_a);

Slope_Gen_Mem_asram_asegment_a0_a_a28_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 28,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a28_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a28_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a28_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a28_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a28_a);

Slope_Gen_Mem_asram_asegment_a0_a_a27_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 27,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a27_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a27_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a27_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a27_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a27_a);

Slope_Gen_Mem_asram_asegment_a0_a_a26_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 26,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a26_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a26_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a26_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a26_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a26_a);

Slope_Gen_Mem_asram_asegment_a0_a_a25_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 25,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a25_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a25_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a25_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a25_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a25_a);

Slope_Gen_Mem_asram_asegment_a0_a_a24_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 24,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a24_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a24_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a24_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a24_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a24_a);

Slope_Gen_Mem_asram_asegment_a0_a_a23_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 23,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a23_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a23_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a23_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a23_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a23_a);

Slope_Gen_Mem_asram_asegment_a0_a_a22_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 22,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a22_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a22_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a22_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a22_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a22_a);

Slope_Gen_Mem_asram_asegment_a0_a_a21_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 21,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a21_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a21_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a21_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a21_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a21_a);

Slope_Gen_Mem_asram_asegment_a0_a_a20_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 20,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a20_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a20_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a20_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a20_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a20_a);

Slope_Gen_Mem_asram_asegment_a0_a_a19_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 19,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a19_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a19_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a19_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a19_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a19_a);

Slope_Gen_Mem_asram_asegment_a0_a_a18_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 18,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a18_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a18_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a18_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a18_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a18_a);

Slope_Gen_Mem_asram_asegment_a0_a_a17_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 17,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a17_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a17_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a17_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a17_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a17_a);

Slope_Gen_Mem_asram_asegment_a0_a_a16_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 16,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a16_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a16_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a16_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a16_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a16_a);

Slope_Gen_Mem_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a15_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a15_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a15_a);

Slope_Gen_Mem_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a14_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a14_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a14_a);

Slope_Gen_Mem_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a13_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a13_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a13_a);

Slope_Gen_Mem_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a12_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a12_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a12_a);

Slope_Gen_Mem_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a11_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a11_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a11_a);

Slope_Gen_Mem_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a10_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a10_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a10_a);

Slope_Gen_Mem_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a9_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a9_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a9_a);

Slope_Gen_Mem_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a8_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a8_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a8_a);

Slope_Gen_Mem_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a7_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a7_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a7_a);

Slope_Gen_Mem_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a6_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a6_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a6_a);

Slope_Gen_Mem_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a5_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a5_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a5_a);

Slope_Gen_Mem_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a4_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a4_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a4_a);

Slope_Gen_Mem_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a3_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a3_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a3_a);

Slope_Gen_Mem_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a2_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a2_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a2_a);

Slope_Gen_Mem_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => add2b_ff_adffs_a1_a,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a1_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a1_a);

Slope_Gen_Mem_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 7,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 127,
	logical_ram_depth => 128,
	logical_ram_name => "lpm_ram_dp:Slope_Gen_Mem|altdpram:sram|content",
	logical_ram_width => 30,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => Slope_Gen_Mem_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Slope_Gen_Mem_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Slope_Gen_Mem_asram_asegment_a0_a_a0_a_modesel,
	dataout => Slope_Gen_Mem_asram_aq_a0_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a_a2108_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a = CARRY(tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101 & !Slope_Gen_Mem_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tlevel_cmp_acomparator_acmp_end_alcarry_a0_a_a2101,
	datab => Slope_Gen_Mem_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a_a2108,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a_a2107_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a = CARRY(add2b_ff_adffs_a1_a & Slope_Gen_Mem_asram_aq_a1_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a # !add2b_ff_adffs_a1_a & (Slope_Gen_Mem_asram_aq_a1_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a1_a,
	datab => Slope_Gen_Mem_asram_aq_a1_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a_a2107,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a_a2106_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a = CARRY(add2b_ff_adffs_a2_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a # !Slope_Gen_Mem_asram_aq_a2_a) # !add2b_ff_adffs_a2_a & !Slope_Gen_Mem_asram_aq_a2_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a2_a,
	datab => Slope_Gen_Mem_asram_aq_a2_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a_a2106,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a_a2105_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a = CARRY(add2b_ff_adffs_a3_a & Slope_Gen_Mem_asram_aq_a3_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a # !add2b_ff_adffs_a3_a & (Slope_Gen_Mem_asram_aq_a3_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a3_a,
	datab => Slope_Gen_Mem_asram_aq_a3_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a_a2105,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a_a2104_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a = CARRY(add2b_ff_adffs_a4_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a # !Slope_Gen_Mem_asram_aq_a4_a) # !add2b_ff_adffs_a4_a & !Slope_Gen_Mem_asram_aq_a4_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a4_a,
	datab => Slope_Gen_Mem_asram_aq_a4_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a_a2104,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a_a2103_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a = CARRY(add2b_ff_adffs_a5_a & Slope_Gen_Mem_asram_aq_a5_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a # !add2b_ff_adffs_a5_a & (Slope_Gen_Mem_asram_aq_a5_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => Slope_Gen_Mem_asram_aq_a5_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a_a2103,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a_a2102_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a = CARRY(add2b_ff_adffs_a6_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a # !Slope_Gen_Mem_asram_aq_a6_a) # !add2b_ff_adffs_a6_a & !Slope_Gen_Mem_asram_aq_a6_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => Slope_Gen_Mem_asram_aq_a6_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a_a2102,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a_a2101_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a = CARRY(add2b_ff_adffs_a7_a & Slope_Gen_Mem_asram_aq_a7_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a # !add2b_ff_adffs_a7_a & (Slope_Gen_Mem_asram_aq_a7_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a7_a,
	datab => Slope_Gen_Mem_asram_aq_a7_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a_a2101,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a_a2100_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a = CARRY(add2b_ff_adffs_a8_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a # !Slope_Gen_Mem_asram_aq_a8_a) # !add2b_ff_adffs_a8_a & !Slope_Gen_Mem_asram_aq_a8_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a8_a,
	datab => Slope_Gen_Mem_asram_aq_a8_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a_a2100,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a_a2099_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a = CARRY(add2b_ff_adffs_a9_a & Slope_Gen_Mem_asram_aq_a9_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a # !add2b_ff_adffs_a9_a & (Slope_Gen_Mem_asram_aq_a9_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => Slope_Gen_Mem_asram_aq_a9_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a_a2099,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a_a2098_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a = CARRY(add2b_ff_adffs_a10_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a # !Slope_Gen_Mem_asram_aq_a10_a) # !add2b_ff_adffs_a10_a & !Slope_Gen_Mem_asram_aq_a10_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a10_a,
	datab => Slope_Gen_Mem_asram_aq_a10_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a_a2098,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a_a2097_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a = CARRY(add2b_ff_adffs_a11_a & Slope_Gen_Mem_asram_aq_a11_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a # !add2b_ff_adffs_a11_a & (Slope_Gen_Mem_asram_aq_a11_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a11_a,
	datab => Slope_Gen_Mem_asram_aq_a11_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a_a2097,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a_a2096_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a = CARRY(add2b_ff_adffs_a12_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a # !Slope_Gen_Mem_asram_aq_a12_a) # !add2b_ff_adffs_a12_a & !Slope_Gen_Mem_asram_aq_a12_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a12_a,
	datab => Slope_Gen_Mem_asram_aq_a12_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a_a2096,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a_a2095_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a = CARRY(add2b_ff_adffs_a13_a & Slope_Gen_Mem_asram_aq_a13_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a # !add2b_ff_adffs_a13_a & (Slope_Gen_Mem_asram_aq_a13_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a13_a,
	datab => Slope_Gen_Mem_asram_aq_a13_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a_a2095,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a_a2094_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a = CARRY(add2b_ff_adffs_a14_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a # !Slope_Gen_Mem_asram_aq_a14_a) # !add2b_ff_adffs_a14_a & !Slope_Gen_Mem_asram_aq_a14_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a14_a,
	datab => Slope_Gen_Mem_asram_aq_a14_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a_a2094,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a_a2093_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a = CARRY(add2b_ff_adffs_a15_a & Slope_Gen_Mem_asram_aq_a15_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a # !add2b_ff_adffs_a15_a & (Slope_Gen_Mem_asram_aq_a15_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a15_a,
	datab => Slope_Gen_Mem_asram_aq_a15_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a_a2093,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a_a2092_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a = CARRY(add2b_ff_adffs_a16_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a # !Slope_Gen_Mem_asram_aq_a16_a) # !add2b_ff_adffs_a16_a & !Slope_Gen_Mem_asram_aq_a16_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a16_a,
	datab => Slope_Gen_Mem_asram_aq_a16_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a_a2092,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a_a2091_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a = CARRY(add2b_ff_adffs_a17_a & Slope_Gen_Mem_asram_aq_a17_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a # !add2b_ff_adffs_a17_a & (Slope_Gen_Mem_asram_aq_a17_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => Slope_Gen_Mem_asram_aq_a17_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a_a2091,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a_a2090_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a = CARRY(add2b_ff_adffs_a18_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a # !Slope_Gen_Mem_asram_aq_a18_a) # !add2b_ff_adffs_a18_a & !Slope_Gen_Mem_asram_aq_a18_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datab => Slope_Gen_Mem_asram_aq_a18_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a_a2090,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a_a2089_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a = CARRY(add2b_ff_adffs_a19_a & Slope_Gen_Mem_asram_aq_a19_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a # !add2b_ff_adffs_a19_a & (Slope_Gen_Mem_asram_aq_a19_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a19_a,
	datab => Slope_Gen_Mem_asram_aq_a19_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a_a2089,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a_a2088_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a = CARRY(add2b_ff_adffs_a20_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a # !Slope_Gen_Mem_asram_aq_a20_a) # !add2b_ff_adffs_a20_a & !Slope_Gen_Mem_asram_aq_a20_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a20_a,
	datab => Slope_Gen_Mem_asram_aq_a20_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a_a2088,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a_a2087_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a = CARRY(add2b_ff_adffs_a21_a & Slope_Gen_Mem_asram_aq_a21_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a # !add2b_ff_adffs_a21_a & (Slope_Gen_Mem_asram_aq_a21_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => Slope_Gen_Mem_asram_aq_a21_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a_a2087,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a_a2086_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a = CARRY(add2b_ff_adffs_a22_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a # !Slope_Gen_Mem_asram_aq_a22_a) # !add2b_ff_adffs_a22_a & !Slope_Gen_Mem_asram_aq_a22_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a22_a,
	datab => Slope_Gen_Mem_asram_aq_a22_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a_a2086,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a_a2085_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a = CARRY(add2b_ff_adffs_a23_a & Slope_Gen_Mem_asram_aq_a23_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a # !add2b_ff_adffs_a23_a & (Slope_Gen_Mem_asram_aq_a23_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a23_a,
	datab => Slope_Gen_Mem_asram_aq_a23_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a_a2085,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a_a2084_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a = CARRY(add2b_ff_adffs_a24_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a # !Slope_Gen_Mem_asram_aq_a24_a) # !add2b_ff_adffs_a24_a & !Slope_Gen_Mem_asram_aq_a24_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a24_a,
	datab => Slope_Gen_Mem_asram_aq_a24_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a_a2084,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a_a2083_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a = CARRY(add2b_ff_adffs_a25_a & Slope_Gen_Mem_asram_aq_a25_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a # !add2b_ff_adffs_a25_a & (Slope_Gen_Mem_asram_aq_a25_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a25_a,
	datab => Slope_Gen_Mem_asram_aq_a25_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a_a2083,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a_a2082_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a = CARRY(add2b_ff_adffs_a26_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a # !Slope_Gen_Mem_asram_aq_a26_a) # !add2b_ff_adffs_a26_a & !Slope_Gen_Mem_asram_aq_a26_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a26_a,
	datab => Slope_Gen_Mem_asram_aq_a26_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a_a2082,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a_a2081_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a = CARRY(add2b_ff_adffs_a27_a & Slope_Gen_Mem_asram_aq_a27_a & !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a # !add2b_ff_adffs_a27_a & (Slope_Gen_Mem_asram_aq_a27_a # 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a27_a,
	datab => Slope_Gen_Mem_asram_aq_a27_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a_a2081,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a);

pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a_a2080_I : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a = CARRY(add2b_ff_adffs_a28_a & (!pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a # !Slope_Gen_Mem_asram_aq_a28_a) # !add2b_ff_adffs_a28_a & !Slope_Gen_Mem_asram_aq_a28_a & 
-- !pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a28_a,
	datab => Slope_Gen_Mem_asram_aq_a28_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a_a2080,
	cout => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a);

pslope_Gen_Comp1_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- pslope_Gen_Comp1_acomparator_acmp_end_aagb_out = add2b_ff_adffs_a29_a & (pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a & Slope_Gen_Mem_asram_aq_a29_a) # !add2b_ff_adffs_a29_a & (pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a # 
-- Slope_Gen_Mem_asram_aq_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F550",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a29_a,
	datad => Slope_Gen_Mem_asram_aq_a29_a,
	cin => pslope_Gen_Comp1_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => pslope_Gen_Comp1_acomparator_acmp_end_aagb_out);

pslope_vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a0_a = DFFE(pslope_Gen_Comp1_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_Gen_Comp1_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a0_a);

pslope_vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a1_a = DFFE(pslope_vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a1_a);

pslope_vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a2_a = DFFE(pslope_vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pslope_vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a2_a);

pslope_vec_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a3_a = DFFE(pslope_vec_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a3_a);

pslope_vec_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a4_a = DFFE(pslope_vec_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pslope_vec_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a4_a);

pslope_vec_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- pslope_vec_a5_a = DFFE(pslope_vec_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => pslope_vec_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_vec_a5_a);

PSlope_Cmp_a410_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a410 = pslope_vec_a5_a # pslope_vec_a3_a # pslope_vec_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pslope_vec_a5_a,
	datac => pslope_vec_a3_a,
	datad => pslope_vec_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a410);

PSlope_Cmp_a409_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a409 = pslope_vec_a0_a # pslope_vec_a1_a # pslope_vec_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pslope_vec_a0_a,
	datac => pslope_vec_a1_a,
	datad => pslope_vec_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a409);

PSlope_Cmp_a411_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a411 = PSlope_Cmp_a409 # PSlope_Cmp_a410 & (TimeConst_FF_adffs_a1_a # !Enable_Level_Str_a16)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF8C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => PSlope_Cmp_a410,
	datac => Enable_Level_Str_a16,
	datad => PSlope_Cmp_a409,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a411);

PSlope_Cmp_a407_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a407 = !pslope_vec_a2_a # !pslope_vec_a1_a # !pslope_vec_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pslope_vec_a0_a,
	datac => pslope_vec_a1_a,
	datad => pslope_vec_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a407);

PSlope_Cmp_a406_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a406 = !pslope_vec_a4_a # !pslope_vec_a3_a # !pslope_vec_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pslope_vec_a5_a,
	datac => pslope_vec_a3_a,
	datad => pslope_vec_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a406);

PSlope_Cmp_a408_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp_a408 = !PSlope_Cmp_a407 & (!TimeConst_FF_adffs_a1_a & Enable_Level_Str_a16 # !PSlope_Cmp_a406)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => PSlope_Cmp_a407,
	datac => Enable_Level_Str_a16,
	datad => PSlope_Cmp_a406,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Cmp_a408);

PSlope_Cmp_aI : apex20ke_lcell
-- Equation(s):
-- PSlope_Cmp = DFFE(PSlope_Cmp_a408 # PSlope_Cmp_a411 & PSlope_Cmp, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Cmp_a411,
	datac => PSlope_Cmp,
	datad => PSlope_Cmp_a408,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PSlope_Cmp);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

Disc_En_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En,
	combout => Disc_En_acombout);

Level_Out_a101_I : apex20ke_lcell
-- Equation(s):
-- Level_Out_a101 = PSlope_Del & !PSlope_Cmp & !PA_Reset_acombout & Disc_En_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PSlope_Del,
	datab => PSlope_Cmp,
	datac => PA_Reset_acombout,
	datad => Disc_En_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Level_Out_a101);

Level_Out_areg0_I : apex20ke_lcell
-- Equation(s):
-- Level_Out_areg0 = DFFE(Level_Out_a101 & (Level_Out_a107 # tlevel_cmp_acomparator_acmp_end_aagb_out), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Level_Out_a107,
	datac => tlevel_cmp_acomparator_acmp_end_aagb_out,
	datad => Level_Out_a101,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Level_Out_areg0);

Mux_a2150_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2150 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a4_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & (!TimeConst_FF_adffs_a1_a & add2b_ff_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a4_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => add2b_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2150);

Mux_a2151_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2151 = TimeConst_FF_adffs_a1_a & (Mux_a2150 & add2b_ff_adffs_a6_a # !Mux_a2150 & (add2b_ff_adffs_a5_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2150)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => add2b_ff_adffs_a5_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2150,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2151);

fir_out_a0_a_a1900_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1900 = add2b_ff_adffs_a7_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a7_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1900);

fir_out_a0_a_a1901_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1901 = TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1901);

fir_out_a0_a_a1898_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1898 = !TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)
-- fir_out_a0_a_a1955 = !TimeConst_FF_adffs_a2_a & (!TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0005",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a2_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1898,
	cascout => fir_out_a0_a_a1955);

fir_out_a0_a_a1899_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_a1899 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a2_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a2_a,
	datab => add2b_ff_adffs_a1_a,
	datac => fir_out_a0_a_a1898,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a0_a_a1899);

fir_out_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a0_a_areg0 = DFFE(fir_out_a0_a_a1900 # fir_out_a0_a_a1899 # Mux_a2151 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2151,
	datab => fir_out_a0_a_a1900,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a0_a_a1899,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a0_a_areg0);

Mux_a2153_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2153 = Mux_a2152 & (add2b_ff_adffs_a7_a # !TimeConst_FF_adffs_a1_a) # !Mux_a2152 & add2b_ff_adffs_a6_a & (TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E4AA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2152,
	datab => add2b_ff_adffs_a6_a,
	datac => add2b_ff_adffs_a7_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2153);

fir_out_a1_a_a1904_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a1_a_a1904 = !PA_Reset_acombout & TimeConst_FF_adffs_a3_a & add2b_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => TimeConst_FF_adffs_a3_a,
	datac => add2b_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a1_a_a1904);

fir_out_a1_a_a1903_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a1_a_a1903 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a3_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a2_a,
	datac => add2b_ff_adffs_a3_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a1_a_a1903);

fir_out_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a1_a_areg0 = DFFE(fir_out_a1_a_a1904 # fir_out_a1_a_a1903 # Mux_a2153 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2153,
	datab => fir_out_a1_a_a1904,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a1_a_a1903,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a1_a_areg0);

fir_out_a2_a_a1907_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a2_a_a1907 = add2b_ff_adffs_a9_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a9_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a2_a_a1907);

Mux_a2154_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2154 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a6_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a5_a & (!TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCE2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a6_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2154);

Mux_a2155_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2155 = TimeConst_FF_adffs_a1_a & (Mux_a2154 & add2b_ff_adffs_a8_a # !Mux_a2154 & (add2b_ff_adffs_a7_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2154)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BBC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a8_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => add2b_ff_adffs_a7_a,
	datad => Mux_a2154,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2155);

fir_out_a2_a_a1906_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a2_a_a1906 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a4_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a3_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a4_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a2_a_a1906);

fir_out_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a2_a_areg0 = DFFE(fir_out_a2_a_a1907 # fir_out_a2_a_a1906 # Mux_a2155 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a2_a_a1907,
	datab => Mux_a2155,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a2_a_a1906,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a2_a_areg0);

Mux_a2156_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2156 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a7_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a6_a & (!TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCE2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a7_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2156);

Mux_a2157_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2157 = TimeConst_FF_adffs_a1_a & (Mux_a2156 & add2b_ff_adffs_a9_a # !Mux_a2156 & (add2b_ff_adffs_a8_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2156)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a9_a,
	datac => add2b_ff_adffs_a8_a,
	datad => Mux_a2156,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2157);

fir_out_a3_a_a1910_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a3_a_a1910 = TimeConst_FF_adffs_a3_a & (add2b_ff_adffs_a10_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => add2b_ff_adffs_a10_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a3_a_a1910);

fir_out_a3_a_a1909_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a3_a_a1909 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a5_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a4_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a5_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a3_a_a1909);

fir_out_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a3_a_areg0 = DFFE(fir_out_a3_a_a1910 # fir_out_a3_a_a1909 # Mux_a2157 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2157,
	datab => fir_out_a3_a_a1910,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a3_a_a1909,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a3_a_areg0);

Mux_a2158_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2158 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a8_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a7_a & (!TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAE4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a7_a,
	datac => add2b_ff_adffs_a8_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2158);

Mux_a2159_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2159 = TimeConst_FF_adffs_a1_a & (Mux_a2158 & (add2b_ff_adffs_a10_a) # !Mux_a2158 & add2b_ff_adffs_a9_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2158)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => add2b_ff_adffs_a10_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2158,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2159);

fir_out_a4_a_a1913_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a4_a_a1913 = TimeConst_FF_adffs_a3_a & (add2b_ff_adffs_a11_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a3_a,
	datac => add2b_ff_adffs_a11_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a4_a_a1913);

fir_out_a4_a_a1912_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a4_a_a1912 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a6_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a5_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a6_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a4_a_a1912);

fir_out_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a4_a_areg0 = DFFE(fir_out_a4_a_a1913 # fir_out_a4_a_a1912 # Mux_a2159 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2159,
	datab => fir_out_a4_a_a1913,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a4_a_a1912,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a4_a_areg0);

Mux_a2160_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2160 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a9_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA44",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a8_a,
	datac => add2b_ff_adffs_a9_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2160);

Mux_a2161_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2161 = TimeConst_FF_adffs_a1_a & (Mux_a2160 & add2b_ff_adffs_a11_a # !Mux_a2160 & (add2b_ff_adffs_a10_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2160)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a11_a,
	datab => add2b_ff_adffs_a10_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2160,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2161);

fir_out_a5_a_a1916_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a5_a_a1916 = add2b_ff_adffs_a12_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a12_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a5_a_a1916);

fir_out_a5_a_a1915_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a5_a_a1915 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a7_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a6_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a7_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a5_a_a1915);

fir_out_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a5_a_areg0 = DFFE(fir_out_a5_a_a1916 # fir_out_a5_a_a1915 # Mux_a2161 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2161,
	datab => fir_out_a5_a_a1916,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a5_a_a1915,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a5_a_areg0);

fir_out_a6_a_a1919_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a6_a_a1919 = add2b_ff_adffs_a13_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a13_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a6_a_a1919);

Mux_a2162_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2162 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a10_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a9_a,
	datab => add2b_ff_adffs_a10_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2162);

Mux_a2163_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2163 = TimeConst_FF_adffs_a1_a & (Mux_a2162 & (add2b_ff_adffs_a12_a) # !Mux_a2162 & add2b_ff_adffs_a11_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2162)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a11_a,
	datab => add2b_ff_adffs_a12_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2162,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2163);

fir_out_a6_a_a1918_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a6_a_a1918 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a8_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a7_a,
	datac => add2b_ff_adffs_a8_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a6_a_a1918);

fir_out_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a6_a_areg0 = DFFE(fir_out_a6_a_a1919 # fir_out_a6_a_a1918 # Mux_a2163 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a6_a_a1919,
	datab => Mux_a2163,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a6_a_a1918,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a6_a_areg0);

fir_out_a7_a_a1922_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a7_a_a1922 = add2b_ff_adffs_a14_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a14_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a7_a_a1922);

Mux_a2164_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2164 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a11_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a10_a & (!TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAE4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a10_a,
	datac => add2b_ff_adffs_a11_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2164);

Mux_a2165_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2165 = Mux_a2164 & (add2b_ff_adffs_a13_a # !TimeConst_FF_adffs_a1_a) # !Mux_a2164 & (add2b_ff_adffs_a12_a & TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a13_a,
	datab => add2b_ff_adffs_a12_a,
	datac => Mux_a2164,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2165);

fir_out_a7_a_a1921_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a7_a_a1921 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a9_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a8_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a9_a,
	datac => add2b_ff_adffs_a8_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a7_a_a1921);

fir_out_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a7_a_areg0 = DFFE(fir_out_a7_a_a1922 # fir_out_a7_a_a1921 # Mux_a2165 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a7_a_a1922,
	datab => Mux_a2165,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a7_a_a1921,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a7_a_areg0);

Mux_a2166_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2166 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a12_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F2C2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a11_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => add2b_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2166);

Mux_a2167_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2167 = TimeConst_FF_adffs_a1_a & (Mux_a2166 & (add2b_ff_adffs_a14_a) # !Mux_a2166 & add2b_ff_adffs_a13_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2166)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F388",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a13_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => add2b_ff_adffs_a14_a,
	datad => Mux_a2166,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2167);

fir_out_a8_a_a1925_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a8_a_a1925 = add2b_ff_adffs_a15_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a8_a_a1925);

fir_out_a8_a_a1924_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a8_a_a1924 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a10_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a9_a,
	datac => add2b_ff_adffs_a10_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a8_a_a1924);

fir_out_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a8_a_areg0 = DFFE(fir_out_a8_a_a1925 # fir_out_a8_a_a1924 # Mux_a2167 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2167,
	datab => fir_out_a8_a_a1925,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a8_a_a1924,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a8_a_areg0);

fir_out_a9_a_a1927_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a9_a_a1927 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a11_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a10_a,
	datac => add2b_ff_adffs_a11_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a9_a_a1927);

fir_out_a9_a_a1928_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a9_a_a1928 = add2b_ff_adffs_a16_a & TimeConst_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => add2b_ff_adffs_a16_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a9_a_a1928);

Mux_a2168_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2168 = TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a13_a # TimeConst_FF_adffs_a1_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a12_a & (!TimeConst_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAE4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a12_a,
	datac => add2b_ff_adffs_a13_a,
	datad => TimeConst_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2168);

Mux_a2169_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2169 = TimeConst_FF_adffs_a1_a & (Mux_a2168 & add2b_ff_adffs_a15_a # !Mux_a2168 & (add2b_ff_adffs_a14_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2168)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a14_a,
	datad => Mux_a2168,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2169);

fir_out_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a9_a_areg0 = DFFE(fir_out_a9_a_a1927 # fir_out_a9_a_a1928 # Mux_a2169 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a9_a_a1927,
	datab => fir_out_a9_a_a1928,
	datac => Mux_a2169,
	datad => fir_out_a0_a_a1901,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a9_a_areg0);

Mux_a2170_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2170 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a14_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F4A4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a13_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => add2b_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2170);

Mux_a2171_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2171 = TimeConst_FF_adffs_a1_a & (Mux_a2170 & (add2b_ff_adffs_a16_a) # !Mux_a2170 & add2b_ff_adffs_a15_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2170)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F588",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a16_a,
	datad => Mux_a2170,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2171);

fir_out_a10_a_a1931_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a10_a_a1931 = add2b_ff_adffs_a17_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a10_a_a1931);

fir_out_a10_a_a1930_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a10_a_a1930 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a12_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a12_a,
	datac => add2b_ff_adffs_a11_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a10_a_a1930);

fir_out_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a10_a_areg0 = DFFE(fir_out_a10_a_a1931 # fir_out_a10_a_a1930 # Mux_a2171 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2171,
	datab => fir_out_a10_a_a1931,
	datac => fir_out_a10_a_a1930,
	datad => fir_out_a0_a_a1901,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a10_a_areg0);

Mux_a2172_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2172 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a15_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a14_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EE50",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a15_a,
	datac => add2b_ff_adffs_a14_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2172);

Mux_a2173_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2173 = TimeConst_FF_adffs_a1_a & (Mux_a2172 & (add2b_ff_adffs_a17_a) # !Mux_a2172 & add2b_ff_adffs_a16_a) # !TimeConst_FF_adffs_a1_a & (Mux_a2172)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a16_a,
	datab => add2b_ff_adffs_a17_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => Mux_a2172,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2173);

fir_out_a11_a_a1934_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a11_a_a1934 = add2b_ff_adffs_a18_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a11_a_a1934);

fir_out_a11_a_a1933_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a11_a_a1933 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a13_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a12_a,
	datac => add2b_ff_adffs_a13_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a11_a_a1933);

fir_out_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a11_a_areg0 = DFFE(fir_out_a11_a_a1934 # fir_out_a11_a_a1933 # Mux_a2173 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2173,
	datab => fir_out_a11_a_a1934,
	datac => fir_out_a11_a_a1933,
	datad => fir_out_a0_a_a1901,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a11_a_areg0);

fir_out_a12_a_a1936_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a12_a_a1936 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a14_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a14_a,
	datac => add2b_ff_adffs_a13_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a12_a_a1936);

fir_out_a12_a_a1937_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a12_a_a1937 = add2b_ff_adffs_a19_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a19_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a12_a_a1937);

Mux_a2174_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2174 = TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a16_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a16_a,
	datab => add2b_ff_adffs_a15_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2174);

Mux_a2175_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2175 = Mux_a2174 & (add2b_ff_adffs_a18_a # !TimeConst_FF_adffs_a1_a) # !Mux_a2174 & (TimeConst_FF_adffs_a1_a & add2b_ff_adffs_a17_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BC8C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datab => Mux_a2174,
	datac => TimeConst_FF_adffs_a1_a,
	datad => add2b_ff_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2175);

fir_out_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a12_a_areg0 = DFFE(fir_out_a12_a_a1936 # fir_out_a12_a_a1937 # Mux_a2175 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a12_a_a1936,
	datab => fir_out_a12_a_a1937,
	datac => Mux_a2175,
	datad => fir_out_a0_a_a1901,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a12_a_areg0);

fir_out_a13_a_a1940_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a13_a_a1940 = add2b_ff_adffs_a20_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a20_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a13_a_a1940);

Mux_a2176_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2176 = TimeConst_FF_adffs_a1_a & TimeConst_FF_adffs_a0_a # !TimeConst_FF_adffs_a1_a & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a17_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a16_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DC98",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a16_a,
	datad => add2b_ff_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2176);

Mux_a2177_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2177 = TimeConst_FF_adffs_a1_a & (Mux_a2176 & add2b_ff_adffs_a19_a # !Mux_a2176 & (add2b_ff_adffs_a18_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2176)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a1_a,
	datab => add2b_ff_adffs_a19_a,
	datac => add2b_ff_adffs_a18_a,
	datad => Mux_a2176,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2177);

fir_out_a13_a_a1939_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a13_a_a1939 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a15_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a14_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a14_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a13_a_a1939);

fir_out_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a13_a_areg0 = DFFE(fir_out_a13_a_a1940 # fir_out_a13_a_a1939 # Mux_a2177 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a13_a_a1940,
	datab => Mux_a2177,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a13_a_a1939,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a13_a_areg0);

Mux_a2178_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2178 = TimeConst_FF_adffs_a1_a & (add2b_ff_adffs_a19_a # TimeConst_FF_adffs_a0_a) # !TimeConst_FF_adffs_a1_a & add2b_ff_adffs_a17_a & (!TimeConst_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0CA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a17_a,
	datab => add2b_ff_adffs_a19_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => TimeConst_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2178);

Mux_a2179_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2179 = TimeConst_FF_adffs_a0_a & (Mux_a2178 & (add2b_ff_adffs_a20_a) # !Mux_a2178 & add2b_ff_adffs_a18_a) # !TimeConst_FF_adffs_a0_a & (Mux_a2178)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F588",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_FF_adffs_a0_a,
	datab => add2b_ff_adffs_a18_a,
	datac => add2b_ff_adffs_a20_a,
	datad => Mux_a2178,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2179);

fir_out_a14_a_a1943_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a14_a_a1943 = add2b_ff_adffs_a21_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a14_a_a1943);

fir_out_a14_a_a1942_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a14_a_a1942 = fir_out_a0_a_a1898 & (TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a16_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CA00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a15_a,
	datab => add2b_ff_adffs_a16_a,
	datac => TimeConst_FF_adffs_a0_a,
	datad => fir_out_a0_a_a1898,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a14_a_a1942);

fir_out_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a14_a_areg0 = DFFE(fir_out_a14_a_a1943 # fir_out_a14_a_a1942 # Mux_a2179 & fir_out_a0_a_a1901, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Mux_a2179,
	datab => fir_out_a14_a_a1943,
	datac => fir_out_a0_a_a1901,
	datad => fir_out_a14_a_a1942,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a14_a_areg0);

Mux_a2180_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2180 = TimeConst_FF_adffs_a0_a & (TimeConst_FF_adffs_a1_a # add2b_ff_adffs_a19_a) # !TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a18_a & !TimeConst_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CEC2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a18_a,
	datab => TimeConst_FF_adffs_a0_a,
	datac => TimeConst_FF_adffs_a1_a,
	datad => add2b_ff_adffs_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2180);

Mux_a2181_I : apex20ke_lcell
-- Equation(s):
-- Mux_a2181 = TimeConst_FF_adffs_a1_a & (Mux_a2180 & add2b_ff_adffs_a21_a # !Mux_a2180 & (add2b_ff_adffs_a20_a)) # !TimeConst_FF_adffs_a1_a & (Mux_a2180)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BBC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a21_a,
	datab => TimeConst_FF_adffs_a1_a,
	datac => add2b_ff_adffs_a20_a,
	datad => Mux_a2180,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Mux_a2181);

fir_out_a15_a_a1954_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a15_a_a1954 = (TimeConst_FF_adffs_a0_a & add2b_ff_adffs_a17_a # !TimeConst_FF_adffs_a0_a & (add2b_ff_adffs_a16_a)) & CASCADE(fir_out_a0_a_a1955)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_FF_adffs_a0_a,
	datac => add2b_ff_adffs_a17_a,
	datad => add2b_ff_adffs_a16_a,
	cascin => fir_out_a0_a_a1955,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a15_a_a1954);

fir_out_a15_a_a1946_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a15_a_a1946 = add2b_ff_adffs_a22_a & (TimeConst_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add2b_ff_adffs_a22_a,
	datac => TimeConst_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_a15_a_a1946);

fir_out_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- fir_out_a15_a_areg0 = DFFE(fir_out_a15_a_a1954 # fir_out_a15_a_a1946 # fir_out_a0_a_a1901 & Mux_a2181, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a0_a_a1901,
	datab => Mux_a2181,
	datac => fir_out_a15_a_a1954,
	datad => fir_out_a15_a_a1946,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_a15_a_areg0);

Level_Cmp_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => level_cmp_Del_vec_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_Cmp);

Level_Out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Level_Out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Level_Out);

fir_out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(0));

fir_out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(1));

fir_out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(2));

fir_out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(3));

fir_out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(4));

fir_out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(5));

fir_out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(6));

fir_out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(7));

fir_out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(8));

fir_out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(9));

fir_out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(10));

fir_out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(11));

fir_out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(12));

fir_out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(13));

fir_out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(14));

fir_out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => fir_out_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(15));
END structure;


