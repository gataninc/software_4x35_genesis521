-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_edisc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the Medium and Low Speed FIR data 
--		It includes the Delay Lines with the appropriate address
--		counters.
--	
--		It also generates the "Threshold Level" comparison for the
--		input to the Pulse Steering Logic
--
--		fir = ( adata - 2*adata_del1 ) + adata_del2
--
--		High Speed 	= 0.5us
--		Medium Speed 	= 2.0us
--		Low Speed 	= 8.0us
--
--	History
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;


architecture test_bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal AD_MR		: std_logic := '1';
	signal PA_Reset	: std_logic;
	signal Disc_En		: std_logic;
	signal TimeConst	: std_logic_Vector(  3 downto 0 );
	signal tlevel		: std_logic_vector( 15 downto 0 );
	signal adata		: std_logic_vector( 15 downto 0 ) := x"0000";
	signal dly_data	: std_logic_vector( 44 downto 0 );
	signal Level_Cmp	: std_logic;
	signal Level_Out	: std_logic;					-- Discriminator Level Exceeded
	signal fir_out		: std_logic_Vector( 15 downto 0 );	-- FIR Output ( for Debug )
	-------------------------------------------------------------------------------
	component fir_dmem is
		port ( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			adata		: in		std_logic_Vector( 14 downto 0 );
			Disc_Mem		: out 	std_logic_Vector( 44 downto 0 ) );
	end component fir_dmem;

	-------------------------------------------------------------------------------
	component fir_edisc is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			AD_MR		: in		std_logic;					-- Clear's FIR
			PA_Reset		: in		std_logic;
			Disc_En		: in		std_logic;
			TimeConst		: in		std_logic_Vector(  3 downto 0 );
			data_abc		: in		std_logic_Vector( 44 downto 0 );	-- First TAP
			tlevel		: in		std_logic_vector( 15 downto 0 );
			Level_Cmp		: out	std_logic;
			Level_Out		: out	std_logic;					-- Discriminator Level Exceeded
			fir_out		: out	std_logic_Vector( 15 downto 0 ));	-- FIR Output ( for Debug )
	end component fir_edisc;
	-------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk		<= not( clk ) after 25 ns;
	ad_mr	<= '0' after 2 us;
	PA_Reset	<= '0';
	Disc_En	<= '1';
	TimeConst	<= x"0";
	tlevel	<= x"0010";
	adata	<= x"0010" after 5 us;

	UFD : fir_dmem
		port map(
			imr		=> imr,
			clk		=> clk,
			adata	=> adata( 14 downto 0 ),
			disc_mem	=> dly_data );
	
	
	U : fir_edisc
		port map(
			imr			=> imr,
			clk			=> clk,
			ad_mr		=> ad_mr,
			PA_Reset		=> PA_Reset,
			Disc_en		=> disc_En,
			TimeConst		=> TimeConst,
			data_abc		=> dly_data,
			tlevel		=> tlevel,
			Level_Cmp		=> level_cmp,
			Level_Out		=> level_out,
			fir_out		=> fir_out );

------------------------------------------------------------------------------------
end test_bench;
------------------------------------------------------------------------------------

