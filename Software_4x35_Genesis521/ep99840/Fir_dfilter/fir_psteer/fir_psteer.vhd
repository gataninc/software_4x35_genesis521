-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_psteer.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
-- 		Super High	100 ns Peak Time
-- 		High 		400 ns Peak TIme
--		Medium 		  1 us PEak Time
--		Low 	  		  8 us Peak Time

--		This module accepts as input each of the Disc Edge Detects
--		and Generates the blocking pulses
--			Index	Discriminator	Block WIdth 	Block Len
--			  0		Super High    	N/A
--			  1		High			  800ns
--			  2	 	Med			  2 us
--			  3	 	Low		 	  16us
--			  4	 	Energy	  	 0.8 to 204.8us
--			
--	It also generates the FDISC ( First Discriminator Output Pulse )
--		It forms the Logical AND of the Discriminator Edge Detect and
--		the Blocking Pulse to generate a single-wide Fast Discriminator Pulse
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_psteer is 
	port( 
		clk            : in      std_logic;			     	-- Master Clock Input
	     imr            : in      std_logic;                    	-- Master Reset
		PA_Reset		: in		std_logic;					-- PreAmp Reset
		OTR			: in		std_logic;
		RTD_PUR		: in		std_logic;
	     Disc_In        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
	     Disc_En        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
		Disc_Cmp		: in		std_logic_Vector(  4 downto 0 );
		Offset_Time	: in		std_logic_vector(  3 downto 0 );	-- Offset TIme
		Offset_Inc	: in		std_logic_vector( 11 downto 0 );
		Peak_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
		Pur_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
		Peak_Done		: buffer	std_logic;
		BLOCK_OUT		: buffer	std_logic_Vector(  5 downto 1 );	-- BLocking Pulses
		FDISC		: buffer	std_Logic;					-- Fast Disc
		Pur			: buffer	std_logic;					-- Pile-Up Reject
		PBusy		: buffer	std_logic;
		PA_Busy		: buffer	std_logic;
		Busy_Str		: buffer	std_logic_Vector( 3 downto 0 ));
end fir_psteer;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_psteer is
	constant iSHigh_to 			: integer := 0;
	constant iHigh_to			: integer := 1;		
	constant iMed_to 			: integer := 2;
	constant iLow_to 			: integer := 3;
	constant iEnergy_To 		: integer := 4;
	constant iReject_to 		: integer := 5;

	constant BLK_LEN1			: integer :=     25;	-- High Speed			=400ns @10Mhz 
	constant BLK_LEN2			: integer :=     33; 	-- Medium Speed		=800ns @ 10Mhz  
	constant BLK_LEN3			: integer :=    143;	-- Low Speed			=6.4us @10Mhz
	
	constant DE_Max			: integer := 4;

	type St_type is (
		ST_IDLE,				-- Idle
		ST_REJECT0,
		ST_REJECT1,
		ST_REJECT2,
		ST_BUSY1,
		ST_BUSY2, 
		ST_RESET0,
		ST_RESET1,
		ST_RESET2 );
		
	signal Busy_State			: ST_Type;
	
	signal BLK_CNT1			: integer range 0 to BLK_LEN1;
	signal BLK_CNT2			: integer range 0 to BLK_LEN2;
	signal BLK_CNT3			: integer range 0 to BLK_LEN3;
	signal BLK_CNT4			: std_logic_vector( 11 downto 0 );
	signal BLK_CNT4_Max			: std_logic_vector( 11 downto 0 );
	signal BLK_ST				: std_logic_Vector( 5 downto 1 );
	signal BLK_TC				: std_logic_Vector( 4 downto 1 );

	signal Peak_time_d2			: std_logic_vector( 11 downto 0 );
	signal Peak_time_d4			: std_logic_vector( 11 downto 0 );
	signal Peak_time_d8			: std_logic_vector( 11 downto 0 );
	signal Peak_time_D2_To		: std_logic_vector( 11 downto 0 );
	signal peak_time_x2			: std_logic_vector( 11 downto 0 );
	signal ifdisc				: std_logic;
	signal CNT_LD				: std_logic;
	signal offset_to			: std_logic_vector( 11 downto 0 );
	signal offset_to_vec		: std_logic_vector( 11 downto 0 );
	signal disc_start			: std_logic_Vector( 4 downto 0 );
	signal iFDisc_Del			: std_logic;
	signal Disc_Cmp_E_Vec		: std_logic_vector( DE_Max downto 0 );
	signal Offset_Inc_Reg		: std_logic_vector( 11 downto 0 );
	signal offset_time_reg		: std_Logic_Vector(  3 downto 0 );
	signal peak_time_reg		: std_logic_Vector( 11 downto 0 );
	
	-- Ver 355B Signal Declarations
	signal Disc_in3_dis			: std_logic;
	signal disc_in3_tc			: std_logic;
	signal disc_in3_cnt			: std_logic_Vector( 7 downto 0 );

	signal Disc_in4_dis			: std_logic;
	signal disc_in4_tc			: std_logic;
	signal disc_in4_cnt			: std_logic_Vector( 11 downto 0 );
	signal pur_time_reg			: std_logic_Vector( 11 downto 0 );
	
	
	-- Ver 355B Signal Declarations
begin
	with Busy_State select
		Busy_Str <= "0000" when ST_IDLE,
				  "0001" when ST_REJECT0,
				  "0010" when ST_REJECT1,
				  "0011" when ST_REJECT2,
				  "0100" when ST_BUSY1,
				  "0101" when ST_BUSY2,
				  "0110" when ST_RESET0,
				  "0111" when ST_RESET1,
				  "1000" when ST_RESET2,
				  "1111" when others;

	pur_time_reg_ff : lpm_ff
		generic map(
			lpm_width	=> 12 )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> pur_time,
			q		=> pur_time_reg );

	offset_inc_reg_ff : lpm_ff
		generic map( 
			LPM_WIDTH	=> 12,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> offset_inc,
			q		=> offset_inc_reg );
			
	offset_time_reg_ff : lpm_ff
		generic map( 
			LPM_WIDTH	=> 4,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> Offset_Time,
			q		=> Offset_time_Reg );
			
	peak_time_reg_ff : lpm_ff
		generic map( 
			LPM_WIDTH	=> 12,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> peak_time,
			q		=> peak_time_Reg );

	------------------------------------------------------------------------------
	-- Pile-Up Reject Logic -------------------------------------------------------
	-------------------------------------------------------------------------------
	Peak_time_D2		<= '0'   & Peak_time_reg( 11 downto 1 );
	Peak_time_D4		<= "00"  & Peak_time_reg( 11 downto 2 );
	Peak_time_D8		<= "000" & Peak_time_reg( 11 downto 3 );
	Peak_time_D2_To	<= Peak_time_D2 + Peak_Time_D8;
	Peak_time_x2		<= Peak_time_reg( 10 downto 0 ) & "0";
	
--	Peak_time_D2_To	<= Peak_time_D2 + ( "00" & Peak_time_D2( 11 downto 2 ) );
	offset_to_vec		<= x"00" & offset_time_Reg;
	
	offset_to_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 12,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',				-- Cary In ( Always +1 )
			dataa			=> offset_to_vec,
			datab			=> offset_inc_reg,
			result			=> offset_to );
			
--	Offset_To			<= offset_to_vec + x"003";
--	Offset_To			<= offset_to_vec + x"001";
--	Offset_To			<= ( x"00" & Offset_time )  + x"001";

	-- Ver 355B Disc_In4_Dis terminal count
	disc_in4_tc <= '1' when ( disc_in4_cnt = peak_time_d4 ) else '0';
	disc_in3_tc <= '1' when ( disc_in3_cnt = conv_std_logic_vector( 96, 8 )) else '0';
	

	with Busy_State select
		BLK_CNT4_Max	<= Peak_time_D2_to		when ST_REject0, -- Long incase no BLM Disc (was _to)
					   Peak_Time_D2   		when ST_Reject1,
					   offset_to			when ST_REJECT2,
					   pur_time_reg		when ST_BUSY1,
					   Peak_Time_reg		when ST_BUSY2,
					   peak_time_x2		when ST_REset0,
					   peak_time_x2		when st_reset1,
					   peak_time_reg   		when st_reset2,
			             Peak_time_D2  		when others;

	------------------------------------------------------------------------
	-- Block Starting Pulse based on the Discriminator Edge Detects ---------
	BLK_ST(iHigh_To)	<= '1' when ( Disc_Start( iSHigh_To	) = '1' ) else '0';

	BLK_ST(iMed_to)	<= '1' when (( Disc_Start( iSHigh_To	) = '1' ) 
							or ( Disc_Start( iHigh_to	) = '1' )) else '0';


	BLK_ST(iLow_To)	<= '1' when (( Disc_Start( iSHigh_to	) = '1' ) 
							or ( Disc_Start( iHigh_to	) = '1' ) 
							or ( Disc_Start( iMed_to 	) = '1' )) else '0';
							
	BLK_ST(iEnergy_to)	<= '1' when (( Disc_Start( iSHigh_to	) = '1' ) 
							or ( Disc_Start( iHigh_to	) = '1' ) 
							or ( Disc_Start( iMed_to 	) = '1' )
							or ( Disc_start( iLow_to 	) = '1' )) else '0';

	BLK_TC(iHigh_to)		<= '1' when ( BLK_CNT1 		= BLK_LEN1 	) else '0';
	BLK_TC(iMed_to)		<= '1' when ( BLK_CNT2 		= BLK_LEN2 	) else '0';		
	BLK_TC(iLow_to)		<= '1' when ( BLK_CNT3 		= BLK_LEN3 	) else '0';	
	BLK_TC(iEnergy_to)		<= '1' when ( BLK_CNT4		= BLK_CNT4_Max	) else '0';
	
	Cnt_LD <= '1' when (( BUsy_State = ST_IDLE    ) and ( Disc_In(iEnergy_To) 	= '1' )) else
			'1' when (( BUsy_State = ST_IDLE 	 ) and ( iFDISC 			= '1' ) and ( IFDisc_Del = '0' )) else
			'1' when (( BUsy_State = ST_Reject0 ) and ( iFDisc 			= '1' ) and ( IFDisc_Del = '0' )) else
			'1' when (( BUsy_State = ST_Reject0 ) and ( Disc_In(iEnergy_To) 	= '1' )) else
			'1' when (( Busy_State = ST_Reject0 ) and ( BLK_TC(iEnergy_To) 	= '1' )) else

			'1' when (( BUsy_State = ST_Reject1 ) and ( iFDisc 			= '1' )  and ( IFDisc_Del = '0' )) else
			'1' when (( Busy_State = ST_Reject1 ) and ( BLK_TC(iEnergy_To) 	= '1' )) else
			
			'1' when (( BUsy_State = ST_Reject2 ) and ( iFDisc 			= '1' ) and ( IFDisc_Del = '0' )) else
			'1' when (( Busy_State = ST_Reject2 ) and ( BLK_TC(iEnergy_To) 	= '1' )) else

			'1' when (( BUsy_State = ST_BUSY1   ) and ( iFDisc 			= '1' )  and ( IFDisc_Del = '0' )) else
			'1' when (( Busy_State = ST_BUSY1 	 ) and ( BLK_TC(iEnergy_To) 	= '1' )) else

			'1' when (( BUsy_State = ST_BUSY2   ) and ( iFDisc 			= '1' )  and ( IFDisc_Del = '0' )) else
			'1' when (( Busy_State = ST_BUSY2 	 ) and ( BLK_TC(iEnergy_To) 	= '1' )) else

			'1' when (( Busy_State = ST_RESET0  ) and ( PA_Reset 			= '1' )) else
			'1' when (( Busy_State = ST_RESET1  ) and ( BLK_TC(iEnergy_To)   = '1' )) else
			
--			'1' when (( Busy_state = ST_IDLE 	 ) and ( SDD_PUR = '1' )) else
--			'1' when (( Busy_state = ST_REJECT0 ) and ( SDD_PUR = '1' )) else
--			'1' when (( Busy_state = ST_REJECT1 ) and ( SDD_PUR = '1' )) else
--			'1' when (( Busy_state = ST_REJECT2 ) and ( SDD_PUR = '1' )) else
--			'1' when (( Busy_state = ST_BUSY1	 ) and ( SDD_PUR = '1' )) else
--			'1' when (( Busy_state = ST_BUSY2	 ) and ( SDD_PUR = '1' )) else			
			'0';

	PBusy		<= '0' when ( Busy_State = ST_IDLE 	) else '1';


	BLK_CNT4_CNTR : LPM_Counter
		generic map(
			LPM_WIDTH		=> 12,
			LPM_TYPE		=> "LPM_COUNTER",
			LPM_DIRECTION	=> "UP" )
		port map(
			aclr			=> imr,
			clock		=> clk,
			sclr			=> Cnt_Ld,
			Cnt_en		=> PBusy,
			q			=> blk_cnt4 );
	------------------------------------------------------------------------------
						
	with Disc_en select
		Disc_Start( iSHigh_to)	<= Disc_in( iShigh_To) 						when "00001",
							   Disc_in( iSHigh_To) and Disc_Cmp( iEnergy_to)  when "10011",	-- 1.6 us
							   Disc_in( iSHigh_to) and Disc_Cmp( iMed_to)	when "10111",	-- 3.2, 6.4 and 12.8
							   Disc_in( iSHigh_to) and Disc_Cmp( iMed_to)	when "11111",	-- 25.6, 51.2, 102.4
							   Disc_in( iShigh_to)						when others;							

	with Disc_En select
		Disc_Start( iHigh_to ) 	<= Disc_in( iHigh_to ) 												 when "00010",
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iEnergy_To) and Disc_Cmp_E_Vec(DE_Max) when "10010", 			-- 1.6us 
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iEnergy_To) and Disc_Cmp_E_Vec(DE_Max) when "10011", 			-- 1.6us 
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iMed_to   ) when "10110", 			-- 3.2, 6.4 and 12.8
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iMed_to   ) when "10111", 			-- 3.2, 6.4 and 12.8
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iMed_to   ) when "11110", 			-- 25.6, 51.2, 102.4
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to )) and disc_Cmp( iMed_to   ) when "11111", 			-- 25.6, 51.2, 102.4
						   	   Disc_in( iHigh_to ) and not( Block_Out( iHigh_to ))						 when others;

	Disc_Start( iMed_to)	<= '1' when (( Disc_in( iMed_to )  = '1' ) and ( Disc_en = "00100" )) else
						   '1' when (( Disc_in( iMed_to )  = '1' ) and ( Block_Out( iMed_to ) = '0' ) and ( Disc_en( iLow_to ) = '0' )) else
						   '1' when (( Disc_in( iMed_to )  = '1' ) and ( Block_Out( iMed_to ) = '0' ) and ( Disc_en( iLow_to ) = '1' ) and ( Disc_Cmp( iLow_to ) = '1' )) else
						   '0';
						
						
	-- Ver 355B added "Disc_In3_Dis = '0'" to Disc_Start( iLow_To) 
	Disc_Start( iLow_To ) 	<= '1' when (( Disc_in( iLow_to) 	= '1' ) and ( Disc_In3_Dis = '0' ) and ( Disc_En = "01000" )) else
						   '1' when (( Disc_in( iLow_to) 	= '1' ) and ( Disc_In3_Dis = '0' ) and ( Block_Out( iLow_to ) = '0' )) else
						   '0';
	
	-- Ver 355B added "Disc_In4_Dis = '0'" to Disc_Start( iEnergy_To) 
	Disc_Start( iEnergy_to ) <= '1' when (( Disc_in(iEnergy_to) = '1' ) and ( Disc_in4_dis = '0' ) and ( Disc_en = "10000" )) else 
						   '1' when (( Disc_in(iEnergy_to) = '1' ) and ( Disc_in4_dis = '0' ) and ( Block_Out(iEnergy_to) = '0' )) else
						   '0';
	
						
	------------------------------------------------------------------------------
	-- Discriminator Fast Discriminator Output ------------------------------------
	ifdisc	<= '1' when (( Disc_Start( iSHigh_to	) = '1' ) 
					or ( Disc_Start( iHigh_to	) = '1' )
					or ( Disc_Start( iMed_to		) = '1' )
					or ( Disc_Start( iLow_to 	) = '1' ) 
					or ( Disc_Start( iEnergy_to 	) = '1' )) else '0';
		
	-- Discriminator Fast Discriminator Output ------------------------------------
	-------------------------------------------------------------------------------

	------------------------------------------------------------------------------
     clock_proc : process( clk, imr )
     begin
		if( imr = '1' ) then
			disc_in3_dis	<= '0';
			disc_in3_cnt	<= x"00";
			
			disc_in4_dis	<= '0';
			disc_in4_cnt	<= x"000";
			
			Peak_Done		<= '0';
			
			for i in 1 to 5 loop
				Block_Out(i) <= '0';
			end loop;
			BLK_CNT1		<= 0;			-- High
			BLK_CNT2		<= 0;			-- Med
			BLK_CNT3		<= 0;			-- Low
			FDisc		<= '0';
			Pur			<= '0';
			Busy_State	<= ST_Idle;
			iFDisc_Del	<= '0';
			PA_Busy		<= '0';
			
			for i in 0 to DE_Max loop
				Disc_Cmp_E_Vec(i) <= '0';
			end loop;
		elsif(( clk'event ) and ( clk = '1' )) then
		
			-----------------------------------------------------------------------------
			-- Ver 355B to ignore second pulses on BLM output which are noise riding on 
			-- signal Restrict this to Amp Times < 25.6us
			if( disc_in(3) = '1' )
				then disc_in3_dis <= '1';
			elsif( disc_in3_tc = '1' )
				then disc_in3_dis <= '0';
			end if;
			
			if( disc_in(3) = '1' )
				then disc_in3_cnt <= x"00";
			elsif( disc_in3_dis = '1' )
				then disc_in3_cnt <= disc_in3_cnt + 1;
			end if;
			
			if( disc_in(4) = '1' )	
				then disc_in4_dis <= '1';
			elsif( disc_in4_tc = '1' )
				then disc_in4_dis <= '0';
			end if;
			
			if( disc_in(4) = '1' )						
				then disc_in4_cnt <= x"000";
			elsif( disc_in4_dis = '1' )
				then disc_in4_cnt <= disc_in4_cnt + 1;
			end if;
			-----------------------------------------------------------------------------
		
		
			Disc_Cmp_E_Vec	<= Disc_Cmp_E_Vec( DE_Max-1 downto 0 ) & Disc_Cmp( iEnergy_to);
	
			------------------------------------------------------------------------
			-- Block Pulse Start/Stop ----------------------------------------------
			for i in 1 to 3 loop
				if(( PA_Reset = '1' ) or ( BLK_ST(i) = '1' ))
					then Block_Out(i) <= '1';
				elsif( BLK_TC(i) = '1' )
					then Block_Out(i) <= '0';
				elsif( Busy_State = ST_Reject0 ) and ( BLK_TC(iEnergy_To) = '1' )			
					then Block_Out(i) <= '0';
				end if;
			end loop;
			
			if(( PA_Reset = '1' ) or ( BLK_ST(iEnergy_To) = '1' ))
				then Block_Out(iEnergy_To) <= '1';
			elsif(( Busy_State = ST_BUSY2 ) and ( BLK_TC(iEnergy_To) = '1' ))
				then Block_Out(iEnergy_To) <= '0';
			elsif( Busy_State = ST_Reject0 ) and ( BLK_TC(iEnergy_To) = '1' )			
				then Block_Out(iEnergy_to) <= '0';
			end if;

			if(( PA_Reset = '1' ) or ( BLK_ST(4) = '1' ))
				then Block_Out(iReject_to) <= '1';
			elsif(( Busy_State = ST_Reject2 ) and ( BLK_TC(iEnergy_To) = '1' ))
				then Block_Out(iReject_to) <= '0';
			elsif( Busy_State = ST_Reject0 ) and ( BLK_TC(iEnergy_To) = '1' )			
				then Block_Out(iReject_to) <= '0';
			end if;
			-- Block Pulse Start/Stop ----------------------------------------------
			------------------------------------------------------------------------
			
			------------------------------------------------------------------------
			-- Blocking Pulse Counters ---------------------------------------------
			if(( PA_Reset = '1' ) or ( Block_Out(iHigh_to) = '0' ) or ( BLK_ST(iHigh_to) = '1' ))
				then BLK_CNT1 <= 0;
			elsif( Block_Out(iHigh_to) = '1' )
				then BLK_CNT1 <= BLK_CNT1 + 1; 
			end if;
			
			if(( PA_Reset = '1' ) or ( Block_Out(iMed_to) = '0' ) or ( BLK_ST(iMed_to) = '1' ))
				then BLK_CNT2 <= 0; 
			elsif( Block_Out(iMed_to) = '1' )
				then BLK_CNT2 <= BLK_CNT2 + 1; 
			end if;
			
			if(( PA_Reset = '1' ) or ( Block_Out(iLow_to) = '0' ) or ( BLK_ST(iLow_to) = '1' ))
				then BLK_CNT3 <= 0;
			elsif( Block_Out(iLow_to) = '1' )
				then BLK_CNT3 <= BLK_CNT3 + 1; 
			end if;
			-- Blocking Pulse Counters ---------------------------------------------
			------------------------------------------------------------------------
			
			------------------------------------------------------------------------
			iFDisc_Del <= iFDisc;
			-- FDISC Generation
			if(( ifdisc = '1' ) and ( iFDisc_Del = '0' ))	-- Ver 354C
				then FDisc <= '1';
				else FDisc <= '0';
			end if;
			-- FDISC Generation
			------------------------------------------------------------------------

			------------------------------------------------------------------------
			-- Peak Done Generation ( for NPS Counting )
			if(( BLK_TC( iEnergy_To ) = '1' ) and ( Busy_State = ST_Reject2 ) and ( Pur = '0' )) 
				then Peak_Done <= '1';
				else Peak_Done <= '0';
			end if;
			-- Peak Done Generation
			------------------------------------------------------------------------

			------------------------------------------------------------------------
			-- Pile-Up Reject Signal -----------------------------------------------
			-- Assert with Reset			
			if( PA_Reset = '1' )				
				then PUR <= '1';
			elsif( RTD_PUR = '1' )			-- 4x17
				then PUR <= '1';			
			elsif( otr = '1' )
				then PUR 	<= '1';
			-- Assert w/ FDISC and in Reject State
			elsif(( Busy_State = ST_REJECT0 ) and ( iFDisc = '1' )  and ( IFDisc_Del = '0' ))
				then PUR <= '1';
			elsif(( Busy_State = ST_REJECT1 ) and ( iFDisc = '1' )  and ( IFDisc_Del = '0' ))
				then PUR <= '1';
			elsif(( Busy_State = ST_REJECT2 ) and ( iFDisc = '1' )  and ( IFDisc_Del = '0' ))
				then PUR <= '1';

			elsif(( Busy_State = ST_BUSY1 ) and ( BLK_TC(iEnergy_to) = '1' ))
				then PUR	<= '0';

			-- Clear when back to Idle
			elsif( Busy_State = ST_IDLE )
				then PUR <= '0';
			end if;
			-- Pile-Up Reject Signal ----------------------------------------------
			------------------------------------------------------------------------
			
			------------------------------------------------------------------------
			if( PA_Reset = '1' )
				then PA_Busy <= '1';
			elsif( Busy_State = ST_IDLE )
				then PA_Busy <= '0';
			end if;
			------------------------------------------------------------------------

			------------------------------------------------------------------------
			-- State Machine
			case Busy_State is
				when ST_IDLE =>					-- Code 0
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif( Disc_In(iEnergy_To) = '1' ) 				then	Busy_State 	<= ST_REJECT1;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif(( iFDisc   = '1' ) and ( IFDisc_Del = '0' ))	then Busy_State 	<= ST_Reject0;
					elsif( otr = '1' )								then Busy_State	<= ST_Reject1;
					end if;

				-- from Any Discriminator INput to Energy Discriminator					
				when ST_REJECT0 =>					-- Code 1
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif( Disc_In(iEnergy_To) = '1' ) 				then	Busy_State 	<= ST_REJECT1;
					elsif(( iFDisc   = '1' ) and ( IFDisc_Del = '0' )) 	then Busy_State 	<= ST_Reject0;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif( BLK_TC(iEnergy_to)  = '1' ) 				then BUsy_State	<= ST_IDLE;
					end if;

				-- from Energy Discriminator to End of Peak Time (1/2 Peak Time )
				when ST_REJECT1 => 					-- Code 2
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif(( iFDisc   = '1' ) and ( IFDisc_Del = '0' )) 	then Busy_State 	<= ST_Reject0;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif( BLK_TC(iEnergy_to) = '1' ) 					then Busy_State 	<= ST_REJECT2;
					end if;

				-- Throughput Factor Delay
				when ST_REJECT2 =>					-- Code 3, End of this state creates Peak Done
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif(( iFDisc   = '1' ) and ( IFDisc_Del = '0' )) 	then Busy_State 	<= ST_Reject0;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif( BLK_TC(iEnergy_to) = '1' ) 					then Busy_State 	<= ST_BUSY1;
					end if;

				-- Finish out the processing time ( PBUSY ) for Baseline Histogram
				when ST_BUSY1	=> 					-- Code 4
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif(( iFDisc   = '1' ) and ( IFDisc_Del = '0' )) 	then Busy_State 	<= ST_Reject0;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif( BLK_TC(iEnergy_to) = '1' ) 					then Busy_State 	<= ST_BUSY2;
					end if;

				when ST_BUSY2	=> 					-- Code 5
					if( PA_Reset = '1' )							then Busy_State	<= ST_RESET0;
					elsif(( iFDisc  = '1' ) and ( IFDisc_Del = '0' )) 	then Busy_State 	<= ST_Reject0;
					elsif( RTD_PUR = '1' ) 							then Busy_State	<= ST_REJECT0;
					elsif( BLK_TC(iEnergy_to) = '1' ) 					then Busy_State 	<= ST_IDLE;
					end if;

				when ST_RESET0 =>
					if( PA_Reset = '0' )							then Busy_State	<= ST_RESET1;
					end if;
					
				when ST_RESET1 => 
					if( BLK_TC(iEnergy_to) = '1' )					then BUSY_STATE	<= ST_RESET2;
					end if;	

				when ST_RESET2 => 
					if( BLK_TC(iEnergy_to) = '1' )					then BUSY_STATE	<= ST_IDLE;
					end if;	
				
				when others	=> BUsy_State <= ST_IDLE;
			end case;
			-- State Machine
			------------------------------------------------------------------------
		end if;
	end process;
	----------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
end behavioral;               -- fir_psteer
---------------------------------------------------------------------------------------