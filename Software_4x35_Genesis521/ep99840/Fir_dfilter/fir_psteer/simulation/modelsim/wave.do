onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -height 25 -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -height 25 -radix hexadecimal /tb/clk
add wave -noupdate -format Literal -height 25 -radix hexadecimal /tb/offset_time
add wave -noupdate -format Literal -height 25 -radix hexadecimal -expand /tb/disc_in
add wave -noupdate -format Literal -height 25 -radix hexadecimal -expand /tb/block_out
add wave -noupdate -format Logic -height 25 -radix hexadecimal /tb/peak_done
add wave -noupdate -format Logic -height 25 -radix hexadecimal /tb/fdisc
add wave -noupdate -format Logic -height 25 -radix hexadecimal /tb/pur
add wave -noupdate -format Literal -height 25 -radix hexadecimal /tb/busy_str
add wave -noupdate -format Logic -height 25 -radix binary /tb/pbusy
add wave -noupdate -format Logic -height 25 -radix binary /tb/qual
add wave -noupdate -format Logic -height 25 -radix binary /tb/qual_abort
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {562541 ns} {28825 ns} {44456 ns}
WaveRestoreZoom {0 ns} {630 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
