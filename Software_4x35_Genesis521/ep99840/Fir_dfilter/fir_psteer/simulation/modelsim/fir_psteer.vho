-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "06/01/2006 14:54:15"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_psteer IS
    PORT (
	clk : IN std_logic;
	imr : IN std_logic;
	PA_Reset : IN std_logic;
	OTR : IN std_logic;
	RTD_PUR : IN std_logic;
	Disc_In : IN std_logic_vector(4 DOWNTO 0);
	Disc_En : IN std_logic_vector(4 DOWNTO 0);
	Disc_Cmp : IN std_logic_vector(4 DOWNTO 0);
	Offset_Time : IN std_logic_vector(3 DOWNTO 0);
	Offset_Inc : IN std_logic_vector(11 DOWNTO 0);
	Peak_Time : IN std_logic_vector(11 DOWNTO 0);
	Pur_Time : IN std_logic_vector(11 DOWNTO 0);
	Peak_Done : OUT std_logic;
	BLOCK_OUT : OUT std_logic_vector(5 DOWNTO 1);
	FDISC : OUT std_logic;
	Pur : OUT std_logic;
	PBusy : OUT std_logic;
	PA_Busy : OUT std_logic;
	Busy_Str : OUT std_logic_vector(3 DOWNTO 0)
	);
END fir_psteer;

ARCHITECTURE structure OF fir_psteer IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_OTR : std_logic;
SIGNAL ww_RTD_PUR : std_logic;
SIGNAL ww_Disc_In : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_Disc_En : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_Disc_Cmp : std_logic_vector(4 DOWNTO 0);
SIGNAL ww_Offset_Time : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Offset_Inc : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Peak_Time : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Pur_Time : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Peak_Done : std_logic;
SIGNAL ww_BLOCK_OUT : std_logic_vector(5 DOWNTO 1);
SIGNAL ww_FDISC : std_logic;
SIGNAL ww_Pur : std_logic;
SIGNAL ww_PBusy : std_logic;
SIGNAL ww_PA_Busy : std_logic;
SIGNAL ww_Busy_Str : std_logic_vector(3 DOWNTO 0);
SIGNAL BLK_CNT1_a4_a : std_logic;
SIGNAL BLK_CNT1_a3_a_a49 : std_logic;
SIGNAL BLK_CNT1_a1_a : std_logic;
SIGNAL BLK_CNT2_a4_a_a55 : std_logic;
SIGNAL BLK_CNT2_a5_a : std_logic;
SIGNAL BLK_ST_a408 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1796 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1797 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1798 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1799 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1800 : std_logic;
SIGNAL BLK_CNT4_Max_a5_a_a1801 : std_logic;
SIGNAL pur_time_reg_ff_adffs_a11_a : std_logic;
SIGNAL BLK_CNT4_Max_a11_a_a1802 : std_logic;
SIGNAL BLK_CNT4_Max_a0_a_a1807 : std_logic;
SIGNAL BLK_CNT4_Max_a9_a_a1809 : std_logic;
SIGNAL Pur_a68 : std_logic;
SIGNAL Pur_a70 : std_logic;
SIGNAL Select_a1052 : std_logic;
SIGNAL Select_a1053 : std_logic;
SIGNAL Select_a1054 : std_logic;
SIGNAL Select_a1061 : std_logic;
SIGNAL add_a805 : std_logic;
SIGNAL add_a809 : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a0_a : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a9_a : std_logic;
SIGNAL clock_proc_a739 : std_logic;
SIGNAL clock_proc_a743 : std_logic;
SIGNAL add_a825 : std_logic;
SIGNAL clock_proc_a819 : std_logic;
SIGNAL clock_proc_a745 : std_logic;
SIGNAL add_a829 : std_logic;
SIGNAL clock_proc_a795 : std_logic;
SIGNAL clock_proc_a747 : std_logic;
SIGNAL BLK_CNT3_a3_a_a73 : std_logic;
SIGNAL BLK_CNT3_a3_a : std_logic;
SIGNAL BLK_CNT3_a2_a_a76 : std_logic;
SIGNAL BLK_CNT3_a2_a : std_logic;
SIGNAL BLK_CNT3_a1_a_a79 : std_logic;
SIGNAL BLK_CNT3_a1_a : std_logic;
SIGNAL BLK_CNT3_a0_a_a82 : std_logic;
SIGNAL BLK_CNT3_a0_a : std_logic;
SIGNAL Equal_a839 : std_logic;
SIGNAL Equal_a799 : std_logic;
SIGNAL BLK_CNT3_a7_a : std_logic;
SIGNAL BLK_CNT3_a6_a_a88 : std_logic;
SIGNAL BLK_CNT3_a6_a : std_logic;
SIGNAL BLK_CNT3_a5_a_a91 : std_logic;
SIGNAL BLK_CNT3_a5_a : std_logic;
SIGNAL BLK_CNT3_a4_a_a94 : std_logic;
SIGNAL BLK_CNT3_a4_a : std_logic;
SIGNAL Equal_a825 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1815 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1816 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1821 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1822 : std_logic;
SIGNAL Equal_a801 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1827 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1829 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1831 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1832 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1833 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1834 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1835 : std_logic;
SIGNAL BLK_CNT4_Max_a1_a_a1836 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1837 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1838 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1839 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1840 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1841 : std_logic;
SIGNAL BLK_CNT4_Max_a4_a_a1842 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1843 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1844 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1845 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1846 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1847 : std_logic;
SIGNAL BLK_CNT4_Max_a10_a_a1848 : std_logic;
SIGNAL Equal_a841 : std_logic;
SIGNAL Equal_a803 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1849 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1850 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1851 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1852 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1853 : std_logic;
SIGNAL BLK_CNT4_Max_a3_a_a1854 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1855 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1856 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1857 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1858 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1859 : std_logic;
SIGNAL BLK_CNT4_Max_a7_a_a1860 : std_logic;
SIGNAL Equal_a827 : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a10_a : std_logic;
SIGNAL clock_proc_a753 : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a6_a : std_logic;
SIGNAL clock_proc_a11 : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a2_a : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a1_a : std_logic;
SIGNAL offset_time_reg_ff_adffs_a3_a : std_logic;
SIGNAL CNT_LD_a361 : std_logic;
SIGNAL CNT_LD_a337 : std_logic;
SIGNAL CNT_LD_a349 : std_logic;
SIGNAL CNT_LD_a339 : std_logic;
SIGNAL CNT_LD_a341 : std_logic;
SIGNAL CNT_LD_a364 : std_logic;
SIGNAL CNT_LD_a343 : std_logic;
SIGNAL CNT_LD_a352 : std_logic;
SIGNAL CNT_LD_a345 : std_logic;
SIGNAL CNT_LD_a347 : std_logic;
SIGNAL clock_proc_a765 : std_logic;
SIGNAL disc_in3_cnt_a2_a : std_logic;
SIGNAL Equal_a817 : std_logic;
SIGNAL disc_in3_cnt_a6_a : std_logic;
SIGNAL Equal_a819 : std_logic;
SIGNAL disc_in4_cnt_a8_a_a124 : std_logic;
SIGNAL Equal_a821 : std_logic;
SIGNAL disc_in4_cnt_a9_a_a133 : std_logic;
SIGNAL disc_in4_cnt_a9_a : std_logic;
SIGNAL Equal_a845 : std_logic;
SIGNAL Equal_a823 : std_logic;
SIGNAL disc_in4_cnt_a11_a : std_logic;
SIGNAL disc_in4_cnt_a10_a_a142 : std_logic;
SIGNAL disc_in4_cnt_a10_a : std_logic;
SIGNAL Equal_a831 : std_logic;
SIGNAL clock_proc_a775 : std_logic;
SIGNAL clock_proc_a777 : std_logic;
SIGNAL clock_proc_a785 : std_logic;
SIGNAL clock_proc_a787 : std_logic;
SIGNAL clock_proc_a789 : std_logic;
SIGNAL clock_proc_a791 : std_logic;
SIGNAL Pur_Time_a11_a_acombout : std_logic;
SIGNAL Offset_Inc_a0_a_acombout : std_logic;
SIGNAL Offset_Inc_a9_a_acombout : std_logic;
SIGNAL Offset_Inc_a10_a_acombout : std_logic;
SIGNAL Offset_Inc_a6_a_acombout : std_logic;
SIGNAL Offset_Inc_a2_a_acombout : std_logic;
SIGNAL Offset_Inc_a1_a_acombout : std_logic;
SIGNAL Offset_Time_a3_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL Disc_En_a0_a_acombout : std_logic;
SIGNAL Disc_En_a4_a_acombout : std_logic;
SIGNAL disc_start_a0_a_a479 : std_logic;
SIGNAL Disc_In_a0_a_acombout : std_logic;
SIGNAL Disc_En_a3_a_acombout : std_logic;
SIGNAL Disc_Cmp_a4_a_acombout : std_logic;
SIGNAL Disc_Cmp_a2_a_acombout : std_logic;
SIGNAL disc_start_a0_a_a478 : std_logic;
SIGNAL disc_start_a0_a_a480 : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL clock_proc_a7 : std_logic;
SIGNAL BLK_CNT1_a0_a : std_logic;
SIGNAL BLK_CNT1_a0_a_a52 : std_logic;
SIGNAL BLK_CNT1_a1_a_a58 : std_logic;
SIGNAL BLK_CNT1_a2_a : std_logic;
SIGNAL BLK_CNT1_a2_a_a55 : std_logic;
SIGNAL BLK_CNT1_a3_a : std_logic;
SIGNAL Equal_a776 : std_logic;
SIGNAL BLOCK_OUT_a150 : std_logic;
SIGNAL Busy_State_a288 : std_logic;
SIGNAL Peak_Time_a0_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a0_a : std_logic;
SIGNAL Pur_Time_a0_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a0_a : std_logic;
SIGNAL RTD_PUR_acombout : std_logic;
SIGNAL Disc_In_a2_a_acombout : std_logic;
SIGNAL clock_proc_a5 : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL Peak_Time_a1_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a1_a : std_logic;
SIGNAL Peak_Time_a7_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a7_a : std_logic;
SIGNAL Busy_State_a287 : std_logic;
SIGNAL Select_a1064 : std_logic;
SIGNAL Busy_State_a290 : std_logic;
SIGNAL Select_a1063 : std_logic;
SIGNAL Busy_State_ast_reset0 : std_logic;
SIGNAL Busy_State_ast_reset1 : std_logic;
SIGNAL Busy_State_ast_reset2 : std_logic;
SIGNAL Busy_State_ast_idle : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL Peak_Time_a10_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a10_a : std_logic;
SIGNAL CNT_LD_a363 : std_logic;
SIGNAL CNT_LD_a351 : std_logic;
SIGNAL Peak_Time_a6_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a6_a : std_logic;
SIGNAL Peak_Time_a3_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a3_a : std_logic;
SIGNAL Peak_Time_a2_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a2_a : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL CNT_LD_a362 : std_logic;
SIGNAL CNT_LD_a350 : std_logic;
SIGNAL CNT_LD_a0 : std_logic;
SIGNAL Pur_Time_a10_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a10_a : std_logic;
SIGNAL clock_proc_a755 : std_logic;
SIGNAL Pur_Time_a5_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a5_a : std_logic;
SIGNAL Pur_Time_a6_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a6_a : std_logic;
SIGNAL clock_proc_a754 : std_logic;
SIGNAL Pur_Time_a8_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a8_a : std_logic;
SIGNAL Pur_Time_a3_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a3_a : std_logic;
SIGNAL Pur_Time_a9_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a9_a : std_logic;
SIGNAL clock_proc_a825 : std_logic;
SIGNAL clock_proc_a801 : std_logic;
SIGNAL Pur_Time_a4_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a4_a : std_logic;
SIGNAL Pur_Time_a1_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a1_a : std_logic;
SIGNAL Pur_Time_a2_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a2_a : std_logic;
SIGNAL Pur_Time_a7_a_acombout : std_logic;
SIGNAL pur_time_reg_ff_adffs_a7_a : std_logic;
SIGNAL clock_proc_a826 : std_logic;
SIGNAL clock_proc_a802 : std_logic;
SIGNAL clock_proc_a821 : std_logic;
SIGNAL clock_proc_a797 : std_logic;
SIGNAL Peak_Time_a4_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a4_a : std_logic;
SIGNAL CNT_LD_a365 : std_logic;
SIGNAL CNT_LD_a353 : std_logic;
SIGNAL Disc_In_a4_a_acombout : std_logic;
SIGNAL OTR_acombout : std_logic;
SIGNAL Select_a1075 : std_logic;
SIGNAL Select_a1074 : std_logic;
SIGNAL Select_a1067 : std_logic;
SIGNAL Busy_State_ast_reject1 : std_logic;
SIGNAL Peak_Time_a11_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a11_a : std_logic;
SIGNAL Peak_Time_a8_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a8_a : std_logic;
SIGNAL CNT_LD_a366 : std_logic;
SIGNAL CNT_LD_a354 : std_logic;
SIGNAL CNT_LD_a18 : std_logic;
SIGNAL Peak_Time_a5_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a5_a : std_logic;
SIGNAL clock_proc_a766 : std_logic;
SIGNAL clock_proc_a767 : std_logic;
SIGNAL clock_proc_a828 : std_logic;
SIGNAL clock_proc_a804 : std_logic;
SIGNAL Peak_Time_a9_a_acombout : std_logic;
SIGNAL peak_time_reg_ff_adffs_a9_a : std_logic;
SIGNAL clock_proc_a827 : std_logic;
SIGNAL clock_proc_a803 : std_logic;
SIGNAL clock_proc_a822 : std_logic;
SIGNAL clock_proc_a798 : std_logic;
SIGNAL CNT_LD_a315 : std_logic;
SIGNAL CNT_LD_a316 : std_logic;
SIGNAL CNT_LD_a317 : std_logic;
SIGNAL CNT_LD_a305 : std_logic;
SIGNAL CNT_LD_a318 : std_logic;
SIGNAL BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL add_a797 : std_logic;
SIGNAL add_a799 : std_logic;
SIGNAL add_a831 : std_logic;
SIGNAL add_a823 : std_logic;
SIGNAL add_a819 : std_logic;
SIGNAL add_a807 : std_logic;
SIGNAL add_a791 : std_logic;
SIGNAL add_a835 : std_logic;
SIGNAL add_a827 : std_logic;
SIGNAL add_a813 : std_logic;
SIGNAL add_a817 : std_logic;
SIGNAL add_a815 : std_logic;
SIGNAL add_a801 : std_logic;
SIGNAL clock_proc_a818 : std_logic;
SIGNAL clock_proc_a794 : std_logic;
SIGNAL clock_proc_a9 : std_logic;
SIGNAL BLK_CNT2_a0_a : std_logic;
SIGNAL BLK_CNT2_a0_a_a61 : std_logic;
SIGNAL BLK_CNT2_a1_a : std_logic;
SIGNAL BLK_CNT2_a1_a_a70 : std_logic;
SIGNAL BLK_CNT2_a2_a : std_logic;
SIGNAL BLK_CNT2_a2_a_a67 : std_logic;
SIGNAL BLK_CNT2_a3_a : std_logic;
SIGNAL BLK_CNT2_a3_a_a64 : std_logic;
SIGNAL BLK_CNT2_a4_a : std_logic;
SIGNAL Equal_a777 : std_logic;
SIGNAL BLOCK_OUT_a156 : std_logic;
SIGNAL BLOCK_OUT_a160 : std_logic;
SIGNAL BLOCK_OUT_a2_a_areg0 : std_logic;
SIGNAL Disc_Cmp_a3_a_acombout : std_logic;
SIGNAL BLK_ST_a407 : std_logic;
SIGNAL BLK_ST_a409 : std_logic;
SIGNAL BLOCK_OUT_a169 : std_logic;
SIGNAL Disc_In_a3_a_acombout : std_logic;
SIGNAL disc_in3_cnt_a0_a : std_logic;
SIGNAL disc_in3_cnt_a0_a_a82 : std_logic;
SIGNAL disc_in3_cnt_a1_a_a79 : std_logic;
SIGNAL disc_in3_cnt_a2_a_a76 : std_logic;
SIGNAL disc_in3_cnt_a3_a : std_logic;
SIGNAL disc_in3_cnt_a3_a_a73 : std_logic;
SIGNAL disc_in3_cnt_a4_a : std_logic;
SIGNAL disc_in3_cnt_a4_a_a94 : std_logic;
SIGNAL disc_in3_cnt_a5_a : std_logic;
SIGNAL disc_in3_cnt_a5_a_a88 : std_logic;
SIGNAL disc_in3_cnt_a6_a_a85 : std_logic;
SIGNAL disc_in3_cnt_a7_a : std_logic;
SIGNAL disc_in3_cnt_a1_a : std_logic;
SIGNAL Equal_a842 : std_logic;
SIGNAL Equal_a828 : std_logic;
SIGNAL Disc_in3_dis : std_logic;
SIGNAL BLK_ST_a410 : std_logic;
SIGNAL Disc_En_a1_a_acombout : std_logic;
SIGNAL Disc_En_a2_a_acombout : std_logic;
SIGNAL BLK_ST_a411 : std_logic;
SIGNAL BLK_ST_a412 : std_logic;
SIGNAL BLOCK_OUT_a170 : std_logic;
SIGNAL BLOCK_OUT_a4_a_areg0 : std_logic;
SIGNAL disc_in4_cnt_a0_a : std_logic;
SIGNAL disc_in4_cnt_a0_a_a118 : std_logic;
SIGNAL disc_in4_cnt_a1_a : std_logic;
SIGNAL disc_in4_cnt_a1_a_a130 : std_logic;
SIGNAL disc_in4_cnt_a2_a : std_logic;
SIGNAL disc_in4_cnt_a2_a_a121 : std_logic;
SIGNAL disc_in4_cnt_a3_a : std_logic;
SIGNAL disc_in4_cnt_a3_a_a115 : std_logic;
SIGNAL disc_in4_cnt_a4_a_a109 : std_logic;
SIGNAL disc_in4_cnt_a5_a : std_logic;
SIGNAL disc_in4_cnt_a5_a_a127 : std_logic;
SIGNAL disc_in4_cnt_a6_a : std_logic;
SIGNAL disc_in4_cnt_a4_a : std_logic;
SIGNAL Equal_a843 : std_logic;
SIGNAL Equal_a829 : std_logic;
SIGNAL disc_in4_cnt_a6_a_a112 : std_logic;
SIGNAL disc_in4_cnt_a7_a : std_logic;
SIGNAL disc_in4_cnt_a7_a_a136 : std_logic;
SIGNAL disc_in4_cnt_a8_a : std_logic;
SIGNAL Equal_a844 : std_logic;
SIGNAL Equal_a830 : std_logic;
SIGNAL Equal_a797 : std_logic;
SIGNAL Disc_in4_dis : std_logic;
SIGNAL ifdisc_a113 : std_logic;
SIGNAL ifdisc_a114 : std_logic;
SIGNAL ifdisc_a115 : std_logic;
SIGNAL ifdisc_a116 : std_logic;
SIGNAL Select_a1051 : std_logic;
SIGNAL Busy_State_ast_busy2 : std_logic;
SIGNAL BLK_CNT4_Max_a1 : std_logic;
SIGNAL BLK_CNT4_Max_a0_a_a1808 : std_logic;
SIGNAL Busy_State_ast_reject2 : std_logic;
SIGNAL Offset_Time_a0_a_acombout : std_logic;
SIGNAL offset_time_reg_ff_adffs_a0_a : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277 : std_logic;
SIGNAL BLK_CNT4_Max_a0_a_a1806 : std_logic;
SIGNAL Equal_a781 : std_logic;
SIGNAL BLK_CNT4_Max_a1804 : std_logic;
SIGNAL BLK_CNT4_Max_a11_a_a1805 : std_logic;
SIGNAL BLK_CNT4_Max_a11_a_a1803 : std_logic;
SIGNAL Equal_a780 : std_logic;
SIGNAL Equal_a782 : std_logic;
SIGNAL Offset_Time_a2_a_acombout : std_logic;
SIGNAL offset_time_reg_ff_adffs_a2_a : std_logic;
SIGNAL Offset_Time_a1_a_acombout : std_logic;
SIGNAL offset_time_reg_ff_adffs_a1_a : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301 : std_logic;
SIGNAL add_a821 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1825 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1828 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1826 : std_logic;
SIGNAL BLK_CNT4_Max_a2_a_a1830 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1820 : std_logic;
SIGNAL Offset_Inc_a5_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a5_a : std_logic;
SIGNAL Offset_Inc_a4_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a4_a : std_logic;
SIGNAL Offset_Inc_a3_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a3_a : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1819 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1823 : std_logic;
SIGNAL BLK_CNT4_Max_a6_a_a1824 : std_logic;
SIGNAL Offset_Inc_a8_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a8_a : std_logic;
SIGNAL Offset_Inc_a7_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a7_a : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1813 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1817 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1814 : std_logic;
SIGNAL BLK_CNT4_Max_a8_a_a1818 : std_logic;
SIGNAL Equal_a840 : std_logic;
SIGNAL Equal_a826 : std_logic;
SIGNAL BLK_CNT4_Max_a9_a_a1810 : std_logic;
SIGNAL BLK_CNT4_Max_a9_a_a1811 : std_logic;
SIGNAL BLK_CNT4_Max_a9_a_a1812 : std_logic;
SIGNAL Equal_a783 : std_logic;
SIGNAL Equal_a784 : std_logic;
SIGNAL Busy_State_ast_busy1 : std_logic;
SIGNAL Select_a1059 : std_logic;
SIGNAL Select_a1060 : std_logic;
SIGNAL Busy_State_a289 : std_logic;
SIGNAL Busy_State_ast_reject0 : std_logic;
SIGNAL add_a803 : std_logic;
SIGNAL add_a811 : std_logic;
SIGNAL add_a793 : std_logic;
SIGNAL Equal_a798 : std_logic;
SIGNAL add_a833 : std_logic;
SIGNAL add_a789 : std_logic;
SIGNAL clock_proc_a820 : std_logic;
SIGNAL clock_proc_a796 : std_logic;
SIGNAL BLOCK_OUT_a154 : std_logic;
SIGNAL BLOCK_OUT_a1_a_areg0 : std_logic;
SIGNAL Disc_In_a1_a_acombout : std_logic;
SIGNAL disc_start_a5 : std_logic;
SIGNAL disc_start_a4 : std_logic;
SIGNAL Disc_Cmp_E_Vec_a0_a : std_logic;
SIGNAL Disc_Cmp_E_Vec_a1_a : std_logic;
SIGNAL Disc_Cmp_E_Vec_a2_a : std_logic;
SIGNAL Disc_Cmp_E_Vec_a3_a : std_logic;
SIGNAL Disc_Cmp_E_Vec_a4_a : std_logic;
SIGNAL disc_start_a490 : std_logic;
SIGNAL disc_start_a1_a_a489 : std_logic;
SIGNAL disc_start_a1_a_a482 : std_logic;
SIGNAL disc_start_a1_a_a483 : std_logic;
SIGNAL disc_start_a1_a_a484 : std_logic;
SIGNAL BLK_ST_a0 : std_logic;
SIGNAL iFDisc_Del : std_logic;
SIGNAL Pur_a71 : std_logic;
SIGNAL Pur_areg0 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 : std_logic;
SIGNAL Offset_Inc_a11_a_acombout : std_logic;
SIGNAL offset_inc_reg_ff_adffs_a11_a : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a291 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 : std_logic;
SIGNAL clock_proc_a741 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269 : std_logic;
SIGNAL clock_proc_a740 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313 : std_logic;
SIGNAL clock_proc_a824 : std_logic;
SIGNAL clock_proc_a800 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309 : std_logic;
SIGNAL offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281 : std_logic;
SIGNAL clock_proc_a823 : std_logic;
SIGNAL clock_proc_a799 : std_logic;
SIGNAL clock_proc_a817 : std_logic;
SIGNAL clock_proc_a793 : std_logic;
SIGNAL Peak_Done_areg0 : std_logic;
SIGNAL BLOCK_OUT_a163 : std_logic;
SIGNAL BLOCK_OUT_a167 : std_logic;
SIGNAL BLOCK_OUT_a3_a_areg0 : std_logic;
SIGNAL BLOCK_OUT_a172 : std_logic;
SIGNAL BLOCK_OUT_a5_a_areg0 : std_logic;
SIGNAL FDISC_areg0 : std_logic;
SIGNAL PA_Busy_areg0 : std_logic;
SIGNAL reduce_or_a19 : std_logic;
SIGNAL reduce_or_a20 : std_logic;
SIGNAL reduce_or_a21 : std_logic;

BEGIN

ww_clk <= clk;
ww_imr <= imr;
ww_PA_Reset <= PA_Reset;
ww_OTR <= OTR;
ww_RTD_PUR <= RTD_PUR;
ww_Disc_In <= Disc_In;
ww_Disc_En <= Disc_En;
ww_Disc_Cmp <= Disc_Cmp;
ww_Offset_Time <= Offset_Time;
ww_Offset_Inc <= Offset_Inc;
ww_Peak_Time <= Peak_Time;
ww_Pur_Time <= Pur_Time;
Peak_Done <= ww_Peak_Done;
BLOCK_OUT <= ww_BLOCK_OUT;
FDISC <= ww_FDISC;
Pur <= ww_Pur;
PBusy <= ww_PBusy;
PA_Busy <= ww_PA_Busy;
Busy_Str <= ww_Busy_Str;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

BLK_CNT1_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT1_a4_a = DFFE(!clock_proc_a7 & BLK_CNT1_a4_a $ (!BLK_CNT1_a3_a_a49), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A5A5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT1_a4_a,
	cin => BLK_CNT1_a3_a_a49,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT1_a4_a);

BLK_CNT1_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT1_a3_a = DFFE(!clock_proc_a7 & BLK_CNT1_a3_a $ (BLK_CNT1_a2_a_a55), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT1_a3_a_a49 = CARRY(!BLK_CNT1_a2_a_a55 # !BLK_CNT1_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT1_a3_a,
	cin => BLK_CNT1_a2_a_a55,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT1_a3_a,
	cout => BLK_CNT1_a3_a_a49);

BLK_CNT1_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT1_a1_a = DFFE(!clock_proc_a7 & BLK_CNT1_a1_a $ (BLK_CNT1_a0_a_a52), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT1_a1_a_a58 = CARRY(!BLK_CNT1_a0_a_a52 # !BLK_CNT1_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT1_a1_a,
	cin => BLK_CNT1_a0_a_a52,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT1_a1_a,
	cout => BLK_CNT1_a1_a_a58);

BLK_CNT2_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a4_a = DFFE(!clock_proc_a9 & BLK_CNT2_a4_a $ !BLK_CNT2_a3_a_a64, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT2_a4_a_a55 = CARRY(BLK_CNT2_a4_a & !BLK_CNT2_a3_a_a64)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT2_a4_a,
	cin => BLK_CNT2_a3_a_a64,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a4_a,
	cout => BLK_CNT2_a4_a_a55);

BLK_CNT2_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a5_a = DFFE(!clock_proc_a9 & BLK_CNT2_a5_a $ (BLK_CNT2_a4_a_a55), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT2_a5_a,
	cin => BLK_CNT2_a4_a_a55,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a5_a);

BLK_ST_a408_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a408 = !Disc_En_a4_a_acombout & !Disc_En_a0_a_acombout & !Disc_En_a1_a_acombout & Disc_En_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a4_a_acombout,
	datab => Disc_En_a0_a_acombout,
	datac => Disc_En_a1_a_acombout,
	datad => Disc_En_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a408);

BLK_CNT4_Max_a5_a_a1796_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1796 = Busy_State_ast_reject2 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269 # Busy_State_ast_reject0 & add_a789) # !Busy_State_ast_reject2 & Busy_State_ast_reject0 & (add_a789)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject2,
	datab => Busy_State_ast_reject0,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269,
	datad => add_a789,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1796);

BLK_CNT4_Max_a5_a_a1797_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1797 = peak_time_reg_ff_adffs_a5_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset2,
	datac => peak_time_reg_ff_adffs_a5_a,
	datad => Busy_State_ast_busy2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1797);

BLK_CNT4_Max_a5_a_a1798_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1798 = peak_time_reg_ff_adffs_a6_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8C8C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject1,
	datab => peak_time_reg_ff_adffs_a6_a,
	datac => Busy_State_ast_idle,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1798);

BLK_CNT4_Max_a5_a_a1799_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1799 = BLK_CNT4_Max_a5_a_a1798 # peak_time_reg_ff_adffs_a4_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a5_a_a1798,
	datab => Busy_State_ast_reset0,
	datac => peak_time_reg_ff_adffs_a4_a,
	datad => Busy_State_ast_reset1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1799);

BLK_CNT4_Max_a5_a_a1800_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1800 = Busy_State_ast_busy1 & (pur_time_reg_ff_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_busy1,
	datac => pur_time_reg_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1800);

BLK_CNT4_Max_a5_a_a1801_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a5_a_a1801 = BLK_CNT4_Max_a5_a_a1799 # BLK_CNT4_Max_a5_a_a1797 # BLK_CNT4_Max_a5_a_a1800 # BLK_CNT4_Max_a5_a_a1796

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a5_a_a1799,
	datab => BLK_CNT4_Max_a5_a_a1797,
	datac => BLK_CNT4_Max_a5_a_a1800,
	datad => BLK_CNT4_Max_a5_a_a1796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a5_a_a1801);

pur_time_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a11_a = DFFE(Pur_Time_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a11_a);

BLK_CNT4_Max_a11_a_a1802_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a11_a_a1802 = Busy_State_ast_reject0 & (add_a793 # pur_time_reg_ff_adffs_a11_a & Busy_State_ast_busy1) # !Busy_State_ast_reject0 & pur_time_reg_ff_adffs_a11_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject0,
	datab => pur_time_reg_ff_adffs_a11_a,
	datac => Busy_State_ast_busy1,
	datad => add_a793,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a11_a_a1802);

BLK_CNT4_Max_a0_a_a1807_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a0_a_a1807 = peak_time_reg_ff_adffs_a1_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D0D0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => Busy_State_ast_reject1,
	datac => peak_time_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a0_a_a1807);

BLK_CNT4_Max_a9_a_a1809_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a9_a_a1809 = Busy_State_ast_reject0 & (add_a801 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281) # !Busy_State_ast_reject0 & (Busy_State_ast_reject2 & 
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject0,
	datab => add_a801,
	datac => Busy_State_ast_reject2,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a9_a_a1809);

Pur_a68_I : apex20ke_lcell
-- Equation(s):
-- Pur_a68 = RTD_PUR_acombout # PA_Reset_acombout # OTR_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => RTD_PUR_acombout,
	datac => PA_Reset_acombout,
	datad => OTR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pur_a68);

Pur_a70_I : apex20ke_lcell
-- Equation(s):
-- Pur_a70 = Pur_a68 # Pur_areg0 & Busy_State_ast_idle & !clock_proc_a797

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF08",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Pur_areg0,
	datab => Busy_State_ast_idle,
	datac => clock_proc_a797,
	datad => Pur_a68,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pur_a70);

Select_a1052_I : apex20ke_lcell
-- Equation(s):
-- Select_a1052 = !Disc_In_a4_a_acombout & !Busy_State_ast_idle & !OTR_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0003",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Disc_In_a4_a_acombout,
	datac => Busy_State_ast_idle,
	datad => OTR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1052);

Select_a1053_I : apex20ke_lcell
-- Equation(s):
-- Select_a1053 = Busy_State_ast_busy2 # Busy_State_ast_reject0 & !Disc_In_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => Busy_State_ast_reject0,
	datad => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1053);

Select_a1054_I : apex20ke_lcell
-- Equation(s):
-- Select_a1054 = Select_a1052 # Select_a1053 & (Equal_a784 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF8C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a784,
	datab => Select_a1053,
	datac => Busy_State_ast_idle,
	datad => Select_a1052,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1054);

Select_a1061_I : apex20ke_lcell
-- Equation(s):
-- Select_a1061 = !PA_Reset_acombout & (!Busy_State_ast_idle & !Disc_In_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0005",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datac => Busy_State_ast_idle,
	datad => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1061);

add_a805_I : apex20ke_lcell
-- Equation(s):
-- add_a805 = peak_time_reg_ff_adffs_a7_a $ peak_time_reg_ff_adffs_a5_a $ !add_a819
-- add_a807 = CARRY(peak_time_reg_ff_adffs_a7_a & (peak_time_reg_ff_adffs_a5_a # !add_a819) # !peak_time_reg_ff_adffs_a7_a & peak_time_reg_ff_adffs_a5_a & !add_a819)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a7_a,
	datab => peak_time_reg_ff_adffs_a5_a,
	cin => add_a819,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a805,
	cout => add_a807);

Pur_Time_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(11),
	combout => Pur_Time_a11_a_acombout);

add_a809_I : apex20ke_lcell
-- Equation(s):
-- add_a809 = peak_time_reg_ff_adffs_a11_a $ !add_a803
-- add_a811 = CARRY(peak_time_reg_ff_adffs_a11_a & !add_a803)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => peak_time_reg_ff_adffs_a11_a,
	cin => add_a803,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a809,
	cout => add_a811);

offset_inc_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a0_a = DFFE(Offset_Inc_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a0_a);

offset_inc_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a9_a = DFFE(Offset_Inc_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a9_a);

add_a825_I : apex20ke_lcell
-- Equation(s):
-- add_a825 = peak_time_reg_ff_adffs_a8_a $ peak_time_reg_ff_adffs_a10_a $ add_a835
-- add_a827 = CARRY(peak_time_reg_ff_adffs_a8_a & !peak_time_reg_ff_adffs_a10_a & !add_a835 # !peak_time_reg_ff_adffs_a8_a & (!add_a835 # !peak_time_reg_ff_adffs_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a8_a,
	datab => peak_time_reg_ff_adffs_a10_a,
	cin => add_a835,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a825,
	cout => add_a827);

clock_proc_a745_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a819 = add_a825 & BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !add_a821) # !add_a825 & !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !add_a821)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a825,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datad => add_a821,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a745,
	cascout => clock_proc_a819);

add_a829_I : apex20ke_lcell
-- Equation(s):
-- add_a829 = peak_time_reg_ff_adffs_a4_a $ peak_time_reg_ff_adffs_a2_a $ add_a799
-- add_a831 = CARRY(peak_time_reg_ff_adffs_a4_a & !peak_time_reg_ff_adffs_a2_a & !add_a799 # !peak_time_reg_ff_adffs_a4_a & (!add_a799 # !peak_time_reg_ff_adffs_a2_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a4_a,
	datab => peak_time_reg_ff_adffs_a2_a,
	cin => add_a799,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a829,
	cout => add_a831);

clock_proc_a795_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a795 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & add_a805 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !add_a829) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !add_a805 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ 
-- !add_a829)) & CASCADE(clock_proc_a819)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => add_a805,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datad => add_a829,
	cascin => clock_proc_a819,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a795);

BLK_CNT3_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a3_a = DFFE(!clock_proc_a11 & BLK_CNT3_a3_a $ BLK_CNT3_a2_a_a76, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a3_a_a73 = CARRY(!BLK_CNT3_a2_a_a76 # !BLK_CNT3_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT3_a3_a,
	cin => BLK_CNT3_a2_a_a76,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a3_a,
	cout => BLK_CNT3_a3_a_a73);

BLK_CNT3_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a2_a = DFFE(!clock_proc_a11 & BLK_CNT3_a2_a $ (!BLK_CNT3_a1_a_a79), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a2_a_a76 = CARRY(BLK_CNT3_a2_a & (!BLK_CNT3_a1_a_a79))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a2_a,
	cin => BLK_CNT3_a1_a_a79,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a2_a,
	cout => BLK_CNT3_a2_a_a76);

BLK_CNT3_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a1_a = DFFE(!clock_proc_a11 & BLK_CNT3_a1_a $ BLK_CNT3_a0_a_a82, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a1_a_a79 = CARRY(!BLK_CNT3_a0_a_a82 # !BLK_CNT3_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT3_a1_a,
	cin => BLK_CNT3_a0_a_a82,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a1_a,
	cout => BLK_CNT3_a1_a_a79);

BLK_CNT3_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a0_a = DFFE(!clock_proc_a11 & BLK_CNT3_a0_a $ BLOCK_OUT_a3_a_areg0, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a0_a_a82 = CARRY(BLK_CNT3_a0_a & BLOCK_OUT_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a0_a,
	datab => BLOCK_OUT_a3_a_areg0,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a0_a,
	cout => BLK_CNT3_a0_a_a82);

Equal_a799_I : apex20ke_lcell
-- Equation(s):
-- Equal_a839 = BLK_CNT3_a0_a & BLK_CNT3_a1_a & BLK_CNT3_a2_a & BLK_CNT3_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a0_a,
	datab => BLK_CNT3_a1_a,
	datac => BLK_CNT3_a2_a,
	datad => BLK_CNT3_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a799,
	cascout => Equal_a839);

BLK_CNT3_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a7_a = DFFE(!clock_proc_a11 & BLK_CNT3_a7_a $ (BLK_CNT3_a6_a_a88), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a7_a,
	cin => BLK_CNT3_a6_a_a88,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a7_a);

BLK_CNT3_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a6_a = DFFE(!clock_proc_a11 & BLK_CNT3_a6_a $ !BLK_CNT3_a5_a_a91, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a6_a_a88 = CARRY(BLK_CNT3_a6_a & !BLK_CNT3_a5_a_a91)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT3_a6_a,
	cin => BLK_CNT3_a5_a_a91,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a6_a,
	cout => BLK_CNT3_a6_a_a88);

BLK_CNT3_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a5_a = DFFE(!clock_proc_a11 & BLK_CNT3_a5_a $ (BLK_CNT3_a4_a_a94), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a5_a_a91 = CARRY(!BLK_CNT3_a4_a_a94 # !BLK_CNT3_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a5_a,
	cin => BLK_CNT3_a4_a_a94,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a5_a,
	cout => BLK_CNT3_a5_a_a91);

BLK_CNT3_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT3_a4_a = DFFE(!clock_proc_a11 & BLK_CNT3_a4_a $ !BLK_CNT3_a3_a_a73, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT3_a4_a_a94 = CARRY(BLK_CNT3_a4_a & !BLK_CNT3_a3_a_a73)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT3_a4_a,
	cin => BLK_CNT3_a3_a_a73,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT3_a4_a,
	cout => BLK_CNT3_a4_a_a94);

Equal_a825_I : apex20ke_lcell
-- Equation(s):
-- Equal_a825 = (!BLK_CNT3_a5_a & !BLK_CNT3_a4_a & BLK_CNT3_a7_a & !BLK_CNT3_a6_a) & CASCADE(Equal_a839)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT3_a5_a,
	datab => BLK_CNT3_a4_a,
	datac => BLK_CNT3_a7_a,
	datad => BLK_CNT3_a6_a,
	cascin => Equal_a839,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a825);

BLK_CNT4_Max_a8_a_a1815_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1815 = peak_time_reg_ff_adffs_a7_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a7_a,
	datac => Busy_State_ast_reset0,
	datad => Busy_State_ast_reset1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1815);

BLK_CNT4_Max_a8_a_a1816_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1816 = BLK_CNT4_Max_a8_a_a1815 # peak_time_reg_ff_adffs_a9_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a9_a,
	datab => Busy_State_ast_idle,
	datac => Busy_State_ast_reject1,
	datad => BLK_CNT4_Max_a8_a_a1815,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1816);

BLK_CNT4_Max_a6_a_a1821_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1821 = peak_time_reg_ff_adffs_a7_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AF00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject1,
	datac => Busy_State_ast_idle,
	datad => peak_time_reg_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1821);

BLK_CNT4_Max_a6_a_a1822_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1822 = BLK_CNT4_Max_a6_a_a1821 # peak_time_reg_ff_adffs_a5_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a5_a,
	datab => Busy_State_ast_reset0,
	datac => Busy_State_ast_reset1,
	datad => BLK_CNT4_Max_a6_a_a1821,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1822);

BLK_CNT4_Max_a2_a_a1827_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1827 = peak_time_reg_ff_adffs_a1_a & (Busy_State_ast_reset1 # Busy_State_ast_reset0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_reset1,
	datac => Busy_State_ast_reset0,
	datad => peak_time_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1827);

BLK_CNT4_Max_a2_a_a1829_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1829 = pur_time_reg_ff_adffs_a2_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pur_time_reg_ff_adffs_a2_a,
	datad => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1829);

BLK_CNT4_Max_a1_a_a1831_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1831 = offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305 & (Busy_State_ast_reject2 # Busy_State_ast_reject0 & add_a829) # !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305 & (Busy_State_ast_reject0 & add_a829)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject0,
	datad => add_a829,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1831);

BLK_CNT4_Max_a1_a_a1832_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1832 = peak_time_reg_ff_adffs_a1_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset2,
	datac => Busy_State_ast_busy2,
	datad => peak_time_reg_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1832);

BLK_CNT4_Max_a1_a_a1833_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1833 = peak_time_reg_ff_adffs_a2_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => peak_time_reg_ff_adffs_a2_a,
	datac => Busy_State_ast_idle,
	datad => Busy_State_ast_reject1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1833);

BLK_CNT4_Max_a1_a_a1834_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1834 = BLK_CNT4_Max_a1_a_a1833 # peak_time_reg_ff_adffs_a0_a & (Busy_State_ast_reset1 # Busy_State_ast_reset0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFE0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => Busy_State_ast_reset0,
	datac => peak_time_reg_ff_adffs_a0_a,
	datad => BLK_CNT4_Max_a1_a_a1833,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1834);

BLK_CNT4_Max_a1_a_a1835_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1835 = pur_time_reg_ff_adffs_a1_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pur_time_reg_ff_adffs_a1_a,
	datad => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1835);

BLK_CNT4_Max_a1_a_a1836_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1_a_a1836 = BLK_CNT4_Max_a1_a_a1831 # BLK_CNT4_Max_a1_a_a1835 # BLK_CNT4_Max_a1_a_a1832 # BLK_CNT4_Max_a1_a_a1834

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a1_a_a1831,
	datab => BLK_CNT4_Max_a1_a_a1835,
	datac => BLK_CNT4_Max_a1_a_a1832,
	datad => BLK_CNT4_Max_a1_a_a1834,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1_a_a1836);

BLK_CNT4_Max_a4_a_a1837_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1837 = offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 & (Busy_State_ast_reject2 # Busy_State_ast_reject0 & add_a805) # !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 & (Busy_State_ast_reject0 & add_a805)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject0,
	datad => add_a805,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1837);

BLK_CNT4_Max_a4_a_a1838_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1838 = peak_time_reg_ff_adffs_a4_a & (Busy_State_ast_busy2 # Busy_State_ast_reset2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => Busy_State_ast_reset2,
	datad => peak_time_reg_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1838);

BLK_CNT4_Max_a4_a_a1839_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1839 = peak_time_reg_ff_adffs_a5_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8A8A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a5_a,
	datab => Busy_State_ast_reject1,
	datac => Busy_State_ast_idle,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1839);

BLK_CNT4_Max_a4_a_a1840_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1840 = BLK_CNT4_Max_a4_a_a1839 # peak_time_reg_ff_adffs_a3_a & (Busy_State_ast_reset1 # Busy_State_ast_reset0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFE0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => Busy_State_ast_reset0,
	datac => peak_time_reg_ff_adffs_a3_a,
	datad => BLK_CNT4_Max_a4_a_a1839,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1840);

BLK_CNT4_Max_a4_a_a1841_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1841 = Busy_State_ast_busy1 & pur_time_reg_ff_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_busy1,
	datad => pur_time_reg_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1841);

BLK_CNT4_Max_a4_a_a1842_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a4_a_a1842 = BLK_CNT4_Max_a4_a_a1837 # BLK_CNT4_Max_a4_a_a1838 # BLK_CNT4_Max_a4_a_a1840 # BLK_CNT4_Max_a4_a_a1841

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a4_a_a1837,
	datab => BLK_CNT4_Max_a4_a_a1838,
	datac => BLK_CNT4_Max_a4_a_a1840,
	datad => BLK_CNT4_Max_a4_a_a1841,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a4_a_a1842);

BLK_CNT4_Max_a10_a_a1843_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1843 = offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 & (Busy_State_ast_reject2 # add_a809 & Busy_State_ast_reject0) # !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 & add_a809 & Busy_State_ast_reject0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289,
	datab => add_a809,
	datac => Busy_State_ast_reject0,
	datad => Busy_State_ast_reject2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1843);

BLK_CNT4_Max_a10_a_a1844_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1844 = peak_time_reg_ff_adffs_a10_a & (Busy_State_ast_busy2 # Busy_State_ast_reset2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => Busy_State_ast_reset2,
	datad => peak_time_reg_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1844);

BLK_CNT4_Max_a10_a_a1845_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1845 = peak_time_reg_ff_adffs_a9_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset0,
	datac => Busy_State_ast_reset1,
	datad => peak_time_reg_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1845);

BLK_CNT4_Max_a10_a_a1846_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1846 = BLK_CNT4_Max_a10_a_a1845 # peak_time_reg_ff_adffs_a11_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F8FA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a11_a,
	datab => Busy_State_ast_reject1,
	datac => BLK_CNT4_Max_a10_a_a1845,
	datad => Busy_State_ast_idle,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1846);

BLK_CNT4_Max_a10_a_a1847_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1847 = pur_time_reg_ff_adffs_a10_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pur_time_reg_ff_adffs_a10_a,
	datac => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1847);

BLK_CNT4_Max_a10_a_a1848_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a10_a_a1848 = BLK_CNT4_Max_a10_a_a1844 # BLK_CNT4_Max_a10_a_a1846 # BLK_CNT4_Max_a10_a_a1847 # BLK_CNT4_Max_a10_a_a1843

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a10_a_a1844,
	datab => BLK_CNT4_Max_a10_a_a1846,
	datac => BLK_CNT4_Max_a10_a_a1847,
	datad => BLK_CNT4_Max_a10_a_a1843,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a10_a_a1848);

Equal_a803_I : apex20ke_lcell
-- Equation(s):
-- Equal_a841 = BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & BLK_CNT4_Max_a4_a_a1842 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a $ !BLK_CNT4_Max_a10_a_a1848) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !BLK_CNT4_Max_a4_a_a1842 & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a $ !BLK_CNT4_Max_a10_a_a1848)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => BLK_CNT4_Max_a4_a_a1842,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datad => BLK_CNT4_Max_a10_a_a1848,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a803,
	cascout => Equal_a841);

BLK_CNT4_Max_a3_a_a1849_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1849 = add_a817 & (Busy_State_ast_reject0 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309) # !add_a817 & (Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a817,
	datab => Busy_State_ast_reject0,
	datac => Busy_State_ast_reject2,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1849);

BLK_CNT4_Max_a3_a_a1850_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1850 = peak_time_reg_ff_adffs_a3_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E0E0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset2,
	datab => Busy_State_ast_busy2,
	datac => peak_time_reg_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1850);

BLK_CNT4_Max_a3_a_a1851_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1851 = peak_time_reg_ff_adffs_a2_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_reset0,
	datac => Busy_State_ast_reset1,
	datad => peak_time_reg_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1851);

BLK_CNT4_Max_a3_a_a1852_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1852 = BLK_CNT4_Max_a3_a_a1851 # peak_time_reg_ff_adffs_a4_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EEAE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a3_a_a1851,
	datab => peak_time_reg_ff_adffs_a4_a,
	datac => Busy_State_ast_idle,
	datad => Busy_State_ast_reject1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1852);

BLK_CNT4_Max_a3_a_a1853_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1853 = pur_time_reg_ff_adffs_a3_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => pur_time_reg_ff_adffs_a3_a,
	datac => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1853);

BLK_CNT4_Max_a3_a_a1854_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a3_a_a1854 = BLK_CNT4_Max_a3_a_a1853 # BLK_CNT4_Max_a3_a_a1849 # BLK_CNT4_Max_a3_a_a1852 # BLK_CNT4_Max_a3_a_a1850

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a3_a_a1853,
	datab => BLK_CNT4_Max_a3_a_a1849,
	datac => BLK_CNT4_Max_a3_a_a1852,
	datad => BLK_CNT4_Max_a3_a_a1850,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a3_a_a1854);

BLK_CNT4_Max_a7_a_a1855_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1855 = Busy_State_ast_reject2 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313 # Busy_State_ast_reject0 & add_a825) # !Busy_State_ast_reject2 & Busy_State_ast_reject0 & (add_a825)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject2,
	datab => Busy_State_ast_reject0,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313,
	datad => add_a825,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1855);

BLK_CNT4_Max_a7_a_a1856_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1856 = peak_time_reg_ff_adffs_a7_a & (Busy_State_ast_busy2 # Busy_State_ast_reset2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => peak_time_reg_ff_adffs_a7_a,
	datad => Busy_State_ast_reset2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1856);

BLK_CNT4_Max_a7_a_a1857_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1857 = peak_time_reg_ff_adffs_a8_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8A8A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a8_a,
	datab => Busy_State_ast_reject1,
	datac => Busy_State_ast_idle,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1857);

BLK_CNT4_Max_a7_a_a1858_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1858 = BLK_CNT4_Max_a7_a_a1857 # peak_time_reg_ff_adffs_a6_a & (Busy_State_ast_reset0 # Busy_State_ast_reset1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a6_a,
	datab => Busy_State_ast_reset0,
	datac => BLK_CNT4_Max_a7_a_a1857,
	datad => Busy_State_ast_reset1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1858);

BLK_CNT4_Max_a7_a_a1859_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1859 = pur_time_reg_ff_adffs_a7_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pur_time_reg_ff_adffs_a7_a,
	datad => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1859);

BLK_CNT4_Max_a7_a_a1860_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a7_a_a1860 = BLK_CNT4_Max_a7_a_a1858 # BLK_CNT4_Max_a7_a_a1856 # BLK_CNT4_Max_a7_a_a1859 # BLK_CNT4_Max_a7_a_a1855

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a7_a_a1858,
	datab => BLK_CNT4_Max_a7_a_a1856,
	datac => BLK_CNT4_Max_a7_a_a1859,
	datad => BLK_CNT4_Max_a7_a_a1855,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a7_a_a1860);

Equal_a827_I : apex20ke_lcell
-- Equation(s):
-- Equal_a827 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & BLK_CNT4_Max_a3_a_a1854 & (BLK_CNT4_Max_a7_a_a1860 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & !BLK_CNT4_Max_a3_a_a1854 & 
-- (BLK_CNT4_Max_a7_a_a1860 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a)) & CASCADE(Equal_a841)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	datab => BLK_CNT4_Max_a7_a_a1860,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datad => BLK_CNT4_Max_a3_a_a1854,
	cascin => Equal_a841,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a827);

offset_inc_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a10_a = DFFE(Offset_Inc_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a10_a);

Offset_Inc_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(0),
	combout => Offset_Inc_a0_a_acombout);

Offset_Inc_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(9),
	combout => Offset_Inc_a9_a_acombout);

offset_inc_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a6_a = DFFE(Offset_Inc_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a6_a);

clock_proc_a11_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a11 = BLK_ST_a409 # PA_Reset_acombout # BLK_ST_a0 # !BLOCK_OUT_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFD",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a3_a_areg0,
	datab => BLK_ST_a409,
	datac => PA_Reset_acombout,
	datad => BLK_ST_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a11);

offset_inc_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a2_a = DFFE(Offset_Inc_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a2_a);

offset_inc_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a1_a = DFFE(Offset_Inc_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a1_a);

offset_time_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_time_reg_ff_adffs_a3_a = DFFE(Offset_Time_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Time_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_time_reg_ff_adffs_a3_a);

CNT_LD_a337_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a361 = BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & peak_time_reg_ff_adffs_a4_a & (peak_time_reg_ff_adffs_a9_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & !peak_time_reg_ff_adffs_a4_a & 
-- (peak_time_reg_ff_adffs_a9_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datab => peak_time_reg_ff_adffs_a9_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datad => peak_time_reg_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a337,
	cascout => CNT_LD_a361);

CNT_LD_a349_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a349 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & peak_time_reg_ff_adffs_a8_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !peak_time_reg_ff_adffs_a0_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & !peak_time_reg_ff_adffs_a8_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !peak_time_reg_ff_adffs_a0_a)) & CASCADE(CNT_LD_a361)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datab => peak_time_reg_ff_adffs_a8_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datad => peak_time_reg_ff_adffs_a0_a,
	cascin => CNT_LD_a361,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a349);

CNT_LD_a343_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a364 = BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & peak_time_reg_ff_adffs_a5_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ !peak_time_reg_ff_adffs_a10_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !peak_time_reg_ff_adffs_a5_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ !peak_time_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datac => peak_time_reg_ff_adffs_a5_a,
	datad => peak_time_reg_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a343,
	cascout => CNT_LD_a364);

CNT_LD_a352_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a352 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & peak_time_reg_ff_adffs_a1_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a $ !peak_time_reg_ff_adffs_a9_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & !peak_time_reg_ff_adffs_a1_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a $ !peak_time_reg_ff_adffs_a9_a)) & CASCADE(CNT_LD_a364)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	datac => peak_time_reg_ff_adffs_a9_a,
	datad => peak_time_reg_ff_adffs_a1_a,
	cascin => CNT_LD_a364,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a352);

Offset_Inc_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(10),
	combout => Offset_Inc_a10_a_acombout);

disc_in3_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a2_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a2_a $ (!disc_in3_cnt_a1_a_a79), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a2_a_a76 = CARRY(disc_in3_cnt_a2_a & (!disc_in3_cnt_a1_a_a79))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a2_a,
	cin => disc_in3_cnt_a1_a_a79,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a2_a,
	cout => disc_in3_cnt_a2_a_a76);

disc_in3_cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a6_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a6_a $ (!disc_in3_cnt_a5_a_a88), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a6_a_a85 = CARRY(disc_in3_cnt_a6_a & (!disc_in3_cnt_a5_a_a88))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a6_a,
	cin => disc_in3_cnt_a5_a_a88,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a6_a,
	cout => disc_in3_cnt_a6_a_a85);

disc_in4_cnt_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a8_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a8_a $ (!disc_in4_cnt_a7_a_a136), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a8_a_a124 = CARRY(disc_in4_cnt_a8_a & (!disc_in4_cnt_a7_a_a136))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in4_cnt_a8_a,
	cin => disc_in4_cnt_a7_a_a136,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a8_a,
	cout => disc_in4_cnt_a8_a_a124);

disc_in4_cnt_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a9_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a9_a $ (disc_in4_cnt_a8_a_a124), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a9_a_a133 = CARRY(!disc_in4_cnt_a8_a_a124 # !disc_in4_cnt_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in4_cnt_a9_a,
	cin => disc_in4_cnt_a8_a_a124,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a9_a,
	cout => disc_in4_cnt_a9_a_a133);

Equal_a823_I : apex20ke_lcell
-- Equation(s):
-- Equal_a845 = peak_time_reg_ff_adffs_a11_a & disc_in4_cnt_a9_a & (peak_time_reg_ff_adffs_a9_a $ !disc_in4_cnt_a7_a) # !peak_time_reg_ff_adffs_a11_a & !disc_in4_cnt_a9_a & (peak_time_reg_ff_adffs_a9_a $ !disc_in4_cnt_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a11_a,
	datab => peak_time_reg_ff_adffs_a9_a,
	datac => disc_in4_cnt_a9_a,
	datad => disc_in4_cnt_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a823,
	cascout => Equal_a845);

disc_in4_cnt_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a11_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a10_a_a142 $ disc_in4_cnt_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => disc_in4_cnt_a11_a,
	cin => disc_in4_cnt_a10_a_a142,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a11_a);

disc_in4_cnt_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a10_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a10_a $ !disc_in4_cnt_a9_a_a133, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a10_a_a142 = CARRY(disc_in4_cnt_a10_a & !disc_in4_cnt_a9_a_a133)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a10_a,
	cin => disc_in4_cnt_a9_a_a133,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a10_a,
	cout => disc_in4_cnt_a10_a_a142);

Equal_a831_I : apex20ke_lcell
-- Equation(s):
-- Equal_a831 = (!disc_in4_cnt_a11_a & (!disc_in4_cnt_a10_a)) & CASCADE(Equal_a845)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a11_a,
	datad => disc_in4_cnt_a10_a,
	cascin => Equal_a845,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a831);

Offset_Inc_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(6),
	combout => Offset_Inc_a6_a_acombout);

Offset_Inc_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(2),
	combout => Offset_Inc_a2_a_acombout);

Offset_Inc_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(1),
	combout => Offset_Inc_a1_a_acombout);

Offset_Time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(3),
	combout => Offset_Time_a3_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

Disc_En_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En(0),
	combout => Disc_En_a0_a_acombout);

Disc_En_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En(4),
	combout => Disc_En_a4_a_acombout);

disc_start_a0_a_a479_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a0_a_a479 = !Disc_En_a4_a_acombout # !Disc_En_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Disc_En_a0_a_acombout,
	datad => Disc_En_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a0_a_a479);

Disc_In_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(0),
	combout => Disc_In_a0_a_acombout);

Disc_En_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En(3),
	combout => Disc_En_a3_a_acombout);

Disc_Cmp_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_Cmp(4),
	combout => Disc_Cmp_a4_a_acombout);

Disc_Cmp_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_Cmp(2),
	combout => Disc_Cmp_a2_a_acombout);

disc_start_a0_a_a478_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a0_a_a478 = Disc_En_a2_a_acombout & (Disc_Cmp_a2_a_acombout) # !Disc_En_a2_a_acombout & (Disc_En_a3_a_acombout # Disc_Cmp_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FE54",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a2_a_acombout,
	datab => Disc_En_a3_a_acombout,
	datac => Disc_Cmp_a4_a_acombout,
	datad => Disc_Cmp_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a0_a_a478);

disc_start_a0_a_a480_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a0_a_a480 = Disc_In_a0_a_acombout & (disc_start_a0_a_a479 # disc_start_a0_a_a478 # !Disc_En_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0D0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a1_a_acombout,
	datab => disc_start_a0_a_a479,
	datac => Disc_In_a0_a_acombout,
	datad => disc_start_a0_a_a478,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a0_a_a480);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

clock_proc_a7_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a7 = disc_start_a0_a_a480 # PA_Reset_acombout # !BLOCK_OUT_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLOCK_OUT_a1_a_areg0,
	datac => disc_start_a0_a_a480,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a7);

BLK_CNT1_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT1_a0_a = DFFE(!clock_proc_a7 & BLOCK_OUT_a1_a_areg0 $ BLK_CNT1_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT1_a0_a_a52 = CARRY(BLOCK_OUT_a1_a_areg0 & BLK_CNT1_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a1_a_areg0,
	datab => BLK_CNT1_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT1_a0_a,
	cout => BLK_CNT1_a0_a_a52);

BLK_CNT1_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT1_a2_a = DFFE(!clock_proc_a7 & BLK_CNT1_a2_a $ !BLK_CNT1_a1_a_a58, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT1_a2_a_a55 = CARRY(BLK_CNT1_a2_a & !BLK_CNT1_a1_a_a58)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT1_a2_a,
	cin => BLK_CNT1_a1_a_a58,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a7,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT1_a2_a,
	cout => BLK_CNT1_a2_a_a55);

Equal_a776_I : apex20ke_lcell
-- Equation(s):
-- Equal_a776 = !BLK_CNT1_a1_a & BLK_CNT1_a0_a & BLK_CNT1_a3_a & !BLK_CNT1_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT1_a1_a,
	datab => BLK_CNT1_a0_a,
	datac => BLK_CNT1_a3_a,
	datad => BLK_CNT1_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a776);

BLOCK_OUT_a150_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a150 = BLOCK_OUT_a1_a_areg0 & (!Equal_a776 # !BLK_CNT1_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "50F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT1_a4_a,
	datac => BLOCK_OUT_a1_a_areg0,
	datad => Equal_a776,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a150);

Busy_State_a288_I : apex20ke_lcell
-- Equation(s):
-- Busy_State_a288 = !Equal_a784 & (Busy_State_ast_reject0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a784,
	datac => Busy_State_ast_reject0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Busy_State_a288);

Peak_Time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(0),
	combout => Peak_Time_a0_a_acombout);

peak_time_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a0_a = DFFE(Peak_Time_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a0_a);

Pur_Time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(0),
	combout => Pur_Time_a0_a_acombout);

pur_time_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a0_a = DFFE(Pur_Time_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a0_a);

RTD_PUR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RTD_PUR,
	combout => RTD_PUR_acombout);

Disc_In_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(2),
	combout => Disc_In_a2_a_acombout);

clock_proc_a5_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a5 = clock_proc_a795 & clock_proc_a794 & clock_proc_a796

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a795,
	datab => clock_proc_a794,
	datac => clock_proc_a796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a5);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(CNT_LD_a318) & Busy_State_ast_idle $ (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ (Busy_State_ast_idle & BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ (Busy_State_ast_idle & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

Peak_Time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(1),
	combout => Peak_Time_a1_a_acombout);

peak_time_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a1_a = DFFE(Peak_Time_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a1_a);

Peak_Time_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(7),
	combout => Peak_Time_a7_a_acombout);

peak_time_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a7_a = DFFE(Peak_Time_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a7_a);

Busy_State_a287_I : apex20ke_lcell
-- Equation(s):
-- Busy_State_a287 = Equal_a784 # !Busy_State_ast_idle

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_idle,
	datad => Equal_a784,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Busy_State_a287);

Select_a1064_I : apex20ke_lcell
-- Equation(s):
-- Select_a1064 = Busy_State_ast_idle & !Busy_State_ast_reject0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_idle,
	datad => Busy_State_ast_reject0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1064);

Busy_State_a290_I : apex20ke_lcell
-- Equation(s):
-- Busy_State_a290 = !RTD_PUR_acombout & Busy_State_ast_reset0 & (iFDisc_Del # !ifdisc_a116)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4050",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => RTD_PUR_acombout,
	datab => iFDisc_Del,
	datac => Busy_State_ast_reset0,
	datad => ifdisc_a116,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Busy_State_a290);

Select_a1063_I : apex20ke_lcell
-- Equation(s):
-- Select_a1063 = !Select_a1059 & (PA_Reset_acombout # !Equal_a784 & Busy_State_a290)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0B0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Equal_a784,
	datac => Select_a1059,
	datad => Busy_State_a290,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1063);

Busy_State_ast_reset0_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reset0 = DFFE(Select_a1063 # PA_Reset_acombout & (Busy_State_ast_reset0 # !Select_a1064), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF8A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Busy_State_ast_reset0,
	datac => Select_a1064,
	datad => Select_a1063,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reset0);

Busy_State_ast_reset1_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reset1 = DFFE(Busy_State_ast_reset1 & (Busy_State_ast_reset0 & !PA_Reset_acombout # !Equal_a784) # !Busy_State_ast_reset1 & Busy_State_ast_reset0 & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0CAE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => Busy_State_ast_reset0,
	datac => PA_Reset_acombout,
	datad => Equal_a784,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reset1);

Busy_State_ast_reset2_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reset2 = DFFE(Equal_a784 & Busy_State_ast_reset1 # !Equal_a784 & (Busy_State_ast_reset2), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_reset1,
	datac => Busy_State_ast_reset2,
	datad => Equal_a784,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reset2);

Busy_State_ast_idle_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_idle = DFFE(Select_a1054 & !Select_a1051 & (!Busy_State_ast_reset2 # !Busy_State_a287) # !Select_a1054 & (!Busy_State_ast_reset2 # !Busy_State_a287), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "153F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1054,
	datab => Busy_State_a287,
	datac => Busy_State_ast_reset2,
	datad => Select_a1051,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_idle);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a $ (Busy_State_ast_idle & BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a $ (Busy_State_ast_idle & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a $ (Busy_State_ast_idle & BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a $ (Busy_State_ast_idle & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & (!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datab => Busy_State_ast_idle,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a $ (Busy_State_ast_idle & BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a $ (Busy_State_ast_idle & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ (Busy_State_ast_idle & BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a $ (Busy_State_ast_idle & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a & !BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_idle,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	cout => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

BLK_CNT4_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(CNT_LD_a318) & BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a $ (BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT & Busy_State_ast_idle), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datad => Busy_State_ast_idle,
	cin => BLK_CNT4_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => CNT_LD_a318,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a);

Peak_Time_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(10),
	combout => Peak_Time_a10_a_acombout);

peak_time_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a10_a = DFFE(Peak_Time_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a10_a);

CNT_LD_a341_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a363 = BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & peak_time_reg_ff_adffs_a7_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a $ !peak_time_reg_ff_adffs_a10_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & !peak_time_reg_ff_adffs_a7_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a $ !peak_time_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	datab => peak_time_reg_ff_adffs_a7_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datad => peak_time_reg_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a341,
	cascout => CNT_LD_a363);

CNT_LD_a351_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a351 = (Busy_State_ast_reset1 & !BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !peak_time_reg_ff_adffs_a1_a)) & CASCADE(CNT_LD_a363)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0082",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datac => peak_time_reg_ff_adffs_a1_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	cascin => CNT_LD_a363,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a351);

Peak_Time_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(6),
	combout => Peak_Time_a6_a_acombout);

peak_time_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a6_a = DFFE(Peak_Time_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a6_a);

Peak_Time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(3),
	combout => Peak_Time_a3_a_acombout);

peak_time_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a3_a = DFFE(Peak_Time_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a3_a);

Peak_Time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(2),
	combout => Peak_Time_a2_a_acombout);

peak_time_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a2_a = DFFE(Peak_Time_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a2_a);

CNT_LD_a339_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a362 = peak_time_reg_ff_adffs_a5_a & BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a $ !peak_time_reg_ff_adffs_a2_a) # !peak_time_reg_ff_adffs_a5_a & !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a $ !peak_time_reg_ff_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a5_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	datac => peak_time_reg_ff_adffs_a2_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a339,
	cascout => CNT_LD_a362);

CNT_LD_a350_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a350 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & peak_time_reg_ff_adffs_a3_a & (peak_time_reg_ff_adffs_a6_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !peak_time_reg_ff_adffs_a3_a & 
-- (peak_time_reg_ff_adffs_a6_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a)) & CASCADE(CNT_LD_a362)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => peak_time_reg_ff_adffs_a6_a,
	datac => peak_time_reg_ff_adffs_a3_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	cascin => CNT_LD_a362,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a350);

CNT_LD_a0_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a0 = CNT_LD_a349 & CNT_LD_a351 & CNT_LD_a350

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CNT_LD_a349,
	datab => CNT_LD_a351,
	datac => CNT_LD_a350,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a0);

Pur_Time_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(10),
	combout => Pur_Time_a10_a_acombout);

pur_time_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a10_a = DFFE(Pur_Time_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a10_a);

clock_proc_a755_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a755 = pur_time_reg_ff_adffs_a11_a & BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a $ !pur_time_reg_ff_adffs_a10_a) # !pur_time_reg_ff_adffs_a11_a & !BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a 
-- & (BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a $ !pur_time_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pur_time_reg_ff_adffs_a11_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datac => pur_time_reg_ff_adffs_a10_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a755);

Pur_Time_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(5),
	combout => Pur_Time_a5_a_acombout);

pur_time_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a5_a = DFFE(Pur_Time_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a5_a);

Pur_Time_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(6),
	combout => Pur_Time_a6_a_acombout);

pur_time_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a6_a = DFFE(Pur_Time_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a6_a);

clock_proc_a754_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a754 = BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & pur_time_reg_ff_adffs_a5_a & (pur_time_reg_ff_adffs_a6_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & !pur_time_reg_ff_adffs_a5_a & 
-- (pur_time_reg_ff_adffs_a6_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datab => pur_time_reg_ff_adffs_a5_a,
	datac => pur_time_reg_ff_adffs_a6_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a754);

Pur_Time_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(8),
	combout => Pur_Time_a8_a_acombout);

pur_time_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a8_a = DFFE(Pur_Time_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a8_a);

Pur_Time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(3),
	combout => Pur_Time_a3_a_acombout);

pur_time_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a3_a = DFFE(Pur_Time_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a3_a);

Pur_Time_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(9),
	combout => Pur_Time_a9_a_acombout);

pur_time_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a9_a = DFFE(Pur_Time_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a9_a);

clock_proc_a785_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a825 = BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & pur_time_reg_ff_adffs_a9_a & (pur_time_reg_ff_adffs_a3_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & !pur_time_reg_ff_adffs_a9_a & 
-- (pur_time_reg_ff_adffs_a3_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datab => pur_time_reg_ff_adffs_a3_a,
	datac => pur_time_reg_ff_adffs_a9_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a785,
	cascout => clock_proc_a825);

clock_proc_a801_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a801 = (pur_time_reg_ff_adffs_a0_a & BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & (pur_time_reg_ff_adffs_a8_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a) # !pur_time_reg_ff_adffs_a0_a & !BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & 
-- (pur_time_reg_ff_adffs_a8_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a)) & CASCADE(clock_proc_a825)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pur_time_reg_ff_adffs_a0_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datac => pur_time_reg_ff_adffs_a8_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	cascin => clock_proc_a825,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a801);

Pur_Time_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(4),
	combout => Pur_Time_a4_a_acombout);

pur_time_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a4_a = DFFE(Pur_Time_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a4_a);

Pur_Time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(1),
	combout => Pur_Time_a1_a_acombout);

pur_time_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a1_a = DFFE(Pur_Time_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a1_a);

Pur_Time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(2),
	combout => Pur_Time_a2_a_acombout);

pur_time_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a2_a = DFFE(Pur_Time_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a2_a);

Pur_Time_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pur_Time(7),
	combout => Pur_Time_a7_a_acombout);

pur_time_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- pur_time_reg_ff_adffs_a7_a = DFFE(Pur_Time_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Pur_Time_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pur_time_reg_ff_adffs_a7_a);

clock_proc_a787_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a826 = BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a & pur_time_reg_ff_adffs_a7_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !pur_time_reg_ff_adffs_a2_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a & !pur_time_reg_ff_adffs_a7_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !pur_time_reg_ff_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datac => pur_time_reg_ff_adffs_a2_a,
	datad => pur_time_reg_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a787,
	cascout => clock_proc_a826);

clock_proc_a802_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a802 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & pur_time_reg_ff_adffs_a4_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !pur_time_reg_ff_adffs_a1_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !pur_time_reg_ff_adffs_a4_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !pur_time_reg_ff_adffs_a1_a)) & CASCADE(clock_proc_a826)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datac => pur_time_reg_ff_adffs_a4_a,
	datad => pur_time_reg_ff_adffs_a1_a,
	cascin => clock_proc_a826,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a802);

clock_proc_a753_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a821 = clock_proc_a801 & clock_proc_a802

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => clock_proc_a801,
	datad => clock_proc_a802,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a753,
	cascout => clock_proc_a821);

clock_proc_a797_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a797 = (clock_proc_a755 & Busy_State_ast_busy1 & clock_proc_a754) & CASCADE(clock_proc_a821)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => clock_proc_a755,
	datac => Busy_State_ast_busy1,
	datad => clock_proc_a754,
	cascin => clock_proc_a821,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a797);

Peak_Time_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(4),
	combout => Peak_Time_a4_a_acombout);

peak_time_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a4_a = DFFE(Peak_Time_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a4_a);

CNT_LD_a345_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a365 = BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & peak_time_reg_ff_adffs_a6_a & (peak_time_reg_ff_adffs_a3_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & !peak_time_reg_ff_adffs_a6_a & 
-- (peak_time_reg_ff_adffs_a3_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datab => peak_time_reg_ff_adffs_a6_a,
	datac => peak_time_reg_ff_adffs_a3_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a345,
	cascout => CNT_LD_a365);

CNT_LD_a353_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a353 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & peak_time_reg_ff_adffs_a4_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a $ !peak_time_reg_ff_adffs_a7_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & !peak_time_reg_ff_adffs_a4_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a $ !peak_time_reg_ff_adffs_a7_a)) & CASCADE(CNT_LD_a365)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datac => peak_time_reg_ff_adffs_a7_a,
	datad => peak_time_reg_ff_adffs_a4_a,
	cascin => CNT_LD_a365,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a353);

Disc_In_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(4),
	combout => Disc_In_a4_a_acombout);

OTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OTR,
	combout => OTR_acombout);

Select_a1051_I : apex20ke_lcell
-- Equation(s):
-- Select_a1051 = !PA_Reset_acombout & !RTD_PUR_acombout & (iFDisc_Del # !ifdisc_a116)
-- Select_a1075 = !PA_Reset_acombout & !RTD_PUR_acombout & (iFDisc_Del # !ifdisc_a116)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0203",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iFDisc_Del,
	datab => PA_Reset_acombout,
	datac => RTD_PUR_acombout,
	datad => ifdisc_a116,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1051,
	cascout => Select_a1075);

Select_a1074_I : apex20ke_lcell
-- Equation(s):
-- Select_a1074 = (!Busy_State_ast_idle & OTR_acombout) & CASCADE(Select_a1075)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_idle,
	datad => OTR_acombout,
	cascin => Select_a1075,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1074);

Select_a1067_I : apex20ke_lcell
-- Equation(s):
-- Select_a1067 = Select_a1074 # !Select_a1064 & !PA_Reset_acombout & Disc_In_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF10",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1064,
	datab => PA_Reset_acombout,
	datac => Disc_In_a4_a_acombout,
	datad => Select_a1074,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1067);

Busy_State_ast_reject1_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reject1 = DFFE(Select_a1067 # !Equal_a784 & Busy_State_ast_reject1 & Select_a1051, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF40",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a784,
	datab => Busy_State_ast_reject1,
	datac => Select_a1051,
	datad => Select_a1067,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reject1);

Peak_Time_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(11),
	combout => Peak_Time_a11_a_acombout);

peak_time_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a11_a = DFFE(Peak_Time_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a11_a);

Peak_Time_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(8),
	combout => Peak_Time_a8_a_acombout);

peak_time_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a8_a = DFFE(Peak_Time_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a8_a);

CNT_LD_a347_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a366 = BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a & peak_time_reg_ff_adffs_a11_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a $ !peak_time_reg_ff_adffs_a8_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a & !peak_time_reg_ff_adffs_a11_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a $ !peak_time_reg_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datab => peak_time_reg_ff_adffs_a11_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datad => peak_time_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a347,
	cascout => CNT_LD_a366);

CNT_LD_a354_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a354 = (!BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a & Busy_State_ast_reject1 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !peak_time_reg_ff_adffs_a2_a)) & CASCADE(CNT_LD_a366)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datac => Busy_State_ast_reject1,
	datad => peak_time_reg_ff_adffs_a2_a,
	cascin => CNT_LD_a366,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a354);

CNT_LD_a18_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a18 = CNT_LD_a352 & CNT_LD_a353 & CNT_LD_a354

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CNT_LD_a352,
	datab => CNT_LD_a353,
	datac => CNT_LD_a354,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a18);

Peak_Time_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(5),
	combout => Peak_Time_a5_a_acombout);

peak_time_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a5_a = DFFE(Peak_Time_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a5_a);

clock_proc_a766_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a766 = BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & peak_time_reg_ff_adffs_a5_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a $ !peak_time_reg_ff_adffs_a6_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & !peak_time_reg_ff_adffs_a5_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a $ !peak_time_reg_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datac => peak_time_reg_ff_adffs_a5_a,
	datad => peak_time_reg_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a766);

clock_proc_a767_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a767 = BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a & peak_time_reg_ff_adffs_a10_a & (peak_time_reg_ff_adffs_a11_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a & 
-- !peak_time_reg_ff_adffs_a10_a & (peak_time_reg_ff_adffs_a11_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datab => peak_time_reg_ff_adffs_a10_a,
	datac => peak_time_reg_ff_adffs_a11_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a767);

clock_proc_a791_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a828 = BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a & peak_time_reg_ff_adffs_a2_a & (peak_time_reg_ff_adffs_a7_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a & !peak_time_reg_ff_adffs_a2_a & 
-- (peak_time_reg_ff_adffs_a7_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datab => peak_time_reg_ff_adffs_a7_a,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datad => peak_time_reg_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a791,
	cascout => clock_proc_a828);

clock_proc_a804_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a804 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & peak_time_reg_ff_adffs_a4_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !peak_time_reg_ff_adffs_a1_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !peak_time_reg_ff_adffs_a4_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !peak_time_reg_ff_adffs_a1_a)) & CASCADE(clock_proc_a828)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datac => peak_time_reg_ff_adffs_a1_a,
	datad => peak_time_reg_ff_adffs_a4_a,
	cascin => clock_proc_a828,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a804);

Peak_Time_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Time(9),
	combout => Peak_Time_a9_a_acombout);

peak_time_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- peak_time_reg_ff_adffs_a9_a = DFFE(Peak_Time_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Time_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => peak_time_reg_ff_adffs_a9_a);

clock_proc_a789_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a827 = peak_time_reg_ff_adffs_a3_a & BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ !peak_time_reg_ff_adffs_a9_a) # !peak_time_reg_ff_adffs_a3_a & !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ !peak_time_reg_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a3_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datac => peak_time_reg_ff_adffs_a9_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a789,
	cascout => clock_proc_a827);

clock_proc_a803_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a803 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & peak_time_reg_ff_adffs_a0_a & (peak_time_reg_ff_adffs_a8_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a & !peak_time_reg_ff_adffs_a0_a & 
-- (peak_time_reg_ff_adffs_a8_a $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a)) & CASCADE(clock_proc_a827)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datab => peak_time_reg_ff_adffs_a8_a,
	datac => peak_time_reg_ff_adffs_a0_a,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	cascin => clock_proc_a827,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a803);

clock_proc_a765_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a822 = clock_proc_a804 & (clock_proc_a803)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => clock_proc_a804,
	datad => clock_proc_a803,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a765,
	cascout => clock_proc_a822);

clock_proc_a798_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a798 = (Busy_State_ast_busy2 & clock_proc_a766 & clock_proc_a767) & CASCADE(clock_proc_a822)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => clock_proc_a766,
	datad => clock_proc_a767,
	cascin => clock_proc_a822,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a798);

CNT_LD_a315_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a315 = Busy_State_ast_reset0 & (PA_Reset_acombout # !Select_a1064 & Disc_In_a4_a_acombout) # !Busy_State_ast_reset0 & !Select_a1064 & (Disc_In_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B3A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset0,
	datab => Select_a1064,
	datac => PA_Reset_acombout,
	datad => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a315);

CNT_LD_a316_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a316 = CNT_LD_a18 # clock_proc_a798 # CNT_LD_a315

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CNT_LD_a18,
	datac => clock_proc_a798,
	datad => CNT_LD_a315,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a316);

CNT_LD_a317_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a317 = CNT_LD_a0 # clock_proc_a797 # CNT_LD_a316

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => CNT_LD_a0,
	datac => clock_proc_a797,
	datad => CNT_LD_a316,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a317);

CNT_LD_a305_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a305 = !iFDisc_Del & ifdisc_a116 & (!Select_a1064 # !Select_a1059)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1500",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iFDisc_Del,
	datab => Select_a1059,
	datac => Select_a1064,
	datad => ifdisc_a116,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a305);

CNT_LD_a318_I : apex20ke_lcell
-- Equation(s):
-- CNT_LD_a318 = clock_proc_a793 # clock_proc_a5 # CNT_LD_a317 # CNT_LD_a305

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a793,
	datab => clock_proc_a5,
	datac => CNT_LD_a317,
	datad => CNT_LD_a305,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CNT_LD_a318);

add_a797_I : apex20ke_lcell
-- Equation(s):
-- add_a797 = peak_time_reg_ff_adffs_a1_a $ peak_time_reg_ff_adffs_a3_a
-- add_a799 = CARRY(peak_time_reg_ff_adffs_a1_a & peak_time_reg_ff_adffs_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a1_a,
	datab => peak_time_reg_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a797,
	cout => add_a799);

add_a821_I : apex20ke_lcell
-- Equation(s):
-- add_a821 = peak_time_reg_ff_adffs_a3_a $ peak_time_reg_ff_adffs_a5_a $ !add_a831
-- add_a823 = CARRY(peak_time_reg_ff_adffs_a3_a & (peak_time_reg_ff_adffs_a5_a # !add_a831) # !peak_time_reg_ff_adffs_a3_a & peak_time_reg_ff_adffs_a5_a & !add_a831)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a3_a,
	datab => peak_time_reg_ff_adffs_a5_a,
	cin => add_a831,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a821,
	cout => add_a823);

add_a817_I : apex20ke_lcell
-- Equation(s):
-- add_a817 = peak_time_reg_ff_adffs_a4_a $ peak_time_reg_ff_adffs_a6_a $ add_a823
-- add_a819 = CARRY(peak_time_reg_ff_adffs_a4_a & !peak_time_reg_ff_adffs_a6_a & !add_a823 # !peak_time_reg_ff_adffs_a4_a & (!add_a823 # !peak_time_reg_ff_adffs_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a4_a,
	datab => peak_time_reg_ff_adffs_a6_a,
	cin => add_a823,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a817,
	cout => add_a819);

add_a789_I : apex20ke_lcell
-- Equation(s):
-- add_a789 = peak_time_reg_ff_adffs_a8_a $ peak_time_reg_ff_adffs_a6_a $ add_a807
-- add_a791 = CARRY(peak_time_reg_ff_adffs_a8_a & !peak_time_reg_ff_adffs_a6_a & !add_a807 # !peak_time_reg_ff_adffs_a8_a & (!add_a807 # !peak_time_reg_ff_adffs_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a8_a,
	datab => peak_time_reg_ff_adffs_a6_a,
	cin => add_a807,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a789,
	cout => add_a791);

add_a833_I : apex20ke_lcell
-- Equation(s):
-- add_a833 = peak_time_reg_ff_adffs_a9_a $ peak_time_reg_ff_adffs_a7_a $ !add_a791
-- add_a835 = CARRY(peak_time_reg_ff_adffs_a9_a & (peak_time_reg_ff_adffs_a7_a # !add_a791) # !peak_time_reg_ff_adffs_a9_a & peak_time_reg_ff_adffs_a7_a & !add_a791)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a9_a,
	datab => peak_time_reg_ff_adffs_a7_a,
	cin => add_a791,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a833,
	cout => add_a835);

add_a813_I : apex20ke_lcell
-- Equation(s):
-- add_a813 = peak_time_reg_ff_adffs_a9_a $ peak_time_reg_ff_adffs_a11_a $ !add_a827
-- add_a815 = CARRY(peak_time_reg_ff_adffs_a9_a & (peak_time_reg_ff_adffs_a11_a # !add_a827) # !peak_time_reg_ff_adffs_a9_a & peak_time_reg_ff_adffs_a11_a & !add_a827)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a9_a,
	datab => peak_time_reg_ff_adffs_a11_a,
	cin => add_a827,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a813,
	cout => add_a815);

add_a801_I : apex20ke_lcell
-- Equation(s):
-- add_a801 = peak_time_reg_ff_adffs_a10_a $ add_a815
-- add_a803 = CARRY(!add_a815 # !peak_time_reg_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => peak_time_reg_ff_adffs_a10_a,
	cin => add_a815,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a801,
	cout => add_a803);

clock_proc_a743_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a818 = BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & add_a801 & (add_a817 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & !add_a801 & (add_a817 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datab => add_a817,
	datac => add_a801,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a743,
	cascout => clock_proc_a818);

clock_proc_a794_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a794 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & add_a813 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a $ !add_a797) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & !add_a813 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a $ 
-- !add_a797)) & CASCADE(clock_proc_a818)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datac => add_a797,
	datad => add_a813,
	cascin => clock_proc_a818,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a794);

clock_proc_a9_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a9 = PA_Reset_acombout # BLK_ST_a0 # !BLOCK_OUT_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFAF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datac => BLOCK_OUT_a2_a_areg0,
	datad => BLK_ST_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a9);

BLK_CNT2_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a0_a = DFFE(!clock_proc_a9 & BLOCK_OUT_a2_a_areg0 $ BLK_CNT2_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT2_a0_a_a61 = CARRY(BLOCK_OUT_a2_a_areg0 & BLK_CNT2_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a2_a_areg0,
	datab => BLK_CNT2_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a0_a,
	cout => BLK_CNT2_a0_a_a61);

BLK_CNT2_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a1_a = DFFE(!clock_proc_a9 & BLK_CNT2_a1_a $ BLK_CNT2_a0_a_a61, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT2_a1_a_a70 = CARRY(!BLK_CNT2_a0_a_a61 # !BLK_CNT2_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT2_a1_a,
	cin => BLK_CNT2_a0_a_a61,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a1_a,
	cout => BLK_CNT2_a1_a_a70);

BLK_CNT2_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a2_a = DFFE(!clock_proc_a9 & BLK_CNT2_a2_a $ !BLK_CNT2_a1_a_a70, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT2_a2_a_a67 = CARRY(BLK_CNT2_a2_a & !BLK_CNT2_a1_a_a70)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT2_a2_a,
	cin => BLK_CNT2_a1_a_a70,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a2_a,
	cout => BLK_CNT2_a2_a_a67);

BLK_CNT2_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- BLK_CNT2_a3_a = DFFE(!clock_proc_a9 & BLK_CNT2_a3_a $ BLK_CNT2_a2_a_a67, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- BLK_CNT2_a3_a_a64 = CARRY(!BLK_CNT2_a2_a_a67 # !BLK_CNT2_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT2_a3_a,
	cin => BLK_CNT2_a2_a_a67,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => clock_proc_a9,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLK_CNT2_a3_a,
	cout => BLK_CNT2_a3_a_a64);

Equal_a777_I : apex20ke_lcell
-- Equation(s):
-- Equal_a777 = !BLK_CNT2_a2_a & !BLK_CNT2_a3_a & BLK_CNT2_a0_a & !BLK_CNT2_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT2_a2_a,
	datab => BLK_CNT2_a3_a,
	datac => BLK_CNT2_a0_a,
	datad => BLK_CNT2_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a777);

BLOCK_OUT_a156_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a156 = BLOCK_OUT_a2_a_areg0 & (BLK_CNT2_a4_a # !Equal_a777 # !BLK_CNT2_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT2_a5_a,
	datab => BLK_CNT2_a4_a,
	datac => BLOCK_OUT_a2_a_areg0,
	datad => Equal_a777,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a156);

BLOCK_OUT_a160_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a160 = BLOCK_OUT_a156 & (!clock_proc_a796 # !clock_proc_a794 # !clock_proc_a795)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a795,
	datab => clock_proc_a794,
	datac => clock_proc_a796,
	datad => BLOCK_OUT_a156,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a160);

BLOCK_OUT_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a2_a_areg0 = DFFE(PA_Reset_acombout # BLOCK_OUT_a160 # BLK_ST_a0, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PA_Reset_acombout,
	datac => BLOCK_OUT_a160,
	datad => BLK_ST_a0,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLOCK_OUT_a2_a_areg0);

Disc_Cmp_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_Cmp(3),
	combout => Disc_Cmp_a3_a_acombout);

BLK_ST_a407_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a407 = !BLOCK_OUT_a2_a_areg0 & (Disc_Cmp_a3_a_acombout # !Disc_En_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3303",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLOCK_OUT_a2_a_areg0,
	datac => Disc_En_a3_a_acombout,
	datad => Disc_Cmp_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a407);

BLK_ST_a409_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a409 = Disc_In_a2_a_acombout & (BLK_ST_a407 # BLK_ST_a408 & !Disc_En_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_ST_a408,
	datab => Disc_En_a3_a_acombout,
	datac => Disc_In_a2_a_acombout,
	datad => BLK_ST_a407,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a409);

BLOCK_OUT_a169_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a169 = !Busy_State_ast_busy2 & !Busy_State_ast_reject0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_busy2,
	datad => Busy_State_ast_reject0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a169);

Disc_In_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(3),
	combout => Disc_In_a3_a_acombout);

disc_in3_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a0_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & Disc_in3_dis $ disc_in3_cnt_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a0_a_a82 = CARRY(Disc_in3_dis & disc_in3_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_in3_dis,
	datab => disc_in3_cnt_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a0_a,
	cout => disc_in3_cnt_a0_a_a82);

disc_in3_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a1_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a1_a $ (disc_in3_cnt_a0_a_a82), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a1_a_a79 = CARRY(!disc_in3_cnt_a0_a_a82 # !disc_in3_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a1_a,
	cin => disc_in3_cnt_a0_a_a82,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a1_a,
	cout => disc_in3_cnt_a1_a_a79);

disc_in3_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a3_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a3_a $ disc_in3_cnt_a2_a_a76, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a3_a_a73 = CARRY(!disc_in3_cnt_a2_a_a76 # !disc_in3_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in3_cnt_a3_a,
	cin => disc_in3_cnt_a2_a_a76,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a3_a,
	cout => disc_in3_cnt_a3_a_a73);

disc_in3_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a4_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a4_a $ !disc_in3_cnt_a3_a_a73, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a4_a_a94 = CARRY(disc_in3_cnt_a4_a & !disc_in3_cnt_a3_a_a73)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in3_cnt_a4_a,
	cin => disc_in3_cnt_a3_a_a73,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a4_a,
	cout => disc_in3_cnt_a4_a_a94);

disc_in3_cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a5_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a5_a $ disc_in3_cnt_a4_a_a94, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in3_cnt_a5_a_a88 = CARRY(!disc_in3_cnt_a4_a_a94 # !disc_in3_cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in3_cnt_a5_a,
	cin => disc_in3_cnt_a4_a_a94,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a5_a,
	cout => disc_in3_cnt_a5_a_a88);

disc_in3_cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in3_cnt_a7_a = DFFE(!GLOBAL(Disc_In_a3_a_acombout) & disc_in3_cnt_a7_a $ (disc_in3_cnt_a6_a_a85), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a7_a,
	cin => disc_in3_cnt_a6_a_a85,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in3_cnt_a7_a);

Equal_a817_I : apex20ke_lcell
-- Equation(s):
-- Equal_a842 = !disc_in3_cnt_a2_a & !disc_in3_cnt_a0_a & !disc_in3_cnt_a1_a & !disc_in3_cnt_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a2_a,
	datab => disc_in3_cnt_a0_a,
	datac => disc_in3_cnt_a1_a,
	datad => disc_in3_cnt_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a817,
	cascout => Equal_a842);

Equal_a828_I : apex20ke_lcell
-- Equation(s):
-- Equal_a828 = (disc_in3_cnt_a6_a & disc_in3_cnt_a5_a & !disc_in3_cnt_a7_a & !disc_in3_cnt_a4_a) & CASCADE(Equal_a842)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in3_cnt_a6_a,
	datab => disc_in3_cnt_a5_a,
	datac => disc_in3_cnt_a7_a,
	datad => disc_in3_cnt_a4_a,
	cascin => Equal_a842,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a828);

Disc_in3_dis_aI : apex20ke_lcell
-- Equation(s):
-- Disc_in3_dis = DFFE(Disc_In_a3_a_acombout # !Equal_a828 & Disc_in3_dis, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a828,
	datac => Disc_in3_dis,
	datad => Disc_In_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_in3_dis);

BLK_ST_a410_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a410 = !Disc_in3_dis & Disc_In_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Disc_in3_dis,
	datad => Disc_In_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a410);

Disc_En_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En(1),
	combout => Disc_En_a1_a_acombout);

Disc_En_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_En(2),
	combout => Disc_En_a2_a_acombout);

BLK_ST_a411_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a411 = !Disc_En_a0_a_acombout & !Disc_En_a4_a_acombout & !Disc_En_a1_a_acombout & !Disc_En_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a0_a_acombout,
	datab => Disc_En_a4_a_acombout,
	datac => Disc_En_a1_a_acombout,
	datad => Disc_En_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a411);

BLK_ST_a412_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a412 = BLK_ST_a410 & (Disc_En_a3_a_acombout & BLK_ST_a411 # !BLOCK_OUT_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D050",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a3_a_areg0,
	datab => Disc_En_a3_a_acombout,
	datac => BLK_ST_a410,
	datad => BLK_ST_a411,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a412);

BLOCK_OUT_a170_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a170 = !PA_Reset_acombout & !BLK_ST_a409 & !BLK_ST_a412 & !BLK_ST_a0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => BLK_ST_a409,
	datac => BLK_ST_a412,
	datad => BLK_ST_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a170);

BLOCK_OUT_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a4_a_areg0 = DFFE(BLOCK_OUT_a4_a_areg0 & (BLOCK_OUT_a169 # !Equal_a784) # !BLOCK_OUT_a170, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D0FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a784,
	datab => BLOCK_OUT_a169,
	datac => BLOCK_OUT_a4_a_areg0,
	datad => BLOCK_OUT_a170,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLOCK_OUT_a4_a_areg0);

disc_in4_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a0_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & Disc_in4_dis $ disc_in4_cnt_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a0_a_a118 = CARRY(Disc_in4_dis & disc_in4_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_in4_dis,
	datab => disc_in4_cnt_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a0_a,
	cout => disc_in4_cnt_a0_a_a118);

disc_in4_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a1_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a1_a $ disc_in4_cnt_a0_a_a118, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a1_a_a130 = CARRY(!disc_in4_cnt_a0_a_a118 # !disc_in4_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a1_a,
	cin => disc_in4_cnt_a0_a_a118,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a1_a,
	cout => disc_in4_cnt_a1_a_a130);

disc_in4_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a2_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a2_a $ !disc_in4_cnt_a1_a_a130, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a2_a_a121 = CARRY(disc_in4_cnt_a2_a & !disc_in4_cnt_a1_a_a130)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a2_a,
	cin => disc_in4_cnt_a1_a_a130,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a2_a,
	cout => disc_in4_cnt_a2_a_a121);

disc_in4_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a3_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a3_a $ disc_in4_cnt_a2_a_a121, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a3_a_a115 = CARRY(!disc_in4_cnt_a2_a_a121 # !disc_in4_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a3_a,
	cin => disc_in4_cnt_a2_a_a121,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a3_a,
	cout => disc_in4_cnt_a3_a_a115);

disc_in4_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a4_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a4_a $ (!disc_in4_cnt_a3_a_a115), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a4_a_a109 = CARRY(disc_in4_cnt_a4_a & (!disc_in4_cnt_a3_a_a115))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in4_cnt_a4_a,
	cin => disc_in4_cnt_a3_a_a115,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a4_a,
	cout => disc_in4_cnt_a4_a_a109);

disc_in4_cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a5_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a5_a $ disc_in4_cnt_a4_a_a109, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a5_a_a127 = CARRY(!disc_in4_cnt_a4_a_a109 # !disc_in4_cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a5_a,
	cin => disc_in4_cnt_a4_a_a109,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a5_a,
	cout => disc_in4_cnt_a5_a_a127);

disc_in4_cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a6_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a6_a $ !disc_in4_cnt_a5_a_a127, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a6_a_a112 = CARRY(disc_in4_cnt_a6_a & !disc_in4_cnt_a5_a_a127)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a6_a,
	cin => disc_in4_cnt_a5_a_a127,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a6_a,
	cout => disc_in4_cnt_a6_a_a112);

Equal_a819_I : apex20ke_lcell
-- Equation(s):
-- Equal_a843 = peak_time_reg_ff_adffs_a6_a & disc_in4_cnt_a4_a & (disc_in4_cnt_a6_a $ !peak_time_reg_ff_adffs_a8_a) # !peak_time_reg_ff_adffs_a6_a & !disc_in4_cnt_a4_a & (disc_in4_cnt_a6_a $ !peak_time_reg_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a6_a,
	datab => disc_in4_cnt_a6_a,
	datac => disc_in4_cnt_a4_a,
	datad => peak_time_reg_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a819,
	cascout => Equal_a843);

Equal_a829_I : apex20ke_lcell
-- Equation(s):
-- Equal_a829 = (peak_time_reg_ff_adffs_a2_a & disc_in4_cnt_a0_a & (disc_in4_cnt_a3_a $ !peak_time_reg_ff_adffs_a5_a) # !peak_time_reg_ff_adffs_a2_a & !disc_in4_cnt_a0_a & (disc_in4_cnt_a3_a $ !peak_time_reg_ff_adffs_a5_a)) & CASCADE(Equal_a843)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a2_a,
	datab => disc_in4_cnt_a3_a,
	datac => peak_time_reg_ff_adffs_a5_a,
	datad => disc_in4_cnt_a0_a,
	cascin => Equal_a843,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a829);

disc_in4_cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- disc_in4_cnt_a7_a = DFFE(!GLOBAL(Disc_In_a4_a_acombout) & disc_in4_cnt_a7_a $ disc_in4_cnt_a6_a_a112, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- disc_in4_cnt_a7_a_a136 = CARRY(!disc_in4_cnt_a6_a_a112 # !disc_in4_cnt_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => disc_in4_cnt_a7_a,
	cin => disc_in4_cnt_a6_a_a112,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => disc_in4_cnt_a7_a,
	cout => disc_in4_cnt_a7_a_a136);

Equal_a821_I : apex20ke_lcell
-- Equation(s):
-- Equal_a844 = peak_time_reg_ff_adffs_a10_a & disc_in4_cnt_a8_a & (peak_time_reg_ff_adffs_a4_a $ !disc_in4_cnt_a2_a) # !peak_time_reg_ff_adffs_a10_a & !disc_in4_cnt_a8_a & (peak_time_reg_ff_adffs_a4_a $ !disc_in4_cnt_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a10_a,
	datab => peak_time_reg_ff_adffs_a4_a,
	datac => disc_in4_cnt_a2_a,
	datad => disc_in4_cnt_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a821,
	cascout => Equal_a844);

Equal_a830_I : apex20ke_lcell
-- Equation(s):
-- Equal_a830 = (disc_in4_cnt_a1_a & peak_time_reg_ff_adffs_a3_a & (disc_in4_cnt_a5_a $ !peak_time_reg_ff_adffs_a7_a) # !disc_in4_cnt_a1_a & !peak_time_reg_ff_adffs_a3_a & (disc_in4_cnt_a5_a $ !peak_time_reg_ff_adffs_a7_a)) & CASCADE(Equal_a844)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_in4_cnt_a1_a,
	datab => peak_time_reg_ff_adffs_a3_a,
	datac => disc_in4_cnt_a5_a,
	datad => peak_time_reg_ff_adffs_a7_a,
	cascin => Equal_a844,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a830);

Equal_a797_I : apex20ke_lcell
-- Equation(s):
-- Equal_a797 = Equal_a831 & (Equal_a829 & Equal_a830)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a831,
	datac => Equal_a829,
	datad => Equal_a830,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a797);

Disc_in4_dis_aI : apex20ke_lcell
-- Equation(s):
-- Disc_in4_dis = DFFE(Disc_In_a4_a_acombout # Disc_in4_dis & !Equal_a797, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0FC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Disc_in4_dis,
	datac => Disc_In_a4_a_acombout,
	datad => Equal_a797,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_in4_dis);

ifdisc_a113_I : apex20ke_lcell
-- Equation(s):
-- ifdisc_a113 = !Disc_in4_dis & Disc_In_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Disc_in4_dis,
	datad => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ifdisc_a113);

ifdisc_a114_I : apex20ke_lcell
-- Equation(s):
-- ifdisc_a114 = !Disc_En_a3_a_acombout & !Disc_En_a2_a_acombout & !Disc_En_a0_a_acombout & !Disc_En_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a3_a_acombout,
	datab => Disc_En_a2_a_acombout,
	datac => Disc_En_a0_a_acombout,
	datad => Disc_En_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ifdisc_a114);

ifdisc_a115_I : apex20ke_lcell
-- Equation(s):
-- ifdisc_a115 = ifdisc_a113 & (Disc_En_a4_a_acombout & ifdisc_a114 # !BLOCK_OUT_a4_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B030",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a4_a_acombout,
	datab => BLOCK_OUT_a4_a_areg0,
	datac => ifdisc_a113,
	datad => ifdisc_a114,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ifdisc_a115);

ifdisc_a116_I : apex20ke_lcell
-- Equation(s):
-- ifdisc_a116 = BLK_ST_a412 # BLK_ST_a409 # ifdisc_a115 # BLK_ST_a0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_ST_a412,
	datab => BLK_ST_a409,
	datac => ifdisc_a115,
	datad => BLK_ST_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ifdisc_a116);

Busy_State_ast_busy2_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_busy2 = DFFE(Select_a1051 & (Equal_a784 & Busy_State_ast_busy1 # !Equal_a784 & (Busy_State_ast_busy2)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_busy1,
	datab => Equal_a784,
	datac => Busy_State_ast_busy2,
	datad => Select_a1051,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_busy2);

BLK_CNT4_Max_a1_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1 = Busy_State_ast_busy2 # Busy_State_ast_reset2

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datad => Busy_State_ast_reset2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1);

BLK_CNT4_Max_a0_a_a1808_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a0_a_a1808 = Busy_State_ast_busy1 & (pur_time_reg_ff_adffs_a0_a # peak_time_reg_ff_adffs_a0_a & BLK_CNT4_Max_a1) # !Busy_State_ast_busy1 & peak_time_reg_ff_adffs_a0_a & (BLK_CNT4_Max_a1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_busy1,
	datab => peak_time_reg_ff_adffs_a0_a,
	datac => pur_time_reg_ff_adffs_a0_a,
	datad => BLK_CNT4_Max_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a0_a_a1808);

Busy_State_ast_reject2_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reject2 = DFFE(Select_a1051 & (Equal_a784 & Busy_State_ast_reject1 # !Equal_a784 & (Busy_State_ast_reject2)), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject1,
	datab => Equal_a784,
	datac => Busy_State_ast_reject2,
	datad => Select_a1051,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reject2);

Offset_Time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(0),
	combout => Offset_Time_a0_a_acombout);

offset_time_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_time_reg_ff_adffs_a0_a = DFFE(Offset_Time_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Time_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_time_reg_ff_adffs_a0_a);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277 = offset_inc_reg_ff_adffs_a0_a $ !offset_time_reg_ff_adffs_a0_a
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279 = CARRY(offset_inc_reg_ff_adffs_a0_a # offset_time_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "99EE",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a0_a,
	datab => offset_time_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279);

BLK_CNT4_Max_a0_a_a1806_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a0_a_a1806 = add_a797 & (Busy_State_ast_reject0 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277) # !add_a797 & Busy_State_ast_reject2 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a797,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject0,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a0_a_a1806);

Equal_a781_I : apex20ke_lcell
-- Equation(s):
-- Equal_a781 = BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a $ (BLK_CNT4_Max_a0_a_a1807 # BLK_CNT4_Max_a0_a_a1808 # BLK_CNT4_Max_a0_a_a1806)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F1E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a0_a_a1807,
	datab => BLK_CNT4_Max_a0_a_a1808,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datad => BLK_CNT4_Max_a0_a_a1806,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a781);

BLK_CNT4_Max_a1804_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a1804 = !Busy_State_ast_reset1 & !Busy_State_ast_reset0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Busy_State_ast_reset1,
	datad => Busy_State_ast_reset0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a1804);

BLK_CNT4_Max_a11_a_a1805_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a11_a_a1805 = offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 & (Busy_State_ast_reject2 # peak_time_reg_ff_adffs_a10_a & !BLK_CNT4_Max_a1804) # !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 & 
-- peak_time_reg_ff_adffs_a10_a & (!BLK_CNT4_Max_a1804)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0EC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273,
	datab => peak_time_reg_ff_adffs_a10_a,
	datac => Busy_State_ast_reject2,
	datad => BLK_CNT4_Max_a1804,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a11_a_a1805);

BLK_CNT4_Max_a11_a_a1803_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a11_a_a1803 = BLK_CNT4_Max_a11_a_a1802 # peak_time_reg_ff_adffs_a11_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EEEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a11_a_a1802,
	datab => peak_time_reg_ff_adffs_a11_a,
	datac => Busy_State_ast_reset2,
	datad => Busy_State_ast_busy2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a11_a_a1803);

Equal_a780_I : apex20ke_lcell
-- Equation(s):
-- Equal_a780 = BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a $ (BLK_CNT4_Max_a11_a_a1805 # BLK_CNT4_Max_a11_a_a1803)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F3C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => BLK_CNT4_Max_a11_a_a1805,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datad => BLK_CNT4_Max_a11_a_a1803,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a780);

Equal_a782_I : apex20ke_lcell
-- Equation(s):
-- Equal_a782 = !Equal_a781 & !Equal_a780 & (BLK_CNT4_Max_a5_a_a1801 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0021",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a5_a_a1801,
	datab => Equal_a781,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datad => Equal_a780,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a782);

Offset_Time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(2),
	combout => Offset_Time_a2_a_acombout);

offset_time_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_time_reg_ff_adffs_a2_a = DFFE(Offset_Time_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Time_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_time_reg_ff_adffs_a2_a);

Offset_Time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(1),
	combout => Offset_Time_a1_a_acombout);

offset_time_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_time_reg_ff_adffs_a1_a = DFFE(Offset_Time_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Time_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_time_reg_ff_adffs_a1_a);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305 = offset_inc_reg_ff_adffs_a1_a $ offset_time_reg_ff_adffs_a1_a $ offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307 = CARRY(offset_inc_reg_ff_adffs_a1_a & !offset_time_reg_ff_adffs_a1_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279 # !offset_inc_reg_ff_adffs_a1_a & 
-- (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279 # !offset_time_reg_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a1_a,
	datab => offset_time_reg_ff_adffs_a1_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a279,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301 = offset_inc_reg_ff_adffs_a2_a $ offset_time_reg_ff_adffs_a2_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303 = CARRY(offset_inc_reg_ff_adffs_a2_a & (offset_time_reg_ff_adffs_a2_a # !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307) # !offset_inc_reg_ff_adffs_a2_a & offset_time_reg_ff_adffs_a2_a 
-- & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a2_a,
	datab => offset_time_reg_ff_adffs_a2_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a307,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303);

BLK_CNT4_Max_a2_a_a1825_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1825 = Busy_State_ast_reject0 & (add_a821 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301) # !Busy_State_ast_reject0 & Busy_State_ast_reject2 & 
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject0,
	datab => Busy_State_ast_reject2,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301,
	datad => add_a821,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1825);

BLK_CNT4_Max_a2_a_a1828_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1828 = BLK_CNT4_Max_a2_a_a1827 # peak_time_reg_ff_adffs_a3_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FABA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a2_a_a1827,
	datab => Busy_State_ast_idle,
	datac => peak_time_reg_ff_adffs_a3_a,
	datad => Busy_State_ast_reject1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1828);

BLK_CNT4_Max_a2_a_a1826_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1826 = peak_time_reg_ff_adffs_a2_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A8A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a2_a,
	datab => Busy_State_ast_reset2,
	datac => Busy_State_ast_busy2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1826);

BLK_CNT4_Max_a2_a_a1830_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a2_a_a1830 = BLK_CNT4_Max_a2_a_a1829 # BLK_CNT4_Max_a2_a_a1825 # BLK_CNT4_Max_a2_a_a1828 # BLK_CNT4_Max_a2_a_a1826

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a2_a_a1829,
	datab => BLK_CNT4_Max_a2_a_a1825,
	datac => BLK_CNT4_Max_a2_a_a1828,
	datad => BLK_CNT4_Max_a2_a_a1826,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a2_a_a1830);

BLK_CNT4_Max_a6_a_a1820_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1820 = peak_time_reg_ff_adffs_a6_a & (Busy_State_ast_reset2 # Busy_State_ast_busy2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A8A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a6_a,
	datab => Busy_State_ast_reset2,
	datac => Busy_State_ast_busy2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1820);

Offset_Inc_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(5),
	combout => Offset_Inc_a5_a_acombout);

offset_inc_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a5_a = DFFE(Offset_Inc_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a5_a);

Offset_Inc_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(4),
	combout => Offset_Inc_a4_a_acombout);

offset_inc_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a4_a = DFFE(Offset_Inc_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a4_a);

Offset_Inc_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(3),
	combout => Offset_Inc_a3_a_acombout);

offset_inc_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a3_a = DFFE(Offset_Inc_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a3_a);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309 = offset_time_reg_ff_adffs_a3_a $ offset_inc_reg_ff_adffs_a3_a $ offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311 = CARRY(offset_time_reg_ff_adffs_a3_a & !offset_inc_reg_ff_adffs_a3_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303 # !offset_time_reg_ff_adffs_a3_a & 
-- (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303 # !offset_inc_reg_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_time_reg_ff_adffs_a3_a,
	datab => offset_inc_reg_ff_adffs_a3_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a303,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 = offset_inc_reg_ff_adffs_a4_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287 = CARRY(offset_inc_reg_ff_adffs_a4_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_inc_reg_ff_adffs_a4_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a311,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269 = offset_inc_reg_ff_adffs_a5_a $ offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271 = CARRY(!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287 # !offset_inc_reg_ff_adffs_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_inc_reg_ff_adffs_a5_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a287,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297 = offset_inc_reg_ff_adffs_a6_a $ (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271)
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299 = CARRY(offset_inc_reg_ff_adffs_a6_a & (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a6_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a271,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299);

BLK_CNT4_Max_a6_a_a1819_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1819 = add_a833 & (Busy_State_ast_reject0 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297) # !add_a833 & Busy_State_ast_reject2 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a833,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject0,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1819);

BLK_CNT4_Max_a6_a_a1823_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1823 = Busy_State_ast_busy1 & (pur_time_reg_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy1,
	datad => pur_time_reg_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1823);

BLK_CNT4_Max_a6_a_a1824_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a6_a_a1824 = BLK_CNT4_Max_a6_a_a1822 # BLK_CNT4_Max_a6_a_a1820 # BLK_CNT4_Max_a6_a_a1819 # BLK_CNT4_Max_a6_a_a1823

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a6_a_a1822,
	datab => BLK_CNT4_Max_a6_a_a1820,
	datac => BLK_CNT4_Max_a6_a_a1819,
	datad => BLK_CNT4_Max_a6_a_a1823,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a6_a_a1824);

Offset_Inc_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(8),
	combout => Offset_Inc_a8_a_acombout);

offset_inc_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a8_a = DFFE(Offset_Inc_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a8_a);

Offset_Inc_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(7),
	combout => Offset_Inc_a7_a_acombout);

offset_inc_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a7_a = DFFE(Offset_Inc_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a7_a);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313 = offset_inc_reg_ff_adffs_a7_a $ offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315 = CARRY(!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299 # !offset_inc_reg_ff_adffs_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_inc_reg_ff_adffs_a7_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a299,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293 = offset_inc_reg_ff_adffs_a8_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295 = CARRY(offset_inc_reg_ff_adffs_a8_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_inc_reg_ff_adffs_a8_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a315,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295);

BLK_CNT4_Max_a8_a_a1813_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1813 = add_a813 & (Busy_State_ast_reject0 # Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293) # !add_a813 & (Busy_State_ast_reject2 & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a813,
	datab => Busy_State_ast_reject0,
	datac => Busy_State_ast_reject2,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1813);

BLK_CNT4_Max_a8_a_a1817_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1817 = pur_time_reg_ff_adffs_a8_a & Busy_State_ast_busy1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => pur_time_reg_ff_adffs_a8_a,
	datad => Busy_State_ast_busy1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1817);

BLK_CNT4_Max_a8_a_a1814_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1814 = peak_time_reg_ff_adffs_a8_a & (Busy_State_ast_busy2 # Busy_State_ast_reset2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_busy2,
	datac => peak_time_reg_ff_adffs_a8_a,
	datad => Busy_State_ast_reset2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1814);

BLK_CNT4_Max_a8_a_a1818_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a8_a_a1818 = BLK_CNT4_Max_a8_a_a1816 # BLK_CNT4_Max_a8_a_a1813 # BLK_CNT4_Max_a8_a_a1817 # BLK_CNT4_Max_a8_a_a1814

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a8_a_a1816,
	datab => BLK_CNT4_Max_a8_a_a1813,
	datac => BLK_CNT4_Max_a8_a_a1817,
	datad => BLK_CNT4_Max_a8_a_a1814,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a8_a_a1818);

Equal_a801_I : apex20ke_lcell
-- Equation(s):
-- Equal_a840 = BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & BLK_CNT4_Max_a6_a_a1824 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a $ !BLK_CNT4_Max_a8_a_a1818) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & !BLK_CNT4_Max_a6_a_a1824 & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a $ !BLK_CNT4_Max_a8_a_a1818)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datab => BLK_CNT4_Max_a6_a_a1824,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	datad => BLK_CNT4_Max_a8_a_a1818,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a801,
	cascout => Equal_a840);

Equal_a826_I : apex20ke_lcell
-- Equation(s):
-- Equal_a826 = (BLK_CNT4_Max_a1_a_a1836 & BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a & (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !BLK_CNT4_Max_a2_a_a1830) # !BLK_CNT4_Max_a1_a_a1836 & !BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a & 
-- (BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a $ !BLK_CNT4_Max_a2_a_a1830)) & CASCADE(Equal_a840)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a1_a_a1836,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datac => BLK_CNT4_Max_a2_a_a1830,
	datad => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	cascin => Equal_a840,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a826);

BLK_CNT4_Max_a9_a_a1810_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a9_a_a1810 = peak_time_reg_ff_adffs_a8_a & (Busy_State_ast_reset1 # Busy_State_ast_reset0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A8A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a8_a,
	datab => Busy_State_ast_reset1,
	datac => Busy_State_ast_reset0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a9_a_a1810);

BLK_CNT4_Max_a9_a_a1811_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a9_a_a1811 = BLK_CNT4_Max_a9_a_a1810 # peak_time_reg_ff_adffs_a10_a & (Busy_State_ast_reject1 # !Busy_State_ast_idle)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFA2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => peak_time_reg_ff_adffs_a10_a,
	datab => Busy_State_ast_idle,
	datac => Busy_State_ast_reject1,
	datad => BLK_CNT4_Max_a9_a_a1810,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a9_a_a1811);

BLK_CNT4_Max_a9_a_a1812_I : apex20ke_lcell
-- Equation(s):
-- BLK_CNT4_Max_a9_a_a1812 = BLK_CNT4_Max_a9_a_a1809 # BLK_CNT4_Max_a9_a_a1811 # peak_time_reg_ff_adffs_a9_a & BLK_CNT4_Max_a1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_Max_a9_a_a1809,
	datab => BLK_CNT4_Max_a9_a_a1811,
	datac => peak_time_reg_ff_adffs_a9_a,
	datad => BLK_CNT4_Max_a1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_CNT4_Max_a9_a_a1812);

Equal_a783_I : apex20ke_lcell
-- Equation(s):
-- Equal_a783 = BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a $ (BLK_CNT4_Max_a9_a_a1812 # pur_time_reg_ff_adffs_a9_a & Busy_State_ast_busy1)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "336C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => pur_time_reg_ff_adffs_a9_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datac => Busy_State_ast_busy1,
	datad => BLK_CNT4_Max_a9_a_a1812,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a783);

Equal_a784_I : apex20ke_lcell
-- Equation(s):
-- Equal_a784 = Equal_a827 & Equal_a782 & Equal_a826 & !Equal_a783

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a827,
	datab => Equal_a782,
	datac => Equal_a826,
	datad => Equal_a783,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a784);

Busy_State_ast_busy1_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_busy1 = DFFE(Select_a1051 & (Equal_a784 & (Busy_State_ast_reject2) # !Equal_a784 & Busy_State_ast_busy1), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_busy1,
	datab => Equal_a784,
	datac => Busy_State_ast_reject2,
	datad => Select_a1051,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_busy1);

Select_a1059_I : apex20ke_lcell
-- Equation(s):
-- Select_a1059 = !Busy_State_ast_reject1 & !Busy_State_ast_busy1 & !Busy_State_ast_busy2 & !Busy_State_ast_reject2

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reject1,
	datab => Busy_State_ast_busy1,
	datac => Busy_State_ast_busy2,
	datad => Busy_State_ast_reject2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1059);

Select_a1060_I : apex20ke_lcell
-- Equation(s):
-- Select_a1060 = !PA_Reset_acombout & (Busy_State_ast_reject0 & !Disc_In_a4_a_acombout # !Select_a1059)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1151",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Select_a1059,
	datac => Busy_State_ast_reject0,
	datad => Disc_In_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Select_a1060);

Busy_State_a289_I : apex20ke_lcell
-- Equation(s):
-- Busy_State_a289 = !RTD_PUR_acombout & (iFDisc_Del # !ifdisc_a116)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iFDisc_Del,
	datac => RTD_PUR_acombout,
	datad => ifdisc_a116,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Busy_State_a289);

Busy_State_ast_reject0_aI : apex20ke_lcell
-- Equation(s):
-- Busy_State_ast_reject0 = DFFE(Select_a1060 & (Busy_State_a288 # !Busy_State_a289) # !Select_a1060 & Select_a1061 & (!Busy_State_a289), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0FA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Select_a1061,
	datab => Busy_State_a288,
	datac => Select_a1060,
	datad => Busy_State_a289,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Busy_State_ast_reject0);

add_a793_I : apex20ke_lcell
-- Equation(s):
-- add_a793 = add_a811

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => add_a811,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a793);

Equal_a798_I : apex20ke_lcell
-- Equation(s):
-- Equal_a798 = BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a $ add_a793

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datad => add_a793,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a798);

clock_proc_a747_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a820 = BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & add_a789 & (add_a833 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a) # !BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a & !add_a789 & (add_a833 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datab => add_a833,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datad => add_a789,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a747,
	cascout => clock_proc_a820);

clock_proc_a796_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a796 = (Busy_State_ast_reject0 & !Equal_a798 & (add_a809 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a)) & CASCADE(clock_proc_a820)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0084",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => add_a809,
	datab => Busy_State_ast_reject0,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datad => Equal_a798,
	cascin => clock_proc_a820,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a796);

BLOCK_OUT_a154_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a154 = BLOCK_OUT_a150 & (!clock_proc_a794 # !clock_proc_a796 # !clock_proc_a795)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4CCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a795,
	datab => BLOCK_OUT_a150,
	datac => clock_proc_a796,
	datad => clock_proc_a794,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a154);

BLOCK_OUT_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a1_a_areg0 = DFFE(disc_start_a0_a_a480 # BLOCK_OUT_a154 # PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => disc_start_a0_a_a480,
	datab => BLOCK_OUT_a154,
	datac => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLOCK_OUT_a1_a_areg0);

Disc_In_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In(1),
	combout => Disc_In_a1_a_acombout);

disc_start_a5_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a5 = !BLOCK_OUT_a1_a_areg0 & Disc_In_a1_a_acombout
-- disc_start_a490 = !BLOCK_OUT_a1_a_areg0 & Disc_In_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BLOCK_OUT_a1_a_areg0,
	datad => Disc_In_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a5,
	cascout => disc_start_a490);

disc_start_a4_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a4 = !BLOCK_OUT_a1_a_areg0 & (Disc_In_a1_a_acombout & Disc_Cmp_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a1_a_areg0,
	datac => Disc_In_a1_a_acombout,
	datad => Disc_Cmp_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a4);

Disc_Cmp_E_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Cmp_E_Vec_a0_a = DFFE(Disc_Cmp_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Cmp_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Cmp_E_Vec_a0_a);

Disc_Cmp_E_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Cmp_E_Vec_a1_a = DFFE(Disc_Cmp_E_Vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Cmp_E_Vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Cmp_E_Vec_a1_a);

Disc_Cmp_E_Vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Cmp_E_Vec_a2_a = DFFE(Disc_Cmp_E_Vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Cmp_E_Vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Cmp_E_Vec_a2_a);

Disc_Cmp_E_Vec_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Cmp_E_Vec_a3_a = DFFE(Disc_Cmp_E_Vec_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Cmp_E_Vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Cmp_E_Vec_a3_a);

Disc_Cmp_E_Vec_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_Cmp_E_Vec_a4_a = DFFE(Disc_Cmp_E_Vec_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_Cmp_E_Vec_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_Cmp_E_Vec_a4_a);

disc_start_a1_a_a489_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a1_a_a489 = (Disc_En_a3_a_acombout # Disc_Cmp_E_Vec_a4_a & Disc_Cmp_a4_a_acombout) & CASCADE(disc_start_a490)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Disc_Cmp_E_Vec_a4_a,
	datac => Disc_Cmp_a4_a_acombout,
	datad => Disc_En_a3_a_acombout,
	cascin => disc_start_a490,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a1_a_a489);

disc_start_a1_a_a482_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a1_a_a482 = Disc_In_a1_a_acombout & (!Disc_En_a0_a_acombout & !Disc_En_a3_a_acombout # !BLOCK_OUT_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5070",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLOCK_OUT_a1_a_areg0,
	datab => Disc_En_a0_a_acombout,
	datac => Disc_In_a1_a_acombout,
	datad => Disc_En_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a1_a_a482);

disc_start_a1_a_a483_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a1_a_a483 = Disc_En_a4_a_acombout & (Disc_En_a2_a_acombout) # !Disc_En_a4_a_acombout & (Disc_En_a2_a_acombout & disc_start_a5 # !Disc_En_a2_a_acombout & (disc_start_a1_a_a482))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E5E0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a4_a_acombout,
	datab => disc_start_a5,
	datac => Disc_En_a2_a_acombout,
	datad => disc_start_a1_a_a482,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a1_a_a483);

disc_start_a1_a_a484_I : apex20ke_lcell
-- Equation(s):
-- disc_start_a1_a_a484 = Disc_En_a4_a_acombout & (disc_start_a1_a_a483 & disc_start_a4 # !disc_start_a1_a_a483 & (disc_start_a1_a_a489)) # !Disc_En_a4_a_acombout & (disc_start_a1_a_a483)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a4_a_acombout,
	datab => disc_start_a4,
	datac => disc_start_a1_a_a489,
	datad => disc_start_a1_a_a483,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => disc_start_a1_a_a484);

BLK_ST_a0_I : apex20ke_lcell
-- Equation(s):
-- BLK_ST_a0 = disc_start_a0_a_a480 # Disc_En_a1_a_acombout & (disc_start_a1_a_a484) # !Disc_En_a1_a_acombout & disc_start_a5

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEF4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_En_a1_a_acombout,
	datab => disc_start_a5,
	datac => disc_start_a0_a_a480,
	datad => disc_start_a1_a_a484,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLK_ST_a0);

iFDisc_Del_aI : apex20ke_lcell
-- Equation(s):
-- iFDisc_Del = DFFE(ifdisc_a115 # BLK_ST_a0 # BLK_ST_a412 # BLK_ST_a409, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ifdisc_a115,
	datab => BLK_ST_a0,
	datac => BLK_ST_a412,
	datad => BLK_ST_a409,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iFDisc_Del);

Pur_a71_I : apex20ke_lcell
-- Equation(s):
-- Pur_a71 = Busy_State_ast_reject2 # Busy_State_ast_reject0 # Busy_State_ast_reject1

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject0,
	datad => Busy_State_ast_reject1,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Pur_a71);

Pur_areg0_I : apex20ke_lcell
-- Equation(s):
-- Pur_areg0 = DFFE(Pur_a70 # !iFDisc_Del & Pur_a71 & ifdisc_a116, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BAAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Pur_a70,
	datab => iFDisc_Del,
	datac => Pur_a71,
	datad => ifdisc_a116,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Pur_areg0);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281 = offset_inc_reg_ff_adffs_a9_a $ (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295)
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283 = CARRY(!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295 # !offset_inc_reg_ff_adffs_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a9_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a295,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 = offset_inc_reg_ff_adffs_a10_a $ (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283)
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a291 = CARRY(offset_inc_reg_ff_adffs_a10_a & (!offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_inc_reg_ff_adffs_a10_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a283,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289,
	cout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a291);

Offset_Inc_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Inc(11),
	combout => Offset_Inc_a11_a_acombout);

offset_inc_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- offset_inc_reg_ff_adffs_a11_a = DFFE(Offset_Inc_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Offset_Inc_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_inc_reg_ff_adffs_a11_a);

offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273_I : apex20ke_lcell
-- Equation(s):
-- offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 = offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a291 $ offset_inc_reg_ff_adffs_a11_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => offset_inc_reg_ff_adffs_a11_a,
	cin => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a291,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273);

clock_proc_a741_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a741 = BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a11_a,
	datab => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a289,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a10_a,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a273,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a741);

clock_proc_a740_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a740 = BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "9009",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a6_a,
	datab => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a297,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a5_a,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a269,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a740);

clock_proc_a777_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a824 = BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301 & (offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313 $ !BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a2_a,
	datab => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a313,
	datac => BLK_CNT4_CNTR_awysi_counter_asload_path_a7_a,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a301,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a777,
	cascout => clock_proc_a824);

clock_proc_a800_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a800 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305)) & CASCADE(clock_proc_a824)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a4_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a1_a,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a285,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a305,
	cascin => clock_proc_a824,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a800);

clock_proc_a775_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a823 = BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a9_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a3_a,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a309,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a775,
	cascout => clock_proc_a823);

clock_proc_a799_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a799 = (BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277) # 
-- !BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a & !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293 & (BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a $ !offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277)) & CASCADE(clock_proc_a823)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_CNT4_CNTR_awysi_counter_asload_path_a8_a,
	datab => BLK_CNT4_CNTR_awysi_counter_asload_path_a0_a,
	datac => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a277,
	datad => offset_to_add_sub_aadder_aresult_node_acs_buffer_a0_a_a293,
	cascin => clock_proc_a823,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a799);

clock_proc_a739_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a817 = clock_proc_a800 & clock_proc_a799

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => clock_proc_a800,
	datad => clock_proc_a799,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a739,
	cascout => clock_proc_a817);

clock_proc_a793_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a793 = (clock_proc_a741 & Busy_State_ast_reject2 & clock_proc_a740) & CASCADE(clock_proc_a817)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => clock_proc_a741,
	datac => Busy_State_ast_reject2,
	datad => clock_proc_a740,
	cascin => clock_proc_a817,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a793);

Peak_Done_areg0_I : apex20ke_lcell
-- Equation(s):
-- Peak_Done_areg0 = DFFE(!Pur_areg0 & clock_proc_a793, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Pur_areg0,
	datad => clock_proc_a793,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Peak_Done_areg0);

BLOCK_OUT_a163_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a163 = !Equal_a825 & (BLOCK_OUT_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a825,
	datad => BLOCK_OUT_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a163);

BLOCK_OUT_a167_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a167 = BLOCK_OUT_a163 & (!clock_proc_a796 # !clock_proc_a794 # !clock_proc_a795)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a795,
	datab => clock_proc_a794,
	datac => clock_proc_a796,
	datad => BLOCK_OUT_a163,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a167);

BLOCK_OUT_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a3_a_areg0 = DFFE(BLK_ST_a409 # PA_Reset_acombout # BLOCK_OUT_a167 # BLK_ST_a0, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BLK_ST_a409,
	datab => PA_Reset_acombout,
	datac => BLOCK_OUT_a167,
	datad => BLK_ST_a0,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLOCK_OUT_a3_a_areg0);

BLOCK_OUT_a172_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a172 = !Busy_State_ast_reject0 & (!Busy_State_ast_reject2)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Busy_State_ast_reject0,
	datad => Busy_State_ast_reject2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => BLOCK_OUT_a172);

BLOCK_OUT_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- BLOCK_OUT_a5_a_areg0 = DFFE(BLOCK_OUT_a5_a_areg0 & (BLOCK_OUT_a172 # !Equal_a784) # !BLOCK_OUT_a170, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C4FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a784,
	datab => BLOCK_OUT_a5_a_areg0,
	datac => BLOCK_OUT_a172,
	datad => BLOCK_OUT_a170,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BLOCK_OUT_a5_a_areg0);

FDISC_areg0_I : apex20ke_lcell
-- Equation(s):
-- FDISC_areg0 = DFFE(!iFDisc_Del & (ifdisc_a116), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => iFDisc_Del,
	datad => ifdisc_a116,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FDISC_areg0);

PA_Busy_areg0_I : apex20ke_lcell
-- Equation(s):
-- PA_Busy_areg0 = DFFE(PA_Reset_acombout # PA_Busy_areg0 & Busy_State_ast_idle, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PA_Busy_areg0,
	datac => Busy_State_ast_idle,
	datad => PA_Reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PA_Busy_areg0);

reduce_or_a19_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a19 = Busy_State_ast_reset1 # Busy_State_ast_reject2 # Busy_State_ast_busy2 # Busy_State_ast_reject0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_busy2,
	datad => Busy_State_ast_reject0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a19);

reduce_or_a20_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a20 = Busy_State_ast_reset1 # Busy_State_ast_reject2 # Busy_State_ast_reject1 # Busy_State_ast_reset0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_reset1,
	datab => Busy_State_ast_reject2,
	datac => Busy_State_ast_reject1,
	datad => Busy_State_ast_reset0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a20);

reduce_or_a21_I : apex20ke_lcell
-- Equation(s):
-- reduce_or_a21 = Busy_State_ast_busy2 # Busy_State_ast_busy1 # Busy_State_ast_reset1 # Busy_State_ast_reset0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Busy_State_ast_busy2,
	datab => Busy_State_ast_busy1,
	datac => Busy_State_ast_reset1,
	datad => Busy_State_ast_reset0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_or_a21);

Disc_Cmp_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_Cmp(0));

Disc_Cmp_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_Cmp(1));

Peak_Done_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Peak_Done_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Peak_Done);

BLOCK_OUT_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BLOCK_OUT_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLOCK_OUT(1));

BLOCK_OUT_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BLOCK_OUT_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLOCK_OUT(2));

BLOCK_OUT_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BLOCK_OUT_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLOCK_OUT(3));

BLOCK_OUT_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BLOCK_OUT_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLOCK_OUT(4));

BLOCK_OUT_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => BLOCK_OUT_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_BLOCK_OUT(5));

FDISC_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => FDISC_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FDISC);

Pur_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Pur_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Pur);

PBusy_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Busy_State_ast_idle,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PBusy);

PA_Busy_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => PA_Busy_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PA_Busy);

Busy_Str_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reduce_or_a19,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Busy_Str(0));

Busy_Str_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reduce_or_a20,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Busy_Str(1));

Busy_Str_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reduce_or_a21,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Busy_Str(2));

Busy_Str_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Busy_State_ast_reset2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Busy_Str(3));
END structure;


