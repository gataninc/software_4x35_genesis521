-------------------------------------------------------------------------------------
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
-------------------------------------------------------------------------------------


------------------------------------------------------------------------------------
entity disc_proc is
	port(
		clk		: in		std_logic;
		delay	: in		integer range 0 to 4095;
		disc_in	: out	std_logic_vector( 4 downto 0 ) );
end disc_proc;
-------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------
architecture test_bench of disc_proc is
	signal idisc_in 	: std_logic_vector( 4 downto 0 );
	signal cnt		: integer range 0 to 7 := 0;

begin
	disc_in	<= idisc_in;
		
	------------------------------------------------------------------------------
	SH_Disc_Proc : process
	begin
		iDisc_In(0) <= '0';
		wait for delay * 1 us;
		forever_loop : loop
			wait until(( CLK'Event ) and ( CLK = '1' ));
			iDisc_In(0) <= '1';
			wait until(( CLK'Event ) and ( CLK = '1' ));
			iDisc_In(0) <= '0';
			wait for 100 us;
			if( cnt < 7 )
				then cnt 	<= cnt + 1;
			end if;
		end loop;
	end process;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	H_Disc_Proc : process
	begin
		iDisc_In(1) <= '0';
		wait until (( cnt'event ) and ( cnt > 0 ));
		wait until(( iDisc_In(0)'Event ) and ( iDisc_In(0) = '1' ));
		wait for 250 ns;
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(1) <= '1'; 
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(1) <= '0'; 
	end process;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	M_Disc_Proc : process
	begin
		iDisc_In(2) <= '0';
		wait until (( cnt'event ) and ( cnt > 1 ));
		wait until(( iDisc_In(1)'Event ) and ( iDisc_In(1) = '1' ));
		wait for 350 ns;
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(2) <= '1'; 
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(2) <= '0';
	end process;
	
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	L_Disc_Proc : process
	begin
		iDisc_In(3) <= '0';
		wait until(( cnt'event ) and ( cnt > 2 ));
		wait until(( iDisc_In(2)'Event ) and ( iDisc_In(2) = '1' ));
		wait for 5650 ns;
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(3) <= '1';
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(3) <= '0';
	end process;
	-------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	E_Disc_Proc : process
	begin
		iDisc_In(4) <= '0';
		wait until(( cnt'event ) and ( cnt > 3 ));
		wait until(( iDisc_In(3)'Event ) and ( iDisc_In(3) = '1' ));
		wait for 6350 ns;
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(4) <= '1';
		wait until(( CLK'Event ) and ( CLK = '1' ));
		iDisc_In(4) <= '0';
	end process;
	
	-------------------------------------------------------------------------------
end test_bench;
