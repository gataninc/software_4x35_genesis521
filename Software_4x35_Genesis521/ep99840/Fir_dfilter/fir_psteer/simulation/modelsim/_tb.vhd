---------------------------------------

-------------------------------------------------------------------------------
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_Bench of tb is
	constant TC_SEL 		: integer := 6;
	signal IMR 			: std_logic := '1';
	signal CLK			: std_logic := '0';
	signal PA_Reset		: std_logic;
	signal iDisc_In		: std_logic_vector(  4 downto 0 );
	signal Disc_In			: std_logic_vector(  4 downto 0 );
	signal Disc_Cmp		: std_logic_vector(  4 downto 0 );
	signal Offset_Time		: std_logic_vector(  3 downto 0 );	-- Offset TIme
	signal Peak_Time		: std_logic_Vector( 11 downto 0 );	-- Peak time
	signal TImeConst		: std_logic_vector(  3 downto 0 );
	signal Peak_Done		: std_logic;
	signal Block_Out		: std_logic_Vector( 5 downto 1 );
	signal fdisc			: std_logic;
	signal pur			: std_logic;
	signal Busy_Str		: std_logic_Vector( 3 downto 0 );
	signal BLK_CNT4_MAx		: std_logic_Vector( 11 downto 0 );
	signal BLK_TC			: std_logic_vector(  4 downto 1 );
	signal Pur_time		: std_logic_vector( 11 downto 0 );
	signal High_Inh		: std_logic := '0';
	signal pbusy			: std_logic;
	signal disc_ina		: std_logic_Vector( 4 downto 0 );
	signal disc_inb		: std_logic_Vector( 4 downto 0 );
	signal Qual			: std_logic_Vector( 3 downto 0 );
	signal Qual_Abort		: std_logic;

	-------------------------------------------------------------------------------
	component disc_proc 
	port(
		clk		: in		std_logic;
		delay	: in		integer range 0 to 4095;
		disc_in	: out	std_logic_vector( 4 downto 0 ) );
	end component disc_proc;
     -------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	component Fir_PSteer 
		port( 
			clk            : in      std_logic;			     	-- Master Clock Input
		     imr            : in      std_logic;                    	-- Master Reset
			PA_Reset		: in		std_logic;					-- PreAmp Reset
			SDD_PUR		: in		std_Logic;
		     Disc_In        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
		     Disc_En        : in		std_logic_Vector(  4 downto 0 );	-- Discriminator Inputs
			Disc_Cmp		: in		std_logic_Vector(  4 downto 0 );
			Offset_Time	: in		std_logic_vector(  3 downto 0 );	-- Offset TIme
			Offset_Inc	: in		std_logic_vector( 11 downto 0 );
			Peak_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
			Pur_Time		: in		std_logic_Vector( 11 downto 0 );	-- Peak time
			Peak_Done		: out	std_logic;
			BLOCK_OUT		: out	std_logic_Vector(  5 downto 1 );	-- BLocking Pulses
			FDISC		: out	std_Logic;					-- Fast Disc
			Pur			: out	std_logic;					-- Pile-Up Reject
			PBusy		: out	std_logic;
			PA_Busy		: out	std_logic;
			Busy_Str		: out	std_logic_Vector( 3 downto 0 ));
	end component Fir_PSteer;
	-------------------------------------------------------------------------------
	signal SDD_PUR 	: std_logic;
	signal disc_en		: std_logic_vector( 4 downto 0 );
	signal offset_inc	: std_logic_Vector( 11 downto 0 );
	signal pa_busy		: std_logic;
begin
	Disc_En		<= "00000";
	offset_inc	<= x"000";
	SDD_PUR <= '0';
	U : Fir_PSteer
		port map(
			CLK			=> CLK,
			IMR			=> IMR,
			PA_Reset		=> PA_Reset,
			SDD_PUR		=> SDD_Pur,
			Disc_In		=> Disc_In,
			Disc_En		=> Disc_En,
			Disc_Cmp		=> Disc_Cmp,
			Offset_Time	=> OFfset_Time,
			Offset_Inc	=> Offset_inc,
			Peak_time		=> Peak_time,
			Pur_time		=> Pur_time,
			Peak_Done		=> Peak_Done,
			Block_out		=> Block_Out,
			fdisc		=> fdisc,
			pur			=> pur,
			pbusy		=> pbusy,
			PA_Busy		=> PA_Busy,
			Busy_Str		=> BUsy_Str );

	UDA : disc_proc
		port map(
			clk		=> clk,
			delay	=> 25,
			disc_in	=> disc_ina );

	UDB : disc_proc
		port map(
			clk		=> clk,
			delay	=> 735,
			disc_in	=> disc_inb );

	Disc_In 	<= Disc_ina or Disc_inb;
	Disc_Cmp 	<= "11111";
	
	IMR 		<= '0' after 750 ns;
	clk		<= not( CLK ) after 50 ns;
	TimeConst <= Conv_Std_logic_Vector( TC_SEL, 4 );
	Pur_Time	<= "00" & Peak_time( 11 downto 2 );
	High_Inh	<= '1' after 70 us;
--	-------------------------------------------------------------------------------

		with TC_Sel select
			Peak_time <=   x"003" when 0,
						x"007" when 1,
						x"00F" when 2,
						x"01F" when 3,
						x"03F" when 4,
						x"07F" when 5,
						x"0FF" when 6,
						x"1FF" when 7,
						x"3FF" when others;

	-------------------------------------------------------------------------------
	iDisc_In_Proc : process
	begin
		Offset_Time		<= x"0";
		PA_Reset			<= '0';
		wait;
		
	end process;
	-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
end test_bench;               -- Fir_PSteer
-------------------------------------------------------------------------------

