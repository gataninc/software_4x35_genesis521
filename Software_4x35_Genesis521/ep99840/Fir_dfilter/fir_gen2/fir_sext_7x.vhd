------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_sext_1x
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_sext_7x is 
	port( 
		in_vec		: in		std_logic_Vector( 15 downto 0 );
		out_vec		: out	std_logic_vector( 31 downto 0 ) );
end fir_sext_7x;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_sext_7x is
	signal in_Vec1	: std_logic_vector( 31 downto 0 );
	signal in_vec2	: std_logic_vector( 31 downto 0 );
	
begin
	-- 1X
	sext_gen_1x: for i in 16 to 31 generate
		iN_vec1(i) 	<= in_vec( 15 );
	end generate;
	
	in_vec1( 15 downto 0 ) <= in_vec( 15 downto  0 );	
		
	-- 8X
	sext_gen_8x : for i in 19 to 31 generate
		in_vec2(i) 	<= in_vec( 15 );
	end generate;
	
	in_vec2( 18 downto 0 ) <= in_vec( 15 downto  0 ) & "000";	
			
	-- Create out_vec(7x) by in_vec2(8x) - in_vec1(1x)
	as1: lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTIOn		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> in_vec2,  		-- dataa
			datab			=> in_vec1,		-- datab
			result			=> out_vec );	-- dataa-datab

------------------------------------------------------------------------------------
end behavioral;               -- fir_sext_7x
------------------------------------------------------------------------------------
			
