------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_sext_1x
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_sext_3x is 
	port( 
		in_vec		: in		std_logic_Vector( 15 downto 0 );
		out_vec		: out	std_logic_vector( 31 downto 0 ) );
end fir_sext_3x;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_sext_3x is
	signal In_Vec1 : std_logic_Vector( 31 downto 0 );
	signal in_Vec2	: std_logic_Vector( 31 downto 0 );
	
begin
	sext_gen_1x : for i in 16 to 31 generate
		in_vec1(i) 	<= in_vec( 15 );
	end generate;
	in_vec1( 15 downto 0 ) <= in_vec( 15 downto  0 );


	sext_gen_2x : for i in 17 to 31 generate
		in_vec2(i) 	<= in_vec( 15 );
	end generate;
	in_vec2( 16 downto 0 ) <= in_vec( 15 downto  0 ) & '0';	
		
	-- Create out_vec(3x) by in_vec1(1x) + in_vec2(2x)
	as1: lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTIOn		=> "ADD",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			cin				=> '0',
			dataa			=> in_vec1,  		-- dataa
			datab			=> in_vec2,		-- datab
			result			=> out_vec );		-- dataa-datab

			
------------------------------------------------------------------------------------
end behavioral;               -- fir_sext_3x
------------------------------------------------------------------------------------

