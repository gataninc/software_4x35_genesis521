------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_gen
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		12/08/05 - MCS Ver 357E
--			Mux_Start changed from 1 to zero (X2)
--			and what-ever else was necessary to keep proper alignment
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_gen2 is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		PA_Reset		: in		std_logic;
		AD_MR		: in		std_logic;					-- Clear's FIR
		TimeConst 	: in		std_logic_Vector(  3 downto 0 );
		Offset_Time	: in		std_logic_Vector(  3 downto 0 );
		data_abc		: in		std_logic_Vector( 79 downto 0 );	-- First TAP
		fir_out		: out	std_logic_Vector( 23 downto 0 ));	-- FIR Output 
end fir_gen2;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_gen2 is
	constant Acc_Width			: integer := 32;
	constant Coef				: integer := 1;

	constant CLAMP_MIN 			: std_logic_Vector( 19 downto 0 ) := x"FF000";
	constant CLAMP_MAX			: std_logic_Vector( 19 downto 0 ) := x"7FFFF";
	constant Mux_Start 			: integer := 1;	-- was 1

	constant Ones 				: std_logic_Vector( 31 downto 0 ) := x"FFFFFFFF";
	constant ZEROES			: std_logic_Vector( 63 downto 0 ) := x"0000000000000000";

	signal datad				: std_logic_vector( 15 downto 0 );
	signal datae				: std_logic_vector( 15 downto 0 );
	signal dataf				: std_logic_vector( 15 downto 0 );

	signal in_Vec1				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal in_vec2				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal in_vec3				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal in_vec4				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal in_vec5				: std_logic_Vector( Acc_Width-1 downto 0 );
	signal in_vec6				: std_logic_Vector( Acc_Width-1 downto 0 );

	signal fir_Data			: std_logic_vector( Acc_Width-1 downto 0 );
	signal Fir_Max_Clip_val 		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Fir_Min_Clip_val 		: std_logic_Vector( Acc_Width-1 downto 0 );

	signal out_vec				: std_logic_Vector( 47 downto 0 );

	signal fir_max_clip			: std_logic;
	signal fir_min_clip			: std_logic;

	signal fir_out_mux			: std_logic_Vector( 23 downto 0 );
	signal TimeCOnst_Reg		: std_logic_Vector(  3 downto 0 );
	
	------------------------------------------------------------------------------------
	component fir_sext_1x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_1x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_sext_2x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_2x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_sext_3x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_3x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_sext_4x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_4x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_sext_7x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_7x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_sext_8x is 
		port( 
			in_vec		: in		std_logic_Vector( 15 downto 0 );
			out_vec		: out	std_logic_vector( 31 downto 0 ) );
	end component fir_sext_8x;
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	component fir_tput is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			Offset_Time	: in		std_logic_Vector(  3 downto 0 );
			in_vec		: in		std_logic_vector( 47 downto 0 );
			out_vec		: out	std_logic_vector( 47 downto 0 ) );
	end component fir_tput;
	------------------------------------------------------------------------------------
	
	------------------------------------------------------------------------------------
	component fir_filter is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			AD_MR		: in		std_logic;					-- Clear's FIR
			in_vec1		: in		std_logic_vector( 31 downto 0 );
			in_vec2		: in		std_logic_vector( 31 downto 0 );
			in_vec3		: in		std_logic_vector( 31 downto 0 );
			in_vec4		: in		std_logic_vector( 31 downto 0 );
			in_vec5		: in		std_logic_vector( 31 downto 0 );
			in_vec6		: in		std_logic_vector( 31 downto 0 );
			fir_out		: out	std_logic_Vector( 31 downto 0 ));	-- FIR Output 
	end component fir_filter;
	------------------------------------------------------------------------------------
begin
	TimeConst_Reg_FF : lpm_ff
		generic map( 
			LPM_WIDTH	=> 4,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> TimeConst,
			q		=> TimeConst_Reg );

	tput_dly : fir_tput
		port map(
			imr			=> imr,
			clk			=> clk,
			offset_time	=> offset_time,
			in_vec		=> data_abc( 79 downto 32 ),
			out_vec		=> out_vec );
			
	datad	<= out_vec( 15 downto 0 );
	datae	<= out_vec( 31 downto 16 );
	dataf	<= out_Vec( 47 downto 32 );

	
--	Orig Coefs: 1 2 3 3 2 1 
	Coef_0_Gen : if( Coef = 0 ) generate
		u_vec1_Gen : fir_sext_1x port map( in_vec => data_abc( 15 downto  0 ), 	out_vec => in_vec1 );
		u_vec2_Gen : fir_sext_2x port map( in_vec => data_abc( 31 downto 16 ), 	out_vec => in_vec2 );
		u_vec3_Gen : fir_sext_3x port map( in_vec => data_abc( 47 downto 32 ), 	out_vec => in_vec3 );
		u_vec4_Gen : fir_sext_3x port map( in_vec => datad,                    	out_vec => in_vec4 );
		u_vec5_Gen : fir_sext_2x port map( in_vec => datae,                    	out_vec => in_vec5 );
		u_vec6_Gen : fir_sext_1x port map( in_vec => dataf,     	               out_vec => in_vec6 );
	end generate;
	

--	Prev Coefs: 1 3 4 4 3 1
	Coef_1_Gen : if( Coef = 1 ) generate
		u_vec1_Gen : fir_sext_1x port map( in_vec => data_abc( 15 downto  0 ), 	out_vec => in_vec1 );
		u_vec2_Gen : fir_sext_3x port map( in_vec => data_abc( 31 downto 16 ), 	out_vec => in_vec2 );
		u_vec3_Gen : fir_sext_4x port map( in_vec => data_abc( 47 downto 32 ), 	out_vec => in_vec3 );
		u_vec4_Gen : fir_sext_4x port map( in_vec => datad,                    	out_vec => in_vec4 );
		u_vec5_Gen : fir_sext_3x port map( in_vec => datae,                    	out_vec => in_vec5 );
		u_vec6_Gen : fir_sext_1x port map( in_vec => dataf,     	               out_vec => in_vec6 );
	end generate;

--	Coeficients: 1 7 8 8 7 1  (134/135 Reso)
	Coef_2_gen : if( Coef = 2 ) generate
		u_vec1_Gen : fir_sext_1x port map( in_vec => data_abc( 15 downto  0 ), 	out_vec => in_vec1 );
		u_vec2_Gen : fir_sext_7x port map( in_vec => data_abc( 31 downto 16 ), 	out_vec => in_vec2 );
		u_vec3_Gen : fir_sext_8x port map( in_vec => data_abc( 47 downto 32 ), 	out_vec => in_vec3 );
		u_vec4_Gen : fir_sext_8x port map( in_vec => datad,                    	out_vec => in_vec4 );
		u_vec5_Gen : fir_sext_7x port map( in_vec => datae,                    	out_vec => in_vec5 );
		u_vec6_Gen : fir_sext_1x port map( in_vec => dataf,     	               out_vec => in_vec6 );
	end generate;

	-- Actual Filter Generation -------------------------------------------------------------------
	ufir_filter: fir_filter
		port map(
			imr		=> imr,
			clk		=> clk,
			ad_mr	=> ad_mr,
			in_vec1	=> in_Vec1,
			in_Vec2	=> in_vec2,
			in_vec3	=> in_vec3,
			in_vec4	=> in_Vec4,
			in_vec5	=> in_Vec5,
			in_Vec6	=> in_vec6,
			fir_out	=> fir_data );
	-- Actual Filter Generation -------------------------------------------------------------------

	with Conv_Integer( TimeConst_reg ) select
		Fir_Max_Clip_Val <= Zeroes( Acc_Width-1 downto Mux_Start+19+1 ) & Clamp_Max & '0'		when 0,
						Zeroes( Acc_Width-1 downto Mux_Start+19+2 ) & Clamp_Max & "00"		when 1,
						Zeroes( Acc_Width-1 downto Mux_Start+19+3 ) & Clamp_Max & "000"		when 2,
						Zeroes( Acc_Width-1 downto Mux_Start+19+4 ) & Clamp_Max & "0000"		when 3,
						Zeroes( Acc_Width-1 downto Mux_Start+19+5 ) & Clamp_Max & "00000"  	when 4,
						Zeroes( Acc_Width-1 downto Mux_Start+19+6 ) & Clamp_Max & "000000"	when 5,
						Zeroes( Acc_Width-1 downto Mux_Start+19+7 ) & Clamp_Max & "0000000" 	when 6,
						Zeroes( Acc_Width-1 downto Mux_Start+19+8 ) & Clamp_Max & "00000000" 	when 7,
						Zeroes( Acc_Width-1 downto Mux_Start+19+9 ) & Clamp_Max & "000000000"	when others;

	with Conv_Integer( TimeConst_reg ) select
		Fir_Min_Clip_Val <= Ones( Acc_Width-1 downto Mux_Start+19+1 ) & Clamp_Min & '1' 		when 0,
						Ones( Acc_Width-1 downto Mux_Start+19+2 ) & Clamp_Min & "11"		when 1,
						Ones( Acc_Width-1 downto Mux_Start+19+3 ) & Clamp_Min & "111"		when 2,
						Ones( Acc_Width-1 downto Mux_Start+19+4 ) & Clamp_Min & "1111"		when 3,
						Ones( Acc_Width-1 downto Mux_Start+19+5 ) & Clamp_Min & "11111"		when 4,
						Ones( Acc_Width-1 downto Mux_Start+19+6 ) & Clamp_Min & "111111" 	when 5,
						Ones( Acc_Width-1 downto Mux_Start+19+7 ) & Clamp_Min & "1111111" 	when 6,
						Ones( Acc_Width-1 downto Mux_Start+19+8 ) & Clamp_Min & "11111111" 	when 7,
						Ones( Acc_Width-1 downto Mux_Start+19+9 ) & Clamp_Min & "111111111" 	when others;

--	with Conv_Integer( TimeConst_reg ) select
--		Fir_Max_Clip_Val <= Zeroes( Acc_Width-1 downto Mux_Start+19+1 ) & Clamp_Max & '0'		when 0,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+2 ) & Clamp_Max & "00"		when 1,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+3 ) & Clamp_Max & "000"		when 2,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+4 ) & Clamp_Max & "0000" 	when 3,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+5 ) & Clamp_Max & "00000"  	when 4,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+6 ) & Clamp_Max & "000000"	when 5,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+7 ) & Clamp_Max & "0000000" 	when 6,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+8 ) & Clamp_Max & "00000000" 	when 7,
--						Zeroes( Acc_Width-1 downto Mux_Start+19+9 ) & Clamp_Max & "000000000"	when others;
--
--	with Conv_Integer( TimeConst_reg ) select
--		Fir_Min_Clip_Val <= Ones( Acc_Width-1 downto Mux_Start+19+1 ) & Clamp_Min & '1'			when 0,
--						Ones( Acc_Width-1 downto Mux_Start+19+2 ) & Clamp_Min & "11"		when 1,
--						Ones( Acc_Width-1 downto Mux_Start+19+3 ) & Clamp_Min & "111"		when 2,
--						Ones( Acc_Width-1 downto Mux_Start+19+4 ) & Clamp_Min & "1111"		when 3,
--						Ones( Acc_Width-1 downto Mux_Start+19+5 ) & Clamp_Min & "11111"		when 4,
--						Ones( Acc_Width-1 downto Mux_Start+19+6 ) & Clamp_Min & "111111" 	when 5,
--						Ones( Acc_Width-1 downto Mux_Start+19+7 ) & Clamp_Min & "1111111" 	when 6,
--						Ones( Acc_Width-1 downto Mux_Start+19+8 ) & Clamp_Min & "11111111" 	when 7,
--						Ones( Acc_Width-1 downto Mux_Start+19+9 ) & Clamp_Min & "111111111" 	when others;
	
	Fir_Out_Mux	<= 	x"000000" when ( PA_Reset = '1' ) else
					Clamp_Max & "1111" when ( Fir_Max_Clip = '1' ) else
				   	Clamp_Min & "0000" when ( Fir_Min_Clip = '1' ) else
				   	Fir_Data( Mux_Start+19 downto Mux_Start ) & "0000"	when ( TimeConst_reg = "0000" ) else
				   	Fir_Data( Mux_Start+20 downto Mux_Start ) & "000" 	when ( TimeConst_reg = "0001" ) else
				  	Fir_Data( Mux_Start+21 downto Mux_Start ) & "00" 		when ( TimeConst_reg = "0010" ) else
					Fir_Data( Mux_Start+22 downto Mux_Start ) & "0"		when ( TimeConst_reg = "0011" ) else
					Fir_Data( Mux_Start+23 downto Mux_Start+0 ) 			when ( TimeConst_reg = "0100" ) else
					Fir_Data( Mux_Start+24 downto Mux_Start+1 ) 			when ( TimeConst_reg = "0101" ) else
					Fir_Data( Mux_Start+25 downto Mux_Start+2 ) 			when ( TimeConst_reg = "0110" ) else
					Fir_Data( Mux_Start+26 downto Mux_Start+3 ) 			when ( TimeConst_reg = "0111" ) else
					Fir_Data( Mux_Start+27 downto Mux_Start+4 );

	-------------------------------------------------------------------------------
	-- Clamp Max and Min Comparitors
	Fir_Max_Cmp_Compare : LPM_COMPARE
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Fir_Data,
			Datab			=> Fir_Max_Clip_val,
			agb				=> Fir_MAX_CLIP );

	Fir_Min_Cmp_Compare : LPM_COMPARE
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			Dataa			=> Fir_Min_Clip_val,
			datab			=> Fir_Data,
			agb				=> Fir_Min_CLIP );
	-- Clamp Max and Min Comparitors
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Final Output Flip-Flop
	Fir_Out_Ff : lpm_ff
		generic map(
			LPM_WIDTH		=> 24,	
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> imr,
			clock		=> clk,
			data			=> fir_out_mux,
			q			=> fir_out );
	-- Final Output Flip-Flop
	-------------------------------------------------------------------------------
			
------------------------------------------------------------------------------------
end behavioral;               -- fir_gen2
------------------------------------------------------------------------------------
			
