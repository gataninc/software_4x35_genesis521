------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_tput
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_tput is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		Offset_Time	: in		std_logic_Vector(  3 downto 0 );
		in_vec		: in		std_logic_vector( 47 downto 0 );
		out_vec		: out	std_logic_vector( 47 downto 0 ) );
end fir_tput;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_tput is
	signal fir_wadr			: std_logic_vector(  3 downto 0 );
	signal fir_radr			: std_logic_Vector(  3 downto 0 );	

begin
	WADR_CNTR : lpm_counter
		generic map(
			LPM_WIDTH				=> 4,
			LPM_TYPE				=> "LPM_COUNTER" )
		port map(
			aclr					=> IMR,
			clock				=> clk,
			q					=> fir_wadr );
	RADR_CNTR : lpm_Add_sub
		generic map(
			LPM_WIDTH				=> 4,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION		=> "UNSIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			Cin					=> '1',
			dataa				=> fiR_wadr,
			datab				=> offset_time,
			result				=> fir_radr );

	flat_top_gen : for i in 0 to 2 generate
		U_RAM : lpm_ram_dp
			generic map(
				LPM_WIDTH				=> 16,	-- Data Width
				LPM_WIDTHAD			=> 4,
				LPM_INDATA			=> "REGISTERED",
				LPM_OUTDATA			=> "REGISTERED",
				LPM_RDADDRESS_CONTROL	=> "REGISTERED",
				LPM_WRADDRESS_CONTROL	=> "REGISTERED",
				LPM_FILE				=> "INIT.MIF",
				LPM_TYPE				=> "LPM_RAM_DP" )
			PORT map(
				Wren					=> '1',
				wrclock				=> clk,
				rdclock 				=> clk,
				rdaddress				=> fir_radr,
				wraddress 			=> fir_wadr,
				data					=> in_vec(  (16*i)+15 downto (16*i)),
				q					=> out_vec( (16*i)+15 downto (16*i)));
	end generate;
------------------------------------------------------------------------------------
end behavioral;               -- fir_tput2
------------------------------------------------------------------------------------
			
