-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:56:39"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_gen2 IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	PA_Reset : IN std_logic;
	AD_MR : IN std_logic;
	TimeConst : IN std_logic_vector(3 DOWNTO 0);
	Offset_Time : IN std_logic_vector(3 DOWNTO 0);
	data_abc : IN std_logic_vector(79 DOWNTO 0);
	fir_out : OUT std_logic_vector(23 DOWNTO 0)
	);
END fir_gen2;

ARCHITECTURE structure OF fir_gen2 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_AD_MR : std_logic;
SIGNAL ww_TimeConst : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Offset_Time : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_data_abc : std_logic_vector(79 DOWNTO 0);
SIGNAL ww_fir_out : std_logic_vector(23 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Fir_Out_Ff_adffs_a0_a_a10919 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a0_a_a10921 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10930 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10934 : std_logic;
SIGNAL fir_out_mux_a23635 : std_logic;
SIGNAL fir_out_mux_a23723 : std_logic;
SIGNAL fir_out_mux_a23645 : std_logic;
SIGNAL fir_out_mux_a23646 : std_logic;
SIGNAL fir_out_mux_a23647 : std_logic;
SIGNAL fir_out_mux_a23648 : std_logic;
SIGNAL fir_out_mux_a23649 : std_logic;
SIGNAL fir_out_mux_a23650 : std_logic;
SIGNAL fir_out_mux_a23653 : std_logic;
SIGNAL fir_out_mux_a23654 : std_logic;
SIGNAL fir_out_mux_a23655 : std_logic;
SIGNAL fir_out_mux_a23657 : std_logic;
SIGNAL fir_out_mux_a23658 : std_logic;
SIGNAL fir_out_mux_a23659 : std_logic;
SIGNAL fir_out_mux_a23660 : std_logic;
SIGNAL fir_out_mux_a23661 : std_logic;
SIGNAL fir_out_mux_a23662 : std_logic;
SIGNAL fir_out_mux_a23677 : std_logic;
SIGNAL fir_out_mux_a23683 : std_logic;
SIGNAL fir_out_mux_a23684 : std_logic;
SIGNAL fir_out_mux_a23685 : std_logic;
SIGNAL fir_out_mux_a23689 : std_logic;
SIGNAL fir_out_mux_a23690 : std_logic;
SIGNAL fir_out_mux_a23691 : std_logic;
SIGNAL fir_out_mux_a23693 : std_logic;
SIGNAL fir_out_mux_a23694 : std_logic;
SIGNAL fir_out_mux_a23695 : std_logic;
SIGNAL fir_out_mux_a23696 : std_logic;
SIGNAL fir_out_mux_a23697 : std_logic;
SIGNAL fir_out_mux_a23698 : std_logic;
SIGNAL fir_out_mux_a23699 : std_logic;
SIGNAL fir_out_mux_a23700 : std_logic;
SIGNAL fir_out_mux_a23701 : std_logic;
SIGNAL fir_out_mux_a23702 : std_logic;
SIGNAL fir_out_mux_a23703 : std_logic;
SIGNAL fir_out_mux_a23704 : std_logic;
SIGNAL fir_out_mux_a23705 : std_logic;
SIGNAL fir_out_mux_a23706 : std_logic;
SIGNAL fir_out_mux_a23707 : std_logic;
SIGNAL fir_out_mux_a23708 : std_logic;
SIGNAL fir_out_mux_a23709 : std_logic;
SIGNAL fir_out_mux_a23710 : std_logic;
SIGNAL fir_out_mux_a23712 : std_logic;
SIGNAL fir_out_mux_a23713 : std_logic;
SIGNAL fir_out_mux_a23714 : std_logic;
SIGNAL fir_out_mux_a23715 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10977 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a10981 : std_logic;
SIGNAL Equal_a199 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11013 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11014 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11020 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1535 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1659 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a46_a_a981COMB : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11049 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11050 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11051 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11052 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11054 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1536 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1660 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a363 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a14_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a461 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a15_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a16_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a18_a : std_logic;
SIGNAL fir_out_mux_a23719 : std_logic;
SIGNAL fir_out_mux_a23720 : std_logic;
SIGNAL fir_out_mux_a23722 : std_logic;
SIGNAL Equal_a201 : std_logic;
SIGNAL Equal_a196 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a11065 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a20_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a22_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a11066 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a24_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11067 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11074 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11059 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a25_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a26_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a27_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1537 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a4_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a3_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a2_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a1_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1661 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a5_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a6_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a7_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a9_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a10_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a14_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a31_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a28_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a30_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1538 : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a4_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a3_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a2_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a1_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a0_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a29_a : std_logic;
SIGNAL ufir_filter_aff2_adffs_a31_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1662 : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a5_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a6_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a7_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a8_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a9_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a10_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a11_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a12_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a13_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a14_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a15_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a16_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a17_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a18_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a19_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a21_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a21_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a20_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a20_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a22_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a22_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a23_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a23_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a24_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a24_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a25_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a25_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a26_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a26_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a27_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a27_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1539 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a31_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a4_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a3_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a3_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a2_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a1_a : std_logic;
SIGNAL ufir_filter_ain_vec6_reg_ff_adffs_a0_a : std_logic;
SIGNAL ufir_filter_aff2_adffs_a30_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1663 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a5_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a6_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a7_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a8_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a8_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a9_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a10_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a11_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a12_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a13_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a14_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a15_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a16_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a31_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a28_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a28_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a30_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a30_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1540 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a4_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a790 : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a1_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a0_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a29_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a29_a : std_logic;
SIGNAL ufir_filter_aff2_adffs_a29_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1664 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a5_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a7_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a9_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774 : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a13_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a12_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a15_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a13_a : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a16_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a14_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a15_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1541 : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a4_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a2_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a0_a : std_logic;
SIGNAL ufir_filter_aff2_adffs_a28_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1665 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a3_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a5_a : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a8_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a9_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a7_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798 : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a12_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a11_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a14_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a15_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1542 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a4_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a3_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a2_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1666 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a5_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a6_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a7_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a8_a : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a9_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a10_a : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a11_a : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a12_a : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a13_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1543 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1667 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1544 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a25_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1668 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1545 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1669 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1546 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1670 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1547 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1671 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1548 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1672 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1549 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1673 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1550 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1674 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1551 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1675 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1552 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a17_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1676 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1553 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a16_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1677 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1554 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a15_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1678 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1555 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1679 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1556 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a13_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1680 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1557 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1681 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1558 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a11_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1682 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1559 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a10_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1683 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1560 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1684 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1561 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1685 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1562 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a7_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1686 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1563 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1687 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1564 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1688 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a_a1689 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a_a10917 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a_a10918 : std_logic;
SIGNAL ufir_filter_aas6_aadder_a_a00008 : std_logic;
SIGNAL data_abc_a78_a_acombout : std_logic;
SIGNAL data_abc_a79_a_acombout : std_logic;
SIGNAL data_abc_a68_a_acombout : std_logic;
SIGNAL data_abc_a67_a_acombout : std_logic;
SIGNAL data_abc_a66_a_acombout : std_logic;
SIGNAL data_abc_a65_a_acombout : std_logic;
SIGNAL data_abc_a69_a_acombout : std_logic;
SIGNAL data_abc_a70_a_acombout : std_logic;
SIGNAL data_abc_a71_a_acombout : std_logic;
SIGNAL data_abc_a72_a_acombout : std_logic;
SIGNAL data_abc_a73_a_acombout : std_logic;
SIGNAL data_abc_a74_a_acombout : std_logic;
SIGNAL data_abc_a75_a_acombout : std_logic;
SIGNAL data_abc_a76_a_acombout : std_logic;
SIGNAL data_abc_a77_a_acombout : std_logic;
SIGNAL Offset_Time_a1_a_acombout : std_logic;
SIGNAL data_abc_a63_a_acombout : std_logic;
SIGNAL data_abc_a1_a_acombout : std_logic;
SIGNAL data_abc_a64_a_acombout : std_logic;
SIGNAL data_abc_a62_a_acombout : std_logic;
SIGNAL data_abc_a61_a_acombout : std_logic;
SIGNAL data_abc_a45_a_acombout : std_logic;
SIGNAL data_abc_a46_a_acombout : std_logic;
SIGNAL data_abc_a34_a_acombout : std_logic;
SIGNAL data_abc_a52_a_acombout : std_logic;
SIGNAL data_abc_a51_a_acombout : std_logic;
SIGNAL data_abc_a50_a_acombout : std_logic;
SIGNAL data_abc_a49_a_acombout : std_logic;
SIGNAL data_abc_a17_a_acombout : std_logic;
SIGNAL data_abc_a48_a_acombout : std_logic;
SIGNAL data_abc_a53_a_acombout : std_logic;
SIGNAL data_abc_a54_a_acombout : std_logic;
SIGNAL data_abc_a55_a_acombout : std_logic;
SIGNAL data_abc_a38_a_acombout : std_logic;
SIGNAL data_abc_a56_a_acombout : std_logic;
SIGNAL data_abc_a39_a_acombout : std_logic;
SIGNAL data_abc_a57_a_acombout : std_logic;
SIGNAL data_abc_a58_a_acombout : std_logic;
SIGNAL data_abc_a59_a_acombout : std_logic;
SIGNAL data_abc_a42_a_acombout : std_logic;
SIGNAL data_abc_a60_a_acombout : std_logic;
SIGNAL data_abc_a9_a_acombout : std_logic;
SIGNAL data_abc_a11_a_acombout : std_logic;
SIGNAL data_abc_a12_a_acombout : std_logic;
SIGNAL data_abc_a13_a_acombout : std_logic;
SIGNAL data_abc_a30_a_acombout : std_logic;
SIGNAL data_abc_a19_a_acombout : std_logic;
SIGNAL data_abc_a21_a_acombout : std_logic;
SIGNAL data_abc_a23_a_acombout : std_logic;
SIGNAL data_abc_a26_a_acombout : std_logic;
SIGNAL data_abc_a28_a_acombout : std_logic;
SIGNAL data_abc_a31_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a20_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a21_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a22_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a23_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a24_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a25_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a26_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a27_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a28_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a29_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a30_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a31_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a30_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a29_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a28_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a27_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a26_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a25_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a24_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a23_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a22_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a21_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a20_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a19_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a18_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a17_a : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a16_a : std_logic;
SIGNAL data_abc_a15_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a31_a : std_logic;
SIGNAL data_abc_a14_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a14_a : std_logic;
SIGNAL data_abc_a29_a_acombout : std_logic;
SIGNAL data_abc_a27_a_acombout : std_logic;
SIGNAL data_abc_a25_a_acombout : std_logic;
SIGNAL data_abc_a24_a_acombout : std_logic;
SIGNAL data_abc_a22_a_acombout : std_logic;
SIGNAL data_abc_a20_a_acombout : std_logic;
SIGNAL data_abc_a18_a_acombout : std_logic;
SIGNAL data_abc_a16_a_acombout : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a13_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a12_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a11_a : std_logic;
SIGNAL data_abc_a10_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a10_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a9_a : std_logic;
SIGNAL data_abc_a8_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a8_a : std_logic;
SIGNAL data_abc_a7_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a7_a : std_logic;
SIGNAL data_abc_a6_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a6_a : std_logic;
SIGNAL data_abc_a5_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a5_a : std_logic;
SIGNAL data_abc_a4_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a4_a : std_logic;
SIGNAL data_abc_a3_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a3_a : std_logic;
SIGNAL data_abc_a2_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a2_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372 : std_logic;
SIGNAL ufir_filter_ain_vec2_reg_ff_adffs_a1_a : std_logic;
SIGNAL data_abc_a0_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec1_reg_ff_adffs_a0_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a816 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734 : std_logic;
SIGNAL data_abc_a47_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a31_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706 : std_logic;
SIGNAL data_abc_a44_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a14_a : std_logic;
SIGNAL data_abc_a43_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a13_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802 : std_logic;
SIGNAL data_abc_a41_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a11_a : std_logic;
SIGNAL data_abc_a40_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a10_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790 : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786 : std_logic;
SIGNAL data_abc_a37_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a7_a : std_logic;
SIGNAL data_abc_a36_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a6_a : std_logic;
SIGNAL data_abc_a35_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a5_a : std_logic;
SIGNAL ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762 : std_logic;
SIGNAL data_abc_a33_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a3_a : std_logic;
SIGNAL data_abc_a32_a_acombout : std_logic;
SIGNAL ufir_filter_ain_vec3_reg_ff_adffs_a2_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a788 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778 : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL Offset_Time_a0_a_acombout : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93 : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95 : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 : std_logic;
SIGNAL Offset_Time_a2_a_acombout : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99 : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 : std_logic;
SIGNAL Offset_Time_a3_a_acombout : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a103 : std_logic;
SIGNAL tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a10_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a12_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a9_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a11_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a8_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a10_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a6_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a8_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a4_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a6_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746 : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a1_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a3_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a0_a : std_logic;
SIGNAL ufir_filter_ain_vec4_reg_ff_adffs_a2_a : std_logic;
SIGNAL ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a788 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686 : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682 : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a15_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a14_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a13_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a12_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a11_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a10_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a9_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a7_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a6_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a5_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a4_a : std_logic;
SIGNAL ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738 : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a2_a : std_logic;
SIGNAL a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392 : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a1_a : std_logic;
SIGNAL tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a : std_logic;
SIGNAL ufir_filter_ain_vec5_reg_ff_adffs_a0_a : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a812 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766 : std_logic;
SIGNAL ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a0_a_a284 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a1_a_a248 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a2_a_a245 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a3_a_a242 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a4_a_a239 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a5_a_a251 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a6_a_a254 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a7_a_a257 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a8_a_a260 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a9_a_a263 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a10_a_a266 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a11_a_a269 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a12_a_a272 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a13_a_a275 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a14_a_a194 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a15_a_a197 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a16_a_a200 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a17_a_a203 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a18_a_a206 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a19_a_a209 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a20_a_a215 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a21_a_a212 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a22_a_a218 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a23_a_a221 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a24_a_a224 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a25_a_a227 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a26_a_a230 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a27_a_a233 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a28_a_a278 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a29_a_a287 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a30_a_a281 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a31_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a23_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a21_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a19_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a17_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401 : std_logic;
SIGNAL ufir_filter_aff1_adffs_a13_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a12_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a11_a : std_logic;
SIGNAL ufir_filter_aff1_adffs_a8_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413 : std_logic;
SIGNAL AD_MR_acombout : std_logic;
SIGNAL ufir_filter_aff2_adffs_a46_a_a924 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a47_a_a927 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a48_a_a930 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a49_a_a933 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a50_a_a936 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a51_a_a939 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a52_a_a945 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a53_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a45_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a44_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a43_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a4_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a5_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a6_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a8_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a9_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a12_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a14_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a18_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a19_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a20_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a21_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a22_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a23_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a24_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a26_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a27_a : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a32_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a33_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a34_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a35_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a36_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a37_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a38_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a39_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a41_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a42_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359 : std_logic;
SIGNAL ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a994 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004 : std_logic;
SIGNAL ufir_filter_aas7_aadder_a_a00008 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a46_a_a983 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a46_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a47_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a48_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a50_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a52_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a53_a_a942 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a54_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a54_a_a948 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a55_a_a951 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a56_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a56_a_a954 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a57_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a57_a_a957 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a58_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a58_a_a960 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a59_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a59_a_a963 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a60_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a60_a_a986 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a61_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a61_a_a992 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a62_a : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a471 : std_logic;
SIGNAL ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a62_a_a989 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a63_a : std_logic;
SIGNAL TimeConst_a3_a_acombout : std_logic;
SIGNAL TimeConst_Reg_FF_adffs_a3_a : std_logic;
SIGNAL TimeConst_a0_a_acombout : std_logic;
SIGNAL TimeConst_Reg_FF_adffs_a0_a : std_logic;
SIGNAL TimeConst_a1_a_acombout : std_logic;
SIGNAL TimeConst_Reg_FF_adffs_a1_a : std_logic;
SIGNAL Fir_Max_Clip_val_a26_a_a213 : std_logic;
SIGNAL TimeConst_a2_a_acombout : std_logic;
SIGNAL TimeConst_Reg_FF_adffs_a2_a : std_logic;
SIGNAL Fir_Max_Clip_val_a24_a_a215 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a55_a : std_logic;
SIGNAL ufir_filter_aff2_adffs_a49_a : std_logic;
SIGNAL Fir_Max_Clip_val_a25_a_a216 : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL ufir_filter_aff2_adffs_a51_a : std_logic;
SIGNAL Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 : std_logic;
SIGNAL Equal_a192 : std_logic;
SIGNAL Equal_a193 : std_logic;
SIGNAL Fir_Max_Clip_val_a22_a_a217 : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Fir_Out_Ff_adffs_a0_a_a10923 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a0_a_a10922 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a0_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a_a10927 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a_a10926 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a_a10925 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a1_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10932 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10933 : std_logic;
SIGNAL Equal_a185 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10931 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10920 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10929 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a_a10935 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a2_a : std_logic;
SIGNAL fir_out_mux_a23641 : std_logic;
SIGNAL fir_out_mux_a23639 : std_logic;
SIGNAL Equal_a189 : std_logic;
SIGNAL fir_out_mux_a23640 : std_logic;
SIGNAL fir_out_mux_a23643 : std_logic;
SIGNAL Equal_a187 : std_logic;
SIGNAL Equal_a184 : std_logic;
SIGNAL fir_out_mux_a23638 : std_logic;
SIGNAL fir_out_mux_a23636 : std_logic;
SIGNAL Equal_a188 : std_logic;
SIGNAL fir_out_mux_a23637 : std_logic;
SIGNAL fir_out_mux_a23644 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a3_a_a10937 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a3_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a4_a_a10940 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a4_a_a10939 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a4_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a5_a_a10943 : std_logic;
SIGNAL fir_out_mux_a23652 : std_logic;
SIGNAL fir_out_mux_a23634 : std_logic;
SIGNAL fir_out_mux_a23633 : std_logic;
SIGNAL fir_out_mux_a23651 : std_logic;
SIGNAL fir_out_mux_a23656 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a5_a_a10942 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a5_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a6_a_a10946 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a6_a_a10945 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a6_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a7_a_a10949 : std_logic;
SIGNAL ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972 : std_logic;
SIGNAL ufir_filter_aff2_adffs_a40_a : std_logic;
SIGNAL fir_out_mux_a23663 : std_logic;
SIGNAL fir_out_mux_a23666 : std_logic;
SIGNAL fir_out_mux_a23642 : std_logic;
SIGNAL fir_out_mux_a23665 : std_logic;
SIGNAL fir_out_mux_a23667 : std_logic;
SIGNAL fir_out_mux_a23664 : std_logic;
SIGNAL fir_out_mux_a23668 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a7_a_a10948 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a7_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a8_a_a10952 : std_logic;
SIGNAL fir_out_mux_a23670 : std_logic;
SIGNAL fir_out_mux_a23672 : std_logic;
SIGNAL fir_out_mux_a23671 : std_logic;
SIGNAL fir_out_mux_a23673 : std_logic;
SIGNAL fir_out_mux_a23669 : std_logic;
SIGNAL fir_out_mux_a23674 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a8_a_a10951 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a8_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a9_a_a10955 : std_logic;
SIGNAL fir_out_mux_a23676 : std_logic;
SIGNAL fir_out_mux_a23678 : std_logic;
SIGNAL fir_out_mux_a23679 : std_logic;
SIGNAL fir_out_mux_a23675 : std_logic;
SIGNAL fir_out_mux_a23680 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a9_a_a10954 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a9_a : std_logic;
SIGNAL fir_out_mux_a23682 : std_logic;
SIGNAL fir_out_mux_a23681 : std_logic;
SIGNAL fir_out_mux_a23686 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a10_a_a10957 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a10_a_a10958 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a10_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a11_a_a10961 : std_logic;
SIGNAL fir_out_mux_a23687 : std_logic;
SIGNAL fir_out_mux_a23688 : std_logic;
SIGNAL fir_out_mux_a23692 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a11_a_a10960 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a11_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a12_a_a10964 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a12_a_a10963 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a12_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a13_a_a10967 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a13_a_a10966 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a13_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a14_a_a10969 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a14_a_a10970 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a14_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a15_a_a10973 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a15_a_a10972 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a15_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10982 : std_logic;
SIGNAL Equal_a191 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a10978 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a10983 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a10975 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a_a10976 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a16_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10980 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10987 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10988 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10990 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10985 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a_a10986 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a17_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10989 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10996 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10994 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10997 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10992 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a_a10993 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a18_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11007 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11002 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11003 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11004 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11005 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11008 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a10999 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11000 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11006 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11012 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11015 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11016 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11010 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a_a11011 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a20_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11021 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11022 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11023 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11024 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11018 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a_a11019 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a21_a : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11028 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a19_a_a11072 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11068 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11030 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11031 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11026 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a_a11027 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a22_a : std_logic;
SIGNAL Equal_a200 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11069 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11047 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11048 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11053 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a_a11073 : std_logic;
SIGNAL Fir_Out_Ff_adffs_a23_a : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_PA_Reset <= PA_Reset;
ww_AD_MR <= AD_MR;
ww_TimeConst <= TimeConst;
ww_Offset_Time <= Offset_Time;
ww_data_abc <= data_abc;
fir_out <= ww_fir_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & 
tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 & 
tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 & tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93);

Fir_Out_Ff_adffs_a0_a_a10919_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a0_a_a10919 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a36_a # !TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a35_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a36_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a35_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a0_a_a10919);

Fir_Out_Ff_adffs_a0_a_a10921_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a0_a_a10921 = TimeConst_Reg_FF_adffs_a1_a & (Fir_Out_Ff_adffs_a0_a_a10919) # !TimeConst_Reg_FF_adffs_a1_a & (Fir_Out_Ff_adffs_a2_a_a10920)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA50",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datac => Fir_Out_Ff_adffs_a2_a_a10920,
	datad => Fir_Out_Ff_adffs_a0_a_a10919,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a0_a_a10921);

Fir_Out_Ff_adffs_a2_a_a10930_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10930 = TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a38_a) # !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a36_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => ufir_filter_aff2_adffs_a36_a,
	datad => ufir_filter_aff2_adffs_a38_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10930);

Fir_Out_Ff_adffs_a2_a_a10934_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10934 = Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # ufir_filter_aff2_adffs_a39_a & TimeConst_Reg_FF_adffs_a3_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a39_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10934);

fir_out_mux_a23635_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23635 = ufir_filter_aff2_adffs_a36_a & (fir_out_mux_a23634 # ufir_filter_aff2_adffs_a35_a & fir_out_mux_a23633) # !ufir_filter_aff2_adffs_a36_a & ufir_filter_aff2_adffs_a35_a & fir_out_mux_a23633

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a36_a,
	datab => ufir_filter_aff2_adffs_a35_a,
	datac => fir_out_mux_a23633,
	datad => fir_out_mux_a23634,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23635);

fir_out_mux_a23638_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23638 = !Equal_a187 & !Equal_a184 & (!TimeConst_Reg_FF_adffs_a0_a # !Equal_a192)
-- fir_out_mux_a23723 = !Equal_a187 & !Equal_a184 & (!TimeConst_Reg_FF_adffs_a0_a # !Equal_a192)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0007",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a192,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => Equal_a187,
	datad => Equal_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23638,
	cascout => fir_out_mux_a23723);

fir_out_mux_a23645_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23645 = ufir_filter_aff2_adffs_a36_a & (fir_out_mux_a23633 # fir_out_mux_a23634 & ufir_filter_aff2_adffs_a37_a) # !ufir_filter_aff2_adffs_a36_a & fir_out_mux_a23634 & ufir_filter_aff2_adffs_a37_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a36_a,
	datab => fir_out_mux_a23634,
	datac => ufir_filter_aff2_adffs_a37_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23645);

fir_out_mux_a23646_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23646 = ufir_filter_aff2_adffs_a34_a & (Equal_a188 # ufir_filter_aff2_adffs_a35_a & fir_out_mux_a23636) # !ufir_filter_aff2_adffs_a34_a & (ufir_filter_aff2_adffs_a35_a & fir_out_mux_a23636)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a34_a,
	datab => Equal_a188,
	datac => ufir_filter_aff2_adffs_a35_a,
	datad => fir_out_mux_a23636,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23646);

fir_out_mux_a23647_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23647 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a40_a) # !Equal_a189 & ufir_filter_aff2_adffs_a41_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a189,
	datab => ufir_filter_aff2_adffs_a41_a,
	datac => ufir_filter_aff2_adffs_a40_a,
	datad => fir_out_mux_a23639,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23647);

fir_out_mux_a23648_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23648 = !TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a0_a & Equal_a185 & ufir_filter_aff2_adffs_a38_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => Equal_a185,
	datad => ufir_filter_aff2_adffs_a38_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23648);

fir_out_mux_a23649_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23649 = fir_out_mux_a23648 # fir_out_mux_a23647 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a39_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => fir_out_mux_a23648,
	datac => ufir_filter_aff2_adffs_a39_a,
	datad => fir_out_mux_a23647,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23649);

fir_out_mux_a23650_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23650 = fir_out_mux_a23645 # fir_out_mux_a23646 # fir_out_mux_a23649 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23645,
	datab => fir_out_mux_a23649,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23646,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23650);

fir_out_mux_a23653_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23653 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a41_a) # !Equal_a189 & ufir_filter_aff2_adffs_a42_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23639,
	datab => ufir_filter_aff2_adffs_a42_a,
	datac => Equal_a189,
	datad => ufir_filter_aff2_adffs_a41_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23653);

fir_out_mux_a23654_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23654 = Equal_a185 & TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a39_a & !TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a185,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => ufir_filter_aff2_adffs_a39_a,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23654);

fir_out_mux_a23655_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23655 = fir_out_mux_a23653 # fir_out_mux_a23654 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a40_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => ufir_filter_aff2_adffs_a40_a,
	datac => fir_out_mux_a23653,
	datad => fir_out_mux_a23654,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23655);

fir_out_mux_a23657_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23657 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a39_a # ufir_filter_aff2_adffs_a38_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a38_a & fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a39_a,
	datac => ufir_filter_aff2_adffs_a38_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23657);

fir_out_mux_a23658_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23658 = fir_out_mux_a23636 & (ufir_filter_aff2_adffs_a37_a # ufir_filter_aff2_adffs_a36_a & Equal_a188) # !fir_out_mux_a23636 & (ufir_filter_aff2_adffs_a36_a & Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23636,
	datab => ufir_filter_aff2_adffs_a37_a,
	datac => ufir_filter_aff2_adffs_a36_a,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23658);

fir_out_mux_a23659_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23659 = fir_out_mux_a23639 & (Equal_a189 & ufir_filter_aff2_adffs_a42_a # !Equal_a189 & (ufir_filter_aff2_adffs_a43_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "88C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => fir_out_mux_a23639,
	datac => ufir_filter_aff2_adffs_a43_a,
	datad => Equal_a189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23659);

fir_out_mux_a23660_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23660 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a40_a & Equal_a185 & !TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a40_a,
	datac => Equal_a185,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23660);

fir_out_mux_a23661_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23661 = fir_out_mux_a23659 # fir_out_mux_a23660 # ufir_filter_aff2_adffs_a41_a & fir_out_mux_a23642

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a41_a,
	datab => fir_out_mux_a23642,
	datac => fir_out_mux_a23659,
	datad => fir_out_mux_a23660,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23661);

fir_out_mux_a23662_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23662 = fir_out_mux_a23657 # fir_out_mux_a23658 # fir_out_mux_a23638 & fir_out_mux_a23661

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23657,
	datab => fir_out_mux_a23638,
	datac => fir_out_mux_a23661,
	datad => fir_out_mux_a23658,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23662);

fir_out_mux_a23677_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23677 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a45_a) # !Equal_a189 & ufir_filter_aff2_adffs_a46_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a189,
	datab => ufir_filter_aff2_adffs_a46_a,
	datac => fir_out_mux_a23639,
	datad => ufir_filter_aff2_adffs_a45_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23677);

fir_out_mux_a23683_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23683 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a46_a) # !Equal_a189 & ufir_filter_aff2_adffs_a47_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C088",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a47_a,
	datab => fir_out_mux_a23639,
	datac => ufir_filter_aff2_adffs_a46_a,
	datad => Equal_a189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23683);

fir_out_mux_a23684_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23684 = !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a44_a & TimeConst_Reg_FF_adffs_a0_a & Equal_a185

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a44_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23684);

fir_out_mux_a23685_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23685 = fir_out_mux_a23683 # fir_out_mux_a23684 # ufir_filter_aff2_adffs_a45_a & fir_out_mux_a23642

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datab => fir_out_mux_a23642,
	datac => fir_out_mux_a23683,
	datad => fir_out_mux_a23684,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23685);

fir_out_mux_a23689_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23689 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a47_a) # !Equal_a189 & ufir_filter_aff2_adffs_a48_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23639,
	datab => ufir_filter_aff2_adffs_a48_a,
	datac => Equal_a189,
	datad => ufir_filter_aff2_adffs_a47_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23689);

fir_out_mux_a23690_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23690 = !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a45_a & TimeConst_Reg_FF_adffs_a0_a & Equal_a185

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a45_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23690);

fir_out_mux_a23691_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23691 = fir_out_mux_a23690 # fir_out_mux_a23689 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a46_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => fir_out_mux_a23690,
	datac => ufir_filter_aff2_adffs_a46_a,
	datad => fir_out_mux_a23689,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23691);

fir_out_mux_a23693_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23693 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a45_a # ufir_filter_aff2_adffs_a44_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & ufir_filter_aff2_adffs_a44_a & (fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a44_a,
	datac => ufir_filter_aff2_adffs_a45_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23693);

fir_out_mux_a23694_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23694 = ufir_filter_aff2_adffs_a42_a & (Equal_a188 # ufir_filter_aff2_adffs_a43_a & fir_out_mux_a23636) # !ufir_filter_aff2_adffs_a42_a & ufir_filter_aff2_adffs_a43_a & fir_out_mux_a23636

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => ufir_filter_aff2_adffs_a43_a,
	datac => fir_out_mux_a23636,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23694);

fir_out_mux_a23695_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23695 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a48_a) # !Equal_a189 & ufir_filter_aff2_adffs_a49_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a49_a,
	datab => Equal_a189,
	datac => ufir_filter_aff2_adffs_a48_a,
	datad => fir_out_mux_a23639,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23695);

fir_out_mux_a23696_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23696 = !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a46_a & TimeConst_Reg_FF_adffs_a0_a & Equal_a185

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a46_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23696);

fir_out_mux_a23697_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23697 = fir_out_mux_a23696 # fir_out_mux_a23695 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a47_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => fir_out_mux_a23696,
	datac => ufir_filter_aff2_adffs_a47_a,
	datad => fir_out_mux_a23695,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23697);

fir_out_mux_a23698_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23698 = fir_out_mux_a23694 # fir_out_mux_a23693 # fir_out_mux_a23638 & fir_out_mux_a23697

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23694,
	datab => fir_out_mux_a23638,
	datac => fir_out_mux_a23693,
	datad => fir_out_mux_a23697,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23698);

fir_out_mux_a23699_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23699 = ufir_filter_aff2_adffs_a45_a & (fir_out_mux_a23633 # ufir_filter_aff2_adffs_a46_a & fir_out_mux_a23634) # !ufir_filter_aff2_adffs_a45_a & ufir_filter_aff2_adffs_a46_a & fir_out_mux_a23634

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datab => ufir_filter_aff2_adffs_a46_a,
	datac => fir_out_mux_a23634,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23699);

fir_out_mux_a23700_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23700 = ufir_filter_aff2_adffs_a43_a & (Equal_a188 # ufir_filter_aff2_adffs_a44_a & fir_out_mux_a23636) # !ufir_filter_aff2_adffs_a43_a & ufir_filter_aff2_adffs_a44_a & (fir_out_mux_a23636)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a43_a,
	datab => ufir_filter_aff2_adffs_a44_a,
	datac => Equal_a188,
	datad => fir_out_mux_a23636,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23700);

fir_out_mux_a23701_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23701 = fir_out_mux_a23639 & (Equal_a189 & ufir_filter_aff2_adffs_a49_a # !Equal_a189 & (ufir_filter_aff2_adffs_a50_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a189,
	datab => ufir_filter_aff2_adffs_a49_a,
	datac => fir_out_mux_a23639,
	datad => ufir_filter_aff2_adffs_a50_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23701);

fir_out_mux_a23702_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23702 = ufir_filter_aff2_adffs_a47_a & Equal_a185 & !TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a47_a,
	datab => Equal_a185,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23702);

fir_out_mux_a23703_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23703 = fir_out_mux_a23702 # fir_out_mux_a23701 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a48_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => ufir_filter_aff2_adffs_a48_a,
	datac => fir_out_mux_a23702,
	datad => fir_out_mux_a23701,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23703);

fir_out_mux_a23704_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23704 = fir_out_mux_a23699 # fir_out_mux_a23700 # fir_out_mux_a23703 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23703,
	datab => fir_out_mux_a23699,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23704);

fir_out_mux_a23705_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23705 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a47_a # ufir_filter_aff2_adffs_a46_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & ufir_filter_aff2_adffs_a46_a & (fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a46_a,
	datac => ufir_filter_aff2_adffs_a47_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23705);

fir_out_mux_a23706_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23706 = ufir_filter_aff2_adffs_a44_a & (Equal_a188 # ufir_filter_aff2_adffs_a45_a & fir_out_mux_a23636) # !ufir_filter_aff2_adffs_a44_a & ufir_filter_aff2_adffs_a45_a & (fir_out_mux_a23636)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a44_a,
	datab => ufir_filter_aff2_adffs_a45_a,
	datac => Equal_a188,
	datad => fir_out_mux_a23636,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23706);

fir_out_mux_a23707_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23707 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a50_a) # !Equal_a189 & ufir_filter_aff2_adffs_a51_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23639,
	datab => ufir_filter_aff2_adffs_a51_a,
	datac => Equal_a189,
	datad => ufir_filter_aff2_adffs_a50_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23707);

fir_out_mux_a23708_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23708 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a48_a & Equal_a185 & !TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a48_a,
	datac => Equal_a185,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23708);

fir_out_mux_a23709_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23709 = fir_out_mux_a23708 # fir_out_mux_a23707 # ufir_filter_aff2_adffs_a49_a & fir_out_mux_a23642

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23708,
	datab => ufir_filter_aff2_adffs_a49_a,
	datac => fir_out_mux_a23642,
	datad => fir_out_mux_a23707,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23709);

fir_out_mux_a23710_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23710 = fir_out_mux_a23705 # fir_out_mux_a23706 # fir_out_mux_a23709 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23709,
	datab => fir_out_mux_a23705,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23706,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23710);

fir_out_mux_a23712_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23712 = ufir_filter_aff2_adffs_a47_a & !Equal_a192 & TimeConst_Reg_FF_adffs_a0_a & Equal_a184

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a47_a,
	datab => Equal_a192,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23712);

fir_out_mux_a23713_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23713 = fir_out_mux_a23722 # fir_out_mux_a23712 # ufir_filter_aff2_adffs_a48_a & fir_out_mux_a23634

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23722,
	datab => ufir_filter_aff2_adffs_a48_a,
	datac => fir_out_mux_a23712,
	datad => fir_out_mux_a23634,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23713);

fir_out_mux_a23714_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23714 = ufir_filter_aff2_adffs_a45_a & (TimeConst_Reg_FF_adffs_a0_a & Equal_a192)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a192,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23714);

fir_out_mux_a23715_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23715 = fir_out_mux_a23714 # fir_out_mux_a23713 # ufir_filter_aff2_adffs_a46_a & fir_out_mux_a23636

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23714,
	datab => ufir_filter_aff2_adffs_a46_a,
	datac => fir_out_mux_a23636,
	datad => fir_out_mux_a23713,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23715);

Fir_Out_Ff_adffs_a17_a_a10977_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10977 = TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a50_a # !TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a46_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B8B8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a50_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => ufir_filter_aff2_adffs_a46_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10977);

Fir_Out_Ff_adffs_a16_a_a10981_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a10981 = Fir_Out_Ff_adffs_a16_a_a11065 # Fir_Out_Ff_adffs_a17_a_a10980 & !TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF20",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a17_a_a10980,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => Fir_Out_Ff_adffs_a16_a_a11065,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a10981);

Equal_a191_I : apex20ke_lcell
-- Equation(s):
-- Equal_a191 = TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a1_a)
-- Equal_a199 = TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a191,
	cascout => Equal_a199);

Fir_Out_Ff_adffs_a21_a_a11013_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11013 = TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a52_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a50_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DD88",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a52_a,
	datad => ufir_filter_aff2_adffs_a50_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11013);

Fir_Out_Ff_adffs_a20_a_a11014_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11014 = !TimeConst_Reg_FF_adffs_a2_a & TimeConst_Reg_FF_adffs_a0_a & Fir_Out_Ff_adffs_a21_a_a11013

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Fir_Out_Ff_adffs_a21_a_a11013,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11014);

Fir_Out_Ff_adffs_a21_a_a11020_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11020 = !TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a21_a_a11015 # !TimeConst_Reg_FF_adffs_a2_a & (Fir_Out_Ff_adffs_a21_a_a11013))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4540",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => Fir_Out_Ff_adffs_a21_a_a11015,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => Fir_Out_Ff_adffs_a21_a_a11013,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11020);

Fir_Out_Ff_adffs_a23_a_a11049_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11049 = TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a55_a) # !TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a54_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EE44",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a54_a,
	datad => ufir_filter_aff2_adffs_a55_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11049);

Fir_Out_Ff_adffs_a23_a_a11050_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11050 = TimeConst_Reg_FF_adffs_a1_a & (Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & Fir_Out_Ff_adffs_a23_a_a11049)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datac => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	datad => Fir_Out_Ff_adffs_a23_a_a11049,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11050);

Fir_Out_Ff_adffs_a23_a_a11051_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11051 = TimeConst_Reg_FF_adffs_a1_a & (TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a59_a) # !TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a58_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a58_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a59_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11051);

Fir_Out_Ff_adffs_a23_a_a11052_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11052 = Fir_Out_Ff_adffs_a23_a_a11050 # TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a23_a_a11051 & !TimeConst_Reg_FF_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a23_a_a11050,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => Fir_Out_Ff_adffs_a23_a_a11051,
	datad => TimeConst_Reg_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11052);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361 = ufir_filter_aff2_adffs_a45_a $ ufir_filter_aff1_adffs_a13_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a363 = CARRY(ufir_filter_aff2_adffs_a45_a & !ufir_filter_aff1_adffs_a13_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359 # !ufir_filter_aff2_adffs_a45_a 
-- & (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359 # !ufir_filter_aff1_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datab => ufir_filter_aff1_adffs_a13_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a363);

ufir_filter_aff1_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a14_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a14_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694 $ !ufir_filter_aff1_adffs_a13_a_a275, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a14_a_a194 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a14_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694 # !ufir_filter_aff1_adffs_a13_a_a275) # !ufir_filter_ain_vec6_reg_ff_adffs_a14_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694 & !ufir_filter_aff1_adffs_a13_a_a275)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a14_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694,
	cin => ufir_filter_aff1_adffs_a13_a_a275,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a14_a,
	cout => ufir_filter_aff1_adffs_a14_a_a194);

ufir_filter_aff1_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a15_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698 $ ufir_filter_aff1_adffs_a14_a_a194, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a15_a_a197 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698 & !ufir_filter_aff1_adffs_a14_a_a194 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a14_a_a194 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698,
	cin => ufir_filter_aff1_adffs_a14_a_a194,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a15_a,
	cout => ufir_filter_aff1_adffs_a15_a_a197);

ufir_filter_aff1_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a16_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702 $ !ufir_filter_aff1_adffs_a15_a_a197, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a16_a_a200 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702 # !ufir_filter_aff1_adffs_a15_a_a197) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702 & !ufir_filter_aff1_adffs_a15_a_a197)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702,
	cin => ufir_filter_aff1_adffs_a15_a_a197,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a16_a,
	cout => ufir_filter_aff1_adffs_a16_a_a200);

ufir_filter_aff1_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a18_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710 $ !ufir_filter_aff1_adffs_a17_a_a203, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a18_a_a206 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710 # !ufir_filter_aff1_adffs_a17_a_a203) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710 & !ufir_filter_aff1_adffs_a17_a_a203)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710,
	cin => ufir_filter_aff1_adffs_a17_a_a203,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a18_a,
	cout => ufir_filter_aff1_adffs_a18_a_a206);

fir_out_mux_a23719_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23719 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a51_a) # !Equal_a189 & ufir_filter_aff2_adffs_a52_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23639,
	datab => ufir_filter_aff2_adffs_a52_a,
	datac => Equal_a189,
	datad => ufir_filter_aff2_adffs_a51_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23719);

fir_out_mux_a23720_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23720 = Equal_a185 & !TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a49_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a185,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a49_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23720);

fir_out_mux_a23722_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23722 = (fir_out_mux_a23720 # fir_out_mux_a23719 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a50_a) & CASCADE(fir_out_mux_a23723)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => fir_out_mux_a23720,
	datac => ufir_filter_aff2_adffs_a50_a,
	datad => fir_out_mux_a23719,
	cascin => fir_out_mux_a23723,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23722);

Equal_a196_I : apex20ke_lcell
-- Equation(s):
-- Equal_a201 = !TimeConst_Reg_FF_adffs_a1_a & !TimeConst_Reg_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0303",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a196,
	cascout => Equal_a201);

Fir_Out_Ff_adffs_a16_a_a11065_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a11065 = (TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a49_a) # !TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a45_a) & CASCADE(Equal_a201)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datab => ufir_filter_aff2_adffs_a49_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	cascin => Equal_a201,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a11065);

ufir_filter_aff1_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a20_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722 $ !ufir_filter_aff1_adffs_a19_a_a209, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a20_a_a215 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722 # !ufir_filter_aff1_adffs_a19_a_a209) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722 & !ufir_filter_aff1_adffs_a19_a_a209)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722,
	cin => ufir_filter_aff1_adffs_a19_a_a209,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a20_a,
	cout => ufir_filter_aff1_adffs_a20_a_a215);

ufir_filter_aff1_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a22_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726 $ !ufir_filter_aff1_adffs_a21_a_a212, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a22_a_a218 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726 # !ufir_filter_aff1_adffs_a21_a_a212) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726 & !ufir_filter_aff1_adffs_a21_a_a212)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726,
	cin => ufir_filter_aff1_adffs_a21_a_a212,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a22_a,
	cout => ufir_filter_aff1_adffs_a22_a_a218);

Fir_Out_Ff_adffs_a18_a_a11066_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a11066 = (TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a54_a # !TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a50_a)) & CASCADE(Equal_a199)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a54_a,
	datac => ufir_filter_aff2_adffs_a50_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	cascin => Equal_a199,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a11066);

ufir_filter_aff1_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a24_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734 $ !ufir_filter_aff1_adffs_a23_a_a221, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a24_a_a224 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734 # !ufir_filter_aff1_adffs_a23_a_a221) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734 & !ufir_filter_aff1_adffs_a23_a_a221)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734,
	cin => ufir_filter_aff1_adffs_a23_a_a221,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a24_a,
	cout => ufir_filter_aff1_adffs_a24_a_a224);

Fir_Out_Ff_adffs_a19_a_a11067_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11067 = (TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a50_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a48_a)) & CASCADE(Fir_Out_Ff_adffs_a19_a_a11074)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a50_a,
	datab => ufir_filter_aff2_adffs_a48_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	cascin => Fir_Out_Ff_adffs_a19_a_a11074,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11067);

Fir_Out_Ff_adffs_a19_a_a11059_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11074 = !TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11059,
	cascout => Fir_Out_Ff_adffs_a19_a_a11074);

ufir_filter_aff1_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a25_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738 $ ufir_filter_aff1_adffs_a24_a_a224, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a25_a_a227 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738 & !ufir_filter_aff1_adffs_a24_a_a224 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a24_a_a224 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738,
	cin => ufir_filter_aff1_adffs_a24_a_a224,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a25_a,
	cout => ufir_filter_aff1_adffs_a25_a_a227);

ufir_filter_aff1_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a26_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742 $ !ufir_filter_aff1_adffs_a25_a_a227, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a26_a_a230 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742 # !ufir_filter_aff1_adffs_a25_a_a227) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742 & !ufir_filter_aff1_adffs_a25_a_a227)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742,
	cin => ufir_filter_aff1_adffs_a25_a_a227,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a26_a,
	cout => ufir_filter_aff1_adffs_a26_a_a230);

ufir_filter_aff1_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a27_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746 $ ufir_filter_aff1_adffs_a26_a_a230, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a27_a_a233 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746 & !ufir_filter_aff1_adffs_a26_a_a230 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a26_a_a230 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746,
	cin => ufir_filter_aff1_adffs_a26_a_a230,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a27_a,
	cout => ufir_filter_aff1_adffs_a27_a_a233);

ufir_filter_aff1_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a4_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a4_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754 $ !ufir_filter_aff1_adffs_a3_a_a242, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a4_a_a239 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a4_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754 # !ufir_filter_aff1_adffs_a3_a_a242) # !ufir_filter_ain_vec6_reg_ff_adffs_a4_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754 & !ufir_filter_aff1_adffs_a3_a_a242)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a4_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754,
	cin => ufir_filter_aff1_adffs_a3_a_a242,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a4_a,
	cout => ufir_filter_aff1_adffs_a4_a_a239);

ufir_filter_aff1_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a3_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a3_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758 $ ufir_filter_aff1_adffs_a2_a_a245, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a3_a_a242 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a3_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758 & !ufir_filter_aff1_adffs_a2_a_a245 # !ufir_filter_ain_vec6_reg_ff_adffs_a3_a & (!ufir_filter_aff1_adffs_a2_a_a245 
-- # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a3_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758,
	cin => ufir_filter_aff1_adffs_a2_a_a245,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a3_a,
	cout => ufir_filter_aff1_adffs_a3_a_a242);

ufir_filter_aff1_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a2_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a2_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762 $ !ufir_filter_aff1_adffs_a1_a_a248, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a2_a_a245 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a2_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762 # !ufir_filter_aff1_adffs_a1_a_a248) # !ufir_filter_ain_vec6_reg_ff_adffs_a2_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762 & !ufir_filter_aff1_adffs_a1_a_a248)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a2_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762,
	cin => ufir_filter_aff1_adffs_a1_a_a248,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a2_a,
	cout => ufir_filter_aff1_adffs_a2_a_a245);

ufir_filter_aff1_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a1_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a1_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766 $ ufir_filter_aff1_adffs_a0_a_a284, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a1_a_a248 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a1_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766 & !ufir_filter_aff1_adffs_a0_a_a284 # !ufir_filter_ain_vec6_reg_ff_adffs_a1_a & (!ufir_filter_aff1_adffs_a0_a_a284 
-- # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a1_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766,
	cin => ufir_filter_aff1_adffs_a0_a_a284,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a1_a,
	cout => ufir_filter_aff1_adffs_a1_a_a248);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000 = ufir_filter_aff2_adffs_a31_a $ ufir_filter_aff2_adffs_a59_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002 = CARRY(ufir_filter_aff2_adffs_a31_a & ufir_filter_aff2_adffs_a59_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010 # !ufir_filter_aff2_adffs_a31_a 
-- & (ufir_filter_aff2_adffs_a59_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a31_a,
	datab => ufir_filter_aff2_adffs_a59_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002);

ufir_filter_aff1_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a5_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a5_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770 $ ufir_filter_aff1_adffs_a4_a_a239, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a5_a_a251 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a5_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770 & !ufir_filter_aff1_adffs_a4_a_a239 # !ufir_filter_ain_vec6_reg_ff_adffs_a5_a & (!ufir_filter_aff1_adffs_a4_a_a239 
-- # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a5_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770,
	cin => ufir_filter_aff1_adffs_a4_a_a239,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a5_a,
	cout => ufir_filter_aff1_adffs_a5_a_a251);

ufir_filter_aff1_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a6_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a6_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774 $ !ufir_filter_aff1_adffs_a5_a_a251, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a6_a_a254 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a6_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774 # !ufir_filter_aff1_adffs_a5_a_a251) # !ufir_filter_ain_vec6_reg_ff_adffs_a6_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774 & !ufir_filter_aff1_adffs_a5_a_a251)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a6_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774,
	cin => ufir_filter_aff1_adffs_a5_a_a251,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a6_a,
	cout => ufir_filter_aff1_adffs_a6_a_a254);

ufir_filter_aff1_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a7_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a7_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778 $ ufir_filter_aff1_adffs_a6_a_a254, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a7_a_a257 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a7_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778 & !ufir_filter_aff1_adffs_a6_a_a254 # !ufir_filter_ain_vec6_reg_ff_adffs_a7_a & (!ufir_filter_aff1_adffs_a6_a_a254 
-- # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a7_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778,
	cin => ufir_filter_aff1_adffs_a6_a_a254,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a7_a,
	cout => ufir_filter_aff1_adffs_a7_a_a257);

ufir_filter_aff1_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a9_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a9_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786 $ ufir_filter_aff1_adffs_a8_a_a260, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a9_a_a263 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a9_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786 & !ufir_filter_aff1_adffs_a8_a_a260 # !ufir_filter_ain_vec6_reg_ff_adffs_a9_a & (!ufir_filter_aff1_adffs_a8_a_a260 
-- # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a9_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786,
	cin => ufir_filter_aff1_adffs_a8_a_a260,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a9_a,
	cout => ufir_filter_aff1_adffs_a9_a_a263);

ufir_filter_aff1_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a10_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a10_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790 $ !ufir_filter_aff1_adffs_a9_a_a263, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a10_a_a266 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a10_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790 # !ufir_filter_aff1_adffs_a9_a_a263) # !ufir_filter_ain_vec6_reg_ff_adffs_a10_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790 & !ufir_filter_aff1_adffs_a9_a_a263)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a10_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790,
	cin => ufir_filter_aff1_adffs_a9_a_a263,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a10_a,
	cout => ufir_filter_aff1_adffs_a10_a_a266);

ufir_filter_ain_vec6_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a14_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a14_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a14_a);

ufir_filter_ain_vec6_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a31_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a31_a);

ufir_filter_aff1_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a28_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806 $ !ufir_filter_aff1_adffs_a27_a_a233, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a28_a_a278 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806 # !ufir_filter_aff1_adffs_a27_a_a233) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806 & !ufir_filter_aff1_adffs_a27_a_a233)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806,
	cin => ufir_filter_aff1_adffs_a27_a_a233,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a28_a,
	cout => ufir_filter_aff1_adffs_a28_a_a278);

ufir_filter_aff1_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a30_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810 $ !ufir_filter_aff1_adffs_a29_a_a287, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a30_a_a281 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810 # !ufir_filter_aff1_adffs_a29_a_a287) # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810 & !ufir_filter_aff1_adffs_a29_a_a287)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810,
	cin => ufir_filter_aff1_adffs_a29_a_a287,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a30_a,
	cout => ufir_filter_aff1_adffs_a30_a_a281);

ufir_filter_ain_vec6_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a4_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a4_a);

ufir_filter_ain_vec6_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a3_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a3_a);

ufir_filter_ain_vec6_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a2_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a2_a);

ufir_filter_ain_vec6_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a1_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a1_a);

ufir_filter_aff1_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a0_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a0_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a0_a_a284 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a0_a & ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a0_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a0_a,
	cout => ufir_filter_aff1_adffs_a0_a_a284);

ufir_filter_aff1_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a29_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818 $ ufir_filter_aff1_adffs_a28_a_a278, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a29_a_a287 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818 & !ufir_filter_aff1_adffs_a28_a_a278 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a28_a_a278 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818,
	cin => ufir_filter_aff1_adffs_a28_a_a278,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a29_a,
	cout => ufir_filter_aff1_adffs_a29_a_a287);

ufir_filter_aff2_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a31_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1000,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a31_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008 = ufir_filter_aff2_adffs_a30_a $ ufir_filter_aff2_adffs_a58_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010 = CARRY(ufir_filter_aff2_adffs_a30_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014 # !ufir_filter_aff2_adffs_a58_a) # 
-- !ufir_filter_aff2_adffs_a30_a & !ufir_filter_aff2_adffs_a58_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a30_a,
	datab => ufir_filter_aff2_adffs_a58_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1010);

ufir_filter_ain_vec6_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a5_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a5_a);

ufir_filter_ain_vec6_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a6_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a6_a);

ufir_filter_ain_vec6_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a7_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a7_a);

ufir_filter_ain_vec6_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a8_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a8_a);

ufir_filter_ain_vec6_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a9_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a9_a);

ufir_filter_ain_vec6_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a10_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a10_a);

ufir_filter_ain_vec6_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a11_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a11_a);

ufir_filter_ain_vec6_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a12_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a12_a);

ufir_filter_ain_vec6_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a13_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a13_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a13_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a13_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674 = ufir_filter_ain_vec4_reg_ff_adffs_a14_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a14_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a14_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a14_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a78_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a14_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369 = !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a363

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a363,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678 = ufir_filter_ain_vec4_reg_ff_adffs_a15_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a15_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a15_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a15_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a676,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a79_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a15_a);

ufir_filter_ain_vec5_reg_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a16_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a16_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a16_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a16_a);

ufir_filter_ain_vec5_reg_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a17_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a16_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a17_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a16_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a17_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a17_a);

ufir_filter_ain_vec5_reg_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a18_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a17_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a18_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a17_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a18_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a18_a);

ufir_filter_ain_vec5_reg_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a19_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a18_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a19_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a18_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a19_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a19_a);

ufir_filter_ain_vec5_reg_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a21_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a20_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a21_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a20_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a21_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a21_a);

ufir_filter_ain_vec5_reg_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a20_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a19_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a20_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a19_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a20_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a20_a);

ufir_filter_ain_vec5_reg_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a22_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a21_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a22_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a21_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a22_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a22_a);

ufir_filter_ain_vec5_reg_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a23_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a22_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a23_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a22_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a23_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a23_a);

ufir_filter_ain_vec5_reg_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a24_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a23_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a24_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a23_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a24_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a24_a);

ufir_filter_ain_vec5_reg_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a25_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a24_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a25_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a24_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a25_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a25_a);

ufir_filter_ain_vec5_reg_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a26_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a25_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a26_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a25_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a26_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a26_a);

ufir_filter_ain_vec5_reg_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a27_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a26_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a27_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a26_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a27_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a27_a);

ufir_filter_ain_vec5_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a31_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a30_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a30_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a31_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734 = ufir_filter_ain_vec4_reg_ff_adffs_a4_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a4_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a4_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a4_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a68_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a4_a);

ufir_filter_ain_vec5_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a3_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a3_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a67_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a3_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742 $ ufir_filter_ain_vec4_reg_ff_adffs_a2_a $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742 & ufir_filter_ain_vec4_reg_ff_adffs_a2_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792 # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742 & (ufir_filter_ain_vec4_reg_ff_adffs_a2_a # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a2_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a66_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a2_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694 = ufir_filter_ain_vec1_reg_ff_adffs_a1_a $ ufir_filter_ain_vec2_reg_ff_adffs_a1_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a1_a & !ufir_filter_ain_vec2_reg_ff_adffs_a1_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700 # !ufir_filter_ain_vec1_reg_ff_adffs_a1_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700 # !ufir_filter_ain_vec2_reg_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a1_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a1_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a65_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a1_a);

ufir_filter_ain_vec6_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec6_reg_ff_adffs_a0_a = DFFE(tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec6_reg_ff_adffs_a0_a);

ufir_filter_aff2_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a30_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1008,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a30_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012 = ufir_filter_aff2_adffs_a29_a $ ufir_filter_aff2_adffs_a57_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014 = CARRY(ufir_filter_aff2_adffs_a29_a & ufir_filter_aff2_adffs_a57_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018 # !ufir_filter_aff2_adffs_a29_a 
-- & (ufir_filter_aff2_adffs_a57_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a29_a,
	datab => ufir_filter_aff2_adffs_a57_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1014);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746 = ufir_filter_ain_vec4_reg_ff_adffs_a5_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a5_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a5_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a5_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a736,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a69_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a5_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750 $ ufir_filter_ain_vec4_reg_ff_adffs_a6_a $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750 & ufir_filter_ain_vec4_reg_ff_adffs_a6_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748 # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750 & (ufir_filter_ain_vec4_reg_ff_adffs_a6_a # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a6_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a748,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a70_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a6_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754 = ufir_filter_ain_vec4_reg_ff_adffs_a7_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a7_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a7_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a7_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a752,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a71_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a7_a);

ufir_filter_ain_vec5_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a8_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a8_a);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a72_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a8_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762 = ufir_filter_ain_vec4_reg_ff_adffs_a9_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a9_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a9_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a9_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a73_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a9_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766 $ ufir_filter_ain_vec4_reg_ff_adffs_a10_a $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766 & ufir_filter_ain_vec4_reg_ff_adffs_a10_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764 # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766 & (ufir_filter_ain_vec4_reg_ff_adffs_a10_a # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a10_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a764,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a74_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a10_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770 $ ufir_filter_ain_vec4_reg_ff_adffs_a11_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770 & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768 # !ufir_filter_ain_vec4_reg_ff_adffs_a11_a) # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770 & !ufir_filter_ain_vec4_reg_ff_adffs_a11_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a11_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a768,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a75_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a11_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774 $ ufir_filter_ain_vec4_reg_ff_adffs_a12_a $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774 & ufir_filter_ain_vec4_reg_ff_adffs_a12_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772 # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774 & (ufir_filter_ain_vec4_reg_ff_adffs_a12_a # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a12_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a772,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a76_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a12_a);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778 = ufir_filter_ain_vec4_reg_ff_adffs_a13_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a13_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a13_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a13_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a776,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a780);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a77_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a13_a);

ufir_filter_ain_vec4_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a14_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a14_a);

data_abc_a78_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(78),
	combout => data_abc_a78_a_acombout);

ufir_filter_ain_vec4_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a15_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a13_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a13_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a15_a);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378);

data_abc_a79_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(79),
	combout => data_abc_a79_a_acombout);

ufir_filter_ain_vec4_reg_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a16_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a14_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a16_a);

ufir_filter_ain_vec4_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a31_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a31_a);

ufir_filter_ain_vec5_reg_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a28_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a27_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a28_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a27_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a28_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a28_a);

ufir_filter_ain_vec5_reg_ff_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a30_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a29_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a30_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a29_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a30_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a30_a);

ufir_filter_ain_vec4_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a4_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a4_a);

data_abc_a68_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(68),
	combout => data_abc_a68_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766 $ ufir_filter_ain_vec3_reg_ff_adffs_a3_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766 & ufir_filter_ain_vec3_reg_ff_adffs_a3_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766 & (ufir_filter_ain_vec3_reg_ff_adffs_a3_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a3_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386);

data_abc_a67_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(67),
	combout => data_abc_a67_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770 $ ufir_filter_ain_vec3_reg_ff_adffs_a2_a
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770 # !ufir_filter_ain_vec3_reg_ff_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a742,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a744);

data_abc_a66_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(66),
	combout => data_abc_a66_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a1_a = DFFE(data_abc_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a1_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698 = ufir_filter_ain_vec2_reg_ff_adffs_a0_a $ ufir_filter_ain_vec1_reg_ff_adffs_a0_a
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a0_a & ufir_filter_ain_vec1_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a0_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a700);

data_abc_a65_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(65),
	combout => data_abc_a65_a_acombout);

tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:2:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a64_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a2_aU_RAM_asram_aq_a0_a);

ufir_filter_ain_vec5_reg_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a29_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a28_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a29_a = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a28_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a29_a,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acout_a29_a);

ufir_filter_aff2_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a29_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1012,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a29_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016 = ufir_filter_aff2_adffs_a28_a $ ufir_filter_aff2_adffs_a56_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018 = CARRY(ufir_filter_aff2_adffs_a28_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022 # !ufir_filter_aff2_adffs_a56_a) # 
-- !ufir_filter_aff2_adffs_a28_a & !ufir_filter_aff2_adffs_a56_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a28_a,
	datab => ufir_filter_aff2_adffs_a56_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1018);

ufir_filter_ain_vec4_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a5_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a5_a);

data_abc_a69_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(69),
	combout => data_abc_a69_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778 $ ufir_filter_ain_vec3_reg_ff_adffs_a6_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778 & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748 # !ufir_filter_ain_vec3_reg_ff_adffs_a6_a) # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778 & !ufir_filter_ain_vec3_reg_ff_adffs_a6_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a6_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a750,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752);

data_abc_a70_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(70),
	combout => data_abc_a70_a_acombout);

ufir_filter_ain_vec4_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a7_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a7_a);

data_abc_a71_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(71),
	combout => data_abc_a71_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758 = ufir_filter_ain_vec3_reg_ff_adffs_a8_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a8_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a8_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a8_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 
-- # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410);

data_abc_a72_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(72),
	combout => data_abc_a72_a_acombout);

ufir_filter_ain_vec4_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a9_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a9_a);

data_abc_a73_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(73),
	combout => data_abc_a73_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794 $ ufir_filter_ain_vec3_reg_ff_adffs_a10_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794 & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764 # !ufir_filter_ain_vec3_reg_ff_adffs_a10_a) # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794 & !ufir_filter_ain_vec3_reg_ff_adffs_a10_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a10_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a766,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768);

data_abc_a74_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(74),
	combout => data_abc_a74_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798 $ ufir_filter_ain_vec3_reg_ff_adffs_a11_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798 & ufir_filter_ain_vec3_reg_ff_adffs_a11_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798 & (ufir_filter_ain_vec3_reg_ff_adffs_a11_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a11_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a768,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a770,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772);

data_abc_a75_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(75),
	combout => data_abc_a75_a_acombout);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774 = ufir_filter_ain_vec3_reg_ff_adffs_a12_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a12_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a12_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a12_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a772,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a774,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776);

data_abc_a76_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(76),
	combout => data_abc_a76_a_acombout);

ufir_filter_ain_vec4_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a13_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a13_a);

data_abc_a77_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(77),
	combout => data_abc_a77_a_acombout);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702 = ufir_filter_ain_vec2_reg_ff_adffs_a14_a $ ufir_filter_ain_vec1_reg_ff_adffs_a14_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a14_a & (ufir_filter_ain_vec1_reg_ff_adffs_a14_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808) # !ufir_filter_ain_vec2_reg_ff_adffs_a14_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a14_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a14_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a14_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a44_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a12_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a62_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a);

Offset_Time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(1),
	combout => Offset_Time_a1_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a15_a = DFFE(data_abc_a45_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a45_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a15_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a45_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a13_a);

ufir_filter_ain_vec3_reg_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a16_a = DFFE(data_abc_a46_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a46_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a16_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a46_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a14_a);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432 = a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432);

data_abc_a63_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(63),
	combout => data_abc_a63_a_acombout);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a17_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a17_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712 # !ufir_filter_ain_vec2_reg_ff_adffs_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a17_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a47_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a15_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a18_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a18_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a18_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a18_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a716,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a19_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a19_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720 # !ufir_filter_ain_vec2_reg_ff_adffs_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a19_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a720,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a21_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a21_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732 # !ufir_filter_ain_vec2_reg_ff_adffs_a21_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a21_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a20_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a20_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a20_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a20_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a724,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a732);

ufir_filter_ain_vec3_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a4_a = DFFE(data_abc_a34_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a34_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a4_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a34_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a2_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a52_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766 = ufir_filter_ain_vec2_reg_ff_adffs_a3_a $ ufir_filter_ain_vec1_reg_ff_adffs_a3_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a3_a & !ufir_filter_ain_vec1_reg_ff_adffs_a3_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772 # !ufir_filter_ain_vec2_reg_ff_adffs_a3_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772 # !ufir_filter_ain_vec1_reg_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a3_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a3_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a766,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a50_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770 = ufir_filter_ain_vec2_reg_ff_adffs_a2_a $ ufir_filter_ain_vec1_reg_ff_adffs_a2_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a2_a & (ufir_filter_ain_vec1_reg_ff_adffs_a2_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696) # !ufir_filter_ain_vec2_reg_ff_adffs_a2_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a2_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a2_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a2_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a696,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a770,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a772);

data_abc_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(1),
	combout => data_abc_a1_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a0_a = DFFE(data_abc_a16_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a16_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a0_a);

data_abc_a64_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(64),
	combout => data_abc_a64_a_acombout);

ufir_filter_aff2_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a28_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1016,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a28_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774 = ufir_filter_ain_vec2_reg_ff_adffs_a5_a $ ufir_filter_ain_vec1_reg_ff_adffs_a5_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a5_a & !ufir_filter_ain_vec1_reg_ff_adffs_a5_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764 # !ufir_filter_ain_vec2_reg_ff_adffs_a5_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764 # !ufir_filter_ain_vec1_reg_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a5_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a5_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a35_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a3_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778 = ufir_filter_ain_vec2_reg_ff_adffs_a6_a $ ufir_filter_ain_vec1_reg_ff_adffs_a6_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a6_a & (ufir_filter_ain_vec1_reg_ff_adffs_a6_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776) # !ufir_filter_ain_vec2_reg_ff_adffs_a6_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a6_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a6_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a6_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a776,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a778,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a54_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782 = ufir_filter_ain_vec2_reg_ff_adffs_a7_a $ ufir_filter_ain_vec1_reg_ff_adffs_a7_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a7_a & !ufir_filter_ain_vec1_reg_ff_adffs_a7_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780 # !ufir_filter_ain_vec2_reg_ff_adffs_a7_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780 # !ufir_filter_ain_vec1_reg_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a7_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a7_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a780,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a37_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a5_a);

ufir_filter_ain_vec3_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a8_a = DFFE(data_abc_a38_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a38_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a8_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a56_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a);

ufir_filter_ain_vec3_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a9_a = DFFE(data_abc_a39_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a39_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a9_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a39_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a7_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794 = ufir_filter_ain_vec2_reg_ff_adffs_a10_a $ ufir_filter_ain_vec1_reg_ff_adffs_a10_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a10_a & (ufir_filter_ain_vec1_reg_ff_adffs_a10_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792) # !ufir_filter_ain_vec2_reg_ff_adffs_a10_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a10_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a10_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a10_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a794,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a58_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798 = ufir_filter_ain_vec1_reg_ff_adffs_a11_a $ ufir_filter_ain_vec2_reg_ff_adffs_a11_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a11_a & !ufir_filter_ain_vec2_reg_ff_adffs_a11_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796 # !ufir_filter_ain_vec1_reg_ff_adffs_a11_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796 # !ufir_filter_ain_vec2_reg_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a11_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a11_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a798,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800);

ufir_filter_ain_vec3_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a12_a = DFFE(data_abc_a42_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a42_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a12_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a60_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806 = ufir_filter_ain_vec1_reg_ff_adffs_a13_a $ ufir_filter_ain_vec2_reg_ff_adffs_a13_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a13_a & !ufir_filter_ain_vec2_reg_ff_adffs_a13_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804 # !ufir_filter_ain_vec1_reg_ff_adffs_a13_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804 # !ufir_filter_ain_vec2_reg_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a13_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a13_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a808);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a43_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a11_a);

ufir_filter_ain_vec2_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a14_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a14_a);

data_abc_a62_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(62),
	combout => data_abc_a62_a_acombout);

data_abc_a61_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(61),
	combout => data_abc_a61_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a15_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a15_a);

data_abc_a45_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(45),
	combout => data_abc_a45_a_acombout);

data_abc_a46_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(46),
	combout => data_abc_a46_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a4_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a4_a);

data_abc_a34_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(34),
	combout => data_abc_a34_a_acombout);

data_abc_a52_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(52),
	combout => data_abc_a52_a_acombout);

data_abc_a51_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(51),
	combout => data_abc_a51_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a3_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a3_a);

data_abc_a50_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(50),
	combout => data_abc_a50_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a2_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a2_a);

data_abc_a49_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(49),
	combout => data_abc_a49_a_acombout);

data_abc_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(17),
	combout => data_abc_a17_a_acombout);

data_abc_a48_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(48),
	combout => data_abc_a48_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a5_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a5_a);

data_abc_a53_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(53),
	combout => data_abc_a53_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a6_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a6_a);

data_abc_a54_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(54),
	combout => data_abc_a54_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a7_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a7_a);

data_abc_a55_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(55),
	combout => data_abc_a55_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a8_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a8_a);

data_abc_a38_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(38),
	combout => data_abc_a38_a_acombout);

data_abc_a56_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(56),
	combout => data_abc_a56_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a9_a = DFFE(data_abc_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a9_a);

data_abc_a39_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(39),
	combout => data_abc_a39_a_acombout);

data_abc_a57_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(57),
	combout => data_abc_a57_a_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a10_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a10_a);

data_abc_a58_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(58),
	combout => data_abc_a58_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a11_a = DFFE(data_abc_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a11_a);

data_abc_a59_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(59),
	combout => data_abc_a59_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a12_a = DFFE(data_abc_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a12_a);

data_abc_a42_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(42),
	combout => data_abc_a42_a_acombout);

data_abc_a60_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(60),
	combout => data_abc_a60_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a13_a = DFFE(data_abc_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a13_a);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376 = data_abc_a30_a_acombout $ data_abc_a29_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378 = CARRY(data_abc_a30_a_acombout & !data_abc_a29_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 # !data_abc_a30_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 # !data_abc_a29_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a30_a_acombout,
	datab => data_abc_a29_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380 = data_abc_a30_a_acombout $ data_abc_a31_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382 = CARRY(data_abc_a30_a_acombout & (data_abc_a31_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378) # !data_abc_a30_a_acombout & 
-- data_abc_a31_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a30_a_acombout,
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a378,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384 = data_abc_a19_a_acombout $ data_abc_a20_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 = CARRY(data_abc_a19_a_acombout & !data_abc_a20_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 # !data_abc_a19_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 # !data_abc_a20_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a19_a_acombout,
	datab => data_abc_a20_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a384,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388 = data_abc_a19_a_acombout $ data_abc_a18_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 = CARRY(data_abc_a19_a_acombout & (data_abc_a18_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394) # !data_abc_a19_a_acombout & 
-- data_abc_a18_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a19_a_acombout,
	datab => data_abc_a18_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392 = data_abc_a17_a_acombout $ data_abc_a18_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 = CARRY(data_abc_a17_a_acombout & !data_abc_a18_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 # !data_abc_a17_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 # !data_abc_a18_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a17_a_acombout,
	datab => data_abc_a18_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028 = ufir_filter_aff2_adffs_a25_a $ ufir_filter_aff2_adffs_a53_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030 = CARRY(ufir_filter_aff2_adffs_a25_a & ufir_filter_aff2_adffs_a53_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034 # !ufir_filter_aff2_adffs_a25_a 
-- & (ufir_filter_aff2_adffs_a53_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a25_a,
	datab => ufir_filter_aff2_adffs_a53_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396 = data_abc_a21_a_acombout $ data_abc_a20_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 = CARRY(data_abc_a21_a_acombout & (data_abc_a20_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386) # !data_abc_a21_a_acombout & 
-- data_abc_a20_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a21_a_acombout,
	datab => data_abc_a20_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400 = data_abc_a21_a_acombout $ data_abc_a22_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402 = CARRY(data_abc_a21_a_acombout & !data_abc_a22_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 # !data_abc_a21_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 # !data_abc_a22_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a21_a_acombout,
	datab => data_abc_a22_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404 = data_abc_a23_a_acombout $ data_abc_a22_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 = CARRY(data_abc_a23_a_acombout & (data_abc_a22_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402) # !data_abc_a23_a_acombout & 
-- data_abc_a22_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a23_a_acombout,
	datab => data_abc_a22_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408 = data_abc_a23_a_acombout $ data_abc_a24_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410 = CARRY(data_abc_a23_a_acombout & !data_abc_a24_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 # !data_abc_a23_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 # !data_abc_a24_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a23_a_acombout,
	datab => data_abc_a24_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a408,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410);

data_abc_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(9),
	combout => data_abc_a9_a_acombout);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416 = data_abc_a26_a_acombout $ data_abc_a25_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418 = CARRY(data_abc_a26_a_acombout & !data_abc_a25_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 # !data_abc_a26_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 # !data_abc_a25_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a26_a_acombout,
	datab => data_abc_a25_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418);

data_abc_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(11),
	combout => data_abc_a11_a_acombout);

data_abc_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(12),
	combout => data_abc_a12_a_acombout);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428 = data_abc_a28_a_acombout $ data_abc_a29_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 = CARRY(data_abc_a28_a_acombout & (data_abc_a29_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426) # !data_abc_a28_a_acombout & 
-- data_abc_a29_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a28_a_acombout,
	datab => data_abc_a29_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430);

data_abc_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(13),
	combout => data_abc_a13_a_acombout);

data_abc_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(30),
	combout => data_abc_a30_a_acombout);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432 = a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432);

data_abc_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(19),
	combout => data_abc_a19_a_acombout);

ufir_filter_aff2_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a25_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1028,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a25_a);

data_abc_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(21),
	combout => data_abc_a21_a_acombout);

data_abc_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(23),
	combout => data_abc_a23_a_acombout);

data_abc_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(26),
	combout => data_abc_a26_a_acombout);

data_abc_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(28),
	combout => data_abc_a28_a_acombout);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060 = ufir_filter_aff2_adffs_a17_a $ ufir_filter_aff2_adffs_a45_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062 = CARRY(ufir_filter_aff2_adffs_a17_a & ufir_filter_aff2_adffs_a45_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066 # !ufir_filter_aff2_adffs_a17_a 
-- & (ufir_filter_aff2_adffs_a45_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a17_a,
	datab => ufir_filter_aff2_adffs_a45_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062);

ufir_filter_aff2_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a17_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1060,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a17_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064 = ufir_filter_aff2_adffs_a16_a $ ufir_filter_aff2_adffs_a44_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066 = CARRY(ufir_filter_aff2_adffs_a16_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070 # !ufir_filter_aff2_adffs_a44_a) # 
-- !ufir_filter_aff2_adffs_a16_a & !ufir_filter_aff2_adffs_a44_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a16_a,
	datab => ufir_filter_aff2_adffs_a44_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1066);

ufir_filter_aff2_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a16_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1064,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a16_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068 = ufir_filter_aff2_adffs_a15_a $ ufir_filter_aff2_adffs_a43_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070 = CARRY(ufir_filter_aff2_adffs_a15_a & ufir_filter_aff2_adffs_a43_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074 # !ufir_filter_aff2_adffs_a15_a 
-- & (ufir_filter_aff2_adffs_a43_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a15_a,
	datab => ufir_filter_aff2_adffs_a43_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1070);

ufir_filter_aff2_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a15_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1068,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a15_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076 = ufir_filter_aff2_adffs_a13_a $ ufir_filter_aff2_adffs_a41_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078 = CARRY(ufir_filter_aff2_adffs_a13_a & ufir_filter_aff2_adffs_a41_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082 # !ufir_filter_aff2_adffs_a13_a 
-- & (ufir_filter_aff2_adffs_a41_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a13_a,
	datab => ufir_filter_aff2_adffs_a41_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078);

ufir_filter_aff2_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a13_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1076,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a13_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084 = ufir_filter_aff2_adffs_a11_a $ ufir_filter_aff2_adffs_a39_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086 = CARRY(ufir_filter_aff2_adffs_a11_a & ufir_filter_aff2_adffs_a39_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090 # !ufir_filter_aff2_adffs_a11_a 
-- & (ufir_filter_aff2_adffs_a39_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a11_a,
	datab => ufir_filter_aff2_adffs_a39_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086);

ufir_filter_aff2_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a11_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1084,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a11_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088 = ufir_filter_aff2_adffs_a10_a $ ufir_filter_aff2_adffs_a38_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090 = CARRY(ufir_filter_aff2_adffs_a10_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094 # !ufir_filter_aff2_adffs_a38_a) # 
-- !ufir_filter_aff2_adffs_a10_a & !ufir_filter_aff2_adffs_a38_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a10_a,
	datab => ufir_filter_aff2_adffs_a38_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1090);

ufir_filter_aff2_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a10_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1088,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a10_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100 = ufir_filter_aff2_adffs_a7_a $ ufir_filter_aff2_adffs_a35_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102 = CARRY(ufir_filter_aff2_adffs_a7_a & ufir_filter_aff2_adffs_a35_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106 # !ufir_filter_aff2_adffs_a7_a & 
-- (ufir_filter_aff2_adffs_a35_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a7_a,
	datab => ufir_filter_aff2_adffs_a35_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102);

ufir_filter_aff2_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a7_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1100,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a7_a);

Fir_Out_Ff_adffs_a1_a_a10917_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a_a10917 = TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a1_a # ufir_filter_aff2_adffs_a35_a) # !TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a34_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B9A8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => ufir_filter_aff2_adffs_a35_a,
	datad => ufir_filter_aff2_adffs_a34_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a1_a_a10917);

Fir_Out_Ff_adffs_a1_a_a10918_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a_a10918 = Fir_Out_Ff_adffs_a1_a_a10917 & (ufir_filter_aff2_adffs_a37_a # !TimeConst_Reg_FF_adffs_a1_a) # !Fir_Out_Ff_adffs_a1_a_a10917 & (ufir_filter_aff2_adffs_a36_a & TimeConst_Reg_FF_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "D8AA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a1_a_a10917,
	datab => ufir_filter_aff2_adffs_a37_a,
	datac => ufir_filter_aff2_adffs_a36_a,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a1_a_a10918);

ufir_filter_aas6_aadder_a_a00008_a0 : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_a_a00008 = ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a369,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_a_a00008);

data_abc_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(31),
	combout => data_abc_a31_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

ufir_filter_ain_vec2_reg_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a16_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a16_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AACC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a432,
	datab => data_abc_a31_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a16_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a16_a);

ufir_filter_ain_vec2_reg_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a17_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a16_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a17_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a16_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a17_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a17_a);

ufir_filter_ain_vec2_reg_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a18_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a17_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a18_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a17_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a18_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a18_a);

ufir_filter_ain_vec2_reg_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a19_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a18_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a19_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a18_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a19_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a19_a);

ufir_filter_ain_vec2_reg_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a20_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a19_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a20_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a19_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a20_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a20_a);

ufir_filter_ain_vec2_reg_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a21_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a20_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a21_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a20_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a21_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a21_a);

ufir_filter_ain_vec2_reg_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a22_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a21_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a22_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a21_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a22_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a22_a);

ufir_filter_ain_vec2_reg_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a23_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a22_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a23_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0CC",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a22_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a23_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a23_a);

ufir_filter_ain_vec2_reg_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a24_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a23_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a24_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a23_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a24_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a24_a);

ufir_filter_ain_vec2_reg_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a25_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a24_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a25_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a24_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a25_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a25_a);

ufir_filter_ain_vec2_reg_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a26_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a25_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a26_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a25_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a26_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a26_a);

ufir_filter_ain_vec2_reg_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a27_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a26_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a27_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a26_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a27_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a27_a);

ufir_filter_ain_vec2_reg_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a28_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a27_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a28_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a27_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a28_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a28_a);

ufir_filter_ain_vec2_reg_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a29_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a28_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a29_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a28_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a29_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a29_a);

ufir_filter_ain_vec2_reg_ff_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a30_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a29_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a30_a = CARRY(data_abc_a31_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0AA",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a31_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a29_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a30_a,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a30_a);

ufir_filter_ain_vec2_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a31_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a30_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acout_a30_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a31_a);

data_abc_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(15),
	combout => data_abc_a15_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a31_a = DFFE(data_abc_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a31_a);

data_abc_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(14),
	combout => data_abc_a14_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a14_a = DFFE(data_abc_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a14_a);

data_abc_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(29),
	combout => data_abc_a29_a_acombout);

data_abc_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(27),
	combout => data_abc_a27_a_acombout);

data_abc_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(25),
	combout => data_abc_a25_a_acombout);

data_abc_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(24),
	combout => data_abc_a24_a_acombout);

data_abc_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(22),
	combout => data_abc_a22_a_acombout);

data_abc_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(20),
	combout => data_abc_a20_a_acombout);

data_abc_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(18),
	combout => data_abc_a18_a_acombout);

data_abc_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(16),
	combout => data_abc_a16_a_acombout);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372 = data_abc_a17_a_acombout $ data_abc_a16_a_acombout
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 = CARRY(data_abc_a17_a_acombout & data_abc_a16_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a17_a_acombout,
	datab => data_abc_a16_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412 = data_abc_a24_a_acombout $ data_abc_a25_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 = CARRY(data_abc_a24_a_acombout & (data_abc_a25_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410) # !data_abc_a24_a_acombout & 
-- data_abc_a25_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a24_a_acombout,
	datab => data_abc_a25_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420 = data_abc_a26_a_acombout $ data_abc_a27_a_acombout $ !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 = CARRY(data_abc_a26_a_acombout & (data_abc_a27_a_acombout # !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418) # !data_abc_a26_a_acombout & 
-- data_abc_a27_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a26_a_acombout,
	datab => data_abc_a27_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422);

a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424 = data_abc_a28_a_acombout $ data_abc_a27_a_acombout $ a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422
-- a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426 = CARRY(data_abc_a28_a_acombout & !data_abc_a27_a_acombout & !a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 # !data_abc_a28_a_acombout & 
-- (!a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 # !data_abc_a27_a_acombout))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => data_abc_a28_a_acombout,
	datab => data_abc_a27_a_acombout,
	cin => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424,
	cout => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426);

ufir_filter_ain_vec2_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a13_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a13_a);

ufir_filter_ain_vec2_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a12_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a12_a);

ufir_filter_ain_vec2_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a11_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a11_a);

data_abc_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(10),
	combout => data_abc_a10_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a10_a = DFFE(data_abc_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a10_a);

ufir_filter_ain_vec2_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a9_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a9_a);

data_abc_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(8),
	combout => data_abc_a8_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a8_a = DFFE(data_abc_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a8_a);

data_abc_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(7),
	combout => data_abc_a7_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a7_a = DFFE(data_abc_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a7_a);

data_abc_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(6),
	combout => data_abc_a6_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a6_a = DFFE(data_abc_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a6_a);

data_abc_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(5),
	combout => data_abc_a5_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a5_a = DFFE(data_abc_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a5_a);

data_abc_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(4),
	combout => data_abc_a4_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a4_a = DFFE(data_abc_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a4_a);

data_abc_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(3),
	combout => data_abc_a3_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a3_a = DFFE(data_abc_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a3_a);

data_abc_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(2),
	combout => data_abc_a2_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a2_a = DFFE(data_abc_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a2_a);

ufir_filter_ain_vec2_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec2_reg_ff_adffs_a1_a = DFFE(a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec2_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec2_reg_ff_adffs_a1_a);

data_abc_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(0),
	combout => data_abc_a0_a_acombout);

ufir_filter_ain_vec1_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec1_reg_ff_adffs_a0_a = DFFE(data_abc_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec1_reg_ff_adffs_a0_a);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762 = ufir_filter_ain_vec2_reg_ff_adffs_a4_a $ ufir_filter_ain_vec1_reg_ff_adffs_a4_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a4_a & (ufir_filter_ain_vec1_reg_ff_adffs_a4_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768) # !ufir_filter_ain_vec2_reg_ff_adffs_a4_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a4_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a4_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a4_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a768,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a764);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786 = ufir_filter_ain_vec2_reg_ff_adffs_a8_a $ ufir_filter_ain_vec1_reg_ff_adffs_a8_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a8_a & (ufir_filter_ain_vec1_reg_ff_adffs_a8_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784) # !ufir_filter_ain_vec2_reg_ff_adffs_a8_a & 
-- ufir_filter_ain_vec1_reg_ff_adffs_a8_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a8_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a8_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a784,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a786,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790 = ufir_filter_ain_vec1_reg_ff_adffs_a9_a $ ufir_filter_ain_vec2_reg_ff_adffs_a9_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a9_a & !ufir_filter_ain_vec2_reg_ff_adffs_a9_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788 # !ufir_filter_ain_vec1_reg_ff_adffs_a9_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788 # !ufir_filter_ain_vec2_reg_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a9_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a9_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a788,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a792);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802 = ufir_filter_ain_vec1_reg_ff_adffs_a12_a $ ufir_filter_ain_vec2_reg_ff_adffs_a12_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a12_a & (ufir_filter_ain_vec2_reg_ff_adffs_a12_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800) # !ufir_filter_ain_vec1_reg_ff_adffs_a12_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a12_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a12_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a12_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a800,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a802,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a804);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706 = ufir_filter_ain_vec2_reg_ff_adffs_a15_a $ ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708 = CARRY(ufir_filter_ain_vec2_reg_ff_adffs_a15_a & !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704 # !ufir_filter_ain_vec2_reg_ff_adffs_a15_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec2_reg_ff_adffs_a15_a,
	datab => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a704,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a16_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a16_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a16_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a16_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a708,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a712);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a22_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a22_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a22_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a22_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a728,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a23_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a23_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736 # !ufir_filter_ain_vec2_reg_ff_adffs_a23_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a23_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a736,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a24_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a24_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a24_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a24_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a740,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a25_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a25_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744 # !ufir_filter_ain_vec2_reg_ff_adffs_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a25_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a744,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a26_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a26_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a26_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a26_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a748,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a27_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a27_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752 # !ufir_filter_ain_vec2_reg_ff_adffs_a27_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a27_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a752,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a28_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a28_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a28_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a28_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a756,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a29_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & !ufir_filter_ain_vec2_reg_ff_adffs_a29_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812 # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812 # !ufir_filter_ain_vec2_reg_ff_adffs_a29_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a29_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a812,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ ufir_filter_ain_vec2_reg_ff_adffs_a30_a $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a816 = CARRY(ufir_filter_ain_vec1_reg_ff_adffs_a31_a & (ufir_filter_ain_vec2_reg_ff_adffs_a30_a # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820) # !ufir_filter_ain_vec1_reg_ff_adffs_a31_a & 
-- ufir_filter_ain_vec2_reg_ff_adffs_a30_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datab => ufir_filter_ain_vec2_reg_ff_adffs_a30_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a820,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814,
	cout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a816);

ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758 = ufir_filter_ain_vec1_reg_ff_adffs_a31_a $ (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a816 $ ufir_filter_ain_vec2_reg_ff_adffs_a31_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec1_reg_ff_adffs_a31_a,
	datad => ufir_filter_ain_vec2_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a816,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758);

data_abc_a47_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(47),
	combout => data_abc_a47_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a31_a = DFFE(data_abc_a47_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a47_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a31_a);

data_abc_a44_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(44),
	combout => data_abc_a44_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a14_a = DFFE(data_abc_a44_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a44_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a14_a);

data_abc_a43_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(43),
	combout => data_abc_a43_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a13_a = DFFE(data_abc_a43_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a43_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a13_a);

data_abc_a41_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(41),
	combout => data_abc_a41_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a11_a = DFFE(data_abc_a41_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a41_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a11_a);

data_abc_a40_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(40),
	combout => data_abc_a40_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a10_a = DFFE(data_abc_a40_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a40_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a10_a);

data_abc_a37_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(37),
	combout => data_abc_a37_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a7_a = DFFE(data_abc_a37_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a37_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a7_a);

data_abc_a36_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(36),
	combout => data_abc_a36_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a6_a = DFFE(data_abc_a36_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a36_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a6_a);

data_abc_a35_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(35),
	combout => data_abc_a35_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a5_a = DFFE(data_abc_a35_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a35_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a5_a);

data_abc_a33_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(33),
	combout => data_abc_a33_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a3_a = DFFE(data_abc_a33_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a33_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a3_a);

data_abc_a32_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_data_abc(32),
	combout => data_abc_a32_a_acombout);

ufir_filter_ain_vec3_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec3_reg_ff_adffs_a2_a = DFFE(data_abc_a32_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => data_abc_a32_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec3_reg_ff_adffs_a2_a);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734 = ufir_filter_ain_vec3_reg_ff_adffs_a4_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a4_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a4_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a4_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a762,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a740,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a734,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774 $ ufir_filter_ain_vec3_reg_ff_adffs_a5_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774 & ufir_filter_ain_vec3_reg_ff_adffs_a5_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774 & (ufir_filter_ain_vec3_reg_ff_adffs_a5_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a774,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a5_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a736,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a746,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a748);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782 $ ufir_filter_ain_vec3_reg_ff_adffs_a7_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782 & ufir_filter_ain_vec3_reg_ff_adffs_a7_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782 & (ufir_filter_ain_vec3_reg_ff_adffs_a7_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a782,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a7_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a752,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a754,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a756);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762 = ufir_filter_ain_vec3_reg_ff_adffs_a9_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a9_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a9_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a9_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a790,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a760,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a762,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a764);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806 $ ufir_filter_ain_vec3_reg_ff_adffs_a13_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806 & ufir_filter_ain_vec3_reg_ff_adffs_a13_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806 & (ufir_filter_ain_vec3_reg_ff_adffs_a13_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a806,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a13_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a776,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a778,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702 $ ufir_filter_ain_vec3_reg_ff_adffs_a14_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702 & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780 # !ufir_filter_ain_vec3_reg_ff_adffs_a14_a) # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702 & !ufir_filter_ain_vec3_reg_ff_adffs_a14_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a702,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a14_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a780,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a674,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678 = ufir_filter_ain_vec3_reg_ff_adffs_a15_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a15_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a15_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a15_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a706,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a676,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a678,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682 = ufir_filter_ain_vec3_reg_ff_adffs_a16_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a16_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a16_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a16_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a710,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a680,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714 $ ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714 & ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714 & (ufir_filter_ain_vec3_reg_ff_adffs_a31_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a714,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a684,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718 $ ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718 & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688 # !ufir_filter_ain_vec3_reg_ff_adffs_a31_a) # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718 & !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a718,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a688,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722 $ ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722 & ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722 & (ufir_filter_ain_vec3_reg_ff_adffs_a31_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a722,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a692,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730 $ ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730 & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696 # !ufir_filter_ain_vec3_reg_ff_adffs_a31_a) # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730 & !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a730,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a696,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726 $ ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726 & ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726 & (ufir_filter_ain_vec3_reg_ff_adffs_a31_a # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a726,
	datab => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a704,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a734,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a738,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a708,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a742,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a712,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a746,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a716,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a750,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a720,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a754,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a724,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a810,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a728,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818 $ !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (!ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784 # !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818) # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a818,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a784,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a788 = CARRY(ufir_filter_ain_vec3_reg_ff_adffs_a31_a & ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814 & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796 # 
-- !ufir_filter_ain_vec3_reg_ff_adffs_a31_a & (ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a814,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786,
	cout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a788);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730 = ufir_filter_ain_vec3_reg_ff_adffs_a31_a $ (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a788 $ !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec3_reg_ff_adffs_a31_a,
	datad => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a758,
	cin => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a788,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730);

tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a = DFFE(!tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a,
	cout => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a = DFFE(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a $ tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a,
	cin => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a,
	cout => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a = DFFE(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a $ !tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & !tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a,
	cin => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a,
	cout => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a = DFFE(tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT $ tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a,
	cin => tput_dly_aWADR_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a);

Offset_Time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(0),
	combout => Offset_Time_a0_a_acombout);

tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93_I : apex20ke_lcell
-- Equation(s):
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93 = tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a $ Offset_Time_a0_a_acombout
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95 = CARRY(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a # !Offset_Time_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a0_a,
	datab => Offset_Time_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a93,
	cout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95);

tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97_I : apex20ke_lcell
-- Equation(s):
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97 = Offset_Time_a1_a_acombout $ tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a $ !tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99 = CARRY(Offset_Time_a1_a_acombout & (!tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95 # !tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a) # !Offset_Time_a1_a_acombout & 
-- !tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a & !tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Offset_Time_a1_a_acombout,
	datab => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a1_a,
	cin => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a95,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a97,
	cout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99);

Offset_Time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(2),
	combout => Offset_Time_a2_a_acombout);

tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101_I : apex20ke_lcell
-- Equation(s):
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101 = tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a $ Offset_Time_a2_a_acombout $ tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a103 = CARRY(tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & (!tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99 # !Offset_Time_a2_a_acombout) # 
-- !tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a & !Offset_Time_a2_a_acombout & !tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a2_a,
	datab => Offset_Time_a2_a_acombout,
	cin => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a99,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a101,
	cout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a103);

Offset_Time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Offset_Time(3),
	combout => Offset_Time_a3_a_acombout);

tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105_I : apex20ke_lcell
-- Equation(s):
-- tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105 = tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a $ tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a103 $ !Offset_Time_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => tput_dly_aWADR_CNTR_awysi_counter_asload_path_a3_a,
	datad => Offset_Time_a3_a_acombout,
	cin => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a103,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => tput_dly_aRADR_CNTR_aadder_aresult_node_acs_buffer_a0_a_a105);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a42_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a10_a);

ufir_filter_ain_vec4_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a12_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a12_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a41_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a9_a);

ufir_filter_ain_vec4_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a11_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a11_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a40_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a8_a);

ufir_filter_ain_vec4_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a10_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a10_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a38_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a6_a);

ufir_filter_ain_vec4_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a8_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a8_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a36_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a4_a);

ufir_filter_ain_vec4_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a6_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a6_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a33_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a1_a);

ufir_filter_ain_vec4_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a3_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a3_a);

tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:0:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a32_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a0_a);

ufir_filter_ain_vec4_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec4_reg_ff_adffs_a2_a = DFFE(tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a0_aU_RAM_asram_aq_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec4_reg_ff_adffs_a2_a);

ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790 = VCC

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a790,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a790,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a792);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738 $ ufir_filter_ain_vec4_reg_ff_adffs_a3_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738 & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744 # !ufir_filter_ain_vec4_reg_ff_adffs_a3_a) # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738 & !ufir_filter_ain_vec4_reg_ff_adffs_a3_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a738,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a3_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a744,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a740);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758 = ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758 $ ufir_filter_ain_vec4_reg_ff_adffs_a8_a $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760 = CARRY(ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758 & ufir_filter_ain_vec4_reg_ff_adffs_a8_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756 # 
-- !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758 & (ufir_filter_ain_vec4_reg_ff_adffs_a8_a # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a758,
	datab => ufir_filter_ain_vec4_reg_ff_adffs_a8_a,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a756,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a760);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682 = ufir_filter_ain_vec4_reg_ff_adffs_a16_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a16_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a16_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a16_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a682,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a680,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a686,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a684,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a690,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a688,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a694,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a692,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a702,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a696,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a698,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a704,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a706,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a710,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a708,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a714,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a712,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a718,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a716,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a722,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a720,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a726,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a724,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a782,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a728,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784 # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a794,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a784,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786 $ !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a788 = CARRY(ufir_filter_ain_vec4_reg_ff_adffs_a31_a & (!ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796 # !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786) # 
-- !ufir_filter_ain_vec4_reg_ff_adffs_a31_a & !ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786 & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a786,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786,
	cout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a788);

ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730 = ufir_filter_ain_vec4_reg_ff_adffs_a31_a $ (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a788 $ ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec4_reg_ff_adffs_a31_a,
	datad => ufir_filter_aas2_aadder_aresult_node_acs_buffer_a2_a_a730,
	cin => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a788,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a63_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a15_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a61_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a59_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a57_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a55_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a53_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a51_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a49_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 
-- # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a2_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a1_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a394,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a390);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 
-- # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a3_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a386,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a4_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a382,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 
-- # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a5_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a398,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a6_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a7_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a402,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a406);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a8_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a410,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a & 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a9_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a414,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a10_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a418,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a & 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422 # 
-- !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a11_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a422,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a $ !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a & (tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a # 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426) # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a & tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a & !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a12_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a426,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430);

a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372_I : apex20ke_lcell
-- Equation(s):
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372 = tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a $ tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a $ a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430
-- a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374 = CARRY(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a & !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a & 
-- !a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 # !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a & (!a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430 # 
-- !tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a14_a,
	datab => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a13_a,
	cin => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a430,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372,
	cout => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a374);

ufir_filter_ain_vec5_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a15_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a376,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a15_a);

ufir_filter_ain_vec5_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a14_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a372,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a14_a);

ufir_filter_ain_vec5_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a13_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a428,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a13_a);

ufir_filter_ain_vec5_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a12_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a424,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a12_a);

ufir_filter_ain_vec5_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a11_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a420,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a11_a);

ufir_filter_ain_vec5_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a10_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a416,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a10_a);

ufir_filter_ain_vec5_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a9_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a412,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a9_a);

ufir_filter_ain_vec5_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a7_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a404,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a7_a);

ufir_filter_ain_vec5_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a6_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a400,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a6_a);

ufir_filter_ain_vec5_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a5_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a396,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a5_a);

ufir_filter_ain_vec5_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a4_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a380,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a4_a);

ufir_filter_ain_vec5_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a2_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a388,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a2_a);

ufir_filter_ain_vec5_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a1_a = DFFE(a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => a_aCoef_1_Gen_au_vec5_Gen_aas1_aadder_aresult_node_acs_buffer_a1_a_a392,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a1_a);

tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "fir_tput:tput_dly|lpm_ram_dp:\flat_top_gen:1:U_RAM|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => data_abc_a48_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => VCC,
	re => VCC,
	waddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a);

ufir_filter_ain_vec5_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_ain_vec5_reg_ff_adffs_a0_a = DFFE(tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => tput_dly_a_aflat_top_gen_a1_aU_RAM_asram_aq_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_ain_vec5_reg_ff_adffs_a0_a);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698 $ ufir_filter_ain_vec5_reg_ff_adffs_a0_a
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698 & ufir_filter_ain_vec5_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a698,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a814,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766 = ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694 $ ufir_filter_ain_vec5_reg_ff_adffs_a1_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768 = CARRY(ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694 & !ufir_filter_ain_vec5_reg_ff_adffs_a1_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816 # 
-- !ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816 # !ufir_filter_ain_vec5_reg_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas1_aadder_aresult_node_acs_buffer_a0_a_a694,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a1_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a816,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a766,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742 $ ufir_filter_ain_vec5_reg_ff_adffs_a2_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742 & (ufir_filter_ain_vec5_reg_ff_adffs_a2_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742 & ufir_filter_ain_vec5_reg_ff_adffs_a2_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a742,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a2_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a768,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a762,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758 = ufir_filter_ain_vec5_reg_ff_adffs_a3_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a3_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a3_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a3_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a738,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a764,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a758,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734 $ ufir_filter_ain_vec5_reg_ff_adffs_a4_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734 & (ufir_filter_ain_vec5_reg_ff_adffs_a4_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734 & ufir_filter_ain_vec5_reg_ff_adffs_a4_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a734,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a4_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a760,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a754,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746 $ ufir_filter_ain_vec5_reg_ff_adffs_a5_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746 & !ufir_filter_ain_vec5_reg_ff_adffs_a5_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756 # !ufir_filter_ain_vec5_reg_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a746,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a5_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a756,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a770,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750 $ ufir_filter_ain_vec5_reg_ff_adffs_a6_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750 & (ufir_filter_ain_vec5_reg_ff_adffs_a6_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750 & ufir_filter_ain_vec5_reg_ff_adffs_a6_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a750,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a6_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a772,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a774,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754 $ ufir_filter_ain_vec5_reg_ff_adffs_a7_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754 & !ufir_filter_ain_vec5_reg_ff_adffs_a7_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776 # !ufir_filter_ain_vec5_reg_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a754,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a7_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a776,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a778,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782 = ufir_filter_ain_vec5_reg_ff_adffs_a8_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a8_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a8_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a8_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a758,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a780,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762 $ ufir_filter_ain_vec5_reg_ff_adffs_a9_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762 & !ufir_filter_ain_vec5_reg_ff_adffs_a9_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784 # !ufir_filter_ain_vec5_reg_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a762,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a9_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a784,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a786,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766 $ ufir_filter_ain_vec5_reg_ff_adffs_a10_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766 & (ufir_filter_ain_vec5_reg_ff_adffs_a10_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766 & ufir_filter_ain_vec5_reg_ff_adffs_a10_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a766,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a10_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a788,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a790,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770 $ ufir_filter_ain_vec5_reg_ff_adffs_a11_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770 & !ufir_filter_ain_vec5_reg_ff_adffs_a11_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792 # !ufir_filter_ain_vec5_reg_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a770,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a11_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a792,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774 $ ufir_filter_ain_vec5_reg_ff_adffs_a12_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774 & (ufir_filter_ain_vec5_reg_ff_adffs_a12_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774 & ufir_filter_ain_vec5_reg_ff_adffs_a12_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a774,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a12_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a796,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778 $ ufir_filter_ain_vec5_reg_ff_adffs_a13_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778 & !ufir_filter_ain_vec5_reg_ff_adffs_a13_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800 # !ufir_filter_ain_vec5_reg_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a778,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a13_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a800,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674 $ ufir_filter_ain_vec5_reg_ff_adffs_a14_a $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674 & (ufir_filter_ain_vec5_reg_ff_adffs_a14_a # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804) # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674 & ufir_filter_ain_vec5_reg_ff_adffs_a14_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a674,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a14_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a804,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a694,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698 = ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678 $ ufir_filter_ain_vec5_reg_ff_adffs_a15_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700 = CARRY(ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678 & !ufir_filter_ain_vec5_reg_ff_adffs_a15_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696 # 
-- !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678 & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696 # !ufir_filter_ain_vec5_reg_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a678,
	datab => ufir_filter_ain_vec5_reg_ff_adffs_a15_a,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a696,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a698,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702 = ufir_filter_ain_vec5_reg_ff_adffs_a16_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a16_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a16_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a16_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a682,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a700,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a702,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706 = ufir_filter_ain_vec5_reg_ff_adffs_a17_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a17_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a17_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a17_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a686,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a704,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710 = ufir_filter_ain_vec5_reg_ff_adffs_a18_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a18_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a18_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a18_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a690,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a708,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a710,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714 = ufir_filter_ain_vec5_reg_ff_adffs_a19_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a19_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a19_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a19_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a694,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a712,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722 = ufir_filter_ain_vec5_reg_ff_adffs_a20_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a20_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a20_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a20_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a702,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a716,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a722,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718 = ufir_filter_ain_vec5_reg_ff_adffs_a21_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a21_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a21_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a21_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a698,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a724,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726 = ufir_filter_ain_vec5_reg_ff_adffs_a22_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a22_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a22_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a22_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a706,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a720,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a726,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730 = ufir_filter_ain_vec5_reg_ff_adffs_a23_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a23_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a23_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a23_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a710,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a728,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734 = ufir_filter_ain_vec5_reg_ff_adffs_a24_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a24_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a24_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a24_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a714,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a732,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a734,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738 = ufir_filter_ain_vec5_reg_ff_adffs_a25_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a25_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a25_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a25_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a718,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a736,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a738,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742 = ufir_filter_ain_vec5_reg_ff_adffs_a26_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a26_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a26_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a26_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a722,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a740,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a742,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746 = ufir_filter_ain_vec5_reg_ff_adffs_a27_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a27_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a27_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a27_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a726,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a744,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a746,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806 = ufir_filter_ain_vec5_reg_ff_adffs_a28_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a28_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a28_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a28_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a782,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a748,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a806,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818 = ufir_filter_ain_vec5_reg_ff_adffs_a29_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a29_a & !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808 # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a29_a & (!ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808 # !ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a29_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a794,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a808,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a818,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810 = ufir_filter_ain_vec5_reg_ff_adffs_a30_a $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786 $ !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a812 = CARRY(ufir_filter_ain_vec5_reg_ff_adffs_a30_a & (ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820) # 
-- !ufir_filter_ain_vec5_reg_ff_adffs_a30_a & ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786 & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a30_a,
	datab => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a786,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a820,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a810,
	cout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a812);

ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750 = ufir_filter_ain_vec5_reg_ff_adffs_a31_a $ (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a812 $ ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec5_reg_ff_adffs_a31_a,
	datad => ufir_filter_aas3_aadder_aresult_node_acs_buffer_a2_a_a730,
	cin => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a812,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750);

ufir_filter_aff1_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a8_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a8_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782 $ !ufir_filter_aff1_adffs_a7_a_a257, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a8_a_a260 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a8_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782 # !ufir_filter_aff1_adffs_a7_a_a257) # !ufir_filter_ain_vec6_reg_ff_adffs_a8_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782 & !ufir_filter_aff1_adffs_a7_a_a257)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a8_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a782,
	cin => ufir_filter_aff1_adffs_a7_a_a257,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a8_a,
	cout => ufir_filter_aff1_adffs_a8_a_a260);

ufir_filter_aff1_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a11_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a11_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794 $ ufir_filter_aff1_adffs_a10_a_a266, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a11_a_a269 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a11_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794 & !ufir_filter_aff1_adffs_a10_a_a266 # !ufir_filter_ain_vec6_reg_ff_adffs_a11_a & 
-- (!ufir_filter_aff1_adffs_a10_a_a266 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a11_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a794,
	cin => ufir_filter_aff1_adffs_a10_a_a266,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a11_a,
	cout => ufir_filter_aff1_adffs_a11_a_a269);

ufir_filter_aff1_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a12_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a12_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798 $ !ufir_filter_aff1_adffs_a11_a_a269, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a12_a_a272 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a12_a & (ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798 # !ufir_filter_aff1_adffs_a11_a_a269) # !ufir_filter_ain_vec6_reg_ff_adffs_a12_a & 
-- ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798 & !ufir_filter_aff1_adffs_a11_a_a269)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a12_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a798,
	cin => ufir_filter_aff1_adffs_a11_a_a269,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a12_a,
	cout => ufir_filter_aff1_adffs_a12_a_a272);

ufir_filter_aff1_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a13_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a13_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802 $ ufir_filter_aff1_adffs_a12_a_a272, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a13_a_a275 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a13_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802 & !ufir_filter_aff1_adffs_a12_a_a272 # !ufir_filter_ain_vec6_reg_ff_adffs_a13_a & 
-- (!ufir_filter_aff1_adffs_a12_a_a272 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a13_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a802,
	cin => ufir_filter_aff1_adffs_a12_a_a272,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a13_a,
	cout => ufir_filter_aff1_adffs_a13_a_a275);

ufir_filter_aff1_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a17_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706 $ ufir_filter_aff1_adffs_a16_a_a200, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a17_a_a203 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706 & !ufir_filter_aff1_adffs_a16_a_a200 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a16_a_a200 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a706,
	cin => ufir_filter_aff1_adffs_a16_a_a200,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a17_a,
	cout => ufir_filter_aff1_adffs_a17_a_a203);

ufir_filter_aff1_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a19_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714 $ ufir_filter_aff1_adffs_a18_a_a206, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a19_a_a209 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714 & !ufir_filter_aff1_adffs_a18_a_a206 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a18_a_a206 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a714,
	cin => ufir_filter_aff1_adffs_a18_a_a206,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a19_a,
	cout => ufir_filter_aff1_adffs_a19_a_a209);

ufir_filter_aff1_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a21_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718 $ ufir_filter_aff1_adffs_a20_a_a215, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a21_a_a212 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718 & !ufir_filter_aff1_adffs_a20_a_a215 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a20_a_a215 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a718,
	cin => ufir_filter_aff1_adffs_a20_a_a215,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a21_a,
	cout => ufir_filter_aff1_adffs_a21_a_a212);

ufir_filter_aff1_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a23_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730 $ ufir_filter_aff1_adffs_a22_a_a218, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff1_adffs_a23_a_a221 = CARRY(ufir_filter_ain_vec6_reg_ff_adffs_a31_a & !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730 & !ufir_filter_aff1_adffs_a22_a_a218 # !ufir_filter_ain_vec6_reg_ff_adffs_a31_a & 
-- (!ufir_filter_aff1_adffs_a22_a_a218 # !ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datab => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a730,
	cin => ufir_filter_aff1_adffs_a22_a_a218,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a23_a,
	cout => ufir_filter_aff1_adffs_a23_a_a221);

ufir_filter_aff1_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff1_adffs_a31_a = DFFE(ufir_filter_ain_vec6_reg_ff_adffs_a31_a $ (ufir_filter_aff1_adffs_a30_a_a281 $ ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_ain_vec6_reg_ff_adffs_a31_a,
	datad => ufir_filter_aas4_aadder_aresult_node_acs_buffer_a0_a_a750,
	cin => ufir_filter_aff1_adffs_a30_a_a281,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff1_adffs_a31_a);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463 = CARRY(ufir_filter_aas6_aadder_a_a00008)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AA",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aas6_aadder_a_a00008,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a461,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401 = ufir_filter_aff1_adffs_a14_a $ ufir_filter_aff2_adffs_a46_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403 = CARRY(ufir_filter_aff1_adffs_a14_a & !ufir_filter_aff2_adffs_a46_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463 # !ufir_filter_aff1_adffs_a14_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463 # !ufir_filter_aff2_adffs_a46_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a14_a,
	datab => ufir_filter_aff2_adffs_a46_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a463,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365 = ufir_filter_aff1_adffs_a0_a $ ufir_filter_aff2_adffs_a32_a
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367 = CARRY(ufir_filter_aff1_adffs_a0_a & ufir_filter_aff2_adffs_a32_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a32_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425 = ufir_filter_aff2_adffs_a53_a $ ufir_filter_aff1_adffs_a21_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427 = CARRY(ufir_filter_aff2_adffs_a53_a & (ufir_filter_aff1_adffs_a21_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431) # !ufir_filter_aff2_adffs_a53_a & 
-- ufir_filter_aff1_adffs_a21_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a53_a,
	datab => ufir_filter_aff1_adffs_a21_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421 = ufir_filter_aff2_adffs_a51_a $ ufir_filter_aff1_adffs_a19_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423 = CARRY(ufir_filter_aff2_adffs_a51_a & (ufir_filter_aff1_adffs_a19_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419) # !ufir_filter_aff2_adffs_a51_a & 
-- ufir_filter_aff1_adffs_a19_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a51_a,
	datab => ufir_filter_aff1_adffs_a19_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413 = ufir_filter_aff2_adffs_a49_a $ ufir_filter_aff1_adffs_a17_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415 = CARRY(ufir_filter_aff2_adffs_a49_a & (ufir_filter_aff1_adffs_a17_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411) # !ufir_filter_aff2_adffs_a49_a & 
-- ufir_filter_aff1_adffs_a17_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a49_a,
	datab => ufir_filter_aff1_adffs_a17_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415);

AD_MR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_AD_MR,
	combout => AD_MR_acombout);

ufir_filter_aff2_adffs_a46_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a46_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401 $ !ufir_filter_aff2_adffs_a46_a_a983, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a46_a_a924 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a46_a_a983 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401 & !ufir_filter_aff2_adffs_a46_a_a983)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a401,
	cin => ufir_filter_aff2_adffs_a46_a_a983,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a46_a,
	cout => ufir_filter_aff2_adffs_a46_a_a924);

ufir_filter_aff2_adffs_a47_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a47_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405 $ ufir_filter_aff2_adffs_a46_a_a924, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a47_a_a927 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405 & !ufir_filter_aff2_adffs_a46_a_a924 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405 # !ufir_filter_aff2_adffs_a46_a_a924))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405,
	cin => ufir_filter_aff2_adffs_a46_a_a924,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a47_a,
	cout => ufir_filter_aff2_adffs_a47_a_a927);

ufir_filter_aff2_adffs_a48_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a48_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409 $ !ufir_filter_aff2_adffs_a47_a_a927, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a48_a_a930 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a47_a_a927 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409 & !ufir_filter_aff2_adffs_a47_a_a927)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409,
	cin => ufir_filter_aff2_adffs_a47_a_a927,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a48_a,
	cout => ufir_filter_aff2_adffs_a48_a_a930);

ufir_filter_aff2_adffs_a49_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a49_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413 $ ufir_filter_aff2_adffs_a48_a_a930, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a49_a_a933 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413 & !ufir_filter_aff2_adffs_a48_a_a930 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413 # !ufir_filter_aff2_adffs_a48_a_a930))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a413,
	cin => ufir_filter_aff2_adffs_a48_a_a930,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a49_a,
	cout => ufir_filter_aff2_adffs_a49_a_a933);

ufir_filter_aff2_adffs_a50_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a50_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417 $ !ufir_filter_aff2_adffs_a49_a_a933, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a50_a_a936 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a49_a_a933 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417 & !ufir_filter_aff2_adffs_a49_a_a933)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417,
	cin => ufir_filter_aff2_adffs_a49_a_a933,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a50_a,
	cout => ufir_filter_aff2_adffs_a50_a_a936);

ufir_filter_aff2_adffs_a51_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a51_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421 $ ufir_filter_aff2_adffs_a50_a_a936, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a51_a_a939 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421 & !ufir_filter_aff2_adffs_a50_a_a936 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421 # !ufir_filter_aff2_adffs_a50_a_a936))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a421,
	cin => ufir_filter_aff2_adffs_a50_a_a936,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a51_a,
	cout => ufir_filter_aff2_adffs_a51_a_a939);

ufir_filter_aff2_adffs_a52_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a52_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429 $ !ufir_filter_aff2_adffs_a51_a_a939, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a52_a_a945 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a51_a_a939 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429 & !ufir_filter_aff2_adffs_a51_a_a939)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429,
	cin => ufir_filter_aff2_adffs_a51_a_a939,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a52_a,
	cout => ufir_filter_aff2_adffs_a52_a_a945);

ufir_filter_aff2_adffs_a53_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a53_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425 $ ufir_filter_aff2_adffs_a52_a_a945, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a53_a_a942 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425 & !ufir_filter_aff2_adffs_a52_a_a945 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425 # !ufir_filter_aff2_adffs_a52_a_a945))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a425,
	cin => ufir_filter_aff2_adffs_a52_a_a945,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a53_a,
	cout => ufir_filter_aff2_adffs_a53_a_a942);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357 = ufir_filter_aff2_adffs_a44_a $ ufir_filter_aff1_adffs_a12_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359 = CARRY(ufir_filter_aff2_adffs_a44_a & (ufir_filter_aff1_adffs_a12_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355) # 
-- !ufir_filter_aff2_adffs_a44_a & ufir_filter_aff1_adffs_a12_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a44_a,
	datab => ufir_filter_aff1_adffs_a12_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a359);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353 = ufir_filter_aff2_adffs_a43_a $ ufir_filter_aff1_adffs_a11_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355 = CARRY(ufir_filter_aff2_adffs_a43_a & !ufir_filter_aff1_adffs_a11_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351 # !ufir_filter_aff2_adffs_a43_a 
-- & (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351 # !ufir_filter_aff1_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a43_a,
	datab => ufir_filter_aff1_adffs_a11_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a355);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341 = ufir_filter_aff2_adffs_a40_a $ ufir_filter_aff1_adffs_a8_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343 = CARRY(ufir_filter_aff2_adffs_a40_a & (ufir_filter_aff1_adffs_a8_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339) # !ufir_filter_aff2_adffs_a40_a 
-- & ufir_filter_aff1_adffs_a8_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => ufir_filter_aff1_adffs_a8_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996 = ufir_filter_aff2_adffs_a60_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998 = CARRY(ufir_filter_aff2_adffs_a60_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002 # !ufir_filter_aff2_adffs_a60_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a60_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a365,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1002,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956 = ufir_filter_aff2_adffs_a61_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958 = CARRY(ufir_filter_aff2_adffs_a61_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325) # !ufir_filter_aff2_adffs_a61_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a61_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a998,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952 = ufir_filter_aff2_adffs_a62_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954 = CARRY(ufir_filter_aff2_adffs_a62_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958 # !ufir_filter_aff2_adffs_a62_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a62_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a958,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a954,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950 # !ufir_filter_aff2_adffs_a63_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a950,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a946,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962 # !ufir_filter_aff2_adffs_a63_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a962,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a966,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970 # !ufir_filter_aff2_adffs_a63_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a341,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a970,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a974,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978 # !ufir_filter_aff2_adffs_a63_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a978,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a353,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a982,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357 $ 
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986 # !ufir_filter_aff2_adffs_a63_a & (ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357 # 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a357,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a986,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992 = ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361 $ 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a994 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990 # 
-- !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361) # !ufir_filter_aff2_adffs_a63_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361 & 
-- !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a361,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a990,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a994);

ufir_filter_aff2_adffs_a45_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a45_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a992,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a45_a);

ufir_filter_aff2_adffs_a44_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a44_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a988,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a44_a);

ufir_filter_aff2_adffs_a43_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a43_a = DFFE(!AD_MR_acombout & ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => AD_MR_acombout,
	datad => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a984,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a43_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112 = ufir_filter_aff2_adffs_a32_a $ ufir_filter_aff2_adffs_a4_a
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114 = CARRY(ufir_filter_aff2_adffs_a4_a # !ufir_filter_aff2_adffs_a32_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66DD",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a32_a,
	datab => ufir_filter_aff2_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114);

ufir_filter_aff2_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a4_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1112,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a4_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108 = ufir_filter_aff2_adffs_a33_a $ ufir_filter_aff2_adffs_a5_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110 = CARRY(ufir_filter_aff2_adffs_a33_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114 # !ufir_filter_aff2_adffs_a5_a) # 
-- !ufir_filter_aff2_adffs_a33_a & !ufir_filter_aff2_adffs_a5_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a33_a,
	datab => ufir_filter_aff2_adffs_a5_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1114,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110);

ufir_filter_aff2_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a5_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1108,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a5_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104 = ufir_filter_aff2_adffs_a34_a $ ufir_filter_aff2_adffs_a6_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106 = CARRY(ufir_filter_aff2_adffs_a34_a & ufir_filter_aff2_adffs_a6_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110 # !ufir_filter_aff2_adffs_a34_a & 
-- (ufir_filter_aff2_adffs_a6_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a34_a,
	datab => ufir_filter_aff2_adffs_a6_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1110,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1106);

ufir_filter_aff2_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a6_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1104,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a6_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096 = ufir_filter_aff2_adffs_a36_a $ ufir_filter_aff2_adffs_a8_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098 = CARRY(ufir_filter_aff2_adffs_a36_a & ufir_filter_aff2_adffs_a8_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102 # !ufir_filter_aff2_adffs_a36_a & 
-- (ufir_filter_aff2_adffs_a8_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a36_a,
	datab => ufir_filter_aff2_adffs_a8_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1102,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098);

ufir_filter_aff2_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a8_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1096,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a8_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092 = ufir_filter_aff2_adffs_a37_a $ ufir_filter_aff2_adffs_a9_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094 = CARRY(ufir_filter_aff2_adffs_a37_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098 # !ufir_filter_aff2_adffs_a9_a) # 
-- !ufir_filter_aff2_adffs_a37_a & !ufir_filter_aff2_adffs_a9_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => ufir_filter_aff2_adffs_a9_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1098,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1094);

ufir_filter_aff2_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a9_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1092,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a9_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080 = ufir_filter_aff2_adffs_a40_a $ ufir_filter_aff2_adffs_a12_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082 = CARRY(ufir_filter_aff2_adffs_a40_a & ufir_filter_aff2_adffs_a12_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086 # !ufir_filter_aff2_adffs_a40_a 
-- & (ufir_filter_aff2_adffs_a12_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => ufir_filter_aff2_adffs_a12_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1086,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1082);

ufir_filter_aff2_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a12_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1080,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a12_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072 = ufir_filter_aff2_adffs_a42_a $ ufir_filter_aff2_adffs_a14_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074 = CARRY(ufir_filter_aff2_adffs_a42_a & ufir_filter_aff2_adffs_a14_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078 # !ufir_filter_aff2_adffs_a42_a 
-- & (ufir_filter_aff2_adffs_a14_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => ufir_filter_aff2_adffs_a14_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1078,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1074);

ufir_filter_aff2_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a14_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1072,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a14_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056 = ufir_filter_aff2_adffs_a46_a $ ufir_filter_aff2_adffs_a18_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058 = CARRY(ufir_filter_aff2_adffs_a46_a & ufir_filter_aff2_adffs_a18_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062 # !ufir_filter_aff2_adffs_a46_a 
-- & (ufir_filter_aff2_adffs_a18_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a46_a,
	datab => ufir_filter_aff2_adffs_a18_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1062,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058);

ufir_filter_aff2_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a18_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1056,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a18_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052 = ufir_filter_aff2_adffs_a47_a $ ufir_filter_aff2_adffs_a19_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054 = CARRY(ufir_filter_aff2_adffs_a47_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058 # !ufir_filter_aff2_adffs_a19_a) # 
-- !ufir_filter_aff2_adffs_a47_a & !ufir_filter_aff2_adffs_a19_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a47_a,
	datab => ufir_filter_aff2_adffs_a19_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1058,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054);

ufir_filter_aff2_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a19_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1052,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a19_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048 = ufir_filter_aff2_adffs_a48_a $ ufir_filter_aff2_adffs_a20_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050 = CARRY(ufir_filter_aff2_adffs_a48_a & ufir_filter_aff2_adffs_a20_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054 # !ufir_filter_aff2_adffs_a48_a 
-- & (ufir_filter_aff2_adffs_a20_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a48_a,
	datab => ufir_filter_aff2_adffs_a20_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1054,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050);

ufir_filter_aff2_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a20_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1048,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a20_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044 = ufir_filter_aff2_adffs_a49_a $ ufir_filter_aff2_adffs_a21_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046 = CARRY(ufir_filter_aff2_adffs_a49_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050 # !ufir_filter_aff2_adffs_a21_a) # 
-- !ufir_filter_aff2_adffs_a49_a & !ufir_filter_aff2_adffs_a21_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a49_a,
	datab => ufir_filter_aff2_adffs_a21_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1050,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046);

ufir_filter_aff2_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a21_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1044,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a21_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040 = ufir_filter_aff2_adffs_a50_a $ ufir_filter_aff2_adffs_a22_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042 = CARRY(ufir_filter_aff2_adffs_a50_a & ufir_filter_aff2_adffs_a22_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046 # !ufir_filter_aff2_adffs_a50_a 
-- & (ufir_filter_aff2_adffs_a22_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a50_a,
	datab => ufir_filter_aff2_adffs_a22_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1046,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042);

ufir_filter_aff2_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a22_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1040,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a22_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036 = ufir_filter_aff2_adffs_a51_a $ ufir_filter_aff2_adffs_a23_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038 = CARRY(ufir_filter_aff2_adffs_a51_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042 # !ufir_filter_aff2_adffs_a23_a) # 
-- !ufir_filter_aff2_adffs_a51_a & !ufir_filter_aff2_adffs_a23_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a51_a,
	datab => ufir_filter_aff2_adffs_a23_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1042,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038);

ufir_filter_aff2_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a23_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1036,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a23_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032 = ufir_filter_aff2_adffs_a52_a $ ufir_filter_aff2_adffs_a24_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034 = CARRY(ufir_filter_aff2_adffs_a52_a & ufir_filter_aff2_adffs_a24_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038 # !ufir_filter_aff2_adffs_a52_a 
-- & (ufir_filter_aff2_adffs_a24_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a52_a,
	datab => ufir_filter_aff2_adffs_a24_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1038,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1034);

ufir_filter_aff2_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a24_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1032,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a24_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024 = ufir_filter_aff2_adffs_a54_a $ ufir_filter_aff2_adffs_a26_a $ ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026 = CARRY(ufir_filter_aff2_adffs_a54_a & ufir_filter_aff2_adffs_a26_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030 # !ufir_filter_aff2_adffs_a54_a 
-- & (ufir_filter_aff2_adffs_a26_a # !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a54_a,
	datab => ufir_filter_aff2_adffs_a26_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1030,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026);

ufir_filter_aff2_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a26_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1024,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a26_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020 = ufir_filter_aff2_adffs_a55_a $ ufir_filter_aff2_adffs_a27_a $ !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022 = CARRY(ufir_filter_aff2_adffs_a55_a & (!ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026 # !ufir_filter_aff2_adffs_a27_a) # 
-- !ufir_filter_aff2_adffs_a55_a & !ufir_filter_aff2_adffs_a27_a & !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a55_a,
	datab => ufir_filter_aff2_adffs_a27_a,
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1026,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020,
	cout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1022);

ufir_filter_aff2_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a27_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1020,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a27_a);

ufir_filter_aff2_adffs_a32_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a32_a = DFFE(!AD_MR_acombout & ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(ufir_filter_aff2_adffs_a32_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "44F0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => AD_MR_acombout,
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a996,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a32_a,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325 = ufir_filter_aff1_adffs_a1_a $ ufir_filter_aff2_adffs_a33_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327 = CARRY(ufir_filter_aff1_adffs_a1_a & !ufir_filter_aff2_adffs_a33_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367 # !ufir_filter_aff1_adffs_a1_a & 
-- (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367 # !ufir_filter_aff2_adffs_a33_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a33_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a367,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a325,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327);

ufir_filter_aff2_adffs_a33_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a33_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a956,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a33_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321 = ufir_filter_aff1_adffs_a2_a $ ufir_filter_aff2_adffs_a34_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323 = CARRY(ufir_filter_aff1_adffs_a2_a & (ufir_filter_aff2_adffs_a34_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327) # !ufir_filter_aff1_adffs_a2_a 
-- & ufir_filter_aff2_adffs_a34_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a2_a,
	datab => ufir_filter_aff2_adffs_a34_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a327,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a321,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323);

ufir_filter_aff2_adffs_a34_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a34_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a952,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a34_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317 = ufir_filter_aff1_adffs_a3_a $ ufir_filter_aff2_adffs_a35_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319 = CARRY(ufir_filter_aff1_adffs_a3_a & !ufir_filter_aff2_adffs_a35_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323 # !ufir_filter_aff1_adffs_a3_a & 
-- (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323 # !ufir_filter_aff2_adffs_a35_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a3_a,
	datab => ufir_filter_aff2_adffs_a35_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a323,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a317,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319);

ufir_filter_aff2_adffs_a35_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a35_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a948,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a35_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313 = ufir_filter_aff1_adffs_a4_a $ ufir_filter_aff2_adffs_a36_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315 = CARRY(ufir_filter_aff1_adffs_a4_a & (ufir_filter_aff2_adffs_a36_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319) # !ufir_filter_aff1_adffs_a4_a 
-- & ufir_filter_aff2_adffs_a36_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a4_a,
	datab => ufir_filter_aff2_adffs_a36_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a319,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a313,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315);

ufir_filter_aff2_adffs_a36_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a36_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a944,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a36_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329 = ufir_filter_aff1_adffs_a5_a $ ufir_filter_aff2_adffs_a37_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331 = CARRY(ufir_filter_aff1_adffs_a5_a & !ufir_filter_aff2_adffs_a37_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315 # !ufir_filter_aff1_adffs_a5_a & 
-- (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315 # !ufir_filter_aff2_adffs_a37_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a5_a,
	datab => ufir_filter_aff2_adffs_a37_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a315,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a329,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331);

ufir_filter_aff2_adffs_a37_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a37_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a960,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a37_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333 = ufir_filter_aff1_adffs_a6_a $ ufir_filter_aff2_adffs_a38_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335 = CARRY(ufir_filter_aff1_adffs_a6_a & (ufir_filter_aff2_adffs_a38_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331) # !ufir_filter_aff1_adffs_a6_a 
-- & ufir_filter_aff2_adffs_a38_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a6_a,
	datab => ufir_filter_aff2_adffs_a38_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a331,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a333,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335);

ufir_filter_aff2_adffs_a38_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a38_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964 & (!AD_MR_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a964,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a38_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337 = ufir_filter_aff1_adffs_a7_a $ ufir_filter_aff2_adffs_a39_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339 = CARRY(ufir_filter_aff1_adffs_a7_a & !ufir_filter_aff2_adffs_a39_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335 # !ufir_filter_aff1_adffs_a7_a & 
-- (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335 # !ufir_filter_aff2_adffs_a39_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a7_a,
	datab => ufir_filter_aff2_adffs_a39_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a335,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a337,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a339);

ufir_filter_aff2_adffs_a39_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a39_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a968,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a39_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345 = ufir_filter_aff1_adffs_a9_a $ ufir_filter_aff2_adffs_a41_a $ ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347 = CARRY(ufir_filter_aff1_adffs_a9_a & !ufir_filter_aff2_adffs_a41_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343 # !ufir_filter_aff1_adffs_a9_a & 
-- (!ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343 # !ufir_filter_aff2_adffs_a41_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a9_a,
	datab => ufir_filter_aff2_adffs_a41_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a343,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a345,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347);

ufir_filter_aff2_adffs_a41_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a41_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a976,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a41_a);

ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349 = ufir_filter_aff1_adffs_a10_a $ ufir_filter_aff2_adffs_a42_a $ !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347
-- ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351 = CARRY(ufir_filter_aff1_adffs_a10_a & (ufir_filter_aff2_adffs_a42_a # !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347) # 
-- !ufir_filter_aff1_adffs_a10_a & ufir_filter_aff2_adffs_a42_a & !ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a10_a,
	datab => ufir_filter_aff2_adffs_a42_a,
	cin => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a347,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a349,
	cout => ufir_filter_aas6_aadder_afirst_seg_adder_aresult_node_acs_buffer_a32_a_a351);

ufir_filter_aff2_adffs_a42_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a42_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a980,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a42_a);

ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004 = !ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a994

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a994,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004);

ufir_filter_aas7_aadder_a_a00008_a0 : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas7_aadder_a_a00008 = ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a1004,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas7_aadder_a_a00008);

ufir_filter_aff2_adffs_a46_a_a983_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a46_a_a983 = CARRY(ufir_filter_aas7_aadder_a_a00008)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aas7_aadder_a_a00008,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aff2_adffs_a46_a_a981COMB,
	cout => ufir_filter_aff2_adffs_a46_a_a983);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405 = ufir_filter_aff1_adffs_a15_a $ ufir_filter_aff2_adffs_a47_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407 = CARRY(ufir_filter_aff1_adffs_a15_a & (ufir_filter_aff2_adffs_a47_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403) # !ufir_filter_aff1_adffs_a15_a & 
-- ufir_filter_aff2_adffs_a47_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a15_a,
	datab => ufir_filter_aff2_adffs_a47_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a403,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a405,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409 = ufir_filter_aff1_adffs_a16_a $ ufir_filter_aff2_adffs_a48_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411 = CARRY(ufir_filter_aff1_adffs_a16_a & !ufir_filter_aff2_adffs_a48_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407 # !ufir_filter_aff1_adffs_a16_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407 # !ufir_filter_aff2_adffs_a48_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a16_a,
	datab => ufir_filter_aff2_adffs_a48_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a407,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a409,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a411);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417 = ufir_filter_aff1_adffs_a18_a $ ufir_filter_aff2_adffs_a50_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419 = CARRY(ufir_filter_aff1_adffs_a18_a & !ufir_filter_aff2_adffs_a50_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415 # !ufir_filter_aff1_adffs_a18_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415 # !ufir_filter_aff2_adffs_a50_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a18_a,
	datab => ufir_filter_aff2_adffs_a50_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a415,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a417,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a419);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429 = ufir_filter_aff1_adffs_a20_a $ ufir_filter_aff2_adffs_a52_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431 = CARRY(ufir_filter_aff1_adffs_a20_a & !ufir_filter_aff2_adffs_a52_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423 # !ufir_filter_aff1_adffs_a20_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423 # !ufir_filter_aff2_adffs_a52_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a20_a,
	datab => ufir_filter_aff2_adffs_a52_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a423,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a429,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a431);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433 = ufir_filter_aff1_adffs_a22_a $ ufir_filter_aff2_adffs_a54_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435 = CARRY(ufir_filter_aff1_adffs_a22_a & !ufir_filter_aff2_adffs_a54_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427 # !ufir_filter_aff1_adffs_a22_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427 # !ufir_filter_aff2_adffs_a54_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a22_a,
	datab => ufir_filter_aff2_adffs_a54_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a427,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435);

ufir_filter_aff2_adffs_a54_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a54_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433 $ !ufir_filter_aff2_adffs_a53_a_a942, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a54_a_a948 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a53_a_a942 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433 & !ufir_filter_aff2_adffs_a53_a_a942)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a433,
	cin => ufir_filter_aff2_adffs_a53_a_a942,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a54_a,
	cout => ufir_filter_aff2_adffs_a54_a_a948);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437 = ufir_filter_aff2_adffs_a55_a $ ufir_filter_aff1_adffs_a23_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439 = CARRY(ufir_filter_aff2_adffs_a55_a & (ufir_filter_aff1_adffs_a23_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435) # !ufir_filter_aff2_adffs_a55_a & 
-- ufir_filter_aff1_adffs_a23_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a55_a,
	datab => ufir_filter_aff1_adffs_a23_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a435,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441 = ufir_filter_aff1_adffs_a24_a $ ufir_filter_aff2_adffs_a56_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443 = CARRY(ufir_filter_aff1_adffs_a24_a & !ufir_filter_aff2_adffs_a56_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439 # !ufir_filter_aff1_adffs_a24_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439 # !ufir_filter_aff2_adffs_a56_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a24_a,
	datab => ufir_filter_aff2_adffs_a56_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a439,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443);

ufir_filter_aff2_adffs_a55_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a55_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437 $ ufir_filter_aff2_adffs_a54_a_a948, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a55_a_a951 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437 & !ufir_filter_aff2_adffs_a54_a_a948 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437 # !ufir_filter_aff2_adffs_a54_a_a948))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a437,
	cin => ufir_filter_aff2_adffs_a54_a_a948,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a55_a,
	cout => ufir_filter_aff2_adffs_a55_a_a951);

ufir_filter_aff2_adffs_a56_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a56_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441 $ !ufir_filter_aff2_adffs_a55_a_a951, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a56_a_a954 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a55_a_a951 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441 & !ufir_filter_aff2_adffs_a55_a_a951)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a441,
	cin => ufir_filter_aff2_adffs_a55_a_a951,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a56_a,
	cout => ufir_filter_aff2_adffs_a56_a_a954);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445 = ufir_filter_aff1_adffs_a25_a $ ufir_filter_aff2_adffs_a57_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447 = CARRY(ufir_filter_aff1_adffs_a25_a & (ufir_filter_aff2_adffs_a57_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443) # !ufir_filter_aff1_adffs_a25_a & 
-- ufir_filter_aff2_adffs_a57_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a25_a,
	datab => ufir_filter_aff2_adffs_a57_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a443,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447);

ufir_filter_aff2_adffs_a57_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a57_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445 $ ufir_filter_aff2_adffs_a56_a_a954, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a57_a_a957 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445 & !ufir_filter_aff2_adffs_a56_a_a954 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445 # !ufir_filter_aff2_adffs_a56_a_a954))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a445,
	cin => ufir_filter_aff2_adffs_a56_a_a954,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a57_a,
	cout => ufir_filter_aff2_adffs_a57_a_a957);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449 = ufir_filter_aff1_adffs_a26_a $ ufir_filter_aff2_adffs_a58_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451 = CARRY(ufir_filter_aff1_adffs_a26_a & !ufir_filter_aff2_adffs_a58_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447 # !ufir_filter_aff1_adffs_a26_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447 # !ufir_filter_aff2_adffs_a58_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a26_a,
	datab => ufir_filter_aff2_adffs_a58_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a447,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451);

ufir_filter_aff2_adffs_a58_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a58_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449 $ !ufir_filter_aff2_adffs_a57_a_a957, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a58_a_a960 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a57_a_a957 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449 & !ufir_filter_aff2_adffs_a57_a_a957)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a449,
	cin => ufir_filter_aff2_adffs_a57_a_a957,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a58_a,
	cout => ufir_filter_aff2_adffs_a58_a_a960);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453 = ufir_filter_aff1_adffs_a27_a $ ufir_filter_aff2_adffs_a59_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455 = CARRY(ufir_filter_aff1_adffs_a27_a & (ufir_filter_aff2_adffs_a59_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451) # !ufir_filter_aff1_adffs_a27_a & 
-- ufir_filter_aff2_adffs_a59_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a27_a,
	datab => ufir_filter_aff2_adffs_a59_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a451,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455);

ufir_filter_aff2_adffs_a59_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a59_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453 $ ufir_filter_aff2_adffs_a58_a_a960, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a59_a_a963 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453 & !ufir_filter_aff2_adffs_a58_a_a960 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453 # !ufir_filter_aff2_adffs_a58_a_a960))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a453,
	cin => ufir_filter_aff2_adffs_a58_a_a960,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a59_a,
	cout => ufir_filter_aff2_adffs_a59_a_a963);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465 = ufir_filter_aff1_adffs_a28_a $ ufir_filter_aff2_adffs_a60_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467 = CARRY(ufir_filter_aff1_adffs_a28_a & !ufir_filter_aff2_adffs_a60_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455 # !ufir_filter_aff1_adffs_a28_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455 # !ufir_filter_aff2_adffs_a60_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a28_a,
	datab => ufir_filter_aff2_adffs_a60_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a455,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467);

ufir_filter_aff2_adffs_a60_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a60_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465 $ !ufir_filter_aff2_adffs_a59_a_a963, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a60_a_a986 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a59_a_a963 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465 & !ufir_filter_aff2_adffs_a59_a_a963)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a465,
	cin => ufir_filter_aff2_adffs_a59_a_a963,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a60_a,
	cout => ufir_filter_aff2_adffs_a60_a_a986);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473 = ufir_filter_aff1_adffs_a29_a $ ufir_filter_aff2_adffs_a61_a $ !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475 = CARRY(ufir_filter_aff1_adffs_a29_a & (ufir_filter_aff2_adffs_a61_a # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467) # !ufir_filter_aff1_adffs_a29_a & 
-- ufir_filter_aff2_adffs_a61_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a29_a,
	datab => ufir_filter_aff2_adffs_a61_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a467,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475);

ufir_filter_aff2_adffs_a61_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a61_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473 $ ufir_filter_aff2_adffs_a60_a_a986, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a61_a_a992 = CARRY(ufir_filter_aff2_adffs_a63_a & ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473 & !ufir_filter_aff2_adffs_a60_a_a986 # !ufir_filter_aff2_adffs_a63_a & 
-- (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473 # !ufir_filter_aff2_adffs_a60_a_a986))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a473,
	cin => ufir_filter_aff2_adffs_a60_a_a986,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a61_a,
	cout => ufir_filter_aff2_adffs_a61_a_a992);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469 = ufir_filter_aff1_adffs_a30_a $ ufir_filter_aff2_adffs_a62_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a471 = CARRY(ufir_filter_aff1_adffs_a30_a & !ufir_filter_aff2_adffs_a62_a & !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475 # !ufir_filter_aff1_adffs_a30_a & 
-- (!ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475 # !ufir_filter_aff2_adffs_a62_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff1_adffs_a30_a,
	datab => ufir_filter_aff2_adffs_a62_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a475,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469,
	cout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a471);

ufir_filter_aff2_adffs_a62_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a62_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469 $ !ufir_filter_aff2_adffs_a61_a_a992, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ufir_filter_aff2_adffs_a62_a_a989 = CARRY(ufir_filter_aff2_adffs_a63_a & (!ufir_filter_aff2_adffs_a61_a_a992 # !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469) # !ufir_filter_aff2_adffs_a63_a & 
-- !ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469 & !ufir_filter_aff2_adffs_a61_a_a992)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datab => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a469,
	cin => ufir_filter_aff2_adffs_a61_a_a992,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a62_a,
	cout => ufir_filter_aff2_adffs_a62_a_a989);

ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457_I : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457 = ufir_filter_aff2_adffs_a63_a $ (ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a471 $ !ufir_filter_aff1_adffs_a31_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datad => ufir_filter_aff1_adffs_a31_a,
	cin => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a471,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457);

ufir_filter_aff2_adffs_a63_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a63_a = DFFE(!GLOBAL(AD_MR_acombout) & ufir_filter_aff2_adffs_a63_a $ (ufir_filter_aff2_adffs_a62_a_a989 $ ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a63_a,
	datad => ufir_filter_aas6_aadder_apart_seg_adder_aresult_node_acs_buffer_a0_a_a457,
	cin => ufir_filter_aff2_adffs_a62_a_a989,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => AD_MR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a63_a);

TimeConst_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(3),
	combout => TimeConst_a3_a_acombout);

TimeConst_Reg_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_Reg_FF_adffs_a3_a = DFFE(TimeConst_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_Reg_FF_adffs_a3_a);

TimeConst_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(0),
	combout => TimeConst_a0_a_acombout);

TimeConst_Reg_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_Reg_FF_adffs_a0_a = DFFE(TimeConst_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_Reg_FF_adffs_a0_a);

TimeConst_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(1),
	combout => TimeConst_a1_a_acombout);

TimeConst_Reg_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_Reg_FF_adffs_a1_a = DFFE(TimeConst_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_Reg_FF_adffs_a1_a);

Fir_Max_Clip_val_a26_a_a213_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Clip_val_a26_a_a213 = TimeConst_Reg_FF_adffs_a3_a # TimeConst_Reg_FF_adffs_a2_a & TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a2_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Clip_val_a26_a_a213);

TimeConst_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeConst(2),
	combout => TimeConst_a2_a_acombout);

TimeConst_Reg_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- TimeConst_Reg_FF_adffs_a2_a = DFFE(TimeConst_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => TimeConst_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => TimeConst_Reg_FF_adffs_a2_a);

Fir_Max_Clip_val_a24_a_a215_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Clip_val_a24_a_a215 = TimeConst_Reg_FF_adffs_a3_a # TimeConst_Reg_FF_adffs_a2_a & (TimeConst_Reg_FF_adffs_a1_a # TimeConst_Reg_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FAEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a3_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Clip_val_a24_a_a215);

Fir_Max_Clip_val_a25_a_a216_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Clip_val_a25_a_a216 = TimeConst_Reg_FF_adffs_a3_a # TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCCC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Clip_val_a25_a_a216);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1564_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(Equal_a193 & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a # !ufir_filter_aff2_adffs_a33_a) # !Equal_a193 & !ufir_filter_aff2_adffs_a33_a & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => ufir_filter_aff2_adffs_a33_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1564,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1563_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(Equal_a192 & ufir_filter_aff2_adffs_a34_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a # !Equal_a192 & (ufir_filter_aff2_adffs_a34_a # 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a192,
	datab => ufir_filter_aff2_adffs_a34_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1563,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1562_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(Fir_Max_Clip_val_a22_a_a217 & !ufir_filter_aff2_adffs_a35_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a # !Fir_Max_Clip_val_a22_a_a217 & 
-- (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a # !ufir_filter_aff2_adffs_a35_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a22_a_a217,
	datab => ufir_filter_aff2_adffs_a35_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1562,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1561_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & ufir_filter_aff2_adffs_a36_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a # 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & (ufir_filter_aff2_adffs_a36_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	datab => ufir_filter_aff2_adffs_a36_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1561,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1560_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(ufir_filter_aff2_adffs_a37_a & !Fir_Max_Clip_val_a24_a_a215 & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a # !ufir_filter_aff2_adffs_a37_a & 
-- (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a # !Fir_Max_Clip_val_a24_a_a215))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => Fir_Max_Clip_val_a24_a_a215,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1560,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1559_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(ufir_filter_aff2_adffs_a38_a & (Fir_Max_Clip_val_a25_a_a216 # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a) # !ufir_filter_aff2_adffs_a38_a & Fir_Max_Clip_val_a25_a_a216 & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a38_a,
	datab => Fir_Max_Clip_val_a25_a_a216,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1559,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1558_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(Fir_Max_Clip_val_a26_a_a213 & !ufir_filter_aff2_adffs_a39_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a # !Fir_Max_Clip_val_a26_a_a213 & 
-- (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a # !ufir_filter_aff2_adffs_a39_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a26_a_a213,
	datab => ufir_filter_aff2_adffs_a39_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1558,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1557_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(ufir_filter_aff2_adffs_a40_a & (TimeConst_Reg_FF_adffs_a3_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a) # !ufir_filter_aff2_adffs_a40_a & TimeConst_Reg_FF_adffs_a3_a & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1557,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1556_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a # !ufir_filter_aff2_adffs_a41_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a41_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1556,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1555_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(ufir_filter_aff2_adffs_a42_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a42_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1555,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1554_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a # !ufir_filter_aff2_adffs_a43_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a43_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1554,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1553_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(ufir_filter_aff2_adffs_a44_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a44_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1553,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1552_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a # !ufir_filter_aff2_adffs_a45_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1552,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1551_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(ufir_filter_aff2_adffs_a46_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a46_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1551,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1550_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a # !ufir_filter_aff2_adffs_a47_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a47_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1550,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1549_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(ufir_filter_aff2_adffs_a48_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a48_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1549,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1548_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a # !ufir_filter_aff2_adffs_a49_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a49_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1548,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1547_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(ufir_filter_aff2_adffs_a50_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a50_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1547,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1546_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a # !ufir_filter_aff2_adffs_a51_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a51_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1546,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1545_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a = CARRY(Equal_a193 & (ufir_filter_aff2_adffs_a52_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a) # !Equal_a193 & ufir_filter_aff2_adffs_a52_a & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => ufir_filter_aff2_adffs_a52_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1545,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1544_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a = CARRY(Equal_a192 & !ufir_filter_aff2_adffs_a53_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a # !Equal_a192 & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a # 
-- !ufir_filter_aff2_adffs_a53_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a192,
	datab => ufir_filter_aff2_adffs_a53_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1544,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1543_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a = CARRY(Fir_Max_Clip_val_a22_a_a217 & ufir_filter_aff2_adffs_a54_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a # !Fir_Max_Clip_val_a22_a_a217 & (ufir_filter_aff2_adffs_a54_a # 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a22_a_a217,
	datab => ufir_filter_aff2_adffs_a54_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1543,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1542_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a = CARRY(Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & !ufir_filter_aff2_adffs_a55_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a # 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a # !ufir_filter_aff2_adffs_a55_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	datab => ufir_filter_aff2_adffs_a55_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1542,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1541_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a = CARRY(ufir_filter_aff2_adffs_a56_a & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a # !Fir_Max_Clip_val_a24_a_a215) # !ufir_filter_aff2_adffs_a56_a & !Fir_Max_Clip_val_a24_a_a215 & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a56_a,
	datab => Fir_Max_Clip_val_a24_a_a215,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1541,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1540_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a = CARRY(Fir_Max_Clip_val_a25_a_a216 & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a # !ufir_filter_aff2_adffs_a57_a) # !Fir_Max_Clip_val_a25_a_a216 & !ufir_filter_aff2_adffs_a57_a & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a25_a_a216,
	datab => ufir_filter_aff2_adffs_a57_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1540,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1539_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a = CARRY(ufir_filter_aff2_adffs_a58_a & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a # !Fir_Max_Clip_val_a26_a_a213) # !ufir_filter_aff2_adffs_a58_a & !Fir_Max_Clip_val_a26_a_a213 & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a58_a,
	datab => Fir_Max_Clip_val_a26_a_a213,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1539,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1538_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a = CARRY(TimeConst_Reg_FF_adffs_a3_a & (!Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a # !ufir_filter_aff2_adffs_a59_a) # !TimeConst_Reg_FF_adffs_a3_a & !ufir_filter_aff2_adffs_a59_a & 
-- !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a3_a,
	datab => ufir_filter_aff2_adffs_a59_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1538,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1537_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a = CARRY(ufir_filter_aff2_adffs_a60_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a60_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1537,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1536_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a = CARRY(!ufir_filter_aff2_adffs_a61_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a61_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1536,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1535_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a = CARRY(ufir_filter_aff2_adffs_a62_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a62_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1535,
	cout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a);

Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out = Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a & !ufir_filter_aff2_adffs_a63_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => ufir_filter_aff2_adffs_a63_a,
	cin => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 = !TimeConst_Reg_FF_adffs_a3_a & (!TimeConst_Reg_FF_adffs_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587);

Equal_a192_I : apex20ke_lcell
-- Equation(s):
-- Equal_a192 = !TimeConst_Reg_FF_adffs_a1_a & Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a192);

Equal_a193_I : apex20ke_lcell
-- Equation(s):
-- Equal_a193 = !TimeConst_Reg_FF_adffs_a0_a & Equal_a192

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a192,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a193);

Fir_Max_Clip_val_a22_a_a217_I : apex20ke_lcell
-- Equation(s):
-- Fir_Max_Clip_val_a22_a_a217 = TimeConst_Reg_FF_adffs_a2_a # TimeConst_Reg_FF_adffs_a3_a # TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a2_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Max_Clip_val_a22_a_a217);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a_a1689_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(!ufir_filter_aff2_adffs_a32_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0055",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a32_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a_a1689,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1688_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(ufir_filter_aff2_adffs_a33_a & (Equal_a193 # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a) # !ufir_filter_aff2_adffs_a33_a & Equal_a193 & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a33_a,
	datab => Equal_a193,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a_a1688,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1687_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(ufir_filter_aff2_adffs_a34_a & !Equal_a192 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a # !ufir_filter_aff2_adffs_a34_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a # 
-- !Equal_a192))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a34_a,
	datab => Equal_a192,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a_a1687,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1686_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(ufir_filter_aff2_adffs_a35_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a # !Fir_Max_Clip_val_a22_a_a217) # !ufir_filter_aff2_adffs_a35_a & !Fir_Max_Clip_val_a22_a_a217 & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a35_a,
	datab => Fir_Max_Clip_val_a22_a_a217,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a_a1686,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1685_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(ufir_filter_aff2_adffs_a36_a & !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a # !ufir_filter_aff2_adffs_a36_a & 
-- (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a # !Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a36_a,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a_a1685,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1684_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(ufir_filter_aff2_adffs_a37_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a # !Fir_Max_Clip_val_a24_a_a215) # !ufir_filter_aff2_adffs_a37_a & !Fir_Max_Clip_val_a24_a_a215 & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => Fir_Max_Clip_val_a24_a_a215,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a_a1684,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1683_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(ufir_filter_aff2_adffs_a38_a & Fir_Max_Clip_val_a25_a_a216 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a # !ufir_filter_aff2_adffs_a38_a & (Fir_Max_Clip_val_a25_a_a216 # 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a38_a,
	datab => Fir_Max_Clip_val_a25_a_a216,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a_a1683,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1682_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(Fir_Max_Clip_val_a26_a_a213 & ufir_filter_aff2_adffs_a39_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a # !Fir_Max_Clip_val_a26_a_a213 & (ufir_filter_aff2_adffs_a39_a # 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a26_a_a213,
	datab => ufir_filter_aff2_adffs_a39_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a_a1682,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1681_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(ufir_filter_aff2_adffs_a40_a & TimeConst_Reg_FF_adffs_a3_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a # !ufir_filter_aff2_adffs_a40_a & (TimeConst_Reg_FF_adffs_a3_a # 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a_a1681,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1680_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(ufir_filter_aff2_adffs_a41_a # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a41_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a_a1680,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1679_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(!ufir_filter_aff2_adffs_a42_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a_a1679,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1678_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(ufir_filter_aff2_adffs_a43_a # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a43_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a_a1678,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1677_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(!ufir_filter_aff2_adffs_a44_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a44_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a_a1677,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1676_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(ufir_filter_aff2_adffs_a45_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a # !Equal_a193) # !ufir_filter_aff2_adffs_a45_a & !Equal_a193 & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a45_a,
	datab => Equal_a193,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a_a1676,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1675_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(ufir_filter_aff2_adffs_a46_a & Equal_a192 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a # !ufir_filter_aff2_adffs_a46_a & (Equal_a192 # 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a46_a,
	datab => Equal_a192,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a_a1675,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1674_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(Fir_Max_Clip_val_a22_a_a217 & (ufir_filter_aff2_adffs_a47_a # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a) # !Fir_Max_Clip_val_a22_a_a217 & ufir_filter_aff2_adffs_a47_a & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a22_a_a217,
	datab => ufir_filter_aff2_adffs_a47_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a_a1674,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1673_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(ufir_filter_aff2_adffs_a48_a & Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a # !ufir_filter_aff2_adffs_a48_a & 
-- (Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a48_a,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a_a1673,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1672_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(Fir_Max_Clip_val_a24_a_a215 & (ufir_filter_aff2_adffs_a49_a # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a) # !Fir_Max_Clip_val_a24_a_a215 & ufir_filter_aff2_adffs_a49_a & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a24_a_a215,
	datab => ufir_filter_aff2_adffs_a49_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a_a1672,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1671_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(Fir_Max_Clip_val_a25_a_a216 & !ufir_filter_aff2_adffs_a50_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a # !Fir_Max_Clip_val_a25_a_a216 & 
-- (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a # !ufir_filter_aff2_adffs_a50_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a25_a_a216,
	datab => ufir_filter_aff2_adffs_a50_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a_a1671,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1670_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(Fir_Max_Clip_val_a26_a_a213 & (ufir_filter_aff2_adffs_a51_a # !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a) # !Fir_Max_Clip_val_a26_a_a213 & ufir_filter_aff2_adffs_a51_a & 
-- !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "008E",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Clip_val_a26_a_a213,
	datab => ufir_filter_aff2_adffs_a51_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a_a1670,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1669_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a = CARRY(TimeConst_Reg_FF_adffs_a3_a & !ufir_filter_aff2_adffs_a52_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a # !TimeConst_Reg_FF_adffs_a3_a & 
-- (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a # !ufir_filter_aff2_adffs_a52_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0017",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a3_a,
	datab => ufir_filter_aff2_adffs_a52_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a_a1669,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1668_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a = CARRY(ufir_filter_aff2_adffs_a53_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a53_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a_a1668,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1667_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a = CARRY(!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a # !ufir_filter_aff2_adffs_a54_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a54_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a_a1667,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1666_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a = CARRY(ufir_filter_aff2_adffs_a55_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a55_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1666,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1665_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a = CARRY(!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a # !ufir_filter_aff2_adffs_a56_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a56_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a_a1665,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1664_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a = CARRY(ufir_filter_aff2_adffs_a57_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a57_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a_a1664,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1663_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a = CARRY(!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a # !ufir_filter_aff2_adffs_a58_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a58_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a_a1663,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1662_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a = CARRY(ufir_filter_aff2_adffs_a59_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a59_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a_a1662,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1661_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a = CARRY(!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a # !ufir_filter_aff2_adffs_a60_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a60_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a_a1661,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1660_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a = CARRY(ufir_filter_aff2_adffs_a61_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a61_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a_a1660,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1659_I : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a = CARRY(!Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a # !ufir_filter_aff2_adffs_a62_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "005F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a62_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a_a1659,
	cout => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a);

Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out = Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a & ufir_filter_aff2_adffs_a63_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => ufir_filter_aff2_adffs_a63_a,
	cin => Fir_Min_Cmp_Compare_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out);

Fir_Out_Ff_adffs_a0_a_a10923_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a0_a_a10923 = ufir_filter_aff2_adffs_a37_a & TimeConst_Reg_FF_adffs_a3_a & (!Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0088",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a0_a_a10923);

Fir_Out_Ff_adffs_a0_a_a10922_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a0_a_a10922 = Fir_Out_Ff_adffs_a0_a_a10921 & !TimeConst_Reg_FF_adffs_a3_a & TimeConst_Reg_FF_adffs_a2_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a0_a_a10921,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a0_a_a10922);

Fir_Out_Ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a0_a = DFFE(!PA_Reset_acombout & (Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Out_Ff_adffs_a0_a_a10923 # Fir_Out_Ff_adffs_a0_a_a10922), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3332",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a0_a_a10923,
	datad => Fir_Out_Ff_adffs_a0_a_a10922,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a0_a);

Fir_Out_Ff_adffs_a1_a_a10927_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a_a10927 = Equal_a184 & TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a33_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a184,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => ufir_filter_aff2_adffs_a33_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a1_a_a10927);

Fir_Out_Ff_adffs_a1_a_a10926_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a_a10926 = Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # TimeConst_Reg_FF_adffs_a3_a & ufir_filter_aff2_adffs_a38_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => ufir_filter_aff2_adffs_a38_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a1_a_a10926);

Fir_Out_Ff_adffs_a1_a_a10925_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a_a10925 = Fir_Out_Ff_adffs_a1_a_a10918 & !TimeConst_Reg_FF_adffs_a3_a & TimeConst_Reg_FF_adffs_a2_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a1_a_a10918,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a1_a_a10925);

Fir_Out_Ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a1_a = DFFE(!PA_Reset_acombout & (Fir_Out_Ff_adffs_a1_a_a10927 # Fir_Out_Ff_adffs_a1_a_a10926 # Fir_Out_Ff_adffs_a1_a_a10925), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3332",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a1_a_a10927,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a1_a_a10926,
	datad => Fir_Out_Ff_adffs_a1_a_a10925,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a1_a);

Fir_Out_Ff_adffs_a2_a_a10932_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10932 = TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a37_a) # !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a35_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a35_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => ufir_filter_aff2_adffs_a37_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10932);

Fir_Out_Ff_adffs_a2_a_a10933_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10933 = Equal_a185 & !TimeConst_Reg_FF_adffs_a0_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & Fir_Out_Ff_adffs_a2_a_a10932

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a185,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a2_a_a10932,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10933);

Equal_a185_I : apex20ke_lcell
-- Equation(s):
-- Equal_a185 = !TimeConst_Reg_FF_adffs_a3_a & TimeConst_Reg_FF_adffs_a2_a
-- Equal_a200 = !TimeConst_Reg_FF_adffs_a3_a & TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a185,
	cascout => Equal_a200);

Fir_Out_Ff_adffs_a2_a_a10931_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10931 = Fir_Out_Ff_adffs_a2_a_a10930 & Equal_a185 & TimeConst_Reg_FF_adffs_a0_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a2_a_a10930,
	datab => Equal_a185,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10931);

Fir_Out_Ff_adffs_a2_a_a10920_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10920 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a34_a # !TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a33_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a34_a,
	datab => ufir_filter_aff2_adffs_a33_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10920);

Fir_Out_Ff_adffs_a2_a_a10929_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10929 = Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & TimeConst_Reg_FF_adffs_a1_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & Fir_Out_Ff_adffs_a2_a_a10920

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a2_a_a10920,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10929);

Fir_Out_Ff_adffs_a2_a_a10935_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a_a10935 = Fir_Out_Ff_adffs_a2_a_a10934 # Fir_Out_Ff_adffs_a2_a_a10933 # Fir_Out_Ff_adffs_a2_a_a10931 # Fir_Out_Ff_adffs_a2_a_a10929

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a2_a_a10934,
	datab => Fir_Out_Ff_adffs_a2_a_a10933,
	datac => Fir_Out_Ff_adffs_a2_a_a10931,
	datad => Fir_Out_Ff_adffs_a2_a_a10929,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a2_a_a10935);

Fir_Out_Ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a2_a = DFFE(!PA_Reset_acombout & Fir_Out_Ff_adffs_a2_a_a10935, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a2_a_a10935,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a2_a);

fir_out_mux_a23641_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23641 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a37_a & Equal_a185 & !TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a37_a,
	datac => Equal_a185,
	datad => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23641);

fir_out_mux_a23639_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23639 = TimeConst_Reg_FF_adffs_a3_a # TimeConst_Reg_FF_adffs_a1_a $ !TimeConst_Reg_FF_adffs_a0_a # !TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EBFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a3_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23639);

Equal_a189_I : apex20ke_lcell
-- Equation(s):
-- Equal_a189 = TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a2_a & !TimeConst_Reg_FF_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => TimeConst_Reg_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a189);

fir_out_mux_a23640_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23640 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a39_a) # !Equal_a189 & ufir_filter_aff2_adffs_a40_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C088",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => fir_out_mux_a23639,
	datac => ufir_filter_aff2_adffs_a39_a,
	datad => Equal_a189,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23640);

fir_out_mux_a23643_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23643 = fir_out_mux_a23641 # fir_out_mux_a23640 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a38_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => ufir_filter_aff2_adffs_a38_a,
	datac => fir_out_mux_a23641,
	datad => fir_out_mux_a23640,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23643);

Equal_a187_I : apex20ke_lcell
-- Equation(s):
-- Equal_a187 = TimeConst_Reg_FF_adffs_a2_a & !TimeConst_Reg_FF_adffs_a1_a & !TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a2_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a187);

Equal_a184_I : apex20ke_lcell
-- Equation(s):
-- Equal_a184 = TimeConst_Reg_FF_adffs_a1_a & Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a184);

fir_out_mux_a23636_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23636 = TimeConst_Reg_FF_adffs_a1_a & Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587 & !TimeConst_Reg_FF_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23636);

Equal_a188_I : apex20ke_lcell
-- Equation(s):
-- Equal_a188 = TimeConst_Reg_FF_adffs_a0_a & Equal_a192

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a192,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a188);

fir_out_mux_a23637_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23637 = ufir_filter_aff2_adffs_a34_a & (fir_out_mux_a23636 # ufir_filter_aff2_adffs_a33_a & Equal_a188) # !ufir_filter_aff2_adffs_a34_a & ufir_filter_aff2_adffs_a33_a & (Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a34_a,
	datab => ufir_filter_aff2_adffs_a33_a,
	datac => fir_out_mux_a23636,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23637);

fir_out_mux_a23644_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23644 = fir_out_mux_a23635 # fir_out_mux_a23637 # fir_out_mux_a23643 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23635,
	datab => fir_out_mux_a23643,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23637,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23644);

Fir_Out_Ff_adffs_a3_a_a10937_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a3_a_a10937 = !Equal_a193 & !PA_Reset_acombout & fir_out_mux_a23644 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => PA_Reset_acombout,
	datac => fir_out_mux_a23644,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a3_a_a10937);

Fir_Out_Ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a3_a = DFFE(Fir_Out_Ff_adffs_a3_a_a10937 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a3_a_a10937,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a3_a);

Fir_Out_Ff_adffs_a4_a_a10940_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a4_a_a10940 = Equal_a193 & !PA_Reset_acombout & ufir_filter_aff2_adffs_a33_a & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => PA_Reset_acombout,
	datac => ufir_filter_aff2_adffs_a33_a,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a4_a_a10940);

Fir_Out_Ff_adffs_a4_a_a10939_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a4_a_a10939 = fir_out_mux_a23650 & !PA_Reset_acombout & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23650,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a4_a_a10939);

Fir_Out_Ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a4_a = DFFE(Fir_Out_Ff_adffs_a4_a_a10940 # Fir_Out_Ff_adffs_a4_a_a10939 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a4_a_a10940,
	datad => Fir_Out_Ff_adffs_a4_a_a10939,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a4_a);

Fir_Out_Ff_adffs_a5_a_a10943_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a5_a_a10943 = Equal_a193 & ufir_filter_aff2_adffs_a34_a & !PA_Reset_acombout & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => ufir_filter_aff2_adffs_a34_a,
	datac => PA_Reset_acombout,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a5_a_a10943);

fir_out_mux_a23652_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23652 = fir_out_mux_a23636 & (ufir_filter_aff2_adffs_a36_a # ufir_filter_aff2_adffs_a35_a & Equal_a188) # !fir_out_mux_a23636 & ufir_filter_aff2_adffs_a35_a & Equal_a188

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23636,
	datab => ufir_filter_aff2_adffs_a35_a,
	datac => Equal_a188,
	datad => ufir_filter_aff2_adffs_a36_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23652);

fir_out_mux_a23634_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23634 = Equal_a187 & !Equal_a184 & (!TimeConst_Reg_FF_adffs_a0_a # !Equal_a192)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0070",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a192,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => Equal_a187,
	datad => Equal_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23634);

fir_out_mux_a23633_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23633 = !Equal_a192 & TimeConst_Reg_FF_adffs_a1_a & TimeConst_Reg_FF_adffs_a0_a & Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a192,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Fir_Max_Cmp_Compare_acomparator_acmp_end_alcarry_a23_a_a1587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23633);

fir_out_mux_a23651_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23651 = ufir_filter_aff2_adffs_a37_a & (fir_out_mux_a23633 # ufir_filter_aff2_adffs_a38_a & fir_out_mux_a23634) # !ufir_filter_aff2_adffs_a37_a & ufir_filter_aff2_adffs_a38_a & fir_out_mux_a23634

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => ufir_filter_aff2_adffs_a38_a,
	datac => fir_out_mux_a23634,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23651);

fir_out_mux_a23656_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23656 = fir_out_mux_a23652 # fir_out_mux_a23651 # fir_out_mux_a23655 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23655,
	datab => fir_out_mux_a23652,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23651,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23656);

Fir_Out_Ff_adffs_a5_a_a10942_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a5_a_a10942 = !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !Equal_a193 & fir_out_mux_a23656 & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Equal_a193,
	datac => fir_out_mux_a23656,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a5_a_a10942);

Fir_Out_Ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a5_a = DFFE(Fir_Out_Ff_adffs_a5_a_a10943 # Fir_Out_Ff_adffs_a5_a_a10942 # !PA_Reset_acombout & Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFDC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Out_Ff_adffs_a5_a_a10943,
	datac => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a5_a_a10942,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a5_a);

Fir_Out_Ff_adffs_a6_a_a10946_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a6_a_a10946 = !PA_Reset_acombout & ufir_filter_aff2_adffs_a35_a & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => ufir_filter_aff2_adffs_a35_a,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a6_a_a10946);

Fir_Out_Ff_adffs_a6_a_a10945_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a6_a_a10945 = fir_out_mux_a23662 & !PA_Reset_acombout & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23662,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a6_a_a10945);

Fir_Out_Ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a6_a = DFFE(Fir_Out_Ff_adffs_a6_a_a10946 # Fir_Out_Ff_adffs_a6_a_a10945 # !PA_Reset_acombout & Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF4",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => Fir_Out_Ff_adffs_a6_a_a10946,
	datad => Fir_Out_Ff_adffs_a6_a_a10945,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a6_a);

Fir_Out_Ff_adffs_a7_a_a10949_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a7_a_a10949 = ufir_filter_aff2_adffs_a36_a & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a36_a,
	datab => Equal_a193,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a7_a_a10949);

ufir_filter_aff2_adffs_a40_a_aI : apex20ke_lcell
-- Equation(s):
-- ufir_filter_aff2_adffs_a40_a = DFFE(ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972 & !AD_MR_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ufir_filter_aas7_aadder_afirst_seg_adder_aresult_node_acs_buffer_a4_a_a972,
	datad => AD_MR_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ufir_filter_aff2_adffs_a40_a);

fir_out_mux_a23663_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23663 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a40_a # ufir_filter_aff2_adffs_a39_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a39_a & fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a40_a,
	datac => ufir_filter_aff2_adffs_a39_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23663);

fir_out_mux_a23666_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23666 = !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a41_a & TimeConst_Reg_FF_adffs_a0_a & Equal_a185

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => ufir_filter_aff2_adffs_a41_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Equal_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23666);

fir_out_mux_a23642_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23642 = !TimeConst_Reg_FF_adffs_a3_a & TimeConst_Reg_FF_adffs_a1_a & !TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a3_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => TimeConst_Reg_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23642);

fir_out_mux_a23665_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23665 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a43_a) # !Equal_a189 & ufir_filter_aff2_adffs_a44_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a189,
	datab => ufir_filter_aff2_adffs_a44_a,
	datac => ufir_filter_aff2_adffs_a43_a,
	datad => fir_out_mux_a23639,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23665);

fir_out_mux_a23667_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23667 = fir_out_mux_a23666 # fir_out_mux_a23665 # ufir_filter_aff2_adffs_a42_a & fir_out_mux_a23642

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => fir_out_mux_a23666,
	datac => fir_out_mux_a23642,
	datad => fir_out_mux_a23665,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23667);

fir_out_mux_a23664_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23664 = ufir_filter_aff2_adffs_a38_a & (fir_out_mux_a23636 # ufir_filter_aff2_adffs_a37_a & Equal_a188) # !ufir_filter_aff2_adffs_a38_a & ufir_filter_aff2_adffs_a37_a & (Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a38_a,
	datab => ufir_filter_aff2_adffs_a37_a,
	datac => fir_out_mux_a23636,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23664);

fir_out_mux_a23668_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23668 = fir_out_mux_a23663 # fir_out_mux_a23664 # fir_out_mux_a23638 & fir_out_mux_a23667

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23638,
	datab => fir_out_mux_a23663,
	datac => fir_out_mux_a23667,
	datad => fir_out_mux_a23664,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23668);

Fir_Out_Ff_adffs_a7_a_a10948_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a7_a_a10948 = !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !Equal_a193 & fir_out_mux_a23668 & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Equal_a193,
	datac => fir_out_mux_a23668,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a7_a_a10948);

Fir_Out_Ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a7_a = DFFE(Fir_Out_Ff_adffs_a7_a_a10949 # Fir_Out_Ff_adffs_a7_a_a10948 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a7_a_a10949,
	datad => Fir_Out_Ff_adffs_a7_a_a10948,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a7_a);

Fir_Out_Ff_adffs_a8_a_a10952_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a8_a_a10952 = ufir_filter_aff2_adffs_a37_a & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a37_a,
	datab => Equal_a193,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a8_a_a10952);

fir_out_mux_a23670_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23670 = ufir_filter_aff2_adffs_a38_a & (Equal_a188 # ufir_filter_aff2_adffs_a39_a & fir_out_mux_a23636) # !ufir_filter_aff2_adffs_a38_a & ufir_filter_aff2_adffs_a39_a & fir_out_mux_a23636

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a38_a,
	datab => ufir_filter_aff2_adffs_a39_a,
	datac => fir_out_mux_a23636,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23670);

fir_out_mux_a23672_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23672 = TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a42_a & !TimeConst_Reg_FF_adffs_a1_a & Equal_a185

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => ufir_filter_aff2_adffs_a42_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => Equal_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23672);

fir_out_mux_a23671_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23671 = fir_out_mux_a23639 & (Equal_a189 & (ufir_filter_aff2_adffs_a44_a) # !Equal_a189 & ufir_filter_aff2_adffs_a45_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23639,
	datab => ufir_filter_aff2_adffs_a45_a,
	datac => Equal_a189,
	datad => ufir_filter_aff2_adffs_a44_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23671);

fir_out_mux_a23673_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23673 = fir_out_mux_a23672 # fir_out_mux_a23671 # fir_out_mux_a23642 & ufir_filter_aff2_adffs_a43_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23642,
	datab => ufir_filter_aff2_adffs_a43_a,
	datac => fir_out_mux_a23672,
	datad => fir_out_mux_a23671,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23673);

fir_out_mux_a23669_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23669 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a41_a # ufir_filter_aff2_adffs_a40_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a40_a & fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a41_a,
	datac => ufir_filter_aff2_adffs_a40_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23669);

fir_out_mux_a23674_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23674 = fir_out_mux_a23670 # fir_out_mux_a23669 # fir_out_mux_a23638 & fir_out_mux_a23673

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23638,
	datab => fir_out_mux_a23670,
	datac => fir_out_mux_a23673,
	datad => fir_out_mux_a23669,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23674);

Fir_Out_Ff_adffs_a8_a_a10951_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a8_a_a10951 = !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !Equal_a193 & fir_out_mux_a23674 & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Equal_a193,
	datac => fir_out_mux_a23674,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a8_a_a10951);

Fir_Out_Ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a8_a = DFFE(Fir_Out_Ff_adffs_a8_a_a10952 # Fir_Out_Ff_adffs_a8_a_a10951 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a8_a_a10952,
	datad => Fir_Out_Ff_adffs_a8_a_a10951,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a8_a);

Fir_Out_Ff_adffs_a9_a_a10955_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a9_a_a10955 = ufir_filter_aff2_adffs_a38_a & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a38_a,
	datab => Equal_a193,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a9_a_a10955);

fir_out_mux_a23676_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23676 = ufir_filter_aff2_adffs_a40_a & (fir_out_mux_a23636 # ufir_filter_aff2_adffs_a39_a & Equal_a188) # !ufir_filter_aff2_adffs_a40_a & ufir_filter_aff2_adffs_a39_a & (Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => ufir_filter_aff2_adffs_a39_a,
	datac => fir_out_mux_a23636,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23676);

fir_out_mux_a23678_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23678 = !TimeConst_Reg_FF_adffs_a1_a & Equal_a185 & TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a43_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a1_a,
	datab => Equal_a185,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a43_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23678);

fir_out_mux_a23679_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23679 = fir_out_mux_a23677 # fir_out_mux_a23678 # ufir_filter_aff2_adffs_a44_a & fir_out_mux_a23642

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23677,
	datab => ufir_filter_aff2_adffs_a44_a,
	datac => fir_out_mux_a23642,
	datad => fir_out_mux_a23678,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23679);

fir_out_mux_a23675_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23675 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a42_a # ufir_filter_aff2_adffs_a41_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & ufir_filter_aff2_adffs_a41_a & (fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a41_a,
	datac => ufir_filter_aff2_adffs_a42_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23675);

fir_out_mux_a23680_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23680 = fir_out_mux_a23676 # fir_out_mux_a23675 # fir_out_mux_a23638 & fir_out_mux_a23679

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23638,
	datab => fir_out_mux_a23676,
	datac => fir_out_mux_a23679,
	datad => fir_out_mux_a23675,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23680);

Fir_Out_Ff_adffs_a9_a_a10954_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a9_a_a10954 = !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out & !Equal_a193 & fir_out_mux_a23680 & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Equal_a193,
	datac => fir_out_mux_a23680,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a9_a_a10954);

Fir_Out_Ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a9_a = DFFE(Fir_Out_Ff_adffs_a9_a_a10955 # Fir_Out_Ff_adffs_a9_a_a10954 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a9_a_a10955,
	datad => Fir_Out_Ff_adffs_a9_a_a10954,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a9_a);

fir_out_mux_a23682_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23682 = ufir_filter_aff2_adffs_a41_a & (fir_out_mux_a23636 # ufir_filter_aff2_adffs_a40_a & Equal_a188) # !ufir_filter_aff2_adffs_a41_a & (ufir_filter_aff2_adffs_a40_a & Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a41_a,
	datab => fir_out_mux_a23636,
	datac => ufir_filter_aff2_adffs_a40_a,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23682);

fir_out_mux_a23681_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23681 = fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a43_a # ufir_filter_aff2_adffs_a42_a & fir_out_mux_a23633) # !fir_out_mux_a23634 & (ufir_filter_aff2_adffs_a42_a & fir_out_mux_a23633)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23634,
	datab => ufir_filter_aff2_adffs_a43_a,
	datac => ufir_filter_aff2_adffs_a42_a,
	datad => fir_out_mux_a23633,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23681);

fir_out_mux_a23686_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23686 = fir_out_mux_a23682 # fir_out_mux_a23681 # fir_out_mux_a23685 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23685,
	datab => fir_out_mux_a23682,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23681,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23686);

Fir_Out_Ff_adffs_a10_a_a10957_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a10_a_a10957 = !PA_Reset_acombout & fir_out_mux_a23686 & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0004",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => fir_out_mux_a23686,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a10_a_a10957);

Fir_Out_Ff_adffs_a10_a_a10958_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a10_a_a10958 = ufir_filter_aff2_adffs_a39_a & !PA_Reset_acombout & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a39_a,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a10_a_a10958);

Fir_Out_Ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a10_a = DFFE(Fir_Out_Ff_adffs_a10_a_a10957 # Fir_Out_Ff_adffs_a10_a_a10958 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a10_a_a10957,
	datad => Fir_Out_Ff_adffs_a10_a_a10958,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a10_a);

Fir_Out_Ff_adffs_a11_a_a10961_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a11_a_a10961 = ufir_filter_aff2_adffs_a40_a & !PA_Reset_acombout & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a40_a,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a11_a_a10961);

fir_out_mux_a23687_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23687 = ufir_filter_aff2_adffs_a44_a & (fir_out_mux_a23634 # ufir_filter_aff2_adffs_a43_a & fir_out_mux_a23633) # !ufir_filter_aff2_adffs_a44_a & ufir_filter_aff2_adffs_a43_a & fir_out_mux_a23633

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a44_a,
	datab => ufir_filter_aff2_adffs_a43_a,
	datac => fir_out_mux_a23633,
	datad => fir_out_mux_a23634,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23687);

fir_out_mux_a23688_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23688 = ufir_filter_aff2_adffs_a42_a & (fir_out_mux_a23636 # ufir_filter_aff2_adffs_a41_a & Equal_a188) # !ufir_filter_aff2_adffs_a42_a & (ufir_filter_aff2_adffs_a41_a & Equal_a188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => fir_out_mux_a23636,
	datac => ufir_filter_aff2_adffs_a41_a,
	datad => Equal_a188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23688);

fir_out_mux_a23692_I : apex20ke_lcell
-- Equation(s):
-- fir_out_mux_a23692 = fir_out_mux_a23687 # fir_out_mux_a23688 # fir_out_mux_a23691 & fir_out_mux_a23638

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23691,
	datab => fir_out_mux_a23687,
	datac => fir_out_mux_a23638,
	datad => fir_out_mux_a23688,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_out_mux_a23692);

Fir_Out_Ff_adffs_a11_a_a10960_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a11_a_a10960 = !Equal_a193 & !PA_Reset_acombout & fir_out_mux_a23692 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => PA_Reset_acombout,
	datac => fir_out_mux_a23692,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a11_a_a10960);

Fir_Out_Ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a11_a = DFFE(Fir_Out_Ff_adffs_a11_a_a10961 # Fir_Out_Ff_adffs_a11_a_a10960 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a11_a_a10961,
	datad => Fir_Out_Ff_adffs_a11_a_a10960,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a11_a);

Fir_Out_Ff_adffs_a12_a_a10964_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a12_a_a10964 = ufir_filter_aff2_adffs_a41_a & !PA_Reset_acombout & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a41_a,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a12_a_a10964);

Fir_Out_Ff_adffs_a12_a_a10963_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a12_a_a10963 = fir_out_mux_a23698 & !PA_Reset_acombout & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23698,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a12_a_a10963);

Fir_Out_Ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a12_a = DFFE(Fir_Out_Ff_adffs_a12_a_a10964 # Fir_Out_Ff_adffs_a12_a_a10963 # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF2",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Out_Ff_adffs_a12_a_a10964,
	datad => Fir_Out_Ff_adffs_a12_a_a10963,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a12_a);

Fir_Out_Ff_adffs_a13_a_a10967_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a13_a_a10967 = ufir_filter_aff2_adffs_a42_a & Equal_a193 & !PA_Reset_acombout & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a42_a,
	datab => Equal_a193,
	datac => PA_Reset_acombout,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a13_a_a10967);

Fir_Out_Ff_adffs_a13_a_a10966_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a13_a_a10966 = fir_out_mux_a23704 & !Equal_a193 & !PA_Reset_acombout & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23704,
	datab => Equal_a193,
	datac => PA_Reset_acombout,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a13_a_a10966);

Fir_Out_Ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a13_a = DFFE(Fir_Out_Ff_adffs_a13_a_a10967 # Fir_Out_Ff_adffs_a13_a_a10966 # !PA_Reset_acombout & Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF4",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => Fir_Out_Ff_adffs_a13_a_a10967,
	datad => Fir_Out_Ff_adffs_a13_a_a10966,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a13_a);

Fir_Out_Ff_adffs_a14_a_a10969_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a14_a_a10969 = fir_out_mux_a23710 & !PA_Reset_acombout & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23710,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a14_a_a10969);

Fir_Out_Ff_adffs_a14_a_a10970_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a14_a_a10970 = Equal_a193 & ufir_filter_aff2_adffs_a43_a & !PA_Reset_acombout & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a193,
	datab => ufir_filter_aff2_adffs_a43_a,
	datac => PA_Reset_acombout,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a14_a_a10970);

Fir_Out_Ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a14_a = DFFE(Fir_Out_Ff_adffs_a14_a_a10969 # Fir_Out_Ff_adffs_a14_a_a10970 # !PA_Reset_acombout & Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF4",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => Fir_Out_Ff_adffs_a14_a_a10969,
	datad => Fir_Out_Ff_adffs_a14_a_a10970,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a14_a);

Fir_Out_Ff_adffs_a15_a_a10973_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a15_a_a10973 = ufir_filter_aff2_adffs_a44_a & !PA_Reset_acombout & Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a44_a,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a15_a_a10973);

Fir_Out_Ff_adffs_a15_a_a10972_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a15_a_a10972 = fir_out_mux_a23715 & !PA_Reset_acombout & !Equal_a193 & !Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_mux_a23715,
	datab => PA_Reset_acombout,
	datac => Equal_a193,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a15_a_a10972);

Fir_Out_Ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a15_a = DFFE(Fir_Out_Ff_adffs_a15_a_a10973 # Fir_Out_Ff_adffs_a15_a_a10972 # !PA_Reset_acombout & Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFDC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Out_Ff_adffs_a15_a_a10973,
	datac => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a15_a_a10972,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a15_a);

Fir_Out_Ff_adffs_a17_a_a10982_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10982 = TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a52_a # !TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a48_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B8B8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a52_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => ufir_filter_aff2_adffs_a48_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10982);

Fir_Out_Ff_adffs_a16_a_a10978_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a10978 = Fir_Out_Ff_adffs_a17_a_a10977 & TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a17_a_a10977,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a10978);

Fir_Out_Ff_adffs_a16_a_a10983_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a10983 = Fir_Out_Ff_adffs_a16_a_a10981 # Fir_Out_Ff_adffs_a16_a_a10978 # Fir_Out_Ff_adffs_a17_a_a10982 & Equal_a191

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a16_a_a10981,
	datab => Fir_Out_Ff_adffs_a17_a_a10982,
	datac => Equal_a191,
	datad => Fir_Out_Ff_adffs_a16_a_a10978,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a10983);

Fir_Out_Ff_adffs_a16_a_a10975_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a10975 = ufir_filter_aff2_adffs_a53_a & (TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a53_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a10975);

Fir_Out_Ff_adffs_a16_a_a10976_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a_a10976 = Fir_Out_Ff_adffs_a16_a_a10975 # !PA_Reset_acombout & (Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a16_a_a10975,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a16_a_a10976);

Fir_Out_Ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a16_a = DFFE(Fir_Out_Ff_adffs_a16_a_a10976 # Fir_Out_Ff_adffs_a16_a_a10983 & !TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a16_a_a10983,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a16_a_a10976,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a16_a);

Fir_Out_Ff_adffs_a17_a_a10980_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10980 = TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a51_a # !TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a47_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B8B8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a51_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => ufir_filter_aff2_adffs_a47_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10980);

Fir_Out_Ff_adffs_a17_a_a10987_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10987 = TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a1_a & Fir_Out_Ff_adffs_a17_a_a10980

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => Fir_Out_Ff_adffs_a17_a_a10980,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10987);

Fir_Out_Ff_adffs_a17_a_a10988_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10988 = !TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a1_a & (Fir_Out_Ff_adffs_a17_a_a10982) # !TimeConst_Reg_FF_adffs_a1_a & Fir_Out_Ff_adffs_a17_a_a10977)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a17_a_a10977,
	datab => Fir_Out_Ff_adffs_a17_a_a10982,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10988);

Fir_Out_Ff_adffs_a17_a_a10990_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10990 = Fir_Out_Ff_adffs_a17_a_a10987 # Fir_Out_Ff_adffs_a17_a_a10988 # Fir_Out_Ff_adffs_a18_a_a10989 & Equal_a191

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a18_a_a10989,
	datab => Fir_Out_Ff_adffs_a17_a_a10987,
	datac => Equal_a191,
	datad => Fir_Out_Ff_adffs_a17_a_a10988,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10990);

Fir_Out_Ff_adffs_a17_a_a10985_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10985 = ufir_filter_aff2_adffs_a54_a & (TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a54_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10985);

Fir_Out_Ff_adffs_a17_a_a10986_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a_a10986 = Fir_Out_Ff_adffs_a17_a_a10985 # !PA_Reset_acombout & (Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0E",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a17_a_a10985,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a17_a_a10986);

Fir_Out_Ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a17_a = DFFE(Fir_Out_Ff_adffs_a17_a_a10986 # Fir_Out_Ff_adffs_a17_a_a10990 & !TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a17_a_a10990,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a17_a_a10986,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a17_a);

Fir_Out_Ff_adffs_a18_a_a10989_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10989 = TimeConst_Reg_FF_adffs_a2_a & ufir_filter_aff2_adffs_a53_a # !TimeConst_Reg_FF_adffs_a2_a & (ufir_filter_aff2_adffs_a49_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a53_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => ufir_filter_aff2_adffs_a49_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10989);

Fir_Out_Ff_adffs_a18_a_a10996_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10996 = Fir_Out_Ff_adffs_a18_a_a11066 # TimeConst_Reg_FF_adffs_a1_a & !TimeConst_Reg_FF_adffs_a0_a & Fir_Out_Ff_adffs_a18_a_a10989

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a18_a_a11066,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Fir_Out_Ff_adffs_a18_a_a10989,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10996);

Fir_Out_Ff_adffs_a18_a_a10994_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10994 = !TimeConst_Reg_FF_adffs_a1_a & (TimeConst_Reg_FF_adffs_a0_a & (Fir_Out_Ff_adffs_a17_a_a10982) # !TimeConst_Reg_FF_adffs_a0_a & Fir_Out_Ff_adffs_a17_a_a10980)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0E02",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a17_a_a10980,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => Fir_Out_Ff_adffs_a17_a_a10982,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10994);

Fir_Out_Ff_adffs_a18_a_a10997_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10997 = Fir_Out_Ff_adffs_a18_a_a10996 # Fir_Out_Ff_adffs_a18_a_a10994

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Fir_Out_Ff_adffs_a18_a_a10996,
	datad => Fir_Out_Ff_adffs_a18_a_a10994,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10997);

Fir_Out_Ff_adffs_a18_a_a10992_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10992 = TimeConst_Reg_FF_adffs_a3_a & ufir_filter_aff2_adffs_a55_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => ufir_filter_aff2_adffs_a55_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10992);

Fir_Out_Ff_adffs_a18_a_a10993_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a_a10993 = Fir_Out_Ff_adffs_a18_a_a10992 # !PA_Reset_acombout & (Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF54",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datac => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a18_a_a10992,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a18_a_a10993);

Fir_Out_Ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a18_a = DFFE(Fir_Out_Ff_adffs_a18_a_a10993 # Fir_Out_Ff_adffs_a18_a_a10997 & !PA_Reset_acombout & !TimeConst_Reg_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a18_a_a10997,
	datab => PA_Reset_acombout,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => Fir_Out_Ff_adffs_a18_a_a10993,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a18_a);

Fir_Out_Ff_adffs_a20_a_a11007_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11007 = TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a55_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a53_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a55_a,
	datab => ufir_filter_aff2_adffs_a53_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11007);

Fir_Out_Ff_adffs_a19_a_a11002_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11002 = TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a54_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a52_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a54_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => ufir_filter_aff2_adffs_a52_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11002);

Fir_Out_Ff_adffs_a19_a_a11003_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11003 = Fir_Out_Ff_adffs_a19_a_a11067 # !TimeConst_Reg_FF_adffs_a0_a & TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a19_a_a11002

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BAAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a19_a_a11067,
	datab => TimeConst_Reg_FF_adffs_a0_a,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => Fir_Out_Ff_adffs_a19_a_a11002,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11003);

Fir_Out_Ff_adffs_a20_a_a11004_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11004 = TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a51_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a49_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a51_a,
	datab => ufir_filter_aff2_adffs_a49_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11004);

Fir_Out_Ff_adffs_a19_a_a11005_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11005 = TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a20_a_a11004

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => Fir_Out_Ff_adffs_a20_a_a11004,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11005);

Fir_Out_Ff_adffs_a19_a_a11008_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11008 = Fir_Out_Ff_adffs_a19_a_a11003 # Fir_Out_Ff_adffs_a19_a_a11005 # Fir_Out_Ff_adffs_a19_a_a11006 & Fir_Out_Ff_adffs_a20_a_a11007

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a19_a_a11006,
	datab => Fir_Out_Ff_adffs_a20_a_a11007,
	datac => Fir_Out_Ff_adffs_a19_a_a11003,
	datad => Fir_Out_Ff_adffs_a19_a_a11005,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11008);

Fir_Out_Ff_adffs_a19_a_a10999_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a10999 = ufir_filter_aff2_adffs_a56_a & TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a56_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a10999);

Fir_Out_Ff_adffs_a19_a_a11000_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11000 = Fir_Out_Ff_adffs_a19_a_a10999 # !PA_Reset_acombout & (Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF32",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a19_a_a10999,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11000);

Fir_Out_Ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a = DFFE(Fir_Out_Ff_adffs_a19_a_a11000 # Fir_Out_Ff_adffs_a19_a_a11008 & !PA_Reset_acombout & !TimeConst_Reg_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a19_a_a11008,
	datab => PA_Reset_acombout,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => Fir_Out_Ff_adffs_a19_a_a11000,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a19_a);

Fir_Out_Ff_adffs_a19_a_a11006_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a19_a_a11006 = TimeConst_Reg_FF_adffs_a2_a & (TimeConst_Reg_FF_adffs_a0_a)
-- Fir_Out_Ff_adffs_a19_a_a11072 = TimeConst_Reg_FF_adffs_a2_a & (TimeConst_Reg_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a2_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a19_a_a11006,
	cascout => Fir_Out_Ff_adffs_a19_a_a11072);

Fir_Out_Ff_adffs_a20_a_a11012_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11012 = !TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a2_a & (Fir_Out_Ff_adffs_a20_a_a11007) # !TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a20_a_a11004)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5410",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => Fir_Out_Ff_adffs_a20_a_a11004,
	datad => Fir_Out_Ff_adffs_a20_a_a11007,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11012);

Fir_Out_Ff_adffs_a21_a_a11015_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11015 = TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a56_a # !TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a54_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a56_a,
	datab => ufir_filter_aff2_adffs_a54_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11015);

Fir_Out_Ff_adffs_a20_a_a11016_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11016 = Fir_Out_Ff_adffs_a20_a_a11014 # Fir_Out_Ff_adffs_a20_a_a11012 # Fir_Out_Ff_adffs_a19_a_a11006 & Fir_Out_Ff_adffs_a21_a_a11015

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a20_a_a11014,
	datab => Fir_Out_Ff_adffs_a19_a_a11006,
	datac => Fir_Out_Ff_adffs_a20_a_a11012,
	datad => Fir_Out_Ff_adffs_a21_a_a11015,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11016);

Fir_Out_Ff_adffs_a20_a_a11010_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11010 = ufir_filter_aff2_adffs_a57_a & (TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a57_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11010);

Fir_Out_Ff_adffs_a20_a_a11011_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a_a11011 = Fir_Out_Ff_adffs_a20_a_a11010 # !PA_Reset_acombout & (Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF32",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a20_a_a11010,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a20_a_a11011);

Fir_Out_Ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a20_a = DFFE(Fir_Out_Ff_adffs_a20_a_a11011 # !PA_Reset_acombout & !TimeConst_Reg_FF_adffs_a3_a & Fir_Out_Ff_adffs_a20_a_a11016, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF10",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PA_Reset_acombout,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => Fir_Out_Ff_adffs_a20_a_a11016,
	datad => Fir_Out_Ff_adffs_a20_a_a11011,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a20_a);

Fir_Out_Ff_adffs_a22_a_a11021_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11021 = TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a53_a) # !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a51_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => ufir_filter_aff2_adffs_a51_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => ufir_filter_aff2_adffs_a53_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11021);

Fir_Out_Ff_adffs_a21_a_a11022_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11022 = TimeConst_Reg_FF_adffs_a0_a & !TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a22_a_a11021

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => TimeConst_Reg_FF_adffs_a2_a,
	datac => Fir_Out_Ff_adffs_a22_a_a11021,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11022);

Fir_Out_Ff_adffs_a22_a_a11023_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11023 = TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a57_a) # !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a55_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CACA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a55_a,
	datab => ufir_filter_aff2_adffs_a57_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11023);

Fir_Out_Ff_adffs_a21_a_a11024_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11024 = Fir_Out_Ff_adffs_a21_a_a11020 # Fir_Out_Ff_adffs_a21_a_a11022 # Fir_Out_Ff_adffs_a22_a_a11023 & Fir_Out_Ff_adffs_a19_a_a11006

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEEE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a21_a_a11020,
	datab => Fir_Out_Ff_adffs_a21_a_a11022,
	datac => Fir_Out_Ff_adffs_a22_a_a11023,
	datad => Fir_Out_Ff_adffs_a19_a_a11006,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11024);

Fir_Out_Ff_adffs_a21_a_a11018_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11018 = ufir_filter_aff2_adffs_a58_a & (TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a58_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11018);

Fir_Out_Ff_adffs_a21_a_a11019_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a_a11019 = Fir_Out_Ff_adffs_a21_a_a11018 # !PA_Reset_acombout & (Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF32",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a21_a_a11018,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a21_a_a11019);

Fir_Out_Ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a21_a = DFFE(Fir_Out_Ff_adffs_a21_a_a11019 # Fir_Out_Ff_adffs_a21_a_a11024 & !PA_Reset_acombout & !TimeConst_Reg_FF_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a21_a_a11024,
	datab => PA_Reset_acombout,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => Fir_Out_Ff_adffs_a21_a_a11019,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a21_a);

Fir_Out_Ff_adffs_a22_a_a11028_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11028 = !TimeConst_Reg_FF_adffs_a0_a & (TimeConst_Reg_FF_adffs_a2_a & Fir_Out_Ff_adffs_a22_a_a11023 # !TimeConst_Reg_FF_adffs_a2_a & (Fir_Out_Ff_adffs_a22_a_a11021))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0D08",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a2_a,
	datab => Fir_Out_Ff_adffs_a22_a_a11023,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => Fir_Out_Ff_adffs_a22_a_a11021,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11028);

Fir_Out_Ff_adffs_a22_a_a11068_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11068 = (TimeConst_Reg_FF_adffs_a1_a & (ufir_filter_aff2_adffs_a58_a) # !TimeConst_Reg_FF_adffs_a1_a & ufir_filter_aff2_adffs_a56_a) & CASCADE(Fir_Out_Ff_adffs_a19_a_a11072)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a56_a,
	datac => TimeConst_Reg_FF_adffs_a1_a,
	datad => ufir_filter_aff2_adffs_a58_a,
	cascin => Fir_Out_Ff_adffs_a19_a_a11072,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11068);

Fir_Out_Ff_adffs_a22_a_a11030_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11030 = Fir_Out_Ff_adffs_a22_a_a11068 # TimeConst_Reg_FF_adffs_a0_a & Fir_Out_Ff_adffs_a19_a_a11002 & !TimeConst_Reg_FF_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF08",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => TimeConst_Reg_FF_adffs_a0_a,
	datab => Fir_Out_Ff_adffs_a19_a_a11002,
	datac => TimeConst_Reg_FF_adffs_a2_a,
	datad => Fir_Out_Ff_adffs_a22_a_a11068,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11030);

Fir_Out_Ff_adffs_a22_a_a11031_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11031 = Fir_Out_Ff_adffs_a22_a_a11028 # Fir_Out_Ff_adffs_a22_a_a11030

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Fir_Out_Ff_adffs_a22_a_a11028,
	datad => Fir_Out_Ff_adffs_a22_a_a11030,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11031);

Fir_Out_Ff_adffs_a22_a_a11026_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11026 = ufir_filter_aff2_adffs_a59_a & (TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a59_a,
	datac => TimeConst_Reg_FF_adffs_a3_a,
	datad => PA_Reset_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11026);

Fir_Out_Ff_adffs_a22_a_a11027_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a_a11027 = Fir_Out_Ff_adffs_a22_a_a11026 # !PA_Reset_acombout & (Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out # Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF32",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datab => PA_Reset_acombout,
	datac => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	datad => Fir_Out_Ff_adffs_a22_a_a11026,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a22_a_a11027);

Fir_Out_Ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a22_a = DFFE(Fir_Out_Ff_adffs_a22_a_a11027 # Fir_Out_Ff_adffs_a22_a_a11031 & !TimeConst_Reg_FF_adffs_a3_a & !PA_Reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF02",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a22_a_a11031,
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datac => PA_Reset_acombout,
	datad => Fir_Out_Ff_adffs_a22_a_a11027,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a22_a);

Fir_Out_Ff_adffs_a23_a_a11069_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11069 = (!TimeConst_Reg_FF_adffs_a1_a & (TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a57_a # !TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a56_a))) & CASCADE(Equal_a200)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2320",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a57_a,
	datab => TimeConst_Reg_FF_adffs_a1_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a56_a,
	cascin => Equal_a200,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11069);

Fir_Out_Ff_adffs_a23_a_a11047_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11047 = TimeConst_Reg_FF_adffs_a0_a & (ufir_filter_aff2_adffs_a53_a) # !TimeConst_Reg_FF_adffs_a0_a & ufir_filter_aff2_adffs_a52_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ufir_filter_aff2_adffs_a52_a,
	datac => TimeConst_Reg_FF_adffs_a0_a,
	datad => ufir_filter_aff2_adffs_a53_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11047);

Fir_Out_Ff_adffs_a23_a_a11048_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11048 = Fir_Out_Ff_adffs_a23_a_a11069 # Equal_a192 & Fir_Out_Ff_adffs_a23_a_a11047

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a192,
	datac => Fir_Out_Ff_adffs_a23_a_a11069,
	datad => Fir_Out_Ff_adffs_a23_a_a11047,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11048);

Fir_Out_Ff_adffs_a23_a_a11053_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11053 = TimeConst_Reg_FF_adffs_a3_a & (ufir_filter_aff2_adffs_a60_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => TimeConst_Reg_FF_adffs_a3_a,
	datad => ufir_filter_aff2_adffs_a60_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11053);

Fir_Out_Ff_adffs_a23_a_a11054_I : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a_a11073 = Fir_Out_Ff_adffs_a23_a_a11052 # Fir_Out_Ff_adffs_a23_a_a11048 # Fir_Out_Ff_adffs_a23_a_a11053 # Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Fir_Out_Ff_adffs_a23_a_a11052,
	datab => Fir_Out_Ff_adffs_a23_a_a11048,
	datac => Fir_Out_Ff_adffs_a23_a_a11053,
	datad => Fir_Min_Cmp_Compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Fir_Out_Ff_adffs_a23_a_a11054,
	cascout => Fir_Out_Ff_adffs_a23_a_a11073);

Fir_Out_Ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- Fir_Out_Ff_adffs_a23_a = DFFE((!PA_Reset_acombout & !Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out) & CASCADE(Fir_Out_Ff_adffs_a23_a_a11073), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PA_Reset_acombout,
	datad => Fir_Max_Cmp_Compare_acomparator_acmp_end_aagb_out,
	cascin => Fir_Out_Ff_adffs_a23_a_a11073,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Fir_Out_Ff_adffs_a23_a);

fir_out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(0));

fir_out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(1));

fir_out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(2));

fir_out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(3));

fir_out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(4));

fir_out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(5));

fir_out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(6));

fir_out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(7));

fir_out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(8));

fir_out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(9));

fir_out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(10));

fir_out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(11));

fir_out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(12));

fir_out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(13));

fir_out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(14));

fir_out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(15));

fir_out_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(16));

fir_out_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(17));

fir_out_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(18));

fir_out_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(19));

fir_out_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(20));

fir_out_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(21));

fir_out_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(22));

fir_out_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Fir_Out_Ff_adffs_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_fir_out(23));
END structure;


