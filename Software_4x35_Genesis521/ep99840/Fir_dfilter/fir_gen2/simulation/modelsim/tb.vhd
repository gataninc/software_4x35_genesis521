-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FIR_MEM.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the delay lines to be used for the DPP-II. The delay
--		lines are broken down into the individual sections to generate all 8 time
--		constant's FIRs
--
--		It outputs 10 different version of delays from the input data (adata)
--		data1a	1us from adata
--		data1b	1us from data1a
--		data2	2us from data1b
--		data4	4us from data2
--		data8	8us from data4
--		data16	16us from data8
--		data32	32us	from data16
--		data64	64us from data32
--		data128	128us from data128
--
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal clk 		: std_logic := '0';
	signal IMR		: std_logic := '1';
	signal AD_MR		: std_logic := '1';
	signal PA_Reset	: std_logic;
	signal TimeConst	: std_logic_Vector( 3 downto 0 );
	signal offset_time	: std_logic_vector( 3 downto 0 );
	signal data_abc	: std_logic_vector( 74 downto 0 );
	signal fir_out		: std_logic_vector( 23 downto 0 );
	signal adata		: std_logic_vector( 15 downto 0 ) := x"0000";
	signal fir_out_reg	: std_logic_vector( 23 downto 0 );

	-------------------------------------------------------------------------------
	component fir_mem is
		port ( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			adata		: in		std_logic_Vector( 14 downto 0 );
			Disc_Mem		: out 	std_logic_Vector( 74 downto 0 ) );
	end component fir_mem;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_gen2 is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			PA_Reset		: in		std_logic;
			AD_MR		: in		std_logic;					-- Clear's FIR
			TimeConst 	: in		std_logic_Vector(  3 downto 0 );
			Offset_Time	: in		std_logic_Vector(  3 downto 0 );
			data_abc		: in		std_logic_Vector( 74 downto 0 );	-- First TAP
			fir_out		: out	std_logic_Vector( 23 downto 0 ));	-- FIR Output 
	end component fir_gen2;

	-------------------------------------------------------------------------------
begin
	IMR			<= '0' after 555 ns;
	clk			<= not( clk ) after 25 ns;
	AD_MR		<= '0' after 2 us;
	PA_Reset		<= '0';
	TimeConst		<= x"0";
	Offset_Time	<= x"0";
	adata		<= x"0123" after 5 us;
	
	UM : fir_mem
		port map(
			imr		=> imr,
			clk		=> clk,
			adata	=> adata( 14 downto 0 ),
			disc_mem	=> data_abc );
	
	U : fir_gen2
		port map(
			imr			=> imr,
			clk			=> clk,
			PA_Reset		=> PA_Reset,
			AD_MR		=> AD_MR,
			TimeConst 	=> TImeConst,
			Offset_Time	=> Offset_Time,
			data_abc		=> data_Abc,
			fir_out		=> fir_out );
		
		
	filter_out_proc : process
	begin
		wait until (( clk'event ) and ( clk = '1' ));
		wait for 10 ns;
		fir_out_Reg <= fir_out;
	end process;
			
-------------------------------------------------------------------------------
			
-------------------------------------------------------------------------------
end test_bench;
-------------------------------------------------------------------------------

