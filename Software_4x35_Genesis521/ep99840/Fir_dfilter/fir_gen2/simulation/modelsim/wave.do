onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/ad_mr
add wave -noupdate -format Literal -radix hexadecimal /tb/timeconst
add wave -noupdate -format Literal -radix hexadecimal /tb/adata
add wave -noupdate -format Literal -radix hexadecimal /tb/data_abc
add wave -noupdate -format Literal -radix hexadecimal /tb/offset_time
add wave -noupdate -format Analog-Step -height 150 -offset -40000.0 -radix decimal -scale 0.001 /tb/fir_out_reg
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {5236 ns} {5936 ns}
WaveRestoreZoom {3253 ns} {8549 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
