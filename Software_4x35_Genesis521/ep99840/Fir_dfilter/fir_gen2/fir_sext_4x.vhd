------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_sext_1x
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_sext_4x is 
	port( 
		in_vec		: in		std_logic_Vector( 15 downto 0 );
		out_vec		: out	std_logic_vector( 31 downto 0 ) );
end fir_sext_4x;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_sext_4x is

begin
	sext_gen : for i in 18 to 31 generate
		out_vec(i) 	<= in_vec( 15 );
	end generate;
	
	out_Vec( 17 downto 0 ) <= in_vec( 15 downto  0 ) & "00";	
			
------------------------------------------------------------------------------------
end behavioral;               -- fir_sext_4x
------------------------------------------------------------------------------------
			
