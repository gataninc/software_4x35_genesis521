------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_filter.vhd
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the six differnet "TAPS" of 
--		the delay line.
--
--	fir(t) = fir(t-1) + (in_vec1(t) + in_vec2(t) - in_vec3(t) - in_vec4(t) + in_vec5(t) + in_vec6(t) )
--
--	History
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_filter is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		AD_MR		: in		std_logic;					-- Clear's FIR
		in_vec1		: in		std_logic_vector( 31 downto 0 );
		in_vec2		: in		std_logic_vector( 31 downto 0 );
		in_vec3		: in		std_logic_vector( 31 downto 0 );
		in_vec4		: in		std_logic_vector( 31 downto 0 );
		in_vec5		: in		std_logic_vector( 31 downto 0 );
		in_vec6		: in		std_logic_vector( 31 downto 0 );
		fir_out		: out	std_logic_Vector( 31 downto 0 ));	-- FIR Output 
end fir_filter;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_filter is
	constant Acc_Width			: integer := 32;
	constant FB_LSB 			: integer := 3;

	signal sub_total1			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total2			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total3			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total4			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total5			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total6			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total7			: std_logic_Vector( acc_width-1 downto 0 );
	signal sub_total8			: std_logic_Vector( acc_width-1 downto 0 );

	signal in_vec1_reg			: std_logic_vector( 31 downto 0 );
	signal in_vec2_reg			: std_logic_vector( 31 downto 0 );
	signal in_vec3_reg			: std_logic_vector( 31 downto 0 );
	signal in_vec4_reg			: std_logic_vector( 31 downto 0 );
	signal in_vec5_reg			: std_logic_vector( 31 downto 0 );
	signal in_vec6_reg			: std_logic_vector( 31 downto 0 );
	signal sub_total6_vec 		: std_logic_vector( (2*Acc_Width)-1 downto 0 );
	signal sub_total7_vec 		: std_logic_vector( (2*Acc_Width)-1 downto 0 );
	signal sub_total8_vec 		: std_logic_vector( (2*Acc_Width)-1 downto 0 );
	signal sub_total9_vec 		: std_logic_vector( (2*Acc_Width)-1 downto 0 );
	signal sub_total10_vec 		: std_logic_vector( (2*Acc_Width)-1 downto 0 );

begin
	in_vec1_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec1,
			q		=> in_vec1_reg );

	in_vec2_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec2,
			q		=> in_vec2_reg );

	in_vec3_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec3,
			q		=> in_vec3_reg );

	in_vec4_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec4,
			q		=> in_vec4_reg );

	in_vec5_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec5,
			q		=> in_vec5_reg );

	in_vec6_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH	=> 32,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			data		=> in_vec6,
			q		=> in_vec6_reg );
	-------------------------------------------------------------------------------
	-- Filter Addition/Subtractions
	as1 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			cin				=> '0',
			dataa			=> in_vec1_reg,		-- dataa
			datab			=> in_vec2_reg,		-- datab
			result			=> sub_total1 );	-- dataa-datab

	as2 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> sub_total1,		-- dataa
			datab			=> in_vec3_reg,		-- datab
			result			=> sub_total2 );	-- dataa-datab

	as3 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> sub_total2,		-- dataa
			datab			=> in_vec4_reg,		-- datab
			result			=> sub_total3 );	-- dataa-datab

	as4 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			cin				=> '0',
			dataa			=> sub_total3,		-- dataa
			datab			=> in_vec5_reg,		-- datab
			result			=> sub_total4 );	-- dataa-datab

	as5 : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION 	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			cin				=> '0',
			dataa			=> sub_total4,			-- dataa
			datab			=> in_vec6_reg,			-- datab
			result			=> sub_total5 );		-- dataa-datab

	ff1: lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			data				=> sub_total5,
			q				=> sub_total6 );			
	-- Filter Addition/Subtractions
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Filter Accumulator from Ver 3540
--	as6: lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED",
--			LPM_TYPE			=> "LPM_ADD_SUB" )
--		port map(
--			add_sub			=> Vcc,				-- Add
--			dataa			=> sub_total8,			-- Filter Output
--			datab			=> sub_total6,		-- Prevoius Staage
--			result			=> sub_total7 );
--
--	ff2 : lpm_Ff
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_TYPE			=> "LPM_FF" )
--		port map(
--			aclr				=> IMR,
--			clock			=> CLK,
--			sclr				=> AD_MR,
--			data				=> sub_total7,
--			q				=> sub_total8 );
--	fir_out <= sub_total8;
	-- Filter Accumulator
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Filter Accumulator Ver 3541

	st6_gen : for i in 0 to (Acc_Width-1) Generate
		sub_total6_vec(i) 	 		<= '0';
		sub_total6_vec(i+Acc_Width) 	<= sub_total6(i);
	end generate;
	
	as6: lpm_add_sub
		generic map(
			LPM_WIDTH			=> 2*Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			cin				=> '0',
			dataa			=> sub_total9_vec,		-- Filter Output
			datab			=> sub_total6_Vec,		-- Prevoius Staage
			result			=> sub_total7_vec );
			
	as7: lpm_add_sub
		generic map(
			LPM_WIDTH			=> 2*Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',				-- 
			dataa			=> sub_total7_vec,			-- Filter Output
			datab			=> sub_total10_vec,		-- Prevoius Staage
			result			=> sub_total8_vec );

	ff2 : lpm_Ff
		generic map(
			LPM_WIDTH			=> 2*Acc_Width,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			sclr				=> AD_MR,
			data				=> sub_total8_vec,
			q				=> sub_total9_vec );

	st10z_gen : for i in 0 to FB_LSB generate
		sub_total10_vec( i ) <= '0';
	end generate;
	
	sub_total10_vec( Acc_Width-1+(FB_LSB+1) downto 0+(FB_LSB+1) ) <= sub_total9_vec( (2*Acc_Width)-1 downto Acc_Width );
	st10_gen : for i in (Acc_Width+(FB_LSB+1)) to (2*Acc_width)-1 generate
		sub_total10_vec(i) <= sub_total9_vec((2*Acc_Width)-1);
	end generate;	
	
	fir_out <= sub_total9_vec( (2*Acc_Width)-1 downto Acc_Width );
	-- Filter Accumulator
	-------------------------------------------------------------------------------

------------------------------------------------------------------------------------
end behavioral;               -- fir_gen2
------------------------------------------------------------------------------------
			
