-- Copyright (C) 1991-2002 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 2.1 Build 189 09/05/2002 Service Pack 1 SJ Full Version"

-- DATE "05/19/2003 10:57:45"

--
-- Device: Altera EP20K300EFC672-2X Package FBGA672
-- 

-- 
-- This VHDL file should be used for MODELSIM-ALTERA (VHDL OUTPUT FROM QUARTUS II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	Cntrate IS
    PORT (
	CLK20 : IN std_logic;
	GMode : IN std_logic;
	hc_rate : IN std_logic_vector(20 DOWNTO 0);
	hc_threshold : IN std_logic_vector(15 DOWNTO 0);
	IFDISC : IN std_logic;
	IMR : IN std_logic;
	LS_Abort : IN std_logic;
	PA_Reset : IN std_logic;
	PEAK_DONE : IN std_logic;
	PRESET : IN std_logic_vector(31 DOWNTO 0);
	PRESET_MODE : IN std_logic_vector(2 DOWNTO 0);
	ROI_Inc : IN std_logic;
	Time_Clr : IN std_logic;
	Time_Start : IN std_logic;
	Time_Stop : IN std_logic;
	CPS : OUT std_logic_vector(20 DOWNTO 0);
	CTIME : OUT std_logic_vector(31 DOWNTO 0);
	Det_Sat : OUT std_logic;
	Hi_Cnt : OUT std_logic;
	LTIME : OUT std_logic_vector(31 DOWNTO 0);
	ms100_tc : OUT std_logic;
	NPS : OUT std_logic_vector(20 DOWNTO 0);
	PAMR : OUT std_logic_vector(20 DOWNTO 0);
	Preset_Out : OUT std_logic;
	Time_Enable : OUT std_logic
	);
END Cntrate;

ARCHITECTURE structure OF Cntrate IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_GMode : std_logic;
SIGNAL ww_hc_rate : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_hc_threshold : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IFDISC : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_LS_Abort : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_PEAK_DONE : std_logic;
SIGNAL ww_PRESET : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_PRESET_MODE : std_logic_vector(2 DOWNTO 0);
SIGNAL ww_ROI_Inc : std_logic;
SIGNAL ww_Time_Clr : std_logic;
SIGNAL ww_Time_Start : std_logic;
SIGNAL ww_Time_Stop : std_logic;
SIGNAL ww_CPS : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_CTIME : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_Det_Sat : std_logic;
SIGNAL ww_Hi_Cnt : std_logic;
SIGNAL ww_LTIME : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_ms100_tc : std_logic;
SIGNAL ww_NPS : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_PAMR : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_Preset_Out : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress : std_logic_vector(15 DOWNTO 0);
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010000000000000101";
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL ms1_cnt_nx6 : std_logic;
SIGNAL ms1_cnt_1 : std_logic;
SIGNAL ms1_cnt_nx12 : std_logic;
SIGNAL ms1_cnt_2 : std_logic;
SIGNAL ms1_cnt_nx18 : std_logic;
SIGNAL ms1_cnt_3 : std_logic;
SIGNAL ms1_cnt_nx24 : std_logic;
SIGNAL ms1_cnt_4 : std_logic;
SIGNAL ms1_cnt_nx30 : std_logic;
SIGNAL ms1_cnt_5 : std_logic;
SIGNAL ms1_cnt_nx36 : std_logic;
SIGNAL ms1_cnt_6 : std_logic;
SIGNAL ms1_cnt_nx42 : std_logic;
SIGNAL ms1_cnt_7 : std_logic;
SIGNAL ms1_cnt_nx48 : std_logic;
SIGNAL ms1_cnt_8 : std_logic;
SIGNAL ms1_cnt_nx58 : std_logic;
SIGNAL ms1_cnt_9 : std_logic;
SIGNAL ms1_cnt_nx62 : std_logic;
SIGNAL ms1_cnt_10 : std_logic;
SIGNAL ms1_cnt_nx66 : std_logic;
SIGNAL ms1_cnt_11 : std_logic;
SIGNAL ms1_cnt_nx70 : std_logic;
SIGNAL ms1_cnt_12 : std_logic;
SIGNAL ms1_cnt_nx74 : std_logic;
SIGNAL ms1_cnt_13 : std_logic;
SIGNAL ms1_cnt_nx78 : std_logic;
SIGNAL ms1_cnt_14 : std_logic;
SIGNAL ix546_a44 : std_logic;
SIGNAL ix651_a30 : std_logic;
SIGNAL ms1_cnt_0 : std_logic;
SIGNAL ix570_a3 : std_logic;
SIGNAL nx1159_a14 : std_logic;
SIGNAL nx1134_a12 : std_logic;
SIGNAL ms100_cnt_nx35 : std_logic;
SIGNAL ms100_cnt_6 : std_logic;
SIGNAL ms100_cnt_0 : std_logic;
SIGNAL nx1147_a30 : std_logic;
SIGNAL nx1138_a12 : std_logic;
SIGNAL ms100_cnt_nx7 : std_logic;
SIGNAL ms100_cnt_1 : std_logic;
SIGNAL ms100_cnt_nx13 : std_logic;
SIGNAL ms100_cnt_2 : std_logic;
SIGNAL ms100_cnt_nx19 : std_logic;
SIGNAL ms100_cnt_3 : std_logic;
SIGNAL ms100_cnt_nx25 : std_logic;
SIGNAL ms100_cnt_4 : std_logic;
SIGNAL ms100_cnt_nx31 : std_logic;
SIGNAL ms100_cnt_5 : std_logic;
SIGNAL nx1147_a2 : std_logic;
SIGNAL nx1134_a14 : std_logic;
SIGNAL nx1145_a10 : std_logic;
SIGNAL IN_CPS_ams100_tc_vec_a0_a : std_logic;
SIGNAL OUT_CPS_ams100_tc_vec_a1_a : std_logic;
SIGNAL IN_CPS_ams100tc_Del : std_logic;
SIGNAL IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91 : std_logic;
SIGNAL IN_CPS_aCPS_STATE_a1_a : std_logic;
SIGNAL IN_CPS_aCPS_STATE_a2_a : std_logic;
SIGNAL rtl_a491 : std_logic;
SIGNAL IN_CPS_aCPS_STATE_a0_a : std_logic;
SIGNAL IN_CPS_aCPS_STATE_a3_a : std_logic;
SIGNAL IN_CPS_areduce_nor_17 : std_logic;
SIGNAL IFDISC_acombout : std_logic;
SIGNAL IN_CPS_ainc_cps_vec_a0_a : std_logic;
SIGNAL IN_CPS_ainc_cps_vec_a1_a : std_logic;
SIGNAL IN_CPS_areduce_nor_6 : std_logic;
SIGNAL IN_CPS_areduce_nor_4 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL IN_CPS_areduce_nor_7 : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a0_a_a121 : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a0_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL IN_CPS_acps_a0_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a1_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL IN_CPS_acps_a1_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a10_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a9_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a8_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a7_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a6_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a5_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a4_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a3_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a2_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL IN_CPS_acps_a2_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL IN_CPS_acps_a3_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL IN_CPS_acps_a4_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL IN_CPS_acps_a5_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL IN_CPS_acps_a6_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL IN_CPS_acps_a7_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL IN_CPS_acps_a8_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a : std_logic;
SIGNAL IN_CPS_acps_a9_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a : std_logic;
SIGNAL IN_CPS_acps_a10_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a11_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a : std_logic;
SIGNAL IN_CPS_acps_a11_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a12_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a : std_logic;
SIGNAL IN_CPS_acps_a12_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a13_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a : std_logic;
SIGNAL IN_CPS_acps_a13_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a14_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a : std_logic;
SIGNAL IN_CPS_acps_a14_a_areg0 : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL IN_CPS_aCPS_RAM_asram_aq_a15_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a : std_logic;
SIGNAL IN_CPS_acps_a15_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a : std_logic;
SIGNAL IN_CPS_acps_a16_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a : std_logic;
SIGNAL IN_CPS_acps_a17_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a : std_logic;
SIGNAL IN_CPS_acps_a18_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a : std_logic;
SIGNAL IN_CPS_acps_a19_a_areg0 : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a : std_logic;
SIGNAL IN_CPS_acps_a20_a_areg0 : std_logic;
SIGNAL PRESET_MODE_a2_a_acombout : std_logic;
SIGNAL PRESET_MODE_a0_a_acombout : std_logic;
SIGNAL PRESET_MODE_a1_a_acombout : std_logic;
SIGNAL ps1_cnt_0 : std_logic;
SIGNAL ix576_a13 : std_logic;
SIGNAL ps1_cnt_nx12 : std_logic;
SIGNAL ps1_cnt_2 : std_logic;
SIGNAL ps1_cnt_nx18 : std_logic;
SIGNAL ps1_cnt_3 : std_logic;
SIGNAL ps1_cnt_nx24 : std_logic;
SIGNAL ps1_cnt_4 : std_logic;
SIGNAL ps1_cnt_nx30 : std_logic;
SIGNAL ps1_cnt_5 : std_logic;
SIGNAL ps1_cnt_nx36 : std_logic;
SIGNAL ps1_cnt_6 : std_logic;
SIGNAL ix571_a30 : std_logic;
SIGNAL ix571_a43 : std_logic;
SIGNAL ix571_a48 : std_logic;
SIGNAL ps1_cnt_nx42 : std_logic;
SIGNAL ps1_cnt_7 : std_logic;
SIGNAL ps1_cnt_nx48 : std_logic;
SIGNAL ps1_cnt_8 : std_logic;
SIGNAL ps1_cnt_nx58 : std_logic;
SIGNAL ps1_cnt_9 : std_logic;
SIGNAL ps1_cnt_nx62 : std_logic;
SIGNAL ps1_cnt_10 : std_logic;
SIGNAL ps1_cnt_nx66 : std_logic;
SIGNAL ps1_cnt_11 : std_logic;
SIGNAL ps1_cnt_nx70 : std_logic;
SIGNAL ps1_cnt_12 : std_logic;
SIGNAL ps1_cnt_nx74 : std_logic;
SIGNAL ps1_cnt_13 : std_logic;
SIGNAL ps1_cnt_nx78 : std_logic;
SIGNAL ps1_cnt_14 : std_logic;
SIGNAL ix572_a3 : std_logic;
SIGNAL ix643_a36 : std_logic;
SIGNAL nx2936_a20 : std_logic;
SIGNAL nx1140_a1 : std_logic;
SIGNAL Time_Start_acombout : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL PA_Reset_Vec_0 : std_logic;
SIGNAL nx1149_a3 : std_logic;
SIGNAL Time_Stop_acombout : std_logic;
SIGNAL nx1130_a2 : std_logic;
SIGNAL PA_Reset_Vec_1 : std_logic;
SIGNAL nx1140_a33 : std_logic;
SIGNAL nx803_a21 : std_logic;
SIGNAL LS_Abort_acombout : std_logic;
SIGNAL nx1142_a1 : std_logic;
SIGNAL PRESET_a31_a_acombout : std_logic;
SIGNAL PEAK_DONE_acombout : std_logic;
SIGNAL OUT_CPS_ainc_cps_vec_a0_a : std_logic;
SIGNAL OUT_CPS_ainc_cps_vec_a1_a : std_logic;
SIGNAL nx1151_a0 : std_logic;
SIGNAL a_2_a20 : std_logic;
SIGNAL ix604_a1 : std_logic;
SIGNAL a_3_a18 : std_logic;
SIGNAL OUT_CPS_areduce_nor_6 : std_logic;
SIGNAL nx1155_a0 : std_logic;
SIGNAL nx2018_a2 : std_logic;
SIGNAL lt_cntr_0 : std_logic;
SIGNAL nx3615 : std_logic;
SIGNAL b_3_dup_2260 : std_logic;
SIGNAL lt_cntr_1 : std_logic;
SIGNAL ix2152_nx27 : std_logic;
SIGNAL b_3_dup_2255 : std_logic;
SIGNAL lt_cntr_2 : std_logic;
SIGNAL ix2152_nx31 : std_logic;
SIGNAL b_3_dup_2250 : std_logic;
SIGNAL lt_cntr_3 : std_logic;
SIGNAL ix2152_nx35 : std_logic;
SIGNAL b_3_dup_2245 : std_logic;
SIGNAL lt_cntr_4 : std_logic;
SIGNAL ix2152_nx39 : std_logic;
SIGNAL b_3_dup_2240 : std_logic;
SIGNAL lt_cntr_5 : std_logic;
SIGNAL ix579_a3 : std_logic;
SIGNAL a_1_a18 : std_logic;
SIGNAL nx1155_a11 : std_logic;
SIGNAL a_1_dup_2234_a18 : std_logic;
SIGNAL ix656_a7 : std_logic;
SIGNAL ix2152_nx43 : std_logic;
SIGNAL b_3 : std_logic;
SIGNAL nx1153_a9 : std_logic;
SIGNAL ix656_a13 : std_logic;
SIGNAL nx2552 : std_logic;
SIGNAL ix580_a3 : std_logic;
SIGNAL nx981_a18 : std_logic;
SIGNAL lt_inc_en : std_logic;
SIGNAL Time_Clr_acombout : std_logic;
SIGNAL nx1130_a35 : std_logic;
SIGNAL nx2929_a10 : std_logic;
SIGNAL lt1_cnt_0 : std_logic;
SIGNAL ix578_a3 : std_logic;
SIGNAL ix641_a39 : std_logic;
SIGNAL nx1136_a12 : std_logic;
SIGNAL nx1130_a26 : std_logic;
SIGNAL ix646_a3 : std_logic;
SIGNAL ix563_a61 : std_logic;
SIGNAL nx2930_a10 : std_logic;
SIGNAL lt1_cnt_nx7 : std_logic;
SIGNAL lt1_cnt_1 : std_logic;
SIGNAL lt1_cnt_nx13 : std_logic;
SIGNAL lt1_cnt_2 : std_logic;
SIGNAL lt1_cnt_nx19 : std_logic;
SIGNAL lt1_cnt_3 : std_logic;
SIGNAL lt1_cnt_nx25 : std_logic;
SIGNAL lt1_cnt_4 : std_logic;
SIGNAL lt1_cnt_nx31 : std_logic;
SIGNAL lt1_cnt_5 : std_logic;
SIGNAL lt1_cnt_nx37 : std_logic;
SIGNAL lt1_cnt_6 : std_logic;
SIGNAL lt1_cnt_nx43 : std_logic;
SIGNAL lt1_cnt_7 : std_logic;
SIGNAL lt1_cnt_nx49 : std_logic;
SIGNAL lt1_cnt_8 : std_logic;
SIGNAL lt1_cnt_nx59 : std_logic;
SIGNAL lt1_cnt_9 : std_logic;
SIGNAL lt1_cnt_nx63 : std_logic;
SIGNAL lt1_cnt_10 : std_logic;
SIGNAL lt1_cnt_nx67 : std_logic;
SIGNAL lt1_cnt_11 : std_logic;
SIGNAL lt1_cnt_nx71 : std_logic;
SIGNAL lt1_cnt_12 : std_logic;
SIGNAL lt1_cnt_nx75 : std_logic;
SIGNAL lt1_cnt_13 : std_logic;
SIGNAL lt1_cnt_nx79 : std_logic;
SIGNAL lt1_cnt_14 : std_logic;
SIGNAL ix600_a31 : std_logic;
SIGNAL ix652_a27 : std_logic;
SIGNAL nx1136_a14 : std_logic;
SIGNAL LT_CEN_a10 : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL ltime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL ctime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL ix609_a6 : std_logic;
SIGNAL nx2896_a10 : std_logic;
SIGNAL PRESET_a30_a_acombout : std_logic;
SIGNAL ix610_a6 : std_logic;
SIGNAL nx2897_a10 : std_logic;
SIGNAL PRESET_a29_a_acombout : std_logic;
SIGNAL ROI_Inc_acombout : std_logic;
SIGNAL ROI_INC_DEL : std_logic;
SIGNAL ROI_INCREMENT_a0 : std_logic;
SIGNAL ROI_CLR_a2 : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL ix516_a21 : std_logic;
SIGNAL ix516_a67 : std_logic;
SIGNAL ix611_a6 : std_logic;
SIGNAL nx2898_a10 : std_logic;
SIGNAL ix517_a21 : std_logic;
SIGNAL ix517_a67 : std_logic;
SIGNAL ix612_a6 : std_logic;
SIGNAL nx2899_a10 : std_logic;
SIGNAL PRESET_a28_a_acombout : std_logic;
SIGNAL PRESET_a27_a_acombout : std_logic;
SIGNAL ix518_a21 : std_logic;
SIGNAL ix518_a67 : std_logic;
SIGNAL ix613_a6 : std_logic;
SIGNAL nx2900_a10 : std_logic;
SIGNAL ix519_a21 : std_logic;
SIGNAL ix519_a67 : std_logic;
SIGNAL ix614_a6 : std_logic;
SIGNAL nx2901_a10 : std_logic;
SIGNAL PRESET_a26_a_acombout : std_logic;
SIGNAL PRESET_a25_a_acombout : std_logic;
SIGNAL ix520_a21 : std_logic;
SIGNAL ix520_a67 : std_logic;
SIGNAL ix615_a6 : std_logic;
SIGNAL nx2902_a10 : std_logic;
SIGNAL PRESET_a24_a_acombout : std_logic;
SIGNAL ix521_a21 : std_logic;
SIGNAL ix521_a67 : std_logic;
SIGNAL ix616_a6 : std_logic;
SIGNAL nx2903_a10 : std_logic;
SIGNAL ix522_a21 : std_logic;
SIGNAL ix522_a67 : std_logic;
SIGNAL ix617_a6 : std_logic;
SIGNAL nx2904_a10 : std_logic;
SIGNAL PRESET_a23_a_acombout : std_logic;
SIGNAL PRESET_a22_a_acombout : std_logic;
SIGNAL ix523_a21 : std_logic;
SIGNAL ix523_a67 : std_logic;
SIGNAL ix618_a6 : std_logic;
SIGNAL nx2905_a10 : std_logic;
SIGNAL ix524_a21 : std_logic;
SIGNAL ix524_a67 : std_logic;
SIGNAL ix619_a6 : std_logic;
SIGNAL nx2906_a10 : std_logic;
SIGNAL PRESET_a21_a_acombout : std_logic;
SIGNAL PRESET_a20_a_acombout : std_logic;
SIGNAL ix525_a21 : std_logic;
SIGNAL ix525_a67 : std_logic;
SIGNAL ix620_a6 : std_logic;
SIGNAL nx2907_a10 : std_logic;
SIGNAL PRESET_a19_a_acombout : std_logic;
SIGNAL ix526_a21 : std_logic;
SIGNAL ix526_a67 : std_logic;
SIGNAL ix621_a6 : std_logic;
SIGNAL nx2908_a10 : std_logic;
SIGNAL ix527_a21 : std_logic;
SIGNAL ix527_a67 : std_logic;
SIGNAL ix622_a6 : std_logic;
SIGNAL nx2909_a10 : std_logic;
SIGNAL PRESET_a18_a_acombout : std_logic;
SIGNAL ix528_a21 : std_logic;
SIGNAL ix528_a67 : std_logic;
SIGNAL ix623_a6 : std_logic;
SIGNAL nx2910_a10 : std_logic;
SIGNAL PRESET_a17_a_acombout : std_logic;
SIGNAL ix529_a21 : std_logic;
SIGNAL ix529_a67 : std_logic;
SIGNAL ix624_a6 : std_logic;
SIGNAL nx2911_a10 : std_logic;
SIGNAL PRESET_a16_a_acombout : std_logic;
SIGNAL PRESET_a15_a_acombout : std_logic;
SIGNAL ix530_a21 : std_logic;
SIGNAL ix530_a67 : std_logic;
SIGNAL ix625_a6 : std_logic;
SIGNAL nx2912_a10 : std_logic;
SIGNAL PRESET_a14_a_acombout : std_logic;
SIGNAL ix531_a21 : std_logic;
SIGNAL ix531_a67 : std_logic;
SIGNAL ix626_a6 : std_logic;
SIGNAL nx2913_a10 : std_logic;
SIGNAL PRESET_a13_a_acombout : std_logic;
SIGNAL ix532_a21 : std_logic;
SIGNAL ix532_a67 : std_logic;
SIGNAL ix627_a6 : std_logic;
SIGNAL nx2914_a10 : std_logic;
SIGNAL ix533_a21 : std_logic;
SIGNAL ix533_a67 : std_logic;
SIGNAL ix628_a6 : std_logic;
SIGNAL nx2915_a10 : std_logic;
SIGNAL PRESET_a12_a_acombout : std_logic;
SIGNAL PRESET_a11_a_acombout : std_logic;
SIGNAL ix534_a21 : std_logic;
SIGNAL ix534_a67 : std_logic;
SIGNAL ix629_a6 : std_logic;
SIGNAL nx2916_a10 : std_logic;
SIGNAL ix535_a21 : std_logic;
SIGNAL ix535_a67 : std_logic;
SIGNAL ix630_a6 : std_logic;
SIGNAL nx2917_a10 : std_logic;
SIGNAL PRESET_a10_a_acombout : std_logic;
SIGNAL ix536_a21 : std_logic;
SIGNAL ix536_a67 : std_logic;
SIGNAL ix631_a6 : std_logic;
SIGNAL nx2918_a10 : std_logic;
SIGNAL PRESET_a9_a_acombout : std_logic;
SIGNAL ix537_a21 : std_logic;
SIGNAL ix537_a67 : std_logic;
SIGNAL ix632_a6 : std_logic;
SIGNAL nx2919_a10 : std_logic;
SIGNAL PRESET_a8_a_acombout : std_logic;
SIGNAL ix538_a21 : std_logic;
SIGNAL ix538_a67 : std_logic;
SIGNAL ix633_a6 : std_logic;
SIGNAL nx2920_a10 : std_logic;
SIGNAL PRESET_a7_a_acombout : std_logic;
SIGNAL PRESET_a6_a_acombout : std_logic;
SIGNAL ix539_a21 : std_logic;
SIGNAL ix539_a67 : std_logic;
SIGNAL ix634_a6 : std_logic;
SIGNAL nx2921_a10 : std_logic;
SIGNAL PRESET_a5_a_acombout : std_logic;
SIGNAL ix540_a21 : std_logic;
SIGNAL ix540_a67 : std_logic;
SIGNAL ix635_a6 : std_logic;
SIGNAL nx2922_a10 : std_logic;
SIGNAL PRESET_a4_a_acombout : std_logic;
SIGNAL ix541_a21 : std_logic;
SIGNAL ix541_a67 : std_logic;
SIGNAL ix636_a6 : std_logic;
SIGNAL nx2923_a10 : std_logic;
SIGNAL PRESET_a3_a_acombout : std_logic;
SIGNAL ix542_a21 : std_logic;
SIGNAL ix542_a67 : std_logic;
SIGNAL ix637_a6 : std_logic;
SIGNAL nx2924_a10 : std_logic;
SIGNAL PRESET_a2_a_acombout : std_logic;
SIGNAL ix543_a21 : std_logic;
SIGNAL ix543_a67 : std_logic;
SIGNAL ix638_a6 : std_logic;
SIGNAL nx2925_a10 : std_logic;
SIGNAL PRESET_a1_a_acombout : std_logic;
SIGNAL ix544_a21 : std_logic;
SIGNAL ix544_a67 : std_logic;
SIGNAL ix639_a6 : std_logic;
SIGNAL nx2926_a10 : std_logic;
SIGNAL ltime_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL ROI_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL ix545_a21 : std_logic;
SIGNAL ix545_a67 : std_logic;
SIGNAL ix640_a6 : std_logic;
SIGNAL nx2927_a10 : std_logic;
SIGNAL PRESET_a0_a_acombout : std_logic;
SIGNAL modgen_gt_13_nx70 : std_logic;
SIGNAL modgen_gt_13_nx72 : std_logic;
SIGNAL modgen_gt_13_nx74 : std_logic;
SIGNAL modgen_gt_13_nx76 : std_logic;
SIGNAL modgen_gt_13_nx78 : std_logic;
SIGNAL modgen_gt_13_nx80 : std_logic;
SIGNAL modgen_gt_13_nx82 : std_logic;
SIGNAL modgen_gt_13_nx84 : std_logic;
SIGNAL modgen_gt_13_nx86 : std_logic;
SIGNAL modgen_gt_13_nx88 : std_logic;
SIGNAL modgen_gt_13_nx90 : std_logic;
SIGNAL modgen_gt_13_nx92 : std_logic;
SIGNAL modgen_gt_13_nx94 : std_logic;
SIGNAL modgen_gt_13_nx96 : std_logic;
SIGNAL modgen_gt_13_nx98 : std_logic;
SIGNAL modgen_gt_13_nx100 : std_logic;
SIGNAL modgen_gt_13_nx102 : std_logic;
SIGNAL modgen_gt_13_nx104 : std_logic;
SIGNAL modgen_gt_13_nx106 : std_logic;
SIGNAL modgen_gt_13_nx108 : std_logic;
SIGNAL modgen_gt_13_nx110 : std_logic;
SIGNAL modgen_gt_13_nx112 : std_logic;
SIGNAL modgen_gt_13_nx114 : std_logic;
SIGNAL modgen_gt_13_nx116 : std_logic;
SIGNAL modgen_gt_13_nx118 : std_logic;
SIGNAL modgen_gt_13_nx120 : std_logic;
SIGNAL modgen_gt_13_nx122 : std_logic;
SIGNAL modgen_gt_13_nx124 : std_logic;
SIGNAL modgen_gt_13_nx126 : std_logic;
SIGNAL modgen_gt_13_nx128 : std_logic;
SIGNAL modgen_gt_13_nx130 : std_logic;
SIGNAL NOT_nx438 : std_logic;
SIGNAL Preset_Done_a8 : std_logic;
SIGNAL ix602_a1 : std_logic;
SIGNAL nx2964_a12 : std_logic;
SIGNAL Preset_Enable : std_logic;
SIGNAL nx1157_a0 : std_logic;
SIGNAL ix645_a1 : std_logic;
SIGNAL nx2966_a12 : std_logic;
SIGNAL ix586_a3 : std_logic;
SIGNAL ix597_a18 : std_logic;
SIGNAL nx2963_a11 : std_logic;
SIGNAL ix609_a284 : std_logic;
SIGNAL ix590_a3 : std_logic;
SIGNAL nx2969_a12 : std_logic;
SIGNAL ix592_a14 : std_logic;
SIGNAL ix599_a38 : std_logic;
SIGNAL ix589_a3 : std_logic;
SIGNAL ix600_a40 : std_logic;
SIGNAL nx2967_a11 : std_logic;
SIGNAL ix587_a3 : std_logic;
SIGNAL nx2972_a21 : std_logic;
SIGNAL ix573_a14 : std_logic;
SIGNAL ix753_a22 : std_logic;
SIGNAL ix594_a3 : std_logic;
SIGNAL nx2959_a21 : std_logic;
SIGNAL ix753_a2 : std_logic;
SIGNAL nx975_a10 : std_logic;
SIGNAL Preset_Done_a7 : std_logic;
SIGNAL nx3414_a15 : std_logic;
SIGNAL nx1700_a2 : std_logic;
SIGNAL Time_Enable_dup0 : std_logic;
SIGNAL nx1694_a1 : std_logic;
SIGNAL time_start_latch : std_logic;
SIGNAL ix553_a20 : std_logic;
SIGNAL nx1149_a68 : std_logic;
SIGNAL nx2928_a10 : std_logic;
SIGNAL ps1_cnt_nx6 : std_logic;
SIGNAL ps1_cnt_1 : std_logic;
SIGNAL nx1142_a26 : std_logic;
SIGNAL nx2941_a12 : std_logic;
SIGNAL ix574_a3 : std_logic;
SIGNAL nx3014_a12 : std_logic;
SIGNAL nx1157_a18 : std_logic;
SIGNAL nx2938_a20 : std_logic;
SIGNAL CT_CEN_a1 : std_logic;
SIGNAL ctime_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL GMode_acombout : std_logic;
SIGNAL nx1780_a1 : std_logic;
SIGNAL PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91 : std_logic;
SIGNAL PAMR_CPS_aCPS_STATE_a1_a : std_logic;
SIGNAL PAMR_CPS_aCPS_STATE_a3_a : std_logic;
SIGNAL rtl_a497 : std_logic;
SIGNAL PAMR_CPS_aCPS_STATE_a0_a : std_logic;
SIGNAL PAMR_CPS_aCPS_STATE_a2_a : std_logic;
SIGNAL PAMR_CPS_areduce_nor_17 : std_logic;
SIGNAL PAMR_CPS_areduce_nor_6 : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL PAMR_CPS_areduce_nor_7 : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121 : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a9_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a8_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a7_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a6_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a5_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a4_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a3_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a2_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a1_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a0_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL PAMR_CPS_acps_a0_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL PAMR_CPS_acps_a1_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL PAMR_CPS_acps_a2_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL PAMR_CPS_acps_a3_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL PAMR_CPS_acps_a4_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL PAMR_CPS_acps_a5_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL PAMR_CPS_acps_a6_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL PAMR_CPS_acps_a7_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL PAMR_CPS_acps_a8_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a : std_logic;
SIGNAL PAMR_CPS_acps_a9_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a10_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a : std_logic;
SIGNAL PAMR_CPS_acps_a10_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a13_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a12_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a11_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a : std_logic;
SIGNAL PAMR_CPS_acps_a11_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a : std_logic;
SIGNAL PAMR_CPS_acps_a12_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a : std_logic;
SIGNAL PAMR_CPS_acps_a13_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a14_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a : std_logic;
SIGNAL PAMR_CPS_acps_a14_a_areg0 : std_logic;
SIGNAL ix582_a3 : std_logic;
SIGNAL nx2976_a12 : std_logic;
SIGNAL ix583_a3 : std_logic;
SIGNAL nx2977_a12 : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL PAMR_CPS_aCPS_RAM_asram_aq_a15_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a : std_logic;
SIGNAL PAMR_CPS_acps_a15_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a : std_logic;
SIGNAL PAMR_CPS_acps_a16_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a : std_logic;
SIGNAL PAMR_CPS_acps_a17_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a : std_logic;
SIGNAL PAMR_CPS_acps_a18_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a : std_logic;
SIGNAL PAMR_CPS_acps_a19_a_areg0 : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a : std_logic;
SIGNAL PAMR_CPS_acps_a20_a_areg0 : std_logic;
SIGNAL ix581_a3 : std_logic;
SIGNAL nx2931_a10 : std_logic;
SIGNAL Sat_Cnt_nx7 : std_logic;
SIGNAL Sat_Cnt_1 : std_logic;
SIGNAL Sat_Cnt_nx13 : std_logic;
SIGNAL Sat_Cnt_2 : std_logic;
SIGNAL Sat_Cnt_nx19 : std_logic;
SIGNAL Sat_Cnt_3 : std_logic;
SIGNAL Sat_Cnt_nx25 : std_logic;
SIGNAL Sat_Cnt_4 : std_logic;
SIGNAL Sat_Cnt_0 : std_logic;
SIGNAL ix650_a24 : std_logic;
SIGNAL ix650_a34 : std_logic;
SIGNAL Sat_Cnt_nx31 : std_logic;
SIGNAL Sat_Cnt_5 : std_logic;
SIGNAL Sat_Cnt_nx38 : std_logic;
SIGNAL Sat_Cnt_6 : std_logic;
SIGNAL Sat_Cnt_nx43 : std_logic;
SIGNAL Sat_Cnt_7 : std_logic;
SIGNAL Sat_Cnt_nx47 : std_logic;
SIGNAL Sat_Cnt_8 : std_logic;
SIGNAL ix584_a3 : std_logic;
SIGNAL Det_Sat_dup0 : std_logic;
SIGNAL hc_rate_a20_a_acombout : std_logic;
SIGNAL hc_rate_a19_a_acombout : std_logic;
SIGNAL hc_rate_a18_a_acombout : std_logic;
SIGNAL hc_rate_a17_a_acombout : std_logic;
SIGNAL hc_rate_a16_a_acombout : std_logic;
SIGNAL hc_rate_a15_a_acombout : std_logic;
SIGNAL hc_rate_a14_a_acombout : std_logic;
SIGNAL hc_rate_a13_a_acombout : std_logic;
SIGNAL hc_rate_a12_a_acombout : std_logic;
SIGNAL hc_rate_a11_a_acombout : std_logic;
SIGNAL hc_rate_a10_a_acombout : std_logic;
SIGNAL hc_rate_a9_a_acombout : std_logic;
SIGNAL hc_rate_a8_a_acombout : std_logic;
SIGNAL hc_rate_a7_a_acombout : std_logic;
SIGNAL hc_rate_a6_a_acombout : std_logic;
SIGNAL hc_rate_a5_a_acombout : std_logic;
SIGNAL hc_rate_a4_a_acombout : std_logic;
SIGNAL hc_rate_a3_a_acombout : std_logic;
SIGNAL hc_rate_a2_a_acombout : std_logic;
SIGNAL hc_rate_a1_a_acombout : std_logic;
SIGNAL hc_rate_a0_a_acombout : std_logic;
SIGNAL modgen_gt_2_nx48 : std_logic;
SIGNAL modgen_gt_2_nx50 : std_logic;
SIGNAL modgen_gt_2_nx52 : std_logic;
SIGNAL modgen_gt_2_nx54 : std_logic;
SIGNAL modgen_gt_2_nx56 : std_logic;
SIGNAL modgen_gt_2_nx58 : std_logic;
SIGNAL modgen_gt_2_nx60 : std_logic;
SIGNAL modgen_gt_2_nx62 : std_logic;
SIGNAL modgen_gt_2_nx64 : std_logic;
SIGNAL modgen_gt_2_nx66 : std_logic;
SIGNAL modgen_gt_2_nx68 : std_logic;
SIGNAL modgen_gt_2_nx70 : std_logic;
SIGNAL modgen_gt_2_nx72 : std_logic;
SIGNAL modgen_gt_2_nx74 : std_logic;
SIGNAL modgen_gt_2_nx76 : std_logic;
SIGNAL modgen_gt_2_nx78 : std_logic;
SIGNAL modgen_gt_2_nx80 : std_logic;
SIGNAL modgen_gt_2_nx82 : std_logic;
SIGNAL modgen_gt_2_nx84 : std_logic;
SIGNAL modgen_gt_2_nx86 : std_logic;
SIGNAL hc_cps_tc : std_logic;
SIGNAL hc_threshold_a15_a_acombout : std_logic;
SIGNAL nx1138_a13 : std_logic;
SIGNAL hc_cnt_cen_a10 : std_logic;
SIGNAL ix546_a24 : std_logic;
SIGNAL nx1159_a13 : std_logic;
SIGNAL ix568_a2 : std_logic;
SIGNAL hc_cnt_clr_a10 : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL hc_threshold_a14_a_acombout : std_logic;
SIGNAL hc_threshold_a13_a_acombout : std_logic;
SIGNAL hc_threshold_a12_a_acombout : std_logic;
SIGNAL hc_threshold_a11_a_acombout : std_logic;
SIGNAL hc_threshold_a10_a_acombout : std_logic;
SIGNAL hc_threshold_a9_a_acombout : std_logic;
SIGNAL hc_threshold_a8_a_acombout : std_logic;
SIGNAL hc_threshold_a7_a_acombout : std_logic;
SIGNAL hc_threshold_a6_a_acombout : std_logic;
SIGNAL hc_threshold_a5_a_acombout : std_logic;
SIGNAL hc_threshold_a4_a_acombout : std_logic;
SIGNAL hc_threshold_a3_a_acombout : std_logic;
SIGNAL hc_threshold_a2_a_acombout : std_logic;
SIGNAL hc_threshold_a1_a_acombout : std_logic;
SIGNAL hc_threshold_a0_a_acombout : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL modgen_gt_3_nx38 : std_logic;
SIGNAL modgen_gt_3_nx40 : std_logic;
SIGNAL modgen_gt_3_nx42 : std_logic;
SIGNAL modgen_gt_3_nx44 : std_logic;
SIGNAL modgen_gt_3_nx46 : std_logic;
SIGNAL modgen_gt_3_nx48 : std_logic;
SIGNAL modgen_gt_3_nx50 : std_logic;
SIGNAL modgen_gt_3_nx52 : std_logic;
SIGNAL modgen_gt_3_nx54 : std_logic;
SIGNAL modgen_gt_3_nx56 : std_logic;
SIGNAL modgen_gt_3_nx58 : std_logic;
SIGNAL modgen_gt_3_nx60 : std_logic;
SIGNAL modgen_gt_3_nx62 : std_logic;
SIGNAL modgen_gt_3_nx64 : std_logic;
SIGNAL modgen_gt_3_nx66 : std_logic;
SIGNAL hc_count_tc : std_logic;
SIGNAL nx1145_a11 : std_logic;
SIGNAL Hi_Cnt_dup0 : std_logic;
SIGNAL rtl_a494 : std_logic;
SIGNAL OUT_CPS_aCPS_STATE_a0_a : std_logic;
SIGNAL OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91 : std_logic;
SIGNAL OUT_CPS_aCPS_STATE_a1_a : std_logic;
SIGNAL OUT_CPS_aCPS_STATE_a2_a : std_logic;
SIGNAL OUT_CPS_aCPS_STATE_a3_a : std_logic;
SIGNAL OUT_CPS_areduce_nor_17 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL OUT_CPS_areduce_nor_7 : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121 : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a0_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL OUT_CPS_acps_a0_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a1_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL OUT_CPS_acps_a1_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a10_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a9_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a8_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a7_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a6_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a5_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a4_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a3_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a2_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a : std_logic;
SIGNAL OUT_CPS_acps_a2_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a : std_logic;
SIGNAL OUT_CPS_acps_a3_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a : std_logic;
SIGNAL OUT_CPS_acps_a4_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a : std_logic;
SIGNAL OUT_CPS_acps_a5_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a : std_logic;
SIGNAL OUT_CPS_acps_a6_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a : std_logic;
SIGNAL OUT_CPS_acps_a7_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a : std_logic;
SIGNAL OUT_CPS_acps_a8_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a : std_logic;
SIGNAL OUT_CPS_acps_a9_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a : std_logic;
SIGNAL OUT_CPS_acps_a10_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a11_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a : std_logic;
SIGNAL OUT_CPS_acps_a11_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a12_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a : std_logic;
SIGNAL OUT_CPS_acps_a12_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a13_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a : std_logic;
SIGNAL OUT_CPS_acps_a13_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a14_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a : std_logic;
SIGNAL OUT_CPS_acps_a14_a_areg0 : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL OUT_CPS_aCPS_RAM_asram_aq_a15_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a : std_logic;
SIGNAL OUT_CPS_acps_a15_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a : std_logic;
SIGNAL OUT_CPS_acps_a16_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a : std_logic;
SIGNAL OUT_CPS_acps_a17_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a : std_logic;
SIGNAL OUT_CPS_acps_a18_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a : std_logic;
SIGNAL OUT_CPS_acps_a19_a_areg0 : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acout_a19_a : std_logic;
SIGNAL OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a : std_logic;
SIGNAL OUT_CPS_acps_a20_a_areg0 : std_logic;
SIGNAL Preset_Out_dup0 : std_logic;
SIGNAL NOT_nx2931_a10 : std_logic;
SIGNAL NOT_nx2928_a10 : std_logic;
SIGNAL NOT_nx2929_a10 : std_logic;
SIGNAL NOT_nx2930_a10 : std_logic;

BEGIN

ww_CLK20 <= CLK20;
ww_GMode <= GMode;
ww_hc_rate <= hc_rate;
ww_hc_threshold <= hc_threshold;
ww_IFDISC <= IFDISC;
ww_IMR <= IMR;
ww_LS_Abort <= LS_Abort;
ww_PA_Reset <= PA_Reset;
ww_PEAK_DONE <= PEAK_DONE;
ww_PRESET <= PRESET;
ww_PRESET_MODE <= PRESET_MODE;
ww_ROI_Inc <= ROI_Inc;
ww_Time_Clr <= Time_Clr;
ww_Time_Start <= Time_Start;
ww_Time_Stop <= Time_Stop;
CPS <= ww_CPS;
CTIME <= ww_CTIME;
Det_Sat <= ww_Det_Sat;
Hi_Cnt <= ww_Hi_Cnt;
LTIME <= ww_LTIME;
ms100_tc <= ww_ms100_tc;
NPS <= ww_NPS;
PAMR <= ww_PAMR;
Preset_Out <= ww_Preset_Out;
Time_Enable <= ww_Time_Enable;

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a & PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a & 
PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a & PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & 
PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a);
NOT_nx2931_a10 <= NOT nx2931_a10;
NOT_nx2928_a10 <= NOT nx2928_a10;
NOT_nx2929_a10 <= NOT nx2929_a10;
NOT_nx2930_a10 <= NOT nx2930_a10;

CLK20_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

ms1_cnt_ix48 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_0 = DFFE(!nx1134_a12 & !ms1_cnt_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx6 = CARRY(ms1_cnt_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_0,
	cout => ms1_cnt_nx6);

ms1_cnt_ix45 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_1 = DFFE(!nx1134_a12 & ms1_cnt_1 $ ms1_cnt_nx6, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx12 = CARRY(!ms1_cnt_nx6 # !ms1_cnt_1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_1,
	cin => ms1_cnt_nx6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_1,
	cout => ms1_cnt_nx12);

ms1_cnt_ix42 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_2 = DFFE(!nx1134_a12 & ms1_cnt_2 $ !ms1_cnt_nx12, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx18 = CARRY(ms1_cnt_2 & !ms1_cnt_nx12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_2,
	cin => ms1_cnt_nx12,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_2,
	cout => ms1_cnt_nx18);

ms1_cnt_ix39 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_3 = DFFE(!nx1134_a12 & ms1_cnt_3 $ ms1_cnt_nx18, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx24 = CARRY(!ms1_cnt_nx18 # !ms1_cnt_3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_3,
	cin => ms1_cnt_nx18,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_3,
	cout => ms1_cnt_nx24);

ms1_cnt_ix36 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_4 = DFFE(!nx1134_a12 & ms1_cnt_4 $ !ms1_cnt_nx24, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx30 = CARRY(ms1_cnt_4 & !ms1_cnt_nx24)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_4,
	cin => ms1_cnt_nx24,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_4,
	cout => ms1_cnt_nx30);

ms1_cnt_ix33 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_5 = DFFE(!nx1134_a12 & ms1_cnt_5 $ ms1_cnt_nx30, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx36 = CARRY(!ms1_cnt_nx30 # !ms1_cnt_5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_5,
	cin => ms1_cnt_nx30,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_5,
	cout => ms1_cnt_nx36);

ms1_cnt_ix30 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_6 = DFFE(!nx1134_a12 & ms1_cnt_6 $ !ms1_cnt_nx36, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx42 = CARRY(ms1_cnt_6 & !ms1_cnt_nx36)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_6,
	cin => ms1_cnt_nx36,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_6,
	cout => ms1_cnt_nx42);

ms1_cnt_ix27 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_7 = DFFE(!nx1134_a12 & ms1_cnt_7 $ ms1_cnt_nx42, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx48 = CARRY(!ms1_cnt_nx42 # !ms1_cnt_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_7,
	cin => ms1_cnt_nx42,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_7,
	cout => ms1_cnt_nx48);

ms1_cnt_ix24 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_8 = DFFE(!nx1134_a12 & ms1_cnt_8 $ !ms1_cnt_nx48, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx58 = CARRY(ms1_cnt_8 & !ms1_cnt_nx48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_8,
	cin => ms1_cnt_nx48,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_8,
	cout => ms1_cnt_nx58);

ms1_cnt_ix21 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_9 = DFFE(!nx1134_a12 & ms1_cnt_9 $ ms1_cnt_nx58, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx62 = CARRY(!ms1_cnt_nx58 # !ms1_cnt_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_9,
	cin => ms1_cnt_nx58,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_9,
	cout => ms1_cnt_nx62);

ms1_cnt_ix18 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_10 = DFFE(!nx1134_a12 & ms1_cnt_10 $ !ms1_cnt_nx62, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx66 = CARRY(ms1_cnt_10 & !ms1_cnt_nx62)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_10,
	cin => ms1_cnt_nx62,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_10,
	cout => ms1_cnt_nx66);

ms1_cnt_ix15 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_11 = DFFE(!nx1134_a12 & ms1_cnt_11 $ ms1_cnt_nx66, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx70 = CARRY(!ms1_cnt_nx66 # !ms1_cnt_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_11,
	cin => ms1_cnt_nx66,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_11,
	cout => ms1_cnt_nx70);

ms1_cnt_ix12 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_12 = DFFE(!nx1134_a12 & ms1_cnt_12 $ !ms1_cnt_nx70, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx74 = CARRY(ms1_cnt_12 & !ms1_cnt_nx70)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_12,
	cin => ms1_cnt_nx70,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_12,
	cout => ms1_cnt_nx74);

ms1_cnt_ix9 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_13 = DFFE(!nx1134_a12 & ms1_cnt_13 $ ms1_cnt_nx74, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ms1_cnt_nx78 = CARRY(!ms1_cnt_nx74 # !ms1_cnt_13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_13,
	cin => ms1_cnt_nx74,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_13,
	cout => ms1_cnt_nx78);

ms1_cnt_ix6 : apex20ke_lcell 
-- Equation(s):
-- ms1_cnt_14 = DFFE(!nx1134_a12 & ms1_cnt_nx78 $ !ms1_cnt_14, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => ms1_cnt_14,
	cin => ms1_cnt_nx78,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms1_cnt_14);

ix546_a24_I : apex20ke_lcell 
-- Equation(s):
-- ix546_a24 = !ms1_cnt_8 & ms1_cnt_11 & ms1_cnt_9 & ms1_cnt_10
-- ix546_a44 = !ms1_cnt_8 & ms1_cnt_11 & ms1_cnt_9 & ms1_cnt_10

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_8,
	datab => ms1_cnt_11,
	datac => ms1_cnt_9,
	datad => ms1_cnt_10,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix546_a24,
	cascout => ix546_a44);

ix651_a30_I : apex20ke_lcell 
-- Equation(s):
-- ix651_a30 = (!ms1_cnt_12 & !ms1_cnt_13 & ms1_cnt_14) & CASCADE(ix546_a44)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ms1_cnt_12,
	datac => ms1_cnt_13,
	datad => ms1_cnt_14,
	cascin => ix546_a44,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix651_a30);

ix570_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix570_a3 = !ms1_cnt_5 & ms1_cnt_4 & !ms1_cnt_7 & !ms1_cnt_6

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_5,
	datab => ms1_cnt_4,
	datac => ms1_cnt_7,
	datad => ms1_cnt_6,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix570_a3);

nx1159_a13_I : apex20ke_lcell 
-- Equation(s):
-- nx1159_a13 = (ms1_cnt_1 & ms1_cnt_0 & ms1_cnt_3 & ms1_cnt_2) & CASCADE(ix570_a3)
-- nx1159_a14 = (ms1_cnt_1 & ms1_cnt_0 & ms1_cnt_3 & ms1_cnt_2) & CASCADE(ix570_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_1,
	datab => ms1_cnt_0,
	datac => ms1_cnt_3,
	datad => ms1_cnt_2,
	cascin => ix570_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1159_a13,
	cascout => nx1159_a14);

nx1134_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx1134_a12 = (ix651_a30) & CASCADE(nx1159_a14)
-- nx1134_a14 = (ix651_a30) & CASCADE(nx1159_a14)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix651_a30,
	cascin => nx1159_a14,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1134_a12,
	cascout => nx1134_a14);

ms100_cnt_ix10 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_5 = DFFE(!nx1138_a12 & ms100_cnt_5 $ ms100_cnt_nx31, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx35 = CARRY(!ms100_cnt_nx31 # !ms100_cnt_5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms100_cnt_5,
	cin => ms100_cnt_nx31,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_5,
	cout => ms100_cnt_nx35);

ms100_cnt_ix7 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_6 = DFFE(!nx1138_a12 & ms100_cnt_6 $ !ms100_cnt_nx35, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms100_cnt_6,
	cin => ms100_cnt_nx35,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_6);

ms100_cnt_ix25 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_0 = DFFE(!nx1138_a12 & !ms100_cnt_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx7 = CARRY(ms100_cnt_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_0,
	cout => ms100_cnt_nx7);

nx1147_a2_I : apex20ke_lcell 
-- Equation(s):
-- nx1147_a2 = !ms100_cnt_2 & ms100_cnt_0 & ms100_cnt_1 & !ms100_cnt_3
-- nx1147_a30 = !ms100_cnt_2 & ms100_cnt_0 & ms100_cnt_1 & !ms100_cnt_3

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ms100_cnt_2,
	datab => ms100_cnt_0,
	datac => ms100_cnt_1,
	datad => ms100_cnt_3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1147_a2,
	cascout => nx1147_a30);

nx1138_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx1138_a12 = (ms100_cnt_5 & ms100_cnt_6 & !ms100_cnt_4) & CASCADE(nx1147_a30)
-- nx1138_a13 = (ms100_cnt_5 & ms100_cnt_6 & !ms100_cnt_4) & CASCADE(nx1147_a30)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ms100_cnt_5,
	datac => ms100_cnt_6,
	datad => ms100_cnt_4,
	cascin => nx1147_a30,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1138_a12,
	cascout => nx1138_a13);

ms100_cnt_ix22 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_1 = DFFE(!nx1138_a12 & ms100_cnt_1 $ ms100_cnt_nx7, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx13 = CARRY(!ms100_cnt_nx7 # !ms100_cnt_1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms100_cnt_1,
	cin => ms100_cnt_nx7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_1,
	cout => ms100_cnt_nx13);

ms100_cnt_ix19 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_2 = DFFE(!nx1138_a12 & ms100_cnt_2 $ !ms100_cnt_nx13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx19 = CARRY(ms100_cnt_2 & !ms100_cnt_nx13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ms100_cnt_2,
	cin => ms100_cnt_nx13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_2,
	cout => ms100_cnt_nx19);

ms100_cnt_ix16 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_3 = DFFE(!nx1138_a12 & ms100_cnt_3 $ ms100_cnt_nx19, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx25 = CARRY(!ms100_cnt_nx19 # !ms100_cnt_3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms100_cnt_3,
	cin => ms100_cnt_nx19,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_3,
	cout => ms100_cnt_nx25);

ms100_cnt_ix13 : apex20ke_lcell 
-- Equation(s):
-- ms100_cnt_4 = DFFE(!nx1138_a12 & ms100_cnt_4 $ !ms100_cnt_nx25, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1134_a12)
-- ms100_cnt_nx31 = CARRY(ms100_cnt_4 & !ms100_cnt_nx25)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms100_cnt_4,
	cin => ms100_cnt_nx25,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1134_a12,
	sclr => nx1138_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_cnt_4,
	cout => ms100_cnt_nx31);

nx1145_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx1145_a10 = (ms100_cnt_5 & !ms100_cnt_4 & ms100_cnt_6 & nx1147_a2) & CASCADE(nx1134_a14)
-- nx1145_a11 = (ms100_cnt_5 & !ms100_cnt_4 & ms100_cnt_6 & nx1147_a2) & CASCADE(nx1134_a14)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ms100_cnt_5,
	datab => ms100_cnt_4,
	datac => ms100_cnt_6,
	datad => nx1147_a2,
	cascin => nx1134_a14,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1145_a10,
	cascout => nx1145_a11);

IN_CPS_ams100_tc_vec_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_ams100_tc_vec_a0_a = DFFE(nx1145_a10, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => nx1145_a10,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_ams100_tc_vec_a0_a);

OUT_CPS_ams100_tc_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_ams100_tc_vec_a1_a = DFFE(IN_CPS_ams100_tc_vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => IN_CPS_ams100_tc_vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_ams100_tc_vec_a1_a);

IN_CPS_ams100tc_Del_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_ams100tc_Del = DFFE(OUT_CPS_ams100_tc_vec_a1_a # !IN_CPS_ams100_tc_vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => IN_CPS_ams100_tc_vec_a0_a,
	datad => OUT_CPS_ams100_tc_vec_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_ams100tc_Del);

IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91 = !IN_CPS_aCPS_STATE_a2_a & (IN_CPS_aCPS_STATE_a1_a # !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4545",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a2_a,
	datab => IN_CPS_aCPS_STATE_a1_a,
	datac => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91);

IN_CPS_aCPS_STATE_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aCPS_STATE_a1_a = DFFE(IN_CPS_aCPS_STATE_a3_a & (IN_CPS_aCPS_STATE_a1_a $ !IN_CPS_aCPS_STATE_a0_a) # !IN_CPS_aCPS_STATE_a3_a & IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91 & (IN_CPS_aCPS_STATE_a1_a $ !IN_CPS_aCPS_STATE_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C382",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a3_a,
	datab => IN_CPS_aCPS_STATE_a1_a,
	datac => IN_CPS_aCPS_STATE_a0_a,
	datad => IN_CPS_aMux_90_rtl_4_a_a00009_a6_a91,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aCPS_STATE_a1_a);

IN_CPS_aCPS_STATE_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aCPS_STATE_a2_a = DFFE(IN_CPS_aCPS_STATE_a3_a & IN_CPS_aCPS_STATE_a2_a & (IN_CPS_aCPS_STATE_a0_a # !IN_CPS_aCPS_STATE_a1_a) # !IN_CPS_aCPS_STATE_a3_a & !IN_CPS_aCPS_STATE_a0_a & !IN_CPS_aCPS_STATE_a2_a & IN_CPS_aCPS_STATE_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "81C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a0_a,
	datab => IN_CPS_aCPS_STATE_a3_a,
	datac => IN_CPS_aCPS_STATE_a2_a,
	datad => IN_CPS_aCPS_STATE_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aCPS_STATE_a2_a);

rtl_a491_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a491 = IN_CPS_aCPS_STATE_a3_a & (IN_CPS_aCPS_STATE_a2_a # !IN_CPS_aCPS_STATE_a1_a) # !IN_CPS_aCPS_STATE_a3_a & !IN_CPS_aCPS_STATE_a2_a & (IN_CPS_aCPS_STATE_a1_a # !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9A9B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a3_a,
	datab => IN_CPS_aCPS_STATE_a2_a,
	datac => IN_CPS_aCPS_STATE_a1_a,
	datad => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a491);

IN_CPS_aCPS_STATE_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aCPS_STATE_a0_a = DFFE(rtl_a491 & !IN_CPS_aCPS_STATE_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => rtl_a491,
	datad => IN_CPS_aCPS_STATE_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aCPS_STATE_a0_a);

IN_CPS_aCPS_STATE_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aCPS_STATE_a3_a = DFFE(IN_CPS_aCPS_STATE_a3_a $ (!IN_CPS_aCPS_STATE_a0_a & !IN_CPS_aCPS_STATE_a2_a & IN_CPS_aCPS_STATE_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C9CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a0_a,
	datab => IN_CPS_aCPS_STATE_a3_a,
	datac => IN_CPS_aCPS_STATE_a2_a,
	datad => IN_CPS_aCPS_STATE_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aCPS_STATE_a3_a);

IN_CPS_areduce_nor_17_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_areduce_nor_17 = IN_CPS_aCPS_STATE_a3_a # IN_CPS_aCPS_STATE_a2_a # IN_CPS_aCPS_STATE_a1_a # IN_CPS_aCPS_STATE_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a3_a,
	datab => IN_CPS_aCPS_STATE_a2_a,
	datac => IN_CPS_aCPS_STATE_a1_a,
	datad => IN_CPS_aCPS_STATE_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_areduce_nor_17);

IFDISC_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IFDISC,
	combout => IFDISC_acombout);

IN_CPS_ainc_cps_vec_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_ainc_cps_vec_a0_a = DFFE(IFDISC_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => IFDISC_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_ainc_cps_vec_a0_a);

IN_CPS_ainc_cps_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_ainc_cps_vec_a1_a = DFFE(IN_CPS_ainc_cps_vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => IN_CPS_ainc_cps_vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_ainc_cps_vec_a1_a);

IN_CPS_areduce_nor_6_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_areduce_nor_6 = IN_CPS_ainc_cps_vec_a0_a & !IN_CPS_ainc_cps_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => IN_CPS_ainc_cps_vec_a0_a,
	datad => IN_CPS_ainc_cps_vec_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_areduce_nor_6);

IN_CPS_areduce_nor_4_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_areduce_nor_4 = IN_CPS_ams100_tc_vec_a0_a & !OUT_CPS_ams100_tc_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => IN_CPS_ams100_tc_vec_a0_a,
	datad => OUT_CPS_ams100_tc_vec_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_areduce_nor_4);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_areduce_nor_6 $ IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

IN_CPS_areduce_nor_7_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_areduce_nor_7 = !IN_CPS_aCPS_STATE_a0_a & IN_CPS_aCPS_STATE_a3_a & !IN_CPS_aCPS_STATE_a2_a & IN_CPS_aCPS_STATE_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a0_a,
	datab => IN_CPS_aCPS_STATE_a3_a,
	datac => IN_CPS_aCPS_STATE_a2_a,
	datad => IN_CPS_aCPS_STATE_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_areduce_nor_7);

IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a = DFFE(IN_CPS_areduce_nor_7 $ IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	cout => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a = DFFE(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ (IN_CPS_areduce_nor_7 & IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_7,
	datab => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cin => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cout => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a = DFFE(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ (IN_CPS_areduce_nor_7 & !IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_7,
	datab => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cin => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cout => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a = DFFE(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a $ (IN_CPS_areduce_nor_7 & IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_7,
	datad => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	cin => IN_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a);

IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a = IN_CPS_aCPS_STATE_a0_a $ !IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a
-- IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a = CARRY(IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a # !IN_CPS_aCPS_STATE_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "99DD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a0_a,
	datab => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	cout => IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a);

IN_CPS_aCPS_RAM_asram_aq_a0_a_a121_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aCPS_RAM_asram_aq_a0_a_a121 = !IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aCPS_RAM_asram_aq_a0_a_a121);

IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a = IN_CPS_aCPS_STATE_a1_a $ IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a
-- IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a = CARRY(IN_CPS_aCPS_STATE_a1_a & !IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & !IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !IN_CPS_aCPS_STATE_a1_a & (!IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a1_a,
	datab => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cin => IN_CPS_aadd_13_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a,
	cout => IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a);

IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a = IN_CPS_aCPS_STATE_a2_a $ IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a
-- IN_CPS_aadd_13_aadder_aresult_node_acout_a3_a = CARRY(IN_CPS_aCPS_STATE_a2_a & IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a # !IN_CPS_aCPS_STATE_a2_a & (IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a # !IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a2_a,
	datab => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cin => IN_CPS_aadd_13_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a,
	cout => IN_CPS_aadd_13_aadder_aresult_node_acout_a3_a);

IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a = IN_CPS_aCPS_STATE_a3_a $ IN_CPS_aadd_13_aadder_aresult_node_acout_a3_a $ IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_aCPS_STATE_a3_a,
	datad => IN_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	cin => IN_CPS_aadd_13_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a0_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a = IN_CPS_acps_a0_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a0_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a = CARRY(IN_CPS_acps_a0_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a0_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a);

IN_CPS_acps_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a0_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a0_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_acps_a0_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a0_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a1_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a = IN_CPS_acps_a1_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a1_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a = CARRY(IN_CPS_acps_a1_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a1_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !IN_CPS_acps_a1_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !IN_CPS_aCPS_RAM_asram_aq_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a1_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a1_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a);

IN_CPS_acps_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a1_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a1_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_acps_a1_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a1_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a10_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a9_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a8_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a7_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a6_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a5_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a4_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a3_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a2_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a = IN_CPS_acps_a2_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a2_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a = CARRY(IN_CPS_acps_a2_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a2_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a) # !IN_CPS_acps_a2_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a2_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a2_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a2_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a);

IN_CPS_acps_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a2_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_acps_a2_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a2_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a = IN_CPS_acps_a3_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a3_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a = CARRY(IN_CPS_acps_a3_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a3_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !IN_CPS_acps_a3_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !IN_CPS_aCPS_RAM_asram_aq_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a3_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a3_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a);

IN_CPS_acps_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a3_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a3_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_acps_a3_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a3_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a = IN_CPS_acps_a4_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a4_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a = CARRY(IN_CPS_acps_a4_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a4_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a) # !IN_CPS_acps_a4_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a4_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a4_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a4_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a);

IN_CPS_acps_a4_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a4_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a4_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_acps_a4_a_areg0,
	datac => IN_CPS_areduce_nor_17,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a4_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a = IN_CPS_acps_a5_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a5_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a = CARRY(IN_CPS_acps_a5_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a5_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !IN_CPS_acps_a5_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !IN_CPS_aCPS_RAM_asram_aq_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a5_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a5_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a);

IN_CPS_acps_a5_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a5_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a5_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a5_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a5_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a = IN_CPS_acps_a6_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a6_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a = CARRY(IN_CPS_acps_a6_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a6_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a) # !IN_CPS_acps_a6_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a6_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a6_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a6_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a);

IN_CPS_acps_a6_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a6_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a6_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a6_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a6_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a = IN_CPS_acps_a7_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a7_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a = CARRY(IN_CPS_acps_a7_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a7_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !IN_CPS_acps_a7_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !IN_CPS_aCPS_RAM_asram_aq_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a7_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a7_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a);

IN_CPS_acps_a7_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a7_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a7_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a7_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a7_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a = IN_CPS_acps_a8_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a8_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a = CARRY(IN_CPS_acps_a8_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a8_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a) # !IN_CPS_acps_a8_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a8_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a8_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a8_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a);

IN_CPS_acps_a8_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a8_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a8_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a8_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a8_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a = IN_CPS_acps_a9_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a9_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a = CARRY(IN_CPS_acps_a9_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a9_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !IN_CPS_acps_a9_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !IN_CPS_aCPS_RAM_asram_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a9_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a9_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a);

IN_CPS_acps_a9_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a9_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a9_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a9_a_areg0,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	datad => IN_CPS_areduce_nor_17,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a9_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a = IN_CPS_acps_a10_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a10_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a = CARRY(IN_CPS_acps_a10_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a10_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a) # !IN_CPS_acps_a10_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a10_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a10_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a10_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a);

IN_CPS_acps_a10_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a10_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a10_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a10_a_areg0,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_areduce_nor_17,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a10_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a11_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a = IN_CPS_acps_a11_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a11_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a = CARRY(IN_CPS_acps_a11_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a11_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !IN_CPS_acps_a11_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !IN_CPS_aCPS_RAM_asram_aq_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a11_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a11_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a);

IN_CPS_acps_a11_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a11_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a11_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a11_a_areg0,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	datad => IN_CPS_areduce_nor_17,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a11_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a12_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a = IN_CPS_acps_a12_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a12_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a = CARRY(IN_CPS_acps_a12_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a12_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a) # !IN_CPS_acps_a12_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a12_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a12_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a12_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a);

IN_CPS_acps_a12_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a12_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a12_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_acps_a12_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a12_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a13_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a = IN_CPS_acps_a13_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a13_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a = CARRY(IN_CPS_acps_a13_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a13_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !IN_CPS_acps_a13_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !IN_CPS_aCPS_RAM_asram_aq_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a13_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a13_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a);

IN_CPS_acps_a13_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a13_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a13_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a13_a_areg0,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_ams100tc_Del,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a13_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a $ (IN_CPS_areduce_nor_6 & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a & !IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cout => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a14_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a = IN_CPS_acps_a14_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a14_a $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a = CARRY(IN_CPS_acps_a14_a_areg0 & (IN_CPS_aCPS_RAM_asram_aq_a14_a # !IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a) # !IN_CPS_acps_a14_a_areg0 & IN_CPS_aCPS_RAM_asram_aq_a14_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a14_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a14_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a);

IN_CPS_acps_a14_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a14_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a14_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a14_a_areg0,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_ams100tc_Del,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a14_a_areg0);

IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a $ (IN_CPS_areduce_nor_6 & IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datad => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	cin => IN_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a);

IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:IN_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress,
	raddr => ww_IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => IN_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => IN_CPS_aCPS_RAM_asram_aq_a15_a);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a = IN_CPS_acps_a15_a_areg0 $ IN_CPS_aCPS_RAM_asram_aq_a15_a $ IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a = CARRY(IN_CPS_acps_a15_a_areg0 & !IN_CPS_aCPS_RAM_asram_aq_a15_a & !IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !IN_CPS_acps_a15_a_areg0 & (!IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !IN_CPS_aCPS_RAM_asram_aq_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a15_a_areg0,
	datab => IN_CPS_aCPS_RAM_asram_aq_a15_a,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a);

IN_CPS_acps_a15_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a15_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a15_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a15_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a15_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a = IN_CPS_acps_a16_a_areg0 $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a = CARRY(IN_CPS_acps_a16_a_areg0 & !IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a16_a_areg0,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a);

IN_CPS_acps_a16_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a16_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a16_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a16_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a16_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a = IN_CPS_acps_a17_a_areg0 $ IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a = CARRY(!IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a # !IN_CPS_acps_a17_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a17_a_areg0,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a);

IN_CPS_acps_a17_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a17_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a17_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a17_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a17_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a = IN_CPS_acps_a18_a_areg0 $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a = CARRY(IN_CPS_acps_a18_a_areg0 & !IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a18_a_areg0,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a);

IN_CPS_acps_a18_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a18_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a18_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => IN_CPS_areduce_nor_17,
	datac => IN_CPS_acps_a18_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a18_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a = IN_CPS_acps_a19_a_areg0 $ IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a
-- IN_CPS_aadd_60_aadder_aresult_node_acout_a19_a = CARRY(!IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a # !IN_CPS_acps_a19_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a19_a_areg0,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	cout => IN_CPS_aadd_60_aadder_aresult_node_acout_a19_a);

IN_CPS_acps_a19_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a19_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a # !IN_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & IN_CPS_acps_a19_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => IN_CPS_acps_a19_a_areg0,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a19_a_areg0);

IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a = IN_CPS_acps_a20_a_areg0 $ !IN_CPS_aadd_60_aadder_aresult_node_acout_a19_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a20_a_areg0,
	cin => IN_CPS_aadd_60_aadder_aresult_node_acout_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a);

IN_CPS_acps_a20_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- IN_CPS_acps_a20_a_areg0 = DFFE(IN_CPS_areduce_nor_17 & IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a # !IN_CPS_areduce_nor_17 & IN_CPS_acps_a20_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_17,
	datab => IN_CPS_acps_a20_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => IN_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => IN_CPS_acps_a20_a_areg0);

PRESET_MODE_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET_MODE(2),
	combout => PRESET_MODE_a2_a_acombout);

PRESET_MODE_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET_MODE(0),
	combout => PRESET_MODE_a0_a_acombout);

PRESET_MODE_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET_MODE(1),
	combout => PRESET_MODE_a1_a_acombout);

ps1_cnt_ix48 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_0 = DFFE(nx2928_a10 & !ps1_cnt_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx6 = CARRY(ps1_cnt_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_0,
	cout => ps1_cnt_nx6);

ix576_a13_I : apex20ke_lcell 
-- Equation(s):
-- ix576_a13 = ps1_cnt_1 & ps1_cnt_0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_1,
	datac => ps1_cnt_0,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix576_a13);

ps1_cnt_ix45 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_1 = DFFE(nx2928_a10 & ps1_cnt_1 $ ps1_cnt_nx6, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx12 = CARRY(!ps1_cnt_nx6 # !ps1_cnt_1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_1,
	cin => ps1_cnt_nx6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_1,
	cout => ps1_cnt_nx12);

ps1_cnt_ix42 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_2 = DFFE(nx2928_a10 & ps1_cnt_2 $ !ps1_cnt_nx12, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx18 = CARRY(ps1_cnt_2 & !ps1_cnt_nx12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_2,
	cin => ps1_cnt_nx12,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_2,
	cout => ps1_cnt_nx18);

ps1_cnt_ix39 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_3 = DFFE(nx2928_a10 & ps1_cnt_3 $ ps1_cnt_nx18, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx24 = CARRY(!ps1_cnt_nx18 # !ps1_cnt_3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_3,
	cin => ps1_cnt_nx18,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_3,
	cout => ps1_cnt_nx24);

ps1_cnt_ix36 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_4 = DFFE(nx2928_a10 & ps1_cnt_4 $ !ps1_cnt_nx24, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx30 = CARRY(ps1_cnt_4 & !ps1_cnt_nx24)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_4,
	cin => ps1_cnt_nx24,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_4,
	cout => ps1_cnt_nx30);

ps1_cnt_ix33 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_5 = DFFE(nx2928_a10 & ps1_cnt_5 $ ps1_cnt_nx30, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx36 = CARRY(!ps1_cnt_nx30 # !ps1_cnt_5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_5,
	cin => ps1_cnt_nx30,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_5,
	cout => ps1_cnt_nx36);

ps1_cnt_ix30 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_6 = DFFE(nx2928_a10 & ps1_cnt_6 $ !ps1_cnt_nx36, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx42 = CARRY(ps1_cnt_6 & !ps1_cnt_nx36)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_6,
	cin => ps1_cnt_nx36,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_6,
	cout => ps1_cnt_nx42);

ix571_a30_I : apex20ke_lcell 
-- Equation(s):
-- ix571_a30 = !ps1_cnt_5 & !ps1_cnt_6

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_5,
	datad => ps1_cnt_6,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix571_a30);

ix571_a43_I : apex20ke_lcell 
-- Equation(s):
-- ix571_a43 = ps1_cnt_2 & ix571_a30 & ps1_cnt_4 & ps1_cnt_3

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_2,
	datab => ix571_a30,
	datac => ps1_cnt_4,
	datad => ps1_cnt_3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix571_a43);

ix571_a48_I : apex20ke_lcell 
-- Equation(s):
-- ix571_a48 = (ix576_a13 & (PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a2_a_acombout)) & CASCADE(ix571_a43)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ix576_a13,
	cascin => ix571_a43,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix571_a48);

ps1_cnt_ix27 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_7 = DFFE(nx2928_a10 & ps1_cnt_7 $ ps1_cnt_nx42, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx48 = CARRY(!ps1_cnt_nx42 # !ps1_cnt_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_7,
	cin => ps1_cnt_nx42,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_7,
	cout => ps1_cnt_nx48);

ps1_cnt_ix24 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_8 = DFFE(nx2928_a10 & ps1_cnt_8 $ !ps1_cnt_nx48, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx58 = CARRY(ps1_cnt_8 & !ps1_cnt_nx48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_8,
	cin => ps1_cnt_nx48,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_8,
	cout => ps1_cnt_nx58);

ps1_cnt_ix21 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_9 = DFFE(nx2928_a10 & ps1_cnt_9 $ ps1_cnt_nx58, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx62 = CARRY(!ps1_cnt_nx58 # !ps1_cnt_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_9,
	cin => ps1_cnt_nx58,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_9,
	cout => ps1_cnt_nx62);

ps1_cnt_ix18 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_10 = DFFE(nx2928_a10 & ps1_cnt_10 $ !ps1_cnt_nx62, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx66 = CARRY(ps1_cnt_10 & !ps1_cnt_nx62)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_10,
	cin => ps1_cnt_nx62,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_10,
	cout => ps1_cnt_nx66);

ps1_cnt_ix15 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_11 = DFFE(nx2928_a10 & ps1_cnt_11 $ ps1_cnt_nx66, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx70 = CARRY(!ps1_cnt_nx66 # !ps1_cnt_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_11,
	cin => ps1_cnt_nx66,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_11,
	cout => ps1_cnt_nx70);

ps1_cnt_ix12 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_12 = DFFE(nx2928_a10 & ps1_cnt_12 $ !ps1_cnt_nx70, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx74 = CARRY(ps1_cnt_12 & !ps1_cnt_nx70)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_12,
	cin => ps1_cnt_nx70,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_12,
	cout => ps1_cnt_nx74);

ps1_cnt_ix9 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_13 = DFFE(nx2928_a10 & ps1_cnt_13 $ ps1_cnt_nx74, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ps1_cnt_nx78 = CARRY(!ps1_cnt_nx74 # !ps1_cnt_13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_13,
	cin => ps1_cnt_nx74,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_13,
	cout => ps1_cnt_nx78);

ps1_cnt_ix6 : apex20ke_lcell 
-- Equation(s):
-- ps1_cnt_14 = DFFE(nx2928_a10 & ps1_cnt_nx78 $ !ps1_cnt_14, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => ps1_cnt_14,
	cin => ps1_cnt_nx78,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_nx2928_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => ps1_cnt_14);

ix572_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix572_a3 = !ps1_cnt_13 & !ps1_cnt_12 & ps1_cnt_14 & ps1_cnt_11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_13,
	datab => ps1_cnt_12,
	datac => ps1_cnt_14,
	datad => ps1_cnt_11,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix572_a3);

ix643_a36_I : apex20ke_lcell 
-- Equation(s):
-- ix643_a36 = (!ps1_cnt_8 & ps1_cnt_10 & ps1_cnt_9 & !ps1_cnt_7) & CASCADE(ix572_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_8,
	datab => ps1_cnt_10,
	datac => ps1_cnt_9,
	datad => ps1_cnt_7,
	cascin => ix572_a3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix643_a36);

nx2936_a20_I : apex20ke_lcell 
-- Equation(s):
-- nx2936_a20 = (ix571_a48) & CASCADE(ix643_a36)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix571_a48,
	cascin => ix643_a36,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2936_a20);

nx1140_a1_I : apex20ke_lcell 
-- Equation(s):
-- nx1140_a1 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout
-- nx1140_a33 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1140_a1,
	cascout => nx1140_a33);

Time_Start_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Start,
	combout => Time_Start_acombout);

PA_Reset_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

reg_PA_Reset_Vec_0_a0 : apex20ke_lcell 
-- Equation(s):
-- PA_Reset_Vec_0 = DFFE(PA_Reset_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PA_Reset_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PA_Reset_Vec_0);

nx1149_a3_I : apex20ke_lcell 
-- Equation(s):
-- nx1149_a3 = nx1140_a1 & !ix553_a20 # !nx1140_a1 & !Time_Start_acombout
-- nx1149_a68 = nx1140_a1 & !ix553_a20 # !nx1140_a1 & !Time_Start_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "03CF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => nx1140_a1,
	datac => Time_Start_acombout,
	datad => ix553_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1149_a3,
	cascout => nx1149_a68);

Time_Stop_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Stop,
	combout => Time_Stop_acombout);

nx1130_a2_I : apex20ke_lcell 
-- Equation(s):
-- nx1130_a2 = PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !Time_Start_acombout
-- nx1130_a35 = PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !Time_Start_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "80FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => Time_Start_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1130_a2,
	cascout => nx1130_a35);

reg_PA_Reset_Vec_1_a0 : apex20ke_lcell 
-- Equation(s):
-- PA_Reset_Vec_1 = DFFE(PA_Reset_Vec_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => PA_Reset_Vec_0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PA_Reset_Vec_1);

nx803_a21_I : apex20ke_lcell 
-- Equation(s):
-- nx803_a21 = (!PA_Reset_Vec_0 & time_start_latch & PA_Reset_Vec_1) & CASCADE(nx1140_a33)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PA_Reset_Vec_0,
	datac => time_start_latch,
	datad => PA_Reset_Vec_1,
	cascin => nx1140_a33,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx803_a21);

LS_Abort_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_LS_Abort,
	combout => LS_Abort_acombout);

nx1142_a1_I : apex20ke_lcell 
-- Equation(s):
-- nx1142_a1 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout
-- nx1142_a26 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1142_a1,
	cascout => nx1142_a26);

PRESET_ibuf_31 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(31),
	combout => PRESET_a31_a_acombout);

PEAK_DONE_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PEAK_DONE,
	combout => PEAK_DONE_acombout);

OUT_CPS_ainc_cps_vec_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_ainc_cps_vec_a0_a = DFFE(PEAK_DONE_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PEAK_DONE_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_ainc_cps_vec_a0_a);

OUT_CPS_ainc_cps_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_ainc_cps_vec_a1_a = DFFE(OUT_CPS_ainc_cps_vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => OUT_CPS_ainc_cps_vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_ainc_cps_vec_a1_a);

nx1151_a0_I : apex20ke_lcell 
-- Equation(s):
-- nx1151_a0 = OUT_CPS_ainc_cps_vec_a0_a & !OUT_CPS_ainc_cps_vec_a1_a & (IN_CPS_ainc_cps_vec_a1_a # !IN_CPS_ainc_cps_vec_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C04",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ainc_cps_vec_a0_a,
	datab => OUT_CPS_ainc_cps_vec_a0_a,
	datac => OUT_CPS_ainc_cps_vec_a1_a,
	datad => IN_CPS_ainc_cps_vec_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => nx1151_a0);

a_2_a20_I : apex20ke_lcell 
-- Equation(s):
-- a_2_a20 = (!a_1_a18 & !nx981_a18) & CASCADE(nx1151_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1111",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_1_a18,
	datab => nx981_a18,
	cascin => nx1151_a0,
	devclrn => devclrn,
	devpor => devpor,
	combout => a_2_a20);

ix604_a1_I : apex20ke_lcell 
-- Equation(s):
-- ix604_a1 = !IN_CPS_ainc_cps_vec_a1_a & !lt_inc_en & IN_CPS_ainc_cps_vec_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => IN_CPS_ainc_cps_vec_a1_a,
	datac => lt_inc_en,
	datad => IN_CPS_ainc_cps_vec_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix604_a1);

a_3_a18_I : apex20ke_lcell 
-- Equation(s):
-- a_3_a18 = (OUT_CPS_ainc_cps_vec_a0_a & !OUT_CPS_ainc_cps_vec_a1_a & !nx981_a18 & !a_1_a18) & CASCADE(ix604_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_ainc_cps_vec_a0_a,
	datab => OUT_CPS_ainc_cps_vec_a1_a,
	datac => nx981_a18,
	datad => a_1_a18,
	cascin => ix604_a1,
	devclrn => devclrn,
	devpor => devpor,
	combout => a_3_a18);

OUT_CPS_areduce_nor_6_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_areduce_nor_6 = OUT_CPS_ainc_cps_vec_a0_a & !OUT_CPS_ainc_cps_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => OUT_CPS_ainc_cps_vec_a0_a,
	datad => OUT_CPS_ainc_cps_vec_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_areduce_nor_6);

nx1155_a0_I : apex20ke_lcell 
-- Equation(s):
-- nx1155_a0 = !IN_CPS_ainc_cps_vec_a1_a & lt_inc_en & IN_CPS_ainc_cps_vec_a0_a
-- nx1155_a11 = !IN_CPS_ainc_cps_vec_a1_a & lt_inc_en & IN_CPS_ainc_cps_vec_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => IN_CPS_ainc_cps_vec_a1_a,
	datac => lt_inc_en,
	datad => IN_CPS_ainc_cps_vec_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1155_a0,
	cascout => nx1155_a11);

nx2018_a2_I : apex20ke_lcell 
-- Equation(s):
-- nx2018_a2 = a_1_a18 # nx981_a18 # OUT_CPS_areduce_nor_6 $ nx1155_a0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => a_1_a18,
	datab => OUT_CPS_areduce_nor_6,
	datac => nx981_a18,
	datad => nx1155_a0,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2018_a2);

reg_lt_cntr_0_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_0 = DFFE(!lt_cntr_0 & (a_3_a18 # a_1_dup_2234_a18 # a_2_a20), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_3_a18,
	datab => a_1_dup_2234_a18,
	datac => lt_cntr_0,
	datad => a_2_a20,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_0);

ix3616 : apex20ke_lcell 
-- Equation(s):
-- nx3615 = CARRY(lt_cntr_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "00AA",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_0,
	devclrn => devclrn,
	devpor => devpor,
	cout => nx3615);

ix2152_ix24 : apex20ke_lcell 
-- Equation(s):
-- b_3_dup_2260 = lt_cntr_1 $ a_1_dup_2234_a18 $ !nx3615
-- ix2152_nx27 = CARRY(lt_cntr_1 & a_1_dup_2234_a18 & !nx3615 # !lt_cntr_1 & (a_1_dup_2234_a18 # !nx3615))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_1,
	datab => a_1_dup_2234_a18,
	cin => nx3615,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3_dup_2260,
	cout => ix2152_nx27);

reg_lt_cntr_1_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_1 = DFFE(b_3_dup_2260 & (a_2_a20 # a_1_dup_2234_a18 # a_3_a18), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCC8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_2_a20,
	datab => b_3_dup_2260,
	datac => a_1_dup_2234_a18,
	datad => a_3_a18,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_1);

ix2152_ix28 : apex20ke_lcell 
-- Equation(s):
-- b_3_dup_2255 = lt_cntr_2 $ a_1_dup_2234_a18 $ ix2152_nx27
-- ix2152_nx31 = CARRY(lt_cntr_2 & (!ix2152_nx27 # !a_1_dup_2234_a18) # !lt_cntr_2 & !a_1_dup_2234_a18 & !ix2152_nx27)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_2,
	datab => a_1_dup_2234_a18,
	cin => ix2152_nx27,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3_dup_2255,
	cout => ix2152_nx31);

reg_lt_cntr_2_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_2 = DFFE(b_3_dup_2255 & (a_2_a20 # a_3_a18 # a_1_dup_2234_a18), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FE00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_2_a20,
	datab => a_3_a18,
	datac => a_1_dup_2234_a18,
	datad => b_3_dup_2255,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_2);

ix2152_ix32 : apex20ke_lcell 
-- Equation(s):
-- b_3_dup_2250 = lt_cntr_3 $ a_1_dup_2234_a18 $ !ix2152_nx31
-- ix2152_nx35 = CARRY(lt_cntr_3 & a_1_dup_2234_a18 & !ix2152_nx31 # !lt_cntr_3 & (a_1_dup_2234_a18 # !ix2152_nx31))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_3,
	datab => a_1_dup_2234_a18,
	cin => ix2152_nx31,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3_dup_2250,
	cout => ix2152_nx35);

reg_lt_cntr_3_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_3 = DFFE(b_3_dup_2250 & (a_2_a20 # a_3_a18 # a_1_dup_2234_a18), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FE00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_2_a20,
	datab => a_3_a18,
	datac => a_1_dup_2234_a18,
	datad => b_3_dup_2250,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_3);

ix2152_ix36 : apex20ke_lcell 
-- Equation(s):
-- b_3_dup_2245 = lt_cntr_4 $ a_1_dup_2234_a18 $ ix2152_nx35
-- ix2152_nx39 = CARRY(lt_cntr_4 & (!ix2152_nx35 # !a_1_dup_2234_a18) # !lt_cntr_4 & !a_1_dup_2234_a18 & !ix2152_nx35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_4,
	datab => a_1_dup_2234_a18,
	cin => ix2152_nx35,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3_dup_2245,
	cout => ix2152_nx39);

reg_lt_cntr_4_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_4 = DFFE(b_3_dup_2245 & (a_2_a20 # a_3_a18 # a_1_dup_2234_a18), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FE00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_2_a20,
	datab => a_3_a18,
	datac => a_1_dup_2234_a18,
	datad => b_3_dup_2245,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_4);

ix2152_ix40 : apex20ke_lcell 
-- Equation(s):
-- b_3_dup_2240 = lt_cntr_5 $ a_1_dup_2234_a18 $ !ix2152_nx39
-- ix2152_nx43 = CARRY(lt_cntr_5 & a_1_dup_2234_a18 & !ix2152_nx39 # !lt_cntr_5 & (a_1_dup_2234_a18 # !ix2152_nx39))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_5,
	datab => a_1_dup_2234_a18,
	cin => ix2152_nx39,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3_dup_2240,
	cout => ix2152_nx43);

reg_lt_cntr_5_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_cntr_5 = DFFE(b_3_dup_2240 & (a_2_a20 # a_3_a18 # a_1_dup_2234_a18), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FE00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => a_2_a20,
	datab => a_3_a18,
	datac => a_1_dup_2234_a18,
	datad => b_3_dup_2240,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_cntr_5);

ix579_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix579_a3 = lt_cntr_3 & lt_cntr_2 & lt_cntr_5 & lt_cntr_4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_3,
	datab => lt_cntr_2,
	datac => lt_cntr_5,
	datad => lt_cntr_4,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix579_a3);

a_1_a18_I : apex20ke_lcell 
-- Equation(s):
-- a_1_a18 = (!nx2552 & lt_cntr_1 & lt_cntr_0) & CASCADE(ix579_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => nx2552,
	datac => lt_cntr_1,
	datad => lt_cntr_0,
	cascin => ix579_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => a_1_a18);

a_1_dup_2234_a18_I : apex20ke_lcell 
-- Equation(s):
-- a_1_dup_2234_a18 = (!nx981_a18 & !a_1_a18 & (OUT_CPS_ainc_cps_vec_a1_a # !OUT_CPS_ainc_cps_vec_a0_a)) & CASCADE(nx1155_a11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_ainc_cps_vec_a0_a,
	datab => OUT_CPS_ainc_cps_vec_a1_a,
	datac => nx981_a18,
	datad => a_1_a18,
	cascin => nx1155_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => a_1_dup_2234_a18);

ix656_a7_I : apex20ke_lcell 
-- Equation(s):
-- ix656_a7 = !a_1_dup_2234_a18 & !a_3_a18

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => a_1_dup_2234_a18,
	datad => a_3_a18,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix656_a7);

ix2152_ix44 : apex20ke_lcell 
-- Equation(s):
-- b_3 = nx2552 $ ix2152_nx43 $ !a_1_dup_2234_a18

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AA5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx2552,
	datad => a_1_dup_2234_a18,
	cin => ix2152_nx43,
	devclrn => devclrn,
	devpor => devpor,
	combout => b_3);

nx1153_a9_I : apex20ke_lcell 
-- Equation(s):
-- nx1153_a9 = nx981_a18 # a_1_a18

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => nx981_a18,
	datad => a_1_a18,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1153_a9);

ix656_a13_I : apex20ke_lcell 
-- Equation(s):
-- ix656_a13 = !nx1153_a9

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => nx1153_a9,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix656_a13);

reg_lt_cntr_6_a0 : apex20ke_lcell 
-- Equation(s):
-- nx2552 = DFFE((ix656_a7 & (IN_CPS_areduce_nor_6 # !OUT_CPS_areduce_nor_6) # !b_3) & CASCADE(ix656_a13), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx2018_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8CFF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_areduce_nor_6,
	datab => ix656_a7,
	datac => OUT_CPS_areduce_nor_6,
	datad => b_3,
	cascin => ix656_a13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx2018_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => nx2552);

ix580_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix580_a3 = !lt_cntr_3 & !lt_cntr_2 & !lt_cntr_5 & !lt_cntr_4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt_cntr_3,
	datab => lt_cntr_2,
	datac => lt_cntr_5,
	datad => lt_cntr_4,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix580_a3);

nx981_a18_I : apex20ke_lcell 
-- Equation(s):
-- nx981_a18 = (nx2552 & !lt_cntr_1 & !lt_cntr_0) & CASCADE(ix580_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => nx2552,
	datac => lt_cntr_1,
	datad => lt_cntr_0,
	cascin => ix580_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx981_a18);

reg_lt_inc_en_a0 : apex20ke_lcell 
-- Equation(s):
-- lt_inc_en = DFFE(nx981_a18, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1153_a9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => nx981_a18,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1153_a9,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt_inc_en);

Time_Clr_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Clr,
	combout => Time_Clr_acombout);

nx2929_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2929_a10 = (!GLOBAL(Time_Clr_acombout) & !nx803_a21 & (!lt_inc_en # !Time_Enable_dup0)) & CASCADE(nx1130_a35)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0105",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Time_Clr_acombout,
	datab => Time_Enable_dup0,
	datac => nx803_a21,
	datad => lt_inc_en,
	cascin => nx1130_a35,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2929_a10);

lt1_cnt_ix49 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_0 = DFFE(nx2930_a10 & !lt1_cnt_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx7 = CARRY(lt1_cnt_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_0,
	cout => lt1_cnt_nx7);

ix578_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix578_a3 = !lt1_cnt_7 & lt1_cnt_4 & !lt1_cnt_6 & !lt1_cnt_5

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_7,
	datab => lt1_cnt_4,
	datac => lt1_cnt_6,
	datad => lt1_cnt_5,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix578_a3);

ix641_a39_I : apex20ke_lcell 
-- Equation(s):
-- ix641_a39 = (lt1_cnt_0 & lt1_cnt_2 & lt1_cnt_3 & lt1_cnt_1) & CASCADE(ix578_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_0,
	datab => lt1_cnt_2,
	datac => lt1_cnt_3,
	datad => lt1_cnt_1,
	cascin => ix578_a3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix641_a39);

nx1136_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx1136_a12 = (ix652_a27) & CASCADE(ix641_a39)
-- nx1136_a14 = (ix652_a27) & CASCADE(ix641_a39)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix652_a27,
	cascin => ix641_a39,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1136_a12,
	cascout => nx1136_a14);

nx1130_a26_I : apex20ke_lcell 
-- Equation(s):
-- nx1130_a26 = PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1130_a26);

ix646_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix646_a3 = PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !Time_Start_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "80FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => Time_Start_acombout,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix646_a3);

ix563_a61_I : apex20ke_lcell 
-- Equation(s):
-- ix563_a61 = (!GLOBAL(Time_Clr_acombout) & (!ix553_a20 # !nx1130_a26)) & CASCADE(ix646_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "050F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx1130_a26,
	datac => Time_Clr_acombout,
	datad => ix553_a20,
	cascin => ix646_a3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix563_a61);

nx2930_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2930_a10 = (lt_inc_en & !nx1136_a12 & Time_Enable_dup0) & CASCADE(ix563_a61)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => lt_inc_en,
	datac => nx1136_a12,
	datad => Time_Enable_dup0,
	cascin => ix563_a61,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2930_a10);

lt1_cnt_ix46 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_1 = DFFE(nx2930_a10 & lt1_cnt_1 $ lt1_cnt_nx7, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx13 = CARRY(!lt1_cnt_nx7 # !lt1_cnt_1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_1,
	cin => lt1_cnt_nx7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_1,
	cout => lt1_cnt_nx13);

lt1_cnt_ix43 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_2 = DFFE(nx2930_a10 & lt1_cnt_2 $ !lt1_cnt_nx13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx19 = CARRY(lt1_cnt_2 & !lt1_cnt_nx13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_2,
	cin => lt1_cnt_nx13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_2,
	cout => lt1_cnt_nx19);

lt1_cnt_ix40 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_3 = DFFE(nx2930_a10 & lt1_cnt_3 $ lt1_cnt_nx19, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx25 = CARRY(!lt1_cnt_nx19 # !lt1_cnt_3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_3,
	cin => lt1_cnt_nx19,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_3,
	cout => lt1_cnt_nx25);

lt1_cnt_ix37 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_4 = DFFE(nx2930_a10 & lt1_cnt_4 $ !lt1_cnt_nx25, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx31 = CARRY(lt1_cnt_4 & !lt1_cnt_nx25)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_4,
	cin => lt1_cnt_nx25,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_4,
	cout => lt1_cnt_nx31);

lt1_cnt_ix34 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_5 = DFFE(nx2930_a10 & lt1_cnt_5 $ lt1_cnt_nx31, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx37 = CARRY(!lt1_cnt_nx31 # !lt1_cnt_5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_5,
	cin => lt1_cnt_nx31,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_5,
	cout => lt1_cnt_nx37);

lt1_cnt_ix31 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_6 = DFFE(nx2930_a10 & lt1_cnt_6 $ !lt1_cnt_nx37, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx43 = CARRY(lt1_cnt_6 & !lt1_cnt_nx37)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_6,
	cin => lt1_cnt_nx37,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_6,
	cout => lt1_cnt_nx43);

lt1_cnt_ix28 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_7 = DFFE(nx2930_a10 & lt1_cnt_7 $ lt1_cnt_nx43, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx49 = CARRY(!lt1_cnt_nx43 # !lt1_cnt_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_7,
	cin => lt1_cnt_nx43,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_7,
	cout => lt1_cnt_nx49);

lt1_cnt_ix25 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_8 = DFFE(nx2930_a10 & lt1_cnt_8 $ !lt1_cnt_nx49, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx59 = CARRY(lt1_cnt_8 & !lt1_cnt_nx49)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_8,
	cin => lt1_cnt_nx49,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_8,
	cout => lt1_cnt_nx59);

lt1_cnt_ix22 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_9 = DFFE(nx2930_a10 & lt1_cnt_9 $ lt1_cnt_nx59, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx63 = CARRY(!lt1_cnt_nx59 # !lt1_cnt_9)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => lt1_cnt_9,
	cin => lt1_cnt_nx59,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_9,
	cout => lt1_cnt_nx63);

lt1_cnt_ix19 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_10 = DFFE(nx2930_a10 & lt1_cnt_10 $ !lt1_cnt_nx63, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx67 = CARRY(lt1_cnt_10 & !lt1_cnt_nx63)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => lt1_cnt_10,
	cin => lt1_cnt_nx63,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_10,
	cout => lt1_cnt_nx67);

lt1_cnt_ix16 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_11 = DFFE(nx2930_a10 & lt1_cnt_11 $ lt1_cnt_nx67, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx71 = CARRY(!lt1_cnt_nx67 # !lt1_cnt_11)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_11,
	cin => lt1_cnt_nx67,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_11,
	cout => lt1_cnt_nx71);

lt1_cnt_ix13 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_12 = DFFE(nx2930_a10 & lt1_cnt_12 $ !lt1_cnt_nx71, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx75 = CARRY(lt1_cnt_12 & !lt1_cnt_nx71)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => lt1_cnt_12,
	cin => lt1_cnt_nx71,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_12,
	cout => lt1_cnt_nx75);

lt1_cnt_ix10 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_13 = DFFE(nx2930_a10 & lt1_cnt_13 $ lt1_cnt_nx75, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)
-- lt1_cnt_nx79 = CARRY(!lt1_cnt_nx75 # !lt1_cnt_13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_13,
	cin => lt1_cnt_nx75,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_13,
	cout => lt1_cnt_nx79);

lt1_cnt_ix7 : apex20ke_lcell 
-- Equation(s):
-- lt1_cnt_14 = DFFE(nx2930_a10 & lt1_cnt_nx79 $ !lt1_cnt_14, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , !nx2929_a10)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => lt1_cnt_14,
	cin => lt1_cnt_nx79,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => NOT_nx2929_a10,
	sclr => NOT_nx2930_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => lt1_cnt_14);

ix600_a31_I : apex20ke_lcell 
-- Equation(s):
-- ix600_a31 = lt1_cnt_9 & lt1_cnt_10 & !lt1_cnt_8

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00A0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_9,
	datac => lt1_cnt_10,
	datad => lt1_cnt_8,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix600_a31);

ix652_a27_I : apex20ke_lcell 
-- Equation(s):
-- ix652_a27 = (!lt1_cnt_12 & lt1_cnt_11 & lt1_cnt_14 & !lt1_cnt_13) & CASCADE(ix600_a31)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_12,
	datab => lt1_cnt_11,
	datac => lt1_cnt_14,
	datad => lt1_cnt_13,
	cascin => ix600_a31,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix652_a27);

LT_CEN_a10_I : apex20ke_lcell 
-- Equation(s):
-- LT_CEN_a10 = (lt_inc_en & Time_Enable_dup0) & CASCADE(nx1136_a14)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => lt_inc_en,
	datad => Time_Enable_dup0,
	cascin => nx1136_a14,
	devclrn => devclrn,
	devpor => devpor,
	combout => LT_CEN_a10);

ltime_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(Time_Clr_acombout) & LT_CEN_a10 $ ltime_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => LT_CEN_a10,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a0_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a1_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a1_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a1_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a2_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a2_a & !ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a2_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a2_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a3_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a3_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a3_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a4_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a4_a & !ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a4_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a4_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a5_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a5_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a5_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a6_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a6_a & !ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a6_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a6_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a7_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a7_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a7_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a8_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a8_a & !ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a8_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a8_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a9_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a9_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a9_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a10_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a10_a & !ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a10_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a10_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a11_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a11_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a11_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a12_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a12_a & !ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a12_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a12_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a13_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a13_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a13_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a14_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a14_a & !ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a14_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a14_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a15_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a15_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a15_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a16_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a16_a & !ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a16_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a16_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a17_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a17_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a17_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a18_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a18_a & !ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a18_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a18_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a19_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a19_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a19_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a20_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a20_a & !ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a20_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a20_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a21_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a21_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a21_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a21_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a22_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a22_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a22_a & !ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a22_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a22_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a23_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a23_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a23_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a23_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a24_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a24_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a24_a & !ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a24_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a24_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a25_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a25_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a25_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a25_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a26_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a26_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a26_a & !ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a26_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a26_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a27_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a27_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a27_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a27_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a28_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a28_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a28_a & !ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a28_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a28_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a29_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a29_a $ (LT_CEN_a10 & ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT # !ltime_cntr_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a29_a,
	datab => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a29_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a30_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a30_a $ (LT_CEN_a10 & !ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ltime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(ltime_cntr_awysi_counter_asload_path_a30_a & !ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LT_CEN_a10,
	datab => ltime_cntr_awysi_counter_asload_path_a30_a,
	cin => ltime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a30_a,
	cout => ltime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT);

ltime_cntr_awysi_counter_acounter_cell_a31_a : apex20ke_lcell 
-- Equation(s):
-- ltime_cntr_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(Time_Clr_acombout) & ltime_cntr_awysi_counter_asload_path_a31_a $ (ltime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT & LT_CEN_a10), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ltime_cntr_awysi_counter_asload_path_a31_a,
	datad => LT_CEN_a10,
	cin => ltime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ltime_cntr_awysi_counter_asload_path_a31_a);

ctime_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(Time_Clr_acombout) & CT_CEN_a1 $ ctime_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CT_CEN_a1,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a0_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a1_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a1_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a1_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a2_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a2_a & !ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a2_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a2_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a3_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a3_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a3_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a4_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a4_a & !ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a4_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a4_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a5_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a5_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a5_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a6_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a6_a & !ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a6_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a6_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a7_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a7_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a7_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a8_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a8_a & !ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a8_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a8_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a9_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a9_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a9_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a10_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a10_a & !ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a10_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a10_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a11_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a11_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a11_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a12_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a12_a & !ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a12_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a12_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a13_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a13_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a13_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a14_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a14_a & !ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a14_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a14_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a15_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a15_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a15_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a16_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a16_a & !ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a16_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a16_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a17_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a17_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a17_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a18_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a18_a & !ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a18_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a18_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a19_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ctime_cntr_awysi_counter_asload_path_a19_a,
	datab => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a19_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a20_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a20_a & !ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a20_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a20_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a21_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a21_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a21_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a21_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a22_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a22_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a22_a & !ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a22_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a22_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a23_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a23_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a23_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a23_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a24_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a24_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a24_a & !ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a24_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a24_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a25_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a25_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a25_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a25_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a26_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a26_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a26_a & !ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a26_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a26_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a27_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a27_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a27_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a27_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a28_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a28_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a28_a & !ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a28_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a28_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a29_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a29_a $ (CT_CEN_a1 & ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT # !ctime_cntr_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a29_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a29_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a30_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a30_a $ (CT_CEN_a1 & !ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ctime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(ctime_cntr_awysi_counter_asload_path_a30_a & !ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CT_CEN_a1,
	datab => ctime_cntr_awysi_counter_asload_path_a30_a,
	cin => ctime_cntr_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a30_a,
	cout => ctime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT);

ctime_cntr_awysi_counter_acounter_cell_a31_a : apex20ke_lcell 
-- Equation(s):
-- ctime_cntr_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(Time_Clr_acombout) & ctime_cntr_awysi_counter_asload_path_a31_a $ (ctime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT & CT_CEN_a1), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CCC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ctime_cntr_awysi_counter_asload_path_a31_a,
	datad => CT_CEN_a1,
	cin => ctime_cntr_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ctime_cntr_awysi_counter_asload_path_a31_a);

ix609_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix609_a6 = PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a31_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix609_a6);

nx2896_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2896_a10 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a31_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix609_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a31_a,
	cascin => ix609_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2896_a10);

PRESET_ibuf_30 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(30),
	combout => PRESET_a30_a_acombout);

ix610_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix610_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a30_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix610_a6);

nx2897_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2897_a10 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a30_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix610_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a30_a,
	cascin => ix610_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2897_a10);

PRESET_ibuf_29 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(29),
	combout => PRESET_a29_a_acombout);

ROI_Inc_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ROI_Inc,
	combout => ROI_Inc_acombout);

reg_ROI_INC_DEL_a0 : apex20ke_lcell 
-- Equation(s):
-- ROI_INC_DEL = DFFE(ROI_Inc_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => ROI_Inc_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_INC_DEL);

ROI_INCREMENT_a0_I : apex20ke_lcell 
-- Equation(s):
-- ROI_INCREMENT_a0 = ROI_Inc_acombout & !ROI_INC_DEL

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ROI_Inc_acombout,
	datad => ROI_INC_DEL,
	devclrn => devclrn,
	devpor => devpor,
	combout => ROI_INCREMENT_a0);

ROI_CLR_a2_I : apex20ke_lcell 
-- Equation(s):
-- ROI_CLR_a2 = PRESET_MODE_a1_a_acombout # GLOBAL(Time_Clr_acombout) # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF7",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => Time_Clr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => ROI_CLR_a2);

ROI_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_INCREMENT_a0 $ ROI_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a0_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a1_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a1_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a1_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a2_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a2_a & !ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a2_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a2_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a3_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a3_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a3_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a4_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a4_a & !ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a4_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a4_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a5_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a5_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a5_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a6_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a6_a & !ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a6_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a6_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a7_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a7_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a7_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a8_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a8_a & !ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a8_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a8_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a9_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a9_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a9_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a10_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a10_a & !ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a10_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a10_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a11_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a11_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a11_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a12_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a12_a & !ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a12_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a12_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a13_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a13_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a13_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a14_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a14_a & !ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a14_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a14_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a15_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a15_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a15_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a16_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a16_a & !ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a16_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a16_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a17_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a17_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a17_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a18_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a18_a & !ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a18_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a18_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a19_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a19_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a19_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a20_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a20_a & !ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a20_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a20_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a21_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a21_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a21_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a21_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a22_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a22_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a22_a & !ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a22_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a22_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a23_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a23_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INCREMENT_a0,
	datab => ROI_CNTR_awysi_counter_asload_path_a23_a,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a23_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a24_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a24_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a24_a & !ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a24_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a24_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a25_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a25_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a25_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a25_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a26_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a26_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a26_a & !ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a26_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a26_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a27_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a27_a $ (ROI_INCREMENT_a0 & ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT # !ROI_CNTR_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a27_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a27_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a28_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a28_a $ (ROI_INCREMENT_a0 & !ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- ROI_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(ROI_CNTR_awysi_counter_asload_path_a28_a & !ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => ROI_CNTR_awysi_counter_asload_path_a28_a,
	datab => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a28_a,
	cout => ROI_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT);

ROI_CNTR_awysi_counter_acounter_cell_a29_a : apex20ke_lcell 
-- Equation(s):
-- ROI_CNTR_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(ROI_CLR_a2) & ROI_CNTR_awysi_counter_asload_path_a29_a $ (ROI_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT & ROI_INCREMENT_a0), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CCC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ROI_CNTR_awysi_counter_asload_path_a29_a,
	datad => ROI_INCREMENT_a0,
	cin => ROI_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ROI_CLR_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => ROI_CNTR_awysi_counter_asload_path_a29_a);

ix516_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix516_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a29_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix516_a21);

ix516_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix516_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a29_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix516_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a29_a,
	cascin => ix516_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix516_a67);

ix611_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix611_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a29_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix611_a6);

nx2898_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2898_a10 = (ix516_a67) & CASCADE(ix611_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix516_a67,
	cascin => ix611_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2898_a10);

ix517_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix517_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a28_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix517_a21);

ix517_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix517_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a28_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix517_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a28_a,
	cascin => ix517_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix517_a67);

ix612_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix612_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a28_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix612_a6);

nx2899_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2899_a10 = (ix517_a67) & CASCADE(ix612_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix517_a67,
	cascin => ix612_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2899_a10);

PRESET_ibuf_28 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(28),
	combout => PRESET_a28_a_acombout);

PRESET_ibuf_27 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(27),
	combout => PRESET_a27_a_acombout);

ix518_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix518_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a27_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix518_a21);

ix518_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix518_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a27_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix518_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a27_a,
	cascin => ix518_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix518_a67);

ix613_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix613_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a27_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix613_a6);

nx2900_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2900_a10 = (ix518_a67) & CASCADE(ix613_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix518_a67,
	cascin => ix613_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2900_a10);

ix519_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix519_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a26_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix519_a21);

ix519_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix519_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a26_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix519_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a26_a,
	cascin => ix519_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix519_a67);

ix614_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix614_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a26_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix614_a6);

nx2901_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2901_a10 = (ix519_a67) & CASCADE(ix614_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix519_a67,
	cascin => ix614_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2901_a10);

PRESET_ibuf_26 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(26),
	combout => PRESET_a26_a_acombout);

PRESET_ibuf_25 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(25),
	combout => PRESET_a25_a_acombout);

ix520_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix520_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a25_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix520_a21);

ix520_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix520_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a25_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix520_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a25_a,
	cascin => ix520_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix520_a67);

ix615_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix615_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a25_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix615_a6);

nx2902_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2902_a10 = (ix520_a67) & CASCADE(ix615_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix520_a67,
	cascin => ix615_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2902_a10);

PRESET_ibuf_24 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(24),
	combout => PRESET_a24_a_acombout);

ix521_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix521_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a24_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix521_a21);

ix521_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix521_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a24_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix521_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a24_a,
	cascin => ix521_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix521_a67);

ix616_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix616_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a24_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix616_a6);

nx2903_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2903_a10 = (ix521_a67) & CASCADE(ix616_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix521_a67,
	cascin => ix616_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2903_a10);

ix522_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix522_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a23_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix522_a21);

ix522_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix522_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a23_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix522_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a23_a,
	cascin => ix522_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix522_a67);

ix617_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix617_a6 = PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a23_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix617_a6);

nx2904_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2904_a10 = (ix522_a67) & CASCADE(ix617_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix522_a67,
	cascin => ix617_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2904_a10);

PRESET_ibuf_23 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(23),
	combout => PRESET_a23_a_acombout);

PRESET_ibuf_22 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(22),
	combout => PRESET_a22_a_acombout);

ix523_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix523_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a22_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix523_a21);

ix523_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix523_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a22_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix523_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a22_a,
	cascin => ix523_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix523_a67);

ix618_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix618_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a22_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix618_a6);

nx2905_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2905_a10 = (ix523_a67) & CASCADE(ix618_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix523_a67,
	cascin => ix618_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2905_a10);

ix524_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix524_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a21_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix524_a21);

ix524_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix524_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a21_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix524_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a21_a,
	cascin => ix524_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix524_a67);

ix619_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix619_a6 = PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a21_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix619_a6);

nx2906_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2906_a10 = (ix524_a67) & CASCADE(ix619_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix524_a67,
	cascin => ix619_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2906_a10);

PRESET_ibuf_21 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(21),
	combout => PRESET_a21_a_acombout);

PRESET_ibuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(20),
	combout => PRESET_a20_a_acombout);

ix525_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix525_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a20_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix525_a21);

ix525_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix525_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a20_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix525_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a20_a,
	cascin => ix525_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix525_a67);

ix620_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix620_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a20_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix620_a6);

nx2907_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2907_a10 = (ix525_a67) & CASCADE(ix620_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix525_a67,
	cascin => ix620_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2907_a10);

PRESET_ibuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(19),
	combout => PRESET_a19_a_acombout);

ix526_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix526_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a19_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix526_a21);

ix526_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix526_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a19_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix526_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a19_a,
	cascin => ix526_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix526_a67);

ix621_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix621_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a19_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix621_a6);

nx2908_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2908_a10 = (ix526_a67) & CASCADE(ix621_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ix526_a67,
	cascin => ix621_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2908_a10);

ix527_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix527_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a18_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix527_a21);

ix527_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix527_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a18_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix527_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a18_a,
	cascin => ix527_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix527_a67);

ix622_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix622_a6 = PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix622_a6);

nx2909_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2909_a10 = (ix527_a67) & CASCADE(ix622_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix527_a67,
	cascin => ix622_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2909_a10);

PRESET_ibuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(18),
	combout => PRESET_a18_a_acombout);

ix528_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix528_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a17_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix528_a21);

ix528_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix528_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a17_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix528_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a17_a,
	cascin => ix528_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix528_a67);

ix623_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix623_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a17_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix623_a6);

nx2910_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2910_a10 = (ix528_a67) & CASCADE(ix623_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix528_a67,
	cascin => ix623_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2910_a10);

PRESET_ibuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(17),
	combout => PRESET_a17_a_acombout);

ix529_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix529_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a16_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix529_a21);

ix529_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix529_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a16_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix529_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a16_a,
	cascin => ix529_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix529_a67);

ix624_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix624_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a16_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix624_a6);

nx2911_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2911_a10 = (ix529_a67) & CASCADE(ix624_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix529_a67,
	cascin => ix624_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2911_a10);

PRESET_ibuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(16),
	combout => PRESET_a16_a_acombout);

PRESET_ibuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(15),
	combout => PRESET_a15_a_acombout);

ix530_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix530_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a15_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix530_a21);

ix530_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix530_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a15_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix530_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a15_a,
	cascin => ix530_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix530_a67);

ix625_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix625_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix625_a6);

nx2912_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2912_a10 = (ix530_a67) & CASCADE(ix625_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix530_a67,
	cascin => ix625_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2912_a10);

PRESET_ibuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(14),
	combout => PRESET_a14_a_acombout);

ix531_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix531_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a14_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix531_a21);

ix531_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix531_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a14_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix531_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a14_a,
	cascin => ix531_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix531_a67);

ix626_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix626_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix626_a6);

nx2913_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2913_a10 = (ix531_a67) & CASCADE(ix626_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix531_a67,
	cascin => ix626_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2913_a10);

PRESET_ibuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(13),
	combout => PRESET_a13_a_acombout);

ix532_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix532_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a13_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix532_a21);

ix532_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix532_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a13_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix532_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a13_a,
	cascin => ix532_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix532_a67);

ix627_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix627_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix627_a6);

nx2914_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2914_a10 = (ix532_a67) & CASCADE(ix627_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix532_a67,
	cascin => ix627_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2914_a10);

ix533_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix533_a21 = PRESET_MODE_a1_a_acombout # !PRESET_MODE_a2_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a12_a # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => ROI_CNTR_awysi_counter_asload_path_a12_a,
	datad => PRESET_MODE_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix533_a21);

ix533_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix533_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a12_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix533_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a12_a,
	cascin => ix533_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix533_a67);

ix628_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix628_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix628_a6);

nx2915_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2915_a10 = (ix533_a67) & CASCADE(ix628_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix533_a67,
	cascin => ix628_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2915_a10);

PRESET_ibuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(12),
	combout => PRESET_a12_a_acombout);

PRESET_ibuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(11),
	combout => PRESET_a11_a_acombout);

ix534_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix534_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a11_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix534_a21);

ix534_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix534_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a11_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix534_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a11_a,
	cascin => ix534_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix534_a67);

ix629_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix629_a6 = PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix629_a6);

nx2916_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2916_a10 = (ix534_a67) & CASCADE(ix629_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix534_a67,
	cascin => ix629_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2916_a10);

ix535_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix535_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a10_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix535_a21);

ix535_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix535_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a10_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix535_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a10_a,
	cascin => ix535_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix535_a67);

ix630_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix630_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix630_a6);

nx2917_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2917_a10 = (ix535_a67) & CASCADE(ix630_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix535_a67,
	cascin => ix630_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2917_a10);

PRESET_ibuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(10),
	combout => PRESET_a10_a_acombout);

ix536_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix536_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a9_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix536_a21);

ix536_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix536_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a9_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix536_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a9_a,
	cascin => ix536_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix536_a67);

ix631_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix631_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix631_a6);

nx2918_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2918_a10 = (ix536_a67) & CASCADE(ix631_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix536_a67,
	cascin => ix631_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2918_a10);

PRESET_ibuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(9),
	combout => PRESET_a9_a_acombout);

ix537_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix537_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a8_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix537_a21);

ix537_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix537_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a8_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix537_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a8_a,
	cascin => ix537_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix537_a67);

ix632_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix632_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix632_a6);

nx2919_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2919_a10 = (ix537_a67) & CASCADE(ix632_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix537_a67,
	cascin => ix632_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2919_a10);

PRESET_ibuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(8),
	combout => PRESET_a8_a_acombout);

ix538_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix538_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a7_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix538_a21);

ix538_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix538_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a7_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix538_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a7_a,
	cascin => ix538_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix538_a67);

ix633_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix633_a6 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "42FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix633_a6);

nx2920_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2920_a10 = (ix538_a67) & CASCADE(ix633_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix538_a67,
	cascin => ix633_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2920_a10);

PRESET_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(7),
	combout => PRESET_a7_a_acombout);

PRESET_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(6),
	combout => PRESET_a6_a_acombout);

ix539_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix539_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a6_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix539_a21);

ix539_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix539_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a6_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix539_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a6_a,
	cascin => ix539_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix539_a67);

ix634_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix634_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix634_a6);

nx2921_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2921_a10 = (ix539_a67) & CASCADE(ix634_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix539_a67,
	cascin => ix634_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2921_a10);

PRESET_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(5),
	combout => PRESET_a5_a_acombout);

ix540_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix540_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a5_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix540_a21);

ix540_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix540_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a5_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix540_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a5_a,
	cascin => ix540_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix540_a67);

ix635_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix635_a6 = PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a2_a_acombout # !ctime_cntr_awysi_counter_asload_path_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a2_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix635_a6);

nx2922_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2922_a10 = (ix540_a67) & CASCADE(ix635_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix540_a67,
	cascin => ix635_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2922_a10);

PRESET_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(4),
	combout => PRESET_a4_a_acombout);

ix541_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix541_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a4_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix541_a21);

ix541_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix541_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a4_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix541_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a4_a,
	cascin => ix541_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix541_a67);

ix636_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix636_a6 = PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix636_a6);

nx2923_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2923_a10 = (ix541_a67) & CASCADE(ix636_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix541_a67,
	cascin => ix636_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2923_a10);

PRESET_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(3),
	combout => PRESET_a3_a_acombout);

ix542_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix542_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a3_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix542_a21);

ix542_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix542_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a3_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix542_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a3_a,
	cascin => ix542_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix542_a67);

ix637_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix637_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix637_a6);

nx2924_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2924_a10 = (ix542_a67) & CASCADE(ix637_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix542_a67,
	cascin => ix637_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2924_a10);

PRESET_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(2),
	combout => PRESET_a2_a_acombout);

ix543_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix543_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a2_a # !PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix543_a21);

ix543_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix543_a67 = (PRESET_MODE_a0_a_acombout # PRESET_MODE_a2_a_acombout # !ltime_cntr_awysi_counter_asload_path_a2_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix543_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a2_a,
	cascin => ix543_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix543_a67);

ix638_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix638_a6 = PRESET_MODE_a0_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a0_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix638_a6);

nx2925_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2925_a10 = (ix543_a67) & CASCADE(ix638_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix543_a67,
	cascin => ix638_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2925_a10);

PRESET_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(1),
	combout => PRESET_a1_a_acombout);

ix544_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix544_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a1_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "DFFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix544_a21);

ix544_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix544_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a1_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix544_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a1_a,
	cascin => ix544_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix544_a67);

ix639_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix639_a6 = PRESET_MODE_a2_a_acombout & !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout & PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout # !ctime_cntr_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "24FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix639_a6);

nx2926_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2926_a10 = (ix544_a67) & CASCADE(ix639_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix544_a67,
	cascin => ix639_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2926_a10);

ix545_a21_I : apex20ke_lcell 
-- Equation(s):
-- ix545_a21 = PRESET_MODE_a1_a_acombout # !ROI_CNTR_awysi_counter_asload_path_a0_a # !PRESET_MODE_a0_a_acombout # !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F7FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ROI_CNTR_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix545_a21);

ix545_a67_I : apex20ke_lcell 
-- Equation(s):
-- ix545_a67 = (PRESET_MODE_a2_a_acombout # PRESET_MODE_a0_a_acombout # !ltime_cntr_awysi_counter_asload_path_a0_a # !PRESET_MODE_a1_a_acombout) & CASCADE(ix545_a21)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ltime_cntr_awysi_counter_asload_path_a0_a,
	cascin => ix545_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix545_a67);

ix640_a6_I : apex20ke_lcell 
-- Equation(s):
-- ix640_a6 = PRESET_MODE_a2_a_acombout & PRESET_MODE_a0_a_acombout & !PRESET_MODE_a1_a_acombout # !PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout & PRESET_MODE_a1_a_acombout # !ctime_cntr_awysi_counter_asload_path_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "18FF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a2_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => PRESET_MODE_a1_a_acombout,
	datad => ctime_cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix640_a6);

nx2927_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2927_a10 = (ix545_a67) & CASCADE(ix640_a6)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix545_a67,
	cascin => ix640_a6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2927_a10);

PRESET_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PRESET(0),
	combout => PRESET_a0_a_acombout);

modgen_gt_13_ix69 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx70 = CARRY(nx2927_a10 & PRESET_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0088",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2927_a10,
	datab => PRESET_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx70);

modgen_gt_13_ix71 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx72 = CARRY(PRESET_a1_a_acombout & !nx2926_a10 & !modgen_gt_13_nx70 # !PRESET_a1_a_acombout & (!modgen_gt_13_nx70 # !nx2926_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a1_a_acombout,
	datab => nx2926_a10,
	cin => modgen_gt_13_nx70,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx72);

modgen_gt_13_ix73 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx74 = CARRY(PRESET_a2_a_acombout & (nx2925_a10 # !modgen_gt_13_nx72) # !PRESET_a2_a_acombout & nx2925_a10 & !modgen_gt_13_nx72)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a2_a_acombout,
	datab => nx2925_a10,
	cin => modgen_gt_13_nx72,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx74);

modgen_gt_13_ix75 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx76 = CARRY(PRESET_a3_a_acombout & !nx2924_a10 & !modgen_gt_13_nx74 # !PRESET_a3_a_acombout & (!modgen_gt_13_nx74 # !nx2924_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a3_a_acombout,
	datab => nx2924_a10,
	cin => modgen_gt_13_nx74,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx76);

modgen_gt_13_ix77 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx78 = CARRY(PRESET_a4_a_acombout & (nx2923_a10 # !modgen_gt_13_nx76) # !PRESET_a4_a_acombout & nx2923_a10 & !modgen_gt_13_nx76)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a4_a_acombout,
	datab => nx2923_a10,
	cin => modgen_gt_13_nx76,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx78);

modgen_gt_13_ix79 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx80 = CARRY(PRESET_a5_a_acombout & !nx2922_a10 & !modgen_gt_13_nx78 # !PRESET_a5_a_acombout & (!modgen_gt_13_nx78 # !nx2922_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a5_a_acombout,
	datab => nx2922_a10,
	cin => modgen_gt_13_nx78,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx80);

modgen_gt_13_ix81 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx82 = CARRY(PRESET_a6_a_acombout & (nx2921_a10 # !modgen_gt_13_nx80) # !PRESET_a6_a_acombout & nx2921_a10 & !modgen_gt_13_nx80)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a6_a_acombout,
	datab => nx2921_a10,
	cin => modgen_gt_13_nx80,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx82);

modgen_gt_13_ix83 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx84 = CARRY(nx2920_a10 & !PRESET_a7_a_acombout & !modgen_gt_13_nx82 # !nx2920_a10 & (!modgen_gt_13_nx82 # !PRESET_a7_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2920_a10,
	datab => PRESET_a7_a_acombout,
	cin => modgen_gt_13_nx82,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx84);

modgen_gt_13_ix85 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx86 = CARRY(nx2919_a10 & (PRESET_a8_a_acombout # !modgen_gt_13_nx84) # !nx2919_a10 & PRESET_a8_a_acombout & !modgen_gt_13_nx84)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2919_a10,
	datab => PRESET_a8_a_acombout,
	cin => modgen_gt_13_nx84,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx86);

modgen_gt_13_ix87 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx88 = CARRY(nx2918_a10 & !PRESET_a9_a_acombout & !modgen_gt_13_nx86 # !nx2918_a10 & (!modgen_gt_13_nx86 # !PRESET_a9_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2918_a10,
	datab => PRESET_a9_a_acombout,
	cin => modgen_gt_13_nx86,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx88);

modgen_gt_13_ix89 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx90 = CARRY(nx2917_a10 & (PRESET_a10_a_acombout # !modgen_gt_13_nx88) # !nx2917_a10 & PRESET_a10_a_acombout & !modgen_gt_13_nx88)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2917_a10,
	datab => PRESET_a10_a_acombout,
	cin => modgen_gt_13_nx88,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx90);

modgen_gt_13_ix91 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx92 = CARRY(PRESET_a11_a_acombout & !nx2916_a10 & !modgen_gt_13_nx90 # !PRESET_a11_a_acombout & (!modgen_gt_13_nx90 # !nx2916_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a11_a_acombout,
	datab => nx2916_a10,
	cin => modgen_gt_13_nx90,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx92);

modgen_gt_13_ix93 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx94 = CARRY(nx2915_a10 & (PRESET_a12_a_acombout # !modgen_gt_13_nx92) # !nx2915_a10 & PRESET_a12_a_acombout & !modgen_gt_13_nx92)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2915_a10,
	datab => PRESET_a12_a_acombout,
	cin => modgen_gt_13_nx92,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx94);

modgen_gt_13_ix95 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx96 = CARRY(PRESET_a13_a_acombout & !nx2914_a10 & !modgen_gt_13_nx94 # !PRESET_a13_a_acombout & (!modgen_gt_13_nx94 # !nx2914_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a13_a_acombout,
	datab => nx2914_a10,
	cin => modgen_gt_13_nx94,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx96);

modgen_gt_13_ix97 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx98 = CARRY(PRESET_a14_a_acombout & (nx2913_a10 # !modgen_gt_13_nx96) # !PRESET_a14_a_acombout & nx2913_a10 & !modgen_gt_13_nx96)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a14_a_acombout,
	datab => nx2913_a10,
	cin => modgen_gt_13_nx96,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx98);

modgen_gt_13_ix99 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx100 = CARRY(PRESET_a15_a_acombout & !nx2912_a10 & !modgen_gt_13_nx98 # !PRESET_a15_a_acombout & (!modgen_gt_13_nx98 # !nx2912_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a15_a_acombout,
	datab => nx2912_a10,
	cin => modgen_gt_13_nx98,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx100);

modgen_gt_13_ix101 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx102 = CARRY(nx2911_a10 & (PRESET_a16_a_acombout # !modgen_gt_13_nx100) # !nx2911_a10 & PRESET_a16_a_acombout & !modgen_gt_13_nx100)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2911_a10,
	datab => PRESET_a16_a_acombout,
	cin => modgen_gt_13_nx100,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx102);

modgen_gt_13_ix103 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx104 = CARRY(nx2910_a10 & !PRESET_a17_a_acombout & !modgen_gt_13_nx102 # !nx2910_a10 & (!modgen_gt_13_nx102 # !PRESET_a17_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2910_a10,
	datab => PRESET_a17_a_acombout,
	cin => modgen_gt_13_nx102,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx104);

modgen_gt_13_ix105 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx106 = CARRY(nx2909_a10 & (PRESET_a18_a_acombout # !modgen_gt_13_nx104) # !nx2909_a10 & PRESET_a18_a_acombout & !modgen_gt_13_nx104)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2909_a10,
	datab => PRESET_a18_a_acombout,
	cin => modgen_gt_13_nx104,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx106);

modgen_gt_13_ix107 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx108 = CARRY(PRESET_a19_a_acombout & !nx2908_a10 & !modgen_gt_13_nx106 # !PRESET_a19_a_acombout & (!modgen_gt_13_nx106 # !nx2908_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a19_a_acombout,
	datab => nx2908_a10,
	cin => modgen_gt_13_nx106,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx108);

modgen_gt_13_ix109 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx110 = CARRY(PRESET_a20_a_acombout & (nx2907_a10 # !modgen_gt_13_nx108) # !PRESET_a20_a_acombout & nx2907_a10 & !modgen_gt_13_nx108)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a20_a_acombout,
	datab => nx2907_a10,
	cin => modgen_gt_13_nx108,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx110);

modgen_gt_13_ix111 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx112 = CARRY(nx2906_a10 & !PRESET_a21_a_acombout & !modgen_gt_13_nx110 # !nx2906_a10 & (!modgen_gt_13_nx110 # !PRESET_a21_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2906_a10,
	datab => PRESET_a21_a_acombout,
	cin => modgen_gt_13_nx110,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx112);

modgen_gt_13_ix113 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx114 = CARRY(PRESET_a22_a_acombout & (nx2905_a10 # !modgen_gt_13_nx112) # !PRESET_a22_a_acombout & nx2905_a10 & !modgen_gt_13_nx112)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a22_a_acombout,
	datab => nx2905_a10,
	cin => modgen_gt_13_nx112,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx114);

modgen_gt_13_ix115 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx116 = CARRY(nx2904_a10 & !PRESET_a23_a_acombout & !modgen_gt_13_nx114 # !nx2904_a10 & (!modgen_gt_13_nx114 # !PRESET_a23_a_acombout))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2904_a10,
	datab => PRESET_a23_a_acombout,
	cin => modgen_gt_13_nx114,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx116);

modgen_gt_13_ix117 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx118 = CARRY(PRESET_a24_a_acombout & (nx2903_a10 # !modgen_gt_13_nx116) # !PRESET_a24_a_acombout & nx2903_a10 & !modgen_gt_13_nx116)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a24_a_acombout,
	datab => nx2903_a10,
	cin => modgen_gt_13_nx116,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx118);

modgen_gt_13_ix119 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx120 = CARRY(PRESET_a25_a_acombout & !nx2902_a10 & !modgen_gt_13_nx118 # !PRESET_a25_a_acombout & (!modgen_gt_13_nx118 # !nx2902_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a25_a_acombout,
	datab => nx2902_a10,
	cin => modgen_gt_13_nx118,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx120);

modgen_gt_13_ix121 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx122 = CARRY(nx2901_a10 & (PRESET_a26_a_acombout # !modgen_gt_13_nx120) # !nx2901_a10 & PRESET_a26_a_acombout & !modgen_gt_13_nx120)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2901_a10,
	datab => PRESET_a26_a_acombout,
	cin => modgen_gt_13_nx120,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx122);

modgen_gt_13_ix123 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx124 = CARRY(PRESET_a27_a_acombout & !nx2900_a10 & !modgen_gt_13_nx122 # !PRESET_a27_a_acombout & (!modgen_gt_13_nx122 # !nx2900_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a27_a_acombout,
	datab => nx2900_a10,
	cin => modgen_gt_13_nx122,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx124);

modgen_gt_13_ix125 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx126 = CARRY(nx2899_a10 & (PRESET_a28_a_acombout # !modgen_gt_13_nx124) # !nx2899_a10 & PRESET_a28_a_acombout & !modgen_gt_13_nx124)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2899_a10,
	datab => PRESET_a28_a_acombout,
	cin => modgen_gt_13_nx124,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx126);

modgen_gt_13_ix127 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx128 = CARRY(PRESET_a29_a_acombout & !nx2898_a10 & !modgen_gt_13_nx126 # !PRESET_a29_a_acombout & (!modgen_gt_13_nx126 # !nx2898_a10))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0017",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a29_a_acombout,
	datab => nx2898_a10,
	cin => modgen_gt_13_nx126,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx128);

modgen_gt_13_ix129 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_13_nx130 = CARRY(PRESET_a30_a_acombout & (nx2897_a10 # !modgen_gt_13_nx128) # !PRESET_a30_a_acombout & nx2897_a10 & !modgen_gt_13_nx128)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "008E",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_a30_a_acombout,
	datab => nx2897_a10,
	cin => modgen_gt_13_nx128,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_13_nx130);

modgen_gt_13_ix131 : apex20ke_lcell 
-- Equation(s):
-- NOT_nx438 = PRESET_a31_a_acombout & !modgen_gt_13_nx130 & !nx2896_a10 # !PRESET_a31_a_acombout & (!nx2896_a10 # !modgen_gt_13_nx130)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "033F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PRESET_a31_a_acombout,
	datad => nx2896_a10,
	cin => modgen_gt_13_nx130,
	devclrn => devclrn,
	devpor => devpor,
	combout => NOT_nx438);

Preset_Done_a8_I : apex20ke_lcell 
-- Equation(s):
-- Preset_Done_a8 = DFFE(NOT_nx438 # LS_Abort_acombout & nx1142_a1, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => LS_Abort_acombout,
	datac => nx1142_a1,
	datad => NOT_nx438,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Preset_Done_a8);

ix602_a1_I : apex20ke_lcell 
-- Equation(s):
-- ix602_a1 = !GLOBAL(Time_Clr_acombout) & Time_Enable_dup0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => Time_Clr_acombout,
	datad => Time_Enable_dup0,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix602_a1);

nx2964_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2964_a12 = (nx1130_a2 & !Time_Stop_acombout & (!ix553_a20 # !nx1130_a26)) & CASCADE(ix602_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "040C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx1130_a26,
	datab => nx1130_a2,
	datac => Time_Stop_acombout,
	datad => ix553_a20,
	cascin => ix602_a1,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2964_a12);

reg_Preset_Enable_a0 : apex20ke_lcell 
-- Equation(s):
-- Preset_Enable = DFFE(PRESET_MODE_a1_a_acombout & (PRESET_MODE_a2_a_acombout # !PRESET_MODE_a0_a_acombout) # !PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC3C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Preset_Enable);

nx1157_a0_I : apex20ke_lcell 
-- Equation(s):
-- nx1157_a0 = !ps1_cnt_5 & !ps1_cnt_6
-- nx1157_a18 = !ps1_cnt_5 & !ps1_cnt_6

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_5,
	datad => ps1_cnt_6,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1157_a0,
	cascout => nx1157_a18);

ix645_a1_I : apex20ke_lcell 
-- Equation(s):
-- ix645_a1 = ps1_cnt_3 & ps1_cnt_4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => ps1_cnt_3,
	datad => ps1_cnt_4,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix645_a1);

nx2966_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2966_a12 = (ix576_a13 & Preset_Enable & ps1_cnt_2 & nx1157_a0) & CASCADE(ix645_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ix576_a13,
	datab => Preset_Enable,
	datac => ps1_cnt_2,
	datad => nx1157_a0,
	cascin => ix645_a1,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2966_a12);

ix586_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix586_a3 = ps1_cnt_11 & !ps1_cnt_13 & !ps1_cnt_12 & ps1_cnt_14

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_11,
	datab => ps1_cnt_13,
	datac => ps1_cnt_12,
	datad => ps1_cnt_14,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix586_a3);

ix597_a18_I : apex20ke_lcell 
-- Equation(s):
-- ix597_a18 = (!ps1_cnt_7 & ps1_cnt_10 & ps1_cnt_9 & !ps1_cnt_8) & CASCADE(ix586_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_7,
	datab => ps1_cnt_10,
	datac => ps1_cnt_9,
	datad => ps1_cnt_8,
	cascin => ix586_a3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix597_a18);

nx2963_a11_I : apex20ke_lcell 
-- Equation(s):
-- nx2963_a11 = (nx2964_a12 & nx2966_a12 & (PRESET_MODE_a0_a_acombout # !PRESET_MODE_a1_a_acombout)) & CASCADE(ix597_a18)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx2964_a12,
	datab => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => nx2966_a12,
	cascin => ix597_a18,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2963_a11);

ix609_a284_I : apex20ke_lcell 
-- Equation(s):
-- ix609_a284 = PRESET_MODE_a1_a_acombout & !PRESET_MODE_a0_a_acombout & !PRESET_MODE_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix609_a284);

ix590_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix590_a3 = lt1_cnt_3 & !lt1_cnt_5 & !lt1_cnt_6 & lt1_cnt_4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_3,
	datab => lt1_cnt_5,
	datac => lt1_cnt_6,
	datad => lt1_cnt_4,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix590_a3);

nx2969_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2969_a12 = (lt_inc_en & lt1_cnt_2 & lt1_cnt_0 & lt1_cnt_1) & CASCADE(ix590_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => lt_inc_en,
	datab => lt1_cnt_2,
	datac => lt1_cnt_0,
	datad => lt1_cnt_1,
	cascin => ix590_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2969_a12);

ix592_a14_I : apex20ke_lcell 
-- Equation(s):
-- ix592_a14 = nx1130_a2 & Time_Enable_dup0 & !GLOBAL(Time_Clr_acombout) & !nx803_a21

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx1130_a2,
	datab => Time_Enable_dup0,
	datac => Time_Clr_acombout,
	datad => nx803_a21,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix592_a14);

ix599_a38_I : apex20ke_lcell 
-- Equation(s):
-- ix599_a38 = (ix609_a284 & !Time_Stop_acombout & nx2969_a12 & Preset_Enable) & CASCADE(ix592_a14)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ix609_a284,
	datab => Time_Stop_acombout,
	datac => nx2969_a12,
	datad => Preset_Enable,
	cascin => ix592_a14,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix599_a38);

ix589_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix589_a3 = !lt1_cnt_13 & lt1_cnt_14 & lt1_cnt_11 & !lt1_cnt_12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_13,
	datab => lt1_cnt_14,
	datac => lt1_cnt_11,
	datad => lt1_cnt_12,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix589_a3);

ix600_a40_I : apex20ke_lcell 
-- Equation(s):
-- ix600_a40 = (!lt1_cnt_8 & lt1_cnt_9 & !lt1_cnt_7 & lt1_cnt_10) & CASCADE(ix589_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => lt1_cnt_8,
	datab => lt1_cnt_9,
	datac => lt1_cnt_7,
	datad => lt1_cnt_10,
	cascin => ix589_a3,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix600_a40);

nx2967_a11_I : apex20ke_lcell 
-- Equation(s):
-- nx2967_a11 = (ix599_a38) & CASCADE(ix600_a40)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => ix599_a38,
	cascin => ix600_a40,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2967_a11);

ix587_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix587_a3 = !ps1_cnt_13 & !ps1_cnt_11 & !ps1_cnt_12 & !ps1_cnt_14

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_13,
	datab => ps1_cnt_11,
	datac => ps1_cnt_12,
	datad => ps1_cnt_14,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix587_a3);

nx2972_a21_I : apex20ke_lcell 
-- Equation(s):
-- nx2972_a21 = (!ps1_cnt_7 & !ps1_cnt_10 & !ps1_cnt_8 & !ps1_cnt_9) & CASCADE(ix587_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_7,
	datab => ps1_cnt_10,
	datac => ps1_cnt_8,
	datad => ps1_cnt_9,
	cascin => ix587_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2972_a21);

ix573_a14_I : apex20ke_lcell 
-- Equation(s):
-- ix573_a14 = ps1_cnt_4 & !ps1_cnt_3

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_4,
	datac => ps1_cnt_3,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix573_a14);

ix753_a22_I : apex20ke_lcell 
-- Equation(s):
-- ix753_a22 = !nx1157_a0 # !ix573_a14 # !Preset_Enable # !ix576_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ix576_a13,
	datab => Preset_Enable,
	datac => ix573_a14,
	datad => nx1157_a0,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix753_a22);

ix594_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix594_a3 = PRESET_MODE_a1_a_acombout & PRESET_MODE_a2_a_acombout & !PRESET_MODE_a0_a_acombout & !Time_Stop_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a2_a_acombout,
	datac => PRESET_MODE_a0_a_acombout,
	datad => Time_Stop_acombout,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix594_a3);

nx2959_a21_I : apex20ke_lcell 
-- Equation(s):
-- nx2959_a21 = (nx1130_a2 & Time_Enable_dup0 & !GLOBAL(Time_Clr_acombout) & !nx803_a21) & CASCADE(ix594_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx1130_a2,
	datab => Time_Enable_dup0,
	datac => Time_Clr_acombout,
	datad => nx803_a21,
	cascin => ix594_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2959_a21);

ix753_a2_I : apex20ke_lcell 
-- Equation(s):
-- ix753_a2 = ix753_a22 # ps1_cnt_2 # !nx2959_a21 # !nx2972_a21

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => nx2972_a21,
	datab => ix753_a22,
	datac => ps1_cnt_2,
	datad => nx2959_a21,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix753_a2);

nx975_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx975_a10 = (!nx2963_a11 & !nx2967_a11) & CASCADE(ix753_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => nx2963_a11,
	datad => nx2967_a11,
	cascin => ix753_a2,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx975_a10);

Preset_Done_a7_I : apex20ke_lcell 
-- Equation(s):
-- Preset_Done_a7 = DFFE(!nx975_a10, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => nx975_a10,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Preset_Done_a7);

nx3414_a15_I : apex20ke_lcell 
-- Equation(s):
-- nx3414_a15 = Preset_Done_a8 & Preset_Done_a7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Preset_Done_a8,
	datad => Preset_Done_a7,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx3414_a15);

nx1700_a2_I : apex20ke_lcell 
-- Equation(s):
-- nx1700_a2 = Time_Stop_acombout # nx803_a21 # nx3414_a15 # !nx1130_a2

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Time_Stop_acombout,
	datab => nx1130_a2,
	datac => nx803_a21,
	datad => nx3414_a15,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1700_a2);

reg_Time_Enable_a0 : apex20ke_lcell 
-- Equation(s):
-- Time_Enable_dup0 = DFFE(!nx1149_a3, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1700_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => nx1149_a3,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1700_a2,
	devclrn => devclrn,
	devpor => devpor,
	regout => Time_Enable_dup0);

nx1694_a1_I : apex20ke_lcell 
-- Equation(s):
-- nx1694_a1 = Time_Start_acombout # Time_Enable_dup0 # !nx1140_a1

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Time_Start_acombout,
	datac => nx1140_a1,
	datad => Time_Enable_dup0,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1694_a1);

reg_time_start_latch_a0 : apex20ke_lcell 
-- Equation(s):
-- time_start_latch = DFFE(PRESET_MODE_a1_a_acombout & PRESET_MODE_a0_a_acombout & Time_Start_acombout & PRESET_MODE_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1694_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PRESET_MODE_a1_a_acombout,
	datab => PRESET_MODE_a0_a_acombout,
	datac => Time_Start_acombout,
	datad => PRESET_MODE_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1694_a1,
	devclrn => devclrn,
	devpor => devpor,
	regout => time_start_latch);

ix553_a20_I : apex20ke_lcell 
-- Equation(s):
-- ix553_a20 = !PA_Reset_Vec_0 & time_start_latch & PA_Reset_Vec_1

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PA_Reset_Vec_0,
	datac => time_start_latch,
	datad => PA_Reset_Vec_1,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix553_a20);

nx2928_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2928_a10 = (!nx2938_a20 & !nx2936_a20) & CASCADE(nx1149_a68)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => nx2938_a20,
	datad => nx2936_a20,
	cascin => nx1149_a68,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2928_a10);

nx2941_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2941_a12 = (ps1_cnt_1 & !ps1_cnt_2 & ps1_cnt_0) & CASCADE(nx1142_a26)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => ps1_cnt_1,
	datac => ps1_cnt_2,
	datad => ps1_cnt_0,
	cascin => nx1142_a26,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2941_a12);

ix574_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix574_a3 = !ps1_cnt_11 & !ps1_cnt_13 & !ps1_cnt_12 & !ps1_cnt_14

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_11,
	datab => ps1_cnt_13,
	datac => ps1_cnt_12,
	datad => ps1_cnt_14,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix574_a3);

nx3014_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx3014_a12 = (!ps1_cnt_7 & !ps1_cnt_10 & !ps1_cnt_9 & !ps1_cnt_8) & CASCADE(ix574_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => ps1_cnt_7,
	datab => ps1_cnt_10,
	datac => ps1_cnt_9,
	datad => ps1_cnt_8,
	cascin => ix574_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx3014_a12);

nx2938_a20_I : apex20ke_lcell 
-- Equation(s):
-- nx2938_a20 = (nx2941_a12 & nx3014_a12 & ps1_cnt_4 & !ps1_cnt_3) & CASCADE(nx1157_a18)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx2941_a12,
	datab => nx3014_a12,
	datac => ps1_cnt_4,
	datad => ps1_cnt_3,
	cascin => nx1157_a18,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2938_a20);

CT_CEN_a1_I : apex20ke_lcell 
-- Equation(s):
-- CT_CEN_a1 = Time_Enable_dup0 & (nx2938_a20 # nx2936_a20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx2938_a20,
	datac => Time_Enable_dup0,
	datad => nx2936_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => CT_CEN_a1);

GMode_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_GMode,
	combout => GMode_acombout);

nx1780_a1_I : apex20ke_lcell 
-- Equation(s):
-- nx1780_a1 = GMode_acombout # nx1138_a12 & nx1134_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx1138_a12,
	datac => GMode_acombout,
	datad => nx1134_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx1780_a1);

PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91 = !PAMR_CPS_aCPS_STATE_a2_a & (PAMR_CPS_aCPS_STATE_a1_a # !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4545",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aCPS_STATE_a1_a,
	datac => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91);

PAMR_CPS_aCPS_STATE_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aCPS_STATE_a1_a = DFFE(PAMR_CPS_aCPS_STATE_a3_a & (PAMR_CPS_aCPS_STATE_a1_a $ !PAMR_CPS_aCPS_STATE_a0_a) # !PAMR_CPS_aCPS_STATE_a3_a & PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91 & (PAMR_CPS_aCPS_STATE_a1_a $ !PAMR_CPS_aCPS_STATE_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A584",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a1_a,
	datab => PAMR_CPS_aCPS_STATE_a3_a,
	datac => PAMR_CPS_aCPS_STATE_a0_a,
	datad => PAMR_CPS_aMux_90_rtl_4_a_a00009_a6_a91,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aCPS_STATE_a1_a);

PAMR_CPS_aCPS_STATE_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aCPS_STATE_a3_a = DFFE(PAMR_CPS_aCPS_STATE_a3_a $ (!PAMR_CPS_aCPS_STATE_a2_a & !PAMR_CPS_aCPS_STATE_a0_a & PAMR_CPS_aCPS_STATE_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E1F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aCPS_STATE_a0_a,
	datac => PAMR_CPS_aCPS_STATE_a3_a,
	datad => PAMR_CPS_aCPS_STATE_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aCPS_STATE_a3_a);

rtl_a497_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a497 = PAMR_CPS_aCPS_STATE_a3_a & (PAMR_CPS_aCPS_STATE_a2_a # !PAMR_CPS_aCPS_STATE_a1_a) # !PAMR_CPS_aCPS_STATE_a3_a & !PAMR_CPS_aCPS_STATE_a2_a & (PAMR_CPS_aCPS_STATE_a1_a # !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9A9B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a3_a,
	datab => PAMR_CPS_aCPS_STATE_a2_a,
	datac => PAMR_CPS_aCPS_STATE_a1_a,
	datad => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a497);

PAMR_CPS_aCPS_STATE_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aCPS_STATE_a0_a = DFFE(!PAMR_CPS_aCPS_STATE_a0_a & rtl_a497, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => PAMR_CPS_aCPS_STATE_a0_a,
	datad => rtl_a497,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aCPS_STATE_a0_a);

PAMR_CPS_aCPS_STATE_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aCPS_STATE_a2_a = DFFE(PAMR_CPS_aCPS_STATE_a2_a & PAMR_CPS_aCPS_STATE_a3_a & (PAMR_CPS_aCPS_STATE_a0_a # !PAMR_CPS_aCPS_STATE_a1_a) # !PAMR_CPS_aCPS_STATE_a2_a & !PAMR_CPS_aCPS_STATE_a0_a & PAMR_CPS_aCPS_STATE_a1_a & !PAMR_CPS_aCPS_STATE_a3_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8A10",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aCPS_STATE_a0_a,
	datac => PAMR_CPS_aCPS_STATE_a1_a,
	datad => PAMR_CPS_aCPS_STATE_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aCPS_STATE_a2_a);

PAMR_CPS_areduce_nor_17_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_areduce_nor_17 = PAMR_CPS_aCPS_STATE_a2_a # PAMR_CPS_aCPS_STATE_a1_a # PAMR_CPS_aCPS_STATE_a3_a # PAMR_CPS_aCPS_STATE_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aCPS_STATE_a1_a,
	datac => PAMR_CPS_aCPS_STATE_a3_a,
	datad => PAMR_CPS_aCPS_STATE_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_areduce_nor_17);

PAMR_CPS_areduce_nor_6_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_areduce_nor_6 = PA_Reset_Vec_0 & !PA_Reset_Vec_1

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PA_Reset_Vec_0,
	datad => PA_Reset_Vec_1,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_areduce_nor_6);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_areduce_nor_6 $ PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

PAMR_CPS_areduce_nor_7_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_areduce_nor_7 = !PAMR_CPS_aCPS_STATE_a2_a & PAMR_CPS_aCPS_STATE_a1_a & PAMR_CPS_aCPS_STATE_a3_a & !PAMR_CPS_aCPS_STATE_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aCPS_STATE_a1_a,
	datac => PAMR_CPS_aCPS_STATE_a3_a,
	datad => PAMR_CPS_aCPS_STATE_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_areduce_nor_7);

PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a = DFFE(PAMR_CPS_areduce_nor_7 $ PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	cout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a = DFFE(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ (PAMR_CPS_areduce_nor_7 & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_7,
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cin => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a = DFFE(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ (PAMR_CPS_areduce_nor_7 & !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_7,
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cin => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a = DFFE(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a $ (PAMR_CPS_areduce_nor_7 & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_7,
	datad => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	cin => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a);

PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a = PAMR_CPS_aCPS_STATE_a0_a $ !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a
-- PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a = CARRY(PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a # !PAMR_CPS_aCPS_STATE_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "99DD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a0_a,
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	cout => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a);

PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121 = !PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aCPS_RAM_asram_aq_a0_a_a121);

PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a = PAMR_CPS_aCPS_STATE_a1_a $ PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a
-- PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a = CARRY(PAMR_CPS_aCPS_STATE_a1_a & !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & !PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !PAMR_CPS_aCPS_STATE_a1_a & (!PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a1_a,
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cin => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a,
	cout => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a);

PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a = PAMR_CPS_aCPS_STATE_a2_a $ PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a
-- PAMR_CPS_aadd_13_aadder_aresult_node_acout_a3_a = CARRY(PAMR_CPS_aCPS_STATE_a2_a & PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a # !PAMR_CPS_aCPS_STATE_a2_a & (PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a # !PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_STATE_a2_a,
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cin => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a,
	cout => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a3_a);

PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a = PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a $ PAMR_CPS_aadd_13_aadder_aresult_node_acout_a3_a $ PAMR_CPS_aCPS_STATE_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PAMR_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	datad => PAMR_CPS_aCPS_STATE_a3_a,
	cin => PAMR_CPS_aadd_13_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a9_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a8_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a7_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a6_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a5_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a4_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a3_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a2_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a1_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a0_a);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a = PAMR_CPS_acps_a0_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a0_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a = CARRY(PAMR_CPS_acps_a0_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a0_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a);

PAMR_CPS_acps_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a0_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a0_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_areduce_nor_17,
	datac => PAMR_CPS_acps_a0_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a0_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a = PAMR_CPS_acps_a1_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a1_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a = CARRY(PAMR_CPS_acps_a1_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a1_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !PAMR_CPS_acps_a1_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !PAMR_CPS_aCPS_RAM_asram_aq_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a1_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a1_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a);

PAMR_CPS_acps_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a1_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a1_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_acps_a1_a_areg0,
	datac => PAMR_CPS_areduce_nor_17,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a1_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a = PAMR_CPS_acps_a2_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a2_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a = CARRY(PAMR_CPS_acps_a2_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a2_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a) # !PAMR_CPS_acps_a2_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a2_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a2_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a2_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a);

PAMR_CPS_acps_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a2_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_acps_a2_a_areg0,
	datac => PAMR_CPS_areduce_nor_17,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a2_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a = PAMR_CPS_acps_a3_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a3_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a = CARRY(PAMR_CPS_acps_a3_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a3_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !PAMR_CPS_acps_a3_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !PAMR_CPS_aCPS_RAM_asram_aq_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a3_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a3_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a);

PAMR_CPS_acps_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a3_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a3_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a3_a_areg0,
	datab => PAMR_CPS_areduce_nor_17,
	datac => IN_CPS_ams100tc_Del,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a3_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a = PAMR_CPS_acps_a4_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a4_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a = CARRY(PAMR_CPS_acps_a4_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a4_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a) # !PAMR_CPS_acps_a4_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a4_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a4_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a4_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a);

PAMR_CPS_acps_a4_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a4_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a4_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_acps_a4_a_areg0,
	datac => PAMR_CPS_areduce_nor_17,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a4_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a = PAMR_CPS_aCPS_RAM_asram_aq_a5_a $ PAMR_CPS_acps_a5_a_areg0 $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a = CARRY(PAMR_CPS_aCPS_RAM_asram_aq_a5_a & !PAMR_CPS_acps_a5_a_areg0 & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !PAMR_CPS_aCPS_RAM_asram_aq_a5_a & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !PAMR_CPS_acps_a5_a_areg0))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_RAM_asram_aq_a5_a,
	datab => PAMR_CPS_acps_a5_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a);

PAMR_CPS_acps_a5_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a5_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a5_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a5_a_areg0,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	datad => PAMR_CPS_areduce_nor_17,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a5_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a = PAMR_CPS_aCPS_RAM_asram_aq_a6_a $ PAMR_CPS_acps_a6_a_areg0 $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a = CARRY(PAMR_CPS_aCPS_RAM_asram_aq_a6_a & (PAMR_CPS_acps_a6_a_areg0 # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a) # !PAMR_CPS_aCPS_RAM_asram_aq_a6_a & PAMR_CPS_acps_a6_a_areg0 & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_RAM_asram_aq_a6_a,
	datab => PAMR_CPS_acps_a6_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a);

PAMR_CPS_acps_a6_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a6_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a6_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_acps_a6_a_areg0,
	datac => PAMR_CPS_areduce_nor_17,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a6_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a = PAMR_CPS_aCPS_RAM_asram_aq_a7_a $ PAMR_CPS_acps_a7_a_areg0 $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a = CARRY(PAMR_CPS_aCPS_RAM_asram_aq_a7_a & !PAMR_CPS_acps_a7_a_areg0 & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !PAMR_CPS_aCPS_RAM_asram_aq_a7_a & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !PAMR_CPS_acps_a7_a_areg0))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_aCPS_RAM_asram_aq_a7_a,
	datab => PAMR_CPS_acps_a7_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a);

PAMR_CPS_acps_a7_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a7_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a7_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "E2C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a7_a_areg0,
	datab => PAMR_CPS_areduce_nor_17,
	datac => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	datad => IN_CPS_ams100tc_Del,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a7_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a = PAMR_CPS_acps_a8_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a8_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a = CARRY(PAMR_CPS_acps_a8_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a8_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a) # !PAMR_CPS_acps_a8_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a8_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a8_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a8_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a);

PAMR_CPS_acps_a8_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a8_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a8_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => PAMR_CPS_acps_a8_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a8_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a = PAMR_CPS_acps_a9_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a9_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a = CARRY(PAMR_CPS_acps_a9_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a9_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !PAMR_CPS_acps_a9_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !PAMR_CPS_aCPS_RAM_asram_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a9_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a9_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a);

PAMR_CPS_acps_a9_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a9_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a9_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a9_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a9_a_areg0);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a10_a);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a = PAMR_CPS_acps_a10_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a10_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a = CARRY(PAMR_CPS_acps_a10_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a10_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a) # !PAMR_CPS_acps_a10_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a10_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a10_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a10_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a);

PAMR_CPS_acps_a10_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a10_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a10_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a10_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a10_a_areg0);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a13_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a12_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a11_a);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a = PAMR_CPS_acps_a11_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a11_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a = CARRY(PAMR_CPS_acps_a11_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a11_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !PAMR_CPS_acps_a11_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !PAMR_CPS_aCPS_RAM_asram_aq_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a11_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a11_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a);

PAMR_CPS_acps_a11_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a11_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a11_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => PAMR_CPS_acps_a11_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a11_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a = PAMR_CPS_acps_a12_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a12_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a = CARRY(PAMR_CPS_acps_a12_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a12_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a) # !PAMR_CPS_acps_a12_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a12_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a12_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a12_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a);

PAMR_CPS_acps_a12_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a12_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a12_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a12_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a12_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a = PAMR_CPS_acps_a13_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a13_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a = CARRY(PAMR_CPS_acps_a13_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a13_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !PAMR_CPS_acps_a13_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !PAMR_CPS_aCPS_RAM_asram_aq_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a13_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a13_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a);

PAMR_CPS_acps_a13_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a13_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a13_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a13_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a13_a_areg0);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a $ (PAMR_CPS_areduce_nor_6 & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a & !PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a14_a);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a = PAMR_CPS_acps_a14_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a14_a $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a = CARRY(PAMR_CPS_acps_a14_a_areg0 & (PAMR_CPS_aCPS_RAM_asram_aq_a14_a # !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a) # !PAMR_CPS_acps_a14_a_areg0 & PAMR_CPS_aCPS_RAM_asram_aq_a14_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a14_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a14_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a);

PAMR_CPS_acps_a14_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a14_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a14_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => PAMR_CPS_acps_a14_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a14_a_areg0);

ix582_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix582_a3 = !PAMR_CPS_acps_a13_a_areg0 & !PAMR_CPS_acps_a11_a_areg0 & !PAMR_CPS_acps_a12_a_areg0 & !PAMR_CPS_acps_a14_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a13_a_areg0,
	datab => PAMR_CPS_acps_a11_a_areg0,
	datac => PAMR_CPS_acps_a12_a_areg0,
	datad => PAMR_CPS_acps_a14_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix582_a3);

nx2976_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2976_a12 = (!PAMR_CPS_acps_a9_a_areg0 & !PAMR_CPS_acps_a8_a_areg0 & !PAMR_CPS_acps_a10_a_areg0 & !PAMR_CPS_acps_a7_a_areg0) & CASCADE(ix582_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a9_a_areg0,
	datab => PAMR_CPS_acps_a8_a_areg0,
	datac => PAMR_CPS_acps_a10_a_areg0,
	datad => PAMR_CPS_acps_a7_a_areg0,
	cascin => ix582_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2976_a12);

ix583_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix583_a3 = !PAMR_CPS_acps_a6_a_areg0 & !PAMR_CPS_acps_a4_a_areg0 & !PAMR_CPS_acps_a5_a_areg0 & !PAMR_CPS_acps_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a6_a_areg0,
	datab => PAMR_CPS_acps_a4_a_areg0,
	datac => PAMR_CPS_acps_a5_a_areg0,
	datad => PAMR_CPS_acps_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix583_a3);

nx2977_a12_I : apex20ke_lcell 
-- Equation(s):
-- nx2977_a12 = (!GMode_acombout & !PAMR_CPS_acps_a1_a_areg0 & !PAMR_CPS_acps_a2_a_areg0 & !PAMR_CPS_acps_a0_a_areg0) & CASCADE(ix583_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => GMode_acombout,
	datab => PAMR_CPS_acps_a1_a_areg0,
	datac => PAMR_CPS_acps_a2_a_areg0,
	datad => PAMR_CPS_acps_a0_a_areg0,
	cascin => ix583_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2977_a12);

PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a $ (PAMR_CPS_areduce_nor_6 & PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C6C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_6,
	datab => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	cin => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a);

PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:PAMR_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress,
	raddr => ww_PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => PAMR_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => PAMR_CPS_aCPS_RAM_asram_aq_a15_a);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a = PAMR_CPS_acps_a15_a_areg0 $ PAMR_CPS_aCPS_RAM_asram_aq_a15_a $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a = CARRY(PAMR_CPS_acps_a15_a_areg0 & !PAMR_CPS_aCPS_RAM_asram_aq_a15_a & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !PAMR_CPS_acps_a15_a_areg0 & (!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !PAMR_CPS_aCPS_RAM_asram_aq_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a15_a_areg0,
	datab => PAMR_CPS_aCPS_RAM_asram_aq_a15_a,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a);

PAMR_CPS_acps_a15_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a15_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a15_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_acps_a15_a_areg0,
	datac => PAMR_CPS_areduce_nor_17,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a15_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a = PAMR_CPS_acps_a16_a_areg0 $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a = CARRY(PAMR_CPS_acps_a16_a_areg0 & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a16_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a);

PAMR_CPS_acps_a16_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a16_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a16_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a16_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a16_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a = PAMR_CPS_acps_a17_a_areg0 $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a = CARRY(!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a # !PAMR_CPS_acps_a17_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a17_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a);

PAMR_CPS_acps_a17_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a17_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a17_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a17_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a17_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a = PAMR_CPS_acps_a18_a_areg0 $ !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a = CARRY(PAMR_CPS_acps_a18_a_areg0 & !PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a18_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a);

PAMR_CPS_acps_a18_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a18_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a # !PAMR_CPS_areduce_nor_17 & PAMR_CPS_acps_a18_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => PAMR_CPS_acps_a18_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a18_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a = PAMR_CPS_acps_a19_a_areg0 $ PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a
-- PAMR_CPS_aadd_60_aadder_aresult_node_acout_a19_a = CARRY(!PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a # !PAMR_CPS_acps_a19_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a19_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	cout => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a19_a);

PAMR_CPS_acps_a19_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a19_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a19_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => PAMR_CPS_acps_a19_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a19_a_areg0);

PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a = PAMR_CPS_aadd_60_aadder_aresult_node_acout_a19_a $ !PAMR_CPS_acps_a20_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => PAMR_CPS_acps_a20_a_areg0,
	cin => PAMR_CPS_aadd_60_aadder_aresult_node_acout_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a);

PAMR_CPS_acps_a20_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- PAMR_CPS_acps_a20_a_areg0 = DFFE(PAMR_CPS_areduce_nor_17 & PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a # !PAMR_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & PAMR_CPS_acps_a20_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => PAMR_CPS_areduce_nor_17,
	datac => PAMR_CPS_acps_a20_a_areg0,
	datad => PAMR_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PAMR_CPS_acps_a20_a_areg0);

ix581_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix581_a3 = !PAMR_CPS_acps_a16_a_areg0 & !PAMR_CPS_acps_a17_a_areg0 & !PAMR_CPS_acps_a15_a_areg0 & !PAMR_CPS_acps_a18_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PAMR_CPS_acps_a16_a_areg0,
	datab => PAMR_CPS_acps_a17_a_areg0,
	datac => PAMR_CPS_acps_a15_a_areg0,
	datad => PAMR_CPS_acps_a18_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix581_a3);

nx2931_a10_I : apex20ke_lcell 
-- Equation(s):
-- nx2931_a10 = (nx2976_a12 & nx2977_a12 & !PAMR_CPS_acps_a19_a_areg0 & !PAMR_CPS_acps_a20_a_areg0) & CASCADE(ix581_a3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx2976_a12,
	datab => nx2977_a12,
	datac => PAMR_CPS_acps_a19_a_areg0,
	datad => PAMR_CPS_acps_a20_a_areg0,
	cascin => ix581_a3,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx2931_a10);

Sat_Cnt_ix31 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_0 = DFFE(nx2931_a10 & !Sat_Cnt_0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx7 = CARRY(Sat_Cnt_0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_0,
	cout => Sat_Cnt_nx7);

Sat_Cnt_ix28 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_1 = DFFE(nx2931_a10 & Sat_Cnt_1 $ Sat_Cnt_nx7, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx13 = CARRY(!Sat_Cnt_nx7 # !Sat_Cnt_1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_1,
	cin => Sat_Cnt_nx7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_1,
	cout => Sat_Cnt_nx13);

Sat_Cnt_ix25 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_2 = DFFE(nx2931_a10 & Sat_Cnt_2 $ !Sat_Cnt_nx13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx19 = CARRY(Sat_Cnt_2 & !Sat_Cnt_nx13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_2,
	cin => Sat_Cnt_nx13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_2,
	cout => Sat_Cnt_nx19);

Sat_Cnt_ix22 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_3 = DFFE(nx2931_a10 & Sat_Cnt_3 $ Sat_Cnt_nx19, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx25 = CARRY(!Sat_Cnt_nx19 # !Sat_Cnt_3)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_3,
	cin => Sat_Cnt_nx19,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_3,
	cout => Sat_Cnt_nx25);

Sat_Cnt_ix19 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_4 = DFFE(nx2931_a10 & Sat_Cnt_4 $ !Sat_Cnt_nx25, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx31 = CARRY(Sat_Cnt_4 & !Sat_Cnt_nx25)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_4,
	cin => Sat_Cnt_nx25,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_4,
	cout => Sat_Cnt_nx31);

ix650_a24_I : apex20ke_lcell 
-- Equation(s):
-- ix650_a24 = Sat_Cnt_0 & Sat_Cnt_1

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0A0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Sat_Cnt_0,
	datac => Sat_Cnt_1,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix650_a24);

ix650_a34_I : apex20ke_lcell 
-- Equation(s):
-- ix650_a34 = (!Sat_Cnt_3 & !Sat_Cnt_2 & Sat_Cnt_4 & !GMode_acombout) & CASCADE(ix650_a24)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0010",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Sat_Cnt_3,
	datab => Sat_Cnt_2,
	datac => Sat_Cnt_4,
	datad => GMode_acombout,
	cascin => ix650_a24,
	devclrn => devclrn,
	devpor => devpor,
	combout => ix650_a34);

Sat_Cnt_ix16 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_5 = DFFE(nx2931_a10 & Sat_Cnt_5 $ Sat_Cnt_nx31, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx38 = CARRY(!Sat_Cnt_nx31 # !Sat_Cnt_5)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_5,
	cin => Sat_Cnt_nx31,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_5,
	cout => Sat_Cnt_nx38);

Sat_Cnt_ix13 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_6 = DFFE(nx2931_a10 & Sat_Cnt_6 $ !Sat_Cnt_nx38, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx43 = CARRY(Sat_Cnt_6 & !Sat_Cnt_nx38)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_6,
	cin => Sat_Cnt_nx38,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_6,
	cout => Sat_Cnt_nx43);

Sat_Cnt_ix10 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_7 = DFFE(nx2931_a10 & Sat_Cnt_7 $ Sat_Cnt_nx43, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)
-- Sat_Cnt_nx47 = CARRY(!Sat_Cnt_nx43 # !Sat_Cnt_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Sat_Cnt_7,
	cin => Sat_Cnt_nx43,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_7,
	cout => Sat_Cnt_nx47);

Sat_Cnt_ix7 : apex20ke_lcell 
-- Equation(s):
-- Sat_Cnt_8 = DFFE(nx2931_a10 & Sat_Cnt_nx47 $ !Sat_Cnt_8, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Sat_Cnt_8,
	cin => Sat_Cnt_nx47,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	sclr => NOT_nx2931_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => Sat_Cnt_8);

ix584_a3_I : apex20ke_lcell 
-- Equation(s):
-- ix584_a3 = Sat_Cnt_6 & Sat_Cnt_5 & Sat_Cnt_7 & Sat_Cnt_8

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Sat_Cnt_6,
	datab => Sat_Cnt_5,
	datac => Sat_Cnt_7,
	datad => Sat_Cnt_8,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix584_a3);

reg_Det_Sat_a0 : apex20ke_lcell 
-- Equation(s):
-- Det_Sat_dup0 = DFFE((ix650_a34) & CASCADE(ix584_a3), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx1780_a1)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => ix650_a34,
	cascin => ix584_a3,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx1780_a1,
	devclrn => devclrn,
	devpor => devpor,
	regout => Det_Sat_dup0);

hc_rate_ibuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(20),
	combout => hc_rate_a20_a_acombout);

hc_rate_ibuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(19),
	combout => hc_rate_a19_a_acombout);

hc_rate_ibuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(18),
	combout => hc_rate_a18_a_acombout);

hc_rate_ibuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(17),
	combout => hc_rate_a17_a_acombout);

hc_rate_ibuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(16),
	combout => hc_rate_a16_a_acombout);

hc_rate_ibuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(15),
	combout => hc_rate_a15_a_acombout);

hc_rate_ibuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(14),
	combout => hc_rate_a14_a_acombout);

hc_rate_ibuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(13),
	combout => hc_rate_a13_a_acombout);

hc_rate_ibuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(12),
	combout => hc_rate_a12_a_acombout);

hc_rate_ibuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(11),
	combout => hc_rate_a11_a_acombout);

hc_rate_ibuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(10),
	combout => hc_rate_a10_a_acombout);

hc_rate_ibuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(9),
	combout => hc_rate_a9_a_acombout);

hc_rate_ibuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(8),
	combout => hc_rate_a8_a_acombout);

hc_rate_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(7),
	combout => hc_rate_a7_a_acombout);

hc_rate_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(6),
	combout => hc_rate_a6_a_acombout);

hc_rate_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(5),
	combout => hc_rate_a5_a_acombout);

hc_rate_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(4),
	combout => hc_rate_a4_a_acombout);

hc_rate_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(3),
	combout => hc_rate_a3_a_acombout);

hc_rate_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(2),
	combout => hc_rate_a2_a_acombout);

hc_rate_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(1),
	combout => hc_rate_a1_a_acombout);

hc_rate_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(0),
	combout => hc_rate_a0_a_acombout);

modgen_gt_2_ix47 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx48 = CARRY(!hc_rate_a0_a_acombout & IN_CPS_acps_a0_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a0_a_acombout,
	datab => IN_CPS_acps_a0_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx48);

modgen_gt_2_ix49 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx50 = CARRY(hc_rate_a1_a_acombout & (!modgen_gt_2_nx48 # !IN_CPS_acps_a1_a_areg0) # !hc_rate_a1_a_acombout & !IN_CPS_acps_a1_a_areg0 & !modgen_gt_2_nx48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a1_a_acombout,
	datab => IN_CPS_acps_a1_a_areg0,
	cin => modgen_gt_2_nx48,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx50);

modgen_gt_2_ix51 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx52 = CARRY(hc_rate_a2_a_acombout & IN_CPS_acps_a2_a_areg0 & !modgen_gt_2_nx50 # !hc_rate_a2_a_acombout & (IN_CPS_acps_a2_a_areg0 # !modgen_gt_2_nx50))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a2_a_acombout,
	datab => IN_CPS_acps_a2_a_areg0,
	cin => modgen_gt_2_nx50,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx52);

modgen_gt_2_ix53 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx54 = CARRY(hc_rate_a3_a_acombout & (!modgen_gt_2_nx52 # !IN_CPS_acps_a3_a_areg0) # !hc_rate_a3_a_acombout & !IN_CPS_acps_a3_a_areg0 & !modgen_gt_2_nx52)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a3_a_acombout,
	datab => IN_CPS_acps_a3_a_areg0,
	cin => modgen_gt_2_nx52,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx54);

modgen_gt_2_ix55 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx56 = CARRY(hc_rate_a4_a_acombout & IN_CPS_acps_a4_a_areg0 & !modgen_gt_2_nx54 # !hc_rate_a4_a_acombout & (IN_CPS_acps_a4_a_areg0 # !modgen_gt_2_nx54))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a4_a_acombout,
	datab => IN_CPS_acps_a4_a_areg0,
	cin => modgen_gt_2_nx54,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx56);

modgen_gt_2_ix57 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx58 = CARRY(hc_rate_a5_a_acombout & (!modgen_gt_2_nx56 # !IN_CPS_acps_a5_a_areg0) # !hc_rate_a5_a_acombout & !IN_CPS_acps_a5_a_areg0 & !modgen_gt_2_nx56)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a5_a_acombout,
	datab => IN_CPS_acps_a5_a_areg0,
	cin => modgen_gt_2_nx56,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx58);

modgen_gt_2_ix59 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx60 = CARRY(IN_CPS_acps_a6_a_areg0 & (!modgen_gt_2_nx58 # !hc_rate_a6_a_acombout) # !IN_CPS_acps_a6_a_areg0 & !hc_rate_a6_a_acombout & !modgen_gt_2_nx58)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a6_a_areg0,
	datab => hc_rate_a6_a_acombout,
	cin => modgen_gt_2_nx58,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx60);

modgen_gt_2_ix61 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx62 = CARRY(IN_CPS_acps_a7_a_areg0 & hc_rate_a7_a_acombout & !modgen_gt_2_nx60 # !IN_CPS_acps_a7_a_areg0 & (hc_rate_a7_a_acombout # !modgen_gt_2_nx60))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a7_a_areg0,
	datab => hc_rate_a7_a_acombout,
	cin => modgen_gt_2_nx60,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx62);

modgen_gt_2_ix63 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx64 = CARRY(hc_rate_a8_a_acombout & IN_CPS_acps_a8_a_areg0 & !modgen_gt_2_nx62 # !hc_rate_a8_a_acombout & (IN_CPS_acps_a8_a_areg0 # !modgen_gt_2_nx62))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a8_a_acombout,
	datab => IN_CPS_acps_a8_a_areg0,
	cin => modgen_gt_2_nx62,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx64);

modgen_gt_2_ix65 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx66 = CARRY(hc_rate_a9_a_acombout & (!modgen_gt_2_nx64 # !IN_CPS_acps_a9_a_areg0) # !hc_rate_a9_a_acombout & !IN_CPS_acps_a9_a_areg0 & !modgen_gt_2_nx64)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a9_a_acombout,
	datab => IN_CPS_acps_a9_a_areg0,
	cin => modgen_gt_2_nx64,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx66);

modgen_gt_2_ix67 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx68 = CARRY(IN_CPS_acps_a10_a_areg0 & (!modgen_gt_2_nx66 # !hc_rate_a10_a_acombout) # !IN_CPS_acps_a10_a_areg0 & !hc_rate_a10_a_acombout & !modgen_gt_2_nx66)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a10_a_areg0,
	datab => hc_rate_a10_a_acombout,
	cin => modgen_gt_2_nx66,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx68);

modgen_gt_2_ix69 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx70 = CARRY(hc_rate_a11_a_acombout & (!modgen_gt_2_nx68 # !IN_CPS_acps_a11_a_areg0) # !hc_rate_a11_a_acombout & !IN_CPS_acps_a11_a_areg0 & !modgen_gt_2_nx68)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a11_a_acombout,
	datab => IN_CPS_acps_a11_a_areg0,
	cin => modgen_gt_2_nx68,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx70);

modgen_gt_2_ix71 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx72 = CARRY(IN_CPS_acps_a12_a_areg0 & (!modgen_gt_2_nx70 # !hc_rate_a12_a_acombout) # !IN_CPS_acps_a12_a_areg0 & !hc_rate_a12_a_acombout & !modgen_gt_2_nx70)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a12_a_areg0,
	datab => hc_rate_a12_a_acombout,
	cin => modgen_gt_2_nx70,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx72);

modgen_gt_2_ix73 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx74 = CARRY(hc_rate_a13_a_acombout & (!modgen_gt_2_nx72 # !IN_CPS_acps_a13_a_areg0) # !hc_rate_a13_a_acombout & !IN_CPS_acps_a13_a_areg0 & !modgen_gt_2_nx72)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a13_a_acombout,
	datab => IN_CPS_acps_a13_a_areg0,
	cin => modgen_gt_2_nx72,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx74);

modgen_gt_2_ix75 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx76 = CARRY(IN_CPS_acps_a14_a_areg0 & (!modgen_gt_2_nx74 # !hc_rate_a14_a_acombout) # !IN_CPS_acps_a14_a_areg0 & !hc_rate_a14_a_acombout & !modgen_gt_2_nx74)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a14_a_areg0,
	datab => hc_rate_a14_a_acombout,
	cin => modgen_gt_2_nx74,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx76);

modgen_gt_2_ix77 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx78 = CARRY(hc_rate_a15_a_acombout & (!modgen_gt_2_nx76 # !IN_CPS_acps_a15_a_areg0) # !hc_rate_a15_a_acombout & !IN_CPS_acps_a15_a_areg0 & !modgen_gt_2_nx76)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a15_a_acombout,
	datab => IN_CPS_acps_a15_a_areg0,
	cin => modgen_gt_2_nx76,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx78);

modgen_gt_2_ix79 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx80 = CARRY(hc_rate_a16_a_acombout & IN_CPS_acps_a16_a_areg0 & !modgen_gt_2_nx78 # !hc_rate_a16_a_acombout & (IN_CPS_acps_a16_a_areg0 # !modgen_gt_2_nx78))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a16_a_acombout,
	datab => IN_CPS_acps_a16_a_areg0,
	cin => modgen_gt_2_nx78,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx80);

modgen_gt_2_ix81 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx82 = CARRY(hc_rate_a17_a_acombout & (!modgen_gt_2_nx80 # !IN_CPS_acps_a17_a_areg0) # !hc_rate_a17_a_acombout & !IN_CPS_acps_a17_a_areg0 & !modgen_gt_2_nx80)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a17_a_acombout,
	datab => IN_CPS_acps_a17_a_areg0,
	cin => modgen_gt_2_nx80,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx82);

modgen_gt_2_ix83 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx84 = CARRY(IN_CPS_acps_a18_a_areg0 & (!modgen_gt_2_nx82 # !hc_rate_a18_a_acombout) # !IN_CPS_acps_a18_a_areg0 & !hc_rate_a18_a_acombout & !modgen_gt_2_nx82)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_acps_a18_a_areg0,
	datab => hc_rate_a18_a_acombout,
	cin => modgen_gt_2_nx82,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx84);

modgen_gt_2_ix85 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_2_nx86 = CARRY(hc_rate_a19_a_acombout & (!modgen_gt_2_nx84 # !IN_CPS_acps_a19_a_areg0) # !hc_rate_a19_a_acombout & !IN_CPS_acps_a19_a_areg0 & !modgen_gt_2_nx84)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a19_a_acombout,
	datab => IN_CPS_acps_a19_a_areg0,
	cin => modgen_gt_2_nx84,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_2_nx86);

modgen_gt_2_ix87 : apex20ke_lcell 
-- Equation(s):
-- hc_cps_tc = hc_rate_a20_a_acombout & !modgen_gt_2_nx86 & IN_CPS_acps_a20_a_areg0 # !hc_rate_a20_a_acombout & (IN_CPS_acps_a20_a_areg0 # !modgen_gt_2_nx86)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5F05",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a20_a_acombout,
	datad => IN_CPS_acps_a20_a_areg0,
	cin => modgen_gt_2_nx86,
	devclrn => devclrn,
	devpor => devpor,
	combout => hc_cps_tc);

hc_threshold_ibuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(15),
	combout => hc_threshold_a15_a_acombout);

hc_cnt_cen_a10_I : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cen_a10 = (nx1134_a12 & hc_cps_tc) & CASCADE(nx1138_a13)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => nx1134_a12,
	datad => hc_cps_tc,
	cascin => nx1138_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => hc_cnt_cen_a10);

ix568_a2_I : apex20ke_lcell 
-- Equation(s):
-- ix568_a2 = !ms1_cnt_13 & !ms1_cnt_12 & ms1_cnt_14

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0500",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => ms1_cnt_13,
	datac => ms1_cnt_12,
	datad => ms1_cnt_14,
	devclrn => devclrn,
	devpor => devpor,
	cascout => ix568_a2);

hc_cnt_clr_a10_I : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_clr_a10 = (nx1138_a12 & ix546_a24 & !hc_cps_tc & nx1159_a13) & CASCADE(ix568_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx1138_a12,
	datab => ix546_a24,
	datac => hc_cps_tc,
	datad => nx1159_a13,
	cascin => ix568_a2,
	devclrn => devclrn,
	devpor => devpor,
	combout => hc_cnt_clr_a10);

hc_cnt_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a0_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cen_a10 $ hc_cnt_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a10,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a1_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a1_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a2_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a2_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a2_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a3_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a3_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a4_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a4_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a4_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a5_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a5_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a6_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a6_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a6_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a7_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a7_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a8_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a8_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a10,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a9_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a9_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a10,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a10_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a10_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a10_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a11_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a11_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a12_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a12_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a12_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a13_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a13_a $ (hc_cnt_cen_a10 & hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a14_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a14_a $ (hc_cnt_cen_a10 & !hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a14_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	datab => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a15_a = DFFE(!hc_cnt_clr_a10 & hc_cnt_cntr_awysi_counter_asload_path_a15_a $ (hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT & hc_cnt_cen_a10), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	datad => hc_cnt_cen_a10,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a10,
	devclrn => devclrn,
	devpor => devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a15_a);

hc_threshold_ibuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(14),
	combout => hc_threshold_a14_a_acombout);

hc_threshold_ibuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(13),
	combout => hc_threshold_a13_a_acombout);

hc_threshold_ibuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(12),
	combout => hc_threshold_a12_a_acombout);

hc_threshold_ibuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(11),
	combout => hc_threshold_a11_a_acombout);

hc_threshold_ibuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(10),
	combout => hc_threshold_a10_a_acombout);

hc_threshold_ibuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(9),
	combout => hc_threshold_a9_a_acombout);

hc_threshold_ibuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(8),
	combout => hc_threshold_a8_a_acombout);

hc_threshold_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(7),
	combout => hc_threshold_a7_a_acombout);

hc_threshold_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(6),
	combout => hc_threshold_a6_a_acombout);

hc_threshold_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(5),
	combout => hc_threshold_a5_a_acombout);

hc_threshold_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(4),
	combout => hc_threshold_a4_a_acombout);

hc_threshold_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(3),
	combout => hc_threshold_a3_a_acombout);

hc_threshold_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(2),
	combout => hc_threshold_a2_a_acombout);

hc_threshold_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(1),
	combout => hc_threshold_a1_a_acombout);

hc_threshold_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(0),
	combout => hc_threshold_a0_a_acombout);

modgen_gt_3_ix37 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx38 = CARRY(hc_threshold_a0_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0022",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a0_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx38);

modgen_gt_3_ix39 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx40 = CARRY(hc_threshold_a1_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a1_a & !modgen_gt_3_nx38 # !hc_threshold_a1_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a1_a # !modgen_gt_3_nx38))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a1_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cin => modgen_gt_3_nx38,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx40);

modgen_gt_3_ix41 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx42 = CARRY(hc_threshold_a2_a_acombout & (!modgen_gt_3_nx40 # !hc_cnt_cntr_awysi_counter_asload_path_a2_a) # !hc_threshold_a2_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a2_a & !modgen_gt_3_nx40)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a2_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cin => modgen_gt_3_nx40,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx42);

modgen_gt_3_ix43 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx44 = CARRY(hc_threshold_a3_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a3_a & !modgen_gt_3_nx42 # !hc_threshold_a3_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a3_a # !modgen_gt_3_nx42))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a3_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cin => modgen_gt_3_nx42,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx44);

modgen_gt_3_ix45 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx46 = CARRY(hc_threshold_a4_a_acombout & (!modgen_gt_3_nx44 # !hc_cnt_cntr_awysi_counter_asload_path_a4_a) # !hc_threshold_a4_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a4_a & !modgen_gt_3_nx44)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a4_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cin => modgen_gt_3_nx44,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx46);

modgen_gt_3_ix47 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx48 = CARRY(hc_threshold_a5_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a5_a & !modgen_gt_3_nx46 # !hc_threshold_a5_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a5_a # !modgen_gt_3_nx46))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a5_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cin => modgen_gt_3_nx46,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx48);

modgen_gt_3_ix49 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx50 = CARRY(hc_threshold_a6_a_acombout & (!modgen_gt_3_nx48 # !hc_cnt_cntr_awysi_counter_asload_path_a6_a) # !hc_threshold_a6_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a6_a & !modgen_gt_3_nx48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a6_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cin => modgen_gt_3_nx48,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx50);

modgen_gt_3_ix51 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx52 = CARRY(hc_threshold_a7_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a7_a & !modgen_gt_3_nx50 # !hc_threshold_a7_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a7_a # !modgen_gt_3_nx50))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a7_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cin => modgen_gt_3_nx50,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx52);

modgen_gt_3_ix53 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx54 = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & hc_threshold_a8_a_acombout & !modgen_gt_3_nx52 # !hc_cnt_cntr_awysi_counter_asload_path_a8_a & (hc_threshold_a8_a_acombout # !modgen_gt_3_nx52))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	datab => hc_threshold_a8_a_acombout,
	cin => modgen_gt_3_nx52,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx54);

modgen_gt_3_ix55 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx56 = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a9_a & (!modgen_gt_3_nx54 # !hc_threshold_a9_a_acombout) # !hc_cnt_cntr_awysi_counter_asload_path_a9_a & !hc_threshold_a9_a_acombout & !modgen_gt_3_nx54)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	datab => hc_threshold_a9_a_acombout,
	cin => modgen_gt_3_nx54,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx56);

modgen_gt_3_ix57 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx58 = CARRY(hc_threshold_a10_a_acombout & (!modgen_gt_3_nx56 # !hc_cnt_cntr_awysi_counter_asload_path_a10_a) # !hc_threshold_a10_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a10_a & !modgen_gt_3_nx56)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a10_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cin => modgen_gt_3_nx56,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx58);

modgen_gt_3_ix59 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx60 = CARRY(hc_threshold_a11_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a11_a & !modgen_gt_3_nx58 # !hc_threshold_a11_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a11_a # !modgen_gt_3_nx58))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a11_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cin => modgen_gt_3_nx58,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx60);

modgen_gt_3_ix61 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx62 = CARRY(hc_threshold_a12_a_acombout & (!modgen_gt_3_nx60 # !hc_cnt_cntr_awysi_counter_asload_path_a12_a) # !hc_threshold_a12_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a12_a & !modgen_gt_3_nx60)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a12_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cin => modgen_gt_3_nx60,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx62);

modgen_gt_3_ix63 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx64 = CARRY(hc_threshold_a13_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a13_a & !modgen_gt_3_nx62 # !hc_threshold_a13_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a13_a # !modgen_gt_3_nx62))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a13_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cin => modgen_gt_3_nx62,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx64);

modgen_gt_3_ix65 : apex20ke_lcell 
-- Equation(s):
-- modgen_gt_3_nx66 = CARRY(hc_threshold_a14_a_acombout & (!modgen_gt_3_nx64 # !hc_cnt_cntr_awysi_counter_asload_path_a14_a) # !hc_threshold_a14_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a14_a & !modgen_gt_3_nx64)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a14_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cin => modgen_gt_3_nx64,
	devclrn => devclrn,
	devpor => devpor,
	cout => modgen_gt_3_nx66);

modgen_gt_3_ix67 : apex20ke_lcell 
-- Equation(s):
-- hc_count_tc = hc_threshold_a15_a_acombout & !modgen_gt_3_nx66 & hc_cnt_cntr_awysi_counter_asload_path_a15_a # !hc_threshold_a15_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a15_a # !modgen_gt_3_nx66)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5F05",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a15_a_acombout,
	datad => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	cin => modgen_gt_3_nx66,
	devclrn => devclrn,
	devpor => devpor,
	combout => hc_count_tc);

reg_Hi_Cnt_a0 : apex20ke_lcell 
-- Equation(s):
-- Hi_Cnt_dup0 = DFFE((hc_cps_tc & hc_count_tc) & CASCADE(nx1145_a11), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => hc_cps_tc,
	datad => hc_count_tc,
	cascin => nx1145_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Hi_Cnt_dup0);

rtl_a494_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a494 = OUT_CPS_aCPS_STATE_a1_a & (OUT_CPS_aCPS_STATE_a2_a $ !OUT_CPS_aCPS_STATE_a3_a) # !OUT_CPS_aCPS_STATE_a1_a & (OUT_CPS_aCPS_STATE_a3_a # !OUT_CPS_aCPS_STATE_a2_a & !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B4B5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a2_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	datac => OUT_CPS_aCPS_STATE_a3_a,
	datad => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a494);

OUT_CPS_aCPS_STATE_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aCPS_STATE_a0_a = DFFE(!OUT_CPS_aCPS_STATE_a0_a & rtl_a494, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => OUT_CPS_aCPS_STATE_a0_a,
	datad => rtl_a494,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aCPS_STATE_a0_a);

OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91 = !OUT_CPS_aCPS_STATE_a2_a & (OUT_CPS_aCPS_STATE_a1_a # !IN_CPS_ams100tc_Del)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4545",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a2_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	datac => IN_CPS_ams100tc_Del,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91);

OUT_CPS_aCPS_STATE_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aCPS_STATE_a1_a = DFFE(OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91 & (OUT_CPS_aCPS_STATE_a1_a $ !OUT_CPS_aCPS_STATE_a0_a) # !OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91 & OUT_CPS_aCPS_STATE_a3_a & (OUT_CPS_aCPS_STATE_a1_a $ !OUT_CPS_aCPS_STATE_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9990",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a1_a,
	datab => OUT_CPS_aCPS_STATE_a0_a,
	datac => OUT_CPS_aMux_90_rtl_4_a_a00009_a6_a91,
	datad => OUT_CPS_aCPS_STATE_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aCPS_STATE_a1_a);

OUT_CPS_aCPS_STATE_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aCPS_STATE_a2_a = DFFE(OUT_CPS_aCPS_STATE_a2_a & OUT_CPS_aCPS_STATE_a3_a & (OUT_CPS_aCPS_STATE_a0_a # !OUT_CPS_aCPS_STATE_a1_a) # !OUT_CPS_aCPS_STATE_a2_a & OUT_CPS_aCPS_STATE_a1_a & !OUT_CPS_aCPS_STATE_a3_a & !OUT_CPS_aCPS_STATE_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A024",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a2_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	datac => OUT_CPS_aCPS_STATE_a3_a,
	datad => OUT_CPS_aCPS_STATE_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aCPS_STATE_a2_a);

OUT_CPS_aCPS_STATE_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aCPS_STATE_a3_a = DFFE(OUT_CPS_aCPS_STATE_a3_a $ (!OUT_CPS_aCPS_STATE_a2_a & OUT_CPS_aCPS_STATE_a1_a & !OUT_CPS_aCPS_STATE_a0_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0B4",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a2_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	datac => OUT_CPS_aCPS_STATE_a3_a,
	datad => OUT_CPS_aCPS_STATE_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aCPS_STATE_a3_a);

OUT_CPS_areduce_nor_17_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_areduce_nor_17 = OUT_CPS_aCPS_STATE_a3_a # OUT_CPS_aCPS_STATE_a1_a # OUT_CPS_aCPS_STATE_a0_a # OUT_CPS_aCPS_STATE_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a3_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	datac => OUT_CPS_aCPS_STATE_a0_a,
	datad => OUT_CPS_aCPS_STATE_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_areduce_nor_17);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_areduce_nor_6 $ OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => OUT_CPS_areduce_nor_6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

OUT_CPS_areduce_nor_7_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_areduce_nor_7 = !OUT_CPS_aCPS_STATE_a0_a & !OUT_CPS_aCPS_STATE_a2_a & OUT_CPS_aCPS_STATE_a1_a & OUT_CPS_aCPS_STATE_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_STATE_a0_a,
	datab => OUT_CPS_aCPS_STATE_a2_a,
	datac => OUT_CPS_aCPS_STATE_a1_a,
	datad => OUT_CPS_aCPS_STATE_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_areduce_nor_7);

OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a = DFFE(OUT_CPS_areduce_nor_7 $ OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => OUT_CPS_areduce_nor_7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	cout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a = DFFE(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ (OUT_CPS_areduce_nor_7 & OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	datab => OUT_CPS_areduce_nor_7,
	cin => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	cout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a = DFFE(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ (OUT_CPS_areduce_nor_7 & !OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	datab => OUT_CPS_areduce_nor_7,
	cin => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	cout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a = DFFE(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a $ (OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT & OUT_CPS_areduce_nor_7), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	datad => OUT_CPS_areduce_nor_7,
	cin => OUT_CPS_aWr_Adr_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a);

OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a = OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a $ !OUT_CPS_aCPS_STATE_a0_a
-- OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a = CARRY(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a # !OUT_CPS_aCPS_STATE_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "99BB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a0_a,
	datab => OUT_CPS_aCPS_STATE_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	cout => OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a);

OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121 = !OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aCPS_RAM_asram_aq_a0_a_a121);

OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a = OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a $ OUT_CPS_aCPS_STATE_a1_a $ OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a
-- OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a = CARRY(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & !OUT_CPS_aCPS_STATE_a1_a & !OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a & (!OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a # !OUT_CPS_aCPS_STATE_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a1_a,
	datab => OUT_CPS_aCPS_STATE_a1_a,
	cin => OUT_CPS_aadd_13_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a2_a,
	cout => OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a);

OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a = OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a $ OUT_CPS_aCPS_STATE_a2_a $ OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a
-- OUT_CPS_aadd_13_aadder_aresult_node_acout_a3_a = CARRY(OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & (!OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a # !OUT_CPS_aCPS_STATE_a2_a) # !OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a & !OUT_CPS_aCPS_STATE_a2_a & !OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a2_a,
	datab => OUT_CPS_aCPS_STATE_a2_a,
	cin => OUT_CPS_aadd_13_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a3_a,
	cout => OUT_CPS_aadd_13_aadder_aresult_node_acout_a3_a);

OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a = OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a $ OUT_CPS_aadd_13_aadder_aresult_node_acout_a3_a $ OUT_CPS_aCPS_STATE_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aWr_Adr_Cntr_awysi_counter_asload_path_a3_a,
	datad => OUT_CPS_aCPS_STATE_a3_a,
	cin => OUT_CPS_aadd_13_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_13_aadder_aresult_node_acs_buffer_a4_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a0_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a0_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a = OUT_CPS_acps_a0_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a0_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a = CARRY(OUT_CPS_acps_a0_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a0_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a);

OUT_CPS_acps_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a0_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a0_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => OUT_CPS_acps_a0_a_areg0,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a0_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a1_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a1_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a = OUT_CPS_acps_a1_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a1_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a = CARRY(OUT_CPS_acps_a1_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a1_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !OUT_CPS_acps_a1_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a # !OUT_CPS_aCPS_RAM_asram_aq_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a1_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a1_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a);

OUT_CPS_acps_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a1_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a1_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => OUT_CPS_areduce_nor_17,
	datac => OUT_CPS_acps_a1_a_areg0,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a1_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	datab => OUT_CPS_areduce_nor_6,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datab => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a10_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a10_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a9_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a9_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a8_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a8_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a7_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a7_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a6_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a6_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a5_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a5_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a4_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a4_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a3_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a3_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a2_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a2_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a = OUT_CPS_acps_a2_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a2_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a = CARRY(OUT_CPS_acps_a2_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a2_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a) # !OUT_CPS_acps_a2_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a2_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a2_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a2_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a);

OUT_CPS_acps_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a2_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a2_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => IN_CPS_ams100tc_Del,
	datac => OUT_CPS_acps_a2_a_areg0,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a2_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a = OUT_CPS_acps_a3_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a3_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a = CARRY(OUT_CPS_acps_a3_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a3_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !OUT_CPS_acps_a3_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a # !OUT_CPS_aCPS_RAM_asram_aq_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a3_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a3_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a);

OUT_CPS_acps_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a3_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a3_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a3_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a3_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a3_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a = OUT_CPS_acps_a4_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a4_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a = CARRY(OUT_CPS_acps_a4_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a4_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a) # !OUT_CPS_acps_a4_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a4_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a4_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a4_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a);

OUT_CPS_acps_a4_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a4_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a4_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EC20",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => OUT_CPS_areduce_nor_17,
	datac => OUT_CPS_acps_a4_a_areg0,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a4_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a = OUT_CPS_acps_a5_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a5_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a = CARRY(OUT_CPS_acps_a5_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a5_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !OUT_CPS_acps_a5_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a # !OUT_CPS_aCPS_RAM_asram_aq_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a5_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a5_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a);

OUT_CPS_acps_a5_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a5_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a5_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a5_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a5_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a5_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a = OUT_CPS_aCPS_RAM_asram_aq_a6_a $ OUT_CPS_acps_a6_a_areg0 $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a = CARRY(OUT_CPS_aCPS_RAM_asram_aq_a6_a & (OUT_CPS_acps_a6_a_areg0 # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a) # !OUT_CPS_aCPS_RAM_asram_aq_a6_a & OUT_CPS_acps_a6_a_areg0 & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_aCPS_RAM_asram_aq_a6_a,
	datab => OUT_CPS_acps_a6_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a);

OUT_CPS_acps_a6_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a6_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a6_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F088",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a6_a_areg0,
	datab => IN_CPS_ams100tc_Del,
	datac => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a6_a,
	datad => OUT_CPS_areduce_nor_17,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a6_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a = OUT_CPS_acps_a7_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a7_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a = CARRY(OUT_CPS_acps_a7_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a7_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !OUT_CPS_acps_a7_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a # !OUT_CPS_aCPS_RAM_asram_aq_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a7_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a7_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a);

OUT_CPS_acps_a7_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a7_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a7_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a7_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a7_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a = OUT_CPS_acps_a8_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a8_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a = CARRY(OUT_CPS_acps_a8_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a8_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a) # !OUT_CPS_acps_a8_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a8_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a8_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a8_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a);

OUT_CPS_acps_a8_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a8_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a8_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a8_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a8_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a = OUT_CPS_acps_a9_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a9_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a = CARRY(OUT_CPS_acps_a9_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a9_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !OUT_CPS_acps_a9_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a # !OUT_CPS_aCPS_RAM_asram_aq_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a9_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a9_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a);

OUT_CPS_acps_a9_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a9_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a9_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a9_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a9_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a = OUT_CPS_acps_a10_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a10_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a = CARRY(OUT_CPS_acps_a10_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a10_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a) # !OUT_CPS_acps_a10_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a10_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a10_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a10_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a);

OUT_CPS_acps_a10_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a10_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a10_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a10_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a10_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datab => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a11_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a11_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a = OUT_CPS_acps_a11_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a11_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a = CARRY(OUT_CPS_acps_a11_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a11_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !OUT_CPS_acps_a11_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a # !OUT_CPS_aCPS_RAM_asram_aq_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a11_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a11_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a);

OUT_CPS_acps_a11_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a11_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a11_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a11_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a11_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a11_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datab => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a12_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a12_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a = OUT_CPS_acps_a12_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a12_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a = CARRY(OUT_CPS_acps_a12_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a12_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a) # !OUT_CPS_acps_a12_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a12_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a12_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a12_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a);

OUT_CPS_acps_a12_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a12_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a12_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a12_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a12_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a12_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datab => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a13_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a13_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a = OUT_CPS_acps_a13_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a13_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a = CARRY(OUT_CPS_acps_a13_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a13_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !OUT_CPS_acps_a13_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a # !OUT_CPS_aCPS_RAM_asram_aq_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a13_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a13_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a);

OUT_CPS_acps_a13_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a13_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a13_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a13_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a13_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a13_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a $ (OUT_CPS_areduce_nor_6 & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a & !OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datab => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	cout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a14_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a14_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a = OUT_CPS_acps_a14_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a14_a $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a = CARRY(OUT_CPS_acps_a14_a_areg0 & (OUT_CPS_aCPS_RAM_asram_aq_a14_a # !OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a) # !OUT_CPS_acps_a14_a_areg0 & OUT_CPS_aCPS_RAM_asram_aq_a14_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a14_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a14_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a);

OUT_CPS_acps_a14_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a14_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a14_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a14_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a14_a_areg0);

OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(IN_CPS_areduce_nor_4) & OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a $ (OUT_CPS_areduce_nor_6 & OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_6,
	datad => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	cin => OUT_CPS_aIN_CPS_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => IN_CPS_areduce_nor_4,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a);

OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "cpscntr:OUT_CPS|lpm_ram_dp:CPS_RAM|altdpram:sram|content",
	init_file => "CPSCNTR.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 16,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 15,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "none",
	read_address_clear => "none",
	data_out_clock => "none",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_aIN_CPS_CNTR_awysi_counter_asload_path_a15_a,
	clk0 => CLK20_acombout,
	we => IN_CPS_areduce_nor_4,
	re => VCC,
	waddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_waddress,
	raddr => ww_OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_raddress,
	devclrn => devclrn,
	devpor => devpor,
	modesel => OUT_CPS_aCPS_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => OUT_CPS_aCPS_RAM_asram_aq_a15_a);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a = OUT_CPS_acps_a15_a_areg0 $ OUT_CPS_aCPS_RAM_asram_aq_a15_a $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a = CARRY(OUT_CPS_acps_a15_a_areg0 & !OUT_CPS_aCPS_RAM_asram_aq_a15_a & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !OUT_CPS_acps_a15_a_areg0 & (!OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a # !OUT_CPS_aCPS_RAM_asram_aq_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a15_a_areg0,
	datab => OUT_CPS_aCPS_RAM_asram_aq_a15_a,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a);

OUT_CPS_acps_a15_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a15_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a15_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a15_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a15_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a = OUT_CPS_acps_a16_a_areg0 $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a = CARRY(OUT_CPS_acps_a16_a_areg0 & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a16_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a);

OUT_CPS_acps_a16_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a16_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a16_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a16_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a16_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a = OUT_CPS_acps_a17_a_areg0 $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a = CARRY(!OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a # !OUT_CPS_acps_a17_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a17_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a);

OUT_CPS_acps_a17_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a17_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a17_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a17_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a17_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a = OUT_CPS_acps_a18_a_areg0 $ !OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a = CARRY(OUT_CPS_acps_a18_a_areg0 & !OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_acps_a18_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a);

OUT_CPS_acps_a18_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a18_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a # !OUT_CPS_areduce_nor_17 & OUT_CPS_acps_a18_a_areg0 & IN_CPS_ams100tc_Del, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EA40",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => OUT_CPS_areduce_nor_17,
	datab => OUT_CPS_acps_a18_a_areg0,
	datac => IN_CPS_ams100tc_Del,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a18_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a = OUT_CPS_acps_a19_a_areg0 $ OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a
-- OUT_CPS_aadd_60_aadder_aresult_node_acout_a19_a = CARRY(!OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a # !OUT_CPS_acps_a19_a_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => OUT_CPS_acps_a19_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	cout => OUT_CPS_aadd_60_aadder_aresult_node_acout_a19_a);

OUT_CPS_acps_a19_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a19_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a19_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => OUT_CPS_acps_a19_a_areg0,
	datac => OUT_CPS_areduce_nor_17,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a19_a_areg0);

OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a_aI : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a = OUT_CPS_aadd_60_aadder_aresult_node_acout_a19_a $ !OUT_CPS_acps_a20_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => OUT_CPS_acps_a20_a_areg0,
	cin => OUT_CPS_aadd_60_aadder_aresult_node_acout_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a);

OUT_CPS_acps_a20_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- OUT_CPS_acps_a20_a_areg0 = DFFE(OUT_CPS_areduce_nor_17 & OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a # !OUT_CPS_areduce_nor_17 & IN_CPS_ams100tc_Del & OUT_CPS_acps_a20_a_areg0, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F808",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => IN_CPS_ams100tc_Del,
	datab => OUT_CPS_acps_a20_a_areg0,
	datac => OUT_CPS_areduce_nor_17,
	datad => OUT_CPS_aadd_60_aadder_aresult_node_acs_buffer_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => OUT_CPS_acps_a20_a_areg0);

reg_Preset_Out_a0 : apex20ke_lcell 
-- Equation(s):
-- Preset_Out_dup0 = DFFE(Preset_Done_a7 & Preset_Done_a8, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Preset_Done_a7,
	datad => Preset_Done_a8,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Preset_Out_dup0);

CPS_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a0_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(0));

CPS_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a1_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(1));

CPS_obuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(10));

CPS_obuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(11));

CPS_obuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a12_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(12));

CPS_obuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a13_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(13));

CPS_obuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a14_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(14));

CPS_obuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a15_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(15));

CPS_obuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a16_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(16));

CPS_obuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a17_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(17));

CPS_obuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a18_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(18));

CPS_obuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a19_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(19));

CPS_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(2));

CPS_obuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a20_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(20));

CPS_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(3));

CPS_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(4));

CPS_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(5));

CPS_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(6));

CPS_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(7));

CPS_obuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(8));

CPS_obuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => IN_CPS_acps_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPS(9));

CTIME_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(0));

CTIME_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(1));

CTIME_obuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(10));

CTIME_obuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(11));

CTIME_obuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(12));

CTIME_obuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(13));

CTIME_obuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(14));

CTIME_obuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(15));

CTIME_obuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(16));

CTIME_obuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(17));

CTIME_obuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(18));

CTIME_obuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(19));

CTIME_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(2));

CTIME_obuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(20));

CTIME_obuf_21 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(21));

CTIME_obuf_22 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(22));

CTIME_obuf_23 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(23));

CTIME_obuf_24 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(24));

CTIME_obuf_25 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(25));

CTIME_obuf_26 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(26));

CTIME_obuf_27 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(27));

CTIME_obuf_28 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(28));

CTIME_obuf_29 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(29));

CTIME_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(3));

CTIME_obuf_30 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(30));

CTIME_obuf_31 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a31_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(31));

CTIME_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(4));

CTIME_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(5));

CTIME_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(6));

CTIME_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(7));

CTIME_obuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(8));

CTIME_obuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ctime_cntr_awysi_counter_asload_path_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CTIME(9));

Det_Sat_obuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Det_Sat_dup0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Det_Sat);

Hi_Cnt_obuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Hi_Cnt_dup0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Hi_Cnt);

LTIME_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(0));

LTIME_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(1));

LTIME_obuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(10));

LTIME_obuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(11));

LTIME_obuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(12));

LTIME_obuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(13));

LTIME_obuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(14));

LTIME_obuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(15));

LTIME_obuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(16));

LTIME_obuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(17));

LTIME_obuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(18));

LTIME_obuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(19));

LTIME_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(2));

LTIME_obuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(20));

LTIME_obuf_21 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(21));

LTIME_obuf_22 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(22));

LTIME_obuf_23 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(23));

LTIME_obuf_24 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(24));

LTIME_obuf_25 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(25));

LTIME_obuf_26 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(26));

LTIME_obuf_27 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(27));

LTIME_obuf_28 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(28));

LTIME_obuf_29 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(29));

LTIME_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(3));

LTIME_obuf_30 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(30));

LTIME_obuf_31 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a31_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(31));

LTIME_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(4));

LTIME_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(5));

LTIME_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(6));

LTIME_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(7));

LTIME_obuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(8));

LTIME_obuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => ltime_cntr_awysi_counter_asload_path_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_LTIME(9));

ms100_tc_obuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => nx1145_a10,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ms100_tc);

NPS_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a0_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(0));

NPS_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a1_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(1));

NPS_obuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(10));

NPS_obuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(11));

NPS_obuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a12_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(12));

NPS_obuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a13_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(13));

NPS_obuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a14_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(14));

NPS_obuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a15_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(15));

NPS_obuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a16_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(16));

NPS_obuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a17_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(17));

NPS_obuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a18_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(18));

NPS_obuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a19_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(19));

NPS_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(2));

NPS_obuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a20_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(20));

NPS_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(3));

NPS_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(4));

NPS_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(5));

NPS_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(6));

NPS_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(7));

NPS_obuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(8));

NPS_obuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => OUT_CPS_acps_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NPS(9));

PAMR_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a0_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(0));

PAMR_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a1_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(1));

PAMR_obuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(10));

PAMR_obuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(11));

PAMR_obuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a12_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(12));

PAMR_obuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a13_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(13));

PAMR_obuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a14_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(14));

PAMR_obuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a15_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(15));

PAMR_obuf_16 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a16_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(16));

PAMR_obuf_17 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a17_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(17));

PAMR_obuf_18 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a18_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(18));

PAMR_obuf_19 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a19_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(19));

PAMR_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(2));

PAMR_obuf_20 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a20_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(20));

PAMR_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(3));

PAMR_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(4));

PAMR_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(5));

PAMR_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(6));

PAMR_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(7));

PAMR_obuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(8));

PAMR_obuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => PAMR_CPS_acps_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_PAMR(9));

Preset_Out_obuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Preset_Out_dup0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Preset_Out);

Time_Enable_obuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Time_Enable_dup0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Time_Enable);
END structure;


