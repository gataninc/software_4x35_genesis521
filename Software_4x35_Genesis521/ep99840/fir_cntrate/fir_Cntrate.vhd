------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_cntrate.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		Maintains Total Input Counts at 100ms Time Intervals
--		Maintains Total Output Counts at 100ms Time Intervals
--
--		Maintains a running sum of the input and output count for the 
--		past 10 time intervals:
--			SUM = SUM + New Input - Past 10th Input
--		Therefore the resulting count rates are per 1 sec but updated every 100ms
--
--		The outputs are double buffered, using the read enables for the resptive registers
--		such that a "long" read will not interfere with the value being read by the host CPU or DSP
--
--   History:       <date> - <Author>
--		08/10/06 - MCS
--			ADDED OTR_EN which is disabled (0) when Preset Mode is Live Spectrum Mapping or WDS Modes
--			otherwise it is enabled ( Per Laszlo's request )
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		August 23, 2001 - MCS
--			Added New Preset Mode (110) for Preset Clock w/ Fast Mapping
--			This changes the time base from 1ms to 1us but cause the Preset Clock
--			to Function as usual.
--		************************************************************************************************
--		September 22, 2000 - MCS:
--			Final Release
--		************************************************************************************************
--		TODO
--			Use PAMR counts per second to determine detector is saturated 
--			ie if it is 0 for more than 2 seconds.
-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library altera;
	use altera.maxplus2.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity fir_cntrate is 
     port( 
		IMR       		: in      std_logic;								-- Master Reset
          CLK20			: in      std_logic;                                   	-- Master Clock
		OTR				: in		std_Logic;
		IFDISC	 		: in		std_logic;
		IPD_END			: in		std_logic;
		MOVE_FLAG			: in		std_logic;
		Video_Abort		: in		std_logic;
		PEAK_DONE			: in		std_logic;
		PA_Reset			: in		std_logic;
		Time_Clr			: in		std_logic;								-- Clear Clock Time and Live Time
		Time_Start		: in		std_logic;
		Time_Stop			: in		std_logic;
		PRESET_MODE		: in		std_logic_Vector(  3 downto 0 );
		PRESET			: in		std_logic_Vector( 31 downto 0 );
		ROI_SUM			: in		std_logic_vector( 31 downto 0 );
		LS_MAP_EN			: buffer	std_logic;
		Time_Clear		: buffer	std_logic;
		Time_Enable		: buffer	std_logic;
		CPS				: buffer	std_logic_vector( 20 downto 0 );		-- Input Count Rate
		NPS				: buffer	std_logic_Vector( 20 downto 0 );		-- Output Count Rate
		CTIME			: buffer	std_logic_Vector( 31 downto 0 );
		LTIME			: buffer	std_logic_Vector( 31 downto 0 );
		net_cps			: buffer	std_logic_Vector( 31 downto 0 );
		net_nps			: buffer	std_logic_Vector( 31 downto 0 );
		Preset_Out		: buffer	std_logic;
		ms100_tc			: buffer	std_logic;
		LS_Abort			: buffer	std_logic;
		WDS_BUSY			: buffer	std_logic );
     end fir_cntrate;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture BEHAVIORAL of fir_cntrate is
	constant ZEROES		: std_logic_Vector( 31 downto 0 ) := "00000000000000000000000000000000";
	constant us1_max		: integer := 19;					-- @ 20 Mhz - Ver 3B04
	constant ms1_max   		: integer := 19999;					-- @ 20 Mhz
	constant ms10_max		: integer := 4;
	constant ms100_max 		: integer := 99;					


	constant PR_NONE 		: std_logic_Vector( 3 downto 0 ) := "0000";
	constant PR_CLOCK		: std_logic_Vector( 3 downto 0 ) := "0001";
	constant PR_LIVE		: std_logic_Vector( 3 downto 0 ) := "0010";
	constant PR_3			: std_logic_Vector( 3 downto 0 ) := "0011";
	constant PR_4			: std_logic_Vector( 3 downto 0 ) := "0100";
	constant PR_ROI		: std_logic_Vector( 3 downto 0 ) := "0101";
	constant PR_PAMR		: std_logic_Vector( 3 downto 0 ) := "0110";		-- Ver 3503
	constant PR_LSMAP_REAL	: std_logic_Vector( 3 downto 0 ) := "0111";
	constant PR_LSMAP_LIVE	: std_logic_Vector( 3 downto 0 ) := "1000";
	constant PR_WDS_REAL	: std_logic_Vector( 3 downto 0 ) := "1001";		-- Ver 3533
	constant PR_WDS_LIVE	: std_logic_Vector( 3 downto 0 ) := "1010";		-- Ver 3533

	constant CPS_IDLE		: std_logic_Vector( 3 downto 0 ) 	:= "1010";
	constant CPS_9    		: std_logic_Vector( 3 downto 0 ) 	:= "1001";
	constant CPS_8    		: std_logic_Vector( 3 downto 0 ) 	:= "1000";
	constant CPS_7    		: std_logic_Vector( 3 downto 0 ) 	:= "0111";
	constant CPS_6    		: std_logic_Vector( 3 downto 0 ) 	:= "0110";
	constant CPS_5    		: std_logic_Vector( 3 downto 0 ) 	:= "0101";
	constant CPS_4    		: std_logic_Vector( 3 downto 0 ) 	:= "0100";
	constant CPS_3   		: std_logic_Vector( 3 downto 0 ) 	:= "0011";
	constant CPS_2    		: std_logic_Vector( 3 downto 0 ) 	:= "0010";
	constant CPS_1    		: std_logic_Vector( 3 downto 0 ) 	:= "0001";
	constant CPS_0    		: std_logic_Vector( 3 downto 0 ) 	:= "0000";

	-- ms1, ms10 and ms100 are the free running counters
	signal ms1_cnt 		: integer range 0 to ms1_max;
	signal ms10_cnt		: integer range 0 to ms10_max;
	signal ms100_cnt 		: integer range 0 to ms100_max;

	-- Preset Time COunter
	signal ps1_tc			: std_logic;
	signal ps1_cnt 		: integer range 0 to ms1_max;
	signal lt1_tc			: std_logic;
	signal lt1_cnt 		: integer range 0 to ms1_max;

	signal CT_CEN			: std_logic;
	signal LT_CEN			: std_logic;
	signal ms1_tc			: std_logic;
	signal ms100tc			: std_logic;

	signal lt_tmr_en 		: std_logic;

--	signal Preset_Cmp_Val	: std_logic_vector( 31 downto 0 );
	signal Preset_Cmp		: std_logic;
	signal Preset_Enable	: std_logic;
	signal INC_CPS			: std_logic;
	signal INC_NPS			: std_logic;
	signal CTime_Cmp		: std_logic;
	signal LTime_Cmp		: std_logic;
	signal RSum_Cmp		: std_logic;

	signal PA_Reset_Vec		: std_logic_Vector( 1 downto 0 );
	signal time_start_latch	: std_logic;
	signal ms100_tc_vec		: std_logic_vector( 2 downto 0 );
	signal inc_wr_adr 		: std_logic;
	signal cps_state		: std_logic_vector( 3 downto 0 );
	signal wr_adr			: std_logic_Vector( 3 downto 0 );
	signal rd_adr			: std_Logic_vector( 3 downto 0 );
	signal IPD_END_VEC		: std_logic_Vector( 2 downto 0 );
	signal TimeStart		: std_logic;
	signal vabort_vec		: std_logic_vector( 1 downto 0 );
	signal Int_Preset_Out	: std_logic;
	
	signal Move_FlaG_Vec	: std_logic_vector( 7 downto 0 );
	signal WDS_Mode		: std_logic;
	signal OTR_EN			: std_Logic;
	
	-------------------------------------------------------------------------------
	component Barnhart 
		port(
			imr			: in		std_logic;
			clk			: in		std_logic;
			inc_cps		: in		std_logic;
			inc_nps		: in		std_logic;
			lt_tmr_en		: buffer	std_logic );
	end component Barnhart;
	-------------------------------------------------------------------------------

     --------------------------------------------------------------------------
	component cpscntr 
	     port( 
			IMR       	: in      std_logic;					-- Master Reset
	          CLK20	    	: in      std_logic;                         -- Master Clock
			ms100_tc		: in		std_Logic;					-- 100ms Timer
			time_enable	: in		std_logic;
			time_clr		: in		std_logic;
			inc_cps		: in		std_logic;					-- FDISC or PEAK_DONE
			rd_adr		: in		std_logic_vector( 3 downto 0 );
			Wr_adr		: in		std_Logic_vector( 3 downto 0 );
			CPS_State		: in		std_logic_Vector( 3 downto 0 );
			inc_cps_ed	: buffer	std_logic;
			cps			: buffer	std_logic_Vector( 20 downto 0 );
			net			: buffer	std_logic_vector( 31 downto 0 ));
	end component;		-- cpscntr;
     --------------------------------------------------------------------------

begin
	with Preset_Mode Select
		OTR_EN	<= '0' when PR_LSMAP_REAL,
				   '0' when PR_LSMAP_LIVE,
				   '0' when PR_WDS_REAL,
				   '0' when PR_WDS_LIVE,
				   '1' when others;
				
	with Preset_Mode select
		LS_Map_En <= '1' when PR_LSMAP_REAL,
				   '1' when PR_LSMAP_LIVE,
				   '0' when others;


	U_Barnhart : Barnhart
		port map(
			imr			=> imr,
			clk			=> clk20,
			inc_cps		=> inc_cps,
			inc_nps		=> inc_nps,
			lt_tmr_en		=> lt_tmr_en );
			
     --------------------------------------------------------------------------
	-- Input Count Rate Component Instantiation
	IN_CPS : cpscntr 
	     port map( 
			IMR			=> IMR,
			CLK20		=> CLK20,
			ms100_tc		=> ms100_tc,
			time_enable	=> Time_Enable,
			time_clr		=> Time_Clear,
			inc_cps		=> IFDISC,
			inc_cps_ed	=> INC_CPS,
			rd_adr		=> Rd_Adr,
			Wr_adr		=> Wr_Adr,
			CPS_State		=> CPS_State,
			cps			=> cps,
			net			=> net_cps );
     --------------------------------------------------------------------------
	-- Output Count Rate Component Instantiation
	OUT_CPS : cpscntr 
	     port map( 
			IMR			=> IMR,
			CLK20		=> CLK20,
			ms100_tc		=> ms100_tc,
			time_enable	=> Time_Enable,
			time_clr		=> Time_Clear,
			inc_cps		=> PEAK_DONE,
			inc_cps_ed	=> INC_NPS,
			rd_adr		=> Rd_Adr,
			Wr_adr		=> Wr_Adr,
			CPS_State		=> CPS_State,
			cps			=> nps,
			net			=> net_nps );

     --------------------------------------------------------------------------
	-- Free Running Timers
	ms1_tc 		<= '1' when ( ms1_cnt = ms1_max ) else '0';
	ms100tc 		<= '1' when (( ms1_tc = '1' ) and ( ms100_cnt = ms100_max )) else '0';

	-- Preset Timer Ver 3B04
	ps1_tc		<= '1' when (( Preset_Mode = PR_LSMAP_REAL ) and ( ps1_cnt = us1_max )) else
				   '1' when (( Preset_Mode = PR_LSMAP_LIVE ) and ( PS1_Cnt = us1_Max )) else
			        '1' when (( Preset_Mode /= PR_LSMAP_REAL ) and ( Preset_Mode /= PR_LSMAP_LIVE ) and ( ps1_cnt = ms1_max )) else 
				   '0';

	lt1_tc		<= '1' when ( lt1_cnt = ms1_max ) else '0';

     --------------------------------------------------------------------------
	-- Clock Time Counter use the 1ms Preset Timer
	ct_cen <= '1' when (( Time_Enable = '1' ) and ( ps1_tc = '1' )) else '0';

	ctime_cntr : LPM_COUNTER
			generic map(
				LPM_WIDTH	=> 32 )
			port map(
				aclr		=> IMR,
				clock	=> CLK20,
				sclr		=> Time_Clear,
				cnt_en	=> ct_cen,
				q		=> CTIME );

     --------------------------------------------------------------------------
	-- Live Time Counter use the 1ms Preset Timer
	lt_cen <= '1' when (    ( Time_Enable 	= '1' ) 
					and ( lt_tmr_en 	= '1' ) 
					and ( lt1_tc 		= '1' )) else '0';

	ltime_cntr : LPM_COUNTER
			generic map(
				LPM_WIDTH	=> 32 )
			port map(
				aclr		=> IMR,
				clock	=> CLK20,
				sclr		=> Time_Clear,
				cnt_en	=> lt_cen,
				q		=> LTIME );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Preset Compare Mux and comparitor
	
	CTime_Comp : lpm_Compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Ctime,
			datab			=> Preset,
			ageb				=> CTime_Cmp );

	LTime_Comp : lpm_Compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Ltime,
			datab			=> Preset,
			ageb				=> LTime_Cmp );

	RSUM_Comp : lpm_Compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> ROI_SUM,
			datab			=> Preset,
			ageb				=> RSum_Cmp );
			
	with Preset_Mode select
		Preset_Cmp <= LTime_Cmp  when PR_Live,			-- Preset Live
				    RSum_Cmp 	when PR_ROI,			-- Preset ROI Sum
				    CTIME_CMP	when PR_LSMAP_REAL,
				    LTIME_CMP	when PR_LSMAP_LIVE,
				    LTime_Cmp  when PR_WDS_LIVE,		-- Preset WDS Live
				    CTime_Cmp	when PR_WDS_Real,		-- Preset WDS Clock
				    CTime_Cmp 	when others;			-- Preset Clock Time
				
--	Preset_Cmp_Val <= LTIME 			when ( Preset_Mode = PR_LIVE 	) else	-- Preset Live Time 
--				   ROI_SUM		when ( Preset_Mode = PR_ROI 	) else 	-- Preset ROI
--				   CTIME;											-- Preset CLock Time
					
--	Preset_Cmp <= '1' when ( Preset_Cmp_Val >= Preset ) else 
--			    '1' when (( Preset_Mode = PR_LSMAP ) and ( LS_ABORT = '1' )) else 
--			    '0';
     --------------------------------------------------------------------------



     --------------------------------------------------------------------------
	clock_proc : process( CLK20, IMR )
     begin
		if( IMR = '1' ) then
			IPD_END_VEC		<= "000";
			VAbort_Vec		<= "00";			
			Time_Clear		<= '0';		
			LS_Abort			<= '0';		
			PA_Reset_Vec		<= "00";
			time_start_latch	<= '0';
			Preset_Enable 		<= '0';
			Time_Enable 		<= '0';
			Preset_Out 		<= '0';
			Int_Preset_Out		<= '0';
			lt1_cnt 			<= 0;	
			ps1_cnt 			<= 0;
			ms1_cnt 			<= 0;
			ms10_cnt			<= 0;
			ms100_cnt 		<= 0;
			CPS_STATE			<= CPS_IDLE;
			ms100_tc_vec		<= "000";
			Move_Flag_Vec		<= x"00";
			WDS_BUSY			<= '0';			-- 3565
			WDS_Mode			<= '0';			-- 3565

			
		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
		
		
			IPD_END_VEC		<= IPD_END_VEC( 1 downto 0) & IPD_END;
			VABORT_VEC		<= VABORT_VEC(0) & Video_Abort;
			Move_Flag_Vec		<= Move_Flag_Vec( 6 downto 0 ) & Move_Flag;
			
			-- Ver 3565
			case Preset_Mode is
				when PR_WDS_REAL	=> WDS_Mode <= '1';
				when PR_WDS_LIVE	=> WDS_Mode <= '1';
				when others		=> WDS_Mode <= '0';
			end case;
			
			-- If WDS Preset Mode, assert Status Move Flg with Move Flag
			-- Keep asserted until Preset Done occurs ( with Time Enable
			if( Move_Flag_Vec = 0 ) 
				then WDS_Busy <= '0';
				else WDS_BUsy <= '1';
			end if;
			
		
			-- If WDS Preset Mode, assert Status Move Flg with Move Flag
			-- Keep asserted until Preset Done occurs ( with Time Enable
			
			if( Time_Clr = '1' )
				then Time_Clear <= '1';
			elsif((( Preset_Mode = PR_LSMAP_REAL ) or ( Preset_MOde = PR_LSMAP_LIVE )) and ( IPD_END_VEC( 1 downto 0 ) = "01" ))
				then Time_Clear <= '1';
			elsif(( WDS_Mode = '1' ) and ( Move_Flag_Vec( 1 downto 0 ) = "10" ))
				then Time_Clear <= '1';				
				else Time_Clear <= '0';
			end if;
			
			if( Time_Start = '1' )
				then TImeStart <= '1';
			elsif((( Preset_Mode = PR_LSMAP_REAL ) or ( Preset_Mode = PR_LSMAP_LIVE )) and ( IPD_END_VEC( 2 downto 1 ) = "01" ))
				then TimeStart <= '1';
			elsif(( WDS_Mode = '1') and ( Move_Flag_Vec( 2 downto 1 ) = "10" ))
				then TimeStart <= '1';				
				else TImeStart <= '0';
			end if;

			-- Live Spectrum MApping Video Abort ---------------------------------------------------------

			if( VABORT_VEC = "01" )		-- Leading Edge Detect
				then LS_ABORT <= '1';
				else LS_ABORT <= '0';
			end if;

		
			-- Preset Done Flag and counts ----------------------------------------------------------------
			

		
			PA_Reset_Vec	<= PA_Reset_Vec(0) & PA_Reset;
			ms100_tc_vec	<= ms100_tc_vec(1 downto 0 ) & ms100tc;
			if( ms100_tc_vec( 1 downto 0 ) = "01" ) 
				then ms100_tc <= '1';
				else ms100_tc <= '0';
			end if;

               ----------------------------------------------------------------
			-- Enable Presets depending on Mode
			case Preset_Mode is
				when PR_CLOCK 		=> Preset_Enable <= '1';	-- Preset Clock
				when PR_LIVE 		=> Preset_Enable <= '1';	-- Preset Live
				when PR_ROI		=> Preset_Enable <= '1';	-- Preset ROI
				when PR_LSMAP_REAL	=> Preset_Enable <= '1';	-- Preset Clock @Fast Mapping Ver 3b04
				when PR_LSMAP_LIVE	=> Preset_Enable <= '1';
				when PR_PAMR		=> Preset_Enable <= '1';	-- Preset- Clock, start with PA Reset TED 3503
				when PR_WDS_REAL	=> Preset_Enable <= '1';
				when PR_WDS_LIVE	=> Preset_Enable <= '1';
				when others		=> Preset_Enable <= '0';
			end case;
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			-- Preset Done Determination
			if( Time_Enable = '0' ) 
				then Int_Preset_Out <= '0';
			elsif( Time_Clear = '1' )
				then Int_Preset_Out <= '0';
			elsif( TIme_Stop = '1' )
				then Int_Preset_Out <= '0';
			elsif(( Preset_Mode /= PR_PAMR ) and ( TimeStart = '1' ))
				then Int_Preset_Out <= '0';
			elsif(( Preset_Mode = PR_PAMR ) and ( Time_Start_LAtch = '1' ) and ( PA_Reset_Vec = "10" ))
				then Int_Preset_Out <= '0';
			elsif(( Time_Enable = '1' ) and ( LS_Abort = '1' ) ) 
				then Int_Preset_Out <= '1';
				
			elsif(( Time_Enable = '1' ) and ( Preset_Enable = '1' ) and ( Preset_Cmp = '1' )) then
				if(( Preset_Mode = PR_LIVE ) or ( Preset_Mode = PR_WDS_LIVE ) or ( Preset_Mode = PR_LSMAP_LIVE )) then						-- Live Time Mode
					if(( lt_tmr_en = '1' ) and ( lt1_tc = '1' ))			-- Live Time Increment in Live Time Mode
						then Int_Preset_Out <= '1';
						else Int_Preset_Out <= '0';
					end if;
				else
					if( ps1_tc = '1' ) 
						then Int_Preset_Out <= '1';
						else Int_Preset_Out <= '0';
					end if;
				end if;
			else
				Int_Preset_Out <= '0';
			end if;
			Preset_Out	<= Int_Preset_Out;
               ----------------------------------------------------------------
			-- Acquisition Time Latch
			if( Preset_mode /= PR_PAMR )
				then Time_Start_latch <= '0';
			elsif( TimeStart = '1' )
				then Time_Start_Latch <= '1';
			elsif( Time_Enable = '1' )
				then Time_Start_Latch <= '0';
			end if;
			
			if(( Preset_Mode = PR_PAMR ) and ( Time_Start_LAtch = '1' ) and ( PA_Reset_Vec = "10" ))
				then Time_Enable <= '1';
			elsif(( Preset_Mode /= PR_PAMR ) and ( TimeStart = '1' ))
				then Time_Enable <= '1';
			elsif(( Int_Preset_Out = '1' ) or ( Time_Stop = '1' )) 
				then Time_Enable <= '0';
			end if;
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			-- 1ms Live Timer Base Time
			if(( Preset_Mode /= PR_PAMR ) and ( TimeStart = '1' ))
				then lt1_cnt <= 0;
			elsif(( Preset_Mode = PR_PAMR ) and ( Time_Start_LAtch = '1' ) and ( PA_Reset_Vec = "10" ))
				then lt1_cnt <= 0;
			elsif( Time_Clear = '1' )
				then lt1_cnt <= 0;
			elsif(( Time_Enable = '1' ) and ( lt_tmr_en = '1' )) then
				if( lt1_tc = '1' )
					then lt1_cnt <= 0;
				elsif( OTR_EN = '0' )
					then lt1_cnt <= lt1_cnt + 1;
				elsif(( OTR_EN = '1' ) and ( OTR = '0' ))
					then lt1_cnt <= lt1_cnt + 1;
				end if;
			end if;
               ----------------------------------------------------------------
	
               ----------------------------------------------------------------
			-- Preset Timer Time Base ( 1us for FMAP, otherwise 1ms)
			if(( Preset_Mode /= PR_PAMR ) and ( TimeStart = '1' ))
				then ps1_cnt <= 0;
			elsif(( Preset_Mode = PR_PAMR ) and ( Time_Start_LAtch = '1' ) and ( PA_Reset_Vec = "10" ))
				then ps1_cnt <= 0;
			elsif( ps1_tc = '1' )
				then ps1_cnt <= 0;
			elsif( OTR_EN = '0' )
				then ps1_Cnt <= ps1_cnt + 1;
			elsif(( OTR_EN = '1' ) and ( OTR = '0' ))				-- Ver 4x13
				then ps1_cnt <= ps1_Cnt + 1;
			end if;
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			-- 1ms Timer TIme Base
			if( ms1_tc = '1' )
				then ms1_cnt <= 0;
				else ms1_cnt <= ms1_cnt + 1;
			end if;
			-- 1ms Timer TIme Base
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			-- 10 msec Timer - TIME BASE
			if( ms1_tc = '1' ) then
				if( ms10_cnt = ms10_max ) 
					then ms10_cnt <= 0;
					else ms10_cnt <= ms10_cnt + 1;	
				end if;
			end if;
			-- 10 msec Timer - TIME BASE
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			-- 100 msec Timer - TIME BASE
			if( ms1_tc = '1' ) then
				if( ms100_cnt = ms100_max ) 
					then ms100_cnt <= 0;
					else ms100_cnt <= ms100_cnt + 1;	
				end if;
			end if;
			-- 100 msec Timer - TIME BASE
               ----------------------------------------------------------------

               ----------------------------------------------------------------
			
			-- CPS State Machine to sequence through the 10 elements and
			-- accumulate them for a final value
			case CPS_STATE is
				when CPS_IDLE => 
					if( ms100_tc = '1' ) then
						CPS_STATE <= CPS_9; 
					end if;
				when cps_9	=> CPS_STATE <= CPS_8;		
				when cps_8	=> CPS_STATE <= CPS_7;		
				when cps_7	=> CPS_STATE <= CPS_6;		
				when cps_6	=> CPS_STATE <= CPS_5;		
				when cps_5	=> CPS_STATE <= CPS_4;		
				when cps_4	=> CPS_STATE <= CPS_3;		
				when cps_3	=> CPS_STATE <= CPS_2;		
				when cps_2	=> CPS_STATE <= CPS_1;		
				when cps_1	=> CPS_STATE <= CPS_0;		
				when cps_0	=> CPS_STATE <= CPS_IDLE;	
				when others	=> cps_STATE <= cps_IDLE;
			end case;
			
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
	
     --------------------------------------------------------------------------
     --------------------------------------------------------------------------
	-- 10 Element Delay Line to maintain the sum of the past 10 measurements
     --------------------------------------------------------------------------
	-- Memory Address Registers
	Inc_Wr_Adr <= '1' when ( CPS_State = CPS_0 ) else '0';

	Wr_Adr_Cntr : Lpm_counter
		generic map(
			LPM_WIDTH		=> 4,
			LPM_DIRECTION 	=> "UP" )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			Cnt_En		=> Inc_Wr_Adr,
			q			=> Wr_Adr );
	
	rd_adr <= wr_adr - cps_state;
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end BEHAVIORAL;          -- fir_cntrate.VHD
-------------------------------------------------------------------------------
