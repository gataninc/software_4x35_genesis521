-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	cpscntr.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module maintains counts at 10X100ms time intervals
--		It also maintains the counts based on an input CNT_EN signal,
--		for Net Counts
--
--		Maintains a running sum of the input and output count for the 
--		past 10 time intervals:
--			SUM = SUM + New Input - Past 10th Input
--		Therefore the resulting count rates are per 1 sec but updated 
--		every 100ms
--
--
--   History:       <date> - <Author>
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library altera;
	use altera.maxplus2.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity cpscntr is 
     port( 
		IMR       	: in      std_logic;					-- Master Reset
          CLK20	    	: in      std_logic;                         -- Master Clock
		ms100_tc		: in		std_Logic;					-- 100ms Timer
		time_enable	: in		std_logic;
		time_clr		: in		std_logic;
		inc_cps		: in		std_logic;					-- FDISC or PEAK_DONE
		rd_adr		: in		std_logic_vector( 3 downto 0 );
		Wr_adr		: in		std_Logic_vector( 3 downto 0 );
		CPS_State		: in		std_logic_Vector( 3 downto 0 );
		inc_cps_ed	: buffer	std_logic;
		cps			: buffer	std_logic_Vector( 20 downto 0 );
		net			: buffer	std_logic_vector( 31 downto 0 ));
     end cpscntr;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture BEHAVIORAL of cpscntr is
	constant CPS_IDLE	: std_logic_Vector( 3 downto 0 ) 	:= "1010";
	
	signal icnt		: std_logic_vector( 15 downto 0 );
	signal inc_cps_vec	: std_logic_vector(  1 downto 0 );
	signal icnt9		: std_logic_Vector( 15 downto 0 );
	signal sum		: std_logic_vector( 20 downto 0 );
	signal Cnt_CNTR	: std_logic_vector( 31 downto 0 );
	signal cnt_max 	: std_logic;
	signal cnt_en		: std_logic;	
	
begin
	inc_cps_ed	<= '1' when ( inc_cps_vec = "01" ) else '0';

     --------------------------------------------------------------------------
	-- Now Generate the Net Counts -------------------------------------------
	CNT_MAX 	<= '1' when ( Cnt_CNTR = x"11111111" ) else '0';
	
	Cnt_EN	<= '1' when (( Time_Enable = '1' ) and ( inc_cps_ed = '1' ) and ( CNT_MAX = '0' )) else '0';
	
	Cnt_Cntr_Counter : LPM_COUNTER
		generic map(
			LPM_WIDTH		=> 32,
			LPM_DIRECTION	=> "UP",
			LPM_TYPE		=> "LPM_COUNTER" )
		port map(
			aclr			=> IMR,
			clock		=> CLK20,
			sclr			=> Time_Clr,
			cnt_en		=> Cnt_EN,
			q			=> CNt_CNTR );
			
	net_ff : lpm_ff
		generic map(
			LPM_WIDTH		=> 32,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			data			=> Cnt_Cntr,
			q			=> net );
	-- Now Generate the Net Counts -------------------------------------------
     --------------------------------------------------------------------------
	
     --------------------------------------------------------------------------
	-- Instantaneous Countrate, it is cleared every 100ms --------------------
	IN_CPS_CNTR : LPM_COUNTER
		generic map(
			LPM_WIDTH		=> 16,
			LPM_DIRECTION	=> "UP" )
		port map(
			aclr			=> IMR,				-- Master Reset
			clock		=> CLK20,				-- Master Clock
			sclr			=> ms100_tc,			-- Free Running 100ms Timer
			cnt_en		=> inc_cps_ed,			-- Increment Input Counts
			q			=> iCNT );			-- Total Input Counts per 100ms
	-- Instantaneous Countrate, it is cleared every 100ms --------------------
	--------------------------------------------------------------------------
	
     --------------------------------------------------------------------------
	-- 10 Element Delay Line to maintain the sum of the past 10 measurements
     --------------------------------------------------------------------------

	CPS_RAM : lpm_ram_dp
		generic map(
			LPM_WIDTH				=> 16,
			LPM_WIDTHAD			=> 4,
		     LPM_INDATA			=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "UNREGISTERED",
			LPM_OUTDATA			=> "UNREGISTERED",
			LPM_FILE				=> "CPSCNTR.MIF" )
		port map(
			wrclock				=> clk20,
			WREN					=> ms100_tc,
			wraddress				=> WR_ADR,
			rdaddress				=> RD_ADR,
			data					=> icnt,
			q					=> icnt9 );

	
     --------------------------------------------------------------------------
	clock_proc : process( clk20, IMR )
     begin
		if( IMR = '1' ) then
			inc_cps_Vec	<= "00";
			sum			<= '0' & x"00000";
			cps			<= '0' & x"00000";
		elsif(( clk20'Event ) and ( clk20 = '1' )) then
			inc_cps_vec	<= iNc_cps_Vec(0) & inc_cps;
			
			if( CPS_STATE = CPS_IDLE ) then
				if( ms100_tc = '1' )
					then sum 	<= '0' & x"00000";
				end if;
			else
				sum 		<= sum + iCNT9;
			end if;
			cps	<= sum;
				
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end BEHAVIORAL;          -- cpscntr.VHD
-------------------------------------------------------------------------------

