------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	Barnhart.VHD
-------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

library altera;
	use altera.maxplus2.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity Barnhart is
	port(
		imr			: in		std_logic;
		clk			: in		std_logic;
		inc_cps		: in		std_logic;
		inc_nps		: in		std_logic;
		lt_tmr_en		: buffer	std_logic );
end Barnhart;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture Behavioral of Barnhart is
	constant LT_CNTR_MAX 	: integer := 127;		-- 1.2% 63;	-- 15;
	constant LT_CNTR_PRESET	: integer := 64;		-- 1.2% 32;	-- 8;

	signal lt_inc_en		: std_logic;
	signal lt_cntr			: integer range 0 to LT_CNTR_MAX;

begin

	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			lt_cntr			<= LT_CNTR_PRESET;  -- Reset to 8
			lt_tmr_en			<= '0';
			lt_inc_en 		<= '0';
		elsif(( clk'event ) and ( clk = '1' )) then

               ----------------------------------------------------------------
			-- Live Time Counter via Barhnart Method 
			-- IF Maximum Count
			if( lt_cntr = LT_CNTR_MAX ) then
				lt_cntr 	<= LT_CNTR_PRESET;  -- Reset to 8
				lt_tmr_en <= '0';			-- Disable Live Time Counter
				lt_inc_en <= '0';			-- Disable Counter from Being Incremented

			-- If Minimum Count
			elsif( lt_cntr = 0 ) then 
				lt_cntr 	<= LT_CNTR_PRESET;  --  reset to 8
				lt_tmr_en <= '1';			-- Enable Live Time Counter 
				lt_inc_en	<= '1';			-- Enable Counter to be incremented

			-- If output Count			
			elsif(( INC_NPS = '1' ) and ( INC_CPS = '0' ))
				then lt_cntr <= lt_Cntr - 1;

			elsif(( INC_NPS = '1' ) and ( INC_CPS = '1' ) and ( lt_inc_en = '0' ))  		-- 6/23/00
				then lt_cntr <= lt_cntr - 1;					-- Decrement Count

			-- IF Input Count and Counter can be incremented
			elsif(( INC_NPS = '0' ) and ( INC_CPS = '1' ) and ( lt_inc_en = '1' ))			-- 6/23/00
				then lt_cntr <= lt_cntr + 1;					-- Increment Count

			end if;
			-- Live Time Counter via Barhnart Method 			
               ----------------------------------------------------------------
		end if;
	end process;
-------------------------------------------------------------------------------
end behavioral;			-- Barhnart.vhd
-------------------------------------------------------------------------------
