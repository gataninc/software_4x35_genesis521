-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	APP_BLOCK.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_beam_blank is 
	port(
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_Logic;					-- Master Clock
		fdisc		: in		std_logic;
		Pol			: in		std_logic;
		Delay		: in		std_logic_Vector( 15 downto 0 );
		Width		: in		std_Logic_Vector( 15 downto 0 );
		Blank		: buffer	std_logic );
end fir_beam_blank;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_beam_blank is

	signal delay_cnt 	: std_logic_vector( 15 downto 0 );
	signal delay_cen	: std_logic;
	signal delay_tc  	: std_logic;
	signal delay_clr	: std_logic;

	signal width_cnt 	: std_logic_vector( 15 downto 0 );
	signal width_tc	: std_logic;
	signal Width_Cen	: std_logic;
	signal width_clr	: std_logic;
begin

	-------------------------------------------------------------------------------
	Delay_Clr <= not( Delay_Cen );
	
	delay_Counter : lpm_Counter
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_COUNTER" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> fdisc,
			cnt_en			=> delay_cen,
			q				=> delay_cnt );

	delay_compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Delay,
			datab			=> Delay_Cnt,
			aeb				=> Delay_TC );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	Width_clr <= '1' when( Width_Cen = '0' ) else 
			   '1' when( fdisc = '1' ) else 
			   '0';
			
	width_Counter : lpm_Counter
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_COUNTER" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> fdisc,
			cnt_en			=> width_cen,
			q				=> width_cnt );

	Width_compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Width,
			datab			=> Width_Cnt,
			aeb				=> Width_TC );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	clk_proc : process( clk, imr )
	begin
		if( imr = '1' ) then
			delay_cen 	<= '0';
			Width_cen 	<= '0';
			Blank		<= '0';
		elsif(( clk'event ) and ( clk = '1' )) then
		
			if( FDisc = '1' )
				then Delay_Cen <= '1';
			elsif(( Delay_Cen = '1' ) and ( Delay_tc = '1' ))
				then Delay_Cen <= '0';
			end if;
			
			if(( delay_cen = '1' ) and ( delay_tc = '1' ))
				then Width_Cen <= '1';
			elsif(( Width_Cen = '1' ) and ( Width_tc = '1' ))
				then Width_Cen <= '0';
			end if;
			
			if( Pol = '1' )
				then Blank <= Width_Cen;
				else Blank <= not( Width_Cen );
			end if;
		end if;
	end process;
	-------------------------------------------------------------------------------

------------------------------------------------------------------------------------
end behavioral;               -- fir_beam_blank
------------------------------------------------------------------------------------