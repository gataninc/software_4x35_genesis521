onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/fdisc
add wave -noupdate -format Logic -radix binary /tb/pol
add wave -noupdate -format Literal -radix hexadecimal /tb/delay
add wave -noupdate -format Literal -radix hexadecimal /tb/width
add wave -noupdate -format Logic -radix binary /tb/blank
TreeUpdate [SetDefaultTree]
WaveRestoreZoom {0 ns} {315 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
