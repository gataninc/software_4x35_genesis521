-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 16:34:35"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_beam_blank IS
    PORT (
	Pol : IN std_logic;
	clk : IN std_logic;
	imr : IN std_logic;
	fdisc : IN std_logic;
	Delay : IN std_logic_vector(15 DOWNTO 0);
	Width : IN std_logic_vector(15 DOWNTO 0);
	Blank : OUT std_logic
	);
END fir_beam_blank;

ARCHITECTURE structure OF fir_beam_blank IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_Pol : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_fdisc : std_logic;
SIGNAL ww_Delay : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Width : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Blank : std_logic;
SIGNAL Pol_apadio : std_logic;
SIGNAL clk_apadio : std_logic;
SIGNAL imr_apadio : std_logic;
SIGNAL fdisc_apadio : std_logic;
SIGNAL Delay_a6_a_apadio : std_logic;
SIGNAL Delay_a7_a_apadio : std_logic;
SIGNAL Delay_a4_a_apadio : std_logic;
SIGNAL Delay_a5_a_apadio : std_logic;
SIGNAL Delay_a0_a_apadio : std_logic;
SIGNAL Delay_a1_a_apadio : std_logic;
SIGNAL Delay_a2_a_apadio : std_logic;
SIGNAL Delay_a3_a_apadio : std_logic;
SIGNAL Delay_a14_a_apadio : std_logic;
SIGNAL Delay_a15_a_apadio : std_logic;
SIGNAL Delay_a12_a_apadio : std_logic;
SIGNAL Delay_a13_a_apadio : std_logic;
SIGNAL Delay_a8_a_apadio : std_logic;
SIGNAL Delay_a9_a_apadio : std_logic;
SIGNAL Delay_a10_a_apadio : std_logic;
SIGNAL Delay_a11_a_apadio : std_logic;
SIGNAL Width_a6_a_apadio : std_logic;
SIGNAL Width_a7_a_apadio : std_logic;
SIGNAL Width_a4_a_apadio : std_logic;
SIGNAL Width_a5_a_apadio : std_logic;
SIGNAL Width_a0_a_apadio : std_logic;
SIGNAL Width_a1_a_apadio : std_logic;
SIGNAL Width_a2_a_apadio : std_logic;
SIGNAL Width_a3_a_apadio : std_logic;
SIGNAL Width_a14_a_apadio : std_logic;
SIGNAL Width_a15_a_apadio : std_logic;
SIGNAL Width_a12_a_apadio : std_logic;
SIGNAL Width_a13_a_apadio : std_logic;
SIGNAL Width_a8_a_apadio : std_logic;
SIGNAL Width_a9_a_apadio : std_logic;
SIGNAL Width_a10_a_apadio : std_logic;
SIGNAL Width_a11_a_apadio : std_logic;
SIGNAL Blank_apadio : std_logic;
SIGNAL Delay_a15_a_acombout : std_logic;
SIGNAL fdisc_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL delay_cen : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL delay_Counter_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL Delay_a14_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out : std_logic;
SIGNAL Delay_a12_a_acombout : std_logic;
SIGNAL Delay_a13_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out : std_logic;
SIGNAL Delay_a10_a_acombout : std_logic;
SIGNAL Delay_a11_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out : std_logic;
SIGNAL Delay_a8_a_acombout : std_logic;
SIGNAL Delay_a9_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out : std_logic;
SIGNAL Delay_a1_a_acombout : std_logic;
SIGNAL delay_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Delay_a0_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL Delay_a5_a_acombout : std_logic;
SIGNAL Delay_a4_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL Delay_a6_a_acombout : std_logic;
SIGNAL Delay_a7_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out : std_logic;
SIGNAL Delay_a2_a_acombout : std_logic;
SIGNAL Delay_a3_a_acombout : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out : std_logic;
SIGNAL Width_a11_a_acombout : std_logic;
SIGNAL Width_a10_a_acombout : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out : std_logic;
SIGNAL Width_a13_a_acombout : std_logic;
SIGNAL Width_a12_a_acombout : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out : std_logic;
SIGNAL Width_a14_a_acombout : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL width_Counter_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL Width_a15_a_acombout : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out : std_logic;
SIGNAL Width_a9_a_acombout : std_logic;
SIGNAL Width_a8_a_acombout : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out : std_logic;
SIGNAL Width_a0_a_acombout : std_logic;
SIGNAL Width_a1_a_acombout : std_logic;
SIGNAL width_Counter_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL Width_a5_a_acombout : std_logic;
SIGNAL Width_a4_a_acombout : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL Width_a6_a_acombout : std_logic;
SIGNAL Width_a7_a_acombout : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out : std_logic;
SIGNAL Width_a2_a_acombout : std_logic;
SIGNAL Width_a3_a_acombout : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out : std_logic;
SIGNAL Width_Cen : std_logic;
SIGNAL Pol_acombout : std_logic;
SIGNAL rtl_a0 : std_logic;
SIGNAL NOT_rtl_a0 : std_logic;

BEGIN

ww_Pol <= Pol;
ww_clk <= clk;
ww_imr <= imr;
ww_fdisc <= fdisc;
ww_Delay <= Delay;
ww_Width <= Width;
Blank <= ww_Blank;
NOT_rtl_a0 <= NOT rtl_a0;

Delay_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(15),
	combout => Delay_a15_a_acombout);

fdisc_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fdisc,
	combout => fdisc_acombout);

clk_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

delay_cen_aI : apex20ke_lcell 
-- Equation(s):
-- delay_cen = DFFE(fdisc_acombout # delay_cen & !delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCFC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fdisc_acombout,
	datac => delay_cen,
	datad => delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_cen);

delay_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(fdisc_acombout) & delay_cen $ delay_Counter_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_cen,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a0_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a1_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a1_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a1_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a2_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a2_a & !delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a2_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a2_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a3_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a3_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a3_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a4_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a4_a & !delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a4_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a4_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a5_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a5_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a5_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a6_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a6_a & !delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a6_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a6_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a7_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a7_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a7_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a8_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a8_a & !delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_cen,
	datab => delay_Counter_awysi_counter_asload_path_a8_a,
	cin => delay_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a8_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a9_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_cen,
	datab => delay_Counter_awysi_counter_asload_path_a9_a,
	cin => delay_Counter_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a9_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a10_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a10_a & !delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a10_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a10_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a11_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a11_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a11_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a12_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a12_a & !delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a12_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a12_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a13_a $ (delay_cen & delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT # !delay_Counter_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a13_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a13_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a14_a $ (delay_cen & !delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- delay_Counter_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(delay_Counter_awysi_counter_asload_path_a14_a & !delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a14_a,
	datab => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a14_a,
	cout => delay_Counter_awysi_counter_acounter_cell_a14_a_aCOUT);

delay_Counter_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- delay_Counter_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(fdisc_acombout) & delay_Counter_awysi_counter_asload_path_a15_a $ (delay_Counter_awysi_counter_acounter_cell_a14_a_aCOUT & delay_cen), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a15_a,
	datad => delay_cen,
	cin => delay_Counter_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => delay_Counter_awysi_counter_asload_path_a15_a);

Delay_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(14),
	combout => Delay_a14_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out = Delay_a15_a_acombout & delay_Counter_awysi_counter_asload_path_a15_a & (Delay_a14_a_acombout $ !delay_Counter_awysi_counter_asload_path_a14_a) # !Delay_a15_a_acombout & !delay_Counter_awysi_counter_asload_path_a15_a & (Delay_a14_a_acombout $ !delay_Counter_awysi_counter_asload_path_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a15_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a15_a,
	datac => Delay_a14_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out);

Delay_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(12),
	combout => Delay_a12_a_acombout);

Delay_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(13),
	combout => Delay_a13_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out = Delay_a12_a_acombout & delay_Counter_awysi_counter_asload_path_a12_a & (Delay_a13_a_acombout $ !delay_Counter_awysi_counter_asload_path_a13_a) # !Delay_a12_a_acombout & !delay_Counter_awysi_counter_asload_path_a12_a & (Delay_a13_a_acombout $ !delay_Counter_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a12_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a12_a,
	datac => Delay_a13_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out);

Delay_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(10),
	combout => Delay_a10_a_acombout);

Delay_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(11),
	combout => Delay_a11_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out = Delay_a10_a_acombout & delay_Counter_awysi_counter_asload_path_a10_a & (delay_Counter_awysi_counter_asload_path_a11_a $ !Delay_a11_a_acombout) # !Delay_a10_a_acombout & !delay_Counter_awysi_counter_asload_path_a10_a & (delay_Counter_awysi_counter_asload_path_a11_a $ !Delay_a11_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a10_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a11_a,
	datac => Delay_a11_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out);

Delay_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(8),
	combout => Delay_a8_a_acombout);

Delay_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(9),
	combout => Delay_a9_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out = delay_Counter_awysi_counter_asload_path_a9_a & Delay_a9_a_acombout & (Delay_a8_a_acombout $ !delay_Counter_awysi_counter_asload_path_a8_a) # !delay_Counter_awysi_counter_asload_path_a9_a & !Delay_a9_a_acombout & (Delay_a8_a_acombout $ !delay_Counter_awysi_counter_asload_path_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_Counter_awysi_counter_asload_path_a9_a,
	datab => Delay_a8_a_acombout,
	datac => delay_Counter_awysi_counter_asload_path_a8_a,
	datad => Delay_a9_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out);

delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out_a19 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out = delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out,
	datab => delay_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out,
	datac => delay_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out,
	datad => delay_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out);

Delay_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(1),
	combout => Delay_a1_a_acombout);

Delay_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(0),
	combout => Delay_a0_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = Delay_a1_a_acombout & delay_Counter_awysi_counter_asload_path_a1_a & (delay_Counter_awysi_counter_asload_path_a0_a $ !Delay_a0_a_acombout) # !Delay_a1_a_acombout & !delay_Counter_awysi_counter_asload_path_a1_a & (delay_Counter_awysi_counter_asload_path_a0_a $ !Delay_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a1_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a0_a,
	datac => Delay_a0_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

Delay_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(5),
	combout => Delay_a5_a_acombout);

Delay_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(4),
	combout => Delay_a4_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out = Delay_a5_a_acombout & delay_Counter_awysi_counter_asload_path_a5_a & (Delay_a4_a_acombout $ !delay_Counter_awysi_counter_asload_path_a4_a) # !Delay_a5_a_acombout & !delay_Counter_awysi_counter_asload_path_a5_a & (Delay_a4_a_acombout $ !delay_Counter_awysi_counter_asload_path_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a5_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a5_a,
	datac => Delay_a4_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out);

Delay_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(6),
	combout => Delay_a6_a_acombout);

Delay_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(7),
	combout => Delay_a7_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out = Delay_a6_a_acombout & delay_Counter_awysi_counter_asload_path_a6_a & (delay_Counter_awysi_counter_asload_path_a7_a $ !Delay_a7_a_acombout) # !Delay_a6_a_acombout & !delay_Counter_awysi_counter_asload_path_a6_a & (delay_Counter_awysi_counter_asload_path_a7_a $ !Delay_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a6_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a7_a,
	datac => Delay_a7_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out);

Delay_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(2),
	combout => Delay_a2_a_acombout);

Delay_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Delay(3),
	combout => Delay_a3_a_acombout);

delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out = Delay_a2_a_acombout & delay_Counter_awysi_counter_asload_path_a2_a & (delay_Counter_awysi_counter_asload_path_a3_a $ !Delay_a3_a_acombout) # !Delay_a2_a_acombout & !delay_Counter_awysi_counter_asload_path_a2_a & (delay_Counter_awysi_counter_asload_path_a3_a $ !Delay_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Delay_a2_a_acombout,
	datab => delay_Counter_awysi_counter_asload_path_a3_a,
	datac => Delay_a3_a_acombout,
	datad => delay_Counter_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out);

delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out_a19 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out = delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => delay_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	datab => delay_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out,
	datac => delay_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out,
	datad => delay_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out);

delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out = delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out & delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out,
	datad => delay_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out);

Width_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(11),
	combout => Width_a11_a_acombout);

Width_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(10),
	combout => Width_a10_a_acombout);

width_Counter_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(fdisc_acombout) & Width_Cen $ width_Counter_awysi_counter_asload_path_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Width_Cen,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a0_a,
	cout => width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a1_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT # !width_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a1_a,
	datab => Width_Cen,
	cin => width_Counter_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a1_a,
	cout => width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a2_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a2_a & !width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a2_a,
	cin => width_Counter_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a2_a,
	cout => width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a3_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT # !width_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a3_a,
	datab => Width_Cen,
	cin => width_Counter_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a3_a,
	cout => width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a4_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a4_a & !width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a4_a,
	cin => width_Counter_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a4_a,
	cout => width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a5_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT # !width_Counter_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a5_a,
	cin => width_Counter_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a5_a,
	cout => width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a6_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a6_a & !width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a6_a,
	cin => width_Counter_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a6_a,
	cout => width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a7_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT # !width_Counter_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a7_a,
	cin => width_Counter_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a7_a,
	cout => width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a8_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a8_a & !width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a8_a,
	cin => width_Counter_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a8_a,
	cout => width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a9_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT # !width_Counter_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a9_a,
	datab => Width_Cen,
	cin => width_Counter_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a9_a,
	cout => width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a10_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a10_a & !width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a10_a,
	cin => width_Counter_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a10_a,
	cout => width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a11_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT # !width_Counter_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a11_a,
	datab => Width_Cen,
	cin => width_Counter_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a11_a,
	cout => width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT);

Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out = Width_a11_a_acombout & width_Counter_awysi_counter_asload_path_a11_a & (Width_a10_a_acombout $ !width_Counter_awysi_counter_asload_path_a10_a) # !Width_a11_a_acombout & !width_Counter_awysi_counter_asload_path_a11_a & (Width_a10_a_acombout $ !width_Counter_awysi_counter_asload_path_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a11_a_acombout,
	datab => Width_a10_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a11_a,
	datad => width_Counter_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out);

Width_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(13),
	combout => Width_a13_a_acombout);

Width_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(12),
	combout => Width_a12_a_acombout);

width_Counter_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a12_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a12_a & !width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a12_a,
	cin => width_Counter_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a12_a,
	cout => width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a13_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT # !width_Counter_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a13_a,
	datab => Width_Cen,
	cin => width_Counter_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a13_a,
	cout => width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT);

Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out = Width_a13_a_acombout & width_Counter_awysi_counter_asload_path_a13_a & (Width_a12_a_acombout $ !width_Counter_awysi_counter_asload_path_a12_a) # !Width_a13_a_acombout & !width_Counter_awysi_counter_asload_path_a13_a & (Width_a12_a_acombout $ !width_Counter_awysi_counter_asload_path_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a13_a_acombout,
	datab => Width_a12_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a13_a,
	datad => width_Counter_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out);

Width_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(14),
	combout => Width_a14_a_acombout);

width_Counter_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a14_a $ (Width_Cen & !width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- width_Counter_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(width_Counter_awysi_counter_asload_path_a14_a & !width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => width_Counter_awysi_counter_asload_path_a14_a,
	cin => width_Counter_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a14_a,
	cout => width_Counter_awysi_counter_acounter_cell_a14_a_aCOUT);

width_Counter_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- width_Counter_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(fdisc_acombout) & width_Counter_awysi_counter_asload_path_a15_a $ (Width_Cen & width_Counter_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datad => width_Counter_awysi_counter_asload_path_a15_a,
	cin => width_Counter_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => fdisc_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => width_Counter_awysi_counter_asload_path_a15_a);

Width_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(15),
	combout => Width_a15_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out = Width_a14_a_acombout & width_Counter_awysi_counter_asload_path_a14_a & (width_Counter_awysi_counter_asload_path_a15_a $ !Width_a15_a_acombout) # !Width_a14_a_acombout & !width_Counter_awysi_counter_asload_path_a14_a & (width_Counter_awysi_counter_asload_path_a15_a $ !Width_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a14_a_acombout,
	datab => width_Counter_awysi_counter_asload_path_a15_a,
	datac => Width_a15_a_acombout,
	datad => width_Counter_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out);

Width_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(9),
	combout => Width_a9_a_acombout);

Width_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(8),
	combout => Width_a8_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out = Width_a9_a_acombout & width_Counter_awysi_counter_asload_path_a9_a & (Width_a8_a_acombout $ !width_Counter_awysi_counter_asload_path_a8_a) # !Width_a9_a_acombout & !width_Counter_awysi_counter_asload_path_a9_a & (Width_a8_a_acombout $ !width_Counter_awysi_counter_asload_path_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a9_a_acombout,
	datab => Width_a8_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a9_a,
	datad => width_Counter_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out);

Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out_a19 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out = Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_compare_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out,
	datab => Width_compare_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out,
	datac => Width_compare_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out,
	datad => Width_compare_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out);

Width_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(0),
	combout => Width_a0_a_acombout);

Width_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(1),
	combout => Width_a1_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = Width_a0_a_acombout & width_Counter_awysi_counter_asload_path_a0_a & (Width_a1_a_acombout $ !width_Counter_awysi_counter_asload_path_a1_a) # !Width_a0_a_acombout & !width_Counter_awysi_counter_asload_path_a0_a & (Width_a1_a_acombout $ !width_Counter_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a0_a_acombout,
	datab => Width_a1_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a0_a,
	datad => width_Counter_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

Width_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(5),
	combout => Width_a5_a_acombout);

Width_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(4),
	combout => Width_a4_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out = width_Counter_awysi_counter_asload_path_a5_a & Width_a5_a_acombout & (width_Counter_awysi_counter_asload_path_a4_a $ !Width_a4_a_acombout) # !width_Counter_awysi_counter_asload_path_a5_a & !Width_a5_a_acombout & (width_Counter_awysi_counter_asload_path_a4_a $ !Width_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a5_a,
	datab => Width_a5_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a4_a,
	datad => Width_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out);

Width_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(6),
	combout => Width_a6_a_acombout);

Width_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(7),
	combout => Width_a7_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out = width_Counter_awysi_counter_asload_path_a6_a & Width_a6_a_acombout & (width_Counter_awysi_counter_asload_path_a7_a $ !Width_a7_a_acombout) # !width_Counter_awysi_counter_asload_path_a6_a & !Width_a6_a_acombout & (width_Counter_awysi_counter_asload_path_a7_a $ !Width_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => width_Counter_awysi_counter_asload_path_a6_a,
	datab => Width_a6_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a7_a,
	datad => Width_a7_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out);

Width_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(2),
	combout => Width_a2_a_acombout);

Width_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Width(3),
	combout => Width_a3_a_acombout);

Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out = Width_a2_a_acombout & width_Counter_awysi_counter_asload_path_a2_a & (Width_a3_a_acombout $ !width_Counter_awysi_counter_asload_path_a3_a) # !Width_a2_a_acombout & !width_Counter_awysi_counter_asload_path_a2_a & (Width_a3_a_acombout $ !width_Counter_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_a2_a_acombout,
	datab => Width_a3_a_acombout,
	datac => width_Counter_awysi_counter_asload_path_a3_a,
	datad => width_Counter_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out);

Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out_a19 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out = Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_compare_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	datab => Width_compare_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out,
	datac => Width_compare_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out,
	datad => Width_compare_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out);

Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out_a0 : apex20ke_lcell 
-- Equation(s):
-- Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out = Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out & Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out,
	datad => Width_compare_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out);

Width_Cen_aI : apex20ke_lcell 
-- Equation(s):
-- Width_Cen = DFFE(Width_Cen & (delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out & delay_cen # !Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out) # !Width_Cen & delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out & delay_cen, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0EA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Width_Cen,
	datab => delay_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	datac => delay_cen,
	datad => Width_compare_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => Width_Cen);

Pol_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Pol,
	combout => Pol_acombout);

rtl_a0_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a0 = Width_Cen $ Pol_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Width_Cen,
	datad => Pol_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a0);

Blank_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => NOT_rtl_a0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Blank);
END structure;


