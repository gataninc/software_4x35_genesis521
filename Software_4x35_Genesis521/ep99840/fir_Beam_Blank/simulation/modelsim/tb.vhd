-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_adisc
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 22, 2002
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module monitors the filtered data ( High, Medium, Low, BLM )
--		and attempts to identify the threshold level
--		by looking for the most negative peaks, and applying the abs function
--	History
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_Bench of tb is
	signal imr		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal fdisc		: std_logic;
	signal Pol		: std_logic;
	signal Delay		: std_logic_Vector( 15 downto 0 );
	signal Width		: std_Logic_Vector( 15 downto 0 );
	signal Blank		: std_logic;
	
	-------------------------------------------------------------------------------
	component fir_Beam_Blank
		port(
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_Logic;					-- Master Clock
			fdisc		: in		std_logic;
			Pol			: in		std_logic;
			Delay		: in		std_logic_Vector( 15 downto 0 );
			Width		: in		std_Logic_Vector( 15 downto 0 );
			Blank		: out	std_logic );
	end component fir_Beam_Blank;
	-------------------------------------------------------------------------------


begin
	imr 		<= '0' after 624 ns;
	clk		<= not( clk ) after 25 ns;


	U : fir_Beam_Blank 
		port map(
			imr			=> imr, 
			clk			=> clk,
			fdisc		=> fdisc,
			Pol			=> pol,
			Delay		=> delay,
			Width		=> width,
			Blank		=> blank );

	tb_proc : process
	begin
		fdisc 	<= '0';
		pol		<= '0';		-- Positive
		delay 	<= x"0000";
		width 	<= x"0010";
		wait for 2 us;

		----------------------------------------------------------------------------------
		assert false
			report "check Blank Polarity"
			severity note;

			Pol <= '0';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '0';
			wait until (( Blank'Event ) and ( Blank = Pol ));
			wait until (( Blank'Event ) and ( Blank = not( Pol )));
			wait for 1 us;

			Pol <= '1';
			wait for 1 us;
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '0';
			wait until (( Blank'Event ) and ( Blank = Pol ));
			wait until (( Blank'Event ) and ( Blank = not( Pol )));
			wait for 1 us;
			
			

		----------------------------------------------------------------------------------
		
		----------------------------------------------------------------------------------
		assert false
			report "check Blank Delay"
			severity note;
		for i in 0 to 7 loop
			delay(i) <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '0';
			wait until (( Blank'Event ) and ( Blank = Pol ));
			wait until (( Blank'Event ) and ( Blank = not( Pol )));
			delay(i) <= '0';
			wait for 1 us;
		end loop;
		----------------------------------------------------------------------------------

		----------------------------------------------------------------------------------
		assert false
			report "check Blank Width"
			severity note;
		Width <= x"0000";
		Delay <= x"0000";
		wait for 1 us;
		for i in 0 to 7 loop
			Width(i) <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '1';
			wait until((clk'event) and ( clk = '1' ));
			fdisc <= '0';
			wait until (( Blank'Event ) and ( Blank = Pol ));
			wait until (( Blank'Event ) and ( Blank = not( Pol )));
			Width(i) <= '0';
			wait for 1 us;
		end loop;
		----------------------------------------------------------------------------------


		assert false
			report "End of Simulations"
			severity failure;
		
		wait;
	end process;
------------------------------------------------------------------------------------
end test_Bench;               -- fir_adisc
------------------------------------------------------------------------------------

