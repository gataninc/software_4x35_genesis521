---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fifo.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--
--   History:       <date> - <Author>
--		07/05/06 : MCS Ver 4x21
--			Changes made in FIFO in DSO mode to facilitate better DSO and FFT Modes of operation
--			DSO Mode given its own keys in the FIFO
--			FF_CNT incoroprated to count the number of words written to the FIFO only for DSO Mode
--			each 1/8 of full fifo will cause na interrupt to the DSP to start reading out the FIFO Data
--		06/26/06 : MCS Ver 4x19
--			Preset_DOne_Cnt_Reg is enabled when PRESET_OUT = 0 AND PS_OUT_LSM = 0
--			in other words, allowed to be updated when not near the Preset Done Condition
--			Set order of Peak Data higher priority then Preset Done
--			FIFO Word counter added to maintain the number of words written to the FIFO
--			during DSO Mode
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity fifo is 
	port( 
		IMR				: in		std_logic;					-- Master Reset
		CLK20			: in		std_logic;					-- Master Clock
		LS_MAP_EN			: in		std_Logic;
		FF_FF			: in		std_logic;					-- fifo Full Flag
		WE_fifo_HI		: in		std_logic;					-- fifo Write Hi
		BIT_ENABLE		: in		std_logic;					-- BIT Mode Enable
		DSO_Enable		: in		std_logic;					-- DSO Mode Enable
		Time_Enable		: in		std_logic;					-- Acquisition Enabled
		Blevel_En			: in		std_logic;					-- Baseline Histogram Mode
		Meas_Done			: in		std_logic;					-- Measurement Done - Normal pulse processing
		Preset_out		: in		std_logic;					-- Preset Done	- Normal pulse processing
		Blevel_Upd		: in		std_logic;					-- Baseline Level Update - during Normal Pulse Processing
		WE_DSO_START		: in		std_logic;					-- DSO Start
		PBusy			: in		std_logic;					-- Pulse Busy	- DSO
		WE_Preset_Done_Cnt	: in		std_logic;
		DSO_TRIG_VEC 		: in		std_logic_Vector(  5 downto 0 );
		DSO_Trig			: in		std_logic_vector(  3 downto 0 );	-- DSO Trigger Select
		DSO_INT			: in		std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
		TEST_REG			: in		std_logic_Vector( 17 downto 0 );
		CPEAK			: in		std_logic_vector( 11 downto 0 );	-- Current Peak Value
		BLEVEL			: in		std_logic_vector(  7 downto 0 );	-- Baseline Level
		Debug			: in		std_logic_vector( 15 downto 0 );	-- DSO Data Port
		Preset_Done_Cnt	: buffer	std_logic_vector( 15 downto 0 );
		FF_DONE			: buffer	std_logic;					-- fifo Write Done
		FF_CNT			: buffer	std_logic_Vector( 15 downto 0 );
		FF_WR			: buffer	std_logic;					-- fifo Write Signal
		FF_WDATA			: buffer	std_logic_vector( 17 downto 0 ) );	-- fifo Data
end fifo;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of fifo is
	-- FIFO State Machine State Constants --------------------------------------------------------
	constant ST_IDLE			: integer := 0; 
	constant ST_PEAK			: integer := 1;
	constant ST_PEAK_LSM		: integer := 2;
	constant ST_PSET			: integer := 3;
	constant ST_PSET_LSM		: integer := 4;
	constant ST_BLDATA			: integer := 5;
	constant ST_DSO_START		: integer := 6;
	constant ST_DSO			: integer := 7;
	constant ST_DSP			: integer := 8;
	constant ST_DONE			: integer := 9;
	-- FIFO State Machine State Constants --------------------------------------------------------

	-- DSO Trigger Constants ---------------------------------------------------------------------
	constant DSO_TRIG_V0 		: integer := 0;
	constant DSO_TRIG_V1 		: integer := 1;
	constant DSO_TRIG_V2 		: integer := 2;
	constant DSO_TRIG_V3 		: integer := 3;
	constant DSO_TRIG_V4 		: integer := 4;
	constant DSO_TRIG_V5 		: integer := 5;
	constant DSO_TRIG_INT  		: integer := 8;
	constant DSO_TRIG_INT_PBUSY 	: integer := 9;
	-- DSO Trigger Constants ---------------------------------------------------------------------

	-- Signal Declarations -----------------------------------------------------------------------
	signal BL_Start			: std_logic;

	signal CPeak_Reg			: std_logic_vector( 11 downto 0 );

	signal DSO_Started			: std_logic;
	signal DSO_INT_CNT			: std_logic_Vector( 15 downto 0 );
	signal DSO_INT_TC			: std_logic;
	signal dso_int_clr			: std_logic;
	signal dso_int_cen			: std_logic;
	signal DSO_INT_Reg			: std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
	signal DSP_FF_WR			: std_logic;
	
	signal fifo_FF				: std_logic;
	signal FF_STATE 			: integer range 0 to 15;
	signal FF_CNT_TC			: std_logic;
	signal FF_CNT_CLR			: std_Logic;
	signal FF_CNT_EN 			: std_Logic;

	signal PDone_Reg			: std_logic;
	signal PDone_Reg_LSM		: std_logic;
	signal Preset_Done_Cnt_Clr	: std_logic;
	signal Preset_Done_Cnt_Reg 	: std_logic_vector( 15 downto 0 );
	signal PS_Out				: std_logic;
	signal PS_OUT_LSM			: std_logic;
	signal Preset_Done_Reg_CEN	: std_logic;
	

begin
	fifo_FF		<= not( FF_FF );
	--------------------------------------------------------------------------------------------------			
	-- Latch Measured Peak to prevent it from getting corrupt during processing ----------------------	
	CPeak_Reg_FF : lpm_Ff
		generic map(
			LPM_WIDTH			=> 12,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			enable			=> Meas_Done,
			data				=> CPeak,
			q				=> Cpeak_Reg );
	-- Latch Measured Peak to prevent it from getting corrupt during processing ----------------------	
	--------------------------------------------------------------------------------------------------			
	
	--------------------------------------------------------------------------------------------------			
	-- FIFO Counter used for DSO Mode ----------------------------------------------------------------
	FF_CNT_CLR 	<= '1' when ( FF_STATE = ST_DSO_START ) else '0';

	FF_CNT_EN 	<= '1' when (( FF_STATE = ST_DSO ) and ( FF_WR = '0' ) and ( FIFO_FF = '0' ) and ( FF_CNT_TC = '0' )) else '0';		
	
	FF_CNT_CNTR : lpm_counter
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "UP" )
		port map(
			aclr				=> imr,
			clock			=> CLK20,
			sclr				=> FF_CNT_CLR,
			cnt_en			=> FF_CNT_EN,
			q				=> FF_CNT );
			
	FF_CNT_COMPARE : lpm_Compare
		generic map(
			LPM_WIDTh			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> FF_CNT,
			datab			=> COnv_Std_logic_Vector( 16384, 16 ),
			ageb				=> FF_CNT_TC );
			
	-- FIFO Counter used for DSO Mode ----------------------------------------------------------------
	--------------------------------------------------------------------------------------------------			
			
	--------------------------------------------------------------------------------------------------			
	-- DSO Interval Timer ----------------------------------------------------------------------------
	-- Pipeline Interval Count to aid in local timing.
	DSO_INT_REG_FF : lpm_Ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> CLK20,
			data				=> DSO_Int,
			q				=> DSO_INT_Reg );

	DSO_INT_CLR	<= '1' when ( FF_STATE = ST_DSO_START ) else
				   '1' when (( conv_integer( DSO_TRIG ) = DSO_TRIG_INT 	 ) and ( DSO_INT_TC = '1' )) else
				   '1' when (( conv_integer( DSO_TRIG ) = DSO_TRIG_INT_PBUSY ) and ( DSO_INT_TC = '1' ) and ( PBUSY = '0' )) else 
				   '0';
				
	DSO_INT_Cen	<= '1' when (( DSO_Enable = '1' ) and ( conv_integer( DSO_TRIG ) = DSO_TRIG_INT  	  )) else
				   '1' when (( DSO_Enable = '1' ) and ( conv_integer( DSO_TRIG ) = DSO_TRIG_INT_PBUSY ) and ( PBUSY = '0' )) else 
				   '0';
						
	DSO_INT_CNTR : lpm_counter
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_COUNTER" )
		port map(
			aclr				=> imr,
			clock			=> CLK20,
			sclr				=> DSO_INT_CLR,
			cnt_en			=> DSO_INT_Cen,
			q				=> dso_int_cnt );
			
	DSO_INT_COMPARE : lpm_Compare
		generic map(
			LPM_WIDTh			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> dso_int_cnt,
			datab			=> DSO_INT_Reg,
			ageb				=> DSO_INT_TC );
			
	-- DSO Interval Timer ----------------------------------------------------------------------------
	--------------------------------------------------------------------------------------------------			

			
	--------------------------------------------------------------------------------------------------			
	-- Preset Done Counter used for Live Spectrum Mapping -------------------------
	Preset_Done_Cnt_Clr <= '1' when (( LS_Map_En = '0' ) or ( WE_Preset_Done_Cnt = '1' )) else '0';

	Preset_Done_Reg_CEN <= '1' when (( Preset_Out = '0' ) And ( PS_OUT_LSM = '0' )) else '0';	-- Ver 4x19
	
	Preset_Done_Cntr : lpm_counter
		generic map(
			LPM_WIDTH	=> 16 )
		port map(
			aclr		=> imr,
			clock	=> clk20,
			sclr		=> Preset_Done_Cnt_Clr,
			cnt_en	=> Preset_Out,
			q		=> PReset_DOne_Cnt );

	Preset_Done_Cnt_FF : lpm_ff
		generic map(
			LPM_WIDTH	=> 16 )
		port map(
			aclr		=> imr,
			clock	=> clk20,
			enable	=> Preset_Done_Reg_Cen,
			data		=> Preset_Done_Cnt,
			q		=> Preset_DOne_Cnt_Reg );
	-- Preset Done Counter used for Live Spectrum Mapping -------------------------
	--------------------------------------------------------------------------------------------------			

	--------------------------------------------------------------------------------------------------			
	FF_PROC : process( CLK20, IMR )
	begin
		if( IMR = '1' ) then
			DSP_FF_WR 	<= '0';
			PDone_Reg		<= '0';
			PDone_Reg_LSM	<= '0';
			PS_OUT		<= '0';
			PS_OUT_LSM	<= '0';
			BL_START		<= '0';
			DSO_STARTED	<= '0';
			FF_DONE		<= '0';
			FF_STATE 		<= ST_IDLE;
			FF_WDATA 		<= conv_std_logic_Vector( 0, 18 );
			FF_WR		<= '1';

		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			-- DSP Access -------------------------------------------------------------
			if(( BIT_ENABLE = '1' ) and ( FF_STATE = ST_DSP ))
				then DSP_FF_WR <= '0';
			elsif( WE_fifo_HI = '1' )
				then DSP_FF_WR <= '1';
			end if;
			-- DSP Access -------------------------------------------------------------

			-- Peak Done -------------------------------------------------------------
			if(( BIT_Enable = '0' ) and ( DSO_Enable = '0' ) and ( LS_MAP_EN = '0' ) and ( Time_Enable = '1' ) and ( MEAS_DONE = '1' )) 
				then PDone_Reg <= '1';
			elsif( FF_STATE = ST_PEAK )
				then PDone_Reg <= '0';
			end if;
			-- Peak Done -------------------------------------------------------------

			-- LSM Peak Done -------------------------------------------------------------
			if(( BIT_Enable = '0' ) and( DSO_Enable = '0' ) and ( LS_Map_En = '1' ) and ( Time_Enable = '1' ) and ( MEAS_DONE = '1' )) 
				then  PDone_Reg_LSM <= '1';
			elsif( FF_STATE = ST_PEAK_LSM )
				then PDone_Reg_LSM <= '0';
			end if;
			-- LSM Peak Done -------------------------------------------------------------

			-- Preset Done -----------------------------------------------------------
			if(( BIT_Enable = '0' ) and ( DSO_Enable = '0' ) and ( LS_Map_En = '0' ) and ( Preset_Out = '1' ))
				then PS_OUT <= '1';
			elsif( FF_STATE = ST_PSET )
				then PS_OUT <= '0';		
			end if;
			-- Preset Done -----------------------------------------------------------

			-- LSM Preset Done -----------------------------------------------------------
			if(( BIT_Enable = '0' ) and ( DSO_Enable = '0' ) and ( LS_Map_En = '1' ) and ( Preset_Out = '1' ))
				then PS_OUT_LSM <= '1';
			elsif( FF_STATE = ST_PSET_LSM )
				then PS_OUT_LSM <= '0';
			end if;
			-- LSM Preset Done -----------------------------------------------------------

			-- Baseline Histogram Mode -----------------------------------------------
			if(( Bit_Enable = '0' ) and ( DSO_Enable = '0' ) and ( LS_Map_En = '0' ) and ( BLevel_En = '1' ) and ( Blevel_Upd = '1' ))
				then BL_Start <= '1';
			elsif( FF_STATE = ST_BLDATA )
				then BL_START <= '0';
			end if;
			-- Baseline Histogram Mode -----------------------------------------------

			-- DSO Mode --------------------------------------------------------------
			if(( DSO_Enable = '1' ) and ( WE_DSO_START = '1' ))
				then DSO_STARTED <= '1';
			elsif( FF_STATE = ST_DSO_START )
				then DSO_STARTED <= '0';
			end if;
			-- DSO Mode --------------------------------------------------------------


			-- fifo Done indication --------------------------------------------------
			-- Used to generate an interrupt to the DSP
			if( FF_STATE = ST_DONE )
				then FF_DONE <= '1';
			elsif(( FF_STATE = ST_DSO ) and ( FF_WR = '0' ) and ( FF_CNT( 10 downto 0 ) = "11111111111" ))
				then FF_DONE <= '1';
				else FF_DONE <= '0';
			end if;
			-- fifo Done indication --------------------------------------------------
			

			-- fifo State Sequencer --------------------------------------------
			case FF_STATE is
				when ST_IDLE =>
						if( DSP_FF_WR = '1' )
							then FF_STATE <= ST_DSP;
						elsif( BL_START = '1' )				-- Baseline Data
							then FF_STATE <= ST_BLDATA;
						elsif( PDone_Reg = '1' )				-- Peak Done
							then FF_STATE <= ST_PEAK;
						elsif( PDone_Reg_LSM = '1' )
							then FF_STATE <= ST_PEAK_LSM;
						elsif( PS_OUT = '1' )				-- Preset Done
							then FF_STATE <= ST_PSET;
						elsif( PS_OUT_LSM = '1' )
							then FF_STATE <= ST_PSET_LSM;
						elsif( DSO_STARTED = '1' ) then
							case conv_integer( DSO_TRIG ) is
								when DSO_TRIG_V0 => if( DSO_TRIG_VEC(0) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when DSO_TRIG_V1 => if( DSO_TRIG_VEC(1) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when DSO_TRIG_V2 => if( DSO_TRIG_VEC(2) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when DSO_TRIG_V3 => if( DSO_TRIG_VEC(3) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when DSO_TRIG_V4 => if( DSO_TRIG_VEC(4) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when DSO_TRIG_V5 => if( DSO_TRIG_VEC(5) = '1' ) then FF_STATE <= ST_DSO_START; end if;
								when others	  => FF_STATE <= ST_DSO_START;
							end case;
						else 
							FF_STATE <= ST_IDLE;

						end if;
				when ST_DSP		=> if( fifo_FF = '0' ) then FF_STATE <= ST_DONE; end if;

				when ST_BLDATA 	=> if( fifo_FF = '0' ) then FF_STATE <= ST_DONE; end if;

				when ST_PEAK 		=> if( fifo_FF = '0' ) then FF_STATE <= ST_DONE; end if;
				when ST_PSET		=> if( fifo_FF = '0' ) then FF_STATE <= ST_DONE; end if;

				when ST_PEAK_LSM 	=> if( fifo_FF = '0' ) then FF_STATE <= ST_IDLE; end if;
				when ST_PSET_LSM	=> if( fifo_FF = '0' ) then FF_STATE <= ST_DONE; end if;

				when ST_DSO_START	=> FF_STATE <= ST_DSO;
				when ST_DSO  		=> if( FF_CNT_TC = '1' ) then FF_STATE <= ST_DONE; end if;

				when ST_DONE		=> FF_STATE <= ST_IDLE;
				when others		=> FF_STATE <= ST_IDLE;
			end case;
			-- fifo State Sequencer --------------------------------------------
			
			-- fifo Write Signal Generation ------------------------------------
			if( FIFO_FF = '0' ) then
				case FF_STATE is
					when ST_DSP		=> FF_WR <= '0'; 
					when ST_PEAK		=> FF_WR <= '0'; 
					when ST_PEAK_LSM	=> FF_WR <= '0'; 
					when ST_PSET		=> FF_WR <= '0'; 
					when ST_PSET_LSM	=> FF_WR <= '0'; 
					when ST_BLDATA		=> FF_WR <= '0'; 
					when ST_DSO		=> case conv_integer( DSO_Trig ) is
									        	when DSO_TRIG_V3 		=> if( blevel_upd = '1' )
																		then FF_WR <= '0'; 
																		else FF_WR <= '1'; 
																   end if;
											when DSO_TRIG_V4 		=> if( blevel_upd = '1' )
																		then FF_WR <= '0'; 
																		else FF_WR <= '1'; 
																   end if;
											when DSO_TRIG_INT 		=> if( DSO_INT_TC = '1' )
																		then FF_WR <= '0'; 
																		else FF_WR <= '1'; 
																   end if;
											when DSO_TRIG_INT_PBUSY 	=> if(( DSO_INT_TC = '1' ) and ( PBusy = '0' )) 
																		then FF_WR <= '0'; 
																		else FF_WR <= '1';
																   end if;
											when others 			=> FF_WR <= '0';
									   end case;
					when others		=> FF_WR <= '1';
				end case;
			else
				FF_WR <= '1';
			end if;
			-- fifo Write Signal Generation ------------------------------------

			-- fifo DAta Generation --------------------------------------------
			case FF_STATE is 
				when ST_DSP		=> FF_WDATA <= TEST_REG( 17 downto 0 );
				when ST_PEAK  		=> FF_WDATA <= "00" & x"0" & CPEAK_Reg;
				when ST_PSET   	=> FF_WDATA <= "00" & x"8000";
				when ST_BLDATA 	=> FF_WDATA <= "01" & x"0" & "000" & not( BLEVEL(7)) & BLEVEL( 7 downto  0 );

				when ST_PEAK_LSM  	=> FF_WDATA <= "00" & x"0" & CPEAK_Reg;
				when ST_PSET_LSM   	=> FF_WDATA <= "10" & Preset_Done_Cnt_Reg;				

				when ST_DSO    	=> FF_WDATA <= "01" & debug;
  				when others		=> FF_WDATA <= "00" & x"0000";
			end case;
			-- fifo DAta Generation --------------------------------------------
		end if;
	end process;
---------------------------------------------------------------------------------------------------			
end behavioral;			-- fifo.VHD
---------------------------------------------------------------------------------------------------			
