 ---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DS2401.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal imr 			: std_logic := '1';
	signal clk20 			: std_logic := '0';
	signal FIFO_FF			: std_logic;					-- FIFO Full Flag
	signal WE_FIFO_HI		: std_logic;					-- FIFO Write Hi
	signal BIT_ENABLE		: std_logic;					-- BIT Mode Enable
	signal DSO_Enable		: std_logic;					-- DSO Mode Enable
	signal Time_Enable		: std_logic;					-- Acquisition Enabled
	signal Blevel_En		: std_logic;					-- Baseline Histogram Mode
	signal LS_Map_Mode		: std_logic;					-- Live Spectrum Map Mode
	signal Meas_Done		: std_logic;					-- Measurement Done - Normal pulse processing
	signal Preset_out		: std_logic;					-- Preset Done	- Normal pulse processing
	signal Blevel_Upd		: std_logic;					-- Baseline Level Update - during Normal Pulse Processing
	signal WE_DSO_START		: std_logic;					-- DSO Start
	signal PBusy			: std_logic;					-- Pulse Busy	- DSO
	signal DSO_TRIG_VEC 	: std_logic_Vector( 5 downto 0 );
	signal DSO_Trig		: std_logic_vector(  3 downto 0 );	-- DSO Trigger Select
	signal DSO_INT			: std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
	signal TEST_REG		: std_logic_Vector( 17 downto 0 );
	signal CPEAK			: std_logic_vector( 11 downto 0 );	-- Current Peak Value
	signal BLEVEL			: std_logic_vector(  7 downto 0 );	-- Baseline Level
	signal Debug			: std_logic_vector( 16 downto 0 );	-- DSO Data Port
	signal FF_DONE			: std_logic;					-- FIFO Write Done
	signal FF_WR			: std_logic;					-- FIFO Write Signal
	signal FF_WDATA		: std_logic_vector( 17 downto 0 );	-- FIFO Data
	---------------------------------------------------------------------------------------------------
	component FIFO port( 
			IMR			: in		std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Master Clock
			FIFO_FF		: in		std_logic;					-- FIFO Full Flag
			WE_FIFO_HI	: in		std_logic;					-- FIFO Write Hi
			BIT_ENABLE	: in		std_logic;					-- BIT Mode Enable
			DSO_Enable	: in		std_logic;					-- DSO Mode Enable
			Time_Enable	: in		std_logic;					-- Acquisition Enabled
			Blevel_En		: in		std_logic;					-- Baseline Histogram Mode
			LS_Map_Mode	: in		std_logic;					-- Live Spectrum Map Mode			
			Meas_Done		: in		std_logic;					-- Measurement Done - Normal pulse processing
			Preset_out	: in		std_logic;					-- Preset Done	- Normal pulse processing
			Blevel_Upd	: in		std_logic;					-- Baseline Level Update - during Normal Pulse Processing
			WE_DSO_START	: in		std_logic;					-- DSO Start
			PBusy		: in		std_logic;					-- Pulse Busy	- DSO
			DSO_TRIG_VEC 	: in		std_logic_Vector( 5 downto 0 );
			DSO_Trig		: in		std_logic_vector(  3 downto 0 );	-- DSO Trigger Select
			DSO_INT		: in		std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
			TEST_REG		: in		std_logic_Vector( 17 downto 0 );
			CPEAK		: in		std_logic_vector( 11 downto 0 );	-- Current Peak Value
			BLEVEL		: in		std_logic_vector(  7 downto 0 );	-- Baseline Level
			Debug		: in		std_logic_vector( 16 downto 0 );	-- DSO Data Port
			FF_DONE		: out	std_logic;					-- FIFO Write Done
			FF_WR		: out	std_logic;					-- FIFO Write Signal
			FF_WDATA		: out	std_logic_vector( 17 downto 0 ) );	-- FIFO Data
	end component FIFO;
	---------------------------------------------------------------------------------------------------
begin
	imr 		<= '0' after 555 ns;
	clk20	<= not( clk20 ) after 25 ns;
	
	U : FIFO
		port map(
			imr			=> imr,
			clk20		=> clk20,
			FIFO_FF		=> FIFO_FF,
			WE_FIFO_HI	=> WE_FIFO_HI,
			BIT_ENABLE	=> BIT_ENABLE,
			DSO_Enable	=> DSO_Enable,
			Time_Enable	=> Time_Enable,
			Blevel_En		=> BLevel_en,
			LS_Map_Mode	=> LS_Map_Mode,
			Meas_Done		=> Meas_Done,
			Preset_out	=> Preset_Out,
			Blevel_Upd	=> Blevel_Upd,
			WE_DSO_START	=> WE_DSO_Start,
			PBusy		=> PBusy,
			DSO_TRIG_VEC 	=> DSO_Trig_Vec,
			DSO_Trig		=> DSO_Trig,
			DSO_INT		=> DSO_Int,
			TEST_REG		=> Test_Reg,
			CPEAK		=> CPeak,
			BLEVEL		=> Blevel,
			Debug		=> Debug,
			FF_DONE		=> FF_Done,
			FF_WR		=> FF_WR,
			FF_WDATA		=> FF_WDATA );

	FIFO_PROC : process
	begin
		FIFO_FF		<= '0';
		WE_FIFO_HI	<= '0';
		BIT_ENABLE	<= '0';
		DSO_Enable	<= '0';
		Time_Enable	<= '0';
		Blevel_En		<= '0';
		LS_Map_Mode	<= '0';
		Meas_Done		<= '0';
		Preset_out	<= '0';
		Blevel_Upd	<= '0';
		WE_DSO_START	<= '0';
		PBusy		<= '0';
		DSO_TRIG_VEC 	<= "011001";		-- Bits 5:0
		DSO_Trig		<= x"0";
		DSO_INT		<= x"0000";
		TEST_REG		<= conv_std_logic_Vector( 0, 18 );
		CPEAK		<= x"000";
		BLEVEL		<= x"00";
		Debug		<= conv_std_logic_Vector( 0, 17 );
		
		wait for 1 us;
		
		-----------------------------------------------------------------------------
		assert false
			report "Test DSP Interface"
			severity note;
		BIT_ENABLE <= '1';
		for i in 0 to 17 loop
			TEST_REG(i) <= '1';
			wait until (( Clk20'event ) and ( clk20 = '1' ));
			WE_FIFO_HI <= '1';
			wait until (( Clk20'event ) and ( clk20 = '1' ));
			WE_FIFO_HI <= '0';
			wait for 200 ns;
			TEST_REG(i) <= '0';
			wait for 200 ns;
		end loop;
		BIT_ENABLE <= '0';
		-----------------------------------------------------------------------------
			
		-----------------------------------------------------------------------------
		assert false
			report "Test Peak Processing"
			severity note;
			
		BIT_ENABLE <= '0';
		DSO_ENABLE <= '0';
		TIME_ENABLE <= '1';
		
		for i in 0 to 11 loop
			CPEAK(i) <= '1';
			wait until (( Clk20'event ) and ( clk20 = '1' ));
			MEAS_DONE <= '1';
			wait until (( Clk20'event ) and ( clk20 = '1' ));
			MEAS_DONE <= '0';
			wait for 200 ns;
			CPEAK(i) <= '0';
			wait for 200 ns;
		end loop;
		BIT_ENABLE <= '0';
		DSO_ENABLE <= '0';
		TIME_ENABLE <= '0';
		-----------------------------------------------------------------------------
		
		-----------------------------------------------------------------------------
		assert false
			report "Test Preset Done Processing"
			severity note;
			
		BIT_ENABLE <= '0';
		DSO_ENABLE <= '0';
		
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		Preset_out <= '1';
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		Preset_out <= '0';
		wait for 200 ns;
		BIT_ENABLE <= '0';
		DSO_ENABLE <= '0';
		-----------------------------------------------------------------------------

		-----------------------------------------------------------------------------
		assert false
			report "Test BLM Processing"
			severity note;
			
		BIT_ENABLE 	<= '0';
		DSO_ENABLE 	<= '0';
		BLEVEL_EN		<= '1';
		LS_MAP_MODE	<= '0';
		
		
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		Blevel_Upd <= '1';
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		Blevel_Upd <= '0';
		wait for 200 ns;
		BIT_ENABLE 	<= '0';
		DSO_ENABLE 	<= '0';
		BLEVEL_EN		<= '0';
		LS_MAP_MODE	<= '0';
		-----------------------------------------------------------------------------
		-----------------------------------------------------------------------------
		assert false
			report "Test DSO Processing"
			severity note;
			
		BIT_ENABLE 	<= '0';
		DSO_ENABLE 	<= '1';
		FIFO_FF		<= '0';
		
		
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		WE_DSO_START <= '1';
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		WE_DSO_START <= '0';
		wait for 200 ns;
		for i in 0 to 40 loop
			wait until (( Clk20'event ) and ( clk20 = '1' ));
			debug <= conv_std_logic_vector( i, 17 );
		end loop;
		
		wait until (( Clk20'event ) and ( clk20 = '1' ));
		FIFO_FF 	<= '1';
		wait for 400 ns;
		FIFO_FF 	<= '0';
		BIT_ENABLE 	<= '0';
		DSO_ENABLE 	<= '0';
		-----------------------------------------------------------------------------
		-----------------------------------------------------------------------------
		assert false
			report "End of Simulation"
			severity failure;
		wait ;
		
		
	end process;
			
---------------------------------------------------------------------------------------------------
end test_bench;			-- DS2401.VHD
---------------------------------------------------------------------------------------------------

