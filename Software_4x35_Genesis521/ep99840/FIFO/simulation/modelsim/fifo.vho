-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 15:57:36"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fifo IS
    PORT (
	CLK20 : IN std_logic;
	IMR : IN std_logic;
	FIFO_FF : IN std_logic;
	DSO_Trig : IN std_logic_vector(3 DOWNTO 0);
	Blevel_Upd : IN std_logic;
	DSO_INT : IN std_logic_vector(15 DOWNTO 0);
	PBusy : IN std_logic;
	TEST_REG : IN std_logic_vector(17 DOWNTO 0);
	Debug : IN std_logic_vector(16 DOWNTO 0);
	BLEVEL : IN std_logic_vector(7 DOWNTO 0);
	DSO_TRIG_VEC : IN std_logic_vector(5 DOWNTO 0);
	CPEAK : IN std_logic_vector(11 DOWNTO 0);
	Meas_Done : IN std_logic;
	Blevel_En : IN std_logic;
	BIT_ENABLE : IN std_logic;
	DSO_Enable : IN std_logic;
	LS_Map_Mode : IN std_logic;
	Preset_out : IN std_logic;
	WE_FIFO_HI : IN std_logic;
	WE_DSO_START : IN std_logic;
	Time_Enable : IN std_logic;
	FF_DONE : OUT std_logic;
	FF_WR : OUT std_logic;
	FF_WDATA : OUT std_logic_vector(17 DOWNTO 0)
	);
END fifo;

ARCHITECTURE structure OF fifo IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_FIFO_FF : std_logic;
SIGNAL ww_DSO_Trig : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_Blevel_Upd : std_logic;
SIGNAL ww_DSO_INT : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_PBusy : std_logic;
SIGNAL ww_TEST_REG : std_logic_vector(17 DOWNTO 0);
SIGNAL ww_Debug : std_logic_vector(16 DOWNTO 0);
SIGNAL ww_BLEVEL : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_DSO_TRIG_VEC : std_logic_vector(5 DOWNTO 0);
SIGNAL ww_CPEAK : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_Meas_Done : std_logic;
SIGNAL ww_Blevel_En : std_logic;
SIGNAL ww_BIT_ENABLE : std_logic;
SIGNAL ww_DSO_Enable : std_logic;
SIGNAL ww_LS_Map_Mode : std_logic;
SIGNAL ww_Preset_out : std_logic;
SIGNAL ww_WE_FIFO_HI : std_logic;
SIGNAL ww_WE_DSO_START : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_FF_DONE : std_logic;
SIGNAL ww_FF_WR : std_logic;
SIGNAL ww_FF_WDATA : std_logic_vector(17 DOWNTO 0);
SIGNAL i_a856_1 : std_logic;
SIGNAL Select_251_rtl_0_rtl_25_a281_1 : std_logic;
SIGNAL CLK20_apadio : std_logic;
SIGNAL IMR_apadio : std_logic;
SIGNAL FIFO_FF_apadio : std_logic;
SIGNAL DSO_Trig_a0_a_apadio : std_logic;
SIGNAL DSO_Trig_a1_a_apadio : std_logic;
SIGNAL DSO_Trig_a2_a_apadio : std_logic;
SIGNAL DSO_Trig_a3_a_apadio : std_logic;
SIGNAL Blevel_Upd_apadio : std_logic;
SIGNAL DSO_INT_a4_a_apadio : std_logic;
SIGNAL DSO_INT_a1_a_apadio : std_logic;
SIGNAL DSO_INT_a9_a_apadio : std_logic;
SIGNAL DSO_INT_a8_a_apadio : std_logic;
SIGNAL DSO_INT_a2_a_apadio : std_logic;
SIGNAL DSO_INT_a0_a_apadio : std_logic;
SIGNAL DSO_INT_a11_a_apadio : std_logic;
SIGNAL DSO_INT_a10_a_apadio : std_logic;
SIGNAL DSO_INT_a12_a_apadio : std_logic;
SIGNAL DSO_INT_a5_a_apadio : std_logic;
SIGNAL DSO_INT_a3_a_apadio : std_logic;
SIGNAL DSO_INT_a15_a_apadio : std_logic;
SIGNAL DSO_INT_a13_a_apadio : std_logic;
SIGNAL DSO_INT_a7_a_apadio : std_logic;
SIGNAL DSO_INT_a14_a_apadio : std_logic;
SIGNAL DSO_INT_a6_a_apadio : std_logic;
SIGNAL PBusy_apadio : std_logic;
SIGNAL TEST_REG_a17_a_apadio : std_logic;
SIGNAL Debug_a16_a_apadio : std_logic;
SIGNAL TEST_REG_a16_a_apadio : std_logic;
SIGNAL Debug_a15_a_apadio : std_logic;
SIGNAL TEST_REG_a15_a_apadio : std_logic;
SIGNAL Debug_a14_a_apadio : std_logic;
SIGNAL TEST_REG_a14_a_apadio : std_logic;
SIGNAL Debug_a13_a_apadio : std_logic;
SIGNAL TEST_REG_a13_a_apadio : std_logic;
SIGNAL Debug_a12_a_apadio : std_logic;
SIGNAL TEST_REG_a12_a_apadio : std_logic;
SIGNAL Debug_a11_a_apadio : std_logic;
SIGNAL TEST_REG_a11_a_apadio : std_logic;
SIGNAL Debug_a10_a_apadio : std_logic;
SIGNAL TEST_REG_a10_a_apadio : std_logic;
SIGNAL Debug_a9_a_apadio : std_logic;
SIGNAL TEST_REG_a9_a_apadio : std_logic;
SIGNAL TEST_REG_a8_a_apadio : std_logic;
SIGNAL Debug_a8_a_apadio : std_logic;
SIGNAL BLEVEL_a7_a_apadio : std_logic;
SIGNAL TEST_REG_a7_a_apadio : std_logic;
SIGNAL Debug_a7_a_apadio : std_logic;
SIGNAL BLEVEL_a6_a_apadio : std_logic;
SIGNAL Debug_a6_a_apadio : std_logic;
SIGNAL TEST_REG_a6_a_apadio : std_logic;
SIGNAL BLEVEL_a5_a_apadio : std_logic;
SIGNAL Debug_a5_a_apadio : std_logic;
SIGNAL TEST_REG_a5_a_apadio : std_logic;
SIGNAL BLEVEL_a4_a_apadio : std_logic;
SIGNAL Debug_a4_a_apadio : std_logic;
SIGNAL TEST_REG_a4_a_apadio : std_logic;
SIGNAL BLEVEL_a3_a_apadio : std_logic;
SIGNAL Debug_a3_a_apadio : std_logic;
SIGNAL TEST_REG_a3_a_apadio : std_logic;
SIGNAL BLEVEL_a2_a_apadio : std_logic;
SIGNAL Debug_a2_a_apadio : std_logic;
SIGNAL TEST_REG_a2_a_apadio : std_logic;
SIGNAL BLEVEL_a1_a_apadio : std_logic;
SIGNAL Debug_a1_a_apadio : std_logic;
SIGNAL TEST_REG_a1_a_apadio : std_logic;
SIGNAL BLEVEL_a0_a_apadio : std_logic;
SIGNAL Debug_a0_a_apadio : std_logic;
SIGNAL TEST_REG_a0_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a2_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a3_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a4_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a5_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a0_a_apadio : std_logic;
SIGNAL DSO_TRIG_VEC_a1_a_apadio : std_logic;
SIGNAL CPEAK_a11_a_apadio : std_logic;
SIGNAL Meas_Done_apadio : std_logic;
SIGNAL CPEAK_a10_a_apadio : std_logic;
SIGNAL CPEAK_a9_a_apadio : std_logic;
SIGNAL CPEAK_a8_a_apadio : std_logic;
SIGNAL CPEAK_a7_a_apadio : std_logic;
SIGNAL CPEAK_a6_a_apadio : std_logic;
SIGNAL CPEAK_a5_a_apadio : std_logic;
SIGNAL CPEAK_a4_a_apadio : std_logic;
SIGNAL CPEAK_a3_a_apadio : std_logic;
SIGNAL CPEAK_a2_a_apadio : std_logic;
SIGNAL CPEAK_a1_a_apadio : std_logic;
SIGNAL CPEAK_a0_a_apadio : std_logic;
SIGNAL Blevel_En_apadio : std_logic;
SIGNAL BIT_ENABLE_apadio : std_logic;
SIGNAL DSO_Enable_apadio : std_logic;
SIGNAL LS_Map_Mode_apadio : std_logic;
SIGNAL Preset_out_apadio : std_logic;
SIGNAL WE_FIFO_HI_apadio : std_logic;
SIGNAL WE_DSO_START_apadio : std_logic;
SIGNAL Time_Enable_apadio : std_logic;
SIGNAL FF_DONE_apadio : std_logic;
SIGNAL FF_WR_apadio : std_logic;
SIGNAL FF_WDATA_a17_a_apadio : std_logic;
SIGNAL FF_WDATA_a16_a_apadio : std_logic;
SIGNAL FF_WDATA_a15_a_apadio : std_logic;
SIGNAL FF_WDATA_a14_a_apadio : std_logic;
SIGNAL FF_WDATA_a13_a_apadio : std_logic;
SIGNAL FF_WDATA_a12_a_apadio : std_logic;
SIGNAL FF_WDATA_a11_a_apadio : std_logic;
SIGNAL FF_WDATA_a10_a_apadio : std_logic;
SIGNAL FF_WDATA_a9_a_apadio : std_logic;
SIGNAL FF_WDATA_a8_a_apadio : std_logic;
SIGNAL FF_WDATA_a7_a_apadio : std_logic;
SIGNAL FF_WDATA_a6_a_apadio : std_logic;
SIGNAL FF_WDATA_a5_a_apadio : std_logic;
SIGNAL FF_WDATA_a4_a_apadio : std_logic;
SIGNAL FF_WDATA_a3_a_apadio : std_logic;
SIGNAL FF_WDATA_a2_a_apadio : std_logic;
SIGNAL FF_WDATA_a1_a_apadio : std_logic;
SIGNAL FF_WDATA_a0_a_apadio : std_logic;
SIGNAL Meas_Done_acombout : std_logic;
SIGNAL Time_Enable_acombout : std_logic;
SIGNAL i_a993 : std_logic;
SIGNAL BIT_ENABLE_acombout : std_logic;
SIGNAL DSO_Enable_acombout : std_logic;
SIGNAL i_a854 : std_logic;
SIGNAL FIFO_FF_acombout : std_logic;
SIGNAL DSO_TRIG_VEC_a3_a_acombout : std_logic;
SIGNAL DSO_Trig_a0_a_acombout : std_logic;
SIGNAL DSO_TRIG_VEC_a2_a_acombout : std_logic;
SIGNAL rtl_a80 : std_logic;
SIGNAL DSO_Trig_a2_a_acombout : std_logic;
SIGNAL DSO_Trig_a1_a_acombout : std_logic;
SIGNAL DSO_TRIG_VEC_a1_a_acombout : std_logic;
SIGNAL DSO_TRIG_VEC_a0_a_acombout : std_logic;
SIGNAL rtl_a75 : std_logic;
SIGNAL i_a860 : std_logic;
SIGNAL DSO_TRIG_VEC_a5_a_acombout : std_logic;
SIGNAL DSO_TRIG_VEC_a4_a_acombout : std_logic;
SIGNAL i_a857 : std_logic;
SIGNAL i_a852 : std_logic;
SIGNAL DSO_Trig_a3_a_acombout : std_logic;
SIGNAL WE_DSO_START_acombout : std_logic;
SIGNAL i_a1004 : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL DSO_Started : std_logic;
SIGNAL i_a960 : std_logic;
SIGNAL i_a855 : std_logic;
SIGNAL rtl_a24 : std_logic;
SIGNAL i_a858 : std_logic;
SIGNAL i_a965 : std_logic;
SIGNAL Select_253_rtl_2_rtl_27_a403 : std_logic;
SIGNAL Select_253_rtl_2_rtl_27_a395 : std_logic;
SIGNAL Blevel_En_acombout : std_logic;
SIGNAL Blevel_Upd_acombout : std_logic;
SIGNAL i_a1011 : std_logic;
SIGNAL LS_Map_Mode_acombout : std_logic;
SIGNAL WE_FIFO_HI_acombout : std_logic;
SIGNAL FF_STATE_a13 : std_logic;
SIGNAL DSP_FF_WR : std_logic;
SIGNAL Select_252_rtl_1_rtl_26_a415 : std_logic;
SIGNAL Select_250_rtl_24_a117 : std_logic;
SIGNAL i_a523 : std_logic;
SIGNAL rtl_a90 : std_logic;
SIGNAL FF_STATE_a8 : std_logic;
SIGNAL Select_253_rtl_2_rtl_27_a47 : std_logic;
SIGNAL Select_253_rtl_2_rtl_27_a46 : std_logic;
SIGNAL i_a856 : std_logic;
SIGNAL Select_253_rtl_2_rtl_27_a430 : std_logic;
SIGNAL FF_STATE_a11 : std_logic;
SIGNAL BL_Start : std_logic;
SIGNAL Select_252_rtl_1_rtl_26_a446 : std_logic;
SIGNAL Select_252_rtl_1_rtl_26_a445 : std_logic;
SIGNAL Select_252_rtl_1_rtl_26_a66 : std_logic;
SIGNAL Select_252_rtl_1_rtl_26_a65 : std_logic;
SIGNAL FF_STATE_a10 : std_logic;
SIGNAL Preset_out_acombout : std_logic;
SIGNAL PS_Out : std_logic;
SIGNAL Select_251_rtl_0_rtl_25_a281 : std_logic;
SIGNAL Select_251_rtl_0_rtl_25_a286 : std_logic;
SIGNAL FF_STATE_a9 : std_logic;
SIGNAL PD_Reg : std_logic;
SIGNAL i_a303 : std_logic;
SIGNAL Select_251_rtl_0_rtl_25_a280 : std_logic;
SIGNAL Select_254_rtl_3_a1 : std_logic;
SIGNAL FF_STATE_a12 : std_logic;
SIGNAL i_a894 : std_logic;
SIGNAL FF_STATE_a14 : std_logic;
SIGNAL FF_DONE_areg0 : std_logic;
SIGNAL PBusy_acombout : std_logic;
SIGNAL dso_int_cen_a4 : std_logic;
SIGNAL i_a1024 : std_logic;
SIGNAL i_a1023 : std_logic;
SIGNAL DSO_INT_a6_a_acombout : std_logic;
SIGNAL DSO_INT_a14_a_acombout : std_logic;
SIGNAL DSO_INT_a0_a_acombout : std_logic;
SIGNAL dso_int_clr_a23 : std_logic;
SIGNAL dso_int_cen_a14 : std_logic;
SIGNAL dso_int_clr_a12 : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL DSO_INT_a2_a_acombout : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL reduce_nor_58_a27 : std_logic;
SIGNAL DSO_INT_a4_a_acombout : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL DSO_INT_a1_a_acombout : std_logic;
SIGNAL reduce_nor_58_a19 : std_logic;
SIGNAL DSO_INT_a8_a_acombout : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL DSO_INT_a9_a_acombout : std_logic;
SIGNAL reduce_nor_58_a22 : std_logic;
SIGNAL DSO_INT_a11_a_acombout : std_logic;
SIGNAL DSO_INT_a10_a_acombout : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL reduce_nor_58_a34 : std_logic;
SIGNAL reduce_nor_58_a99 : std_logic;
SIGNAL reduce_nor_58_a117 : std_logic;
SIGNAL dso_int_cen_a33 : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL reduce_nor_58_a82 : std_logic;
SIGNAL DSO_INT_a7_a_acombout : std_logic;
SIGNAL DSO_INT_a13_a_acombout : std_logic;
SIGNAL reduce_nor_58_a67 : std_logic;
SIGNAL DSO_INT_a3_a_acombout : std_logic;
SIGNAL DSO_INT_a15_a_acombout : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL DSO_INT_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL reduce_nor_58_a54 : std_logic;
SIGNAL DSO_INT_a12_a_acombout : std_logic;
SIGNAL DSO_INT_a5_a_acombout : std_logic;
SIGNAL reduce_nor_58_a43 : std_logic;
SIGNAL reduce_nor_58_a104 : std_logic;
SIGNAL reduce_nor_58 : std_logic;
SIGNAL i_a498 : std_logic;
SIGNAL i_a935 : std_logic;
SIGNAL i_a951 : std_logic;
SIGNAL FF_WR_areg0 : std_logic;
SIGNAL TEST_REG_a17_a_acombout : std_logic;
SIGNAL FF_WDATA_a17_a_areg0 : std_logic;
SIGNAL TEST_REG_a16_a_acombout : std_logic;
SIGNAL Debug_a16_a_acombout : std_logic;
SIGNAL Select_295_rtl_7_a1 : std_logic;
SIGNAL FF_WDATA_a16_a_areg0 : std_logic;
SIGNAL TEST_REG_a15_a_acombout : std_logic;
SIGNAL Debug_a15_a_acombout : std_logic;
SIGNAL Select_297_rtl_8_a1 : std_logic;
SIGNAL FF_WDATA_a15_a_areg0 : std_logic;
SIGNAL Debug_a14_a_acombout : std_logic;
SIGNAL TEST_REG_a14_a_acombout : std_logic;
SIGNAL FF_WDATA_a14_a_areg0 : std_logic;
SIGNAL TEST_REG_a13_a_acombout : std_logic;
SIGNAL Debug_a13_a_acombout : std_logic;
SIGNAL FF_WDATA_a13_a_areg0 : std_logic;
SIGNAL TEST_REG_a12_a_acombout : std_logic;
SIGNAL Debug_a12_a_acombout : std_logic;
SIGNAL FF_WDATA_a12_a_areg0 : std_logic;
SIGNAL CPEAK_a11_a_acombout : std_logic;
SIGNAL CPeak_Reg_a11_a : std_logic;
SIGNAL Debug_a11_a_acombout : std_logic;
SIGNAL TEST_REG_a11_a_acombout : std_logic;
SIGNAL Select_305_rtl_12_a6 : std_logic;
SIGNAL FF_WDATA_a11_a_areg0 : std_logic;
SIGNAL CPEAK_a10_a_acombout : std_logic;
SIGNAL CPeak_Reg_a10_a : std_logic;
SIGNAL Debug_a10_a_acombout : std_logic;
SIGNAL TEST_REG_a10_a_acombout : std_logic;
SIGNAL Select_307_rtl_13_a6 : std_logic;
SIGNAL FF_WDATA_a10_a_areg0 : std_logic;
SIGNAL CPEAK_a9_a_acombout : std_logic;
SIGNAL CPeak_Reg_a9_a : std_logic;
SIGNAL Debug_a9_a_acombout : std_logic;
SIGNAL TEST_REG_a9_a_acombout : std_logic;
SIGNAL Select_309_rtl_14_a6 : std_logic;
SIGNAL FF_WDATA_a9_a_areg0 : std_logic;
SIGNAL BLEVEL_a7_a_acombout : std_logic;
SIGNAL TEST_REG_a8_a_acombout : std_logic;
SIGNAL Select_311_rtl_15_a4 : std_logic;
SIGNAL Debug_a8_a_acombout : std_logic;
SIGNAL CPEAK_a8_a_acombout : std_logic;
SIGNAL CPeak_Reg_a8_a : std_logic;
SIGNAL Select_311_rtl_15_a11 : std_logic;
SIGNAL FF_WDATA_a8_a_areg0 : std_logic;
SIGNAL CPEAK_a7_a_acombout : std_logic;
SIGNAL CPeak_Reg_a7_a : std_logic;
SIGNAL Debug_a7_a_acombout : std_logic;
SIGNAL Select_313_rtl_16_a11 : std_logic;
SIGNAL TEST_REG_a7_a_acombout : std_logic;
SIGNAL Select_313_rtl_16_a4 : std_logic;
SIGNAL FF_WDATA_a7_a_areg0 : std_logic;
SIGNAL BLEVEL_a6_a_acombout : std_logic;
SIGNAL Select_315_rtl_17_a2 : std_logic;
SIGNAL TEST_REG_a6_a_acombout : std_logic;
SIGNAL Debug_a6_a_acombout : std_logic;
SIGNAL CPEAK_a6_a_acombout : std_logic;
SIGNAL CPeak_Reg_a6_a : std_logic;
SIGNAL Select_315_rtl_17_a11 : std_logic;
SIGNAL FF_WDATA_a6_a_areg0 : std_logic;
SIGNAL TEST_REG_a5_a_acombout : std_logic;
SIGNAL BLEVEL_a5_a_acombout : std_logic;
SIGNAL Select_317_rtl_18_a2 : std_logic;
SIGNAL Debug_a5_a_acombout : std_logic;
SIGNAL CPEAK_a5_a_acombout : std_logic;
SIGNAL CPeak_Reg_a5_a : std_logic;
SIGNAL Select_317_rtl_18_a11 : std_logic;
SIGNAL FF_WDATA_a5_a_areg0 : std_logic;
SIGNAL Debug_a4_a_acombout : std_logic;
SIGNAL CPEAK_a4_a_acombout : std_logic;
SIGNAL CPeak_Reg_a4_a : std_logic;
SIGNAL Select_319_rtl_19_a11 : std_logic;
SIGNAL TEST_REG_a4_a_acombout : std_logic;
SIGNAL BLEVEL_a4_a_acombout : std_logic;
SIGNAL Select_319_rtl_19_a2 : std_logic;
SIGNAL FF_WDATA_a4_a_areg0 : std_logic;
SIGNAL TEST_REG_a3_a_acombout : std_logic;
SIGNAL Debug_a3_a_acombout : std_logic;
SIGNAL CPEAK_a3_a_acombout : std_logic;
SIGNAL CPeak_Reg_a3_a : std_logic;
SIGNAL Select_321_rtl_20_a11 : std_logic;
SIGNAL BLEVEL_a3_a_acombout : std_logic;
SIGNAL Select_321_rtl_20_a2 : std_logic;
SIGNAL FF_WDATA_a3_a_areg0 : std_logic;
SIGNAL BLEVEL_a2_a_acombout : std_logic;
SIGNAL Select_323_rtl_21_a2 : std_logic;
SIGNAL TEST_REG_a2_a_acombout : std_logic;
SIGNAL Debug_a2_a_acombout : std_logic;
SIGNAL CPEAK_a2_a_acombout : std_logic;
SIGNAL CPeak_Reg_a2_a : std_logic;
SIGNAL Select_323_rtl_21_a11 : std_logic;
SIGNAL FF_WDATA_a2_a_areg0 : std_logic;
SIGNAL Debug_a1_a_acombout : std_logic;
SIGNAL CPEAK_a1_a_acombout : std_logic;
SIGNAL CPeak_Reg_a1_a : std_logic;
SIGNAL Select_325_rtl_22_a11 : std_logic;
SIGNAL TEST_REG_a1_a_acombout : std_logic;
SIGNAL BLEVEL_a1_a_acombout : std_logic;
SIGNAL Select_325_rtl_22_a2 : std_logic;
SIGNAL FF_WDATA_a1_a_areg0 : std_logic;
SIGNAL BLEVEL_a0_a_acombout : std_logic;
SIGNAL Select_327_rtl_23_a2 : std_logic;
SIGNAL TEST_REG_a0_a_acombout : std_logic;
SIGNAL Debug_a0_a_acombout : std_logic;
SIGNAL CPEAK_a0_a_acombout : std_logic;
SIGNAL CPeak_Reg_a0_a : std_logic;
SIGNAL Select_327_rtl_23_a11 : std_logic;
SIGNAL FF_WDATA_a0_a_areg0 : std_logic;
SIGNAL NOT_FF_WR_areg0 : std_logic;

BEGIN

ww_CLK20 <= CLK20;
ww_IMR <= IMR;
ww_FIFO_FF <= FIFO_FF;
ww_DSO_Trig <= DSO_Trig;
ww_Blevel_Upd <= Blevel_Upd;
ww_DSO_INT <= DSO_INT;
ww_PBusy <= PBusy;
ww_TEST_REG <= TEST_REG;
ww_Debug <= Debug;
ww_BLEVEL <= BLEVEL;
ww_DSO_TRIG_VEC <= DSO_TRIG_VEC;
ww_CPEAK <= CPEAK;
ww_Meas_Done <= Meas_Done;
ww_Blevel_En <= Blevel_En;
ww_BIT_ENABLE <= BIT_ENABLE;
ww_DSO_Enable <= DSO_Enable;
ww_LS_Map_Mode <= LS_Map_Mode;
ww_Preset_out <= Preset_out;
ww_WE_FIFO_HI <= WE_FIFO_HI;
ww_WE_DSO_START <= WE_DSO_START;
ww_Time_Enable <= Time_Enable;
FF_DONE <= ww_FF_DONE;
FF_WR <= ww_FF_WR;
FF_WDATA <= ww_FF_WDATA;
NOT_FF_WR_areg0 <= NOT FF_WR_areg0;

Meas_Done_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Meas_Done,
	combout => Meas_Done_acombout);

Time_Enable_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => Time_Enable_acombout);

i_a993_I : apex20ke_lcell 
-- Equation(s):
-- i_a993 = Meas_Done_acombout & Time_Enable_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Meas_Done_acombout,
	datad => Time_Enable_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a993);

BIT_ENABLE_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_ENABLE,
	combout => BIT_ENABLE_acombout);

DSO_Enable_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_Enable,
	combout => DSO_Enable_acombout);

i_a854_I : apex20ke_lcell 
-- Equation(s):
-- i_a854 = !BIT_ENABLE_acombout & !DSO_Enable_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => BIT_ENABLE_acombout,
	datad => DSO_Enable_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a854);

FIFO_FF_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FIFO_FF,
	combout => FIFO_FF_acombout);

DSO_TRIG_VEC_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(3),
	combout => DSO_TRIG_VEC_a3_a_acombout);

DSO_Trig_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_Trig(0),
	combout => DSO_Trig_a0_a_acombout);

DSO_TRIG_VEC_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(2),
	combout => DSO_TRIG_VEC_a2_a_acombout);

rtl_a80_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a80 = DSO_TRIG_VEC_a3_a_acombout & !DSO_Trig_a0_a_acombout & !DSO_TRIG_VEC_a2_a_acombout # !DSO_TRIG_VEC_a3_a_acombout & (DSO_Trig_a0_a_acombout # !DSO_TRIG_VEC_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "505F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_TRIG_VEC_a3_a_acombout,
	datac => DSO_Trig_a0_a_acombout,
	datad => DSO_TRIG_VEC_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a80);

DSO_Trig_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_Trig(2),
	combout => DSO_Trig_a2_a_acombout);

DSO_Trig_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_Trig(1),
	combout => DSO_Trig_a1_a_acombout);

DSO_TRIG_VEC_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(1),
	combout => DSO_TRIG_VEC_a1_a_acombout);

DSO_TRIG_VEC_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(0),
	combout => DSO_TRIG_VEC_a0_a_acombout);

rtl_a75_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a75 = DSO_TRIG_VEC_a1_a_acombout & !DSO_Trig_a0_a_acombout & !DSO_TRIG_VEC_a0_a_acombout # !DSO_TRIG_VEC_a1_a_acombout & (DSO_Trig_a0_a_acombout # !DSO_TRIG_VEC_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "303F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSO_TRIG_VEC_a1_a_acombout,
	datac => DSO_Trig_a0_a_acombout,
	datad => DSO_TRIG_VEC_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a75);

i_a860_I : apex20ke_lcell 
-- Equation(s):
-- i_a860 = !DSO_Trig_a2_a_acombout & (DSO_Trig_a1_a_acombout & rtl_a80 # !DSO_Trig_a1_a_acombout & rtl_a75)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2320",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a80,
	datab => DSO_Trig_a2_a_acombout,
	datac => DSO_Trig_a1_a_acombout,
	datad => rtl_a75,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a860);

DSO_TRIG_VEC_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(5),
	combout => DSO_TRIG_VEC_a5_a_acombout);

DSO_TRIG_VEC_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_TRIG_VEC(4),
	combout => DSO_TRIG_VEC_a4_a_acombout);

i_a857_I : apex20ke_lcell 
-- Equation(s):
-- i_a857 = !DSO_Trig_a1_a_acombout & DSO_Trig_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Trig_a1_a_acombout,
	datad => DSO_Trig_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a857);

i_a852_I : apex20ke_lcell 
-- Equation(s):
-- i_a852 = i_a857 & (DSO_Trig_a0_a_acombout & !DSO_TRIG_VEC_a5_a_acombout # !DSO_Trig_a0_a_acombout & !DSO_TRIG_VEC_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_TRIG_VEC_a5_a_acombout,
	datab => DSO_TRIG_VEC_a4_a_acombout,
	datac => DSO_Trig_a0_a_acombout,
	datad => i_a857,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a852);

DSO_Trig_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_Trig(3),
	combout => DSO_Trig_a3_a_acombout);

WE_DSO_START_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_DSO_START,
	combout => WE_DSO_START_acombout);

i_a1004_I : apex20ke_lcell 
-- Equation(s):
-- i_a1004 = DSO_Enable_acombout & WE_DSO_START_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Enable_acombout,
	datad => WE_DSO_START_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a1004);

CLK20_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

DSO_Started_aI : apex20ke_lcell 
-- Equation(s):
-- DSO_Started = DFFE(BIT_ENABLE_acombout & !FF_STATE_a12 & DSO_Started # !BIT_ENABLE_acombout & (i_a1004 # !FF_STATE_a12 & DSO_Started), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7530",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => BIT_ENABLE_acombout,
	datab => FF_STATE_a12,
	datac => DSO_Started,
	datad => i_a1004,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_Started);

i_a960_I : apex20ke_lcell 
-- Equation(s):
-- i_a960 = !DSO_Trig_a3_a_acombout & FF_STATE_a9 & DSO_Started

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_Trig_a3_a_acombout,
	datac => FF_STATE_a9,
	datad => DSO_Started,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a960);

i_a855_I : apex20ke_lcell 
-- Equation(s):
-- i_a855 = !DSO_Trig_a2_a_acombout & !DSO_Trig_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Trig_a2_a_acombout,
	datad => DSO_Trig_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a855);

rtl_a24_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a24 = DSO_Trig_a0_a_acombout & DSO_TRIG_VEC_a5_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Trig_a0_a_acombout,
	datad => DSO_TRIG_VEC_a5_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a24);

i_a858_I : apex20ke_lcell 
-- Equation(s):
-- i_a858 = !DSO_Trig_a2_a_acombout & DSO_Trig_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Trig_a2_a_acombout,
	datad => DSO_Trig_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a858);

i_a965_I : apex20ke_lcell 
-- Equation(s):
-- i_a965 = DSO_Trig_a2_a_acombout & !DSO_Trig_a1_a_acombout & (DSO_Trig_a0_a_acombout # !DSO_TRIG_VEC_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "080C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_Trig_a0_a_acombout,
	datab => DSO_Trig_a2_a_acombout,
	datac => DSO_Trig_a1_a_acombout,
	datad => DSO_TRIG_VEC_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a965);

Select_253_rtl_2_rtl_27_a403_I : apex20ke_lcell 
-- Equation(s):
-- Select_253_rtl_2_rtl_27_a403 = rtl_a24 & i_a858 & rtl_a80 # !rtl_a24 & (i_a965 # i_a858 & rtl_a80)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D5C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a24,
	datab => i_a858,
	datac => rtl_a80,
	datad => i_a965,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_253_rtl_2_rtl_27_a403);

Select_253_rtl_2_rtl_27_a395_I : apex20ke_lcell 
-- Equation(s):
-- Select_253_rtl_2_rtl_27_a395 = !DSO_Trig_a3_a_acombout & (Select_253_rtl_2_rtl_27_a403 # i_a855 & rtl_a75)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3320",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a855,
	datab => DSO_Trig_a3_a_acombout,
	datac => rtl_a75,
	datad => Select_253_rtl_2_rtl_27_a403,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_253_rtl_2_rtl_27_a395);

Blevel_En_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Blevel_En,
	combout => Blevel_En_acombout);

Blevel_Upd_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Blevel_Upd,
	combout => Blevel_Upd_acombout);

i_a1011_I : apex20ke_lcell 
-- Equation(s):
-- i_a1011 = !BIT_ENABLE_acombout & !DSO_Enable_acombout & Blevel_En_acombout & Blevel_Upd_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => BIT_ENABLE_acombout,
	datab => DSO_Enable_acombout,
	datac => Blevel_En_acombout,
	datad => Blevel_Upd_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a1011);

LS_Map_Mode_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_LS_Map_Mode,
	combout => LS_Map_Mode_acombout);

WE_FIFO_HI_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_FIFO_HI,
	combout => WE_FIFO_HI_acombout);

FF_STATE_a13_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a13 = DFFE(FF_STATE_a8 & FF_STATE_a13 & FIFO_FF_acombout # !FF_STATE_a8 & (DSP_FF_WR # FF_STATE_a13 & FIFO_FF_acombout), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F444",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a8,
	datab => DSP_FF_WR,
	datac => FF_STATE_a13,
	datad => FIFO_FF_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a13);

DSP_FF_WR_aI : apex20ke_lcell 
-- Equation(s):
-- DSP_FF_WR = DFFE(WE_FIFO_HI_acombout & (BIT_ENABLE_acombout # !FF_STATE_a13 & DSP_FF_WR) # !WE_FIFO_HI_acombout & !FF_STATE_a13 & DSP_FF_WR, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8F88",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => WE_FIFO_HI_acombout,
	datab => BIT_ENABLE_acombout,
	datac => FF_STATE_a13,
	datad => DSP_FF_WR,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSP_FF_WR);

Select_252_rtl_1_rtl_26_a415_I : apex20ke_lcell 
-- Equation(s):
-- Select_252_rtl_1_rtl_26_a415 = !DSP_FF_WR & !FF_STATE_a8
-- Select_252_rtl_1_rtl_26_a446 = !DSP_FF_WR & !FF_STATE_a8

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSP_FF_WR,
	datad => FF_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_252_rtl_1_rtl_26_a415,
	cascout => Select_252_rtl_1_rtl_26_a446);

Select_250_rtl_24_a117_I : apex20ke_lcell 
-- Equation(s):
-- Select_250_rtl_24_a117 = Select_252_rtl_1_rtl_26_a415 & !PS_Out & !BL_Start & !PD_Reg

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_252_rtl_1_rtl_26_a415,
	datab => PS_Out,
	datac => BL_Start,
	datad => PD_Reg,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_250_rtl_24_a117);

i_a523_I : apex20ke_lcell 
-- Equation(s):
-- i_a523 = DSO_Trig_a1_a_acombout & rtl_a80 # !DSO_Trig_a1_a_acombout & rtl_a75

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F5A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_Trig_a1_a_acombout,
	datac => rtl_a80,
	datad => rtl_a75,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a523);

rtl_a90_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a90 = !DSO_Trig_a3_a_acombout & (i_a852 # i_a523 & !DSO_Trig_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F02",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a523,
	datab => DSO_Trig_a2_a_acombout,
	datac => DSO_Trig_a3_a_acombout,
	datad => i_a852,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a90);

FF_STATE_a8_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a8 = DFFE(!FF_STATE_a14 & (!rtl_a90 & DSO_Started # !Select_250_rtl_24_a117), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0705",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_250_rtl_24_a117,
	datab => rtl_a90,
	datac => FF_STATE_a14,
	datad => DSO_Started,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a8);

Select_253_rtl_2_rtl_27_a47_I : apex20ke_lcell 
-- Equation(s):
-- Select_253_rtl_2_rtl_27_a47 = !FF_STATE_a8 & !PS_Out & BL_Start & !DSP_FF_WR

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0010",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a8,
	datab => PS_Out,
	datac => BL_Start,
	datad => DSP_FF_WR,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_253_rtl_2_rtl_27_a47);

Select_253_rtl_2_rtl_27_a46_I : apex20ke_lcell 
-- Equation(s):
-- Select_253_rtl_2_rtl_27_a46 = FF_STATE_a11 & FIFO_FF_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FF_STATE_a11,
	datad => FIFO_FF_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_253_rtl_2_rtl_27_a46);

i_a856_I : apex20ke_lcell 
-- Equation(s):
-- i_a856 = DSO_Started & !PD_Reg

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => DSO_Started,
	datad => PD_Reg,
	devclrn => devclrn,
	devpor => devpor,
	cascout => i_a856);

Select_253_rtl_2_rtl_27_a430_I : apex20ke_lcell 
-- Equation(s):
-- Select_253_rtl_2_rtl_27_a430 = (FF_STATE_a11 & !DSP_FF_WR & !FF_STATE_a8 & !PS_Out) & CASCADE(i_a856)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a11,
	datab => DSP_FF_WR,
	datac => FF_STATE_a8,
	datad => PS_Out,
	cascin => i_a856,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_253_rtl_2_rtl_27_a430);

FF_STATE_a11_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a11 = DFFE(Select_253_rtl_2_rtl_27_a47 # Select_253_rtl_2_rtl_27_a46 # Select_253_rtl_2_rtl_27_a395 & Select_253_rtl_2_rtl_27_a430, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEFA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_253_rtl_2_rtl_27_a47,
	datab => Select_253_rtl_2_rtl_27_a395,
	datac => Select_253_rtl_2_rtl_27_a46,
	datad => Select_253_rtl_2_rtl_27_a430,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a11);

BL_Start_aI : apex20ke_lcell 
-- Equation(s):
-- BL_Start = DFFE(i_a1011 & (BL_Start & !FF_STATE_a11 # !LS_Map_Mode_acombout) # !i_a1011 & BL_Start & !FF_STATE_a11, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0ACE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a1011,
	datab => BL_Start,
	datac => LS_Map_Mode_acombout,
	datad => FF_STATE_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => BL_Start);

Select_252_rtl_1_rtl_26_a445_I : apex20ke_lcell 
-- Equation(s):
-- Select_252_rtl_1_rtl_26_a445 = (!BL_Start & DSO_Started & FF_STATE_a10 & !PD_Reg) & CASCADE(Select_252_rtl_1_rtl_26_a446)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => BL_Start,
	datab => DSO_Started,
	datac => FF_STATE_a10,
	datad => PD_Reg,
	cascin => Select_252_rtl_1_rtl_26_a446,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_252_rtl_1_rtl_26_a445);

Select_252_rtl_1_rtl_26_a66_I : apex20ke_lcell 
-- Equation(s):
-- Select_252_rtl_1_rtl_26_a66 = !FF_STATE_a8 & !DSP_FF_WR & PS_Out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => FF_STATE_a8,
	datac => DSP_FF_WR,
	datad => PS_Out,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_252_rtl_1_rtl_26_a66);

Select_252_rtl_1_rtl_26_a65_I : apex20ke_lcell 
-- Equation(s):
-- Select_252_rtl_1_rtl_26_a65 = FIFO_FF_acombout & FF_STATE_a10

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FIFO_FF_acombout,
	datad => FF_STATE_a10,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_252_rtl_1_rtl_26_a65);

FF_STATE_a10_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a10 = DFFE(Select_252_rtl_1_rtl_26_a66 # Select_252_rtl_1_rtl_26_a65 # Select_253_rtl_2_rtl_27_a395 & Select_252_rtl_1_rtl_26_a445, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_253_rtl_2_rtl_27_a395,
	datab => Select_252_rtl_1_rtl_26_a445,
	datac => Select_252_rtl_1_rtl_26_a66,
	datad => Select_252_rtl_1_rtl_26_a65,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a10);

Preset_out_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Preset_out,
	combout => Preset_out_acombout);

PS_Out_aI : apex20ke_lcell 
-- Equation(s):
-- PS_Out = DFFE(FF_STATE_a10 & Preset_out_acombout & i_a854 # !FF_STATE_a10 & (PS_Out # Preset_out_acombout & i_a854), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "D5C0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a10,
	datab => Preset_out_acombout,
	datac => i_a854,
	datad => PS_Out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PS_Out);

Select_251_rtl_0_rtl_25_a281_I : apex20ke_lcell 
-- Equation(s):
-- Select_251_rtl_0_rtl_25_a281 = !PS_Out & !DSP_FF_WR & !FF_STATE_a8 & !BL_Start

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => PS_Out,
	datab => DSP_FF_WR,
	datac => FF_STATE_a8,
	datad => BL_Start,
	devclrn => devclrn,
	devpor => devpor,
	cascout => Select_251_rtl_0_rtl_25_a281);

Select_251_rtl_0_rtl_25_a286_I : apex20ke_lcell 
-- Equation(s):
-- Select_251_rtl_0_rtl_25_a286 = (PD_Reg # i_a960 & (i_a860 # i_a852)) & CASCADE(Select_251_rtl_0_rtl_25_a281)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a860,
	datab => i_a852,
	datac => PD_Reg,
	datad => i_a960,
	cascin => Select_251_rtl_0_rtl_25_a281,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_251_rtl_0_rtl_25_a286);

FF_STATE_a9_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a9 = DFFE(Select_251_rtl_0_rtl_25_a286 # FF_STATE_a9 & FIFO_FF_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a9,
	datac => FIFO_FF_acombout,
	datad => Select_251_rtl_0_rtl_25_a286,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a9);

PD_Reg_aI : apex20ke_lcell 
-- Equation(s):
-- PD_Reg = DFFE(PD_Reg & (i_a993 & i_a854 # !FF_STATE_a9) # !PD_Reg & i_a993 & i_a854, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0EA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => PD_Reg,
	datab => i_a993,
	datac => i_a854,
	datad => FF_STATE_a9,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => PD_Reg);

i_a303_I : apex20ke_lcell 
-- Equation(s):
-- i_a303 = !PD_Reg & DSO_Started & !BL_Start

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => PD_Reg,
	datac => DSO_Started,
	datad => BL_Start,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a303);

Select_251_rtl_0_rtl_25_a280_I : apex20ke_lcell 
-- Equation(s):
-- Select_251_rtl_0_rtl_25_a280 = !DSP_FF_WR & !FF_STATE_a8 & !PS_Out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_FF_WR,
	datac => FF_STATE_a8,
	datad => PS_Out,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_251_rtl_0_rtl_25_a280);

Select_254_rtl_3_a1_I : apex20ke_lcell 
-- Equation(s):
-- Select_254_rtl_3_a1 = !FIFO_FF_acombout & FF_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FIFO_FF_acombout,
	datad => FF_STATE_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_254_rtl_3_a1);

FF_STATE_a12_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a12 = DFFE(Select_254_rtl_3_a1 # i_a303 & Select_251_rtl_0_rtl_25_a280 & !rtl_a90, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF08",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a303,
	datab => Select_251_rtl_0_rtl_25_a280,
	datac => rtl_a90,
	datad => Select_254_rtl_3_a1,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a12);

i_a894_I : apex20ke_lcell 
-- Equation(s):
-- i_a894 = !FF_STATE_a10 & !FF_STATE_a11 & !FF_STATE_a9 & !FF_STATE_a13
-- i_a1024 = !FF_STATE_a10 & !FF_STATE_a11 & !FF_STATE_a9 & !FF_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a10,
	datab => FF_STATE_a11,
	datac => FF_STATE_a9,
	datad => FF_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a894,
	cascout => i_a1024);

FF_STATE_a14_I : apex20ke_lcell 
-- Equation(s):
-- FF_STATE_a14 = DFFE(FF_STATE_a12 & (FIFO_FF_acombout # !i_a894) # !FF_STATE_a12 & !FIFO_FF_acombout & !i_a894, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0CF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => FF_STATE_a12,
	datac => FIFO_FF_acombout,
	datad => i_a894,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_STATE_a14);

FF_DONE_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_DONE_areg0 = DFFE(FF_STATE_a14, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => FF_STATE_a14,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_DONE_areg0);

PBusy_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PBusy,
	combout => PBusy_acombout);

dso_int_cen_a4_I : apex20ke_lcell 
-- Equation(s):
-- dso_int_cen_a4 = !DSO_Trig_a0_a_acombout # !PBusy_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => PBusy_acombout,
	datad => DSO_Trig_a0_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => dso_int_cen_a4);

i_a1023_I : apex20ke_lcell 
-- Equation(s):
-- i_a1023 = (!DSO_Trig_a2_a_acombout & DSO_Trig_a3_a_acombout & !DSO_Trig_a1_a_acombout) & CASCADE(i_a1024)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_Trig_a2_a_acombout,
	datac => DSO_Trig_a3_a_acombout,
	datad => DSO_Trig_a1_a_acombout,
	cascin => i_a1024,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a1023);

DSO_INT_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(6),
	combout => DSO_INT_a6_a_acombout);

DSO_INT_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(14),
	combout => DSO_INT_a14_a_acombout);

DSO_INT_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(0),
	combout => DSO_INT_a0_a_acombout);

dso_int_clr_a23_I : apex20ke_lcell 
-- Equation(s):
-- dso_int_clr_a23 = !FF_STATE_a8 # !DSO_Trig_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSO_Trig_a3_a_acombout,
	datad => FF_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	combout => dso_int_clr_a23);

dso_int_cen_a14_I : apex20ke_lcell 
-- Equation(s):
-- dso_int_cen_a14 = dso_int_cen_a4 & !DSO_Trig_a1_a_acombout & !DSO_Trig_a2_a_acombout & FF_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a4,
	datab => DSO_Trig_a1_a_acombout,
	datac => DSO_Trig_a2_a_acombout,
	datad => FF_STATE_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => dso_int_cen_a14);

dso_int_clr_a12_I : apex20ke_lcell 
-- Equation(s):
-- dso_int_clr_a12 = dso_int_clr_a23 # dso_int_cen_a14 & !reduce_nor_58_a99 & !reduce_nor_58_a104

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAAE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_clr_a23,
	datab => dso_int_cen_a14,
	datac => reduce_nor_58_a99,
	datad => reduce_nor_58_a104,
	devclrn => devclrn,
	devpor => devpor,
	combout => dso_int_clr_a12);

DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(dso_int_clr_a12) & dso_int_cen_a33 $ DSO_INT_CNTR_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a0_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

DSO_INT_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(2),
	combout => DSO_INT_a2_a_acombout);

DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a1_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a1_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a1_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a2_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a2_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a2_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a2_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

reduce_nor_58_a27_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a27 = DSO_INT_a0_a_acombout & (DSO_INT_a2_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a2_a # !DSO_INT_CNTR_awysi_counter_asload_path_a0_a) # !DSO_INT_a0_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a0_a # DSO_INT_a2_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a0_a_acombout,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a0_a,
	datac => DSO_INT_a2_a_acombout,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a27);

DSO_INT_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(4),
	combout => DSO_INT_a4_a_acombout);

DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a3_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a3_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a3_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a4_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a4_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a4_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a4_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

DSO_INT_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(1),
	combout => DSO_INT_a1_a_acombout);

reduce_nor_58_a19_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a19 = DSO_INT_a4_a_acombout & (DSO_INT_a1_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a1_a # !DSO_INT_CNTR_awysi_counter_asload_path_a4_a) # !DSO_INT_a4_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a4_a # DSO_INT_a1_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a4_a_acombout,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a4_a,
	datac => DSO_INT_a1_a_acombout,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a19);

DSO_INT_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(8),
	combout => DSO_INT_a8_a_acombout);

DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a5_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a5_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a5_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a6_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a6_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a6_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a6_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a7_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a7_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a7_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a8_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a8_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a8_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a8_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a9_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a9_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a9_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

DSO_INT_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(9),
	combout => DSO_INT_a9_a_acombout);

reduce_nor_58_a22_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a22 = DSO_INT_a8_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a9_a $ DSO_INT_a9_a_acombout # !DSO_INT_CNTR_awysi_counter_asload_path_a8_a) # !DSO_INT_a8_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a8_a # DSO_INT_CNTR_awysi_counter_asload_path_a9_a $ DSO_INT_a9_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a8_a_acombout,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a9_a,
	datac => DSO_INT_a9_a_acombout,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a22);

DSO_INT_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(11),
	combout => DSO_INT_a11_a_acombout);

DSO_INT_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(10),
	combout => DSO_INT_a10_a_acombout);

DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a10_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a10_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a10_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a10_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a11_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a33,
	datab => DSO_INT_CNTR_awysi_counter_asload_path_a11_a,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a11_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

reduce_nor_58_a34_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a34 = DSO_INT_a11_a_acombout & (DSO_INT_a10_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a10_a # !DSO_INT_CNTR_awysi_counter_asload_path_a11_a) # !DSO_INT_a11_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a11_a # DSO_INT_a10_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a11_a_acombout,
	datab => DSO_INT_a10_a_acombout,
	datac => DSO_INT_CNTR_awysi_counter_asload_path_a10_a,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a34);

reduce_nor_58_a99_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a99 = reduce_nor_58_a27 # reduce_nor_58_a19 # reduce_nor_58_a22 # reduce_nor_58_a34

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_58_a27,
	datab => reduce_nor_58_a19,
	datac => reduce_nor_58_a22,
	datad => reduce_nor_58_a34,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a99);

reduce_nor_58_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58 = reduce_nor_58_a104 # reduce_nor_58_a99
-- reduce_nor_58_a117 = reduce_nor_58_a104 # reduce_nor_58_a99

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => reduce_nor_58_a104,
	datad => reduce_nor_58_a99,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58,
	cascout => reduce_nor_58_a117);

dso_int_cen_a33_I : apex20ke_lcell 
-- Equation(s):
-- dso_int_cen_a33 = (i_a855 & DSO_Trig_a3_a_acombout & FF_STATE_a12 & dso_int_cen_a4) & CASCADE(reduce_nor_58_a117)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a855,
	datab => DSO_Trig_a3_a_acombout,
	datac => FF_STATE_a12,
	datad => dso_int_cen_a4,
	cascin => reduce_nor_58_a117,
	devclrn => devclrn,
	devpor => devpor,
	combout => dso_int_cen_a33);

DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a12_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a12_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a12_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a12_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a13_a $ (dso_int_cen_a33 & DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !DSO_INT_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a13_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a13_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a14_a $ (dso_int_cen_a33 & !DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(DSO_INT_CNTR_awysi_counter_asload_path_a14_a & !DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a14_a,
	datab => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a14_a,
	cout => DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

reduce_nor_58_a82_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a82 = DSO_INT_a6_a_acombout & (DSO_INT_a14_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a14_a # !DSO_INT_CNTR_awysi_counter_asload_path_a6_a) # !DSO_INT_a6_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a6_a # DSO_INT_a14_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a6_a_acombout,
	datab => DSO_INT_a14_a_acombout,
	datac => DSO_INT_CNTR_awysi_counter_asload_path_a6_a,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a82);

DSO_INT_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(7),
	combout => DSO_INT_a7_a_acombout);

DSO_INT_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(13),
	combout => DSO_INT_a13_a_acombout);

reduce_nor_58_a67_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a67 = DSO_INT_a7_a_acombout & (DSO_INT_a13_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a13_a # !DSO_INT_CNTR_awysi_counter_asload_path_a7_a) # !DSO_INT_a7_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a7_a # DSO_INT_a13_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a7_a_acombout,
	datab => DSO_INT_a13_a_acombout,
	datac => DSO_INT_CNTR_awysi_counter_asload_path_a7_a,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a67);

DSO_INT_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(3),
	combout => DSO_INT_a3_a_acombout);

DSO_INT_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(15),
	combout => DSO_INT_a15_a_acombout);

DSO_INT_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- DSO_INT_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(dso_int_clr_a12) & DSO_INT_CNTR_awysi_counter_asload_path_a15_a $ (DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT & dso_int_cen_a33), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_CNTR_awysi_counter_asload_path_a15_a,
	datad => dso_int_cen_a33,
	cin => DSO_INT_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => dso_int_clr_a12,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSO_INT_CNTR_awysi_counter_asload_path_a15_a);

reduce_nor_58_a54_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a54 = DSO_INT_a3_a_acombout & (DSO_INT_a15_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a15_a # !DSO_INT_CNTR_awysi_counter_asload_path_a3_a) # !DSO_INT_a3_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a3_a # DSO_INT_a15_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a3_a_acombout,
	datab => DSO_INT_a15_a_acombout,
	datac => DSO_INT_CNTR_awysi_counter_asload_path_a3_a,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a54);

DSO_INT_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(12),
	combout => DSO_INT_a12_a_acombout);

DSO_INT_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_INT(5),
	combout => DSO_INT_a5_a_acombout);

reduce_nor_58_a43_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a43 = DSO_INT_a12_a_acombout & (DSO_INT_a5_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a5_a # !DSO_INT_CNTR_awysi_counter_asload_path_a12_a) # !DSO_INT_a12_a_acombout & (DSO_INT_CNTR_awysi_counter_asload_path_a12_a # DSO_INT_a5_a_acombout $ DSO_INT_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_INT_a12_a_acombout,
	datab => DSO_INT_a5_a_acombout,
	datac => DSO_INT_CNTR_awysi_counter_asload_path_a5_a,
	datad => DSO_INT_CNTR_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a43);

reduce_nor_58_a104_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_58_a104 = reduce_nor_58_a82 # reduce_nor_58_a67 # reduce_nor_58_a54 # reduce_nor_58_a43

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_58_a82,
	datab => reduce_nor_58_a67,
	datac => reduce_nor_58_a54,
	datad => reduce_nor_58_a43,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_58_a104);

i_a498_I : apex20ke_lcell 
-- Equation(s):
-- i_a498 = DSO_Trig_a2_a_acombout & !DSO_Trig_a0_a_acombout & !DSO_Trig_a1_a_acombout # !DSO_Trig_a2_a_acombout & DSO_Trig_a0_a_acombout & DSO_Trig_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4422",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSO_Trig_a2_a_acombout,
	datab => DSO_Trig_a0_a_acombout,
	datad => DSO_Trig_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a498);

i_a935_I : apex20ke_lcell 
-- Equation(s):
-- i_a935 = !Blevel_Upd_acombout & !DSO_Trig_a3_a_acombout & i_a498

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Blevel_Upd_acombout,
	datac => DSO_Trig_a3_a_acombout,
	datad => i_a498,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a935);

i_a951_I : apex20ke_lcell 
-- Equation(s):
-- i_a951 = FIFO_FF_acombout # i_a894 & (i_a935 # !FF_STATE_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a935,
	datab => FF_STATE_a12,
	datac => FIFO_FF_acombout,
	datad => i_a894,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a951);

FF_WR_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WR_areg0 = DFFE(!i_a951 & (dso_int_cen_a4 & !reduce_nor_58 # !i_a1023), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "003B",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => dso_int_cen_a4,
	datab => i_a1023,
	datac => reduce_nor_58,
	datad => i_a951,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WR_areg0);

TEST_REG_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(17),
	combout => TEST_REG_a17_a_acombout);

FF_WDATA_a17_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a17_a_areg0 = DFFE(FF_STATE_a12 # TEST_REG_a17_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datac => TEST_REG_a17_a_acombout,
	datad => FF_STATE_a13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a17_a_areg0);

TEST_REG_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(16),
	combout => TEST_REG_a16_a_acombout);

Debug_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(16),
	combout => Debug_a16_a_acombout);

Select_295_rtl_7_a1_I : apex20ke_lcell 
-- Equation(s):
-- Select_295_rtl_7_a1 = FF_STATE_a12 & Debug_a16_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FF_STATE_a12,
	datad => Debug_a16_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_295_rtl_7_a1);

FF_WDATA_a16_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a16_a_areg0 = DFFE(FF_STATE_a11 # Select_295_rtl_7_a1 # TEST_REG_a16_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TEST_REG_a16_a_acombout,
	datab => FF_STATE_a13,
	datac => FF_STATE_a11,
	datad => Select_295_rtl_7_a1,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a16_a_areg0);

TEST_REG_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(15),
	combout => TEST_REG_a15_a_acombout);

Debug_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(15),
	combout => Debug_a15_a_acombout);

Select_297_rtl_8_a1_I : apex20ke_lcell 
-- Equation(s):
-- Select_297_rtl_8_a1 = Debug_a15_a_acombout & FF_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Debug_a15_a_acombout,
	datad => FF_STATE_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_297_rtl_8_a1);

FF_WDATA_a15_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a15_a_areg0 = DFFE(FF_STATE_a10 # Select_297_rtl_8_a1 # TEST_REG_a15_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TEST_REG_a15_a_acombout,
	datab => FF_STATE_a13,
	datac => FF_STATE_a10,
	datad => Select_297_rtl_8_a1,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a15_a_areg0);

Debug_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(14),
	combout => Debug_a14_a_acombout);

TEST_REG_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(14),
	combout => TEST_REG_a14_a_acombout);

FF_WDATA_a14_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a14_a_areg0 = DFFE(Debug_a14_a_acombout & (FF_STATE_a12 # TEST_REG_a14_a_acombout & FF_STATE_a13) # !Debug_a14_a_acombout & TEST_REG_a14_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Debug_a14_a_acombout,
	datab => TEST_REG_a14_a_acombout,
	datac => FF_STATE_a12,
	datad => FF_STATE_a13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a14_a_areg0);

TEST_REG_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(13),
	combout => TEST_REG_a13_a_acombout);

Debug_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(13),
	combout => Debug_a13_a_acombout);

FF_WDATA_a13_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a13_a_areg0 = DFFE(FF_STATE_a13 & (TEST_REG_a13_a_acombout # FF_STATE_a12 & Debug_a13_a_acombout) # !FF_STATE_a13 & FF_STATE_a12 & Debug_a13_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a13,
	datab => TEST_REG_a13_a_acombout,
	datac => FF_STATE_a12,
	datad => Debug_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a13_a_areg0);

TEST_REG_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(12),
	combout => TEST_REG_a12_a_acombout);

Debug_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(12),
	combout => Debug_a12_a_acombout);

FF_WDATA_a12_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a12_a_areg0 = DFFE(FF_STATE_a12 & (Debug_a12_a_acombout # TEST_REG_a12_a_acombout & FF_STATE_a13) # !FF_STATE_a12 & TEST_REG_a12_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => TEST_REG_a12_a_acombout,
	datac => Debug_a12_a_acombout,
	datad => FF_STATE_a13,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a12_a_areg0);

CPEAK_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(11),
	combout => CPEAK_a11_a_acombout);

CPeak_Reg_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a11_a = DFFE(CPEAK_a11_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a11_a);

Debug_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(11),
	combout => Debug_a11_a_acombout);

TEST_REG_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(11),
	combout => TEST_REG_a11_a_acombout);

Select_305_rtl_12_a6_I : apex20ke_lcell 
-- Equation(s):
-- Select_305_rtl_12_a6 = Debug_a11_a_acombout & (FF_STATE_a12 # TEST_REG_a11_a_acombout & FF_STATE_a13) # !Debug_a11_a_acombout & TEST_REG_a11_a_acombout & FF_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Debug_a11_a_acombout,
	datab => TEST_REG_a11_a_acombout,
	datac => FF_STATE_a12,
	datad => FF_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_305_rtl_12_a6);

FF_WDATA_a11_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a11_a_areg0 = DFFE(Select_305_rtl_12_a6 # CPeak_Reg_a11_a & FF_STATE_a9, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CPeak_Reg_a11_a,
	datac => FF_STATE_a9,
	datad => Select_305_rtl_12_a6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a11_a_areg0);

CPEAK_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(10),
	combout => CPEAK_a10_a_acombout);

CPeak_Reg_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a10_a = DFFE(CPEAK_a10_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a10_a);

Debug_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(10),
	combout => Debug_a10_a_acombout);

TEST_REG_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(10),
	combout => TEST_REG_a10_a_acombout);

Select_307_rtl_13_a6_I : apex20ke_lcell 
-- Equation(s):
-- Select_307_rtl_13_a6 = Debug_a10_a_acombout & (FF_STATE_a12 # TEST_REG_a10_a_acombout & FF_STATE_a13) # !Debug_a10_a_acombout & TEST_REG_a10_a_acombout & FF_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Debug_a10_a_acombout,
	datab => FF_STATE_a12,
	datac => TEST_REG_a10_a_acombout,
	datad => FF_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_307_rtl_13_a6);

FF_WDATA_a10_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a10_a_areg0 = DFFE(Select_307_rtl_13_a6 # CPeak_Reg_a10_a & FF_STATE_a9, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => CPeak_Reg_a10_a,
	datac => FF_STATE_a9,
	datad => Select_307_rtl_13_a6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a10_a_areg0);

CPEAK_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(9),
	combout => CPEAK_a9_a_acombout);

CPeak_Reg_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a9_a = DFFE(CPEAK_a9_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a9_a);

Debug_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(9),
	combout => Debug_a9_a_acombout);

TEST_REG_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(9),
	combout => TEST_REG_a9_a_acombout);

Select_309_rtl_14_a6_I : apex20ke_lcell 
-- Equation(s):
-- Select_309_rtl_14_a6 = FF_STATE_a12 & (Debug_a9_a_acombout # FF_STATE_a13 & TEST_REG_a9_a_acombout) # !FF_STATE_a12 & FF_STATE_a13 & TEST_REG_a9_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => Debug_a9_a_acombout,
	datac => FF_STATE_a13,
	datad => TEST_REG_a9_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_309_rtl_14_a6);

FF_WDATA_a9_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a9_a_areg0 = DFFE(Select_309_rtl_14_a6 # FF_STATE_a9 & CPeak_Reg_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => FF_STATE_a9,
	datac => CPeak_Reg_a9_a,
	datad => Select_309_rtl_14_a6,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a9_a_areg0);

BLEVEL_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(7),
	combout => BLEVEL_a7_a_acombout);

TEST_REG_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(8),
	combout => TEST_REG_a8_a_acombout);

Select_311_rtl_15_a4_I : apex20ke_lcell 
-- Equation(s):
-- Select_311_rtl_15_a4 = TEST_REG_a8_a_acombout & FF_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TEST_REG_a8_a_acombout,
	datad => FF_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_311_rtl_15_a4);

Debug_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(8),
	combout => Debug_a8_a_acombout);

CPEAK_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(8),
	combout => CPEAK_a8_a_acombout);

CPeak_Reg_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a8_a = DFFE(CPEAK_a8_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a8_a);

Select_311_rtl_15_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_311_rtl_15_a11 = FF_STATE_a9 & (CPeak_Reg_a8_a # Debug_a8_a_acombout & FF_STATE_a12) # !FF_STATE_a9 & Debug_a8_a_acombout & FF_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a9,
	datab => Debug_a8_a_acombout,
	datac => FF_STATE_a12,
	datad => CPeak_Reg_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_311_rtl_15_a11);

FF_WDATA_a8_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a8_a_areg0 = DFFE(Select_311_rtl_15_a4 # Select_311_rtl_15_a11 # !BLEVEL_a7_a_acombout & FF_STATE_a11, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFDC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => BLEVEL_a7_a_acombout,
	datab => Select_311_rtl_15_a4,
	datac => FF_STATE_a11,
	datad => Select_311_rtl_15_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a8_a_areg0);

CPEAK_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(7),
	combout => CPEAK_a7_a_acombout);

CPeak_Reg_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a7_a = DFFE(CPEAK_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a7_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a7_a);

Debug_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(7),
	combout => Debug_a7_a_acombout);

Select_313_rtl_16_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_313_rtl_16_a11 = CPeak_Reg_a7_a & (FF_STATE_a9 # Debug_a7_a_acombout & FF_STATE_a12) # !CPeak_Reg_a7_a & Debug_a7_a_acombout & FF_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CPeak_Reg_a7_a,
	datab => FF_STATE_a9,
	datac => Debug_a7_a_acombout,
	datad => FF_STATE_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_313_rtl_16_a11);

TEST_REG_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(7),
	combout => TEST_REG_a7_a_acombout);

Select_313_rtl_16_a4_I : apex20ke_lcell 
-- Equation(s):
-- Select_313_rtl_16_a4 = TEST_REG_a7_a_acombout & FF_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => TEST_REG_a7_a_acombout,
	datad => FF_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_313_rtl_16_a4);

FF_WDATA_a7_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a7_a_areg0 = DFFE(Select_313_rtl_16_a11 # Select_313_rtl_16_a4 # BLEVEL_a7_a_acombout & FF_STATE_a11, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => BLEVEL_a7_a_acombout,
	datab => Select_313_rtl_16_a11,
	datac => FF_STATE_a11,
	datad => Select_313_rtl_16_a4,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a7_a_areg0);

BLEVEL_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(6),
	combout => BLEVEL_a6_a_acombout);

Select_315_rtl_17_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_315_rtl_17_a2 = FF_STATE_a11 & BLEVEL_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => FF_STATE_a11,
	datad => BLEVEL_a6_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_315_rtl_17_a2);

TEST_REG_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(6),
	combout => TEST_REG_a6_a_acombout);

Debug_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(6),
	combout => Debug_a6_a_acombout);

CPEAK_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(6),
	combout => CPEAK_a6_a_acombout);

CPeak_Reg_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a6_a = DFFE(CPEAK_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a6_a);

Select_315_rtl_17_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_315_rtl_17_a11 = FF_STATE_a12 & (Debug_a6_a_acombout # FF_STATE_a9 & CPeak_Reg_a6_a) # !FF_STATE_a12 & FF_STATE_a9 & CPeak_Reg_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => FF_STATE_a9,
	datac => Debug_a6_a_acombout,
	datad => CPeak_Reg_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_315_rtl_17_a11);

FF_WDATA_a6_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a6_a_areg0 = DFFE(Select_315_rtl_17_a2 # Select_315_rtl_17_a11 # TEST_REG_a6_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_315_rtl_17_a2,
	datab => TEST_REG_a6_a_acombout,
	datac => FF_STATE_a13,
	datad => Select_315_rtl_17_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a6_a_areg0);

TEST_REG_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(5),
	combout => TEST_REG_a5_a_acombout);

BLEVEL_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(5),
	combout => BLEVEL_a5_a_acombout);

Select_317_rtl_18_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_317_rtl_18_a2 = BLEVEL_a5_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => BLEVEL_a5_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_317_rtl_18_a2);

Debug_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(5),
	combout => Debug_a5_a_acombout);

CPEAK_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(5),
	combout => CPEAK_a5_a_acombout);

CPeak_Reg_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a5_a = DFFE(CPEAK_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a5_a);

Select_317_rtl_18_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_317_rtl_18_a11 = FF_STATE_a12 & (Debug_a5_a_acombout # FF_STATE_a9 & CPeak_Reg_a5_a) # !FF_STATE_a12 & FF_STATE_a9 & CPeak_Reg_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => FF_STATE_a9,
	datac => Debug_a5_a_acombout,
	datad => CPeak_Reg_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_317_rtl_18_a11);

FF_WDATA_a5_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a5_a_areg0 = DFFE(Select_317_rtl_18_a2 # Select_317_rtl_18_a11 # TEST_REG_a5_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TEST_REG_a5_a_acombout,
	datab => Select_317_rtl_18_a2,
	datac => FF_STATE_a13,
	datad => Select_317_rtl_18_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a5_a_areg0);

Debug_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(4),
	combout => Debug_a4_a_acombout);

CPEAK_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(4),
	combout => CPEAK_a4_a_acombout);

CPeak_Reg_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a4_a = DFFE(CPEAK_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a4_a);

Select_319_rtl_19_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_319_rtl_19_a11 = FF_STATE_a12 & (Debug_a4_a_acombout # FF_STATE_a9 & CPeak_Reg_a4_a) # !FF_STATE_a12 & FF_STATE_a9 & CPeak_Reg_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => FF_STATE_a9,
	datac => Debug_a4_a_acombout,
	datad => CPeak_Reg_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_319_rtl_19_a11);

TEST_REG_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(4),
	combout => TEST_REG_a4_a_acombout);

BLEVEL_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(4),
	combout => BLEVEL_a4_a_acombout);

Select_319_rtl_19_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_319_rtl_19_a2 = BLEVEL_a4_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => BLEVEL_a4_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_319_rtl_19_a2);

FF_WDATA_a4_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a4_a_areg0 = DFFE(Select_319_rtl_19_a11 # Select_319_rtl_19_a2 # TEST_REG_a4_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_319_rtl_19_a11,
	datab => TEST_REG_a4_a_acombout,
	datac => FF_STATE_a13,
	datad => Select_319_rtl_19_a2,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a4_a_areg0);

TEST_REG_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(3),
	combout => TEST_REG_a3_a_acombout);

Debug_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(3),
	combout => Debug_a3_a_acombout);

CPEAK_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(3),
	combout => CPEAK_a3_a_acombout);

CPeak_Reg_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a3_a = DFFE(CPEAK_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a3_a);

Select_321_rtl_20_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_321_rtl_20_a11 = FF_STATE_a12 & (Debug_a3_a_acombout # FF_STATE_a9 & CPeak_Reg_a3_a) # !FF_STATE_a12 & FF_STATE_a9 & CPeak_Reg_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => FF_STATE_a9,
	datac => Debug_a3_a_acombout,
	datad => CPeak_Reg_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_321_rtl_20_a11);

BLEVEL_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(3),
	combout => BLEVEL_a3_a_acombout);

Select_321_rtl_20_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_321_rtl_20_a2 = BLEVEL_a3_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => BLEVEL_a3_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_321_rtl_20_a2);

FF_WDATA_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a3_a_areg0 = DFFE(Select_321_rtl_20_a11 # Select_321_rtl_20_a2 # TEST_REG_a3_a_acombout & FF_STATE_a13, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => TEST_REG_a3_a_acombout,
	datab => Select_321_rtl_20_a11,
	datac => FF_STATE_a13,
	datad => Select_321_rtl_20_a2,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a3_a_areg0);

BLEVEL_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(2),
	combout => BLEVEL_a2_a_acombout);

Select_323_rtl_21_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_323_rtl_21_a2 = BLEVEL_a2_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => BLEVEL_a2_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_323_rtl_21_a2);

TEST_REG_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(2),
	combout => TEST_REG_a2_a_acombout);

Debug_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(2),
	combout => Debug_a2_a_acombout);

CPEAK_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(2),
	combout => CPEAK_a2_a_acombout);

CPeak_Reg_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a2_a = DFFE(CPEAK_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a2_a);

Select_323_rtl_21_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_323_rtl_21_a11 = FF_STATE_a12 & (Debug_a2_a_acombout # FF_STATE_a9 & CPeak_Reg_a2_a) # !FF_STATE_a12 & FF_STATE_a9 & CPeak_Reg_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a12,
	datab => FF_STATE_a9,
	datac => Debug_a2_a_acombout,
	datad => CPeak_Reg_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_323_rtl_21_a11);

FF_WDATA_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a2_a_areg0 = DFFE(Select_323_rtl_21_a2 # Select_323_rtl_21_a11 # FF_STATE_a13 & TEST_REG_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_323_rtl_21_a2,
	datab => FF_STATE_a13,
	datac => TEST_REG_a2_a_acombout,
	datad => Select_323_rtl_21_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a2_a_areg0);

Debug_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(1),
	combout => Debug_a1_a_acombout);

CPEAK_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(1),
	combout => CPEAK_a1_a_acombout);

CPeak_Reg_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a1_a = DFFE(CPEAK_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a1_a);

Select_325_rtl_22_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_325_rtl_22_a11 = FF_STATE_a9 & (CPeak_Reg_a1_a # FF_STATE_a12 & Debug_a1_a_acombout) # !FF_STATE_a9 & FF_STATE_a12 & Debug_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a9,
	datab => FF_STATE_a12,
	datac => Debug_a1_a_acombout,
	datad => CPeak_Reg_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_325_rtl_22_a11);

TEST_REG_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(1),
	combout => TEST_REG_a1_a_acombout);

BLEVEL_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(1),
	combout => BLEVEL_a1_a_acombout);

Select_325_rtl_22_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_325_rtl_22_a2 = BLEVEL_a1_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => BLEVEL_a1_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_325_rtl_22_a2);

FF_WDATA_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a1_a_areg0 = DFFE(Select_325_rtl_22_a11 # Select_325_rtl_22_a2 # FF_STATE_a13 & TEST_REG_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_325_rtl_22_a11,
	datab => FF_STATE_a13,
	datac => TEST_REG_a1_a_acombout,
	datad => Select_325_rtl_22_a2,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a1_a_areg0);

BLEVEL_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BLEVEL(0),
	combout => BLEVEL_a0_a_acombout);

Select_327_rtl_23_a2_I : apex20ke_lcell 
-- Equation(s):
-- Select_327_rtl_23_a2 = BLEVEL_a0_a_acombout & FF_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => BLEVEL_a0_a_acombout,
	datad => FF_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_327_rtl_23_a2);

TEST_REG_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TEST_REG(0),
	combout => TEST_REG_a0_a_acombout);

Debug_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Debug(0),
	combout => Debug_a0_a_acombout);

CPEAK_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPEAK(0),
	combout => CPEAK_a0_a_acombout);

CPeak_Reg_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- CPeak_Reg_a0_a = DFFE(CPEAK_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , Meas_Done_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => CPEAK_a0_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => CPeak_Reg_a0_a);

Select_327_rtl_23_a11_I : apex20ke_lcell 
-- Equation(s):
-- Select_327_rtl_23_a11 = FF_STATE_a9 & (CPeak_Reg_a0_a # FF_STATE_a12 & Debug_a0_a_acombout) # !FF_STATE_a9 & FF_STATE_a12 & Debug_a0_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => FF_STATE_a9,
	datab => FF_STATE_a12,
	datac => Debug_a0_a_acombout,
	datad => CPeak_Reg_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => Select_327_rtl_23_a11);

FF_WDATA_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- FF_WDATA_a0_a_areg0 = DFFE(Select_327_rtl_23_a2 # Select_327_rtl_23_a11 # FF_STATE_a13 & TEST_REG_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Select_327_rtl_23_a2,
	datab => FF_STATE_a13,
	datac => TEST_REG_a0_a_acombout,
	datad => Select_327_rtl_23_a11,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => FF_WDATA_a0_a_areg0);

FF_DONE_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_DONE_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_DONE);

FF_WR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => NOT_FF_WR_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WR);

FF_WDATA_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a17_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(17));

FF_WDATA_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a16_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(16));

FF_WDATA_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a15_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(15));

FF_WDATA_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a14_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(14));

FF_WDATA_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a13_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(13));

FF_WDATA_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a12_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(12));

FF_WDATA_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a11_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(11));

FF_WDATA_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a10_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(10));

FF_WDATA_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a9_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(9));

FF_WDATA_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a8_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(8));

FF_WDATA_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a7_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(7));

FF_WDATA_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a6_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(6));

FF_WDATA_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a5_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(5));

FF_WDATA_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a4_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(4));

FF_WDATA_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a3_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(3));

FF_WDATA_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(2));

FF_WDATA_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a1_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(1));

FF_WDATA_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => FF_WDATA_a0_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_WDATA(0));
END structure;


