onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/FIFO_FF
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_FIFO_HI
add wave -noupdate -format Logic -radix hexadecimal /tb/BIT_ENABLE
add wave -noupdate -format Logic -radix hexadecimal /tb/DSO_Enable
add wave -noupdate -format Logic -radix hexadecimal /tb/Time_Enable
add wave -noupdate -format Logic -radix hexadecimal /tb/Blevel_En
add wave -noupdate -format Logic -radix hexadecimal /tb/LS_Map_Mode
add wave -noupdate -format Logic -radix hexadecimal /tb/Meas_Done
add wave -noupdate -format Logic -radix hexadecimal /tb/Preset_out
add wave -noupdate -format Logic -radix hexadecimal /tb/Blevel_Upd
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_DSO_START
add wave -noupdate -format Logic -radix hexadecimal /tb/PBusy
add wave -noupdate -format Logic -radix hexadecimal /tb/DSO_TRIG_VEC
add wave -noupdate -format Logic -radix hexadecimal /tb/DSO_Trig
add wave -noupdate -format Logic -radix hexadecimal /tb/DSO_INT
add wave -noupdate -format Logic -radix hexadecimal /tb/TEST_REG
add wave -noupdate -format Logic -radix hexadecimal /tb/CPEAK
add wave -noupdate -format Logic -radix hexadecimal /tb/BLEVEL
add wave -noupdate -format Logic -radix hexadecimal /tb/Debug
add wave -noupdate -format Logic -radix hexadecimal /tb/FF_DONE
add wave -noupdate -format Logic -radix hexadecimal /tb/FF_WR
add wave -noupdate -format Logic -radix hexadecimal /tb/FF_WDATA
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {755959 ps} {19165170 ps} {8937182 ps}
WaveRestoreZoom {0 ps} {3870510 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2