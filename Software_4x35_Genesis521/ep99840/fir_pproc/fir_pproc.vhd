 ------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_pproc
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		1/13/06 : MCS : Ver 4004
--			Offset was moved to fir_blr where there were more bits
--		12/08/05 - MCS - Ver 357E
--			Fine Gain was removed ( or held fixed ) by mult max_fir by 16384 (4000hex)
--			but the multiplier was moved to the AD9240 module. 
--			Apply the fine gain to the input ramp, then run it through the filter
--			but leave the zero at the tail end 
--		09/01/05 - MCS - Ver 3571
--			Non Linear Correction changed to 9 bits, OFFSET_LU changed from 8 to 9 bits
--			keeping a range of +/- 1/16 of a channel but to +/- 16 channels
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity fir_pproc is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		Time_CLr		: in		std_logic;
		Time_Enable	: in		std_logic;
		EDisc_Out		: in		std_logic;					-- Discriminator Level Exceeded
		PUR			: in		std_logic;
		PBUSY		: in		std_logic;
		Peak_Done		: in		std_logic;
		memory_access	: in		std_logic;					-- Memory Access Flag
		ROI_WE		: in		std_Logic;					-- Write to ROI Look-up Memory
--		OFFSET_LU		: in		std_logic_Vector(  8 downto 0 );		
		EDX_XFER		: in		std_Logic;
		CLIP_MIN		: in		std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
		CLIP_MAX		: in		std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
		fir_out		: in		std_logic_vector( 15 downto 0 );
		DSP_D		: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
		DSP_RAMA		: in		std_logic_Vector( 10 downto 0 );	-- DSP Address Bus
		ROI_LU		: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
		Meas_Done		: buffer	std_logic;					-- Measurement is Done
		ROI_SUM		: buffer	std_logic_vector( 31 downto 0 );
		CPEak		: buffer	std_logic_Vector( 11 downto 0 ) );
end fir_pproc;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of fir_pproc is
	constant PD_VEC_MAX		: integer := 2+3;

	constant MAX_LSB 		: integer := 14-1;		-- 0x4000 = x1


	signal cpeak_clr		: std_logic;
	signal cpeak_en		: std_logic;

	signal PD_Vec			: std_logic_vector(  PD_VEC_MAX downto 0 );
	signal ROI_ADDR		: std_logic_Vector(  7 downto 0 );
	signal ROI_DEF			: std_logic;
	signal Cmp_Vec			: std_logic_Vector(  3 downto 0 );

	signal CPeak_Mux		: std_logic_vector( 12 downto 0 );


	signal fir_out_del		: std_logic_Vector( 15 downto 0 );
	signal ROI_INC			: std_logic;
	signal max_fir_clr		: std_Logic;
	signal max_fir_en		: std_logic;	
	signal max_fir_cmp		: std_logic;
	signal max_fir			: std_logic_vector( 15 downto 0 );
	signal fir_out_Pslope	: std_logic;

begin
     -------------------------------------------------------------------------------
	-- Max Fir Detection
	-- Find the Positive Slope (>=) of fir_out
	fir_out_del_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(	
			aclr				=> imr,
			clock			=> clk,
			data				=> fir_out,
			q				=> fir_out_Del );
			
	fir_pslope_compare : lpm_compare
		generic map(
			lpm_width			=> 16,
			lpm_representation	=> "SIGNED" )
		port map(
			dataa			=> fir_out,			-- Use FIR Output
			datab			=> fir_out_del,			
			ageb				=> fir_out_Pslope );

			
	-- Check FIR_OUT against MAX_FIR, if >=, need to update max_fir
	fir_max_cmp : lpm_compare
		generic map(
			lpm_width			=> 16,
			lpm_representation	=> "SIGNED" )
		port map(
			dataa			=> fir_out,			-- Use FIR Output
			datab			=> max_fir,			
			ageb				=> max_fir_cmp );

			
	-- Clear the Stored Max Peak after it has been processed

	max_fir_clr	<= '1' when (( EDisc_Out 		= '1' ) 
						or ( PD_Vec(PD_VEC_MAX)	= '1' )
						or ( Pur 				= '1' )
						or ( PBusy			= '0' )) else '0';
						

	max_fir_en	<= '1' when ( max_fir_clr 	= '1' ) else
				   '1' when (( fir_out_Pslope = '1' ) and ( Max_fir_Cmp = '1' )) else 
				   '0';

	max_fir_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			enable			=> max_fir_en,
			sclr				=> max_fir_clr,
			data				=> fir_out,
			q				=> max_fir );
				
     -------------------------------------------------------------------------------
     -------------------------------------------------------------------------------
	Cpeak_Mux_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 13 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> max_fir( 12 downto 0 ),
			q				=> Cpeak_Mux );
--	CPeak_Mux <= CPeak_Out( 17 downto 0 );

--     -------------------------------------------------------------------------------
--	-- Multiply MAX_FIR by 2^14 ( ie 0x4000 = x1 ) 	
--	max_fir_Ext_gen_a : for i in 0 to MAX_LSB generate
--		Max_Fir_Ext(i) <= '0';
--	end generate;
--	
--	max_fir_Ext_gen_b : for i in MAX_LSB+1 to MAX_LSB+1+15 generate
--		Max_Fir_Ext(i) <= Max_Fir(i-(MAX_LSB+1));
--	end generate;
--	
--	max_fir_Ext_gen_c : for i in MAX_LSB+1+15+1 to 31 generate
--		Max_Fir_Ext(i) <= Max_Fir(15);
--	end generate;
--   -------------------------------------------------------------------------------
--     -------------------------------------------------------------------------------
--	-- Sign Extend Offset to Offset_Ext
--	gzo_gen_a : for i in 0 to Offset_LSB generate
--		Offset_Ext(i) <= '0';
--	end generate;
--	
--	gzo_gen_b : for i in Offset_LSB+1 to Offset_LSB+1+15 generate					
--		Offset_Ext(i) <= Offset(i-(Offset_LSB+1) );
--	end generate;
--	
--	gzo_gen_c : for i in Offset_LSB+1+15+1 to 31 generate					
--		Offset_Ext(i) <= OFFSET(15);
--	end generate;
--	-- Sign Extend Offset to Offset_Ext
--   -------------------------------------------------------------------------------
--	CPEAK_ZERO : LPM_ADD_SUB
--		generic map(
--			LPM_WIDTH			=> 32,			
--			LPM_REPRESENTATION	=> "SIGNED" ) 

--		port map(
--			add_sub			=> Vcc,					-- Add
--			Cin				=> Gnd,
--			dataa			=> Max_Fir_Ext,				
--			datab			=> Offset_Ext,
--			result			=> GZ_Product );
--
------------------------------------------------------------------------------------
	-- The following Code has been removed ( multiplier ) but moved to the 
	-- input A/D converter module

     -------------------------------------------------------------------------------
	-- Round-off Counter 
	--	GZ_Ro_Cnt_CNTR : LPM_COUNTER
	--		generic map(
	--			LPM_WIDTH			=> RO_DEPTH,
	--			LPM_TYPE			=> "LPM_COUNTER" )
	--		port map(
	--			aclr				=> IMR,
	--			clock			=> CLK,
	--			cnt_en			=> PD_Vec(1),
	--			q				=> GZ_Ro_Cnt );

	--	GZ_Ro_Sum 	<= zeroes( 31 downto 14 ) & GZ_RO_Cnt & Zeroes( 3 downto 0 );
	-- Round-off Counter 
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Fine Gain Multiplier
	--	CPEAK_GAIN : LPM_MULT			
	--		generic map(
	--			LPM_WIDTHA		=> 16,
	--			LPM_WIDTHB		=> 16,
	--			LPM_WIDTHS  		=> 32,	
	--		     LPM_WIDTHP		=> 32,
	--		     LPM_REPRESENTATION	=> "UNSIGNED",
	--			lpm_hint 			=> "MAXIMIZE_SPEED=1",
	--			LPM_TYPE			=> "LPM_MULT" )
	--		port map(
	--			dataa			=> Max_Fir,
	--			datab			=> FGain,
	--			sum				=> GZ_Ro_Sum,
	--			result			=> GPRODUCT );
	--	-- Fine Gain Multiplier
     -------------------------------------------------------------------------------
	-- The following Code has been removed ( multiplier ) but moved to the 
	-- input A/D converter module
------------------------------------------------------------------------------------

	

--	GZ_Product_Out_FF : LPM_FF
--		generic map(
--			LPM_WIDTH			=> 18,			
--			LPM_TYPE			=> "LPM_FF" )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			data				=> GZ_Product( 31 downto 14 ),	
--			q				=> GZ_Product_Out );	
	
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Sign Extend Non-Linearity Offset to 18 bits
--	S_NL_OFFSET_GEN : for i in 8 to 21 generate
--		S_NL_Offset(i) <= Offset_Lu(7);
--	end generate;
--	S_NL_OFFSET( 7 downto 0 ) <= Offset_Lu;
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Sign Extend Non-Linearity Offset to 18 bits
	-- Ver 3570
--	S_NL_OFFSET_GEN : for i in 9 to 21 generate
--		S_NL_Offset(i) <= Offset_Lu(8);
--	end generate;
--	S_NL_OFFSET( 8 downto 1 ) <= Offset_Lu;		
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Sign Extend Non-Linearity Offset to 18 bits
	-- Ver 3571
--	S_NL_OFFSET_GEN : for i in 9 to 21 generate
--		S_NL_Offset(i) <= Offset_Lu(8);
--	end generate;
--	S_NL_OFFSET( 8 downto 0 ) <= Offset_Lu;
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Sign Extend Non-Linearity Offset to 18 bits
	-- Ver 3572
--	S_NL_OFFSET_GEN : for i in 8+1 to 21 generate
--		S_NL_Offset(i) <= Offset_Lu(8);
--	end generate;
---	S_NL_OFFSET( 8 downto 0 ) <= Offset_Lu;	
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Add/Sub in the Non-Linearity Offset	
--	CPEAK_NLIN : LPM_ADD_SUB
--		generic map(
--			LPM_WIDTH			=> 22,			
--			LPM_REPRESENTATION	=> "SIGNED",
--			LPM_TYPE			=> "LPM_ADD_SUB" ) 
--		port map(
--			add_sub			=> Vcc,					-- Add
--			dataa			=> GZ_Product( 31 downto 10 ),	
--			datab			=> S_NL_Offset,
--			result			=> GZ_NL_Product );
     -------------------------------------------------------------------------------

	-- Now only keep the output bits
--	iCPeak_FF : LPM_FF
--		generic map(
--			LPM_WIDTH			=> 18,			-- 13
--			LPM_TYPE			=> "LPM_FF" )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			data				=> GZ_NL_Product( 21 downto 4 ),		-- 26
--			q				=> CPeak_Out );	
--			
--	CPeak_Mux <= CPeak_Out( 17 downto 0 );
--	-------------------------------------------------------------------------------
	cpeak_clr	<= '1' when  ( EDisc_Out = '1' ) else '0';
	Cpeak_En	<= '1' when (( CPeak_Clr = '1' ) or ( PD_Vec(PD_VEC_MAX-1) = '1' )) else '0';
	
	Cpeak_ff	: lpm_ff
		generic map(
			LPM_WIDTH	=> 12,
			LPM_TYPE	=> "LPM_FF" )
		port map(
			aclr		=> imr,
			clock	=> clk,
			enable	=> cpeak_en,
			sclr		=> cpeak_Clr,
			data		=> CPeak_Mux( 11 downto 0 ),
			q		=> CPeak );
	-------------------------------------------------------------------------------
	
     -------------------------------------------------------------------------------
	-- ROI Defined Look-up Memory

	ROI_ADDR	<= CPeak_Mux( 11 downto 4 ) when ( Memory_Access = '0' ) else
			   DSP_RAMA( 7 downto 0 );

	ROI_RAM : LPM_RAM_DQ
		generic map(
			LPM_WIDTH			=> 16,
			LPM_WIDTHAD		=> 8,
			LPM_INDATA		=> "REGISTERED",
			LPM_ADDRESS_CONTROL	=> "REGISTERED",
			LPM_OUTDATA		=> "REGISTERED",
			LPM_FILE			=> "ROI_RAM.MIF",
			LPM_TYPE			=> "LPM_RAM_DQ" )
		port map(
			inclock			=> clk,
			outclock			=> CLK,
			data				=> DSP_D,
			address			=> ROI_ADDR,
			we				=> ROI_WE,
			q				=> ROI_LU );

	ROI_DEF  	<= ROI_LU( Conv_Integer( CPeak_Mux( 3 downto 0 )));

     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Compare Converted peak to Min/Max Clip Value
		
	-- Zero Detect, 
	Cmp_Vec(0) <= '0' when ( Conv_Integer( CPeak_Mux ) = 0 ) else '1';

	Clip_Min_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 13, 
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> CPeak_Mux,
			datab			=> '0' & Clip_Min,
			ageb				=> Cmp_Vec(1) );

	Clip_Max_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 13, 
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> CPeak_Mux,
			datab			=> '0' & Clip_max,
			aleb				=> Cmp_Vec(2) );	

	Cmp_Vec(3) <= '1' when ( Cpeak_Mux(12) = '0' ) else '0';	
     -------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- ROI Counter - Counts all evenets in defined ROI
	ROI_SUM_CNTR : LPM_COUNTER
		generic map(
			LPM_WIDTH	=> 32 )
		port map(
			aclr		=> IMR,
			clock	=> CLK,
			sclr		=> Time_Clr,
			cnt_en	=> ROI_INC,
			q		=> ROI_SUM );

     -------------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			for i in 0 to PD_VEC_MAX loop
				PD_VEC(i) <= '0';
			end loop;
			Meas_Done		<= '0';		
			ROI_INC		<= '0';
		elsif(( clk'event ) and ( clk = '1' )) then
			PD_Vec		<= PD_Vec( PD_VEC_MAX-1 downto 0) & Peak_Done;

			---------------------------------------------------------------------
			-- Peak Done is used for Counting Stored Count Rate
			if(( PD_Vec(PD_VEC_MAX downto PD_VEC_MAX-1) = "01" ) and ( Pur = '0' ) and ( EDX_XFER = '0' ) and ( ROI_DEF = '1' ) and ( Cmp_Vec(3) = '1' ))
				then Meas_Done <= '1';
			elsif(( PD_Vec(PD_VEC_MAX downto PD_VEC_MAX-1) = "01" ) and ( Pur = '0' ) and ( EDX_XFER = '1' ) and ( CMP_VEC = "1111" ))
				then Meas_Done <= '1';
				else Meas_Done <= '0';
			end if;
			
			if(( Time_Enable = '1' ) and ( PD_Vec(PD_VEC_MAX downto PD_VEC_MAX-1) = "01" ) and ( Pur = '0' ) and ( Cmp_Vec(3) = '1' ) and ( ROI_DEF = '1' ))
				then ROI_INC <= '1';
				else ROI_INC <= '0';
			end if;
			-- Measurement is Done and not out of range --------------------
               ----------------------------------------------------------------
		end if;
	end process;
	-------------------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;               -- fir_gen
------------------------------------------------------------------------------------
			
