 ------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_pproc
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module generates the FIR data from the three differnet "TAPS" of 
--		the delay line.
--
--	fir = (( dataa - datab ) - datab_del3 ) + ( datac_del3 ))
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;
	
entity tb is
end tb;

architecture test_bench of tb is
	signal imr 			: std_logic := '1';
	signal clk			: std_logic := '0';
	signal TConv_Sel		: std_logic;
	signal Time_CLr		: std_logic;
	signal Time_Enable		: std_logic;
	signal EDisc_Out		: std_logic;					-- Discriminator Level Exceeded
	signal PUR			: std_logic;
	signal PBUSY			: std_logic;
	signal Peak_Done		: std_logic;
	signal memory_access	: std_logic;					-- Memory Access Flag
	signal ROI_WE			: std_Logic;					-- Write to ROI Look-up Memory
	signal OFFSET_LU		: std_logic_Vector(  7 downto 0 );		
	signal EDX_XFER		: std_Logic;
	signal CLIP_MIN		: std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
	signal CLIP_MAX		: std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
	signal fir_out			: std_logic_vector( 15 downto 0 );
	signal fgain			: std_logic_Vector( 15 downto 0 );
	signal offset			: std_logic_Vector( 15 downto 0 );
	signal DSP_D			: std_logic_vector( 15 downto 0 );	-- DSP Data Bus
	signal DSP_RAMA		: std_logic_Vector( 10 downto 0 );	-- DSP Address Bus
	signal ROI_LU			: std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
	signal Meas_Done		: std_logic;					-- Measurement is Done
	signal ROI_SUM			: std_logic_vector( 31 downto 0 );
	signal CPEak			: std_logic_Vector( 11 downto 0 );	-- Converted Peak
	signal GZ_Product_Out	: std_logic_vector( 17 downto 0 );
	signal GProduct		: std_logic_vector( 31 downto 0 );
	signal Fir_Trig		: std_logic;
------------------------------------------------------------------------------------
component fir_pproc is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		TConv_Sel		: in		std_logic;
		Time_CLr		: in		std_logic;
		Time_Enable	: in		std_logic;
		EDisc_Out		: in		std_logic;					-- Discriminator Level Exceeded
		PUR			: in		std_logic;
		PBUSY		: in		std_logic;
		Peak_Done		: in		std_logic;
		memory_access	: in		std_logic;					-- Memory Access Flag
		ROI_WE		: in		std_Logic;					-- Write to ROI Look-up Memory
		OFFSET_LU		: in		std_logic_Vector(  7 downto 0 );		
		EDX_XFER		: in		std_Logic;
		CLIP_MIN		: in		std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
		CLIP_MAX		: in		std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
		fir_out		: in		std_logic_vector( 15 downto 0 );
		fgain		: in		std_logic_Vector( 15 downto 0 );
		offset		: in		std_logic_Vector( 15 downto 0 );
		DSP_D		: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
		DSP_RAMA		: in		std_logic_Vector( 10 downto 0 );	-- DSP Address Bus
		ROI_LU		: out	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
		Meas_Done		: out	std_logic;					-- Measurement is Done
		ROI_SUM		: out	std_logic_vector( 31 downto 0 );
		CPEak		: out	std_logic_Vector( 11 downto 0 );	-- Converted Peak
		GZ_Product_Out	: out  	std_logic_vector( 17 downto 0 );
		GProduct		: out	std_logic_vector( 31 downto 0 ));
end  component fir_pproc;
------------------------------------------------------------------------------------

begin
	imr 	<= '0' after 555 ns;
	clk	<= not( clk ) after 25 ns;
	
	U : fir_pproc 
		port map(
			imr			=> imr,
			clk			=> clk,
			TConv_Sel		=> tconv_sel,
			Time_CLr		=> time_Clr,
			Time_Enable	=> time_enable,
			EDisc_Out		=> edisc_out,
			PUR			=> pur, 
			PBUSY		=> pbusy,
			Peak_Done		=> peak_done,
			memory_access	=> memory_access,
			ROI_WE		=> roi_we,
			OFFSET_LU		=> offset_Lu,
			EDX_XFER		=> edx_xfer,
			CLIP_MIN		=> clip_min,
			CLIP_MAX		=> clip_max,
			fir_out		=> fir_out,
			fgain		=> fgain,
			offset		=> offset,
			DSP_D		=> dsp_d,
			DSP_RAMA		=> dsp_rama,
			ROI_LU		=> roi_lu,
			Meas_Done		=> meas_done,
			ROI_SUM		=> roi_sum,
			CPEak		=> cpeak,
			GZ_Product_Out	=> gz_product_out,
			GProduct		=> gproduct );
	
     -------------------------------------------------------------------------------		
	fir_out_proc : process
	begin
		fir_out 		<= x"0000";
		PBUSY		<= '0';
		EDisc_Out		<= '0';
		Peak_Done		<= '0';
		wait until (( Fir_Trig'Event ) and ( FIR_Trig = '1' ));
		PBUSY		<= '1';		
		for i in 0 to 31 loop
			wait until (( CLK'event ) and ( clk = '1' )); wait for 7 ns;
			fir_out( 15 downto 4 ) <= conv_std_logic_vector( i, 12 );
			if( i = 15 ) 
				then EDisc_Out <= '1';
				else EDisc_Out <= '0';
			end if;
		end loop;
		wait for 200 ns;
		Peak_Done 	<= '1';
			wait until (( CLK'event ) and ( clk = '1' )); wait for 7 ns;
		Peak_Done <= '0';
		
		for i in 0 to 31 loop
			wait until (( CLK'event ) and ( clk = '1' )); wait for 7 ns;
			fir_out( 15 downto 4 ) <= conv_std_logic_vector( 31-i, 12 );
		end loop;
	end process;
     -------------------------------------------------------------------------------		

	tb_proc : process
	begin
		Fir_Trig		<= '0';
		TConv_Sel		<= '0';
		Time_CLr		<= '0';
		Time_Enable	<= '0';
		PUR			<= '0';
		memory_access	<= '0';
		ROI_WE		<= '0';
		EDX_XFER		<= '0';
		OFFSET_LU		<= x"00";
		CLIP_MIN		<= x"000";
		CLIP_MAX		<= x"FFF";
		fgain		<= x"4000";
		offset		<= x"0000";
		DSP_D		<= x"0000";
		DSP_RAMA		<= conv_std_logic_Vector( 0, 11 );

		--------------------------------------------------------------------------------		
		assert false
			report "Fill Memory"
			severity note;
			
		memory_access	<= '1';
		DSP_D		<= x"FFFF";
		ROI_WE		<= '1';
		wait until (( CLK'event ) and ( clk = '1' ));
		for i in 0 to 255 loop
			DSP_RAMA 	<= conv_std_logic_vector( i, 11 );
			wait until (( CLK'event ) and ( clk = '1' ));
		end loop;
		wait until (( CLK'event ) and ( clk = '1' ));
		ROI_WE 		<= '0';
		memory_access	<= '0';
		--------------------------------------------------------------------------------		
		
		
		--------------------------------------------------------------------------------		
		assert false
			report "Check Peak w/o EDX_XFER"
			severity note;
			Time_Enable 	<= '1';
			EDX_XFER		<= '0';
			
			for j in 0 to 7 loop
				Fir_Trig <= '1';
				wait until (( PBusy'Event ) and ( PBusy = '1' ));
				Fir_Trig <= '0';
				wait until (( PBusy'Event ) and ( PBusy = '0' ));
				wait for 5 us;
			end loop;					-- j
			Time_Enable 	<= '1';
			EDX_XFER		<= '0';
		--------------------------------------------------------------------------------		

		
		--------------------------------------------------------------------------------		
		assert false
			report "Check Peak w EDX_XFER"
			severity note;
			Time_Enable 	<= '1';
			EDX_XFER		<= '1';
			
			for j in 0 to 7 loop
				Fir_Trig <= '1';
				wait until (( PBusy'Event ) and ( PBusy = '1' ));
				Fir_Trig <= '0';
				wait until (( PBusy'Event ) and ( PBusy = '0' ));
				wait for 5 us;
			end loop;					-- j
			Time_Enable 	<= '1';
			EDX_XFER		<= '0';
		--------------------------------------------------------------------------------		
			
		wait for 5 us;
		assert false
			report "End of Simulation"
			severity failure;
		wait ;
	
	end process;
	
------------------------------------------------------------------------------------
end test_bench;               -- fir_gen
------------------------------------------------------------------------------------
			
