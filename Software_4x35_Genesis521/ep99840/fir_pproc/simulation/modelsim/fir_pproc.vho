-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:08:30"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_pproc IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	Time_CLr : IN std_logic;
	Time_Enable : IN std_logic;
	EDisc_Out : IN std_logic;
	PUR : IN std_logic;
	PBUSY : IN std_logic;
	Peak_Done : IN std_logic;
	memory_access : IN std_logic;
	ROI_WE : IN std_logic;
	EDX_XFER : IN std_logic;
	CLIP_MIN : IN std_logic_vector(11 DOWNTO 0);
	CLIP_MAX : IN std_logic_vector(11 DOWNTO 0);
	fir_out : IN std_logic_vector(15 DOWNTO 0);
	DSP_D : IN std_logic_vector(15 DOWNTO 0);
	DSP_RAMA : IN std_logic_vector(10 DOWNTO 0);
	ROI_LU : OUT std_logic_vector(15 DOWNTO 0);
	Meas_Done : OUT std_logic;
	ROI_SUM : OUT std_logic_vector(31 DOWNTO 0);
	CPEak : OUT std_logic_vector(11 DOWNTO 0)
	);
END fir_pproc;

ARCHITECTURE structure OF fir_pproc IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_Time_CLr : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_EDisc_Out : std_logic;
SIGNAL ww_PUR : std_logic;
SIGNAL ww_PBUSY : std_logic;
SIGNAL ww_Peak_Done : std_logic;
SIGNAL ww_memory_access : std_logic;
SIGNAL ww_ROI_WE : std_logic;
SIGNAL ww_EDX_XFER : std_logic;
SIGNAL ww_CLIP_MIN : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_CLIP_MAX : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_fir_out : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DSP_D : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DSP_RAMA : std_logic_vector(10 DOWNTO 0);
SIGNAL ww_ROI_LU : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Meas_Done : std_logic;
SIGNAL ww_ROI_SUM : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CPEak : std_logic_vector(11 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL ROI_RAM_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_RAM_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "000100000100010101";
SIGNAL ROI_DEF_a57 : std_logic;
SIGNAL ROI_DEF_a58 : std_logic;
SIGNAL ROI_DEF_a59 : std_logic;
SIGNAL ROI_DEF_a60 : std_logic;
SIGNAL ROI_DEF_a61 : std_logic;
SIGNAL ROI_DEF_a62 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a_a822 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a_a829 : std_logic;
SIGNAL clock_proc_a102 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a_a823 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a_a830 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a14_a_a1044 : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a_a1044 : std_logic;
SIGNAL fir_out_del_ff_adffs_a15_a : std_logic;
SIGNAL Equal_a162 : std_logic;
SIGNAL Equal_a165 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a_a824 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a_a831 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a13_a_a1045 : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a_a1045 : std_logic;
SIGNAL fir_out_del_ff_adffs_a14_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a_a825 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a_a832 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a12_a_a1046 : std_logic;
SIGNAL fir_out_del_ff_adffs_a13_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a_a1046 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a_a826 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a_a833 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a11_a_a1047 : std_logic;
SIGNAL fir_out_del_ff_adffs_a12_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a_a1047 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a_a827 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a_a834 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a10_a_a1048 : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a_a1048 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a_a828 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a_a835 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a9_a_a1049 : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a_a1049 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a_a829 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a_a836 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a8_a_a1050 : std_logic;
SIGNAL fir_out_del_ff_adffs_a9_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a_a1050 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a_a830 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a_a837 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a7_a_a1051 : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a_a1051 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a_a831 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a_a838 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a6_a_a1052 : std_logic;
SIGNAL fir_out_del_ff_adffs_a7_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a_a1052 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a_a832 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a_a839 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a5_a_a1053 : std_logic;
SIGNAL fir_out_del_ff_adffs_a6_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a_a1053 : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a_a833 : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a_a840 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a4_a_a1054 : std_logic;
SIGNAL fir_out_del_ff_adffs_a5_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a_a1054 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a3_a_a1055 : std_logic;
SIGNAL fir_out_del_ff_adffs_a4_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a_a1055 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a2_a_a1056 : std_logic;
SIGNAL fir_out_del_ff_adffs_a3_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a_a1056 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a1_a_a1057 : std_logic;
SIGNAL fir_out_del_ff_adffs_a2_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a_a1057 : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a0_a_a1058 : std_logic;
SIGNAL fir_out_del_ff_adffs_a1_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a_a1058 : std_logic;
SIGNAL fir_out_del_ff_adffs_a0_a : std_logic;
SIGNAL DSP_RAMA_a6_a_acombout : std_logic;
SIGNAL DSP_RAMA_a7_a_acombout : std_logic;
SIGNAL CLIP_MIN_a11_a_acombout : std_logic;
SIGNAL CLIP_MIN_a9_a_acombout : std_logic;
SIGNAL CLIP_MAX_a6_a_acombout : std_logic;
SIGNAL CLIP_MAX_a4_a_acombout : std_logic;
SIGNAL CLIP_MIN_a4_a_acombout : std_logic;
SIGNAL CLIP_MAX_a3_a_acombout : std_logic;
SIGNAL CLIP_MAX_a2_a_acombout : std_logic;
SIGNAL CLIP_MIN_a1_a_acombout : std_logic;
SIGNAL DSP_D_a0_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL ROI_WE_acombout : std_logic;
SIGNAL DSP_RAMA_a0_a_acombout : std_logic;
SIGNAL PBUSY_acombout : std_logic;
SIGNAL EDisc_Out_acombout : std_logic;
SIGNAL Peak_Done_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL PD_Vec_a0_a : std_logic;
SIGNAL PD_Vec_a1_a : std_logic;
SIGNAL PD_Vec_a2_a : std_logic;
SIGNAL PD_Vec_a3_a : std_logic;
SIGNAL PD_Vec_a4_a : std_logic;
SIGNAL PD_Vec_a5_a : std_logic;
SIGNAL PUR_acombout : std_logic;
SIGNAL max_fir_clr_a22 : std_logic;
SIGNAL max_fir_clr_a23 : std_logic;
SIGNAL max_fir_ff_adffs_a15_a : std_logic;
SIGNAL fir_out_a15_a_acombout : std_logic;
SIGNAL fir_out_a14_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a14_a : std_logic;
SIGNAL fir_out_a13_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a13_a : std_logic;
SIGNAL fir_out_a12_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a12_a : std_logic;
SIGNAL fir_out_a11_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a11_a : std_logic;
SIGNAL fir_out_a10_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a10_a : std_logic;
SIGNAL fir_out_a9_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a9_a : std_logic;
SIGNAL fir_out_a8_a_acombout : std_logic;
SIGNAL fir_out_a7_a_acombout : std_logic;
SIGNAL fir_out_a6_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a6_a : std_logic;
SIGNAL fir_out_a5_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a5_a : std_logic;
SIGNAL max_fir_ff_adffs_a3_a : std_logic;
SIGNAL fir_out_a2_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a2_a : std_logic;
SIGNAL fir_out_a1_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a1_a : std_logic;
SIGNAL fir_out_a0_a_acombout : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL fir_max_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL fir_out_del_ff_adffs_a11_a : std_logic;
SIGNAL fir_out_del_ff_adffs_a10_a : std_logic;
SIGNAL fir_out_del_ff_adffs_a8_a : std_logic;
SIGNAL fir_out_a4_a_acombout : std_logic;
SIGNAL fir_out_a3_a_acombout : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL fir_pslope_compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL max_fir_en_a13 : std_logic;
SIGNAL max_fir_ff_adffs_a4_a : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a4_a : std_logic;
SIGNAL memory_access_acombout : std_logic;
SIGNAL ROI_ADDR_a0_a_a56 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a5_a : std_logic;
SIGNAL DSP_RAMA_a1_a_acombout : std_logic;
SIGNAL ROI_ADDR_a1_a_a57 : std_logic;
SIGNAL DSP_RAMA_a2_a_acombout : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a6_a : std_logic;
SIGNAL ROI_ADDR_a2_a_a58 : std_logic;
SIGNAL max_fir_ff_adffs_a7_a : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a7_a : std_logic;
SIGNAL DSP_RAMA_a3_a_acombout : std_logic;
SIGNAL ROI_ADDR_a3_a_a59 : std_logic;
SIGNAL DSP_RAMA_a4_a_acombout : std_logic;
SIGNAL max_fir_ff_adffs_a8_a : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a8_a : std_logic;
SIGNAL ROI_ADDR_a4_a_a60 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a9_a : std_logic;
SIGNAL DSP_RAMA_a5_a_acombout : std_logic;
SIGNAL ROI_ADDR_a5_a_a61 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a10_a : std_logic;
SIGNAL ROI_ADDR_a6_a_a62 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a11_a : std_logic;
SIGNAL ROI_ADDR_a7_a_a63 : std_logic;
SIGNAL ROI_RAM_asram_aq_a0_a : std_logic;
SIGNAL DSP_D_a1_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a1_a : std_logic;
SIGNAL DSP_D_a2_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a2_a : std_logic;
SIGNAL DSP_D_a3_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a3_a : std_logic;
SIGNAL DSP_D_a4_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a4_a : std_logic;
SIGNAL DSP_D_a5_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a5_a : std_logic;
SIGNAL DSP_D_a6_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a6_a : std_logic;
SIGNAL DSP_D_a7_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a7_a : std_logic;
SIGNAL DSP_D_a8_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a8_a : std_logic;
SIGNAL DSP_D_a9_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a9_a : std_logic;
SIGNAL DSP_D_a10_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a10_a : std_logic;
SIGNAL DSP_D_a11_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a11_a : std_logic;
SIGNAL DSP_D_a12_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a12_a : std_logic;
SIGNAL DSP_D_a13_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a13_a : std_logic;
SIGNAL DSP_D_a14_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a14_a : std_logic;
SIGNAL DSP_D_a15_a_acombout : std_logic;
SIGNAL ROI_RAM_asram_aq_a15_a : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a1_a : std_logic;
SIGNAL max_fir_ff_adffs_a0_a : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a0_a : std_logic;
SIGNAL ROI_DEF_a55 : std_logic;
SIGNAL ROI_DEF_a56 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a3_a : std_logic;
SIGNAL ROI_DEF_a63 : std_logic;
SIGNAL ROI_DEF_a64 : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a12_a : std_logic;
SIGNAL clock_proc_a101 : std_logic;
SIGNAL EDX_XFER_acombout : std_logic;
SIGNAL Cpeak_Mux_ff_adffs_a2_a : std_logic;
SIGNAL Equal_a168 : std_logic;
SIGNAL Equal_a169 : std_logic;
SIGNAL Equal_a164 : std_logic;
SIGNAL clock_proc_a99 : std_logic;
SIGNAL CLIP_MIN_a10_a_acombout : std_logic;
SIGNAL CLIP_MIN_a8_a_acombout : std_logic;
SIGNAL CLIP_MIN_a7_a_acombout : std_logic;
SIGNAL CLIP_MIN_a6_a_acombout : std_logic;
SIGNAL CLIP_MIN_a5_a_acombout : std_logic;
SIGNAL CLIP_MIN_a3_a_acombout : std_logic;
SIGNAL CLIP_MIN_a2_a_acombout : std_logic;
SIGNAL CLIP_MIN_a0_a_acombout : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Clip_Min_Cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL CLIP_MAX_a11_a_acombout : std_logic;
SIGNAL CLIP_MAX_a10_a_acombout : std_logic;
SIGNAL CLIP_MAX_a9_a_acombout : std_logic;
SIGNAL CLIP_MAX_a8_a_acombout : std_logic;
SIGNAL CLIP_MAX_a7_a_acombout : std_logic;
SIGNAL CLIP_MAX_a5_a_acombout : std_logic;
SIGNAL CLIP_MAX_a1_a_acombout : std_logic;
SIGNAL CLIP_MAX_a0_a_acombout : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Clip_Max_Cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL clock_proc_a100 : std_logic;
SIGNAL Meas_Done_areg0 : std_logic;
SIGNAL Time_Enable_acombout : std_logic;
SIGNAL ROI_INC : std_logic;
SIGNAL Time_CLr_acombout : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL ROI_SUM_CNTR_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL cpeak_en_a0 : std_logic;
SIGNAL Cpeak_ff_adffs_a0_a : std_logic;
SIGNAL Cpeak_ff_adffs_a1_a : std_logic;
SIGNAL Cpeak_ff_adffs_a2_a : std_logic;
SIGNAL Cpeak_ff_adffs_a3_a : std_logic;
SIGNAL Cpeak_ff_adffs_a4_a : std_logic;
SIGNAL Cpeak_ff_adffs_a5_a : std_logic;
SIGNAL Cpeak_ff_adffs_a6_a : std_logic;
SIGNAL Cpeak_ff_adffs_a7_a : std_logic;
SIGNAL Cpeak_ff_adffs_a8_a : std_logic;
SIGNAL Cpeak_ff_adffs_a9_a : std_logic;
SIGNAL Cpeak_ff_adffs_a10_a : std_logic;
SIGNAL Cpeak_ff_adffs_a11_a : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_Time_CLr <= Time_CLr;
ww_Time_Enable <= Time_Enable;
ww_EDisc_Out <= EDisc_Out;
ww_PUR <= PUR;
ww_PBUSY <= PBUSY;
ww_Peak_Done <= Peak_Done;
ww_memory_access <= memory_access;
ww_ROI_WE <= ROI_WE;
ww_EDX_XFER <= EDX_XFER;
ww_CLIP_MIN <= CLIP_MIN;
ww_CLIP_MAX <= CLIP_MAX;
ww_fir_out <= fir_out;
ww_DSP_D <= DSP_D;
ww_DSP_RAMA <= DSP_RAMA;
ROI_LU <= ww_ROI_LU;
Meas_Done <= ww_Meas_Done;
ROI_SUM <= ww_ROI_SUM;
CPEak <= ww_CPEak;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

ROI_RAM_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

ROI_RAM_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & ROI_ADDR_a7_a_a63 & ROI_ADDR_a6_a_a62 & ROI_ADDR_a5_a_a61 & ROI_ADDR_a4_a_a60 & ROI_ADDR_a3_a_a59 & ROI_ADDR_a2_a_a58 & ROI_ADDR_a1_a_a57 & ROI_ADDR_a0_a_a56);

DSP_RAMA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(6),
	combout => DSP_RAMA_a6_a_acombout);

DSP_RAMA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(7),
	combout => DSP_RAMA_a7_a_acombout);

ROI_DEF_a57_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a57 = Cpeak_Mux_ff_adffs_a1_a & (Cpeak_Mux_ff_adffs_a0_a # ROI_RAM_asram_aq_a6_a) # !Cpeak_Mux_ff_adffs_a1_a & ROI_RAM_asram_aq_a4_a & !Cpeak_Mux_ff_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AEA4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a1_a,
	datab => ROI_RAM_asram_aq_a4_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_RAM_asram_aq_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a57);

ROI_DEF_a58_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a58 = Cpeak_Mux_ff_adffs_a0_a & (ROI_DEF_a57 & ROI_RAM_asram_aq_a7_a # !ROI_DEF_a57 & (ROI_RAM_asram_aq_a5_a)) # !Cpeak_Mux_ff_adffs_a0_a & (ROI_DEF_a57)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_RAM_asram_aq_a7_a,
	datab => ROI_RAM_asram_aq_a5_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_DEF_a57,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a58);

ROI_DEF_a59_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a59 = Cpeak_Mux_ff_adffs_a1_a & (ROI_RAM_asram_aq_a2_a # Cpeak_Mux_ff_adffs_a0_a) # !Cpeak_Mux_ff_adffs_a1_a & (!Cpeak_Mux_ff_adffs_a0_a & ROI_RAM_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a1_a,
	datab => ROI_RAM_asram_aq_a2_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_RAM_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a59);

ROI_DEF_a60_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a60 = Cpeak_Mux_ff_adffs_a0_a & (ROI_DEF_a59 & ROI_RAM_asram_aq_a3_a # !ROI_DEF_a59 & (ROI_RAM_asram_aq_a1_a)) # !Cpeak_Mux_ff_adffs_a0_a & (ROI_DEF_a59)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_RAM_asram_aq_a3_a,
	datab => ROI_RAM_asram_aq_a1_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_DEF_a59,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a60);

ROI_DEF_a61_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a61 = Cpeak_Mux_ff_adffs_a2_a & (ROI_DEF_a58 # Cpeak_Mux_ff_adffs_a3_a) # !Cpeak_Mux_ff_adffs_a2_a & (!Cpeak_Mux_ff_adffs_a3_a & ROI_DEF_a60)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ADA8",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a2_a,
	datab => ROI_DEF_a58,
	datac => Cpeak_Mux_ff_adffs_a3_a,
	datad => ROI_DEF_a60,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a61);

ROI_DEF_a62_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a62 = Cpeak_Mux_ff_adffs_a1_a & (Cpeak_Mux_ff_adffs_a0_a # ROI_RAM_asram_aq_a14_a) # !Cpeak_Mux_ff_adffs_a1_a & ROI_RAM_asram_aq_a12_a & !Cpeak_Mux_ff_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CEC2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_RAM_asram_aq_a12_a,
	datab => Cpeak_Mux_ff_adffs_a1_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_RAM_asram_aq_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a62);

clock_proc_a102_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a102 = PD_Vec_a4_a & (!Cpeak_Mux_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PD_Vec_a4_a,
	datac => Cpeak_Mux_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a102);

CLIP_MIN_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(11),
	combout => CLIP_MIN_a11_a_acombout);

fir_out_del_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a15_a = DFFE(fir_out_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a15_a);

fir_out_del_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a14_a = DFFE(fir_out_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a14_a);

CLIP_MIN_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(9),
	combout => CLIP_MIN_a9_a_acombout);

fir_out_del_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a13_a = DFFE(fir_out_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a13_a);

fir_out_del_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a12_a = DFFE(fir_out_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a12_a);

CLIP_MAX_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(6),
	combout => CLIP_MAX_a6_a_acombout);

fir_out_del_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a9_a = DFFE(fir_out_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a9_a);

CLIP_MAX_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(4),
	combout => CLIP_MAX_a4_a_acombout);

CLIP_MIN_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(4),
	combout => CLIP_MIN_a4_a_acombout);

CLIP_MAX_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(3),
	combout => CLIP_MAX_a3_a_acombout);

fir_out_del_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a7_a = DFFE(fir_out_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a7_a);

CLIP_MAX_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(2),
	combout => CLIP_MAX_a2_a_acombout);

fir_out_del_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a6_a = DFFE(fir_out_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a6_a);

CLIP_MIN_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(1),
	combout => CLIP_MIN_a1_a_acombout);

fir_out_del_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a5_a = DFFE(fir_out_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a5_a);

fir_out_del_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a4_a = DFFE(fir_out_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a4_a);

fir_out_del_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a3_a = DFFE(fir_out_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a3_a);

fir_out_del_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a2_a = DFFE(fir_out_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a2_a);

fir_out_del_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a1_a = DFFE(fir_out_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a1_a);

fir_out_del_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a0_a = DFFE(fir_out_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a0_a);

DSP_D_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(0),
	combout => DSP_D_a0_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

ROI_WE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ROI_WE,
	combout => ROI_WE_acombout);

DSP_RAMA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(0),
	combout => DSP_RAMA_a0_a_acombout);

PBUSY_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PBUSY,
	combout => PBUSY_acombout);

EDisc_Out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EDisc_Out,
	combout => EDisc_Out_acombout);

Peak_Done_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Peak_Done,
	combout => Peak_Done_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

PD_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a0_a = DFFE(Peak_Done_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Peak_Done_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a0_a);

PD_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a1_a = DFFE(PD_Vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PD_Vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a1_a);

PD_Vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a2_a = DFFE(PD_Vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PD_Vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a2_a);

PD_Vec_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a3_a = DFFE(PD_Vec_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PD_Vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a3_a);

PD_Vec_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a4_a = DFFE(PD_Vec_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PD_Vec_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a4_a);

PD_Vec_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- PD_Vec_a5_a = DFFE(PD_Vec_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PD_Vec_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => PD_Vec_a5_a);

PUR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PUR,
	combout => PUR_acombout);

max_fir_clr_a22_I : apex20ke_lcell
-- Equation(s):
-- max_fir_clr_a22 = !PD_Vec_a5_a & (!PUR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PD_Vec_a5_a,
	datad => PUR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => max_fir_clr_a22);

max_fir_clr_a23_I : apex20ke_lcell
-- Equation(s):
-- max_fir_clr_a23 = EDisc_Out_acombout # !PBUSY_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PBUSY_acombout,
	datad => EDisc_Out_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => max_fir_clr_a23);

max_fir_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a15_a = DFFE(fir_out_a15_a_acombout & PBUSY_acombout & !EDisc_Out_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a15_a_acombout,
	datab => PBUSY_acombout,
	datac => EDisc_Out_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a15_a);

fir_out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(15),
	combout => fir_out_a15_a_acombout);

fir_out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(14),
	combout => fir_out_a14_a_acombout);

max_fir_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a14_a = DFFE(PBUSY_acombout & fir_out_a14_a_acombout & max_fir_clr_a22 & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a14_a_acombout,
	datac => max_fir_clr_a22,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a14_a);

fir_out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(13),
	combout => fir_out_a13_a_acombout);

max_fir_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a13_a = DFFE(!EDisc_Out_acombout & PBUSY_acombout & fir_out_a13_a_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EDisc_Out_acombout,
	datab => PBUSY_acombout,
	datac => fir_out_a13_a_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a13_a);

fir_out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(12),
	combout => fir_out_a12_a_acombout);

max_fir_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a12_a = DFFE(PBUSY_acombout & fir_out_a12_a_acombout & max_fir_clr_a22 & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a12_a_acombout,
	datac => max_fir_clr_a22,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a12_a);

fir_out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(11),
	combout => fir_out_a11_a_acombout);

max_fir_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a11_a = DFFE(max_fir_clr_a22 & fir_out_a11_a_acombout & PBUSY_acombout & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_clr_a22,
	datab => fir_out_a11_a_acombout,
	datac => PBUSY_acombout,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a11_a);

fir_out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(10),
	combout => fir_out_a10_a_acombout);

max_fir_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a10_a = DFFE(PBUSY_acombout & fir_out_a10_a_acombout & max_fir_clr_a22 & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a10_a_acombout,
	datac => max_fir_clr_a22,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a10_a);

fir_out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(9),
	combout => fir_out_a9_a_acombout);

max_fir_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a9_a = DFFE(PBUSY_acombout & fir_out_a9_a_acombout & !EDisc_Out_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a9_a_acombout,
	datac => EDisc_Out_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a9_a);

fir_out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(8),
	combout => fir_out_a8_a_acombout);

fir_out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(7),
	combout => fir_out_a7_a_acombout);

fir_out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(6),
	combout => fir_out_a6_a_acombout);

max_fir_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a6_a = DFFE(PBUSY_acombout & fir_out_a6_a_acombout & max_fir_clr_a22 & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a6_a_acombout,
	datac => max_fir_clr_a22,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a6_a);

fir_out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(5),
	combout => fir_out_a5_a_acombout);

max_fir_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a5_a = DFFE(PBUSY_acombout & fir_out_a5_a_acombout & max_fir_clr_a22 & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a5_a_acombout,
	datac => max_fir_clr_a22,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a5_a);

max_fir_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a3_a = DFFE(fir_out_a3_a_acombout & PBUSY_acombout & !EDisc_Out_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a3_a_acombout,
	datab => PBUSY_acombout,
	datac => EDisc_Out_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a3_a);

fir_out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(2),
	combout => fir_out_a2_a_acombout);

max_fir_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a2_a = DFFE(max_fir_clr_a22 & fir_out_a2_a_acombout & PBUSY_acombout & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_clr_a22,
	datab => fir_out_a2_a_acombout,
	datac => PBUSY_acombout,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a2_a);

fir_out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(1),
	combout => fir_out_a1_a_acombout);

max_fir_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a1_a = DFFE(max_fir_clr_a22 & fir_out_a1_a_acombout & PBUSY_acombout & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_clr_a22,
	datab => fir_out_a1_a_acombout,
	datac => PBUSY_acombout,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a1_a);

fir_out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(0),
	combout => fir_out_a0_a_acombout);

fir_max_cmp_acomparator_acmp_end_alcarry_a0_a_a1058_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(max_fir_ff_adffs_a0_a & !fir_out_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_ff_adffs_a0_a,
	datab => fir_out_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a0_a_a1058,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a0_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a1_a_a1057_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(fir_out_a1_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a0_a # !max_fir_ff_adffs_a1_a) # !fir_out_a1_a_acombout & !max_fir_ff_adffs_a1_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a1_a_acombout,
	datab => max_fir_ff_adffs_a1_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a1_a_a1057,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a1_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a2_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(fir_out_a2_a_acombout & max_fir_ff_adffs_a2_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a1_a # !fir_out_a2_a_acombout & (max_fir_ff_adffs_a2_a # !fir_max_cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a2_a_acombout,
	datab => max_fir_ff_adffs_a2_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a2_a_a1056,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a2_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a3_a_a1055_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(fir_out_a3_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a2_a # !max_fir_ff_adffs_a3_a) # !fir_out_a3_a_acombout & !max_fir_ff_adffs_a3_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a3_a_acombout,
	datab => max_fir_ff_adffs_a3_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a3_a_a1055,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a3_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a4_a_a1054_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(fir_out_a4_a_acombout & max_fir_ff_adffs_a4_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a3_a # !fir_out_a4_a_acombout & (max_fir_ff_adffs_a4_a # !fir_max_cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a4_a_acombout,
	datab => max_fir_ff_adffs_a4_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a4_a_a1054,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a4_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a5_a_a1053_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(fir_out_a5_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a4_a # !max_fir_ff_adffs_a5_a) # !fir_out_a5_a_acombout & !max_fir_ff_adffs_a5_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a5_a_acombout,
	datab => max_fir_ff_adffs_a5_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a5_a_a1053,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a5_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a6_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(fir_out_a6_a_acombout & max_fir_ff_adffs_a6_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a5_a # !fir_out_a6_a_acombout & (max_fir_ff_adffs_a6_a # !fir_max_cmp_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a6_a_acombout,
	datab => max_fir_ff_adffs_a6_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a6_a_a1052,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a6_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a7_a_a1051_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(max_fir_ff_adffs_a7_a & fir_out_a7_a_acombout & !fir_max_cmp_acomparator_acmp_end_alcarry_a6_a # !max_fir_ff_adffs_a7_a & (fir_out_a7_a_acombout # !fir_max_cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_ff_adffs_a7_a,
	datab => fir_out_a7_a_acombout,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a7_a_a1051,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a7_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a8_a_a1050_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(max_fir_ff_adffs_a8_a & (!fir_max_cmp_acomparator_acmp_end_alcarry_a7_a # !fir_out_a8_a_acombout) # !max_fir_ff_adffs_a8_a & !fir_out_a8_a_acombout & !fir_max_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_ff_adffs_a8_a,
	datab => fir_out_a8_a_acombout,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a8_a_a1050,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a8_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a9_a_a1049_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(fir_out_a9_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a8_a # !max_fir_ff_adffs_a9_a) # !fir_out_a9_a_acombout & !max_fir_ff_adffs_a9_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a9_a_acombout,
	datab => max_fir_ff_adffs_a9_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a9_a_a1049,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a9_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a10_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(fir_out_a10_a_acombout & max_fir_ff_adffs_a10_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a9_a # !fir_out_a10_a_acombout & (max_fir_ff_adffs_a10_a # !fir_max_cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a10_a_acombout,
	datab => max_fir_ff_adffs_a10_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a10_a_a1048,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a10_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a11_a_a1047_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(fir_out_a11_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a10_a # !max_fir_ff_adffs_a11_a) # !fir_out_a11_a_acombout & !max_fir_ff_adffs_a11_a & 
-- !fir_max_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a11_a_acombout,
	datab => max_fir_ff_adffs_a11_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a11_a_a1047,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a11_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a12_a_a1046_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(fir_out_a12_a_acombout & max_fir_ff_adffs_a12_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a11_a # !fir_out_a12_a_acombout & (max_fir_ff_adffs_a12_a # 
-- !fir_max_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a12_a_acombout,
	datab => max_fir_ff_adffs_a12_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a12_a_a1046,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a12_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a13_a_a1045_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(fir_out_a13_a_acombout & (!fir_max_cmp_acomparator_acmp_end_alcarry_a12_a # !max_fir_ff_adffs_a13_a) # !fir_out_a13_a_acombout & !max_fir_ff_adffs_a13_a & 
-- !fir_max_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a13_a_acombout,
	datab => max_fir_ff_adffs_a13_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a13_a_a1045,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a13_a);

fir_max_cmp_acomparator_acmp_end_alcarry_a14_a_a1044_I : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(fir_out_a14_a_acombout & max_fir_ff_adffs_a14_a & !fir_max_cmp_acomparator_acmp_end_alcarry_a13_a # !fir_out_a14_a_acombout & (max_fir_ff_adffs_a14_a # 
-- !fir_max_cmp_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a14_a_acombout,
	datab => max_fir_ff_adffs_a14_a,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_alcarry_a14_a_a1044,
	cout => fir_max_cmp_acomparator_acmp_end_alcarry_a14_a);

fir_max_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- fir_max_cmp_acomparator_acmp_end_aagb_out = max_fir_ff_adffs_a15_a & fir_max_cmp_acomparator_acmp_end_alcarry_a14_a & fir_out_a15_a_acombout # !max_fir_ff_adffs_a15_a & (fir_max_cmp_acomparator_acmp_end_alcarry_a14_a # fir_out_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F330",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => max_fir_ff_adffs_a15_a,
	datad => fir_out_a15_a_acombout,
	cin => fir_max_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_max_cmp_acomparator_acmp_end_aagb_out);

fir_out_del_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a11_a = DFFE(fir_out_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a11_a);

fir_out_del_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a10_a = DFFE(fir_out_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a10_a);

fir_out_del_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_out_del_ff_adffs_a8_a = DFFE(fir_out_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_out_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_out_del_ff_adffs_a8_a);

fir_out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(4),
	combout => fir_out_a4_a_acombout);

fir_out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_out(3),
	combout => fir_out_a3_a_acombout);

fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a_a1058_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a = CARRY(fir_out_del_ff_adffs_a0_a & !fir_out_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a0_a,
	datab => fir_out_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a_a1058,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a_a1057_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a = CARRY(fir_out_del_ff_adffs_a1_a & fir_out_a1_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a # !fir_out_del_ff_adffs_a1_a & (fir_out_a1_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a1_a,
	datab => fir_out_a1_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a_a1057,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a = CARRY(fir_out_del_ff_adffs_a2_a & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a # !fir_out_a2_a_acombout) # !fir_out_del_ff_adffs_a2_a & !fir_out_a2_a_acombout & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a2_a,
	datab => fir_out_a2_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a_a1056,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a_a1055_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a = CARRY(fir_out_del_ff_adffs_a3_a & fir_out_a3_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a # !fir_out_del_ff_adffs_a3_a & (fir_out_a3_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a3_a,
	datab => fir_out_a3_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a_a1055,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a_a1054_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a = CARRY(fir_out_del_ff_adffs_a4_a & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a # !fir_out_a4_a_acombout) # !fir_out_del_ff_adffs_a4_a & !fir_out_a4_a_acombout & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a4_a,
	datab => fir_out_a4_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a_a1054,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a_a1053_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a = CARRY(fir_out_del_ff_adffs_a5_a & fir_out_a5_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a # !fir_out_del_ff_adffs_a5_a & (fir_out_a5_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a5_a,
	datab => fir_out_a5_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a_a1053,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a = CARRY(fir_out_del_ff_adffs_a6_a & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a # !fir_out_a6_a_acombout) # !fir_out_del_ff_adffs_a6_a & !fir_out_a6_a_acombout & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a6_a,
	datab => fir_out_a6_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a_a1052,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a_a1051_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a = CARRY(fir_out_del_ff_adffs_a7_a & fir_out_a7_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a # !fir_out_del_ff_adffs_a7_a & (fir_out_a7_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a7_a,
	datab => fir_out_a7_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a_a1051,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a_a1050_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a = CARRY(fir_out_a8_a_acombout & fir_out_del_ff_adffs_a8_a & !fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a # !fir_out_a8_a_acombout & (fir_out_del_ff_adffs_a8_a # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a8_a_acombout,
	datab => fir_out_del_ff_adffs_a8_a,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a_a1050,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a_a1049_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a = CARRY(fir_out_del_ff_adffs_a9_a & fir_out_a9_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a # !fir_out_del_ff_adffs_a9_a & (fir_out_a9_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a9_a,
	datab => fir_out_a9_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a_a1049,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a = CARRY(fir_out_a10_a_acombout & fir_out_del_ff_adffs_a10_a & !fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a # !fir_out_a10_a_acombout & (fir_out_del_ff_adffs_a10_a # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a10_a_acombout,
	datab => fir_out_del_ff_adffs_a10_a,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a_a1048,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a_a1047_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a = CARRY(fir_out_a11_a_acombout & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a # !fir_out_del_ff_adffs_a11_a) # !fir_out_a11_a_acombout & !fir_out_del_ff_adffs_a11_a & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a11_a_acombout,
	datab => fir_out_del_ff_adffs_a11_a,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a_a1047,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a_a1046_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a = CARRY(fir_out_del_ff_adffs_a12_a & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a # !fir_out_a12_a_acombout) # !fir_out_del_ff_adffs_a12_a & !fir_out_a12_a_acombout & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a12_a,
	datab => fir_out_a12_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a_a1046,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a_a1045_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a = CARRY(fir_out_del_ff_adffs_a13_a & fir_out_a13_a_acombout & !fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a # !fir_out_del_ff_adffs_a13_a & (fir_out_a13_a_acombout # 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a13_a,
	datab => fir_out_a13_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a_a1045,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a);

fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a_a1044_I : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a = CARRY(fir_out_del_ff_adffs_a14_a & (!fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a # !fir_out_a14_a_acombout) # !fir_out_del_ff_adffs_a14_a & !fir_out_a14_a_acombout & 
-- !fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a14_a,
	datab => fir_out_a14_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a_a1044,
	cout => fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a);

fir_pslope_compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- fir_pslope_compare_acomparator_acmp_end_aagb_out = fir_out_del_ff_adffs_a15_a & (fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a & fir_out_a15_a_acombout) # !fir_out_del_ff_adffs_a15_a & (fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a # 
-- fir_out_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F550",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_del_ff_adffs_a15_a,
	datad => fir_out_a15_a_acombout,
	cin => fir_pslope_compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_pslope_compare_acomparator_acmp_end_aagb_out);

max_fir_en_a13_I : apex20ke_lcell
-- Equation(s):
-- max_fir_en_a13 = max_fir_clr_a23 # !fir_max_cmp_acomparator_acmp_end_aagb_out & !fir_pslope_compare_acomparator_acmp_end_aagb_out # !max_fir_clr_a22

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DDDF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => max_fir_clr_a22,
	datab => max_fir_clr_a23,
	datac => fir_max_cmp_acomparator_acmp_end_aagb_out,
	datad => fir_pslope_compare_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => max_fir_en_a13);

max_fir_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a4_a = DFFE(fir_out_a4_a_acombout & PBUSY_acombout & !EDisc_Out_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_out_a4_a_acombout,
	datab => PBUSY_acombout,
	datac => EDisc_Out_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a4_a);

Cpeak_Mux_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a4_a = DFFE(max_fir_ff_adffs_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a4_a);

memory_access_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_memory_access,
	combout => memory_access_acombout);

ROI_ADDR_a0_a_a56_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a0_a_a56 = memory_access_acombout & DSP_RAMA_a0_a_acombout # !memory_access_acombout & (Cpeak_Mux_ff_adffs_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_RAMA_a0_a_acombout,
	datac => Cpeak_Mux_ff_adffs_a4_a,
	datad => memory_access_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a0_a_a56);

Cpeak_Mux_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a5_a = DFFE(max_fir_ff_adffs_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a5_a);

DSP_RAMA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(1),
	combout => DSP_RAMA_a1_a_acombout);

ROI_ADDR_a1_a_a57_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a1_a_a57 = memory_access_acombout & (DSP_RAMA_a1_a_acombout) # !memory_access_acombout & Cpeak_Mux_ff_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => memory_access_acombout,
	datac => Cpeak_Mux_ff_adffs_a5_a,
	datad => DSP_RAMA_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a1_a_a57);

DSP_RAMA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(2),
	combout => DSP_RAMA_a2_a_acombout);

Cpeak_Mux_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a6_a = DFFE(max_fir_ff_adffs_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a6_a);

ROI_ADDR_a2_a_a58_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a2_a_a58 = memory_access_acombout & DSP_RAMA_a2_a_acombout # !memory_access_acombout & (Cpeak_Mux_ff_adffs_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_RAMA_a2_a_acombout,
	datac => Cpeak_Mux_ff_adffs_a6_a,
	datad => memory_access_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a2_a_a58);

max_fir_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a7_a = DFFE(!EDisc_Out_acombout & fir_out_a7_a_acombout & PBUSY_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EDisc_Out_acombout,
	datab => fir_out_a7_a_acombout,
	datac => PBUSY_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a7_a);

Cpeak_Mux_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a7_a = DFFE(max_fir_ff_adffs_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a7_a);

DSP_RAMA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(3),
	combout => DSP_RAMA_a3_a_acombout);

ROI_ADDR_a3_a_a59_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a3_a_a59 = memory_access_acombout & (DSP_RAMA_a3_a_acombout) # !memory_access_acombout & Cpeak_Mux_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => memory_access_acombout,
	datac => Cpeak_Mux_ff_adffs_a7_a,
	datad => DSP_RAMA_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a3_a_a59);

DSP_RAMA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(4),
	combout => DSP_RAMA_a4_a_acombout);

max_fir_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a8_a = DFFE(PBUSY_acombout & fir_out_a8_a_acombout & !EDisc_Out_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PBUSY_acombout,
	datab => fir_out_a8_a_acombout,
	datac => EDisc_Out_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a8_a);

Cpeak_Mux_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a8_a = DFFE(max_fir_ff_adffs_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a8_a);

ROI_ADDR_a4_a_a60_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a4_a_a60 = memory_access_acombout & DSP_RAMA_a4_a_acombout # !memory_access_acombout & (Cpeak_Mux_ff_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_RAMA_a4_a_acombout,
	datac => Cpeak_Mux_ff_adffs_a8_a,
	datad => memory_access_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a4_a_a60);

Cpeak_Mux_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a9_a = DFFE(max_fir_ff_adffs_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a9_a);

DSP_RAMA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(5),
	combout => DSP_RAMA_a5_a_acombout);

ROI_ADDR_a5_a_a61_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a5_a_a61 = memory_access_acombout & (DSP_RAMA_a5_a_acombout) # !memory_access_acombout & Cpeak_Mux_ff_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC30",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => memory_access_acombout,
	datac => Cpeak_Mux_ff_adffs_a9_a,
	datad => DSP_RAMA_a5_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a5_a_a61);

Cpeak_Mux_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a10_a = DFFE(max_fir_ff_adffs_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a10_a);

ROI_ADDR_a6_a_a62_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a6_a_a62 = memory_access_acombout & DSP_RAMA_a6_a_acombout # !memory_access_acombout & (Cpeak_Mux_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_RAMA_a6_a_acombout,
	datac => Cpeak_Mux_ff_adffs_a10_a,
	datad => memory_access_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a6_a_a62);

Cpeak_Mux_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a11_a = DFFE(max_fir_ff_adffs_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a11_a);

ROI_ADDR_a7_a_a63_I : apex20ke_lcell
-- Equation(s):
-- ROI_ADDR_a7_a_a63 = memory_access_acombout & DSP_RAMA_a7_a_acombout # !memory_access_acombout & (Cpeak_Mux_ff_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_RAMA_a7_a_acombout,
	datac => Cpeak_Mux_ff_adffs_a11_a,
	datad => memory_access_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_ADDR_a7_a_a63);

ROI_RAM_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a0_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a0_a_modesel,
	dataout => ROI_RAM_asram_aq_a0_a);

DSP_D_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(1),
	combout => DSP_D_a1_a_acombout);

ROI_RAM_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a1_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a1_a_modesel,
	dataout => ROI_RAM_asram_aq_a1_a);

DSP_D_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(2),
	combout => DSP_D_a2_a_acombout);

ROI_RAM_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a2_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a2_a_modesel,
	dataout => ROI_RAM_asram_aq_a2_a);

DSP_D_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(3),
	combout => DSP_D_a3_a_acombout);

ROI_RAM_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a3_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a3_a_modesel,
	dataout => ROI_RAM_asram_aq_a3_a);

DSP_D_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(4),
	combout => DSP_D_a4_a_acombout);

ROI_RAM_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a4_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a4_a_modesel,
	dataout => ROI_RAM_asram_aq_a4_a);

DSP_D_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(5),
	combout => DSP_D_a5_a_acombout);

ROI_RAM_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a5_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a5_a_modesel,
	dataout => ROI_RAM_asram_aq_a5_a);

DSP_D_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(6),
	combout => DSP_D_a6_a_acombout);

ROI_RAM_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a6_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a6_a_modesel,
	dataout => ROI_RAM_asram_aq_a6_a);

DSP_D_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(7),
	combout => DSP_D_a7_a_acombout);

ROI_RAM_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a7_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a7_a_modesel,
	dataout => ROI_RAM_asram_aq_a7_a);

DSP_D_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(8),
	combout => DSP_D_a8_a_acombout);

ROI_RAM_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a8_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a8_a_modesel,
	dataout => ROI_RAM_asram_aq_a8_a);

DSP_D_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(9),
	combout => DSP_D_a9_a_acombout);

ROI_RAM_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a9_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a9_a_modesel,
	dataout => ROI_RAM_asram_aq_a9_a);

DSP_D_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(10),
	combout => DSP_D_a10_a_acombout);

ROI_RAM_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a10_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a10_a_modesel,
	dataout => ROI_RAM_asram_aq_a10_a);

DSP_D_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(11),
	combout => DSP_D_a11_a_acombout);

ROI_RAM_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a11_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a11_a_modesel,
	dataout => ROI_RAM_asram_aq_a11_a);

DSP_D_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(12),
	combout => DSP_D_a12_a_acombout);

ROI_RAM_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a12_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a12_a_modesel,
	dataout => ROI_RAM_asram_aq_a12_a);

DSP_D_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(13),
	combout => DSP_D_a13_a_acombout);

ROI_RAM_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a13_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a13_a_modesel,
	dataout => ROI_RAM_asram_aq_a13_a);

DSP_D_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(14),
	combout => DSP_D_a14_a_acombout);

ROI_RAM_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a14_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a14_a_modesel,
	dataout => ROI_RAM_asram_aq_a14_a);

DSP_D_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(15),
	combout => DSP_D_a15_a_acombout);

ROI_RAM_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 8,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "ROI_RAM.MIF",
	last_address => 255,
	logical_ram_depth => 256,
	logical_ram_name => "lpm_ram_dq:ROI_RAM|altram:sram|content",
	logical_ram_width => 16,
	operation_mode => "single_port",
	read_address_clear => "none",
	read_address_clock => "clock0",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => DSP_D_a15_a_acombout,
	clk0 => clk_acombout,
	clk1 => clk_acombout,
	we => ROI_WE_acombout,
	waddr => ROI_RAM_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => ROI_RAM_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => ROI_RAM_asram_asegment_a0_a_a15_a_modesel,
	dataout => ROI_RAM_asram_aq_a15_a);

Cpeak_Mux_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a1_a = DFFE(max_fir_ff_adffs_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a1_a);

max_fir_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- max_fir_ff_adffs_a0_a = DFFE(!EDisc_Out_acombout & fir_out_a0_a_acombout & PBUSY_acombout & max_fir_clr_a22, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , max_fir_en_a13)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EDisc_Out_acombout,
	datab => fir_out_a0_a_acombout,
	datac => PBUSY_acombout,
	datad => max_fir_clr_a22,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => max_fir_en_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => max_fir_ff_adffs_a0_a);

Cpeak_Mux_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a0_a = DFFE(max_fir_ff_adffs_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a0_a);

ROI_DEF_a55_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a55 = Cpeak_Mux_ff_adffs_a1_a & (Cpeak_Mux_ff_adffs_a0_a) # !Cpeak_Mux_ff_adffs_a1_a & (Cpeak_Mux_ff_adffs_a0_a & (ROI_RAM_asram_aq_a9_a) # !Cpeak_Mux_ff_adffs_a0_a & ROI_RAM_asram_aq_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F2C2",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_RAM_asram_aq_a8_a,
	datab => Cpeak_Mux_ff_adffs_a1_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_RAM_asram_aq_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a55);

ROI_DEF_a56_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a56 = ROI_DEF_a55 & (ROI_RAM_asram_aq_a11_a # !Cpeak_Mux_ff_adffs_a1_a) # !ROI_DEF_a55 & ROI_RAM_asram_aq_a10_a & (Cpeak_Mux_ff_adffs_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E2CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_RAM_asram_aq_a10_a,
	datab => ROI_DEF_a55,
	datac => ROI_RAM_asram_aq_a11_a,
	datad => Cpeak_Mux_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a56);

Cpeak_Mux_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a3_a = DFFE(max_fir_ff_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a3_a);

ROI_DEF_a63_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a63 = ROI_DEF_a62 & (ROI_RAM_asram_aq_a15_a # !Cpeak_Mux_ff_adffs_a0_a) # !ROI_DEF_a62 & (Cpeak_Mux_ff_adffs_a0_a & ROI_RAM_asram_aq_a13_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DA8A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_DEF_a62,
	datab => ROI_RAM_asram_aq_a15_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => ROI_RAM_asram_aq_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a63);

ROI_DEF_a64_I : apex20ke_lcell
-- Equation(s):
-- ROI_DEF_a64 = ROI_DEF_a61 & (ROI_DEF_a63 # !Cpeak_Mux_ff_adffs_a3_a) # !ROI_DEF_a61 & ROI_DEF_a56 & Cpeak_Mux_ff_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EA4A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_DEF_a61,
	datab => ROI_DEF_a56,
	datac => Cpeak_Mux_ff_adffs_a3_a,
	datad => ROI_DEF_a63,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => ROI_DEF_a64);

Cpeak_Mux_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a12_a = DFFE(max_fir_ff_adffs_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => max_fir_ff_adffs_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a12_a);

clock_proc_a101_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a101 = PD_Vec_a4_a & !PD_Vec_a5_a & !Cpeak_Mux_ff_adffs_a12_a & !PUR_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PD_Vec_a4_a,
	datab => PD_Vec_a5_a,
	datac => Cpeak_Mux_ff_adffs_a12_a,
	datad => PUR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a101);

EDX_XFER_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EDX_XFER,
	combout => EDX_XFER_acombout);

Cpeak_Mux_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_Mux_ff_adffs_a2_a = DFFE(max_fir_ff_adffs_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => max_fir_ff_adffs_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_Mux_ff_adffs_a2_a);

Equal_a162_I : apex20ke_lcell
-- Equation(s):
-- Equal_a168 = !Cpeak_Mux_ff_adffs_a10_a & !Cpeak_Mux_ff_adffs_a9_a & !Cpeak_Mux_ff_adffs_a11_a & !Cpeak_Mux_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a10_a,
	datab => Cpeak_Mux_ff_adffs_a9_a,
	datac => Cpeak_Mux_ff_adffs_a11_a,
	datad => Cpeak_Mux_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a162,
	cascout => Equal_a168);

Equal_a165_I : apex20ke_lcell
-- Equation(s):
-- Equal_a169 = (!Cpeak_Mux_ff_adffs_a3_a & !Cpeak_Mux_ff_adffs_a1_a & !Cpeak_Mux_ff_adffs_a0_a & !Cpeak_Mux_ff_adffs_a2_a) & CASCADE(Equal_a168)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a3_a,
	datab => Cpeak_Mux_ff_adffs_a1_a,
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => Cpeak_Mux_ff_adffs_a2_a,
	cascin => Equal_a168,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a165,
	cascout => Equal_a169);

Equal_a164_I : apex20ke_lcell
-- Equation(s):
-- Equal_a164 = (!Cpeak_Mux_ff_adffs_a7_a & !Cpeak_Mux_ff_adffs_a5_a & !Cpeak_Mux_ff_adffs_a4_a & !Cpeak_Mux_ff_adffs_a6_a) & CASCADE(Equal_a169)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a7_a,
	datab => Cpeak_Mux_ff_adffs_a5_a,
	datac => Cpeak_Mux_ff_adffs_a4_a,
	datad => Cpeak_Mux_ff_adffs_a6_a,
	cascin => Equal_a169,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a164);

clock_proc_a99_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a99 = PD_Vec_a4_a & !Cpeak_Mux_ff_adffs_a12_a & !Equal_a164 & max_fir_clr_a22

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => PD_Vec_a4_a,
	datab => Cpeak_Mux_ff_adffs_a12_a,
	datac => Equal_a164,
	datad => max_fir_clr_a22,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a99);

CLIP_MIN_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(10),
	combout => CLIP_MIN_a10_a_acombout);

CLIP_MIN_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(8),
	combout => CLIP_MIN_a8_a_acombout);

CLIP_MIN_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(7),
	combout => CLIP_MIN_a7_a_acombout);

CLIP_MIN_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(6),
	combout => CLIP_MIN_a6_a_acombout);

CLIP_MIN_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(5),
	combout => CLIP_MIN_a5_a_acombout);

CLIP_MIN_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(3),
	combout => CLIP_MIN_a3_a_acombout);

CLIP_MIN_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(2),
	combout => CLIP_MIN_a2_a_acombout);

CLIP_MIN_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MIN(0),
	combout => CLIP_MIN_a0_a_acombout);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a_a840_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(!Cpeak_Mux_ff_adffs_a0_a & CLIP_MIN_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a0_a,
	datab => CLIP_MIN_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a_a840,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a_a839_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(CLIP_MIN_a1_a_acombout & Cpeak_Mux_ff_adffs_a1_a & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a # !CLIP_MIN_a1_a_acombout & (Cpeak_Mux_ff_adffs_a1_a # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MIN_a1_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a1_a,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a_a839,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a_a838_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(Cpeak_Mux_ff_adffs_a2_a & CLIP_MIN_a2_a_acombout & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a # !Cpeak_Mux_ff_adffs_a2_a & (CLIP_MIN_a2_a_acombout # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a2_a,
	datab => CLIP_MIN_a2_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a_a838,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a_a837_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(Cpeak_Mux_ff_adffs_a3_a & (!Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a # !CLIP_MIN_a3_a_acombout) # !Cpeak_Mux_ff_adffs_a3_a & !CLIP_MIN_a3_a_acombout & 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a3_a,
	datab => CLIP_MIN_a3_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a_a837,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a_a836_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(CLIP_MIN_a4_a_acombout & (!Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a # !Cpeak_Mux_ff_adffs_a4_a) # !CLIP_MIN_a4_a_acombout & !Cpeak_Mux_ff_adffs_a4_a & 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MIN_a4_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a4_a,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a_a836,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a_a835_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(Cpeak_Mux_ff_adffs_a5_a & (!Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a # !CLIP_MIN_a5_a_acombout) # !Cpeak_Mux_ff_adffs_a5_a & !CLIP_MIN_a5_a_acombout & 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a5_a,
	datab => CLIP_MIN_a5_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a_a835,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a_a834_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(Cpeak_Mux_ff_adffs_a6_a & CLIP_MIN_a6_a_acombout & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a # !Cpeak_Mux_ff_adffs_a6_a & (CLIP_MIN_a6_a_acombout # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a6_a,
	datab => CLIP_MIN_a6_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a_a834,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a_a833_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(Cpeak_Mux_ff_adffs_a7_a & (!Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a # !CLIP_MIN_a7_a_acombout) # !Cpeak_Mux_ff_adffs_a7_a & !CLIP_MIN_a7_a_acombout & 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a7_a,
	datab => CLIP_MIN_a7_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a_a833,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a_a832_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(Cpeak_Mux_ff_adffs_a8_a & CLIP_MIN_a8_a_acombout & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a # !Cpeak_Mux_ff_adffs_a8_a & (CLIP_MIN_a8_a_acombout # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a8_a,
	datab => CLIP_MIN_a8_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a_a832,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a_a831_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(CLIP_MIN_a9_a_acombout & Cpeak_Mux_ff_adffs_a9_a & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a # !CLIP_MIN_a9_a_acombout & (Cpeak_Mux_ff_adffs_a9_a # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MIN_a9_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a9_a,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a_a831,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a_a830_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(Cpeak_Mux_ff_adffs_a10_a & CLIP_MIN_a10_a_acombout & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a # !Cpeak_Mux_ff_adffs_a10_a & (CLIP_MIN_a10_a_acombout # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a10_a,
	datab => CLIP_MIN_a10_a_acombout,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a_a830,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a);

Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a_a829_I : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(CLIP_MIN_a11_a_acombout & Cpeak_Mux_ff_adffs_a11_a & !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a # !CLIP_MIN_a11_a_acombout & (Cpeak_Mux_ff_adffs_a11_a # 
-- !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MIN_a11_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a11_a,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a_a829,
	cout => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a);

Clip_Min_Cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- Clip_Min_Cmp_acomparator_acmp_end_aagb_out = Cpeak_Mux_ff_adffs_a12_a # !Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Cpeak_Mux_ff_adffs_a12_a,
	cin => Clip_Min_Cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Min_Cmp_acomparator_acmp_end_aagb_out);

CLIP_MAX_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(11),
	combout => CLIP_MAX_a11_a_acombout);

CLIP_MAX_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(10),
	combout => CLIP_MAX_a10_a_acombout);

CLIP_MAX_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(9),
	combout => CLIP_MAX_a9_a_acombout);

CLIP_MAX_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(8),
	combout => CLIP_MAX_a8_a_acombout);

CLIP_MAX_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(7),
	combout => CLIP_MAX_a7_a_acombout);

CLIP_MAX_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(5),
	combout => CLIP_MAX_a5_a_acombout);

CLIP_MAX_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(1),
	combout => CLIP_MAX_a1_a_acombout);

CLIP_MAX_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLIP_MAX(0),
	combout => CLIP_MAX_a0_a_acombout);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a_a833_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(Cpeak_Mux_ff_adffs_a0_a & !CLIP_MAX_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a0_a,
	datab => CLIP_MAX_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a_a833,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a_a832_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(Cpeak_Mux_ff_adffs_a1_a & CLIP_MAX_a1_a_acombout & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a # !Cpeak_Mux_ff_adffs_a1_a & (CLIP_MAX_a1_a_acombout # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a1_a,
	datab => CLIP_MAX_a1_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a_a832,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a_a831_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(CLIP_MAX_a2_a_acombout & Cpeak_Mux_ff_adffs_a2_a & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a # !CLIP_MAX_a2_a_acombout & (Cpeak_Mux_ff_adffs_a2_a # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MAX_a2_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a2_a,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a_a831,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a_a830_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(CLIP_MAX_a3_a_acombout & (!Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a # !Cpeak_Mux_ff_adffs_a3_a) # !CLIP_MAX_a3_a_acombout & !Cpeak_Mux_ff_adffs_a3_a & 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MAX_a3_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a3_a,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a_a830,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a_a829_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(CLIP_MAX_a4_a_acombout & Cpeak_Mux_ff_adffs_a4_a & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a # !CLIP_MAX_a4_a_acombout & (Cpeak_Mux_ff_adffs_a4_a # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MAX_a4_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a4_a,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a_a829,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a_a828_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(Cpeak_Mux_ff_adffs_a5_a & CLIP_MAX_a5_a_acombout & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a # !Cpeak_Mux_ff_adffs_a5_a & (CLIP_MAX_a5_a_acombout # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a5_a,
	datab => CLIP_MAX_a5_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a_a828,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a_a827_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(CLIP_MAX_a6_a_acombout & Cpeak_Mux_ff_adffs_a6_a & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a # !CLIP_MAX_a6_a_acombout & (Cpeak_Mux_ff_adffs_a6_a # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CLIP_MAX_a6_a_acombout,
	datab => Cpeak_Mux_ff_adffs_a6_a,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a_a827,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a_a826_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(Cpeak_Mux_ff_adffs_a7_a & CLIP_MAX_a7_a_acombout & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a # !Cpeak_Mux_ff_adffs_a7_a & (CLIP_MAX_a7_a_acombout # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a7_a,
	datab => CLIP_MAX_a7_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a_a826,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a_a825_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(Cpeak_Mux_ff_adffs_a8_a & (!Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a # !CLIP_MAX_a8_a_acombout) # !Cpeak_Mux_ff_adffs_a8_a & !CLIP_MAX_a8_a_acombout & 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a8_a,
	datab => CLIP_MAX_a8_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a_a825,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a_a824_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(Cpeak_Mux_ff_adffs_a9_a & CLIP_MAX_a9_a_acombout & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a # !Cpeak_Mux_ff_adffs_a9_a & (CLIP_MAX_a9_a_acombout # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a9_a,
	datab => CLIP_MAX_a9_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a_a824,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a_a823_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(Cpeak_Mux_ff_adffs_a10_a & (!Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a # !CLIP_MAX_a10_a_acombout) # !Cpeak_Mux_ff_adffs_a10_a & !CLIP_MAX_a10_a_acombout & 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a10_a,
	datab => CLIP_MAX_a10_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a_a823,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a);

Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a_a822_I : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(Cpeak_Mux_ff_adffs_a11_a & CLIP_MAX_a11_a_acombout & !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a # !Cpeak_Mux_ff_adffs_a11_a & (CLIP_MAX_a11_a_acombout # 
-- !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cpeak_Mux_ff_adffs_a11_a,
	datab => CLIP_MAX_a11_a_acombout,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a_a822,
	cout => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a);

Clip_Max_Cmp_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- Clip_Max_Cmp_acomparator_acmp_end_aagb_out = !Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a & !Cpeak_Mux_ff_adffs_a12_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Cpeak_Mux_ff_adffs_a12_a,
	cin => Clip_Max_Cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Clip_Max_Cmp_acomparator_acmp_end_aagb_out);

clock_proc_a100_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a100 = EDX_XFER_acombout & clock_proc_a99 & !Clip_Min_Cmp_acomparator_acmp_end_aagb_out & !Clip_Max_Cmp_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EDX_XFER_acombout,
	datab => clock_proc_a99,
	datac => Clip_Min_Cmp_acomparator_acmp_end_aagb_out,
	datad => Clip_Max_Cmp_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a100);

Meas_Done_areg0_I : apex20ke_lcell
-- Equation(s):
-- Meas_Done_areg0 = DFFE(clock_proc_a100 $ (ROI_DEF_a64 & clock_proc_a101 & !EDX_XFER_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F708",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_DEF_a64,
	datab => clock_proc_a101,
	datac => EDX_XFER_acombout,
	datad => clock_proc_a100,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Meas_Done_areg0);

Time_Enable_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => Time_Enable_acombout);

ROI_INC_aI : apex20ke_lcell
-- Equation(s):
-- ROI_INC = DFFE(clock_proc_a102 & max_fir_clr_a22 & ROI_DEF_a64 & Time_Enable_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a102,
	datab => max_fir_clr_a22,
	datac => ROI_DEF_a64,
	datad => Time_Enable_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_INC);

Time_CLr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_CLr,
	combout => Time_CLr_acombout);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_INC $ (ROI_SUM_CNTR_awysi_counter_asload_path_a0_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a0_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a1_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a1_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a1_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a2_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a2_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a2_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a2_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a3_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a3_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a3_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a4_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a4_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a4_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a4_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a5_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a5_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a5_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a6_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a6_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a6_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a6_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a7_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a7_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a7_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a8_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a8_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a8_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a8_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a9_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a9_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a9_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a10_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a10_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a10_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a10_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a11_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a11_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a11_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a12_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a12_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a12_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a12_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a13_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a13_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a13_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a14_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a14_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a14_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a14_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a15_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a15_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a15_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a16_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a16_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a16_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a16_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a17_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a17_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a17_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a18_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a18_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a18_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a18_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a19_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a19_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a19_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a20_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a20_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a20_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a20_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a21_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a21_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a21_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a22_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a22_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a22_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a22_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a23_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a23_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a23_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a24_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a24_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a24_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a24_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a25_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a25_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a25_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a26_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a26_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a26_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a26_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a27_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a27_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a27_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a28_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a28_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a28_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a28_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a29_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT # !ROI_SUM_CNTR_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a29_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a29_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a30_a $ (ROI_INC & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(ROI_SUM_CNTR_awysi_counter_asload_path_a30_a & !ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datab => ROI_SUM_CNTR_awysi_counter_asload_path_a30_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a30_a,
	cout => ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a_aCOUT);

ROI_SUM_CNTR_awysi_counter_acounter_cell_a31_a : apex20ke_lcell
-- Equation(s):
-- ROI_SUM_CNTR_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(Time_CLr_acombout) & ROI_SUM_CNTR_awysi_counter_asload_path_a31_a $ (ROI_INC & ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a_aCOUT), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5FA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => ROI_INC,
	datad => ROI_SUM_CNTR_awysi_counter_asload_path_a31_a,
	cin => ROI_SUM_CNTR_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => clk_acombout,
	aclr => imr_acombout,
	sclr => Time_CLr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => ROI_SUM_CNTR_awysi_counter_asload_path_a31_a);

cpeak_en_a0_I : apex20ke_lcell
-- Equation(s):
-- cpeak_en_a0 = PD_Vec_a4_a # EDisc_Out_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PD_Vec_a4_a,
	datad => EDisc_Out_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cpeak_en_a0);

Cpeak_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a0_a = DFFE(Cpeak_Mux_ff_adffs_a0_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a0_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a0_a);

Cpeak_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a1_a = DFFE(Cpeak_Mux_ff_adffs_a1_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a1_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a1_a);

Cpeak_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a2_a = DFFE(Cpeak_Mux_ff_adffs_a2_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a2_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a2_a);

Cpeak_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a3_a = DFFE(Cpeak_Mux_ff_adffs_a3_a & (!EDisc_Out_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cpeak_Mux_ff_adffs_a3_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a3_a);

Cpeak_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a4_a = DFFE(Cpeak_Mux_ff_adffs_a4_a & (!EDisc_Out_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cpeak_Mux_ff_adffs_a4_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a4_a);

Cpeak_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a5_a = DFFE(Cpeak_Mux_ff_adffs_a5_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a5_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a5_a);

Cpeak_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a6_a = DFFE(Cpeak_Mux_ff_adffs_a6_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a6_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a6_a);

Cpeak_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a7_a = DFFE(Cpeak_Mux_ff_adffs_a7_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a7_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a7_a);

Cpeak_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a8_a = DFFE(Cpeak_Mux_ff_adffs_a8_a & !EDisc_Out_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Cpeak_Mux_ff_adffs_a8_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a8_a);

Cpeak_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a9_a = DFFE(Cpeak_Mux_ff_adffs_a9_a & (!EDisc_Out_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cpeak_Mux_ff_adffs_a9_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a9_a);

Cpeak_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a10_a = DFFE(Cpeak_Mux_ff_adffs_a10_a & (!EDisc_Out_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cpeak_Mux_ff_adffs_a10_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a10_a);

Cpeak_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Cpeak_ff_adffs_a11_a = DFFE(Cpeak_Mux_ff_adffs_a11_a & (!EDisc_Out_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , cpeak_en_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cpeak_Mux_ff_adffs_a11_a,
	datad => EDisc_Out_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	ena => cpeak_en_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cpeak_ff_adffs_a11_a);

DSP_RAMA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(8));

DSP_RAMA_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(9));

DSP_RAMA_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_RAMA(10));

ROI_LU_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(0));

ROI_LU_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(1));

ROI_LU_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(2));

ROI_LU_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(3));

ROI_LU_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(4));

ROI_LU_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(5));

ROI_LU_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(6));

ROI_LU_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(7));

ROI_LU_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(8));

ROI_LU_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(9));

ROI_LU_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(10));

ROI_LU_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(11));

ROI_LU_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(12));

ROI_LU_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(13));

ROI_LU_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(14));

ROI_LU_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_RAM_asram_aq_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_LU(15));

Meas_Done_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Meas_Done_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Meas_Done);

ROI_SUM_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(0));

ROI_SUM_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(1));

ROI_SUM_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(2));

ROI_SUM_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(3));

ROI_SUM_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(4));

ROI_SUM_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(5));

ROI_SUM_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(6));

ROI_SUM_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(7));

ROI_SUM_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(8));

ROI_SUM_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(9));

ROI_SUM_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(10));

ROI_SUM_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(11));

ROI_SUM_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(12));

ROI_SUM_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(13));

ROI_SUM_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(14));

ROI_SUM_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(15));

ROI_SUM_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(16));

ROI_SUM_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(17));

ROI_SUM_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(18));

ROI_SUM_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(19));

ROI_SUM_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(20));

ROI_SUM_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(21));

ROI_SUM_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(22));

ROI_SUM_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(23));

ROI_SUM_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(24));

ROI_SUM_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(25));

ROI_SUM_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(26));

ROI_SUM_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(27));

ROI_SUM_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(28));

ROI_SUM_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(29));

ROI_SUM_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(30));

ROI_SUM_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ROI_SUM_CNTR_awysi_counter_asload_path_a31_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ROI_SUM(31));

CPEak_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(0));

CPEak_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(1));

CPEak_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(2));

CPEak_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(3));

CPEak_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(4));

CPEak_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(5));

CPEak_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(6));

CPEak_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(7));

CPEak_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(8));

CPEak_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(9));

CPEak_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(10));

CPEak_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cpeak_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_CPEak(11));
END structure;


