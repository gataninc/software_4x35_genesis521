onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/tconv_sel
add wave -noupdate -format Logic -radix binary /tb/time_clr
add wave -noupdate -format Logic -radix binary /tb/time_enable
add wave -noupdate -format Logic -radix binary /tb/pur
add wave -noupdate -format Logic -radix binary /tb/pbusy
add wave -noupdate -format Logic -radix binary /tb/edisc_out
add wave -noupdate -format Logic -radix binary /tb/peak_done
add wave -noupdate -format Logic -radix binary /tb/memory_access
add wave -noupdate -format Logic -radix binary /tb/roi_we
add wave -noupdate -format Literal -radix hexadecimal /tb/offset_lu
add wave -noupdate -format Logic -radix binary /tb/edx_xfer
add wave -noupdate -format Literal -radix hexadecimal /tb/clip_min
add wave -noupdate -format Literal -radix hexadecimal /tb/clip_max
add wave -noupdate -format Literal -radix hexadecimal /tb/fir_out
add wave -noupdate -format Literal -radix hexadecimal /tb/fgain
add wave -noupdate -format Literal -radix hexadecimal /tb/offset
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_d
add wave -noupdate -format Literal -radix hexadecimal /tb/dsp_rama
add wave -noupdate -format Literal -radix hexadecimal /tb/roi_lu
add wave -noupdate -format Logic -radix binary /tb/meas_done
add wave -noupdate -format Literal -radix hexadecimal /tb/roi_sum
add wave -noupdate -format Literal -radix hexadecimal /tb/cpeak
add wave -noupdate -format Literal -radix hexadecimal /tb/gz_product_out
add wave -noupdate -format Literal -radix hexadecimal /tb/gproduct
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.25 /tb/fir_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {4153 ns} {169162 ns} {189838 ns}
WaveRestoreZoom {22255 ns} {33954 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
