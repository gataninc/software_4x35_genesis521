-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:35:07"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	hotswap IS
    PORT (
	imr : IN std_logic;
	clk20 : IN std_logic;
	ms100_tc : IN std_logic;
	Sense : IN std_logic;
	DC_ON : OUT std_logic
	);
END hotswap;

ARCHITECTURE structure OF hotswap IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk20 : std_logic;
SIGNAL ww_ms100_tc : std_logic;
SIGNAL ww_Sense : std_logic;
SIGNAL ww_DC_ON : std_logic;
SIGNAL DC_DC_DLY_CNT_a3_a_a79 : std_logic;
SIGNAL DC_DC_DLY_CNT_a3_a : std_logic;
SIGNAL DC_DC_DLY_CNT_a2_a_a82 : std_logic;
SIGNAL DC_DC_DLY_CNT_a2_a : std_logic;
SIGNAL DC_DC_DLY_CNT_a1_a_a85 : std_logic;
SIGNAL DC_DC_DLY_CNT_a1_a : std_logic;
SIGNAL DC_DC_DLY_CNT_a0_a_a88 : std_logic;
SIGNAL DC_DC_DLY_CNT_a0_a : std_logic;
SIGNAL DC_DC_DLY_CEN_a329 : std_logic;
SIGNAL DC_DC_DLY_CNT_a5_a : std_logic;
SIGNAL DC_DC_DLY_CNT_a4_a_a94 : std_logic;
SIGNAL DC_DC_DLY_CNT_a4_a : std_logic;
SIGNAL DC_DC_DLY_CEN_a330 : std_logic;
SIGNAL Sense_acombout : std_logic;
SIGNAL clk20_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL ms100_tc_acombout : std_logic;
SIGNAL sense_vec_a0_a_a208 : std_logic;
SIGNAL sense_vec_a1_a_a11 : std_logic;
SIGNAL sense_vec_a0_a_a209 : std_logic;
SIGNAL sense_vec_a1_a_a210 : std_logic;
SIGNAL Equal_a63 : std_logic;
SIGNAL DC_DC_DLY_CEN : std_logic;
SIGNAL DC_ON_areg0 : std_logic;
SIGNAL ALT_INV_DC_DC_DLY_CEN : std_logic;

BEGIN

ww_imr <= imr;
ww_clk20 <= clk20;
ww_ms100_tc <= ms100_tc;
ww_Sense <= Sense;
DC_ON <= ww_DC_ON;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_DC_DC_DLY_CEN <= NOT DC_DC_DLY_CEN;

DC_DC_DLY_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a3_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & DC_DC_DLY_CNT_a3_a $ DC_DC_DLY_CNT_a2_a_a82, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)
-- DC_DC_DLY_CNT_a3_a_a79 = CARRY(!DC_DC_DLY_CNT_a2_a_a82 # !DC_DC_DLY_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DC_DC_DLY_CNT_a3_a,
	cin => DC_DC_DLY_CNT_a2_a_a82,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a3_a,
	cout => DC_DC_DLY_CNT_a3_a_a79);

DC_DC_DLY_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a2_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & DC_DC_DLY_CNT_a2_a $ (!DC_DC_DLY_CNT_a1_a_a85), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)
-- DC_DC_DLY_CNT_a2_a_a82 = CARRY(DC_DC_DLY_CNT_a2_a & (!DC_DC_DLY_CNT_a1_a_a85))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DC_DC_DLY_CNT_a2_a,
	cin => DC_DC_DLY_CNT_a1_a_a85,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a2_a,
	cout => DC_DC_DLY_CNT_a2_a_a82);

DC_DC_DLY_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a1_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & DC_DC_DLY_CNT_a1_a $ DC_DC_DLY_CNT_a0_a_a88, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)
-- DC_DC_DLY_CNT_a1_a_a85 = CARRY(!DC_DC_DLY_CNT_a0_a_a88 # !DC_DC_DLY_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DC_DC_DLY_CNT_a1_a,
	cin => DC_DC_DLY_CNT_a0_a_a88,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a1_a,
	cout => DC_DC_DLY_CNT_a1_a_a85);

DC_DC_DLY_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a0_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & !DC_DC_DLY_CNT_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)
-- DC_DC_DLY_CNT_a0_a_a88 = CARRY(DC_DC_DLY_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "55AA",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DC_DC_DLY_CNT_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a0_a,
	cout => DC_DC_DLY_CNT_a0_a_a88);

DC_DC_DLY_CEN_a329_I : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CEN_a329 = DC_DC_DLY_CNT_a2_a # DC_DC_DLY_CNT_a1_a # DC_DC_DLY_CNT_a3_a # !DC_DC_DLY_CNT_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFEF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DC_DC_DLY_CNT_a2_a,
	datab => DC_DC_DLY_CNT_a1_a,
	datac => DC_DC_DLY_CNT_a0_a,
	datad => DC_DC_DLY_CNT_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DC_DC_DLY_CEN_a329);

DC_DC_DLY_CNT_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a5_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & DC_DC_DLY_CNT_a4_a_a94 $ DC_DC_DLY_CNT_a5_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DC_DC_DLY_CNT_a5_a,
	cin => DC_DC_DLY_CNT_a4_a_a94,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a5_a);

DC_DC_DLY_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CNT_a4_a = DFFE(GLOBAL(DC_DC_DLY_CEN) & DC_DC_DLY_CNT_a4_a $ !DC_DC_DLY_CNT_a3_a_a79, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)
-- DC_DC_DLY_CNT_a4_a_a94 = CARRY(DC_DC_DLY_CNT_a4_a & !DC_DC_DLY_CNT_a3_a_a79)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DC_DC_DLY_CNT_a4_a,
	cin => DC_DC_DLY_CNT_a3_a_a79,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	sclr => ALT_INV_DC_DC_DLY_CEN,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CNT_a4_a,
	cout => DC_DC_DLY_CNT_a4_a_a94);

DC_DC_DLY_CEN_a330_I : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CEN_a330 = DC_DC_DLY_CEN_a329 # !DC_DC_DLY_CNT_a5_a # !DC_DC_DLY_CNT_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DC_DC_DLY_CNT_a4_a,
	datac => DC_DC_DLY_CEN_a329,
	datad => DC_DC_DLY_CNT_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DC_DC_DLY_CEN_a330);

Sense_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Sense,
	combout => Sense_acombout);

clk20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk20,
	combout => clk20_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

ms100_tc_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ms100_tc,
	combout => ms100_tc_acombout);

sense_vec_a0_a_a208_I : apex20ke_lcell
-- Equation(s):
-- sense_vec_a0_a_a208 = DFFE(sense_vec_a1_a_a11 $ (Sense_acombout), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => sense_vec_a1_a_a11,
	datac => Sense_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => sense_vec_a0_a_a208);

sense_vec_a1_a_a11_I : apex20ke_lcell
-- Equation(s):
-- sense_vec_a1_a_a11 = imr_acombout & Sense_acombout # !imr_acombout & (sense_vec_a1_a_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sense_acombout,
	datac => sense_vec_a1_a_a11,
	datad => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sense_vec_a1_a_a11);

sense_vec_a0_a_a209_I : apex20ke_lcell
-- Equation(s):
-- sense_vec_a0_a_a209 = imr_acombout & Sense_acombout # !imr_acombout & (sense_vec_a0_a_a208 $ sense_vec_a1_a_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA3C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sense_acombout,
	datab => sense_vec_a0_a_a208,
	datac => sense_vec_a1_a_a11,
	datad => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => sense_vec_a0_a_a209);

sense_vec_a1_a_a210_I : apex20ke_lcell
-- Equation(s):
-- sense_vec_a1_a_a210 = DFFE(imr_acombout & (Sense_acombout $ (sense_vec_a1_a_a11)) # !imr_acombout & (sense_vec_a0_a_a208), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5ACC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sense_acombout,
	datab => sense_vec_a0_a_a208,
	datac => sense_vec_a1_a_a11,
	datad => imr_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => sense_vec_a1_a_a210);

Equal_a63_I : apex20ke_lcell
-- Equation(s):
-- Equal_a63 = Sense_acombout & !imr_acombout & (sense_vec_a1_a_a210 $ !sense_vec_a1_a_a11) # !Sense_acombout & (sense_vec_a1_a_a210 $ !sense_vec_a1_a_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "41C3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sense_acombout,
	datab => sense_vec_a1_a_a210,
	datac => sense_vec_a1_a_a11,
	datad => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a63);

DC_DC_DLY_CEN_aI : apex20ke_lcell
-- Equation(s):
-- DC_DC_DLY_CEN = DFFE(DC_DC_DLY_CEN_a330 & (DC_DC_DLY_CEN # sense_vec_a0_a_a209 & Equal_a63) # !DC_DC_DLY_CEN_a330 & sense_vec_a0_a_a209 & (Equal_a63), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ECA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DC_DC_DLY_CEN_a330,
	datab => sense_vec_a0_a_a209,
	datac => DC_DC_DLY_CEN,
	datad => Equal_a63,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_DC_DLY_CEN);

DC_ON_areg0_I : apex20ke_lcell
-- Equation(s):
-- DC_ON_areg0 = DFFE(!DC_DC_DLY_CEN & sense_vec_a0_a_a209 & (!Equal_a63), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , ms100_tc_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DC_DC_DLY_CEN,
	datab => sense_vec_a0_a_a209,
	datad => Equal_a63,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => ms100_tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DC_ON_areg0);

DC_ON_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DC_ON_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DC_ON);
END structure;


