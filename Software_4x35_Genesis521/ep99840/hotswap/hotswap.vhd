---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	LTC1595.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		Tclk High Min 							= 60ns
--		Tclk Low Min  							= 60ns
--		Load PW Min 							= 60ns
--		Dac Output ( Current Mode ) Settling Time	= 1 us
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

LIBRARY altera;
	USE altera.maxplus2.ALL;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
---------------------------------------------------------------------------------------------------

entity hotswap is
	port(
		imr 		: in		std_logic;
		clk20	: in		std_logic;
		ms100_tc	: in		std_logic;
		Sense	: in		std_logic;		-- TMP_SENSE(1)
		DC_ON	: buffer	std_logic );		-- HV_DIS(1)
end hotswap;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of hotswap is
	constant DC_DC_DLY_CNT_MAX		: integer := 49;

	signal sense_vec 		: std_logic_Vector( 1 downto 0 );
	signal DC_DC_DLY_CNT 	: integer range 0 to DC_DC_DLY_CNT_MAX;
	signal DC_DC_DLY_TC		: std_logic;
	signal DC_DC_DLY_CEN	: std_logic;
	

---------------------------------------------------------------------------------------------------
begin
	DC_DC_DLY_TC <= '1' when ( DC_DC_DLY_CNT = DC_DC_DLY_CNT_MAX ) else '0';

     ----------------------------------------------------------------------------------------------
	clk_proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			sense_vec <= sense_vec(0) & sense;
			dc_dc_dly_cen 	<= '0';
			DC_DC_DLY_CNT	<= 0;
			DC_ON		<= '0';
		elsif(( clk20'event ) and ( clk20 = '1' )) then
			if( ms100_tc = '1' ) then

				sense_VEC		<= sense_VEC(0) & sense;
			
				if( SENSE_VEC = "01" ) 
					then DC_DC_DLY_CEN 	<= '1';
				elsif( DC_DC_DLY_TC = '1' )
					then DC_DC_DLY_CEN <= '0';
				end if;
				
				if( DC_DC_DLY_CEN = '0' )
					then DC_DC_DLY_CNT	<= 0;
					else DC_DC_DLY_CNT 	<= DC_DC_DLY_CNT + 1;
				end if;
				

				if(( SENSE_VEC = "11" ) and ( DC_DC_DLY_CEN = '0' ))
					then DC_ON <= '1';		-- DC/DC On
					else DC_ON <= '0';		-- DC/DC Off
				end if;
			end if;					-- ms100_tc
		end if;						-- clock
	end process;					
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;
---------------------------------------------------------------------------------------------------
					
