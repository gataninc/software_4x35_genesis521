---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	AD9240.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--		Code to interface to the Analog Devices AD9240 A/D Converter
--		but has the additional logic for Saturated Detection and PreAmp Reset
--		The AD9240 outputs a "straight" binary code, it must be converted to "Signed" by inverting the MSB
--
--   History:       <date> - <Author>
-- 		08/23/06 - MCS - Ver 4x27
--			Added a RESET_GEN_CNT_MAX to limit the pulse width for the reset set to 200us
--		08/22/06 - MCS - Ver 4x26
--			Max/Min Adjust threshold changed to original method due to reset being stuck active with a ramp that is too small
--			seen by increased gain from VItus SDD
--		08/11/06 - MCS - Ver 4x25
--			Added the circuit to force a reset generate if the ramp is saturated,
--			ie no reset for some time..
--		08/11/06 - MCS - Ver 4x24 - Genesis 5.1 Release Candidate
--		08/03/05 - MCS - Ver 356F
--			Enabled Detector Reset Circuitry
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

---------------------------------------------------------------------------------------------------
entity AD9240 is 
	port( 
		IMR			: in		std_Logic;					-- Master Reset
		CLK20		: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
		FDisc		: in		std_logic;
		PCT_50		: in		std_logic;
		PA_Reset		: in		std_logic;
		OTR			: in		std_logic;					-- Out of Range
		ADout		: in		std_logic_vector( 13 downto 0 );	-- A/D Data Output
		fgain		: in		std_logic_Vector( 15 downto 0 );
		raw_ad		: buffer	std_Logic_Vector( 15 downto 0 );
		CLK_FAD		: buffer	std_logic;					-- Clock to A/D Converter
		Reset_Gen		: buffer	std_Logic;
		Dout			: buffer	std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
		Max			: buffer	std_logic_Vector( 13 downto 0 );
		Min			: buffer	std_logic_vector( 13 downto 0 ) );
end AD9240;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of AD9240 is

	constant Dout_LSB 			: integer := 14;		
--	constant Pos_Thr 			: integer := 14000;	-- 7500;
--	constant Neg_thr			: integer := 7000;	-- -1250;
	constant Large_Step 		: integer := 50;
	constant Small_Step			: integer := 1;
	constant Min_Pos			: integer := ( 16384 * 55 ) / 100;
	constant Max_Neg			: integer := ( 16384 * 45 ) / 100;

	constant PCT_98			: integer := ( 16384 * 90 ) /100;
	constant PCT_90			: integer := ( 16384 * 90 ) /100;
	constant PCT_10			: integer := ( 16384 * 10 ) /100;
	constant PCT_5		     	: integer := ( 16384 *  5 ) /100;
	constant PCT_2				: integer := ( 16384 *  2 ) /100;

	constant Sat_Cnt_Max		: integer :=  200000000;		--  1 Seconds
	constant Force_Reset_PW		: integer := 500000;
	constant REset_Gen_Cnt_MAx 	: integer := 3999;	-- 200/.05			-- 4x27
	
	
	signal clamp_hi		: std_logic;
	signal clamp_lo		: std_logic;
	signal clk_fad_n		: std_logic;

	signal Dout_del1 		: std_logic_Vector( 13 downto 0 );
	signal Dout_del2 		: std_logic_Vector( 13 downto 0 );
	signal dout_sum		: std_logic_vector( 15 downto 0 );

	signal fgain_reg		: std_Logic_vector( 15 downto 0 );
	signal fgain_Mult		: std_logic_Vector( 15 downto 0 );
	signal Force_Reset_Cnt 	: integer range 0 to Force_Reset_PW;
	signal Force_Reset_Cen 	: std_logic;
	signal Force_Reset_Tc	: std_logic;
	
	signal MM_UPdate		: std_logic;
	signal mm_update_Del	: std_logic;


	signal Max_diff 		: std_logic_Vector( 13 downto 0 );
	signal Max_Diff_abs		: std_logic_Vector( 13 downto 0 );
	signal Max_Adj_2		: std_logic;
	signal Max_Adj_10		: std_logic;
	signal Max_Pos_Adj		: std_Logic;
--	signal Max_Neg_Cmp 		: std_logic;
--	signal Min_Neg_Cmp 		: Std_Logic;

	signal Min_diff 		: std_logic_Vector( 13 downto 0 );
	signal Min_Diff_abs		: std_logic_Vector( 13 downto 0 );
	signal Min_Adj_2		: std_logic;
	signal Min_Adj_10		: std_logic;
	signal Min_Pos_Adj		: std_Logic;
--	signal Min_Pos_Cmp		: std_logic;
--	signal Max_Pos_Cmp		: std_logic;

	signal product			: std_Logic_vector( 31 downto 0 );
	signal Pos_Thr			: std_logic_Vector( 13 downto 0 );
	signal Neg_Thr			: std_logic_Vector( 13 downto 0 );

	signal Reset_Pos		: std_logic;
	signal Reset_Neg		: std_logic;
	signal Reset_Pos_Vec	: std_logic_Vector( 2 downto 0 );
	signal Reset_Neg_Vec	: std_logic_Vector( 2 downto 0 );
	
	signal REset_Gen_Cnt 	: integer range 0 to Reset_Gen_Cnt_Max;			-- 4x27
	signal Reset_Gen_Tc 	: std_Logic;								-- 4x27


	signal Sat_Cnt			: integer range 0 to Sat_Cnt_Max;
	signal Sat_Cnt_Tc		: std_Logic;


	------------------------------------------------------------------------------------
	component ad_maxmin is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			PA_Reset		: in		std_logic;
			Din			: in		std_logic_vector( 13 downto 0 );
			MM_Update		: buffer  std_Logic;
			Max			: buffer	std_logic_Vector( 13 downto 0 );
			Min			: buffer	std_logic_vector( 13 downto 0 ) );
	end component ad_maxmin;
	------------------------------------------------------------------------------------

begin
	Sat_Cnt_Tc 	<= '1' when ( Sat_Cnt = Sat_Cnt_Max ) else '0';
	Force_Reset_Tc <= '1' when ( Force_Reset_Cnt = Force_Reset_PW ) else '0';

	------------------------------------------------------------------------------------	
	-- Actual Inteface to the A/D converter --------------------------------------------
	------------------------------------------------------------------------------------	
	-- Sense Out-of-Range from A/D Converter -------------------------------------------			
	clamp_hi		<= '1' when ( OTR = '1' ) and ( ADout(13) = '1' ) else '0';
	clamp_lo 		<= '1' when ( OTR = '1' ) and ( ADout(13) = '0' ) else '0';

	dout_del_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			sset				=> clamp_hi,
			sclr				=> clamp_lo,
			enable			=> clk_fad_n,
			data				=> adout, 
			q				=> dout_del1 );
	-- Sense Out-of-Range from A/D Converter -------------------------------------------
	------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	AD_CLK_Proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			CLK_FAD		<= '0';
			CLK_FAD_n		<= '1';
		elsif(( CLK20'Event ) and ( CLk20 = '1' )) then
			CLK_FAD		<= not( CLK_FAD );
			clk_FAD_n		<= CLK_FAD;
		end if;
	end process;
	-------------------------------------------------------------------------------

	-- Actual Inteface to the A/D converter --------------------------------------------
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	-- From the Raw A/D, determine Max Min values
	raw_ad	<= "00" & dout_del1;			-- Output A/D value, before any alterations		

	U_ADMM: ad_maxmin
		port map(
			imr				=> imr,
			clk				=> clk20,
			PA_Reset			=> PA_Reset,
			Din				=> Dout_Del1,
			MM_Update			=> MM_Update,
			Max				=> Max,
			Min				=> Min );
	-- From the Raw A/D, determine Max Min values
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	-- Compare ramp to generate reset -------------------------------------------------
	Reset_Pos_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> adout,
			datab			=> Pos_thr,
			agb				=> Reset_Pos );

	Reset_Neg_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> adout,
			datab			=> Neg_Thr,
			alb				=> Reset_Neg );
			
--	Max_Pos_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 14,
--			LPM_REPRESENTATION	=> "UNSIGNED" )
--		port map(
--			dataa			=> Pos_Thr,
--			datab			=> conv_Std_logic_Vector( 16300, 14 ),
--			alb				=> Max_Pos_Cmp );
			
--	Min_Pos_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 14,
--			LPM_REPRESENTATION	=> "UNSIGNED" )
--		port map(
--			dataa			=> Pos_Thr,
--			datab			=> conv_Std_logic_Vector( Min_Pos, 14 ),
--			ageb				=> Min_Pos_Cmp );
			
--	Max_Neg_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 14,
--			LPM_REPRESENTATION	=> "UNSIGNED" )
--		port map(
--			dataa			=> Neg_Thr,
--			datab			=> Conv_Std_Logic_Vector( Max_Neg, 14 ),
--			aleb				=> Max_Neg_Cmp );

--	Min_NeG_Compare : lpm_compare
--		generic map(
--			LPM_WIDTH			=> 14,
--			LPM_REPRESENTATION	=> "UNSIGNED" )
--		port map(
--			dataa			=> Neg_Thr,
--			datab			=> Conv_Std_Logic_Vector( 83, 14 ),
--			agb				=> Min_Neg_Cmp );
			
	-- Compare ramp to generate reset -------------------------------------------------
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Pipeline Dout to create the sum
	dout_del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> dout_del1,
			q				=> dout_del2 );
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Add two Pipelines of Dout for the sum
	dout_sum_ff : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			Cin				=> '0',
			dataa			=> "00" & dout_del1,
			datab			=> "00" & dout_del2,
			result			=> dout_sum );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- increase fgain by TBD% )

	fgain_mult <= fgain(15) & fgain( 15 downto 1 ) when ( PCT_50 = '1' ) else	-- Divide by 2 and add 50% increase
			    fgain(15) & fgain(15) & fgain( 15 downto 2 );				-- Divide by 4 and add 25% increase
	
	fgain_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			Cin				=> '0',	
			dataa			=> fgain,
			datab			=> fgain_Mult,
			result 			=> fgain_reg );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Mult Fine Gain and Sum ---------------------------------------------
	FG_MULT : lpm_mult
		generic map(
			LPM_WIDTHA		=> 16,
			LPM_WIDTHB		=> 16,
			LPM_WIDTHS		=> 32,
			LPM_WIDTHP		=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_HINT			=> "MAXIMIZE_SPEED=1" )
		port map(
			dataa			=> dout_sum,
			datab			=> fgain_Reg,
			sum				=> conv_std_Logic_Vector( 0, 32 ),
			result			=> product );
	-------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	-- Final Output
	Dout_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> not( product( Dout_LSB+15)) & product( Dout_LSB+14 downto Dout_LSB ),
			q				=> dout );
	-------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	-- Calculate: abs( Max - PCT_90 )
	Max_Pos_Adj_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Max,
			datab			=> conv_Std_logic_Vector( PCT_90, 14 ),
			alb				=> Max_Pos_Adj );

	Max_diff_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 14,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> Max,
			datab			=> conv_Std_logic_Vector( PCT_90, 14 ),
			result			=> Max_diff );

			
	Max_Diff_Absolute : lpm_abs
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			data				=> Max_diff,
			result			=> Max_Diff_Abs );
	
	Max_Adj_2_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Max_Diff_abs,
			datab			=> Conv_Std_Logic_Vector( PCT_2, 14 ),
			agb				=> Max_Adj_2 );

	Max_Adj_10_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Max_Diff_abs,
			datab			=> Conv_Std_Logic_Vector( PCT_5, 14 ),
			agb				=> Max_Adj_10 );

	-- Calculate: abs( Max - PCT_90 )
	-------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	-- Calculate: abs( Min - PCT_10 )
	Min_Pos_Adj_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Min,
			datab			=> conv_Std_logic_Vector( PCT_10, 14 ),
			alb				=> Min_Pos_Adj );

	Min_diff_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 14,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION 	=> "SIGNED" )
		port map(
			Cin				=> '1',
			dataa			=> Min,
			datab			=> conv_Std_logic_Vector( PCT_10, 14 ),
			result			=> Min_diff );
		
	Min_Diff_Absolute : lpm_abs
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			data				=> Min_diff,
			result			=> Min_Diff_Abs );
	
	Min_Adj_2_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Min_Diff_abs,
			datab			=> Conv_Std_Logic_Vector( PCT_2, 14 ),
			agb				=> Min_Adj_2 );

	Min_Adj_10_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Min_Diff_abs,
			datab			=> Conv_Std_Logic_Vector( PCT_5, 14 ),
			agb				=> Min_Adj_10 );

	-- Calculate: abs( MIn - PCT_10 )
	-------------------------------------------------------------------------------
		
	Reset_Gen_Tc <= '1' when ( Reset_Gen_Cnt = Reset_Gen_Cnt_Max ) else '0';	-- Ver 4x27
			
	-------------------------------------------------------------------------------
	CLK20_Proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			Sat_Cnt			<= 0;
			Force_Reset_Cen	<= '0';
			Force_Reset_Cnt	<= 0;
			Reset_Gen			<= '0';	
			Reset_Gen_Cnt		<= 0;			-- Ver 4x27
			MM_Update_Del		<= '0';
			Reset_Pos_Vec		<= "000";
			Reset_Neg_Vec		<= "000";
			Pos_Thr			<= Conv_Std_logic_Vector( 14000, 14 );
			Neg_Thr			<= Conv_Std_logic_Vector(  7000, 14 );
			
		elsif(( CLK20'Event ) and ( CLk20 = '1' )) then

			-- Ver 4x25 -------------------------------------------------------------------------------------------
			-- If Detector misses the reset, use this mechanism to issue another reset to get the detector started
			if(( Reset_Gen = '1' ) or ( Fdisc = '1' ))
				then Sat_Cnt <= 0;
				else Sat_Cnt <= Sat_Cnt + 1;
			end if;

			if( Sat_Cnt_Tc = '1' )
				then Force_Reset_Cen <= '1';
			elsif( ForcE_reset_tc = '1' )
				then Force_Reset_Cen <= '0';
			end if;	
			
			if( Force_Reset_Cen = '0' )
				then Force_Reset_Cnt <= 0;
				else Force_Reset_Cnt <= Force_Reset_Cnt + 1;
			end if;	
			-- If Detector misses the reset, use this mechanism to issue another reset to get the detector started
			-- Ver 4x25 -------------------------------------------------------------------------------------------
		
			Reset_Pos_Vec	<= Reset_Pos_Vec( 1 downto 0 ) & Reset_Pos;
			Reset_NeG_Vec	<= Reset_Neg_Vec( 1 downto 0 ) & Reset_Neg;

			if( Force_Reset_Cen = '1' ) 
				then Reset_Gen <= '1';
			elsif( Reset_Neg_Vec = "111" )
				then Reset_Gen <= '0';
			elsif( Reset_Pos_Vec = "111" )
				then Reset_Gen <= '1';
			elsif( Reset_Gen_Tc = '1' )
				then Reset_Gen <= '0';				
			end if;
					
			------------------------------------------------------------------------------------------------------
			-- Ver 4x27
			-- Create Timer to limit the maximum pulse width of the Generated Reset Pulse to prevent "Lock-up"
			if( Reset_Gen = '0' )
				then Reset_Gen_Cnt <= 0;
				else REset_Gen_Cnt <= Reset_Gen_Cnt + 1;
			end if;	
			-- Ver 4x27
			------------------------------------------------------------------------------------------------------
		
			MM_Update_Del	<= MM_Update;

			if( MM_Update_Del = '1' ) then
				if( Max_Adj_10 = '1' ) then
					if( Max_Pos_Adj = '1' )
						then Pos_Thr <= Pos_Thr + Large_Step;
						else Pos_Thr <= Pos_Thr - Large_Step;
					end if;
				elsif( Max_Adj_2 = '1' ) then
					if( Max_Pos_Adj = '1' )
						then Pos_Thr <= Pos_Thr + Small_Step;
						else Pos_Thr <= Pos_Thr - Small_Step;
					end if;				
				end if;
			
				if( Min_Adj_10 = '1' ) then				
					if( Min_Pos_Adj = '1' )
						then Neg_Thr <= Neg_Thr + Large_Step;
						else Neg_Thr <= Neg_Thr - Large_Step;
					end if;
				elsif( Min_Adj_2 = '1' ) then				
					if( Min_Pos_Adj = '1' )
						then Neg_Thr <= Neg_Thr + Small_Step;
						else Neg_Thr <= Neg_Thr - Small_Step;
					end if;
				end if;
			end if;			

--			Ver 4x26, this was removed ( from testing with Vitus SDD ) when Ramp is too small, has difficulty resetting
--			if( MM_Update_Del = '1' ) then
--				if( Max_Adj_10 = '1' ) then
--					if(( Max_Pos_Adj = '1' ) and ( Max_Pos_Cmp = '1' ))
--						then Pos_Thr <= Pos_Thr + Large_Step;
--					elsif(( Max_Pos_Adj = '0' ) and ( Min_Pos_Cmp = '1' ))
--						then Pos_Thr <= Pos_Thr - Large_Step;
--					end if;
--				elsif( Max_Adj_2 = '1' ) then		
--					if(( Max_Pos_Adj = '1' ) and ( Max_Pos_Cmp = '1' )) 
--						then Pos_Thr <= Pos_Thr + Small_Step;
--					elsif(( Max_Pos_Adj = '0' ) and ( Min_Pos_Cmp = '1' ))
--						then Pos_Thr <= Pos_Thr - Small_Step;
--					end if;				
--				end if;
--				if( Min_Adj_10 = '1' ) then				
--					if(( Min_Pos_Adj = '1' ) and ( Max_Neg_Cmp = '1' ))
--						then Neg_Thr <= Neg_Thr + Large_Step;
--					elsif(( Min_Pos_Adj = '0' ) and ( Min_Neg_Cmp = '1' ))
--						then Neg_Thr <= Neg_Thr - Large_Step;
--					end if;
--				elsif( Min_Adj_2 = '1' ) then				
--					if(( Min_Pos_Adj = '1' ) and ( Max_Neg_Cmp = '1' ))
--						then Neg_Thr <= Neg_Thr + Small_Step;
--					elsif(( Min_Pos_Adj = '0' ) and ( Min_Neg_Cmp = '1' ))
--						then Neg_Thr <= Neg_Thr - Small_Step;
--					end if;
--				end if;
--			end if;			
		end if;
	end process;
	-------------------------------------------------------------------------------
	----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end behavioral;			-- AD9240.VHD
---------------------------------------------------------------------------------------------------

