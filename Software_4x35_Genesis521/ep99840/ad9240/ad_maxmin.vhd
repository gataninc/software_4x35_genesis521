------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ad_maxmin
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module monitors the input data stream and determines the max and min values
--		based on an adaptive decay approach that latches the max and slowly drops it back 
--		to baseline
--
--		During a PreAmp reset, it does not update the value
--
--	History
--		06/21/05 - MCS
--			Created with for QuartusII Version 5.0 SP 0.21
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity ad_maxmin is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		PA_Reset		: in		std_logic;
		Din			: in		std_logic_vector( 13 downto 0 );
		MM_Update		: buffer  std_Logic;
		Max			: buffer	std_logic_Vector( 13 downto 0 );
		Min			: buffer	std_logic_vector( 13 downto 0 ) );
end ad_maxmin;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of ad_maxmin is
	constant DECAY				: std_logic_Vector( 31 downto 0 ) := x"00000010";
	constant Reset_End_Cnt_Max 	: integer := 15;

	signal Max_Cmp			: std_Logic;
	signal Min_Cmp			: std_Logic;

	signal Max_val			: std_logic_Vector( 13 downto 0 );
	signal Min_Val			: std_logic_Vector( 13 downto 0 );

	
	signal Min_update		: std_logic;
	signal Max_update		: std_logic;

	signal Reset_End_Cnt	: integer range 0 to Reset_End_Cnt_max;
	signal Reset_End_Tc		: std_logic;
	signal Reset_End		: std_logic;
	
	signal PA_Reset_Vec		: std_Logic_vector( 1 downto 0 );

begin
	Reset_End_Tc	<= '1' when ( Reset_End_CNt = Reset_End_Cnt_Max ) else '0';
	

	MM_Update  	<= '1' when ( PA_Reset_Vec  = "01" ) else 
			        '0';

	
	Min_update 	<= '1' when ( Reset_End_Tc  = '1' ) else
			 	   '1' when ( Min_Cmp 	   = '1' ) else
			    	   '0';

	Max_update 	<= '1' when ( Reset_End_Tc  = '1' ) else
			    	   '1' when ( Max_Cmp 	   = '1' ) else
			    	   '0';
	
		
	------------------------------------------------------------------
	-- Max Compare
	MAX_CMP_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> din,
			datab			=> Max_Val,
			agb				=> Max_Cmp );
	-- Max Compare
	------------------------------------------------------------------

	------------------------------------------------------------------
	MAX_val_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> Reset_End_Tc,
			enable			=> Max_update,
			data				=> din,
			q				=> Max_Val );

	-- Final Output Registers ----------------------------------------
	Max_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )	
		port map(
			aclr				=> imr,
			clock			=> clk,
			enable			=> MM_Update,
			data				=> max_val,
			q				=> Max );
	------------------------------------------------------------------

	------------------------------------------------------------------
	-- Min Compare
	MIN_CMP_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 14,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> din,
			datab			=> Min_Val,
			alb				=> Min_Cmp );
	-- Min Compare
	------------------------------------------------------------------

	Min_val_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sset				=> Reset_End_Tc,
			enable			=> Min_update,
			data				=> din,
			q				=> Min_Val );			
	------------------------------------------------------------------
	
	------------------------------------------------------------------
	-- Final Output Registers ----------------------------------------
	Min_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )	
		port map(	
			aclr				=> imr,
			clock			=> clk,
			enable			=> MM_Update,
			data				=> Min_val,
			q				=> Min );

	-- Final Output Registers ----------------------------------------
	------------------------------------------------------------------

	------------------------------------------------------------------
	clock_proc : process( clk, imr )
	begin
		if( imr = '1' ) then
			PA_Reset_Vec <= "00";
			Reset_End		<= '0';
			Reset_End_Cnt	<= 0;
		elsif(( CLK'event ) and ( clk = '1' )) then
			PA_Reset_Vec <= PA_Reset_Vec(0) & PA_Reset;	
			
			if( PA_Reset_Vec = "10" ) 
				then Reset_End <= '1';
			elsif( Reset_End_Tc = '1' )
				then Reset_End <= '0';
			end if;
			
			if( Reset_End = '0' )
				then Reset_End_Cnt <= 0;
				else Reset_End_Cnt <= Reset_End_Cnt + 1;
			end if;		
		end if;
	end process;
	------------------------------------------------------------------
------------------------------------------------------------------------------------
end behavioral;		-- ad_maxmin
------------------------------------------------------------------------------------
