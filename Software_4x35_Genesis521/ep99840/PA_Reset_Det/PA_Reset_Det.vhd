---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	PA_Reset_Det.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------

library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity PA_Reset_Det is 
	port( 
		IMR			: in		std_Logic;					-- Master Reset
		CLK			: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
		Fir_Mr		: in		std_logic;
		We_Clr_AD		: in		std_logic;					-- Filter Reset ( from DSP )
		Dout			: in		std_logic_vector( 15 downto 0 );	-- A/D Data Output
		ms100tc		: in		std_logic;
		CPS			: in		std_logic_Vector( 20 downto 0 );
		hc_rate		: in		std_logic_Vector( 20 downto 0 );
		hc_threshold	: in		std_logic_Vector( 15 downto 0 );
		AD_CLR		: buffer	std_logic;
		det_sat		: buffer	std_logic;
		Hi_Cnt		: buffer	std_logic;
		Reset		: buffer	std_logic;					-- PreAmp Reset
		NSlope_Cmp_out	: buffer	std_logic;
		pslope_cmp_out	: buffer 	std_logic;
		Reset_time	: buffer	std_logic_vector( 31 downto 0 );
		Reset_Width	: buffer	std_logic_Vector( 15 downto 0 ) );
end PA_Reset_Det;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of PA_Reset_Det is
	constant NSlope_thr			: std_logic_vector( 15 downto 0 ) := x"0800";
	constant PSlope_thr			: std_Logic_Vector( 15 downto 0 ) := x"0100";
	constant Dout_Max_Threshold 	: integer := 32767;
	constant Reset_End_Cnt_MAx 	: integer := 15;
	constant us1_cnt_max 		: integer := 19;		-- 1 us @ 20Mhz (50 ns)
	constant Dly_Width			: integer := 4;
	constant Data_Width			: integer := 16;
	constant Acc_width			: integer := 21;
	constant fir_mr_str_cnt_max 	: integer := 31;
	constant Reset_Time_Max_Int	: integer :=  100000000;		-- was 50Meg
	constant Ad_Clr_Len			: integer :=  8192;	-- 2*(16+16+32+64+128+256+512+1024+2048);


	
	signal Ad_Clr_Cnt 			: integer range 0 to Ad_Clr_Len;
	signal Ad_Clr_tc			: std_logic;
	signal adr_offset			: std_logic_Vector( dly_width-1 downto 0 );

	signal dataa				: std_logic_vector( Acc_Width-1 downto 0 );
	signal datab				: std_logic_vector( Acc_Width-1 downto 0 );
	signal datac				: std_logic_vector( Acc_Width-1 downto 0 );
	signal Dout_max_Cmp 		: std_logic;
	signal Dout_Del1			: std_logic_Vector( Data_Width-1 downto 0 );
	signal Dout_Del2			: std_logic_vector( Data_Width-1 downto 0 );
	signal Dout_Diff			: std_logic_vector( Acc_Width-1 downto 0 );


	signal fir_mr_str			: std_logic;
	signal fir_mr_str_tc		: std_logic;
	signal fir_mr_str_cnt		: integer range 0 to fir_mr_str_cnt_max;


	signal hc_cps_tc 			: std_logic;						
	signal hc_cnt_clr			: std_logic;
	signal hc_cnt_cen			: std_logic;
	signal hc_count			: std_logic_Vector( 15 downto 0 );
	signal hc_count_tc			: std_logic;

	signal Nslope_Thr_Vec		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal NSlope_Cmp			: std_logic;
	signal No_Reset			: std_logic;


	signal Pslope_Thr_Vec		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal pslope_cmp			: std_logic;
	
	signal radr				: std_logic_Vector(  dly_width-1 downto 0 );
	signal Reset_time_max 		: std_logic_vector( 31 downto 0 );
	signal reset_Del			: std_logic;
	signal Reset_Cnt			: std_logic_Vector( 15 downto 0 );
	signal Reset_end_tc			: std_logic;
	signal Reset_End			: std_logic;
	signal Reset_End_Cnt		: integer range 0 to Reset_End_Cnt_Max;
	signal Reset_LEd			: std_logic;	
	signal Reset_Cnt_Vec		: std_logic_Vector( 15 downto 0 );
	signal Reset_Time_Cnt		: std_logic_Vector( 31 downto 0 );	
	signal reset_Time_Cen 		: std_logic;

	signal suma				: std_logic_vector( Acc_Width-1 downto 0 );
	signal sumb 				: std_logic_vector( Acc_Width-1 downto 0 );
	signal sumc 				: std_logic_vector( Acc_Width-1 downto 0 );

	signal us1_cnt 			: integer range 0 to us1_cnt_max;
	signal us1_tc				: std_logic;

	signal wadr				: std_logic_vector(  dly_width-1 downto 0 );

begin
	us1_tc 		<= '1' when ( us1_cnt = us1_cnt_max ) else '0';
	
	-- Delay Address Generator ------------------------------------------------------------------
	wadr_cntr : lpm_counter
		generic map(
			LPM_WIDTH				=> dly_width,
			LPM_DIRECTION			=> "UP",
			LPM_TYPE				=> "LPM_COUNTER" )
		port map(	
			aclr					=> imr,
			clock				=> clk,
			q					=> wadr );
			
	adr_offset <= conv_std_logic_Vector( 6, dly_width );

	radr_Add_sub : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> dly_width,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			cin					=> '1',					-- Carry In
			dataa				=> wadr,
			datab				=> adr_offset,
			result				=> radr );
	-- Delay Address Generator ------------------------------------------------------------------


	-- Delay Memories ---------------------------------------------------------------------------
	Dly_RAM1: lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Data_Width,	-- Data Width
			LPM_WIDTHAD			=> dly_width,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT.MIF",
			LPM_TYPE				=> "LPM_RAM_DP" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress				=> radr,
			wraddress 			=> wadr,
			data					=> Dout,
			q					=> Dout_Del1 );

	Dly_RAM2: lpm_ram_dp
		generic map(
			LPM_WIDTH				=> Data_Width,	-- Data Width
			LPM_WIDTHAD			=> dly_width,
			LPM_INDATA			=> "REGISTERED",
			LPM_OUTDATA			=> "REGISTERED",
			LPM_RDADDRESS_CONTROL	=> "REGISTERED",
			LPM_WRADDRESS_CONTROL	=> "REGISTERED",
			LPM_FILE				=> "INIT.MIF",
			LPM_TYPE				=> "LPM_RAM_DP" )
		PORT map(
			Wren					=> '1',
			wrclock				=> clk,
			rdclock 				=> clk,
			rdaddress				=> radr,
			wraddress 			=> wadr,
			data					=> Dout_Del1,
			q					=> Dout_Del2 );
	-- Delay Memories ---------------------------------------------------------------------------

	-- Filter Generators ------------------------------------------------------------------------
	extend_gen1 : for i in data_width to acc_width - 1 generate
		dataa(i) <= dout(data_width-1);
		datac(i) <= dout_del2(data_width-1);
	end generate;
	dataa( data_width-1 downto 0 ) <= dout;
	datac( data_width-1 downto 0 ) <= dout_del2;
	
	extend_gen2 : for i in data_width+1 to acc_width - 1 generate
		datab(i) <= dout_del1( data_width-1 );
	end generate;
	datab( data_width downto 0 )	<= Dout_del1 & '0';					-- 2x

	SumA_Add_sub : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> acc_width,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			cin					=> '1',					-- Carry In
			dataa				=> datab,
			datab				=> dataa,
			result				=> suma );

	Sumb_Add_sub : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> acc_width,
			LPM_DIRECTION			=> "SUB",
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			cin					=> '1',					-- Carry In
			dataa				=> suma,
			datab				=> datac,
			result				=> sumb );


	Sumc_Add_sub : lpm_add_Sub
		generic map(
			LPM_WIDTH				=> acc_width,
			LPM_DIRECTION			=> "ADD",
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_ADD_SUB" )
		port map(
			cin					=> '0',					-- Add
			dataa				=> Dout_Diff,
			datab				=> sumb,
			result				=> sumc );
			
	Dout_Diff_FF : lpm_ff
		generic map(
			lpm_width				=> acc_width,
			lpm_type				=> "lpm_ff" )
		port map(	
			aclr					=> imr,
			clock				=> clk,
			sclr					=> FIR_MR_STR,
			data					=> sumc,
			q					=> Dout_Diff );
	-- Filter Generators ------------------------------------------------------------------------
		

	-- Negative Slope Detection -----------------------------------------------------------------
	NSlope_Thr_Vec_Gen : for i in 16 to Acc_Width-1 generate
		NSlope_Thr_Vec(i) <= NSlope_Thr(15);
	end generate;
	NSlope_Thr_Vec( 15 downto 0 ) <= Nslope_thr;
	
	NSlope_Compare : lpm_compare
		generic map(
			LPM_WIDTH				=> acc_width,
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_COMPARE" )
		port map(
			dataa				=> Dout_Diff,
			datab				=> NSlope_Thr_Vec,
			agb					=> NSlope_Cmp );
	-- Negative Slope Detection -----------------------------------------------------------------

	-- Positive Slope Detection -----------------------------------------------------------------
	PSlope_Thr_Vec_Gen : for i in 16 to Acc_Width-1 generate
		PSlope_Thr_Vec(i) <= PSlope_Thr(15);
	end generate;
	PSlope_Thr_Vec( 15 downto 0 ) <= Pslope_thr;
	
	PSlope_Compare : lpm_compare
		generic map(
			LPM_WIDTH				=> acc_width,
			LPM_REPRESENTATION 		=> "SIGNED",
			LPM_TYPE				=> "LPM_COMPARE" )
		port map(
			dataa				=> PSlope_Thr_Vec,
			datab				=> Dout_Diff,
			agb					=> PSlope_Cmp );
	-- Positive Slope Detection -----------------------------------------------------------------
			
 	----------------------------------------------------------------------------------------------
	Reset_LED	<= '1' when (( Reset = '1' ) and ( Reset_Del = '0' )) else '0';

 	----------------------------------------------------------------------------------------------
	-- Reset_Width Counter
	Reset_Cntr : Lpm_counter
		generic map(
			LPM_WIDTH				=> 16,
			LPM_DIRECTION			=> "UP",
			LPM_TYPE				=> "LPM_COUNTER" )
		port map(
			aclr					=> imr,
			clock				=> clk,
			sclr					=> Reset_LED,
			cnt_en				=> Reset,
			q					=> Reset_Cnt );

	Reset_Cnt_Vec <= '0' & Reset_Cnt( 15 downto 1 );		-- at 20Mhz but displayed at 10Mhz
	
	reset_width_ff : lpm_ff
		generic map(
			lpm_width				=> 16,
			lpm_type				=> "lpm_ff" )
		port map(
			aclr					=> imr,
			clock				=> clk,
			enable				=> Reset_End_tc,
			data					=> Reset_Cnt_Vec,
			q					=> Reset_Width );
	
	Reset_End_tc <= '1' when ( Reset_End_Cnt = Reset_End_Cnt_Max ) else '0';
	-- Reset_Width Counter
 	----------------------------------------------------------------------------------------------

 	----------------------------------------------------------------------------------------------
	-- Reset Time Counter 
	Reset_Time_Max <= conv_std_logic_vector( Reset_Time_Max_Int, 32 );


	reset_Time_Cen	<= '1' when (( Reset = '0' ) and ( us1_tc = '1' )) else '0';

	Reset_Time_Cnt_Cntr : lpm_Counter
		generic map(
			LPM_WIDTH			=> 32,
			LPM_TYPE			=> "LPM_COUNTER" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> Reset_LED,
			cnt_en			=> Reset_Time_Cen,
			q				=> reset_time_Cnt );

	Reset_Time_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 32,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			enable			=> ReseT_LED,
			data				=> reset_Time_Cnt,
			q				=> reset_time );			

	No_Reset_Cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> Reset_Time_Cnt,
			datab			=> Reset_Time_Max,
			agb				=> No_Reset );

	-- Check to see of Ramp is above 1/2 scale
	DOUT_FS_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Dout,
			datab			=> conv_Std_logic_vector( Dout_MAx_Threshold, 16 ),
			agb				=> Dout_Max_Cmp );

	-- Reset Time Counter 
 	----------------------------------------------------------------------------------------------

	fir_mr_str_tc	<= '1' when ( fir_mr_str_cnt = fir_mr_str_cnt_max ) else '0';
	
	     --------------------------------------------------------------------------
	--High Count Rate Detection 
	cps_cmp : lpm_compare
		generic map(
			LPM_WIDTH			=> 21,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> cps,
			datab			=> hc_rate,
			agb				=> hc_cps_tc );

	hc_cnt_clr	<= '1' when (( ms100tc = '1' ) and ( hc_cps_tc = '0' )) else '0';
	hc_cnt_cen	<= '1' when (( ms100tc = '1' ) and ( hc_cps_tc = '1' )) else '0';
	
	hc_cnt_cntr : lpm_counter
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> IMR,
			clock			=> CLK,
			sclr				=> hc_cnt_clr,
			cnt_en			=> hc_cnt_cen,
			q				=> hc_count );
			
	hc_count_cmp : lpm_compare
			generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> hc_count,
			datab			=> hc_threshold,
			agb				=> hc_count_Tc );
			
			


 	----------------------------------------------------------------------------------------------
	Reset_Proc : process( IMR, CLK )
	begin
		if( IMR = '1' ) then
			Reset_Del		<= '0';
			Reset		<= '0';
			Reset_End		<= '0';
			Reset_End_Cnt	<= 0;

			us1_cnt		<= 0;
			Det_Sat 		<= '0';
			Hi_Cnt		<= '0';
		elsif(( CLK'Event ) and ( CLK = '1' )) then
			Reset_Del		<= Reset;
			NSlope_Cmp_out	<= nSlope_Cmp;
			pslope_cmp_out	<= PSlope_Cmp;


			-- Reset Signal --------------------------------------------------
			if( NSlope_Cmp = '1' )
				then Reset <= '1';
			elsif( Pslope_Cmp = '1' )
				then Reset <= '0';
			end if;
			-- Reset Signal --------------------------------------------------

			-- Reset End Stretching ------------------------------------------			
			if(( Reset = '0' ) and ( Reset_Del = '1' ))
				then Reset_End <= '1';
			elsif( Reset_End_Tc = '1' )
				then Reset_End <= '0';
			end if;

			if( Reset_End = '0' )
				then Reset_End_Cnt <= 0;
				else Reset_End_Cnt <= Reset_End_Cnt + 1;
			end if;
			-- Reset End Stretching ------------------------------------------

	
			-- 1us Time Counter for Pulse Time Measurement -------------------
			if( us1_tc = '1' )
				then us1_cnt <= 0;
				else us1_cnt <= us1_cnt + 1;
			end if;			
			-- 1us Time Counter for Pulse Time Measurement -------------------
			

			-- Filter reset Stretching for Input Filter ----------------------
			if( fir_mr = '1' )
				then fir_mr_str <= '1';
			elsif( fir_mr_str_Tc = '1' )
				then fir_mr_str <= '0';
			end if;
			
			if( fir_mr_str = '0' )
				then fir_mr_str_cnt <= 0;
				else fir_mr_str_cnt <= fir_mr_str_cnt + 1;
			end if;
			-- Filter reset Stretching for Input Filter ----------------------
			
			-- Detector Saturation/HIgh Count Rate Detection ---------------------------------------
			if(( ms100tc = '1' ) and ( No_Reset = '1' ) and ( Dout_Max_Cmp = '1'  ))		-- Ver 4x15
				then Det_Sat <= '1';
			elsif(( ms100tc = '1' ) and ( No_Reset = '0' ))
				then Det_Sat <= '0';
			end if;
			
               ----------------------------------------------------------------
			-- RTEM High Count Rate Detection
			if(( ms100tc = '1' ) and ( hc_cps_tc  = '1' ) and ( hc_count_Tc = '1' ))
				then Hi_Cnt <= '1';
				else Hi_Cnt <= '0';
			end if;

			
		end if;
	end process;
 	----------------------------------------------------------------------------------------------


	Ad_Clr_tc 	<= '1' when ( Ad_Clr_Cnt = Ad_Clr_Len ) else '0';
	
     --------------------------------------------------------------------------
	--
	--             +-+
	-- We_Clr_AD   | |
	--        -----+ +---------------------------------------------------------
	--
	--
	--               +------------------------------------------------+
	-- AD_CLR        |                                                |  Clamp Input
	--        -------+                        				      +--------
     --------------------------------------------------------------------------

	AD_Clr_Proc : procesS( imr, clk )
	begin
		if( imr = '1' ) then
			Ad_Clr_Cnt		<= 0;
			AD_CLR			<= '0';
		elsif(( clk'event ) and ( clk = '1' )) then
			-- A/D Clear generation with will stay active until FED of PA_RESET and AD_MR=0
			if( We_Clr_AD = '1' ) 
				then AD_CLR	<= '1';
			elsif( Ad_Clr_tc = '1' )
				then AD_CLR	<= '0';
			end if;
			

			-- Counter for duration of reset pulse -------------------------------							
			if( AD_CLR = '0' )
				then Ad_Clr_Cnt <= 0;
				else Ad_Clr_Cnt <= Ad_Clr_Cnt + 1;
			end if;				
			
		end if;
	end process;
     --------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- PA_Reset_Det.VHD
---------------------------------------------------------------------------------------------------
