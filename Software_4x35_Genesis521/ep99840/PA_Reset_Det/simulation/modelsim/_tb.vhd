---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;
	

entity tb is
end tb;

architecture test_bench of tb is

	signal IMR 		: std_logic := '1';
	signal CLK		: std_logic := '0';
	signal clk10		: std_logic := '0';
	signal Dout		: std_logic_Vector( 14 downto 0 );
	signal Reset		: std_logic;
	signal Reset_Width	: std_logic_Vector( 15 downto 0 );
	signal Ramp_Min 	: std_logic_vector( 14 downto 0 );
	signal Ramp_Max 	: std_logic_Vector( 14 downto 0 );

	signal Reset_LED	: std_logic;
	signal Reset_TED	: std_logic;
	signal Reset_End	: std_logic;
	signal Min_Cmp		: std_logic;
	signal Max_Cmp		: std_logic;

	signal nslope		: std_logic;
	signal Pslope		: std_logic;

	signal NSlope_Thr	: std_logic_Vector( 15 downto 0 );
	signal Reset_time	: std_logic_vector( 31 downto 0 );
	signal Dout_Diff	: std_logic_vector( 15 downto 0 );
	signal dout_del	: std_logic_vector( 14 downto 0 );
	signal dout_sum	: std_logic_vector( 14 downto 0 );

	signal Fir_Mr		: std_logic;
	signal ME_FET		: std_logic;
	signal INHIBIT		: std_logic;					-- Inhibit from DU ( active High )
	signal PSlope_Thr	: std_logic_Vector( 15 downto 0 );		
	signal ms100tc		: std_logic;
	signal CPS		: std_logic_Vector( 20 downto 0 );
	signal hc_rate		: std_logic_Vector( 20 downto 0 );
	signal hc_threshold	: std_logic_Vector( 15 downto 0 );
	signal det_sat		: std_logic;
	signal Hi_Cnt		: std_logic;

	---------------------------------------------------------------------------------------------------
	component FIR_Reset is 
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK			: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			Fir_Mr		: in		std_logic;
			ME_FET		: in		std_logic;
			INHIBIT		: in		std_logic;					-- Inhibit from DU ( active High )
			NSlope_Thr	: in		std_logic_Vector( 15 downto 0 );
			PSlope_Thr	: in		std_logic_Vector( 15 downto 0 );
			Dout			: in		std_logic_vector( 14 downto 0 );	-- A/D Data Output
			ms100tc		: in		std_logic;
			CPS			: in		std_logic_Vector( 20 downto 0 );
			hc_rate		: in		std_logic_Vector( 20 downto 0 );
			hc_threshold	: in		std_logic_Vector( 15 downto 0 );
			det_sat		: out	std_logic;
			Hi_Cnt		: out	std_logic;
			Reset		: out	std_logic;					-- PreAmp Reset
			Reset_time	: out	std_logic_vector( 31 downto 0 );
			Reset_Width	: out	std_logic_Vector( 15 downto 0 );
			Ramp_Min		: out	std_logic_vector( 14 downto 0 );
			Ramp_Max		: out	std_logic_vector( 14 downto 0 ) );
	end component FIR_Reset;
	---------------------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component data_Rom is 
	     port( 
	          imr 			: in std_logic;
	          clk 			: in std_logic;
	          dout 		: out std_logic_Vector( 14 downto 0 ) );
	end component data_Rom;
	-------------------------------------------------------------------------------

begin
-------------------------------------------------------------------------------
	IMR 			<= '0' after 550 ns;
	CLK 			<= not( CLK ) after 25 ns;
	NSlope_Thr	<= x"0800";
	PSlope_Thr	<= x"0100";
	UD : data_Rom 
		port map(
			imr			=> imr,
			clk			=> clk,
			dout			=> dout );

	fir_mr		<= '0';	
	ME_FET		<= '1';
	Inhibit		<= '0';
	ms100tc		<= '0';
	cps			<= conv_std_logic_Vector( 255, 21 );
	hc_rate		<= conv_std_logic_Vector( 255, 21 );
	hc_threshold	<= x"0100";
	
	U : FIR_Reset
		port map(
			IMR			=> IMR,
			CLK			=> CLK,
			Fir_Mr		=> fir_mr,
			ME_FET		=> me_fet,
			INHIBIT		=> inhibit,
			NSlope_Thr	=> Nslope_thr,
			PSlope_Thr	=> pslope_Thr,
			Dout			=> Dout_sum( 14 downto 0 ),
			ms100tc		=> ms100tc,
			CPS			=> cps,
			hc_rate		=> hc_rate,
			hc_threshold	=> hc_threshold,
			det_sat		=> det_sat,
			Hi_Cnt		=> hi_cnt,
			Reset		=> Reset,
			Reset_time	=> Reset_Time,
			Reset_width	=> Reset_Width,
			Ramp_Min		=> Ramp_Min,
			Ramp_Max		=> Ramp_Max );

	dout_proc : process( clk )
	begin
		if( clk'event ) and ( clk = '1' ) then
			dout_del <= dout;
			dout_sum <= dout + dout_del;
		end if;
	end process;
-------------------------------------------------------------------------------
end;
-------------------------------------------------------------------------------
