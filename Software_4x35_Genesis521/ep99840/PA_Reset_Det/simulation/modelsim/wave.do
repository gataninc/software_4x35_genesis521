onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk
add wave -noupdate -format Analog-Step -height 300 -offset 8000.0 -radix decimal -scale 0.01 /tb/dout
add wave -noupdate -format Analog-Step -height 300 -offset 8000.0 -radix decimal -scale 0.01 /tb/dout_sum
add wave -noupdate -format Literal -height 15 -offset 8000.0 -radix decimal -scale 0.01 /tb/dout
add wave -noupdate -format Logic /tb/reset
add wave -noupdate -format Literal -radix decimal /tb/ramp_max
add wave -noupdate -format Literal -radix decimal /tb/ramp_min
add wave -noupdate -format Literal -radix decimal /tb/reset_width
add wave -noupdate -format Analog-Step -height 300 -offset 8000.0 -radix decimal -scale 0.01 /tb/dout_diff
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {561855 ns} {998975 ns}
WaveRestoreZoom {0 ns} {13129 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
