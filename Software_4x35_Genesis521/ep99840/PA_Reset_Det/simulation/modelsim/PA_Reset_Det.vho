-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "05/16/2006 10:44:13"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	PA_Reset_Det IS
    PORT (
	IMR : IN std_logic;
	CLK : IN std_logic;
	Fir_Mr : IN std_logic;
	We_Clr_AD : IN std_logic;
	Dout : IN std_logic_vector(15 DOWNTO 0);
	ms100tc : IN std_logic;
	CPS : IN std_logic_vector(20 DOWNTO 0);
	hc_rate : IN std_logic_vector(20 DOWNTO 0);
	hc_threshold : IN std_logic_vector(15 DOWNTO 0);
	AD_CLR : OUT std_logic;
	det_sat : OUT std_logic;
	Hi_Cnt : OUT std_logic;
	Reset : OUT std_logic;
	NSlope_Cmp_out : OUT std_logic;
	pslope_cmp_out : OUT std_logic;
	Reset_time : OUT std_logic_vector(31 DOWNTO 0);
	Reset_Width : OUT std_logic_vector(15 DOWNTO 0)
	);
END PA_Reset_Det;

ARCHITECTURE structure OF PA_Reset_Det IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_Fir_Mr : std_logic;
SIGNAL ww_We_Clr_AD : std_logic;
SIGNAL ww_Dout : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_ms100tc : std_logic;
SIGNAL ww_CPS : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_hc_rate : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_hc_threshold : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_AD_CLR : std_logic;
SIGNAL ww_det_sat : std_logic;
SIGNAL ww_Hi_Cnt : std_logic;
SIGNAL ww_Reset : std_logic;
SIGNAL ww_NSlope_Cmp_out : std_logic;
SIGNAL ww_pslope_cmp_out : std_logic;
SIGNAL ww_Reset_time : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_Reset_Width : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a15_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a15_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a15_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Ad_Clr_Cnt_a3_a : std_logic;
SIGNAL Equal_a208 : std_logic;
SIGNAL Ad_Clr_Cnt_a5_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a_a164 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a14_a_a1044 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a19_a_a1414 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a146 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a27 : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Equal_a215 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a_a165 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a13_a_a1045 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a18_a_a1415 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a147 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a28 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a_a166 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a12_a_a1046 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a17_a_a1416 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a148 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a15_a : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478 : std_logic;
SIGNAL fir_mr_str_cnt_a4_a : std_logic;
SIGNAL fir_mr_str_cnt_a3_a_a63 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a29 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a_a167 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a11_a_a1047 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a16_a_a1417 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a149 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a30 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a_a168 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a10_a_a1048 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a15_a_a1418 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a150 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a31 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a_a169 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a9_a_a1049 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a14_a_a1419 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a151 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a32 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a_a170 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a8_a_a1050 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a13_a_a1420 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a152 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a14_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a33 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a_a171 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a7_a_a1051 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a12_a_a1421 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a153 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a13_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a34 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a_a172 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a6_a_a1052 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a11_a_a1422 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a154 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a12_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a35 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a_a173 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a5_a_a1053 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a10_a_a1423 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a155 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a11_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a36 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a_a174 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a4_a_a1054 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a9_a_a1424 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a156 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a10_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a37 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a_a175 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a3_a_a1055 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a8_a_a1425 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a157 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a9_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a9_a : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a_a176 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a2_a_a1056 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a7_a_a1426 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a7_a_a158 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a8_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a8_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a_a177 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a1_a_a1057 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a6_a_a1427 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a6_a_a159 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a7_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a_a178 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a0_a_a1058 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a5_a_a1428 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a5_a_a160 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a6_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a_a179 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a4_a_a1429 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a4_a_a161 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a5_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a_a180 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a3_a_a1430 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a3_a_a162 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a4_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a_a181 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a2_a_a1431 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a2_a_a163 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a3_a : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a_a182 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a1_a_a1432 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a1_a_a164 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a2_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a_a183 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a0_a_a1433 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a504 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a1_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a_a184 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a_a185 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a_a186 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a_a187 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a_a188 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a_a189 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a_a190 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a_a191 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a_a192 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a_a193 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510 : std_logic;
SIGNAL hc_threshold_a15_a_acombout : std_logic;
SIGNAL hc_rate_a20_a_acombout : std_logic;
SIGNAL hc_threshold_a14_a_acombout : std_logic;
SIGNAL hc_rate_a19_a_acombout : std_logic;
SIGNAL hc_threshold_a13_a_acombout : std_logic;
SIGNAL CPS_a18_a_acombout : std_logic;
SIGNAL hc_threshold_a12_a_acombout : std_logic;
SIGNAL hc_rate_a17_a_acombout : std_logic;
SIGNAL Dout_a15_a_acombout : std_logic;
SIGNAL hc_threshold_a11_a_acombout : std_logic;
SIGNAL CPS_a16_a_acombout : std_logic;
SIGNAL hc_threshold_a10_a_acombout : std_logic;
SIGNAL CPS_a15_a_acombout : std_logic;
SIGNAL CPS_a14_a_acombout : std_logic;
SIGNAL Dout_a14_a_acombout : std_logic;
SIGNAL hc_rate_a13_a_acombout : std_logic;
SIGNAL Dout_a13_a_acombout : std_logic;
SIGNAL hc_threshold_a7_a_acombout : std_logic;
SIGNAL hc_rate_a12_a_acombout : std_logic;
SIGNAL Dout_a12_a_acombout : std_logic;
SIGNAL hc_threshold_a6_a_acombout : std_logic;
SIGNAL CPS_a11_a_acombout : std_logic;
SIGNAL Dout_a11_a_acombout : std_logic;
SIGNAL hc_threshold_a5_a_acombout : std_logic;
SIGNAL CPS_a10_a_acombout : std_logic;
SIGNAL Dout_a10_a_acombout : std_logic;
SIGNAL hc_threshold_a4_a_acombout : std_logic;
SIGNAL CPS_a9_a_acombout : std_logic;
SIGNAL Dout_a9_a_acombout : std_logic;
SIGNAL hc_threshold_a3_a_acombout : std_logic;
SIGNAL CPS_a8_a_acombout : std_logic;
SIGNAL Dout_a8_a_acombout : std_logic;
SIGNAL hc_threshold_a2_a_acombout : std_logic;
SIGNAL CPS_a7_a_acombout : std_logic;
SIGNAL Dout_a7_a_acombout : std_logic;
SIGNAL hc_threshold_a1_a_acombout : std_logic;
SIGNAL CPS_a6_a_acombout : std_logic;
SIGNAL Dout_a6_a_acombout : std_logic;
SIGNAL hc_threshold_a0_a_acombout : std_logic;
SIGNAL CPS_a5_a_acombout : std_logic;
SIGNAL Dout_a5_a_acombout : std_logic;
SIGNAL hc_rate_a4_a_acombout : std_logic;
SIGNAL Dout_a4_a_acombout : std_logic;
SIGNAL hc_rate_a3_a_acombout : std_logic;
SIGNAL Dout_a3_a_acombout : std_logic;
SIGNAL CPS_a2_a_acombout : std_logic;
SIGNAL Dout_a2_a_acombout : std_logic;
SIGNAL hc_rate_a1_a_acombout : std_logic;
SIGNAL Dout_a1_a_acombout : std_logic;
SIGNAL CPS_a0_a_acombout : std_logic;
SIGNAL Dout_a0_a_acombout : std_logic;
SIGNAL We_Clr_AD_acombout : std_logic;
SIGNAL CLK_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL Ad_Clr_Cnt_a0_a : std_logic;
SIGNAL Ad_Clr_Cnt_a0_a_a122 : std_logic;
SIGNAL Ad_Clr_Cnt_a1_a : std_logic;
SIGNAL Ad_Clr_Cnt_a1_a_a119 : std_logic;
SIGNAL Ad_Clr_Cnt_a2_a : std_logic;
SIGNAL Ad_Clr_Cnt_a2_a_a116 : std_logic;
SIGNAL Ad_Clr_Cnt_a3_a_a113 : std_logic;
SIGNAL Ad_Clr_Cnt_a4_a : std_logic;
SIGNAL Ad_Clr_Cnt_a4_a_a134 : std_logic;
SIGNAL Ad_Clr_Cnt_a5_a_a131 : std_logic;
SIGNAL Ad_Clr_Cnt_a6_a_a128 : std_logic;
SIGNAL Ad_Clr_Cnt_a7_a : std_logic;
SIGNAL Ad_Clr_Cnt_a7_a_a125 : std_logic;
SIGNAL Ad_Clr_Cnt_a8_a_a146 : std_logic;
SIGNAL Ad_Clr_Cnt_a9_a : std_logic;
SIGNAL Ad_Clr_Cnt_a9_a_a143 : std_logic;
SIGNAL Ad_Clr_Cnt_a10_a : std_logic;
SIGNAL Ad_Clr_Cnt_a8_a : std_logic;
SIGNAL Equal_a210 : std_logic;
SIGNAL Ad_Clr_Cnt_a6_a : std_logic;
SIGNAL Equal_a209 : std_logic;
SIGNAL Ad_Clr_Cnt_a10_a_a140 : std_logic;
SIGNAL Ad_Clr_Cnt_a11_a : std_logic;
SIGNAL Ad_Clr_Cnt_a11_a_a137 : std_logic;
SIGNAL Ad_Clr_Cnt_a12_a_a152 : std_logic;
SIGNAL Ad_Clr_Cnt_a13_a : std_logic;
SIGNAL Ad_Clr_Cnt_a12_a : std_logic;
SIGNAL Equal_a211 : std_logic;
SIGNAL Equal_a212 : std_logic;
SIGNAL AD_CLR_areg0 : std_logic;
SIGNAL DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL ms100tc_acombout : std_logic;
SIGNAL add_a529 : std_logic;
SIGNAL us1_cnt_a0_a : std_logic;
SIGNAL add_a531 : std_logic;
SIGNAL add_a513 : std_logic;
SIGNAL us1_cnt_a1_a : std_logic;
SIGNAL add_a515 : std_logic;
SIGNAL add_a521 : std_logic;
SIGNAL us1_cnt_a2_a : std_logic;
SIGNAL add_a523 : std_logic;
SIGNAL add_a525 : std_logic;
SIGNAL us1_cnt_a3_a : std_logic;
SIGNAL add_a527 : std_logic;
SIGNAL add_a517 : std_logic;
SIGNAL us1_cnt_a4_a : std_logic;
SIGNAL Equal_a214 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL Dly_RAM2_asram_asegment_a0_a_a15_a_a20 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a15_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a14_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a13_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a12_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a11_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a10_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a9_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a8_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a7_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a6_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a5_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a4_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a3_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a2_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a1_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a0_a : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a442 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a0_a : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a480 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546 : std_logic;
SIGNAL Fir_Mr_acombout : std_logic;
SIGNAL fir_mr_str_cnt_a0_a : std_logic;
SIGNAL fir_mr_str_cnt_a0_a_a51 : std_logic;
SIGNAL fir_mr_str_cnt_a1_a : std_logic;
SIGNAL fir_mr_str_cnt_a1_a_a54 : std_logic;
SIGNAL fir_mr_str_cnt_a2_a : std_logic;
SIGNAL fir_mr_str_cnt_a2_a_a60 : std_logic;
SIGNAL fir_mr_str_cnt_a3_a : std_logic;
SIGNAL fir_mr_str_a35 : std_logic;
SIGNAL fir_mr_str : std_logic;
SIGNAL Dout_Diff_FF_adffs_a0_a_a342 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a1_a_a339 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a2_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a2_a_a336 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a3_a_a333 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a4_a_a330 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a5_a_a327 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a6_a_a324 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a7_a_a321 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a8_a_a318 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a9_a_a315 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a10_a_a312 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a11_a_a309 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a12_a_a306 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a13_a_a303 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a14_a_a300 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a15_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a15_a_a297 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a16_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a16_a_a294 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a17_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a17_a_a291 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a18_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a18_a_a288 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a19_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a19_a_a285 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a20_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a14_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a13_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a12_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a11_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a10_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a7_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a6_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a5_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a4_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a3_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a1_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a0_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Reset_areg0 : std_logic;
SIGNAL reset_Time_Cen_a0 : std_logic;
SIGNAL reset_Del : std_logic;
SIGNAL Reset_LEd_a0 : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL det_sat_areg0 : std_logic;
SIGNAL CPS_a20_a_acombout : std_logic;
SIGNAL CPS_a19_a_acombout : std_logic;
SIGNAL hc_rate_a18_a_acombout : std_logic;
SIGNAL CPS_a17_a_acombout : std_logic;
SIGNAL hc_rate_a16_a_acombout : std_logic;
SIGNAL hc_rate_a15_a_acombout : std_logic;
SIGNAL hc_rate_a14_a_acombout : std_logic;
SIGNAL CPS_a13_a_acombout : std_logic;
SIGNAL CPS_a12_a_acombout : std_logic;
SIGNAL hc_rate_a11_a_acombout : std_logic;
SIGNAL hc_rate_a10_a_acombout : std_logic;
SIGNAL hc_rate_a9_a_acombout : std_logic;
SIGNAL hc_rate_a8_a_acombout : std_logic;
SIGNAL hc_rate_a7_a_acombout : std_logic;
SIGNAL hc_rate_a6_a_acombout : std_logic;
SIGNAL hc_rate_a5_a_acombout : std_logic;
SIGNAL CPS_a4_a_acombout : std_logic;
SIGNAL CPS_a3_a_acombout : std_logic;
SIGNAL hc_rate_a2_a_acombout : std_logic;
SIGNAL CPS_a1_a_acombout : std_logic;
SIGNAL hc_rate_a0_a_acombout : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL hc_cnt_cen_a9 : std_logic;
SIGNAL hc_cnt_clr_a0 : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL hc_threshold_a9_a_acombout : std_logic;
SIGNAL hc_threshold_a8_a_acombout : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Hi_Cnt_areg0 : std_logic;
SIGNAL NSlope_Cmp_out_areg0 : std_logic;
SIGNAL pslope_cmp_out_areg0 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194 : std_logic;
SIGNAL Reset_Time_FF_adffs_a0_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a1_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a2_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a3_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a4_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a5_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a6_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a7_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a8_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a9_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a10_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a11_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a12_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a13_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a14_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a15_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a16_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a17_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a18_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a19_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a20_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a21_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a22_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a23_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a24_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a25_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a26_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a27_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a28_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a29_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a30_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a31_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL Reset_End : std_logic;
SIGNAL Reset_End_Cnt_a1_a : std_logic;
SIGNAL Reset_End_Cnt_a2_a : std_logic;
SIGNAL Reset_End_Cnt_a0_a : std_logic;
SIGNAL Reset_End_Cnt_a3_a : std_logic;
SIGNAL Equal_a213 : std_logic;
SIGNAL reset_width_ff_adffs_a0_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL reset_width_ff_adffs_a1_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL reset_width_ff_adffs_a2_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL reset_width_ff_adffs_a3_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL reset_width_ff_adffs_a4_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL reset_width_ff_adffs_a5_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL reset_width_ff_adffs_a6_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL reset_width_ff_adffs_a7_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL reset_width_ff_adffs_a8_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL reset_width_ff_adffs_a9_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL reset_width_ff_adffs_a10_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL reset_width_ff_adffs_a11_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL reset_width_ff_adffs_a12_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL reset_width_ff_adffs_a13_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL reset_width_ff_adffs_a14_a : std_logic;
SIGNAL ALT_INV_AD_CLR_areg0 : std_logic;
SIGNAL ALT_INV_fir_mr_str : std_logic;
SIGNAL ALT_INV_IMR_acombout : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK <= CLK;
ww_Fir_Mr <= Fir_Mr;
ww_We_Clr_AD <= We_Clr_AD;
ww_Dout <= Dout;
ww_ms100tc <= ms100tc;
ww_CPS <= CPS;
ww_hc_rate <= hc_rate;
ww_hc_threshold <= hc_threshold;
AD_CLR <= ww_AD_CLR;
det_sat <= ww_det_sat;
Hi_Cnt <= ww_Hi_Cnt;
Reset <= ww_Reset;
NSlope_Cmp_out <= ww_NSlope_Cmp_out;
pslope_cmp_out <= ww_pslope_cmp_out;
Reset_time <= ww_Reset_time;
Reset_Width <= ww_Reset_Width;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

Dly_RAM2_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a15_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a15_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 & 
Dly_RAM2_asram_asegment_a0_a_a15_a_a20 & wadr_cntr_awysi_counter_asload_path_a0_a);
ALT_INV_AD_CLR_areg0 <= NOT AD_CLR_areg0;
ALT_INV_fir_mr_str <= NOT fir_mr_str;
ALT_INV_IMR_acombout <= NOT IMR_acombout;

Ad_Clr_Cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a3_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a3_a $ (Ad_Clr_Cnt_a2_a_a116), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a3_a_a113 = CARRY(!Ad_Clr_Cnt_a2_a_a116 # !Ad_Clr_Cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a3_a,
	cin => Ad_Clr_Cnt_a2_a_a116,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a3_a,
	cout => Ad_Clr_Cnt_a3_a_a113);

Equal_a208_I : apex20ke_lcell
-- Equation(s):
-- Equal_a208 = !Ad_Clr_Cnt_a2_a & !Ad_Clr_Cnt_a1_a & !Ad_Clr_Cnt_a3_a & !Ad_Clr_Cnt_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a2_a,
	datab => Ad_Clr_Cnt_a1_a,
	datac => Ad_Clr_Cnt_a3_a,
	datad => Ad_Clr_Cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a208);

Ad_Clr_Cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a5_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a5_a $ (Ad_Clr_Cnt_a4_a_a134), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a5_a_a131 = CARRY(!Ad_Clr_Cnt_a4_a_a134 # !Ad_Clr_Cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a5_a,
	cin => Ad_Clr_Cnt_a4_a_a134,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a5_a,
	cout => Ad_Clr_Cnt_a5_a_a131);

hc_threshold_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(15),
	combout => hc_threshold_a15_a_acombout);

hc_rate_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(20),
	combout => hc_rate_a20_a_acombout);

Equal_a215_I : apex20ke_lcell
-- Equation(s):
-- Equal_a215 = Reset_End_Cnt_a1_a & (Reset_End_Cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a1_a,
	datac => Reset_End_Cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a215);

hc_threshold_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(14),
	combout => hc_threshold_a14_a_acombout);

hc_rate_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(19),
	combout => hc_rate_a19_a_acombout);

hc_threshold_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(13),
	combout => hc_threshold_a13_a_acombout);

CPS_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(18),
	combout => CPS_a18_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a15_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a15_a_modesel,
	dataout => Dly_RAM2_asram_aq_a15_a);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478 = Dly_RAM2_asram_aq_a15_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a480 = CARRY(Dly_RAM2_asram_aq_a15_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436) # !Dly_RAM2_asram_aq_a15_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a15_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a480);

fir_mr_str_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a4_a = DFFE(GLOBAL(fir_mr_str) & fir_mr_str_cnt_a4_a $ (!fir_mr_str_cnt_a3_a_a63), GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A5A5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a4_a,
	cin => fir_mr_str_cnt_a3_a_a63,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a4_a);

fir_mr_str_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a3_a = DFFE(GLOBAL(fir_mr_str) & fir_mr_str_cnt_a3_a $ (fir_mr_str_cnt_a2_a_a60), GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a3_a_a63 = CARRY(!fir_mr_str_cnt_a2_a_a60 # !fir_mr_str_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a3_a,
	cin => fir_mr_str_cnt_a2_a_a60,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a3_a,
	cout => fir_mr_str_cnt_a3_a_a63);

hc_threshold_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(12),
	combout => hc_threshold_a12_a_acombout);

hc_rate_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(17),
	combout => hc_rate_a17_a_acombout);

Dout_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(15),
	combout => Dout_a15_a_acombout);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482 = Dly_RAM2_asram_aq_a15_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484 = CARRY(Dly_RAM2_asram_aq_a15_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488 # !Dly_RAM2_asram_aq_a15_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a15_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a484);

hc_threshold_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(11),
	combout => hc_threshold_a11_a_acombout);

CPS_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(16),
	combout => CPS_a16_a_acombout);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486 = Dly_RAM2_asram_aq_a15_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488 = CARRY(Dly_RAM2_asram_aq_a15_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444) # !Dly_RAM2_asram_aq_a15_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a15_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a488);

hc_threshold_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(10),
	combout => hc_threshold_a10_a_acombout);

CPS_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(15),
	combout => CPS_a15_a_acombout);

CPS_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(14),
	combout => CPS_a14_a_acombout);

Dout_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(14),
	combout => Dout_a14_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a14_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a14_a_modesel,
	dataout => Dly_RAM2_asram_aq_a14_a);

hc_rate_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(13),
	combout => hc_rate_a13_a_acombout);

Dout_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(13),
	combout => Dout_a13_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a13_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a13_a_modesel,
	dataout => Dly_RAM2_asram_aq_a13_a);

hc_threshold_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(7),
	combout => hc_threshold_a7_a_acombout);

hc_rate_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(12),
	combout => hc_rate_a12_a_acombout);

Dout_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(12),
	combout => Dout_a12_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a12_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a12_a_modesel,
	dataout => Dly_RAM2_asram_aq_a12_a);

hc_threshold_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(6),
	combout => hc_threshold_a6_a_acombout);

CPS_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(11),
	combout => CPS_a11_a_acombout);

Dout_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(11),
	combout => Dout_a11_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a11_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a11_a_modesel,
	dataout => Dly_RAM2_asram_aq_a11_a);

hc_threshold_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(5),
	combout => hc_threshold_a5_a_acombout);

CPS_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(10),
	combout => CPS_a10_a_acombout);

Dout_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(10),
	combout => Dout_a10_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a10_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a10_a_modesel,
	dataout => Dly_RAM2_asram_aq_a10_a);

hc_threshold_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(4),
	combout => hc_threshold_a4_a_acombout);

CPS_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(9),
	combout => CPS_a9_a_acombout);

Dout_Diff_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a9_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a9_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510 $ Dout_Diff_FF_adffs_a8_a_a318, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a9_a_a315 = CARRY(Dout_Diff_FF_adffs_a9_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510 & !Dout_Diff_FF_adffs_a8_a_a318 # !Dout_Diff_FF_adffs_a9_a & (!Dout_Diff_FF_adffs_a8_a_a318 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a9_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510,
	cin => Dout_Diff_FF_adffs_a8_a_a318,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a9_a,
	cout => Dout_Diff_FF_adffs_a9_a_a315);

Dout_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(9),
	combout => Dout_a9_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a9_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a9_a_modesel,
	dataout => Dly_RAM2_asram_aq_a9_a);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514 = Dly_RAM2_asram_aq_a8_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516 = CARRY(Dly_RAM2_asram_aq_a8_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520 # !Dly_RAM2_asram_aq_a8_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a8_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516);

hc_threshold_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(3),
	combout => hc_threshold_a3_a_acombout);

CPS_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(8),
	combout => CPS_a8_a_acombout);

Dout_Diff_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a8_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514 $ PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 $ !Dout_Diff_FF_adffs_a7_a_a321, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a8_a_a318 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514 & (PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 # !Dout_Diff_FF_adffs_a7_a_a321) # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514 & 
-- PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 & !Dout_Diff_FF_adffs_a7_a_a321)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a514,
	datab => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38,
	cin => Dout_Diff_FF_adffs_a7_a_a321,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a8_a,
	cout => Dout_Diff_FF_adffs_a8_a_a318);

Dout_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(8),
	combout => Dout_a8_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a8_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a8_a_modesel,
	dataout => Dly_RAM2_asram_aq_a8_a);

hc_threshold_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(2),
	combout => hc_threshold_a2_a_acombout);

CPS_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(7),
	combout => CPS_a7_a_acombout);

Dout_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(7),
	combout => Dout_a7_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a7_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a7_a_modesel,
	dataout => Dly_RAM2_asram_aq_a7_a);

hc_threshold_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(1),
	combout => hc_threshold_a1_a_acombout);

CPS_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(6),
	combout => CPS_a6_a_acombout);

Dout_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(6),
	combout => Dout_a6_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a6_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a6_a_modesel,
	dataout => Dly_RAM2_asram_aq_a6_a);

hc_threshold_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(0),
	combout => hc_threshold_a0_a_acombout);

CPS_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(5),
	combout => CPS_a5_a_acombout);

Dout_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(5),
	combout => Dout_a5_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a5_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a5_a_modesel,
	dataout => Dly_RAM2_asram_aq_a5_a);

hc_rate_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(4),
	combout => hc_rate_a4_a_acombout);

Dout_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(4),
	combout => Dout_a4_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a4_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a4_a_modesel,
	dataout => Dly_RAM2_asram_aq_a4_a);

hc_rate_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(3),
	combout => hc_rate_a3_a_acombout);

Dout_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(3),
	combout => Dout_a3_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a3_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a3_a_modesel,
	dataout => Dly_RAM2_asram_aq_a3_a);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538 = Dly_RAM2_asram_aq_a2_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540 = CARRY(Dly_RAM2_asram_aq_a2_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544 # !Dly_RAM2_asram_aq_a2_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a2_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540);

CPS_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(2),
	combout => CPS_a2_a_acombout);

Dout_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(2),
	combout => Dout_a2_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a2_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a2_a_modesel,
	dataout => Dly_RAM2_asram_aq_a2_a);

hc_rate_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(1),
	combout => hc_rate_a1_a_acombout);

NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165 = Dout_Diff_FF_adffs_a0_a
-- NSlope_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(Dout_Diff_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a);

Dout_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(1),
	combout => Dout_a1_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a1_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a1_a_modesel,
	dataout => Dly_RAM2_asram_aq_a1_a);

CPS_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(0),
	combout => CPS_a0_a_acombout);

Dout_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(0),
	combout => Dout_a0_a_acombout);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a = Dout_a0_a_acombout
-- SumA_Add_sub_aadder_aresult_node_acout_a0_a = CARRY(!Dout_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC33",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a,
	cout => SumA_Add_sub_aadder_aresult_node_acout_a0_a);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510 = SumA_Add_sub_aadder_aresult_node_acout_a0_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => SumA_Add_sub_aadder_aresult_node_acout_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510);

We_Clr_AD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_We_Clr_AD,
	combout => We_Clr_AD_acombout);

CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK,
	combout => CLK_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

Ad_Clr_Cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a0_a = DFFE(AD_CLR_areg0 & !Ad_Clr_Cnt_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a0_a_a122 = CARRY(Ad_Clr_Cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a0_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a0_a,
	cout => Ad_Clr_Cnt_a0_a_a122);

Ad_Clr_Cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a1_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a1_a $ Ad_Clr_Cnt_a0_a_a122, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a1_a_a119 = CARRY(!Ad_Clr_Cnt_a0_a_a122 # !Ad_Clr_Cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a1_a,
	cin => Ad_Clr_Cnt_a0_a_a122,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a1_a,
	cout => Ad_Clr_Cnt_a1_a_a119);

Ad_Clr_Cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a2_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a2_a $ !Ad_Clr_Cnt_a1_a_a119, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a2_a_a116 = CARRY(Ad_Clr_Cnt_a2_a & !Ad_Clr_Cnt_a1_a_a119)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a2_a,
	cin => Ad_Clr_Cnt_a1_a_a119,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a2_a,
	cout => Ad_Clr_Cnt_a2_a_a116);

Ad_Clr_Cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a4_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a4_a $ !Ad_Clr_Cnt_a3_a_a113, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a4_a_a134 = CARRY(Ad_Clr_Cnt_a4_a & !Ad_Clr_Cnt_a3_a_a113)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a4_a,
	cin => Ad_Clr_Cnt_a3_a_a113,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a4_a,
	cout => Ad_Clr_Cnt_a4_a_a134);

Ad_Clr_Cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a6_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a6_a $ (!Ad_Clr_Cnt_a5_a_a131), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a6_a_a128 = CARRY(Ad_Clr_Cnt_a6_a & (!Ad_Clr_Cnt_a5_a_a131))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a6_a,
	cin => Ad_Clr_Cnt_a5_a_a131,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a6_a,
	cout => Ad_Clr_Cnt_a6_a_a128);

Ad_Clr_Cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a7_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a7_a $ Ad_Clr_Cnt_a6_a_a128, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a7_a_a125 = CARRY(!Ad_Clr_Cnt_a6_a_a128 # !Ad_Clr_Cnt_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a7_a,
	cin => Ad_Clr_Cnt_a6_a_a128,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a7_a,
	cout => Ad_Clr_Cnt_a7_a_a125);

Ad_Clr_Cnt_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a8_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a8_a $ (!Ad_Clr_Cnt_a7_a_a125), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a8_a_a146 = CARRY(Ad_Clr_Cnt_a8_a & (!Ad_Clr_Cnt_a7_a_a125))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a8_a,
	cin => Ad_Clr_Cnt_a7_a_a125,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a8_a,
	cout => Ad_Clr_Cnt_a8_a_a146);

Ad_Clr_Cnt_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a9_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a9_a $ (Ad_Clr_Cnt_a8_a_a146), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a9_a_a143 = CARRY(!Ad_Clr_Cnt_a8_a_a146 # !Ad_Clr_Cnt_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a9_a,
	cin => Ad_Clr_Cnt_a8_a_a146,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a9_a,
	cout => Ad_Clr_Cnt_a9_a_a143);

Ad_Clr_Cnt_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a10_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a10_a $ !Ad_Clr_Cnt_a9_a_a143, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a10_a_a140 = CARRY(Ad_Clr_Cnt_a10_a & !Ad_Clr_Cnt_a9_a_a143)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a10_a,
	cin => Ad_Clr_Cnt_a9_a_a143,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a10_a,
	cout => Ad_Clr_Cnt_a10_a_a140);

Equal_a210_I : apex20ke_lcell
-- Equation(s):
-- Equal_a210 = !Ad_Clr_Cnt_a11_a & !Ad_Clr_Cnt_a9_a & !Ad_Clr_Cnt_a10_a & !Ad_Clr_Cnt_a8_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a11_a,
	datab => Ad_Clr_Cnt_a9_a,
	datac => Ad_Clr_Cnt_a10_a,
	datad => Ad_Clr_Cnt_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a210);

Equal_a209_I : apex20ke_lcell
-- Equation(s):
-- Equal_a209 = !Ad_Clr_Cnt_a5_a & !Ad_Clr_Cnt_a7_a & !Ad_Clr_Cnt_a6_a & !Ad_Clr_Cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a5_a,
	datab => Ad_Clr_Cnt_a7_a,
	datac => Ad_Clr_Cnt_a6_a,
	datad => Ad_Clr_Cnt_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a209);

Ad_Clr_Cnt_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a11_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a11_a $ Ad_Clr_Cnt_a10_a_a140, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a11_a_a137 = CARRY(!Ad_Clr_Cnt_a10_a_a140 # !Ad_Clr_Cnt_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a11_a,
	cin => Ad_Clr_Cnt_a10_a_a140,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a11_a,
	cout => Ad_Clr_Cnt_a11_a_a137);

Ad_Clr_Cnt_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a12_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a12_a $ (!Ad_Clr_Cnt_a11_a_a137), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a12_a_a152 = CARRY(Ad_Clr_Cnt_a12_a & (!Ad_Clr_Cnt_a11_a_a137))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a12_a,
	cin => Ad_Clr_Cnt_a11_a_a137,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a12_a,
	cout => Ad_Clr_Cnt_a12_a_a152);

Ad_Clr_Cnt_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a13_a = DFFE(AD_CLR_areg0 & Ad_Clr_Cnt_a13_a $ (Ad_Clr_Cnt_a12_a_a152), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a13_a,
	cin => Ad_Clr_Cnt_a12_a_a152,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a13_a);

Equal_a211_I : apex20ke_lcell
-- Equation(s):
-- Equal_a211 = Ad_Clr_Cnt_a13_a & (!Ad_Clr_Cnt_a12_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a13_a,
	datad => Ad_Clr_Cnt_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a211);

Equal_a212_I : apex20ke_lcell
-- Equation(s):
-- Equal_a212 = Equal_a208 & Equal_a210 & Equal_a209 & Equal_a211

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a208,
	datab => Equal_a210,
	datac => Equal_a209,
	datad => Equal_a211,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a212);

AD_CLR_areg0_I : apex20ke_lcell
-- Equation(s):
-- AD_CLR_areg0 = DFFE(We_Clr_AD_acombout # AD_CLR_areg0 & !Equal_a212, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => We_Clr_AD_acombout,
	datac => AD_CLR_areg0,
	datad => Equal_a212,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AD_CLR_areg0);

DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out = GND

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out);

ms100tc_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ms100tc,
	combout => ms100tc_acombout);

add_a529_I : apex20ke_lcell
-- Equation(s):
-- add_a529 = !us1_cnt_a0_a
-- add_a531 = CARRY(us1_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a529,
	cout => add_a531);

us1_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a0_a = DFFE(add_a529, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add_a529,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a0_a);

add_a513_I : apex20ke_lcell
-- Equation(s):
-- add_a513 = us1_cnt_a1_a $ add_a531
-- add_a515 = CARRY(!add_a531 # !us1_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a1_a,
	cin => add_a531,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a513,
	cout => add_a515);

us1_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a1_a = DFFE(add_a513, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add_a513,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a1_a);

add_a521_I : apex20ke_lcell
-- Equation(s):
-- add_a521 = us1_cnt_a2_a $ !add_a515
-- add_a523 = CARRY(us1_cnt_a2_a & !add_a515)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a2_a,
	cin => add_a515,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a521,
	cout => add_a523);

us1_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a2_a = DFFE(add_a521 $ (Equal_a214 & us1_cnt_a0_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5FA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a214,
	datac => us1_cnt_a0_a,
	datad => add_a521,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a2_a);

add_a525_I : apex20ke_lcell
-- Equation(s):
-- add_a525 = us1_cnt_a3_a $ add_a523
-- add_a527 = CARRY(!add_a523 # !us1_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a3_a,
	cin => add_a523,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a525,
	cout => add_a527);

us1_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a3_a = DFFE(add_a525, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => add_a525,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a3_a);

add_a517_I : apex20ke_lcell
-- Equation(s):
-- add_a517 = add_a527 $ !us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => us1_cnt_a4_a,
	cin => add_a527,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a517);

us1_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a4_a = DFFE(add_a517 $ (Equal_a214 & us1_cnt_a0_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5FA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a214,
	datac => us1_cnt_a0_a,
	datad => add_a517,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a4_a);

Equal_a214_I : apex20ke_lcell
-- Equation(s):
-- Equal_a214 = !us1_cnt_a2_a & !us1_cnt_a3_a & us1_cnt_a1_a & us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a2_a,
	datab => us1_cnt_a3_a,
	datac => us1_cnt_a1_a,
	datad => us1_cnt_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a214);

PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 = Dout_Diff_FF_adffs_a8_a
-- PSlope_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(!Dout_Diff_FF_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA55",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a37_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(Dout_Diff_FF_adffs_a9_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a9_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a37,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a9_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a36_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(!Dout_Diff_FF_adffs_a10_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a10_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a36,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a10_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a35_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(Dout_Diff_FF_adffs_a11_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a11_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a35,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a11_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a34_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(!Dout_Diff_FF_adffs_a12_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a12_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a34,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a12_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a33_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(Dout_Diff_FF_adffs_a13_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a13_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a33,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a13_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a32_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(!Dout_Diff_FF_adffs_a14_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a14_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a32,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a14_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a31_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(Dout_Diff_FF_adffs_a15_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a15_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a31,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a15_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a30_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(!Dout_Diff_FF_adffs_a16_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a16_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a30,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a16_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a29_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(Dout_Diff_FF_adffs_a17_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a17_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a29,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a17_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a28_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(!Dout_Diff_FF_adffs_a18_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a18_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a28,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a18_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a27_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(Dout_Diff_FF_adffs_a19_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a19_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a27,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a19_a);

PSlope_Compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_aagb_out = Dout_Diff_FF_adffs_a20_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a19_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "AFAF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a20_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_aagb_out);

wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(wadr_cntr_awysi_counter_asload_path_a1_a $ wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(wadr_cntr_awysi_counter_asload_path_a2_a $ !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a2_a & !wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT $ wadr_cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a3_a);

radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a = wadr_cntr_awysi_counter_asload_path_a1_a
-- radr_Add_sub_aadder_aresult_node_acout_a1_a = CARRY(wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a1_a);

Dly_RAM2_asram_asegment_a0_a_a15_a_a20_I : apex20ke_lcell
-- Equation(s):
-- Dly_RAM2_asram_asegment_a0_a_a15_a_a20 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dly_RAM2_asram_asegment_a0_a_a15_a_a20);

radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a42 : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43 = radr_Add_sub_aadder_aresult_node_acout_a1_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => radr_Add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38_I : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38 = wadr_cntr_awysi_counter_asload_path_a2_a $ radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a38);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39_I : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39 = wadr_cntr_awysi_counter_asload_path_a3_a $ (!radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43 # !wadr_cntr_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A50F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a2_a,
	datac => wadr_cntr_awysi_counter_asload_path_a3_a,
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a43,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a39);

Dly_RAM1_asram_asegment_a0_a_a15_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 15,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a15_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a15_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a15_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a15_a_modesel,
	dataout => Dly_RAM1_asram_aq_a15_a);

Dly_RAM1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 14,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a14_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a14_a_modesel,
	dataout => Dly_RAM1_asram_aq_a14_a);

Dly_RAM1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 13,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a13_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a13_a_modesel,
	dataout => Dly_RAM1_asram_aq_a13_a);

Dly_RAM1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 12,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a12_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a12_a_modesel,
	dataout => Dly_RAM1_asram_aq_a12_a);

Dly_RAM1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 11,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a11_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a11_a_modesel,
	dataout => Dly_RAM1_asram_aq_a11_a);

Dly_RAM1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 10,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a10_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a10_a_modesel,
	dataout => Dly_RAM1_asram_aq_a10_a);

Dly_RAM1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 9,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a9_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a9_a_modesel,
	dataout => Dly_RAM1_asram_aq_a9_a);

Dly_RAM1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 8,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a8_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a8_a_modesel,
	dataout => Dly_RAM1_asram_aq_a8_a);

Dly_RAM1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 7,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a7_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a7_a_modesel,
	dataout => Dly_RAM1_asram_aq_a7_a);

Dly_RAM1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 6,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a6_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a6_a_modesel,
	dataout => Dly_RAM1_asram_aq_a6_a);

Dly_RAM1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 5,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a5_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a5_a_modesel,
	dataout => Dly_RAM1_asram_aq_a5_a);

Dly_RAM1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 4,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a4_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a4_a_modesel,
	dataout => Dly_RAM1_asram_aq_a4_a);

Dly_RAM1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 3,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a3_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a3_a_modesel,
	dataout => Dly_RAM1_asram_aq_a3_a);

Dly_RAM1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 2,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a2_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a2_a_modesel,
	dataout => Dly_RAM1_asram_aq_a2_a);

Dly_RAM1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 1,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a1_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a1_a_modesel,
	dataout => Dly_RAM1_asram_aq_a1_a);

Dly_RAM1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dout_a0_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a0_a_modesel,
	dataout => Dly_RAM1_asram_aq_a0_a);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506 = CARRY(SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00AA",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a510,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a504,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500 = Dout_a1_a_acombout $ Dly_RAM1_asram_aq_a0_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502 = CARRY(Dout_a1_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506 # !Dly_RAM1_asram_aq_a0_a) # !Dout_a1_a_acombout & !Dly_RAM1_asram_aq_a0_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a1_a_acombout,
	datab => Dly_RAM1_asram_aq_a0_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a506,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496 = Dout_a2_a_acombout $ Dly_RAM1_asram_aq_a1_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498 = CARRY(Dout_a2_a_acombout & Dly_RAM1_asram_aq_a1_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502 # !Dout_a2_a_acombout & (Dly_RAM1_asram_aq_a1_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a2_a_acombout,
	datab => Dly_RAM1_asram_aq_a1_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a502,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a496,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492 = Dout_a3_a_acombout $ Dly_RAM1_asram_aq_a2_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494 = CARRY(Dout_a3_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498 # !Dly_RAM1_asram_aq_a2_a) # !Dout_a3_a_acombout & !Dly_RAM1_asram_aq_a2_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a3_a_acombout,
	datab => Dly_RAM1_asram_aq_a2_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a498,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488 = Dout_a4_a_acombout $ Dly_RAM1_asram_aq_a3_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490 = CARRY(Dout_a4_a_acombout & Dly_RAM1_asram_aq_a3_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494 # !Dout_a4_a_acombout & (Dly_RAM1_asram_aq_a3_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a4_a_acombout,
	datab => Dly_RAM1_asram_aq_a3_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a494,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484 = Dout_a5_a_acombout $ Dly_RAM1_asram_aq_a4_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486 = CARRY(Dout_a5_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490 # !Dly_RAM1_asram_aq_a4_a) # !Dout_a5_a_acombout & !Dly_RAM1_asram_aq_a4_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a5_a_acombout,
	datab => Dly_RAM1_asram_aq_a4_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a490,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480 = Dout_a6_a_acombout $ Dly_RAM1_asram_aq_a5_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482 = CARRY(Dout_a6_a_acombout & Dly_RAM1_asram_aq_a5_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486 # !Dout_a6_a_acombout & (Dly_RAM1_asram_aq_a5_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a6_a_acombout,
	datab => Dly_RAM1_asram_aq_a5_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a486,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476 = Dout_a7_a_acombout $ Dly_RAM1_asram_aq_a6_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478 = CARRY(Dout_a7_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482 # !Dly_RAM1_asram_aq_a6_a) # !Dout_a7_a_acombout & !Dly_RAM1_asram_aq_a6_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a7_a_acombout,
	datab => Dly_RAM1_asram_aq_a6_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a482,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472 = Dout_a8_a_acombout $ Dly_RAM1_asram_aq_a7_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474 = CARRY(Dout_a8_a_acombout & Dly_RAM1_asram_aq_a7_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478 # !Dout_a8_a_acombout & (Dly_RAM1_asram_aq_a7_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a8_a_acombout,
	datab => Dly_RAM1_asram_aq_a7_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a478,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a472,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468 = Dout_a9_a_acombout $ Dly_RAM1_asram_aq_a8_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470 = CARRY(Dout_a9_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474 # !Dly_RAM1_asram_aq_a8_a) # !Dout_a9_a_acombout & !Dly_RAM1_asram_aq_a8_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a9_a_acombout,
	datab => Dly_RAM1_asram_aq_a8_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a474,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464 = Dout_a10_a_acombout $ Dly_RAM1_asram_aq_a9_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466 = CARRY(Dout_a10_a_acombout & Dly_RAM1_asram_aq_a9_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470 # !Dout_a10_a_acombout & (Dly_RAM1_asram_aq_a9_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a10_a_acombout,
	datab => Dly_RAM1_asram_aq_a9_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a470,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460 = Dout_a11_a_acombout $ Dly_RAM1_asram_aq_a10_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462 = CARRY(Dout_a11_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466 # !Dly_RAM1_asram_aq_a10_a) # !Dout_a11_a_acombout & !Dly_RAM1_asram_aq_a10_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a11_a_acombout,
	datab => Dly_RAM1_asram_aq_a10_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a466,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456 = Dout_a12_a_acombout $ Dly_RAM1_asram_aq_a11_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458 = CARRY(Dout_a12_a_acombout & Dly_RAM1_asram_aq_a11_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462 # !Dout_a12_a_acombout & (Dly_RAM1_asram_aq_a11_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a12_a_acombout,
	datab => Dly_RAM1_asram_aq_a11_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a462,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452 = Dout_a13_a_acombout $ Dly_RAM1_asram_aq_a12_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454 = CARRY(Dout_a13_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458 # !Dly_RAM1_asram_aq_a12_a) # !Dout_a13_a_acombout & !Dly_RAM1_asram_aq_a12_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a13_a_acombout,
	datab => Dly_RAM1_asram_aq_a12_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a458,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448 = Dout_a14_a_acombout $ Dly_RAM1_asram_aq_a13_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450 = CARRY(Dout_a14_a_acombout & Dly_RAM1_asram_aq_a13_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454 # !Dout_a14_a_acombout & (Dly_RAM1_asram_aq_a13_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a14_a_acombout,
	datab => Dly_RAM1_asram_aq_a13_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a454,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444 = Dout_a15_a_acombout $ Dly_RAM1_asram_aq_a14_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446 = CARRY(Dout_a15_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450 # !Dly_RAM1_asram_aq_a14_a) # !Dout_a15_a_acombout & !Dly_RAM1_asram_aq_a14_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a15_a_acombout,
	datab => Dly_RAM1_asram_aq_a14_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a450,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a444,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440 = Dout_a15_a_acombout $ Dly_RAM1_asram_aq_a15_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a442 = CARRY(Dout_a15_a_acombout & Dly_RAM1_asram_aq_a15_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446 # !Dout_a15_a_acombout & (Dly_RAM1_asram_aq_a15_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a15_a_acombout,
	datab => Dly_RAM1_asram_aq_a15_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a446,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a440,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a442);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436 = Dout_a15_a_acombout $ (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a442 $ !Dly_RAM1_asram_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a15_a_acombout,
	datad => Dly_RAM1_asram_aq_a15_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a442,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436);

Dly_RAM2_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	address_width => 4,
	bit_number => 0,
	data_in_clear => "none",
	data_in_clock => "clock0",
	data_out_clear => "none",
	data_out_clock => "clock1",
	deep_ram_mode => "off",
	first_address => 0,
	init_file => "INIT.MIF",
	last_address => 15,
	logical_ram_depth => 16,
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	logical_ram_width => 16,
	operation_mode => "dual_port",
	read_address_clear => "none",
	read_address_clock => "clock1",
	read_enable_clear => "none",
	read_enable_clock => "none",
	write_logic_clear => "none",
	write_logic_clock => "clock0")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a0_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a0_a_modesel,
	dataout => Dly_RAM2_asram_aq_a0_a);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546 = SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a $ Dly_RAM2_asram_aq_a0_a
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548 = CARRY(SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a # !Dly_RAM2_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a,
	datab => Dly_RAM2_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542 = Dly_RAM2_asram_aq_a1_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544 = CARRY(Dly_RAM2_asram_aq_a1_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500) # !Dly_RAM2_asram_aq_a1_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a1_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a500,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a548,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a544);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534 = Dly_RAM2_asram_aq_a3_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536 = CARRY(Dly_RAM2_asram_aq_a3_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492) # !Dly_RAM2_asram_aq_a3_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a3_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a492,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a540,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530 = Dly_RAM2_asram_aq_a4_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532 = CARRY(Dly_RAM2_asram_aq_a4_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536 # !Dly_RAM2_asram_aq_a4_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a4_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a488,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a536,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526 = Dly_RAM2_asram_aq_a5_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528 = CARRY(Dly_RAM2_asram_aq_a5_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484) # !Dly_RAM2_asram_aq_a5_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a5_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a484,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a532,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522 = Dly_RAM2_asram_aq_a6_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524 = CARRY(Dly_RAM2_asram_aq_a6_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528 # !Dly_RAM2_asram_aq_a6_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a6_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a480,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a528,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518 = Dly_RAM2_asram_aq_a7_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520 = CARRY(Dly_RAM2_asram_aq_a7_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476) # !Dly_RAM2_asram_aq_a7_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a7_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a476,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a524,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a520);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510 = Dly_RAM2_asram_aq_a9_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512 = CARRY(Dly_RAM2_asram_aq_a9_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468) # !Dly_RAM2_asram_aq_a9_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a9_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a468,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a516,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a510,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506 = Dly_RAM2_asram_aq_a10_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508 = CARRY(Dly_RAM2_asram_aq_a10_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512 # !Dly_RAM2_asram_aq_a10_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a10_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a464,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a512,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502 = Dly_RAM2_asram_aq_a11_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504 = CARRY(Dly_RAM2_asram_aq_a11_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460) # !Dly_RAM2_asram_aq_a11_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a11_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a460,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a508,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498 = Dly_RAM2_asram_aq_a12_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500 = CARRY(Dly_RAM2_asram_aq_a12_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504 # !Dly_RAM2_asram_aq_a12_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a12_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a456,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a504,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494 = Dly_RAM2_asram_aq_a13_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496 = CARRY(Dly_RAM2_asram_aq_a13_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452) # !Dly_RAM2_asram_aq_a13_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a13_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a452,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a500,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492 = CARRY(Dly_RAM2_asram_aq_a14_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496 # !Dly_RAM2_asram_aq_a14_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a448,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a496,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a492);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 = Dly_RAM2_asram_aq_a15_a $ (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a480 $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a15_a,
	datad => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a436,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a480,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474);

Fir_Mr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Fir_Mr,
	combout => Fir_Mr_acombout);

fir_mr_str_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a0_a = DFFE(GLOBAL(fir_mr_str) & !fir_mr_str_cnt_a0_a, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a0_a_a51 = CARRY(fir_mr_str_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a0_a,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a0_a,
	cout => fir_mr_str_cnt_a0_a_a51);

fir_mr_str_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a1_a = DFFE(GLOBAL(fir_mr_str) & fir_mr_str_cnt_a1_a $ fir_mr_str_cnt_a0_a_a51, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a1_a_a54 = CARRY(!fir_mr_str_cnt_a0_a_a51 # !fir_mr_str_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a1_a,
	cin => fir_mr_str_cnt_a0_a_a51,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a1_a,
	cout => fir_mr_str_cnt_a1_a_a54);

fir_mr_str_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a2_a = DFFE(GLOBAL(fir_mr_str) & fir_mr_str_cnt_a2_a $ !fir_mr_str_cnt_a1_a_a54, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a2_a_a60 = CARRY(fir_mr_str_cnt_a2_a & !fir_mr_str_cnt_a1_a_a54)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a2_a,
	cin => fir_mr_str_cnt_a1_a_a54,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a2_a,
	cout => fir_mr_str_cnt_a2_a_a60);

fir_mr_str_a35_I : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_a35 = !fir_mr_str_cnt_a1_a # !fir_mr_str_cnt_a3_a # !fir_mr_str_cnt_a2_a # !fir_mr_str_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7FFF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a4_a,
	datab => fir_mr_str_cnt_a2_a,
	datac => fir_mr_str_cnt_a3_a,
	datad => fir_mr_str_cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_mr_str_a35);

fir_mr_str_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str = DFFE(Fir_Mr_acombout # fir_mr_str & (fir_mr_str_a35 # !fir_mr_str_cnt_a0_a), GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FDCC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a0_a,
	datab => Fir_Mr_acombout,
	datac => fir_mr_str_a35,
	datad => fir_mr_str,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str);

Dout_Diff_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a0_a = DFFE(!GLOBAL(fir_mr_str) & NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a0_a_a342 = CARRY(NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165 & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a_a165,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a546,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a0_a,
	cout => Dout_Diff_FF_adffs_a0_a_a342);

Dout_Diff_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a1_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a1_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542 $ Dout_Diff_FF_adffs_a0_a_a342, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a1_a_a339 = CARRY(Dout_Diff_FF_adffs_a1_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542 & !Dout_Diff_FF_adffs_a0_a_a342 # !Dout_Diff_FF_adffs_a1_a & (!Dout_Diff_FF_adffs_a0_a_a342 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a1_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a542,
	cin => Dout_Diff_FF_adffs_a0_a_a342,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a1_a,
	cout => Dout_Diff_FF_adffs_a1_a_a339);

Dout_Diff_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a2_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538 $ Dout_Diff_FF_adffs_a2_a $ !Dout_Diff_FF_adffs_a1_a_a339, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a2_a_a336 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538 & (Dout_Diff_FF_adffs_a2_a # !Dout_Diff_FF_adffs_a1_a_a339) # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538 & Dout_Diff_FF_adffs_a2_a & 
-- !Dout_Diff_FF_adffs_a1_a_a339)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a538,
	datab => Dout_Diff_FF_adffs_a2_a,
	cin => Dout_Diff_FF_adffs_a1_a_a339,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a2_a,
	cout => Dout_Diff_FF_adffs_a2_a_a336);

Dout_Diff_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a3_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a3_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534 $ Dout_Diff_FF_adffs_a2_a_a336, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a3_a_a333 = CARRY(Dout_Diff_FF_adffs_a3_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534 & !Dout_Diff_FF_adffs_a2_a_a336 # !Dout_Diff_FF_adffs_a3_a & (!Dout_Diff_FF_adffs_a2_a_a336 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a3_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a534,
	cin => Dout_Diff_FF_adffs_a2_a_a336,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a3_a,
	cout => Dout_Diff_FF_adffs_a3_a_a333);

Dout_Diff_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a4_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a4_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530 $ !Dout_Diff_FF_adffs_a3_a_a333, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a4_a_a330 = CARRY(Dout_Diff_FF_adffs_a4_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530 # !Dout_Diff_FF_adffs_a3_a_a333) # !Dout_Diff_FF_adffs_a4_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530 & 
-- !Dout_Diff_FF_adffs_a3_a_a333)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a4_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a530,
	cin => Dout_Diff_FF_adffs_a3_a_a333,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a4_a,
	cout => Dout_Diff_FF_adffs_a4_a_a330);

Dout_Diff_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a5_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a5_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526 $ Dout_Diff_FF_adffs_a4_a_a330, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a5_a_a327 = CARRY(Dout_Diff_FF_adffs_a5_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526 & !Dout_Diff_FF_adffs_a4_a_a330 # !Dout_Diff_FF_adffs_a5_a & (!Dout_Diff_FF_adffs_a4_a_a330 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a5_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a526,
	cin => Dout_Diff_FF_adffs_a4_a_a330,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a5_a,
	cout => Dout_Diff_FF_adffs_a5_a_a327);

Dout_Diff_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a6_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a6_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522 $ !Dout_Diff_FF_adffs_a5_a_a327, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a6_a_a324 = CARRY(Dout_Diff_FF_adffs_a6_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522 # !Dout_Diff_FF_adffs_a5_a_a327) # !Dout_Diff_FF_adffs_a6_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522 & 
-- !Dout_Diff_FF_adffs_a5_a_a327)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a6_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a522,
	cin => Dout_Diff_FF_adffs_a5_a_a327,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a6_a,
	cout => Dout_Diff_FF_adffs_a6_a_a324);

Dout_Diff_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a7_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a7_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518 $ Dout_Diff_FF_adffs_a6_a_a324, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a7_a_a321 = CARRY(Dout_Diff_FF_adffs_a7_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518 & !Dout_Diff_FF_adffs_a6_a_a324 # !Dout_Diff_FF_adffs_a7_a & (!Dout_Diff_FF_adffs_a6_a_a324 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a7_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a518,
	cin => Dout_Diff_FF_adffs_a6_a_a324,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a7_a,
	cout => Dout_Diff_FF_adffs_a7_a_a321);

Dout_Diff_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a10_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a10_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506 $ !Dout_Diff_FF_adffs_a9_a_a315, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a10_a_a312 = CARRY(Dout_Diff_FF_adffs_a10_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506 # !Dout_Diff_FF_adffs_a9_a_a315) # !Dout_Diff_FF_adffs_a10_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506 & 
-- !Dout_Diff_FF_adffs_a9_a_a315)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a10_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a506,
	cin => Dout_Diff_FF_adffs_a9_a_a315,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a10_a,
	cout => Dout_Diff_FF_adffs_a10_a_a312);

Dout_Diff_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a11_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a11_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502 $ Dout_Diff_FF_adffs_a10_a_a312, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a11_a_a309 = CARRY(Dout_Diff_FF_adffs_a11_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502 & !Dout_Diff_FF_adffs_a10_a_a312 # !Dout_Diff_FF_adffs_a11_a & (!Dout_Diff_FF_adffs_a10_a_a312 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a11_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a502,
	cin => Dout_Diff_FF_adffs_a10_a_a312,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a11_a,
	cout => Dout_Diff_FF_adffs_a11_a_a309);

Dout_Diff_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a12_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a12_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498 $ !Dout_Diff_FF_adffs_a11_a_a309, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a12_a_a306 = CARRY(Dout_Diff_FF_adffs_a12_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498 # !Dout_Diff_FF_adffs_a11_a_a309) # !Dout_Diff_FF_adffs_a12_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498 & 
-- !Dout_Diff_FF_adffs_a11_a_a309)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a12_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a498,
	cin => Dout_Diff_FF_adffs_a11_a_a309,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a12_a,
	cout => Dout_Diff_FF_adffs_a12_a_a306);

Dout_Diff_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a13_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a13_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494 $ Dout_Diff_FF_adffs_a12_a_a306, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a13_a_a303 = CARRY(Dout_Diff_FF_adffs_a13_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494 & !Dout_Diff_FF_adffs_a12_a_a306 # !Dout_Diff_FF_adffs_a13_a & (!Dout_Diff_FF_adffs_a12_a_a306 # 
-- !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a13_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a494,
	cin => Dout_Diff_FF_adffs_a12_a_a306,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a13_a,
	cout => Dout_Diff_FF_adffs_a13_a_a303);

Dout_Diff_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a14_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a14_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490 $ !Dout_Diff_FF_adffs_a13_a_a303, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a14_a_a300 = CARRY(Dout_Diff_FF_adffs_a14_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490 # !Dout_Diff_FF_adffs_a13_a_a303) # !Dout_Diff_FF_adffs_a14_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490 & 
-- !Dout_Diff_FF_adffs_a13_a_a303)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a14_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a490,
	cin => Dout_Diff_FF_adffs_a13_a_a303,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a14_a,
	cout => Dout_Diff_FF_adffs_a14_a_a300);

Dout_Diff_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a15_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486 $ Dout_Diff_FF_adffs_a15_a $ Dout_Diff_FF_adffs_a14_a_a300, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a15_a_a297 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486 & !Dout_Diff_FF_adffs_a15_a & !Dout_Diff_FF_adffs_a14_a_a300 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486 & (!Dout_Diff_FF_adffs_a14_a_a300 # 
-- !Dout_Diff_FF_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a486,
	datab => Dout_Diff_FF_adffs_a15_a,
	cin => Dout_Diff_FF_adffs_a14_a_a300,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a15_a,
	cout => Dout_Diff_FF_adffs_a15_a_a297);

Dout_Diff_FF_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a16_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482 $ Dout_Diff_FF_adffs_a16_a $ !Dout_Diff_FF_adffs_a15_a_a297, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a16_a_a294 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482 & (Dout_Diff_FF_adffs_a16_a # !Dout_Diff_FF_adffs_a15_a_a297) # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482 & Dout_Diff_FF_adffs_a16_a & 
-- !Dout_Diff_FF_adffs_a15_a_a297)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a482,
	datab => Dout_Diff_FF_adffs_a16_a,
	cin => Dout_Diff_FF_adffs_a15_a_a297,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a16_a,
	cout => Dout_Diff_FF_adffs_a16_a_a294);

Dout_Diff_FF_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a17_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478 $ Dout_Diff_FF_adffs_a17_a $ Dout_Diff_FF_adffs_a16_a_a294, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a17_a_a291 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478 & !Dout_Diff_FF_adffs_a17_a & !Dout_Diff_FF_adffs_a16_a_a294 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478 & (!Dout_Diff_FF_adffs_a16_a_a294 # 
-- !Dout_Diff_FF_adffs_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a478,
	datab => Dout_Diff_FF_adffs_a17_a,
	cin => Dout_Diff_FF_adffs_a16_a_a294,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a17_a,
	cout => Dout_Diff_FF_adffs_a17_a_a291);

Dout_Diff_FF_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a18_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 $ Dout_Diff_FF_adffs_a18_a $ !Dout_Diff_FF_adffs_a17_a_a291, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a18_a_a288 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 & (Dout_Diff_FF_adffs_a18_a # !Dout_Diff_FF_adffs_a17_a_a291) # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 & Dout_Diff_FF_adffs_a18_a & 
-- !Dout_Diff_FF_adffs_a17_a_a291)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474,
	datab => Dout_Diff_FF_adffs_a18_a,
	cin => Dout_Diff_FF_adffs_a17_a_a291,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a18_a,
	cout => Dout_Diff_FF_adffs_a18_a_a288);

Dout_Diff_FF_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a19_a = DFFE(!GLOBAL(fir_mr_str) & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 $ Dout_Diff_FF_adffs_a19_a $ Dout_Diff_FF_adffs_a18_a_a288, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Dout_Diff_FF_adffs_a19_a_a285 = CARRY(Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 & !Dout_Diff_FF_adffs_a19_a & !Dout_Diff_FF_adffs_a18_a_a288 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474 & (!Dout_Diff_FF_adffs_a18_a_a288 # 
-- !Dout_Diff_FF_adffs_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474,
	datab => Dout_Diff_FF_adffs_a19_a,
	cin => Dout_Diff_FF_adffs_a18_a_a288,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a19_a,
	cout => Dout_Diff_FF_adffs_a19_a_a285);

Dout_Diff_FF_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a20_a = DFFE(!GLOBAL(fir_mr_str) & Dout_Diff_FF_adffs_a20_a $ Dout_Diff_FF_adffs_a19_a_a285 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3CC3",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a20_a,
	datad => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a474,
	cin => Dout_Diff_FF_adffs_a19_a_a285,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a20_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a1_a_a164_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(!Dout_Diff_FF_adffs_a1_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a1_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a1_a_a164,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a1_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a2_a_a163_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(Dout_Diff_FF_adffs_a2_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a2_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a2_a_a163,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a2_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a3_a_a162_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(!Dout_Diff_FF_adffs_a3_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a3_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a3_a_a162,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a3_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a4_a_a161_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(Dout_Diff_FF_adffs_a4_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a4_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a4_a_a161,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a4_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a5_a_a160_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(!Dout_Diff_FF_adffs_a5_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a5_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a5_a_a160,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a5_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a6_a_a159_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(Dout_Diff_FF_adffs_a6_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a6_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a6_a_a159,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a6_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a7_a_a158_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(!Dout_Diff_FF_adffs_a7_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a7_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a7_a_a158,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a7_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a157_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38 # !NSlope_Compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a38,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a157,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a8_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a156_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(!Dout_Diff_FF_adffs_a9_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a9_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a156,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a9_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a155_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(Dout_Diff_FF_adffs_a10_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a10_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a155,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a10_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a154_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(!NSlope_Compare_acomparator_acmp_end_alcarry_a10_a # !Dout_Diff_FF_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a11_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a154,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a11_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a153_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(Dout_Diff_FF_adffs_a12_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a12_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a153,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a12_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a152_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(!Dout_Diff_FF_adffs_a13_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a13_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a152,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a13_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a151_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(Dout_Diff_FF_adffs_a14_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a14_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a151,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a14_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a150_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(!Dout_Diff_FF_adffs_a15_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a15_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a150,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a15_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a149_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(Dout_Diff_FF_adffs_a16_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a16_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a149,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a16_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a148_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(!Dout_Diff_FF_adffs_a17_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a17_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a148,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a17_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a147_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(Dout_Diff_FF_adffs_a18_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a18_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a147,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a18_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a146_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(!Dout_Diff_FF_adffs_a19_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a19_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a146,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a19_a);

NSlope_Compare_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_aagb_out = !NSlope_Compare_acomparator_acmp_end_alcarry_a19_a & !Dout_Diff_FF_adffs_a20_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Dout_Diff_FF_adffs_a20_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_aagb_out);

Reset_areg0_I : apex20ke_lcell
-- Equation(s):
-- Reset_areg0 = DFFE(NSlope_Compare_acomparator_acmp_end_aagb_out # !PSlope_Compare_acomparator_acmp_end_aagb_out & Reset_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF30",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => PSlope_Compare_acomparator_acmp_end_aagb_out,
	datac => Reset_areg0,
	datad => NSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_areg0);

reset_Time_Cen_a0_I : apex20ke_lcell
-- Equation(s):
-- reset_Time_Cen_a0 = Equal_a214 & !Reset_areg0 & us1_cnt_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a214,
	datac => Reset_areg0,
	datad => us1_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reset_Time_Cen_a0);

reset_Del_aI : apex20ke_lcell
-- Equation(s):
-- reset_Del = DFFE(Reset_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_Del);

Reset_LEd_a0_I : apex20ke_lcell
-- Equation(s):
-- Reset_LEd_a0 = Reset_areg0 & (!reset_Del)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_areg0,
	datad => reset_Del,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Reset_LEd_a0);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(Reset_LEd_a0) & reset_Time_Cen_a0 $ Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3CF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => reset_Time_Cen_a0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a31_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a $ (Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT & reset_Time_Cen_a0), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	datad => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194 = Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCCC",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a_a193_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a_a193,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a_a192_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a_a192,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a_a191_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a_a191,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a_a190_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a_a190,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a_a189_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a_a189,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a_a188_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a_a188,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a_a187_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a_a187,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a_a186_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a_a186,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a_a185_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a_a185,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a_a184_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a_a184,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a_a183_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a_a183,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a_a182_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a_a182,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a_a181_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a_a181,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a_a180_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a_a180,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a_a179_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a_a179,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a_a178_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a_a178,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a_a177_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a_a177,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a_a176_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a_a176,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a_a175_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a_a175,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a_a174_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a_a174,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a_a173_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a_a173,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a_a172_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a_a172,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a_a171_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a_a171,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a_a170_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000C",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a_a170,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a_a169_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a_a169,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a_a168_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "000A",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a_a168,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a_a167_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a_a167,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a_a166_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00AF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a_a166,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a_a165_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0005",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a_a165,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a_a164_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a_a164,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a);

No_Reset_Cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_aagb_out = No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a # Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_aagb_out);

det_sat_areg0_I : apex20ke_lcell
-- Equation(s):
-- det_sat_areg0 = DFFE(ms100tc_acombout & No_Reset_Cmp_acomparator_acmp_end_aagb_out & (det_sat_areg0 # DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out) # !ms100tc_acombout & det_sat_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EA0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => det_sat_areg0,
	datab => DOUT_FS_COMPARE_acomparator_acmp_end_aagb_out,
	datac => ms100tc_acombout,
	datad => No_Reset_Cmp_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => det_sat_areg0);

CPS_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(20),
	combout => CPS_a20_a_acombout);

CPS_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(19),
	combout => CPS_a19_a_acombout);

hc_rate_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(18),
	combout => hc_rate_a18_a_acombout);

CPS_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(17),
	combout => CPS_a17_a_acombout);

hc_rate_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(16),
	combout => hc_rate_a16_a_acombout);

hc_rate_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(15),
	combout => hc_rate_a15_a_acombout);

hc_rate_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(14),
	combout => hc_rate_a14_a_acombout);

CPS_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(13),
	combout => CPS_a13_a_acombout);

CPS_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(12),
	combout => CPS_a12_a_acombout);

hc_rate_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(11),
	combout => hc_rate_a11_a_acombout);

hc_rate_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(10),
	combout => hc_rate_a10_a_acombout);

hc_rate_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(9),
	combout => hc_rate_a9_a_acombout);

hc_rate_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(8),
	combout => hc_rate_a8_a_acombout);

hc_rate_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(7),
	combout => hc_rate_a7_a_acombout);

hc_rate_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(6),
	combout => hc_rate_a6_a_acombout);

hc_rate_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(5),
	combout => hc_rate_a5_a_acombout);

CPS_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(4),
	combout => CPS_a4_a_acombout);

CPS_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(3),
	combout => CPS_a3_a_acombout);

hc_rate_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(2),
	combout => hc_rate_a2_a_acombout);

CPS_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(1),
	combout => CPS_a1_a_acombout);

hc_rate_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(0),
	combout => hc_rate_a0_a_acombout);

cps_cmp_acomparator_acmp_end_alcarry_a0_a_a1433_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(CPS_a0_a_acombout & !hc_rate_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a0_a_acombout,
	datab => hc_rate_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a0_a_a1433,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a0_a);

cps_cmp_acomparator_acmp_end_alcarry_a1_a_a1432_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(hc_rate_a1_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a0_a # !CPS_a1_a_acombout) # !hc_rate_a1_a_acombout & !CPS_a1_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a1_a_acombout,
	datab => CPS_a1_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a1_a_a1432,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a1_a);

cps_cmp_acomparator_acmp_end_alcarry_a2_a_a1431_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(CPS_a2_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a1_a # !hc_rate_a2_a_acombout) # !CPS_a2_a_acombout & !hc_rate_a2_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a2_a_acombout,
	datab => hc_rate_a2_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a2_a_a1431,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a2_a);

cps_cmp_acomparator_acmp_end_alcarry_a3_a_a1430_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(hc_rate_a3_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a2_a # !CPS_a3_a_acombout) # !hc_rate_a3_a_acombout & !CPS_a3_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a3_a_acombout,
	datab => CPS_a3_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a3_a_a1430,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a3_a);

cps_cmp_acomparator_acmp_end_alcarry_a4_a_a1429_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(hc_rate_a4_a_acombout & CPS_a4_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a3_a # !hc_rate_a4_a_acombout & (CPS_a4_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a4_a_acombout,
	datab => CPS_a4_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a4_a_a1429,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a4_a);

cps_cmp_acomparator_acmp_end_alcarry_a5_a_a1428_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(CPS_a5_a_acombout & hc_rate_a5_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a4_a # !CPS_a5_a_acombout & (hc_rate_a5_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a5_a_acombout,
	datab => hc_rate_a5_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a5_a_a1428,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a5_a);

cps_cmp_acomparator_acmp_end_alcarry_a6_a_a1427_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(CPS_a6_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a5_a # !hc_rate_a6_a_acombout) # !CPS_a6_a_acombout & !hc_rate_a6_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a6_a_acombout,
	datab => hc_rate_a6_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a6_a_a1427,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a6_a);

cps_cmp_acomparator_acmp_end_alcarry_a7_a_a1426_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(CPS_a7_a_acombout & hc_rate_a7_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a6_a # !CPS_a7_a_acombout & (hc_rate_a7_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a7_a_acombout,
	datab => hc_rate_a7_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a7_a_a1426,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a7_a);

cps_cmp_acomparator_acmp_end_alcarry_a8_a_a1425_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(CPS_a8_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a7_a # !hc_rate_a8_a_acombout) # !CPS_a8_a_acombout & !hc_rate_a8_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a8_a_acombout,
	datab => hc_rate_a8_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a8_a_a1425,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a8_a);

cps_cmp_acomparator_acmp_end_alcarry_a9_a_a1424_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(CPS_a9_a_acombout & hc_rate_a9_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a8_a # !CPS_a9_a_acombout & (hc_rate_a9_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a9_a_acombout,
	datab => hc_rate_a9_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a9_a_a1424,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a9_a);

cps_cmp_acomparator_acmp_end_alcarry_a10_a_a1423_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(CPS_a10_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a9_a # !hc_rate_a10_a_acombout) # !CPS_a10_a_acombout & !hc_rate_a10_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a10_a_acombout,
	datab => hc_rate_a10_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a10_a_a1423,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a10_a);

cps_cmp_acomparator_acmp_end_alcarry_a11_a_a1422_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(CPS_a11_a_acombout & hc_rate_a11_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a10_a # !CPS_a11_a_acombout & (hc_rate_a11_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a11_a_acombout,
	datab => hc_rate_a11_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a11_a_a1422,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a11_a);

cps_cmp_acomparator_acmp_end_alcarry_a12_a_a1421_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(hc_rate_a12_a_acombout & CPS_a12_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a11_a # !hc_rate_a12_a_acombout & (CPS_a12_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a12_a_acombout,
	datab => CPS_a12_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a12_a_a1421,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a12_a);

cps_cmp_acomparator_acmp_end_alcarry_a13_a_a1420_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(hc_rate_a13_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a12_a # !CPS_a13_a_acombout) # !hc_rate_a13_a_acombout & !CPS_a13_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a13_a_acombout,
	datab => CPS_a13_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a13_a_a1420,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a13_a);

cps_cmp_acomparator_acmp_end_alcarry_a14_a_a1419_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(CPS_a14_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a13_a # !hc_rate_a14_a_acombout) # !CPS_a14_a_acombout & !hc_rate_a14_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a14_a_acombout,
	datab => hc_rate_a14_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a14_a_a1419,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a14_a);

cps_cmp_acomparator_acmp_end_alcarry_a15_a_a1418_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a15_a = CARRY(CPS_a15_a_acombout & hc_rate_a15_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a14_a # !CPS_a15_a_acombout & (hc_rate_a15_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a15_a_acombout,
	datab => hc_rate_a15_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a15_a_a1418,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a15_a);

cps_cmp_acomparator_acmp_end_alcarry_a16_a_a1417_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a16_a = CARRY(CPS_a16_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a15_a # !hc_rate_a16_a_acombout) # !CPS_a16_a_acombout & !hc_rate_a16_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a16_a_acombout,
	datab => hc_rate_a16_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a16_a_a1417,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a16_a);

cps_cmp_acomparator_acmp_end_alcarry_a17_a_a1416_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a17_a = CARRY(hc_rate_a17_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a16_a # !CPS_a17_a_acombout) # !hc_rate_a17_a_acombout & !CPS_a17_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a17_a_acombout,
	datab => CPS_a17_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a17_a_a1416,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a17_a);

cps_cmp_acomparator_acmp_end_alcarry_a18_a_a1415_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a18_a = CARRY(CPS_a18_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a17_a # !hc_rate_a18_a_acombout) # !CPS_a18_a_acombout & !hc_rate_a18_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a18_a_acombout,
	datab => hc_rate_a18_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a18_a_a1415,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a18_a);

cps_cmp_acomparator_acmp_end_alcarry_a19_a_a1414_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a19_a = CARRY(hc_rate_a19_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a18_a # !CPS_a19_a_acombout) # !hc_rate_a19_a_acombout & !CPS_a19_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a19_a_acombout,
	datab => CPS_a19_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_alcarry_a19_a_a1414,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a19_a);

cps_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_aagb_out = hc_rate_a20_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a19_a & CPS_a20_a_acombout) # !hc_rate_a20_a_acombout & (CPS_a20_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5F05",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a20_a_acombout,
	datad => CPS_a20_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_aagb_out);

hc_cnt_cen_a9_I : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cen_a9 = ms100tc_acombout & cps_cmp_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ms100tc_acombout,
	datad => cps_cmp_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_cnt_cen_a9);

hc_cnt_clr_a0_I : apex20ke_lcell
-- Equation(s):
-- hc_cnt_clr_a0 = ms100tc_acombout & !cps_cmp_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => ms100tc_acombout,
	datad => cps_cmp_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_cnt_clr_a0);

hc_cnt_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cen_a9 $ (hc_cnt_cntr_awysi_counter_asload_path_a0_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a9,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a1_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a2_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a2_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a3_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a4_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a4_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a5_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a6_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a6_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a7_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a8_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a9,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a9_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a9,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a10_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a10_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a11_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a12_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a12_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a13_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a14_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a14_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a15_a $ (hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT & hc_cnt_cen_a9), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AAA",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	datad => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a15_a);

hc_threshold_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(9),
	combout => hc_threshold_a9_a_acombout);

hc_threshold_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(8),
	combout => hc_threshold_a8_a_acombout);

hc_count_cmp_acomparator_acmp_end_alcarry_a0_a_a1058_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(!hc_threshold_a0_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0044",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a0_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a0_a_a1058,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a0_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a1_a_a1057_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(hc_threshold_a1_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a0_a # !hc_cnt_cntr_awysi_counter_asload_path_a1_a) # !hc_threshold_a1_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a1_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a1_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a1_a_a1057,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a1_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a2_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(hc_threshold_a2_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a2_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a1_a # !hc_threshold_a2_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a2_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a2_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a2_a_a1056,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a2_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a3_a_a1055_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(hc_threshold_a3_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a2_a # !hc_cnt_cntr_awysi_counter_asload_path_a3_a) # !hc_threshold_a3_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a3_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a3_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a3_a_a1055,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a3_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a4_a_a1054_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(hc_threshold_a4_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a4_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a3_a # !hc_threshold_a4_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a4_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a4_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a4_a_a1054,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a4_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a5_a_a1053_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(hc_threshold_a5_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a4_a # !hc_cnt_cntr_awysi_counter_asload_path_a5_a) # !hc_threshold_a5_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a5_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a5_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a5_a_a1053,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a5_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a6_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(hc_threshold_a6_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a6_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a5_a # !hc_threshold_a6_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a6_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a6_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a6_a_a1052,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a6_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a7_a_a1051_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(hc_threshold_a7_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a6_a # !hc_cnt_cntr_awysi_counter_asload_path_a7_a) # !hc_threshold_a7_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a7_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a7_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a7_a_a1051,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a7_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a8_a_a1050_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & (!hc_count_cmp_acomparator_acmp_end_alcarry_a7_a # !hc_threshold_a8_a_acombout) # !hc_cnt_cntr_awysi_counter_asload_path_a8_a & !hc_threshold_a8_a_acombout 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	datab => hc_threshold_a8_a_acombout,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a8_a_a1050,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a8_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a9_a_a1049_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a9_a & hc_threshold_a9_a_acombout & !hc_count_cmp_acomparator_acmp_end_alcarry_a8_a # !hc_cnt_cntr_awysi_counter_asload_path_a9_a & (hc_threshold_a9_a_acombout # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	datab => hc_threshold_a9_a_acombout,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a9_a_a1049,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a9_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a10_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(hc_threshold_a10_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a10_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a9_a # !hc_threshold_a10_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a10_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a10_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a10_a_a1048,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a10_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a11_a_a1047_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(hc_threshold_a11_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a10_a # !hc_cnt_cntr_awysi_counter_asload_path_a11_a) # !hc_threshold_a11_a_acombout & 
-- !hc_cnt_cntr_awysi_counter_asload_path_a11_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a11_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a11_a_a1047,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a11_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a12_a_a1046_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(hc_threshold_a12_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a12_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a11_a # !hc_threshold_a12_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a12_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a12_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a12_a_a1046,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a12_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a13_a_a1045_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(hc_threshold_a13_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a12_a # !hc_cnt_cntr_awysi_counter_asload_path_a13_a) # !hc_threshold_a13_a_acombout & 
-- !hc_cnt_cntr_awysi_counter_asload_path_a13_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a13_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a13_a_a1045,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a13_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a14_a_a1044_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(hc_threshold_a14_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a14_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a13_a # !hc_threshold_a14_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a14_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a14_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_alcarry_a14_a_a1044,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a14_a);

hc_count_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_aagb_out = hc_threshold_a15_a_acombout & (hc_count_cmp_acomparator_acmp_end_alcarry_a14_a & hc_cnt_cntr_awysi_counter_asload_path_a15_a) # !hc_threshold_a15_a_acombout & (hc_count_cmp_acomparator_acmp_end_alcarry_a14_a # 
-- hc_cnt_cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F550",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a15_a_acombout,
	datad => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_aagb_out);

Hi_Cnt_areg0_I : apex20ke_lcell
-- Equation(s):
-- Hi_Cnt_areg0 = DFFE(cps_cmp_acomparator_acmp_end_aagb_out & ms100tc_acombout & hc_count_cmp_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => cps_cmp_acomparator_acmp_end_aagb_out,
	datac => ms100tc_acombout,
	datad => hc_count_cmp_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Hi_Cnt_areg0);

NSlope_Cmp_out_areg0_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Cmp_out_areg0 = DFFE(NSlope_Compare_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => NSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => NSlope_Cmp_out_areg0);

pslope_cmp_out_areg0_I : apex20ke_lcell
-- Equation(s):
-- pslope_cmp_out_areg0 = DFFE(PSlope_Compare_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => PSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_cmp_out_areg0);

Reset_Time_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a0_a = DFFE(No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a0_a);

Reset_Time_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a1_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a1_a);

Reset_Time_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a2_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a2_a);

Reset_Time_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a3_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a3_a);

Reset_Time_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a4_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a4_a);

Reset_Time_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a5_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a5_a);

Reset_Time_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a6_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a6_a);

Reset_Time_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a7_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a7_a);

Reset_Time_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a8_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a8_a);

Reset_Time_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a9_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a9_a);

Reset_Time_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a10_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a10_a);

Reset_Time_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a11_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a11_a);

Reset_Time_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a12_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a12_a);

Reset_Time_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a13_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a13_a);

Reset_Time_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a14_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a14_a);

Reset_Time_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a15_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a15_a);

Reset_Time_FF_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a16_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a16_a);

Reset_Time_FF_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a17_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a17_a);

Reset_Time_FF_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a18_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a18_a);

Reset_Time_FF_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a19_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a19_a);

Reset_Time_FF_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a20_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a20_a);

Reset_Time_FF_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a21_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a21_a);

Reset_Time_FF_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a22_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a22_a);

Reset_Time_FF_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a23_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a23_a);

Reset_Time_FF_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a24_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a24_a);

Reset_Time_FF_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a25_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a25_a);

Reset_Time_FF_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a26_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a26_a);

Reset_Time_FF_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a27_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a27_a);

Reset_Time_FF_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a28_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a28_a);

Reset_Time_FF_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a29_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a29_a);

Reset_Time_FF_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a30_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a30_a);

Reset_Time_FF_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a31_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a31_a);

Reset_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY()

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AF0",
	operation_mode => "qfbk_counter",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a0_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

Reset_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a1_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a1_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a1_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

Reset_End_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End = DFFE(Equal_a213 & reset_Del & !Reset_areg0 # !Equal_a213 & (Reset_End # reset_Del & !Reset_areg0), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5D0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a213,
	datab => reset_Del,
	datac => Reset_areg0,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End);

Reset_End_Cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a1_a = DFFE(Reset_End & (Reset_End_Cnt_a0_a $ Reset_End_Cnt_a1_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5A00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a0_a,
	datac => Reset_End_Cnt_a1_a,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a1_a);

Reset_End_Cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a2_a = DFFE(Reset_End & (Reset_End_Cnt_a2_a $ (Reset_End_Cnt_a0_a & Reset_End_Cnt_a1_a)), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6C00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a0_a,
	datab => Reset_End_Cnt_a2_a,
	datac => Reset_End_Cnt_a1_a,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a2_a);

Reset_End_Cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a0_a = DFFE(!Reset_End_Cnt_a0_a & Reset_End, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_End_Cnt_a0_a,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a0_a);

Reset_End_Cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a3_a = DFFE(Reset_End & (Reset_End_Cnt_a3_a $ (Equal_a215 & Reset_End_Cnt_a2_a)), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "7800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a215,
	datab => Reset_End_Cnt_a2_a,
	datac => Reset_End_Cnt_a3_a,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a3_a);

Equal_a213_I : apex20ke_lcell
-- Equation(s):
-- Equal_a213 = Reset_End_Cnt_a1_a & Reset_End_Cnt_a2_a & Reset_End_Cnt_a0_a & Reset_End_Cnt_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a1_a,
	datab => Reset_End_Cnt_a2_a,
	datac => Reset_End_Cnt_a0_a,
	datad => Reset_End_Cnt_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a213);

reset_width_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a0_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a1_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a0_a);

Reset_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a2_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a2_a & !Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a2_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a2_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

reset_width_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a1_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a2_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a2_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a1_a);

Reset_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a3_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a3_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a3_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

reset_width_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a2_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a3_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a2_a);

Reset_Cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a4_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a4_a & !Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a4_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a4_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

reset_width_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a3_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a4_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a4_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a3_a);

Reset_Cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a5_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a5_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a5_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

reset_width_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a4_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a5_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a5_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a4_a);

Reset_Cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a6_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a6_a & !Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a6_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a6_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

reset_width_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a5_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a6_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a6_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a5_a);

Reset_Cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a7_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a7_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a7_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

reset_width_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a6_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a7_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a7_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a6_a);

Reset_Cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a8_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a8_a & (!Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a8_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a8_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

reset_width_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a7_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a8_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a8_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a7_a);

Reset_Cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a9_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a9_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a9_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

reset_width_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a8_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a9_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a9_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a8_a);

Reset_Cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a10_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a10_a & (!Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a10_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a10_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

reset_width_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a9_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a10_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a10_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a9_a);

Reset_Cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a11_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a11_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a11_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

reset_width_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a10_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a11_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a11_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a10_a);

Reset_Cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a12_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a12_a & (!Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a12_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a12_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

reset_width_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a11_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a12_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a12_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a11_a);

Reset_Cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a13_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a13_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a13_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

reset_width_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a12_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a13_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Reset_Cntr_awysi_counter_asload_path_a13_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a12_a);

Reset_Cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a14_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a14_a & (!Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a14_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a14_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

reset_width_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a13_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a14_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a14_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a13_a);

Reset_Cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a15_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3FC0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Reset_areg0,
	datad => Reset_Cntr_awysi_counter_asload_path_a15_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a15_a);

reset_width_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a14_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a15_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Equal_a213)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a15_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Equal_a213,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a14_a);

AD_CLR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AD_CLR);

det_sat_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => det_sat_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_det_sat);

Hi_Cnt_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Hi_Cnt_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Hi_Cnt);

Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset);

NSlope_Cmp_out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => NSlope_Cmp_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NSlope_Cmp_out);

pslope_cmp_out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => pslope_cmp_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_pslope_cmp_out);

Reset_time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(0));

Reset_time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(1));

Reset_time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(2));

Reset_time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(3));

Reset_time_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(4));

Reset_time_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(5));

Reset_time_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(6));

Reset_time_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(7));

Reset_time_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(8));

Reset_time_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(9));

Reset_time_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(10));

Reset_time_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(11));

Reset_time_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(12));

Reset_time_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(13));

Reset_time_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(14));

Reset_time_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(15));

Reset_time_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(16));

Reset_time_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(17));

Reset_time_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(18));

Reset_time_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(19));

Reset_time_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(20));

Reset_time_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(21));

Reset_time_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(22));

Reset_time_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(23));

Reset_time_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(24));

Reset_time_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(25));

Reset_time_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(26));

Reset_time_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(27));

Reset_time_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(28));

Reset_time_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(29));

Reset_time_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(30));

Reset_time_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a31_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(31));

Reset_Width_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(0));

Reset_Width_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(1));

Reset_Width_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(2));

Reset_Width_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(3));

Reset_Width_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(4));

Reset_Width_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(5));

Reset_Width_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(6));

Reset_Width_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(7));

Reset_Width_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(8));

Reset_Width_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(9));

Reset_Width_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(10));

Reset_Width_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(11));

Reset_Width_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(12));

Reset_Width_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(13));

Reset_Width_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(14));

Reset_Width_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(15));
END structure;


