-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic       
-- functions, and any output files any of the foregoing           
-- (including device programming or simulation files), and any    
-- associated documentation or information are expressly subject  
-- to the terms and conditions of the Altera Program License      
-- Subscription Agreement, Altera MegaCore Function License       
-- Agreement, or other applicable license agreement, including,   
-- without limitation, that your use is for the sole purpose of   
-- programming logic devices manufactured by Altera and sold by   
-- Altera or its authorized distributors.  Please refer to the    
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.0 Build 148 04/26/2005 Service Pack 0.21 SJ Full Version"

-- DATE "06/21/2005 14:30:38"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_reset IS
    PORT (
	We_Clr_AD : IN std_logic;
	CLK : IN std_logic;
	IMR : IN std_logic;
	ms100tc : IN std_logic;
	CPS : IN std_logic_vector(20 DOWNTO 0);
	hc_rate : IN std_logic_vector(20 DOWNTO 0);
	hc_threshold : IN std_logic_vector(15 DOWNTO 0);
	Fir_Mr : IN std_logic;
	Dout : IN std_logic_vector(14 DOWNTO 0);
	AD_CLR : OUT std_logic;
	det_sat : OUT std_logic;
	Hi_Cnt : OUT std_logic;
	Reset : OUT std_logic;
	NSlope_Cmp_out : OUT std_logic;
	pslope_cmp_out : OUT std_logic;
	Reset_time : OUT std_logic_vector(31 DOWNTO 0);
	Reset_Width : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_reset;

ARCHITECTURE structure OF fir_reset IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_We_Clr_AD : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_ms100tc : std_logic;
SIGNAL ww_CPS : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_hc_rate : std_logic_vector(20 DOWNTO 0);
SIGNAL ww_hc_threshold : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Fir_Mr : std_logic;
SIGNAL ww_Dout : std_logic_vector(14 DOWNTO 0);
SIGNAL ww_AD_CLR : std_logic;
SIGNAL ww_det_sat : std_logic;
SIGNAL ww_Hi_Cnt : std_logic;
SIGNAL ww_Reset : std_logic;
SIGNAL ww_NSlope_Cmp_out : std_logic;
SIGNAL ww_pslope_cmp_out : std_logic;
SIGNAL ww_Reset_time : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_Reset_Width : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus : std_logic_vector(15 DOWNTO 0);
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a11_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a7_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a5_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a3_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a14_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a13_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a12_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a10_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a9_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a8_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a6_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a4_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a2_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a1_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM1_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Dly_RAM2_asram_asegment_a0_a_a0_a_modesel : std_logic_vector(17 DOWNTO 0) := "010101000100010101";
SIGNAL Ad_Clr_Cnt_a0_a : std_logic;
SIGNAL Ad_Clr_Cnt_a7_a : std_logic;
SIGNAL Ad_Clr_Cnt_a5_a : std_logic;
SIGNAL reduce_nor_a126 : std_logic;
SIGNAL Reset_End_Cnt_a2_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a_a164 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a19_a_a939 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a14_a_a694 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a146 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a27 : std_logic;
SIGNAL us1_cnt_a3_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a_a165 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a18_a_a940 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a13_a_a695 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a147 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a28 : std_logic;
SIGNAL add_a517 : std_logic;
SIGNAL add_a521 : std_logic;
SIGNAL add_a519 : std_logic;
SIGNAL add_a527 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a_a166 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a17_a_a941 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a12_a_a696 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a18_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a148 : std_logic;
SIGNAL fir_mr_str_cnt_a4_a : std_logic;
SIGNAL fir_mr_str_cnt_a3_a_a68 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a29 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a_a167 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a16_a_a942 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a11_a_a697 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a149 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a30 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a_a168 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a15_a_a943 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a10_a_a698 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a14_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a150 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a31 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a_a169 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a14_a_a944 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a9_a_a699 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a151 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a32 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a_a170 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a13_a_a945 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a8_a_a700 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a152 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a33 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a_a171 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a12_a_a946 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a7_a_a701 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a153 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a34 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a_a172 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a11_a_a947 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a6_a_a702 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a154 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a35 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a_a173 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a10_a_a948 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a5_a_a703 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a11_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a155 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a36 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a_a174 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a9_a_a949 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a4_a_a704 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a13_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a156 : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a37 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a_a175 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a8_a_a950 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a3_a_a705 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a12_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a157 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a_a176 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a7_a_a951 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a2_a_a706 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a11_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a7_a_a158 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a_a177 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a6_a_a952 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a1_a_a707 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a7_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a10_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a7_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a6_a_a159 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a_a178 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a5_a_a953 : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a0_a_a708 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a9_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388 : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a5_a_a160 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a_a179 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a4_a_a954 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a5_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a8_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a5_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a4_a_a161 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a_a180 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a3_a_a955 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a7_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a4_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a3_a_a162 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a_a181 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a2_a_a956 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a3_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a6_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a3_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a2_a_a163 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a_a182 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a1_a_a957 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a5_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a2_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a1_a_a164 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a_a183 : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a0_a_a958 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a4_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a1_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a_a184 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a379 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a3_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a_a185 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a : std_logic;
SIGNAL Dly_RAM2_asram_aq_a2_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a_a186 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a1_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a_a187 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a_a188 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a_a189 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a_a190 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a_a191 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a_a192 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a_a193 : std_logic;
SIGNAL CPS_a19_a_acombout : std_logic;
SIGNAL hc_threshold_a14_a_acombout : std_logic;
SIGNAL CPS_a18_a_acombout : std_logic;
SIGNAL hc_threshold_a13_a_acombout : std_logic;
SIGNAL CPS_a17_a_acombout : std_logic;
SIGNAL hc_threshold_a12_a_acombout : std_logic;
SIGNAL hc_rate_a16_a_acombout : std_logic;
SIGNAL hc_threshold_a11_a_acombout : std_logic;
SIGNAL hc_rate_a15_a_acombout : std_logic;
SIGNAL hc_threshold_a10_a_acombout : std_logic;
SIGNAL hc_rate_a14_a_acombout : std_logic;
SIGNAL hc_rate_a13_a_acombout : std_logic;
SIGNAL hc_rate_a12_a_acombout : std_logic;
SIGNAL hc_threshold_a7_a_acombout : std_logic;
SIGNAL Dout_a13_a_acombout : std_logic;
SIGNAL hc_rate_a11_a_acombout : std_logic;
SIGNAL hc_threshold_a6_a_acombout : std_logic;
SIGNAL hc_rate_a10_a_acombout : std_logic;
SIGNAL hc_threshold_a5_a_acombout : std_logic;
SIGNAL Dout_a11_a_acombout : std_logic;
SIGNAL hc_rate_a9_a_acombout : std_logic;
SIGNAL hc_threshold_a4_a_acombout : std_logic;
SIGNAL Dout_a10_a_acombout : std_logic;
SIGNAL hc_rate_a8_a_acombout : std_logic;
SIGNAL hc_threshold_a3_a_acombout : std_logic;
SIGNAL Dout_a9_a_acombout : std_logic;
SIGNAL hc_rate_a7_a_acombout : std_logic;
SIGNAL hc_threshold_a2_a_acombout : std_logic;
SIGNAL CPS_a6_a_acombout : std_logic;
SIGNAL hc_threshold_a1_a_acombout : std_logic;
SIGNAL Dout_a7_a_acombout : std_logic;
SIGNAL hc_rate_a5_a_acombout : std_logic;
SIGNAL hc_threshold_a0_a_acombout : std_logic;
SIGNAL hc_rate_a4_a_acombout : std_logic;
SIGNAL Dout_a5_a_acombout : std_logic;
SIGNAL hc_rate_a3_a_acombout : std_logic;
SIGNAL CPS_a2_a_acombout : std_logic;
SIGNAL Dout_a3_a_acombout : std_logic;
SIGNAL CPS_a1_a_acombout : std_logic;
SIGNAL Dout_a2_a_acombout : std_logic;
SIGNAL hc_rate_a0_a_acombout : std_logic;
SIGNAL Dout_a1_a_acombout : std_logic;
SIGNAL CLK_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL We_Clr_AD_acombout : std_logic;
SIGNAL Ad_Clr_Cnt_a0_a_a234 : std_logic;
SIGNAL Ad_Clr_Cnt_a1_a : std_logic;
SIGNAL Ad_Clr_Cnt_a1_a_a231 : std_logic;
SIGNAL Ad_Clr_Cnt_a2_a_a228 : std_logic;
SIGNAL Ad_Clr_Cnt_a3_a : std_logic;
SIGNAL Ad_Clr_Cnt_a3_a_a225 : std_logic;
SIGNAL Ad_Clr_Cnt_a4_a : std_logic;
SIGNAL Ad_Clr_Cnt_a4_a_a246 : std_logic;
SIGNAL Ad_Clr_Cnt_a5_a_a243 : std_logic;
SIGNAL Ad_Clr_Cnt_a6_a : std_logic;
SIGNAL Ad_Clr_Cnt_a6_a_a240 : std_logic;
SIGNAL Ad_Clr_Cnt_a7_a_a237 : std_logic;
SIGNAL Ad_Clr_Cnt_a8_a : std_logic;
SIGNAL Ad_Clr_Cnt_a8_a_a258 : std_logic;
SIGNAL Ad_Clr_Cnt_a9_a_a255 : std_logic;
SIGNAL Ad_Clr_Cnt_a10_a_a252 : std_logic;
SIGNAL Ad_Clr_Cnt_a11_a_a249 : std_logic;
SIGNAL Ad_Clr_Cnt_a12_a : std_logic;
SIGNAL Ad_Clr_Cnt_a12_a_a261 : std_logic;
SIGNAL Ad_Clr_Cnt_a13_a : std_logic;
SIGNAL reduce_nor_a128 : std_logic;
SIGNAL Ad_Clr_Cnt_a2_a : std_logic;
SIGNAL reduce_nor_a125 : std_logic;
SIGNAL Ad_Clr_Cnt_a9_a : std_logic;
SIGNAL Ad_Clr_Cnt_a11_a : std_logic;
SIGNAL Ad_Clr_Cnt_a10_a : std_logic;
SIGNAL reduce_nor_a127 : std_logic;
SIGNAL reduce_nor_a129 : std_logic;
SIGNAL AD_CLR_areg0 : std_logic;
SIGNAL add_a531 : std_logic;
SIGNAL us1_cnt_a0_a : std_logic;
SIGNAL fir_mr_str_cnt_a0_a : std_logic;
SIGNAL fir_mr_str_cnt_a0_a_a56 : std_logic;
SIGNAL fir_mr_str_cnt_a1_a : std_logic;
SIGNAL fir_mr_str_cnt_a1_a_a59 : std_logic;
SIGNAL fir_mr_str_cnt_a2_a : std_logic;
SIGNAL fir_mr_str_cnt_a2_a_a65 : std_logic;
SIGNAL fir_mr_str_cnt_a3_a : std_logic;
SIGNAL fir_mr_str_a35 : std_logic;
SIGNAL Fir_Mr_acombout : std_logic;
SIGNAL fir_mr_str : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL wadr_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a : std_logic;
SIGNAL Dly_RAM2_asram_asegment_a0_a_a14_a_a0 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acout_a1_a : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44 : std_logic;
SIGNAL radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 : std_logic;
SIGNAL Dly_RAM1_asram_aq_a14_a : std_logic;
SIGNAL Dout_a14_a_acombout : std_logic;
SIGNAL Dly_RAM1_asram_aq_a13_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a12_a : std_logic;
SIGNAL Dout_a12_a_acombout : std_logic;
SIGNAL Dly_RAM1_asram_aq_a10_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a9_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a8_a : std_logic;
SIGNAL Dout_a8_a_acombout : std_logic;
SIGNAL Dly_RAM1_asram_aq_a6_a : std_logic;
SIGNAL Dout_a6_a_acombout : std_logic;
SIGNAL Dly_RAM1_asram_aq_a4_a : std_logic;
SIGNAL Dout_a4_a_acombout : std_logic;
SIGNAL Dly_RAM1_asram_aq_a2_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a1_a : std_logic;
SIGNAL Dly_RAM1_asram_aq_a0_a : std_logic;
SIGNAL Dout_a0_a_acombout : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acout_a0_a : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a321 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371 : std_logic;
SIGNAL SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375 : std_logic;
SIGNAL Dly_RAM2_asram_aq_a0_a : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426 : std_logic;
SIGNAL Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a338 : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a20_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a17_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a16_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a15_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a14_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a13_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a12_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a11_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a10_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a9_a : std_logic;
SIGNAL Dout_Diff_FF_adffs_a8_a : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a6_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL NSlope_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336 : std_logic;
SIGNAL Dout_Diff_FF_adffs_a19_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL PSlope_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Reset_areg0 : std_logic;
SIGNAL us1_cnt_a4_a : std_logic;
SIGNAL add_a533 : std_logic;
SIGNAL add_a523 : std_logic;
SIGNAL us1_cnt_a1_a : std_logic;
SIGNAL add_a525 : std_logic;
SIGNAL add_a515 : std_logic;
SIGNAL us1_cnt_a2_a : std_logic;
SIGNAL reduce_nor_a130 : std_logic;
SIGNAL reset_Time_Cen_a0 : std_logic;
SIGNAL reset_Del : std_logic;
SIGNAL Reset_LEd_a0 : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL ms100tc_acombout : std_logic;
SIGNAL det_sat_areg0 : std_logic;
SIGNAL hc_rate_a20_a_acombout : std_logic;
SIGNAL CPS_a20_a_acombout : std_logic;
SIGNAL hc_rate_a19_a_acombout : std_logic;
SIGNAL hc_rate_a18_a_acombout : std_logic;
SIGNAL hc_rate_a17_a_acombout : std_logic;
SIGNAL CPS_a16_a_acombout : std_logic;
SIGNAL CPS_a15_a_acombout : std_logic;
SIGNAL CPS_a14_a_acombout : std_logic;
SIGNAL CPS_a13_a_acombout : std_logic;
SIGNAL CPS_a12_a_acombout : std_logic;
SIGNAL CPS_a11_a_acombout : std_logic;
SIGNAL CPS_a10_a_acombout : std_logic;
SIGNAL CPS_a9_a_acombout : std_logic;
SIGNAL CPS_a8_a_acombout : std_logic;
SIGNAL CPS_a7_a_acombout : std_logic;
SIGNAL hc_rate_a6_a_acombout : std_logic;
SIGNAL CPS_a5_a_acombout : std_logic;
SIGNAL CPS_a4_a_acombout : std_logic;
SIGNAL CPS_a3_a_acombout : std_logic;
SIGNAL hc_rate_a2_a_acombout : std_logic;
SIGNAL hc_rate_a1_a_acombout : std_logic;
SIGNAL CPS_a0_a_acombout : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL cps_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL hc_cnt_cen_a9 : std_logic;
SIGNAL hc_cnt_clr_a0 : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL hc_threshold_a15_a_acombout : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL hc_threshold_a9_a_acombout : std_logic;
SIGNAL hc_threshold_a8_a_acombout : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL hc_cnt_cntr_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL hc_count_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Hi_Cnt_areg0 : std_logic;
SIGNAL NSlope_Cmp_out_areg0 : std_logic;
SIGNAL pslope_cmp_out_areg0 : std_logic;
SIGNAL No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194 : std_logic;
SIGNAL Reset_Time_FF_adffs_a0_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a1_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a2_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a3_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a4_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a5_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a6_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a7_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a8_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a9_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a10_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a11_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a12_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a13_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a14_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a15_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a16_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a17_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a18_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a19_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a20_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a21_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a22_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a23_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a24_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a25_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a26_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a27_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a28_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a29_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a30_a : std_logic;
SIGNAL Reset_Time_FF_adffs_a31_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL Reset_End : std_logic;
SIGNAL Reset_End_Cnt_a0_a : std_logic;
SIGNAL Reset_End_Cnt_a1_a : std_logic;
SIGNAL add_a514 : std_logic;
SIGNAL Reset_End_Cnt_a3_a : std_logic;
SIGNAL reduce_nor_a2 : std_logic;
SIGNAL reset_width_ff_adffs_a0_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL reset_width_ff_adffs_a1_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL reset_width_ff_adffs_a2_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL reset_width_ff_adffs_a3_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL reset_width_ff_adffs_a4_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL reset_width_ff_adffs_a5_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL reset_width_ff_adffs_a6_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL reset_width_ff_adffs_a7_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL reset_width_ff_adffs_a8_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL reset_width_ff_adffs_a9_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL reset_width_ff_adffs_a10_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL reset_width_ff_adffs_a11_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL reset_width_ff_adffs_a12_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL reset_width_ff_adffs_a13_a : std_logic;
SIGNAL Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL Reset_Cntr_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL reset_width_ff_adffs_a14_a : std_logic;
SIGNAL ALT_INV_fir_mr_str : std_logic;
SIGNAL ALT_INV_IMR_acombout : std_logic;

BEGIN

ww_We_Clr_AD <= We_Clr_AD;
ww_CLK <= CLK;
ww_IMR <= IMR;
ww_ms100tc <= ms100tc;
ww_CPS <= CPS;
ww_hc_rate <= hc_rate;
ww_hc_threshold <= hc_threshold;
ww_Fir_Mr <= Fir_Mr;
ww_Dout <= Dout;
AD_CLR <= ww_AD_CLR;
det_sat <= ww_det_sat;
Hi_Cnt <= ww_Hi_Cnt;
Reset <= ww_Reset;
NSlope_Cmp_out <= ww_NSlope_Cmp_out;
pslope_cmp_out <= ww_pslope_cmp_out;
Reset_time <= ww_Reset_time;
Reset_Width <= ww_Reset_Width;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & wadr_cntr_awysi_counter_asload_path_a3_a & wadr_cntr_awysi_counter_asload_path_a2_a & wadr_cntr_awysi_counter_asload_path_a1_a & 
wadr_cntr_awysi_counter_asload_path_a0_a);

Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus <= (gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & gnd & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 & radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 & 
Dly_RAM2_asram_asegment_a0_a_a14_a_a0 & wadr_cntr_awysi_counter_asload_path_a0_a);
ALT_INV_fir_mr_str <= NOT fir_mr_str;
ALT_INV_IMR_acombout <= NOT IMR_acombout;

Ad_Clr_Cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a0_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a0_a $ AD_CLR_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a0_a_a234 = CARRY(Ad_Clr_Cnt_a0_a & AD_CLR_areg0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a0_a,
	datab => AD_CLR_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a0_a,
	cout => Ad_Clr_Cnt_a0_a_a234);

Ad_Clr_Cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a7_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a7_a $ (Ad_Clr_Cnt_a6_a_a240), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a7_a_a237 = CARRY(!Ad_Clr_Cnt_a6_a_a240 # !Ad_Clr_Cnt_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a7_a,
	cin => Ad_Clr_Cnt_a6_a_a240,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a7_a,
	cout => Ad_Clr_Cnt_a7_a_a237);

Ad_Clr_Cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a5_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a5_a $ (Ad_Clr_Cnt_a4_a_a246), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a5_a_a243 = CARRY(!Ad_Clr_Cnt_a4_a_a246 # !Ad_Clr_Cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a5_a,
	cin => Ad_Clr_Cnt_a4_a_a246,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a5_a,
	cout => Ad_Clr_Cnt_a5_a_a243);

reduce_nor_a126_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a126 = Ad_Clr_Cnt_a5_a # Ad_Clr_Cnt_a6_a # Ad_Clr_Cnt_a7_a # Ad_Clr_Cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a5_a,
	datab => Ad_Clr_Cnt_a6_a,
	datac => Ad_Clr_Cnt_a7_a,
	datad => Ad_Clr_Cnt_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a126);

Reset_End_Cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a2_a = DFFE(Reset_End & (Reset_End_Cnt_a2_a $ (Reset_End_Cnt_a0_a & Reset_End_Cnt_a1_a)), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "28A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End,
	datab => Reset_End_Cnt_a0_a,
	datac => Reset_End_Cnt_a2_a,
	datad => Reset_End_Cnt_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a2_a);

us1_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a3_a = DFFE(add_a519, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a519,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a3_a);

CPS_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(19),
	combout => CPS_a19_a_acombout);

hc_threshold_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(14),
	combout => hc_threshold_a14_a_acombout);

add_a515_I : apex20ke_lcell
-- Equation(s):
-- add_a515 = us1_cnt_a2_a $ !add_a525
-- add_a517 = CARRY(us1_cnt_a2_a & !add_a525)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a2_a,
	cin => add_a525,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a515,
	cout => add_a517);

add_a519_I : apex20ke_lcell
-- Equation(s):
-- add_a519 = us1_cnt_a3_a $ (add_a517)
-- add_a521 = CARRY(!add_a517 # !us1_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a3_a,
	cin => add_a517,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a519,
	cout => add_a521);

add_a527_I : apex20ke_lcell
-- Equation(s):
-- add_a527 = add_a521 $ !us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => us1_cnt_a4_a,
	cin => add_a521,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a527);

CPS_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(18),
	combout => CPS_a18_a_acombout);

hc_threshold_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(13),
	combout => hc_threshold_a13_a_acombout);

Dout_Diff_FF_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a18_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a18_a);

fir_mr_str_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a4_a = DFFE(fir_mr_str & fir_mr_str_cnt_a3_a_a68 $ !fir_mr_str_cnt_a4_a, GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => fir_mr_str_cnt_a4_a,
	cin => fir_mr_str_cnt_a3_a_a68,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a4_a);

fir_mr_str_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a3_a = DFFE(fir_mr_str & fir_mr_str_cnt_a3_a $ fir_mr_str_cnt_a2_a_a65, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a3_a_a68 = CARRY(!fir_mr_str_cnt_a2_a_a65 # !fir_mr_str_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a3_a,
	cin => fir_mr_str_cnt_a2_a_a65,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a3_a,
	cout => fir_mr_str_cnt_a3_a_a68);

CPS_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(17),
	combout => CPS_a17_a_acombout);

hc_threshold_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(12),
	combout => hc_threshold_a12_a_acombout);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344 = Dout_Diff_FF_adffs_a18_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346 = CARRY(Dout_Diff_FF_adffs_a18_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350) # !Dout_Diff_FF_adffs_a18_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a18_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a344,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346);

hc_rate_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(16),
	combout => hc_rate_a16_a_acombout);

hc_threshold_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(11),
	combout => hc_threshold_a11_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a14_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a14_a_modesel,
	dataout => Dly_RAM2_asram_aq_a14_a);

hc_rate_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(15),
	combout => hc_rate_a15_a_acombout);

hc_threshold_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(10),
	combout => hc_threshold_a10_a_acombout);

hc_rate_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(14),
	combout => hc_rate_a14_a_acombout);

hc_rate_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(13),
	combout => hc_rate_a13_a_acombout);

hc_rate_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(12),
	combout => hc_rate_a12_a_acombout);

hc_threshold_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(7),
	combout => hc_threshold_a7_a_acombout);

Dout_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(13),
	combout => Dout_a13_a_acombout);

hc_rate_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(11),
	combout => hc_rate_a11_a_acombout);

hc_threshold_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(6),
	combout => hc_threshold_a6_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a11_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a11_a_modesel,
	dataout => Dly_RAM1_asram_aq_a11_a);

hc_rate_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(10),
	combout => hc_rate_a10_a_acombout);

hc_threshold_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(5),
	combout => hc_threshold_a5_a_acombout);

Dout_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(11),
	combout => Dout_a11_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a13_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a13_a_modesel,
	dataout => Dly_RAM2_asram_aq_a13_a);

hc_rate_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(9),
	combout => hc_rate_a9_a_acombout);

hc_threshold_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(4),
	combout => hc_threshold_a4_a_acombout);

Dout_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(10),
	combout => Dout_a10_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a12_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a12_a_modesel,
	dataout => Dly_RAM2_asram_aq_a12_a);

hc_rate_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(8),
	combout => hc_rate_a8_a_acombout);

hc_threshold_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(3),
	combout => hc_threshold_a3_a_acombout);

Dout_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(9),
	combout => Dout_a9_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a11_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 11,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a11_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a11_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a11_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a11_a_modesel,
	dataout => Dly_RAM2_asram_aq_a11_a);

hc_rate_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(7),
	combout => hc_rate_a7_a_acombout);

hc_threshold_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(2),
	combout => hc_threshold_a2_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a7_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a7_a_modesel,
	dataout => Dly_RAM1_asram_aq_a7_a);

Dly_RAM2_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a10_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a10_a_modesel,
	dataout => Dly_RAM2_asram_aq_a10_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384 = Dout_Diff_FF_adffs_a8_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 = CARRY(Dout_Diff_FF_adffs_a8_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390) # !Dout_Diff_FF_adffs_a8_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a8_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386);

Dout_Diff_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a7_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a7_a);

CPS_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(6),
	combout => CPS_a6_a_acombout);

hc_threshold_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(1),
	combout => hc_threshold_a1_a_acombout);

Dout_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(7),
	combout => Dout_a7_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a9_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a9_a_modesel,
	dataout => Dly_RAM2_asram_aq_a9_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388 = Dout_Diff_FF_adffs_a7_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 = CARRY(Dout_Diff_FF_adffs_a7_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 # !Dout_Diff_FF_adffs_a7_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a7_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390);

hc_rate_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(5),
	combout => hc_rate_a5_a_acombout);

hc_threshold_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(0),
	combout => hc_threshold_a0_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a5_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a5_a_modesel,
	dataout => Dly_RAM1_asram_aq_a5_a);

Dly_RAM2_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a8_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a8_a_modesel,
	dataout => Dly_RAM2_asram_aq_a8_a);

Dout_Diff_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a5_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a5_a);

hc_rate_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(4),
	combout => hc_rate_a4_a_acombout);

Dout_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(5),
	combout => Dout_a5_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a7_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 7,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a7_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a7_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a7_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a7_a_modesel,
	dataout => Dly_RAM2_asram_aq_a7_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396 = Dout_Diff_FF_adffs_a5_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 = CARRY(Dout_Diff_FF_adffs_a5_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 # !Dout_Diff_FF_adffs_a5_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a5_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398);

Dout_Diff_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a4_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str,
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a4_a);

hc_rate_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(3),
	combout => hc_rate_a3_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a3_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a3_a_modesel,
	dataout => Dly_RAM1_asram_aq_a3_a);

Dly_RAM2_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a6_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a6_a_modesel,
	dataout => Dly_RAM2_asram_aq_a6_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400 = Dout_Diff_FF_adffs_a4_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 = CARRY(Dout_Diff_FF_adffs_a4_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406) # !Dout_Diff_FF_adffs_a4_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a4_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402);

Dout_Diff_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a3_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str,
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a3_a);

CPS_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(2),
	combout => CPS_a2_a_acombout);

Dout_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(3),
	combout => Dout_a3_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a5_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 5,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a5_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a5_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a5_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a5_a_modesel,
	dataout => Dly_RAM2_asram_aq_a5_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404 = Dout_Diff_FF_adffs_a3_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 = CARRY(Dout_Diff_FF_adffs_a3_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 # !Dout_Diff_FF_adffs_a3_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a3_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406);

Dout_Diff_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a2_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str,
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a2_a);

CPS_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(1),
	combout => CPS_a1_a_acombout);

Dout_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(2),
	combout => Dout_a2_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a4_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a4_a_modesel,
	dataout => Dly_RAM2_asram_aq_a4_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408 = Dout_Diff_FF_adffs_a2_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 = CARRY(Dout_Diff_FF_adffs_a2_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414) # !Dout_Diff_FF_adffs_a2_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a2_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410);

Dout_Diff_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a1_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str,
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a1_a);

Dout_Diff_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a0_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- NSlope_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(Dout_Diff_FF_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "AAF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a0_a,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a);

hc_rate_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(0),
	combout => hc_rate_a0_a_acombout);

Dout_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(1),
	combout => Dout_a1_a_acombout);

Dly_RAM2_asram_asegment_a0_a_a3_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 3,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a3_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a3_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a3_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a3_a_modesel,
	dataout => Dly_RAM2_asram_aq_a3_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412 = Dout_Diff_FF_adffs_a1_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 = CARRY(Dout_Diff_FF_adffs_a1_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 # !Dout_Diff_FF_adffs_a1_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a1_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a = Dout_a0_a_acombout
-- SumA_Add_sub_aadder_aresult_node_acout_a0_a = CARRY(!Dout_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "CC33",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a,
	cout => SumA_Add_sub_aadder_aresult_node_acout_a0_a);

Dly_RAM2_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a2_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a2_a_modesel,
	dataout => Dly_RAM2_asram_aq_a2_a);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416 = Dout_Diff_FF_adffs_a0_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 = CARRY(Dout_Diff_FF_adffs_a0_a & Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a0_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418);

Dly_RAM2_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a1_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a1_a_modesel,
	dataout => Dly_RAM2_asram_aq_a1_a);

CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK,
	combout => CLK_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

We_Clr_AD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_We_Clr_AD,
	combout => We_Clr_AD_acombout);

Ad_Clr_Cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a1_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a1_a $ Ad_Clr_Cnt_a0_a_a234, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a1_a_a231 = CARRY(!Ad_Clr_Cnt_a0_a_a234 # !Ad_Clr_Cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a1_a,
	cin => Ad_Clr_Cnt_a0_a_a234,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a1_a,
	cout => Ad_Clr_Cnt_a1_a_a231);

Ad_Clr_Cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a2_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a2_a $ (!Ad_Clr_Cnt_a1_a_a231), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a2_a_a228 = CARRY(Ad_Clr_Cnt_a2_a & (!Ad_Clr_Cnt_a1_a_a231))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a2_a,
	cin => Ad_Clr_Cnt_a1_a_a231,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a2_a,
	cout => Ad_Clr_Cnt_a2_a_a228);

Ad_Clr_Cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a3_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a3_a $ Ad_Clr_Cnt_a2_a_a228, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a3_a_a225 = CARRY(!Ad_Clr_Cnt_a2_a_a228 # !Ad_Clr_Cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a3_a,
	cin => Ad_Clr_Cnt_a2_a_a228,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a3_a,
	cout => Ad_Clr_Cnt_a3_a_a225);

Ad_Clr_Cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a4_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a4_a $ !Ad_Clr_Cnt_a3_a_a225, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a4_a_a246 = CARRY(Ad_Clr_Cnt_a4_a & !Ad_Clr_Cnt_a3_a_a225)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a4_a,
	cin => Ad_Clr_Cnt_a3_a_a225,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a4_a,
	cout => Ad_Clr_Cnt_a4_a_a246);

Ad_Clr_Cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a6_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a6_a $ !Ad_Clr_Cnt_a5_a_a243, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a6_a_a240 = CARRY(Ad_Clr_Cnt_a6_a & !Ad_Clr_Cnt_a5_a_a243)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a6_a,
	cin => Ad_Clr_Cnt_a5_a_a243,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a6_a,
	cout => Ad_Clr_Cnt_a6_a_a240);

Ad_Clr_Cnt_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a8_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a8_a $ !Ad_Clr_Cnt_a7_a_a237, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a8_a_a258 = CARRY(Ad_Clr_Cnt_a8_a & !Ad_Clr_Cnt_a7_a_a237)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a8_a,
	cin => Ad_Clr_Cnt_a7_a_a237,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a8_a,
	cout => Ad_Clr_Cnt_a8_a_a258);

Ad_Clr_Cnt_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a9_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a9_a $ (Ad_Clr_Cnt_a8_a_a258), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a9_a_a255 = CARRY(!Ad_Clr_Cnt_a8_a_a258 # !Ad_Clr_Cnt_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a9_a,
	cin => Ad_Clr_Cnt_a8_a_a258,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a9_a,
	cout => Ad_Clr_Cnt_a9_a_a255);

Ad_Clr_Cnt_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a10_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a10_a $ (!Ad_Clr_Cnt_a9_a_a255), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a10_a_a252 = CARRY(Ad_Clr_Cnt_a10_a & (!Ad_Clr_Cnt_a9_a_a255))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a10_a,
	cin => Ad_Clr_Cnt_a9_a_a255,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a10_a,
	cout => Ad_Clr_Cnt_a10_a_a252);

Ad_Clr_Cnt_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a11_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a11_a $ (Ad_Clr_Cnt_a10_a_a252), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a11_a_a249 = CARRY(!Ad_Clr_Cnt_a10_a_a252 # !Ad_Clr_Cnt_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a11_a,
	cin => Ad_Clr_Cnt_a10_a_a252,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a11_a,
	cout => Ad_Clr_Cnt_a11_a_a249);

Ad_Clr_Cnt_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a12_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a12_a $ !Ad_Clr_Cnt_a11_a_a249, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Ad_Clr_Cnt_a12_a_a261 = CARRY(Ad_Clr_Cnt_a12_a & !Ad_Clr_Cnt_a11_a_a249)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Ad_Clr_Cnt_a12_a,
	cin => Ad_Clr_Cnt_a11_a_a249,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a12_a,
	cout => Ad_Clr_Cnt_a12_a_a261);

Ad_Clr_Cnt_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Ad_Clr_Cnt_a13_a = DFFE(!GLOBAL(We_Clr_AD_acombout) & Ad_Clr_Cnt_a13_a $ (Ad_Clr_Cnt_a12_a_a261), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a13_a,
	cin => Ad_Clr_Cnt_a12_a_a261,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => We_Clr_AD_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Ad_Clr_Cnt_a13_a);

reduce_nor_a128_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a128 = Ad_Clr_Cnt_a12_a # !Ad_Clr_Cnt_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Ad_Clr_Cnt_a13_a,
	datad => Ad_Clr_Cnt_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a128);

reduce_nor_a125_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a125 = Ad_Clr_Cnt_a0_a # Ad_Clr_Cnt_a3_a # Ad_Clr_Cnt_a2_a # Ad_Clr_Cnt_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a0_a,
	datab => Ad_Clr_Cnt_a3_a,
	datac => Ad_Clr_Cnt_a2_a,
	datad => Ad_Clr_Cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a125);

reduce_nor_a127_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a127 = Ad_Clr_Cnt_a8_a # Ad_Clr_Cnt_a9_a # Ad_Clr_Cnt_a11_a # Ad_Clr_Cnt_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Ad_Clr_Cnt_a8_a,
	datab => Ad_Clr_Cnt_a9_a,
	datac => Ad_Clr_Cnt_a11_a,
	datad => Ad_Clr_Cnt_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a127);

reduce_nor_a129_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a129 = reduce_nor_a126 # reduce_nor_a128 # reduce_nor_a125 # reduce_nor_a127

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_a126,
	datab => reduce_nor_a128,
	datac => reduce_nor_a125,
	datad => reduce_nor_a127,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a129);

AD_CLR_areg0_I : apex20ke_lcell
-- Equation(s):
-- AD_CLR_areg0 = DFFE(We_Clr_AD_acombout # AD_CLR_areg0 & reduce_nor_a129, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => AD_CLR_areg0,
	datac => reduce_nor_a129,
	datad => We_Clr_AD_acombout,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AD_CLR_areg0);

add_a531_I : apex20ke_lcell
-- Equation(s):
-- add_a531 = !us1_cnt_a0_a
-- add_a533 = CARRY(us1_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a531,
	cout => add_a533);

us1_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a0_a = DFFE(add_a531, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a531,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a0_a);

fir_mr_str_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a0_a = DFFE(fir_mr_str & !fir_mr_str_cnt_a0_a, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a0_a_a56 = CARRY(fir_mr_str_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a0_a,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a0_a,
	cout => fir_mr_str_cnt_a0_a_a56);

fir_mr_str_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a1_a = DFFE(fir_mr_str & fir_mr_str_cnt_a1_a $ fir_mr_str_cnt_a0_a_a56, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a1_a_a59 = CARRY(!fir_mr_str_cnt_a0_a_a56 # !fir_mr_str_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a1_a,
	cin => fir_mr_str_cnt_a0_a_a56,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a1_a,
	cout => fir_mr_str_cnt_a1_a_a59);

fir_mr_str_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_cnt_a2_a = DFFE(fir_mr_str & fir_mr_str_cnt_a2_a $ !fir_mr_str_cnt_a1_a_a59, GLOBAL(CLK_acombout), , , !IMR_acombout)
-- fir_mr_str_cnt_a2_a_a65 = CARRY(fir_mr_str_cnt_a2_a & !fir_mr_str_cnt_a1_a_a59)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str_cnt_a2_a,
	cin => fir_mr_str_cnt_a1_a_a59,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	sclr => ALT_INV_fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str_cnt_a2_a,
	cout => fir_mr_str_cnt_a2_a_a65);

fir_mr_str_a35_I : apex20ke_lcell
-- Equation(s):
-- fir_mr_str_a35 = !fir_mr_str_cnt_a1_a # !fir_mr_str_cnt_a3_a # !fir_mr_str_cnt_a2_a # !fir_mr_str_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a4_a,
	datab => fir_mr_str_cnt_a2_a,
	datac => fir_mr_str_cnt_a3_a,
	datad => fir_mr_str_cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_mr_str_a35);

Fir_Mr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Fir_Mr,
	combout => Fir_Mr_acombout);

fir_mr_str_aI : apex20ke_lcell
-- Equation(s):
-- fir_mr_str = DFFE(Fir_Mr_acombout # fir_mr_str & (fir_mr_str_a35 # !fir_mr_str_cnt_a0_a), GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC4",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => fir_mr_str_cnt_a0_a,
	datab => fir_mr_str,
	datac => fir_mr_str_a35,
	datad => Fir_Mr_acombout,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_mr_str);

wadr_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a0_a = DFFE(!wadr_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a0_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a1_a = DFFE(wadr_cntr_awysi_counter_asload_path_a1_a $ wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a1_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a2_a = DFFE(wadr_cntr_awysi_counter_asload_path_a2_a $ (!wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(wadr_cntr_awysi_counter_asload_path_a2_a & (!wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => wadr_cntr_awysi_counter_asload_path_a2_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a2_a,
	cout => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

wadr_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- wadr_cntr_awysi_counter_asload_path_a3_a = DFFE(wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT $ wadr_cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => wadr_cntr_awysi_counter_asload_path_a3_a,
	cin => wadr_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => wadr_cntr_awysi_counter_asload_path_a3_a);

radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a = wadr_cntr_awysi_counter_asload_path_a1_a
-- radr_Add_sub_aadder_aresult_node_acout_a1_a = CARRY(wadr_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "CCCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => wadr_cntr_awysi_counter_asload_path_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	cout => radr_Add_sub_aadder_aresult_node_acout_a1_a);

Dly_RAM2_asram_asegment_a0_a_a14_a_a0_I : apex20ke_lcell
-- Equation(s):
-- Dly_RAM2_asram_asegment_a0_a_a14_a_a0 = !radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dly_RAM2_asram_asegment_a0_a_a14_a_a0);

radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a47 : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48 = radr_Add_sub_aadder_aresult_node_acout_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => radr_Add_sub_aadder_aresult_node_acout_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41_I : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41 = radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48 $ wadr_cntr_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48,
	datad => wadr_cntr_awysi_counter_asload_path_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a41);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44_I : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44 = wadr_cntr_awysi_counter_asload_path_a2_a & radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => wadr_cntr_awysi_counter_asload_path_a2_a,
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a48,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44);

radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43_I : apex20ke_lcell
-- Equation(s):
-- radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43 = wadr_cntr_awysi_counter_asload_path_a3_a $ !radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => wadr_cntr_awysi_counter_asload_path_a3_a,
	datad => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a44,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => radr_Add_sub_aadder_aresult_node_acs_buffer_a2_a_a43);

Dly_RAM1_asram_asegment_a0_a_a14_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 14,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a14_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a14_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a14_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a14_a_modesel,
	dataout => Dly_RAM1_asram_aq_a14_a);

Dout_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(14),
	combout => Dout_a14_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a13_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 13,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a13_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a13_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a13_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a13_a_modesel,
	dataout => Dly_RAM1_asram_aq_a13_a);

Dly_RAM1_asram_asegment_a0_a_a12_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 12,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a12_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a12_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a12_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a12_a_modesel,
	dataout => Dly_RAM1_asram_aq_a12_a);

Dout_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(12),
	combout => Dout_a12_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a10_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 10,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a10_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a10_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a10_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a10_a_modesel,
	dataout => Dly_RAM1_asram_aq_a10_a);

Dly_RAM1_asram_asegment_a0_a_a9_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 9,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a9_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a9_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a9_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a9_a_modesel,
	dataout => Dly_RAM1_asram_aq_a9_a);

Dly_RAM1_asram_asegment_a0_a_a8_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 8,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a8_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a8_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a8_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a8_a_modesel,
	dataout => Dly_RAM1_asram_aq_a8_a);

Dout_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(8),
	combout => Dout_a8_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a6_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 6,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a6_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a6_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a6_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a6_a_modesel,
	dataout => Dly_RAM1_asram_aq_a6_a);

Dout_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(6),
	combout => Dout_a6_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a4_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 4,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a4_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a4_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a4_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a4_a_modesel,
	dataout => Dly_RAM1_asram_aq_a4_a);

Dout_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(4),
	combout => Dout_a4_a_acombout);

Dly_RAM1_asram_asegment_a0_a_a2_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 2,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a2_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a2_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a2_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a2_a_modesel,
	dataout => Dly_RAM1_asram_aq_a2_a);

Dly_RAM1_asram_asegment_a0_a_a1_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 1,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a1_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a1_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a1_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a1_a_modesel,
	dataout => Dly_RAM1_asram_aq_a1_a);

Dly_RAM1_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM1|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dout_a0_a_acombout,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM1_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Dly_RAM1_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM1_asram_asegment_a0_a_a0_a_modesel,
	dataout => Dly_RAM1_asram_aq_a0_a);

Dout_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Dout(0),
	combout => Dout_a0_a_acombout);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387 = SumA_Add_sub_aadder_aresult_node_acout_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => SumA_Add_sub_aadder_aresult_node_acout_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381 = CARRY(SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a387,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375 = Dout_a1_a_acombout $ Dly_RAM1_asram_aq_a0_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377 = CARRY(Dout_a1_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381 # !Dly_RAM1_asram_aq_a0_a) # !Dout_a1_a_acombout & !Dly_RAM1_asram_aq_a0_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a1_a_acombout,
	datab => Dly_RAM1_asram_aq_a0_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a381,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371 = Dout_a2_a_acombout $ Dly_RAM1_asram_aq_a1_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373 = CARRY(Dout_a2_a_acombout & Dly_RAM1_asram_aq_a1_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377 # !Dout_a2_a_acombout & (Dly_RAM1_asram_aq_a1_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a2_a_acombout,
	datab => Dly_RAM1_asram_aq_a1_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a377,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367 = Dout_a3_a_acombout $ Dly_RAM1_asram_aq_a2_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369 = CARRY(Dout_a3_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373 # !Dly_RAM1_asram_aq_a2_a) # !Dout_a3_a_acombout & !Dly_RAM1_asram_aq_a2_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a3_a_acombout,
	datab => Dly_RAM1_asram_aq_a2_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a373,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363 = Dly_RAM1_asram_aq_a3_a $ Dout_a4_a_acombout $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365 = CARRY(Dly_RAM1_asram_aq_a3_a & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369 # !Dout_a4_a_acombout) # !Dly_RAM1_asram_aq_a3_a & !Dout_a4_a_acombout & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM1_asram_aq_a3_a,
	datab => Dout_a4_a_acombout,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a369,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359 = Dout_a5_a_acombout $ Dly_RAM1_asram_aq_a4_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361 = CARRY(Dout_a5_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365 # !Dly_RAM1_asram_aq_a4_a) # !Dout_a5_a_acombout & !Dly_RAM1_asram_aq_a4_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a5_a_acombout,
	datab => Dly_RAM1_asram_aq_a4_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a365,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355 = Dly_RAM1_asram_aq_a5_a $ Dout_a6_a_acombout $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357 = CARRY(Dly_RAM1_asram_aq_a5_a & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361 # !Dout_a6_a_acombout) # !Dly_RAM1_asram_aq_a5_a & !Dout_a6_a_acombout & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM1_asram_aq_a5_a,
	datab => Dout_a6_a_acombout,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a361,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351 = Dout_a7_a_acombout $ Dly_RAM1_asram_aq_a6_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353 = CARRY(Dout_a7_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357 # !Dly_RAM1_asram_aq_a6_a) # !Dout_a7_a_acombout & !Dly_RAM1_asram_aq_a6_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a7_a_acombout,
	datab => Dly_RAM1_asram_aq_a6_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a357,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347 = Dly_RAM1_asram_aq_a7_a $ Dout_a8_a_acombout $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349 = CARRY(Dly_RAM1_asram_aq_a7_a & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353 # !Dout_a8_a_acombout) # !Dly_RAM1_asram_aq_a7_a & !Dout_a8_a_acombout & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM1_asram_aq_a7_a,
	datab => Dout_a8_a_acombout,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a353,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343 = Dout_a9_a_acombout $ Dly_RAM1_asram_aq_a8_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345 = CARRY(Dout_a9_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349 # !Dly_RAM1_asram_aq_a8_a) # !Dout_a9_a_acombout & !Dly_RAM1_asram_aq_a8_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a9_a_acombout,
	datab => Dly_RAM1_asram_aq_a8_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a349,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339 = Dout_a10_a_acombout $ Dly_RAM1_asram_aq_a9_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341 = CARRY(Dout_a10_a_acombout & Dly_RAM1_asram_aq_a9_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345 # !Dout_a10_a_acombout & (Dly_RAM1_asram_aq_a9_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a10_a_acombout,
	datab => Dly_RAM1_asram_aq_a9_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a345,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335 = Dout_a11_a_acombout $ Dly_RAM1_asram_aq_a10_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337 = CARRY(Dout_a11_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341 # !Dly_RAM1_asram_aq_a10_a) # !Dout_a11_a_acombout & !Dly_RAM1_asram_aq_a10_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a11_a_acombout,
	datab => Dly_RAM1_asram_aq_a10_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a341,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331 = Dly_RAM1_asram_aq_a11_a $ Dout_a12_a_acombout $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333 = CARRY(Dly_RAM1_asram_aq_a11_a & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337 # !Dout_a12_a_acombout) # !Dly_RAM1_asram_aq_a11_a & !Dout_a12_a_acombout & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM1_asram_aq_a11_a,
	datab => Dout_a12_a_acombout,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a337,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327 = Dout_a13_a_acombout $ Dly_RAM1_asram_aq_a12_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329 = CARRY(Dout_a13_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333 # !Dly_RAM1_asram_aq_a12_a) # !Dout_a13_a_acombout & !Dly_RAM1_asram_aq_a12_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a13_a_acombout,
	datab => Dly_RAM1_asram_aq_a12_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a333,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323 = Dout_a14_a_acombout $ Dly_RAM1_asram_aq_a13_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325 = CARRY(Dout_a14_a_acombout & Dly_RAM1_asram_aq_a13_a & !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329 # !Dout_a14_a_acombout & (Dly_RAM1_asram_aq_a13_a # 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a14_a_acombout,
	datab => Dly_RAM1_asram_aq_a13_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a329,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319 = Dout_a14_a_acombout $ Dly_RAM1_asram_aq_a14_a $ !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a321 = CARRY(Dout_a14_a_acombout & (!SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325 # !Dly_RAM1_asram_aq_a14_a) # !Dout_a14_a_acombout & !Dly_RAM1_asram_aq_a14_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_a14_a_acombout,
	datab => Dly_RAM1_asram_aq_a14_a,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a325,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319,
	cout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a321);

SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315_I : apex20ke_lcell
-- Equation(s):
-- SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 = Dly_RAM1_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a321 $ Dout_a14_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Dly_RAM1_asram_aq_a14_a,
	datad => Dout_a14_a_acombout,
	cin => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a321,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315);

Dly_RAM2_asram_asegment_a0_a_a0_a : apex20ke_ram_slice
-- pragma translate_off
GENERIC MAP (
	operation_mode => "dual_port",
	logical_ram_name => "lpm_ram_dp:Dly_RAM2|altdpram:sram|content",
	init_file => "INIT.MIF",
	logical_ram_depth => 16,
	logical_ram_width => 15,
	address_width => 4,
	first_address => 0,
	last_address => 15,
	bit_number => 0,
	data_in_clock => "clock0",
	data_in_clear => "none",
	write_logic_clock => "clock0",
	write_logic_clear => "none",
	read_enable_clock => "none",
	read_enable_clear => "none",
	read_address_clock => "clock1",
	read_address_clear => "none",
	data_out_clock => "clock1",
	data_out_clear => "none",
	deep_ram_mode => "off")
-- pragma translate_on
PORT MAP (
	datain => Dly_RAM1_asram_aq_a0_a,
	clk0 => CLK_acombout,
	clk1 => CLK_acombout,
	we => VCC,
	re => VCC,
	waddr => Dly_RAM2_asram_asegment_a0_a_a0_a_WADDR_bus,
	raddr => Dly_RAM2_asram_asegment_a0_a_a0_a_RADDR_bus,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	modesel => Dly_RAM2_asram_asegment_a0_a_a0_a_modesel,
	dataout => Dly_RAM2_asram_aq_a0_a);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430 = SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a $ Dly_RAM2_asram_aq_a0_a
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432 = CARRY(SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a # !Dly_RAM2_asram_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "66BB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => SumA_Add_sub_aadder_aresult_node_acs_buffer_a0_a,
	datab => Dly_RAM2_asram_aq_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a430,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426 = Dly_RAM2_asram_aq_a1_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428 = CARRY(Dly_RAM2_asram_aq_a1_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375) # !Dly_RAM2_asram_aq_a1_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a1_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a375,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a432,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a426,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422 = Dly_RAM2_asram_aq_a2_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424 = CARRY(Dly_RAM2_asram_aq_a2_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428 # !Dly_RAM2_asram_aq_a2_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a2_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a371,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a428,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a422,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418 = Dly_RAM2_asram_aq_a3_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420 = CARRY(Dly_RAM2_asram_aq_a3_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367) # !Dly_RAM2_asram_aq_a3_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a3_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a367,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a424,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a418,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414 = Dly_RAM2_asram_aq_a4_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416 = CARRY(Dly_RAM2_asram_aq_a4_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420 # !Dly_RAM2_asram_aq_a4_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a4_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a363,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a420,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a414,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410 = Dly_RAM2_asram_aq_a5_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412 = CARRY(Dly_RAM2_asram_aq_a5_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359) # !Dly_RAM2_asram_aq_a5_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a5_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a359,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a416,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a410,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 = Dly_RAM2_asram_aq_a6_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408 = CARRY(Dly_RAM2_asram_aq_a6_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412 # !Dly_RAM2_asram_aq_a6_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a6_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a355,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a412,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402 = Dly_RAM2_asram_aq_a7_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404 = CARRY(Dly_RAM2_asram_aq_a7_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351) # !Dly_RAM2_asram_aq_a7_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a7_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a351,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a408,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a402,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398 = Dly_RAM2_asram_aq_a8_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400 = CARRY(Dly_RAM2_asram_aq_a8_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404 # !Dly_RAM2_asram_aq_a8_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a8_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a347,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a404,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 = Dly_RAM2_asram_aq_a9_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396 = CARRY(Dly_RAM2_asram_aq_a9_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343) # !Dly_RAM2_asram_aq_a9_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a9_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a343,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a400,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 = Dly_RAM2_asram_aq_a10_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392 = CARRY(Dly_RAM2_asram_aq_a10_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396 # !Dly_RAM2_asram_aq_a10_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a10_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a339,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a396,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 = Dly_RAM2_asram_aq_a11_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388 = CARRY(Dly_RAM2_asram_aq_a11_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335) # !Dly_RAM2_asram_aq_a11_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a11_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a335,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 = Dly_RAM2_asram_aq_a12_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384 = CARRY(Dly_RAM2_asram_aq_a12_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388 # !Dly_RAM2_asram_aq_a12_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a12_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a331,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a388,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 = Dly_RAM2_asram_aq_a13_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380 = CARRY(Dly_RAM2_asram_aq_a13_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327) # !Dly_RAM2_asram_aq_a13_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a13_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a327,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376 = CARRY(Dly_RAM2_asram_aq_a14_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380 # !Dly_RAM2_asram_aq_a14_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a323,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372 = CARRY(Dly_RAM2_asram_aq_a14_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319) # !Dly_RAM2_asram_aq_a14_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a319,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368 = CARRY(Dly_RAM2_asram_aq_a14_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372 # !Dly_RAM2_asram_aq_a14_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364 = CARRY(Dly_RAM2_asram_aq_a14_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315) # !Dly_RAM2_asram_aq_a14_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 = CARRY(Dly_RAM2_asram_aq_a14_a & SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364 # !Dly_RAM2_asram_aq_a14_a & 
-- (SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 = Dly_RAM2_asram_aq_a14_a $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 = CARRY(Dly_RAM2_asram_aq_a14_a & (!Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 # !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315) # !Dly_RAM2_asram_aq_a14_a & 
-- !SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315 & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datab => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350,
	cout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352);

Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358_I : apex20ke_lcell
-- Equation(s):
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358 = Dly_RAM2_asram_aq_a14_a $ (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 $ SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dly_RAM2_asram_aq_a14_a,
	datad => SumA_Add_sub_aadder_aresult_node_acs_buffer_a1_a_a315,
	cin => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392 = Dout_Diff_FF_adffs_a6_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 = CARRY(Dout_Diff_FF_adffs_a6_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398) # !Dout_Diff_FF_adffs_a6_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a6_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a406,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a398,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380 = Dout_Diff_FF_adffs_a9_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 = CARRY(Dout_Diff_FF_adffs_a9_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 # !Dout_Diff_FF_adffs_a9_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a9_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a394,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376 = Dout_Diff_FF_adffs_a10_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 = CARRY(Dout_Diff_FF_adffs_a10_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382) # !Dout_Diff_FF_adffs_a10_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a10_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a390,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372 = Dout_Diff_FF_adffs_a11_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 = CARRY(Dout_Diff_FF_adffs_a11_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 # !Dout_Diff_FF_adffs_a11_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a11_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a386,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368 = Dout_Diff_FF_adffs_a12_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 = CARRY(Dout_Diff_FF_adffs_a12_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374) # !Dout_Diff_FF_adffs_a12_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a12_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a382,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364 = Dout_Diff_FF_adffs_a13_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 = CARRY(Dout_Diff_FF_adffs_a13_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 # !Dout_Diff_FF_adffs_a13_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a13_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a378,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360 = Dout_Diff_FF_adffs_a14_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 = CARRY(Dout_Diff_FF_adffs_a14_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366) # !Dout_Diff_FF_adffs_a14_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a14_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a374,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 = Dout_Diff_FF_adffs_a15_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358 = CARRY(Dout_Diff_FF_adffs_a15_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 # !Dout_Diff_FF_adffs_a15_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a15_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a370,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 = Dout_Diff_FF_adffs_a16_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 $ !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 = CARRY(Dout_Diff_FF_adffs_a16_a & (Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 # !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358) # !Dout_Diff_FF_adffs_a16_a & 
-- Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a16_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a366,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348 = Dout_Diff_FF_adffs_a17_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 = CARRY(Dout_Diff_FF_adffs_a17_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 # !Dout_Diff_FF_adffs_a17_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a17_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a362,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a354,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336 = Dout_Diff_FF_adffs_a19_a $ Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a338 = CARRY(Dout_Diff_FF_adffs_a19_a & !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350 & !Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346 # !Dout_Diff_FF_adffs_a19_a & 
-- (!Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346 # !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a19_a,
	datab => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a350,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a346,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336,
	cout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a338);

Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340_I : apex20ke_lcell
-- Equation(s):
-- Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340 = Dout_Diff_FF_adffs_a20_a $ Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a338 $ !Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CC3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a20_a,
	datad => Sumb_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a358,
	cin => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a338,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340);

Dout_Diff_FF_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a20_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a340,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a20_a);

Dout_Diff_FF_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a17_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a348,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a17_a);

Dout_Diff_FF_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a16_a = DFFE(Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352 & !fir_mr_str, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a352,
	datad => fir_mr_str,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a16_a);

Dout_Diff_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a15_a = DFFE(Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356 & !fir_mr_str, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a356,
	datad => fir_mr_str,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a15_a);

Dout_Diff_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a14_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a360,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a14_a);

Dout_Diff_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a13_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a364,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a13_a);

Dout_Diff_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a12_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a368,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a12_a);

Dout_Diff_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a11_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a372,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a11_a);

Dout_Diff_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a10_a = DFFE(!fir_mr_str & (Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a376,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a10_a);

Dout_Diff_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a9_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a380,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a9_a);

Dout_Diff_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a8_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- PSlope_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(!Dout_Diff_FF_adffs_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "AA0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a384,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => fir_mr_str,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a8_a,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a);

Dout_Diff_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a6_a = DFFE(!fir_mr_str & Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => fir_mr_str,
	datad => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a392,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a6_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a1_a_a164_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(!Dout_Diff_FF_adffs_a1_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a1_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a1_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a2_a_a163_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(Dout_Diff_FF_adffs_a2_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a2_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a2_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a3_a_a162_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(!Dout_Diff_FF_adffs_a3_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a3_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a3_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a4_a_a161_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(Dout_Diff_FF_adffs_a4_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a4_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a4_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a5_a_a160_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(!Dout_Diff_FF_adffs_a5_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a5_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a5_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a6_a_a159_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(Dout_Diff_FF_adffs_a6_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a6_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a6_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a7_a_a158_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(!Dout_Diff_FF_adffs_a7_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a7_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a7_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a8_a_a157_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(Dout_Diff_FF_adffs_a8_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a8_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a8_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a156_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(!Dout_Diff_FF_adffs_a9_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a9_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a9_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a155_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(Dout_Diff_FF_adffs_a10_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a10_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a10_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a154_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(!NSlope_Compare_acomparator_acmp_end_alcarry_a10_a # !Dout_Diff_FF_adffs_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a11_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a11_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a153_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(Dout_Diff_FF_adffs_a12_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a12_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a12_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a152_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(!Dout_Diff_FF_adffs_a13_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a13_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a13_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a151_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(Dout_Diff_FF_adffs_a14_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a14_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a14_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a150_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(!Dout_Diff_FF_adffs_a15_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a15_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a15_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a149_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(Dout_Diff_FF_adffs_a16_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a16_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a16_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a148_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(!Dout_Diff_FF_adffs_a17_a & !NSlope_Compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a17_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a17_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a147_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(Dout_Diff_FF_adffs_a18_a # !NSlope_Compare_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a18_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a18_a);

NSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a146_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(!Dout_Diff_FF_adffs_a19_a & (!NSlope_Compare_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a19_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => NSlope_Compare_acomparator_acmp_end_alcarry_a19_a);

NSlope_Compare_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- NSlope_Compare_acomparator_acmp_end_aagb_out = !NSlope_Compare_acomparator_acmp_end_alcarry_a19_a & !Dout_Diff_FF_adffs_a20_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_Diff_FF_adffs_a20_a,
	cin => NSlope_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => NSlope_Compare_acomparator_acmp_end_aagb_out);

Dout_Diff_FF_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_Diff_FF_adffs_a19_a = DFFE(Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336 & !fir_mr_str, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Sumc_Add_sub_aadder_aresult_node_acs_buffer_a0_a_a336,
	datad => fir_mr_str,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_Diff_FF_adffs_a19_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a9_a_a37_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(Dout_Diff_FF_adffs_a9_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a9_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a9_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a10_a_a36_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(!Dout_Diff_FF_adffs_a10_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a10_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a10_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a11_a_a35_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(Dout_Diff_FF_adffs_a11_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a11_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a11_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a12_a_a34_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(!Dout_Diff_FF_adffs_a12_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a12_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a12_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a13_a_a33_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(Dout_Diff_FF_adffs_a13_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a13_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a13_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a14_a_a32_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a14_a = CARRY(!Dout_Diff_FF_adffs_a14_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a14_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a14_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a15_a_a31_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a15_a = CARRY(Dout_Diff_FF_adffs_a15_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a15_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a15_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a16_a_a30_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a16_a = CARRY(!Dout_Diff_FF_adffs_a16_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a16_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a16_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a17_a_a29_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a17_a = CARRY(Dout_Diff_FF_adffs_a17_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a17_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a17_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a18_a_a28_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a18_a = CARRY(!Dout_Diff_FF_adffs_a18_a & (!PSlope_Compare_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a18_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a18_a);

PSlope_Compare_acomparator_acmp_end_alcarry_a19_a_a27_I : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_alcarry_a19_a = CARRY(Dout_Diff_FF_adffs_a19_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Dout_Diff_FF_adffs_a19_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => PSlope_Compare_acomparator_acmp_end_alcarry_a19_a);

PSlope_Compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- PSlope_Compare_acomparator_acmp_end_aagb_out = Dout_Diff_FF_adffs_a20_a # !PSlope_Compare_acomparator_acmp_end_alcarry_a19_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "AFAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Diff_FF_adffs_a20_a,
	cin => PSlope_Compare_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => PSlope_Compare_acomparator_acmp_end_aagb_out);

Reset_areg0_I : apex20ke_lcell
-- Equation(s):
-- Reset_areg0 = DFFE(NSlope_Compare_acomparator_acmp_end_aagb_out # Reset_areg0 & !PSlope_Compare_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0FC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Reset_areg0,
	datac => NSlope_Compare_acomparator_acmp_end_aagb_out,
	datad => PSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_areg0);

us1_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a4_a = DFFE(add_a527 $ (!reduce_nor_a130 & us1_cnt_a0_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A5AA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => add_a527,
	datac => reduce_nor_a130,
	datad => us1_cnt_a0_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a4_a);

add_a523_I : apex20ke_lcell
-- Equation(s):
-- add_a523 = us1_cnt_a1_a $ (add_a533)
-- add_a525 = CARRY(!add_a533 # !us1_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a1_a,
	cin => add_a533,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a523,
	cout => add_a525);

us1_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a1_a = DFFE(add_a523, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => add_a523,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a1_a);

us1_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- us1_cnt_a2_a = DFFE(add_a515 $ (!reduce_nor_a130 & us1_cnt_a0_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C3CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => add_a515,
	datac => reduce_nor_a130,
	datad => us1_cnt_a0_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => us1_cnt_a2_a);

reduce_nor_a130_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a130 = us1_cnt_a3_a # us1_cnt_a2_a # !us1_cnt_a1_a # !us1_cnt_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => us1_cnt_a3_a,
	datab => us1_cnt_a4_a,
	datac => us1_cnt_a1_a,
	datad => us1_cnt_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a130);

reset_Time_Cen_a0_I : apex20ke_lcell
-- Equation(s):
-- reset_Time_Cen_a0 = us1_cnt_a0_a & !Reset_areg0 & !reduce_nor_a130

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => us1_cnt_a0_a,
	datac => Reset_areg0,
	datad => reduce_nor_a130,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reset_Time_Cen_a0);

reset_Del_aI : apex20ke_lcell
-- Equation(s):
-- reset_Del = DFFE(Reset_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_Del);

Reset_LEd_a0_I : apex20ke_lcell
-- Equation(s):
-- Reset_LEd_a0 = Reset_areg0 & !reset_Del

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_areg0,
	datad => reset_Del,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Reset_LEd_a0);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(Reset_LEd_a0) & reset_Time_Cen_a0 $ Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reset_Time_Cen_a0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a & (!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	datab => reset_Time_Cen_a0,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a $ (reset_Time_Cen_a0 & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )
-- Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a & !Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	cout => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT);

Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a31_a : apex20ke_lcell
-- Equation(s):
-- Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a $ (reset_Time_Cen_a0 & Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT), GLOBAL(CLK_acombout), 
-- !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reset_Time_Cen_a0,
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	cin => Reset_Time_Cnt_Cntr_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194 = Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "CCCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a_a193_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a_a192_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a_a191_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a_a190_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a_a189_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a_a188_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a_a187_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a_a186_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a_a185_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a_a184_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a_a183_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a_a182_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a_a181_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a_a180_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a_a179_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a_a178_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a_a177_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a_a176_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a_a175_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a_a174_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a_a173_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a_a172_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a_a171_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a = CARRY(!No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a # !Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a_a170_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a_a169_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a & !No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a_a168_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "000A",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a_a167_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a_a166_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00AF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a_a165_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a = CARRY(!Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a & (!No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a);

No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a_a164_I : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a = CARRY(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a # !No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a);

No_Reset_Cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- No_Reset_Cmp_acomparator_acmp_end_aagb_out = No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a # Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	cin => No_Reset_Cmp_acomparator_acmp_end_alcarry_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => No_Reset_Cmp_acomparator_acmp_end_aagb_out);

ms100tc_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ms100tc,
	combout => ms100tc_acombout);

det_sat_areg0_I : apex20ke_lcell
-- Equation(s):
-- det_sat_areg0 = DFFE(No_Reset_Cmp_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , ms100tc_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => No_Reset_Cmp_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => ms100tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => det_sat_areg0);

hc_rate_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(20),
	combout => hc_rate_a20_a_acombout);

CPS_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(20),
	combout => CPS_a20_a_acombout);

hc_rate_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(19),
	combout => hc_rate_a19_a_acombout);

hc_rate_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(18),
	combout => hc_rate_a18_a_acombout);

hc_rate_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(17),
	combout => hc_rate_a17_a_acombout);

CPS_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(16),
	combout => CPS_a16_a_acombout);

CPS_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(15),
	combout => CPS_a15_a_acombout);

CPS_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(14),
	combout => CPS_a14_a_acombout);

CPS_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(13),
	combout => CPS_a13_a_acombout);

CPS_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(12),
	combout => CPS_a12_a_acombout);

CPS_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(11),
	combout => CPS_a11_a_acombout);

CPS_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(10),
	combout => CPS_a10_a_acombout);

CPS_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(9),
	combout => CPS_a9_a_acombout);

CPS_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(8),
	combout => CPS_a8_a_acombout);

CPS_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(7),
	combout => CPS_a7_a_acombout);

hc_rate_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(6),
	combout => hc_rate_a6_a_acombout);

CPS_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(5),
	combout => CPS_a5_a_acombout);

CPS_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(4),
	combout => CPS_a4_a_acombout);

CPS_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(3),
	combout => CPS_a3_a_acombout);

hc_rate_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(2),
	combout => hc_rate_a2_a_acombout);

hc_rate_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_rate(1),
	combout => hc_rate_a1_a_acombout);

CPS_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CPS(0),
	combout => CPS_a0_a_acombout);

cps_cmp_acomparator_acmp_end_alcarry_a0_a_a958_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(!hc_rate_a0_a_acombout & CPS_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a0_a_acombout,
	datab => CPS_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a0_a);

cps_cmp_acomparator_acmp_end_alcarry_a1_a_a957_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(CPS_a1_a_acombout & hc_rate_a1_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a0_a # !CPS_a1_a_acombout & (hc_rate_a1_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a1_a_acombout,
	datab => hc_rate_a1_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a1_a);

cps_cmp_acomparator_acmp_end_alcarry_a2_a_a956_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(CPS_a2_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a1_a # !hc_rate_a2_a_acombout) # !CPS_a2_a_acombout & !hc_rate_a2_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a2_a_acombout,
	datab => hc_rate_a2_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a2_a);

cps_cmp_acomparator_acmp_end_alcarry_a3_a_a955_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(hc_rate_a3_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a2_a # !CPS_a3_a_acombout) # !hc_rate_a3_a_acombout & !CPS_a3_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a3_a_acombout,
	datab => CPS_a3_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a3_a);

cps_cmp_acomparator_acmp_end_alcarry_a4_a_a954_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(hc_rate_a4_a_acombout & CPS_a4_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a3_a # !hc_rate_a4_a_acombout & (CPS_a4_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a4_a_acombout,
	datab => CPS_a4_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a4_a);

cps_cmp_acomparator_acmp_end_alcarry_a5_a_a953_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(hc_rate_a5_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a4_a # !CPS_a5_a_acombout) # !hc_rate_a5_a_acombout & !CPS_a5_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a5_a_acombout,
	datab => CPS_a5_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a5_a);

cps_cmp_acomparator_acmp_end_alcarry_a6_a_a952_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(CPS_a6_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a5_a # !hc_rate_a6_a_acombout) # !CPS_a6_a_acombout & !hc_rate_a6_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a6_a_acombout,
	datab => hc_rate_a6_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a6_a);

cps_cmp_acomparator_acmp_end_alcarry_a7_a_a951_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(hc_rate_a7_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a6_a # !CPS_a7_a_acombout) # !hc_rate_a7_a_acombout & !CPS_a7_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a7_a_acombout,
	datab => CPS_a7_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a7_a);

cps_cmp_acomparator_acmp_end_alcarry_a8_a_a950_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(hc_rate_a8_a_acombout & CPS_a8_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a7_a # !hc_rate_a8_a_acombout & (CPS_a8_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a8_a_acombout,
	datab => CPS_a8_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a8_a);

cps_cmp_acomparator_acmp_end_alcarry_a9_a_a949_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(hc_rate_a9_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a8_a # !CPS_a9_a_acombout) # !hc_rate_a9_a_acombout & !CPS_a9_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a9_a_acombout,
	datab => CPS_a9_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a9_a);

cps_cmp_acomparator_acmp_end_alcarry_a10_a_a948_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(hc_rate_a10_a_acombout & CPS_a10_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a9_a # !hc_rate_a10_a_acombout & (CPS_a10_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a10_a_acombout,
	datab => CPS_a10_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a10_a);

cps_cmp_acomparator_acmp_end_alcarry_a11_a_a947_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(hc_rate_a11_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a10_a # !CPS_a11_a_acombout) # !hc_rate_a11_a_acombout & !CPS_a11_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a11_a_acombout,
	datab => CPS_a11_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a11_a);

cps_cmp_acomparator_acmp_end_alcarry_a12_a_a946_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(hc_rate_a12_a_acombout & CPS_a12_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a11_a # !hc_rate_a12_a_acombout & (CPS_a12_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a12_a_acombout,
	datab => CPS_a12_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a12_a);

cps_cmp_acomparator_acmp_end_alcarry_a13_a_a945_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(hc_rate_a13_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a12_a # !CPS_a13_a_acombout) # !hc_rate_a13_a_acombout & !CPS_a13_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a13_a_acombout,
	datab => CPS_a13_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a13_a);

cps_cmp_acomparator_acmp_end_alcarry_a14_a_a944_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(hc_rate_a14_a_acombout & CPS_a14_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a13_a # !hc_rate_a14_a_acombout & (CPS_a14_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a14_a_acombout,
	datab => CPS_a14_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a14_a);

cps_cmp_acomparator_acmp_end_alcarry_a15_a_a943_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a15_a = CARRY(hc_rate_a15_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a14_a # !CPS_a15_a_acombout) # !hc_rate_a15_a_acombout & !CPS_a15_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a15_a_acombout,
	datab => CPS_a15_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a15_a);

cps_cmp_acomparator_acmp_end_alcarry_a16_a_a942_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a16_a = CARRY(hc_rate_a16_a_acombout & CPS_a16_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a15_a # !hc_rate_a16_a_acombout & (CPS_a16_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_rate_a16_a_acombout,
	datab => CPS_a16_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a16_a);

cps_cmp_acomparator_acmp_end_alcarry_a17_a_a941_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a17_a = CARRY(CPS_a17_a_acombout & hc_rate_a17_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a16_a # !CPS_a17_a_acombout & (hc_rate_a17_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a17_a_acombout,
	datab => hc_rate_a17_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a17_a);

cps_cmp_acomparator_acmp_end_alcarry_a18_a_a940_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a18_a = CARRY(CPS_a18_a_acombout & (!cps_cmp_acomparator_acmp_end_alcarry_a17_a # !hc_rate_a18_a_acombout) # !CPS_a18_a_acombout & !hc_rate_a18_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a18_a_acombout,
	datab => hc_rate_a18_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a18_a);

cps_cmp_acomparator_acmp_end_alcarry_a19_a_a939_I : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_alcarry_a19_a = CARRY(CPS_a19_a_acombout & hc_rate_a19_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a18_a # !CPS_a19_a_acombout & (hc_rate_a19_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CPS_a19_a_acombout,
	datab => hc_rate_a19_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => cps_cmp_acomparator_acmp_end_alcarry_a19_a);

cps_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- cps_cmp_acomparator_acmp_end_aagb_out = hc_rate_a20_a_acombout & !cps_cmp_acomparator_acmp_end_alcarry_a19_a & CPS_a20_a_acombout # !hc_rate_a20_a_acombout & (CPS_a20_a_acombout # !cps_cmp_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3F03",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => hc_rate_a20_a_acombout,
	datad => CPS_a20_a_acombout,
	cin => cps_cmp_acomparator_acmp_end_alcarry_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => cps_cmp_acomparator_acmp_end_aagb_out);

hc_cnt_cen_a9_I : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cen_a9 = ms100tc_acombout & cps_cmp_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ms100tc_acombout,
	datad => cps_cmp_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_cnt_cen_a9);

hc_cnt_clr_a0_I : apex20ke_lcell
-- Equation(s):
-- hc_cnt_clr_a0 = ms100tc_acombout & !cps_cmp_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ms100tc_acombout,
	datad => cps_cmp_acomparator_acmp_end_aagb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_cnt_clr_a0);

hc_cnt_cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cen_a9 $ hc_cnt_cntr_awysi_counter_asload_path_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => hc_cnt_cen_a9,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a1_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a2_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a2_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a3_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a4_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a4_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a5_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a6_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a6_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a7_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a8_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & !hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a9,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a9_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cen_a9,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a10_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a10_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a11_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a12_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a12_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a13_a $ (hc_cnt_cen_a9 & hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !hc_cnt_cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a14_a $ (hc_cnt_cen_a9 & !hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a14_a & (!hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	datab => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cout => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

hc_cnt_cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- hc_cnt_cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(hc_cnt_clr_a0) & hc_cnt_cntr_awysi_counter_asload_path_a15_a $ (hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT & hc_cnt_cen_a9), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CCC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	datad => hc_cnt_cen_a9,
	cin => hc_cnt_cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => hc_cnt_clr_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => hc_cnt_cntr_awysi_counter_asload_path_a15_a);

hc_threshold_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(15),
	combout => hc_threshold_a15_a_acombout);

hc_threshold_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(9),
	combout => hc_threshold_a9_a_acombout);

hc_threshold_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_hc_threshold(8),
	combout => hc_threshold_a8_a_acombout);

hc_count_cmp_acomparator_acmp_end_alcarry_a0_a_a708_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(!hc_threshold_a0_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a0_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a0_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a1_a_a707_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(hc_threshold_a1_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a0_a # !hc_cnt_cntr_awysi_counter_asload_path_a1_a) # !hc_threshold_a1_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a1_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a1_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a1_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a1_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a2_a_a706_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(hc_threshold_a2_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a2_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a1_a # !hc_threshold_a2_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a2_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a2_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a2_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a2_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a3_a_a705_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(hc_threshold_a3_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a2_a # !hc_cnt_cntr_awysi_counter_asload_path_a3_a) # !hc_threshold_a3_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a3_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a3_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a3_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a3_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a4_a_a704_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(hc_threshold_a4_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a4_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a3_a # !hc_threshold_a4_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a4_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a4_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a4_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a4_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a5_a_a703_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(hc_threshold_a5_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a4_a # !hc_cnt_cntr_awysi_counter_asload_path_a5_a) # !hc_threshold_a5_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a5_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a5_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a5_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a5_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a6_a_a702_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(hc_threshold_a6_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a6_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a5_a # !hc_threshold_a6_a_acombout & (hc_cnt_cntr_awysi_counter_asload_path_a6_a # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a6_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a6_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a6_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a7_a_a701_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(hc_threshold_a7_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a6_a # !hc_cnt_cntr_awysi_counter_asload_path_a7_a) # !hc_threshold_a7_a_acombout & !hc_cnt_cntr_awysi_counter_asload_path_a7_a 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a7_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a7_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a7_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a8_a_a700_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a8_a & (!hc_count_cmp_acomparator_acmp_end_alcarry_a7_a # !hc_threshold_a8_a_acombout) # !hc_cnt_cntr_awysi_counter_asload_path_a8_a & !hc_threshold_a8_a_acombout 
-- & !hc_count_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a8_a,
	datab => hc_threshold_a8_a_acombout,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a8_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a9_a_a699_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(hc_cnt_cntr_awysi_counter_asload_path_a9_a & hc_threshold_a9_a_acombout & !hc_count_cmp_acomparator_acmp_end_alcarry_a8_a # !hc_cnt_cntr_awysi_counter_asload_path_a9_a & (hc_threshold_a9_a_acombout # 
-- !hc_count_cmp_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_cnt_cntr_awysi_counter_asload_path_a9_a,
	datab => hc_threshold_a9_a_acombout,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a9_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a10_a_a698_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(hc_threshold_a10_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a10_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a9_a # !hc_threshold_a10_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a10_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a10_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a10_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a10_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a11_a_a697_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(hc_threshold_a11_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a10_a # !hc_cnt_cntr_awysi_counter_asload_path_a11_a) # !hc_threshold_a11_a_acombout & 
-- !hc_cnt_cntr_awysi_counter_asload_path_a11_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a11_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a11_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a11_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a12_a_a696_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(hc_threshold_a12_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a12_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a11_a # !hc_threshold_a12_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a12_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a12_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a12_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a12_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a13_a_a695_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(hc_threshold_a13_a_acombout & (!hc_count_cmp_acomparator_acmp_end_alcarry_a12_a # !hc_cnt_cntr_awysi_counter_asload_path_a13_a) # !hc_threshold_a13_a_acombout & 
-- !hc_cnt_cntr_awysi_counter_asload_path_a13_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a13_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a13_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a13_a);

hc_count_cmp_acomparator_acmp_end_alcarry_a14_a_a694_I : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_alcarry_a14_a = CARRY(hc_threshold_a14_a_acombout & hc_cnt_cntr_awysi_counter_asload_path_a14_a & !hc_count_cmp_acomparator_acmp_end_alcarry_a13_a # !hc_threshold_a14_a_acombout & 
-- (hc_cnt_cntr_awysi_counter_asload_path_a14_a # !hc_count_cmp_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => hc_threshold_a14_a_acombout,
	datab => hc_cnt_cntr_awysi_counter_asload_path_a14_a,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => hc_count_cmp_acomparator_acmp_end_alcarry_a14_a);

hc_count_cmp_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- hc_count_cmp_acomparator_acmp_end_aagb_out = hc_cnt_cntr_awysi_counter_asload_path_a15_a & (hc_count_cmp_acomparator_acmp_end_alcarry_a14_a # !hc_threshold_a15_a_acombout) # !hc_cnt_cntr_awysi_counter_asload_path_a15_a & 
-- hc_count_cmp_acomparator_acmp_end_alcarry_a14_a & !hc_threshold_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C0FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => hc_cnt_cntr_awysi_counter_asload_path_a15_a,
	datad => hc_threshold_a15_a_acombout,
	cin => hc_count_cmp_acomparator_acmp_end_alcarry_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => hc_count_cmp_acomparator_acmp_end_aagb_out);

Hi_Cnt_areg0_I : apex20ke_lcell
-- Equation(s):
-- Hi_Cnt_areg0 = DFFE(ms100tc_acombout & hc_count_cmp_acomparator_acmp_end_aagb_out & cps_cmp_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => ms100tc_acombout,
	datac => hc_count_cmp_acomparator_acmp_end_aagb_out,
	datad => cps_cmp_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Hi_Cnt_areg0);

NSlope_Cmp_out_areg0_I : apex20ke_lcell
-- Equation(s):
-- NSlope_Cmp_out_areg0 = DFFE(NSlope_Compare_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => NSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => NSlope_Cmp_out_areg0);

pslope_cmp_out_areg0_I : apex20ke_lcell
-- Equation(s):
-- pslope_cmp_out_areg0 = DFFE(PSlope_Compare_acomparator_acmp_end_aagb_out, GLOBAL(CLK_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => PSlope_Compare_acomparator_acmp_end_aagb_out,
	clk => CLK_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => pslope_cmp_out_areg0);

Reset_Time_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a0_a = DFFE(No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => No_Reset_Cmp_acomparator_acmp_end_alcarry_a0_a_a194,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a0_a);

Reset_Time_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a1_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a1_a);

Reset_Time_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a2_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a2_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a2_a);

Reset_Time_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a3_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a3_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a3_a);

Reset_Time_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a4_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a4_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a4_a);

Reset_Time_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a5_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a5_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a5_a);

Reset_Time_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a6_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a6_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a6_a);

Reset_Time_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a7_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a7_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a7_a);

Reset_Time_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a8_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a8_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a8_a);

Reset_Time_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a9_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a9_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a9_a);

Reset_Time_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a10_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a10_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a10_a);

Reset_Time_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a11_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a11_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a11_a);

Reset_Time_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a12_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a12_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a12_a);

Reset_Time_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a13_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a13_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a13_a);

Reset_Time_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a14_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a14_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a14_a);

Reset_Time_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a15_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a15_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a15_a);

Reset_Time_FF_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a16_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a16_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a16_a);

Reset_Time_FF_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a17_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a17_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a17_a);

Reset_Time_FF_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a18_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a18_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a18_a);

Reset_Time_FF_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a19_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a19_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a19_a);

Reset_Time_FF_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a20_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a20_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a20_a);

Reset_Time_FF_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a21_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a21_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a21_a);

Reset_Time_FF_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a22_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a22_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a22_a);

Reset_Time_FF_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a23_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a23_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a23_a);

Reset_Time_FF_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a24_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a24_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a24_a);

Reset_Time_FF_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a25_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a25_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a25_a);

Reset_Time_FF_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a26_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a26_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a26_a);

Reset_Time_FF_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a27_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a27_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a27_a);

Reset_Time_FF_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a28_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a28_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a28_a);

Reset_Time_FF_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a29_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a29_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a29_a);

Reset_Time_FF_adffs_a30_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a30_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a30_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a30_a);

Reset_Time_FF_adffs_a31_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_Time_FF_adffs_a31_a = DFFE(Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , Reset_LEd_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Time_Cnt_Cntr_awysi_counter_asload_path_a31_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Time_FF_adffs_a31_a);

Reset_Cntr_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY()

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Reset_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT);

Reset_Cntr_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a1_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a1_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a1_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT);

Reset_End_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End = DFFE(reduce_nor_a2 & (!Reset_areg0 & reset_Del) # !reduce_nor_a2 & (Reset_End # !Reset_areg0 & reset_Del), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4F44",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_a2,
	datab => Reset_End,
	datac => Reset_areg0,
	datad => reset_Del,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End);

Reset_End_Cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a0_a = DFFE(!Reset_End_Cnt_a0_a & (Reset_End), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Reset_End_Cnt_a0_a,
	datad => Reset_End,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a0_a);

Reset_End_Cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a1_a = DFFE(Reset_End & (Reset_End_Cnt_a0_a $ Reset_End_Cnt_a1_a), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0AA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End,
	datac => Reset_End_Cnt_a0_a,
	datad => Reset_End_Cnt_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a1_a);

add_a514_I : apex20ke_lcell
-- Equation(s):
-- add_a514 = Reset_End_Cnt_a0_a & Reset_End_Cnt_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_End_Cnt_a0_a,
	datad => Reset_End_Cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => add_a514);

Reset_End_Cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Reset_End_Cnt_a3_a = DFFE(Reset_End & (Reset_End_Cnt_a3_a $ (Reset_End_Cnt_a2_a & add_a514)), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7080",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a2_a,
	datab => add_a514,
	datac => Reset_End,
	datad => Reset_End_Cnt_a3_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_End_Cnt_a3_a);

reduce_nor_a2_I : apex20ke_lcell
-- Equation(s):
-- reduce_nor_a2 = Reset_End_Cnt_a2_a & Reset_End_Cnt_a3_a & Reset_End_Cnt_a0_a & Reset_End_Cnt_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_End_Cnt_a2_a,
	datab => Reset_End_Cnt_a3_a,
	datac => Reset_End_Cnt_a0_a,
	datad => Reset_End_Cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => reduce_nor_a2);

reset_width_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a0_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a1_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a1_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a0_a);

Reset_Cntr_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a2_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a2_a & (!Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a2_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a2_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT);

reset_width_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a1_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a2_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a2_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a1_a);

Reset_Cntr_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a3_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a3_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a3_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT);

reset_width_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a2_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a3_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a3_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a2_a);

Reset_Cntr_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a4_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a4_a & (!Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a4_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a4_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT);

reset_width_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a3_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a4_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a4_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a3_a);

Reset_Cntr_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a5_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a5_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a5_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT);

reset_width_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a4_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a5_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a5_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a4_a);

Reset_Cntr_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a6_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a6_a & (!Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a6_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a6_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT);

reset_width_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a5_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a6_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a6_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a5_a);

Reset_Cntr_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a7_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a7_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a7_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT);

reset_width_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a6_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a7_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a7_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a6_a);

Reset_Cntr_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a8_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a8_a & (!Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a8_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a8_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT);

reset_width_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a7_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a8_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a8_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a7_a);

Reset_Cntr_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a9_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_Cntr_awysi_counter_asload_path_a9_a,
	datab => Reset_areg0,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a9_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT);

reset_width_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a8_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a9_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a9_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a8_a);

Reset_Cntr_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a10_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a10_a & !Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a10_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a10_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT);

reset_width_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a9_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a10_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Cntr_awysi_counter_asload_path_a10_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a9_a);

Reset_Cntr_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a11_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a11_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a11_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT);

reset_width_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a10_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a11_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Cntr_awysi_counter_asload_path_a11_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a10_a);

Reset_Cntr_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a12_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a12_a & !Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a12_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a12_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT);

reset_width_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a11_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a12_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a12_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a11_a);

Reset_Cntr_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a13_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT # !Reset_Cntr_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a13_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a13_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT);

reset_width_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a12_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a13_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a13_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a12_a);

Reset_Cntr_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a14_a $ (Reset_areg0 & !Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(Reset_Cntr_awysi_counter_asload_path_a14_a & !Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datab => Reset_Cntr_awysi_counter_asload_path_a14_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a14_a,
	cout => Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT);

reset_width_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a13_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a14_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Reset_Cntr_awysi_counter_asload_path_a14_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a13_a);

Reset_Cntr_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- Reset_Cntr_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(Reset_LEd_a0) & Reset_Cntr_awysi_counter_asload_path_a15_a $ (Reset_areg0 & Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Reset_areg0,
	datad => Reset_Cntr_awysi_counter_asload_path_a15_a,
	cin => Reset_Cntr_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => Reset_LEd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Reset_Cntr_awysi_counter_asload_path_a15_a);

reset_width_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- reset_width_ff_adffs_a14_a = DFFE(Reset_Cntr_awysi_counter_asload_path_a15_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , reduce_nor_a2)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Reset_Cntr_awysi_counter_asload_path_a15_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => reset_width_ff_adffs_a14_a);

AD_CLR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AD_CLR_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AD_CLR);

det_sat_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => det_sat_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_det_sat);

Hi_Cnt_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Hi_Cnt_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Hi_Cnt);

Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset);

NSlope_Cmp_out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => NSlope_Cmp_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_NSlope_Cmp_out);

pslope_cmp_out_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => pslope_cmp_out_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_pslope_cmp_out);

Reset_time_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(0));

Reset_time_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(1));

Reset_time_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(2));

Reset_time_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(3));

Reset_time_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(4));

Reset_time_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(5));

Reset_time_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(6));

Reset_time_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(7));

Reset_time_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(8));

Reset_time_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(9));

Reset_time_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(10));

Reset_time_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(11));

Reset_time_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(12));

Reset_time_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(13));

Reset_time_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(14));

Reset_time_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(15));

Reset_time_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(16));

Reset_time_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(17));

Reset_time_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a18_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(18));

Reset_time_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a19_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(19));

Reset_time_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(20));

Reset_time_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(21));

Reset_time_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(22));

Reset_time_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(23));

Reset_time_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a24_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(24));

Reset_time_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a25_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(25));

Reset_time_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a26_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(26));

Reset_time_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a27_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(27));

Reset_time_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a28_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(28));

Reset_time_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a29_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(29));

Reset_time_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a30_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(30));

Reset_time_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => Reset_Time_FF_adffs_a31_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_time(31));

Reset_Width_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(0));

Reset_Width_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(1));

Reset_Width_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(2));

Reset_Width_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(3));

Reset_Width_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(4));

Reset_Width_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(5));

Reset_Width_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(6));

Reset_Width_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(7));

Reset_Width_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(8));

Reset_Width_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(9));

Reset_Width_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(10));

Reset_Width_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(11));

Reset_Width_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(12));

Reset_Width_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(13));

Reset_Width_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => reset_width_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(14));

Reset_Width_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => GND,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Reset_Width(15));
END structure;


