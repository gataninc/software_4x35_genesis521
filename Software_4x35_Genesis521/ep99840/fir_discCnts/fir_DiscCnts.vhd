---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_disccnts.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
---------------------------------------------------------------------------------------------------

library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

---------------------------------------------------------------------------------------------------
entity fir_disccnts is 
	port( 
		IMR			: in		std_Logic;					-- Master Reset
		CLK			: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
		PA_Reset		: in		std_logic;
		TimeEnable	: in		std_logic;
		TimeClr		: in		std_logic;
		Disc_In		: in		std_logic;
		Cnts			: buffer	std_logic_Vector(  7 downto 0 ) );
end fir_disccnts;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of fir_disccnts is

	signal Disc_In_Vec  : std_logic_Vector( 1 downto 0 );
	signal MaxCnts		: std_logic;
begin
	MaxCnts 	<= '1' when ( Cnts = x"FF" ) else '0';
     ----------------------------------------------------------------------------------------------
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			Disc_In_Vec 	<= "00";
			Cnts			<= x"00";
		elsif(( clk'event ) and ( clk = '1' )) then
			Disc_In_Vec <= Disc_in_Vec(0) & Disc_in;
			
			if( TimeClr = '1' )
				then Cnts <= x"00";
			elsif(( TimeEnable = '1' ) and ( Disc_In_Vec = "01" ) and ( PA_Reset = '0' ) and ( MaxCnts = '0' ))
				then Cnts <= Cnts + 1;
			end if;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
end behavioral;			-- fir_disccnts.VHD
---------------------------------------------------------------------------------------------------

