onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk
add wave -noupdate -format Logic -radix binary /tb/pa_reset
add wave -noupdate -format Logic -radix binary /tb/timeenable
add wave -noupdate -format Logic -radix binary /tb/timeclr
add wave -noupdate -format Logic -radix binary /tb/disc_in
add wave -noupdate -format Logic -radix hexadecimal /tb/cnts
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {12483 ns}
WaveRestoreZoom {11616 ns} {13467 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 78
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
