---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_disccnts.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------


library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
entity tb is
end tb;

architecture test_bench of tb is
	signal imr 		: std_logic := '1';
	signal clk		: std_logic := '0';
	signal PA_Reset	: std_logic;
	signal timeEnable	: std_logic;
	signal TimeCLr		: std_logic;
	signal Disc_In		: std_logic;
	signal Cnts		: std_logic_Vector( 7 downto 0 );
	---------------------------------------------------------------------------------------------------
	component fir_disccnts
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK			: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			PA_Reset		: in		std_logic;
			TimeEnable	: in		std_logic;
			TimeClr		: in		std_logic;
			Disc_In		: in		std_logic;
			Cnts			: out	std_logic_Vector(  7 downto 0 ) );
	end component fir_disccnts;
	---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk		<= not( clk ) after 25 ns;
	
	U : fir_disccnts
		port map(
			imr			=> imr,
			clk			=> clk,
			pa_reset		=> pa_Reset,
			timeenable	=> timeenable,
			timeclr		=> timeclr,
			disc_in		=> disc_in,
			cnts			=> cnts );
			
	disc_in_proc : process
	begin
		disc_in <= '0';
		wait for 5 us;
		forever_loop : loop
			wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
			disc_in <= '1';
			wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
			disc_in <= '0';
			wait for 300 ns;
		end loop;
	end process;
	
	main_proc : process
	begin
		PA_Reset		<= '0';
		timeenable	<= '0';
		timeclr		<= '0';
		wait for 1 us;
		wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
		TimeCLr		<= '1';
		wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
		TimeCLr		<= '0';
		
		wait for 5 us;
		wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
		TimeEnable		<= '1';
		
		wait for 5 us;
		wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
		Timeclr		<= '1';
		wait until (( Clk'event ) and ( clk = '1' )); wait for 7 ns;
		Timeclr		<= '0';
		
		wait ;
	end process;
---------------------------------------------------------------------------------------------------
end test_bench;			-- fir_disccnts.VHD
---------------------------------------------------------------------------------------------------

