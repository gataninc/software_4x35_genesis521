-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:03:44"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_disccnts IS
    PORT (
	IMR : IN std_logic;
	CLK : IN std_logic;
	PA_Reset : IN std_logic;
	TimeEnable : IN std_logic;
	TimeClr : IN std_logic;
	Disc_In : IN std_logic;
	Cnts : OUT std_logic_vector(7 DOWNTO 0)
	);
END fir_disccnts;

ARCHITECTURE structure OF fir_disccnts IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_TimeEnable : std_logic;
SIGNAL ww_TimeClr : std_logic;
SIGNAL ww_Disc_In : std_logic;
SIGNAL ww_Cnts : std_logic_vector(7 DOWNTO 0);
SIGNAL Equal_a78 : std_logic;
SIGNAL CLK_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL TimeClr_acombout : std_logic;
SIGNAL Cnts_a0_a_a73 : std_logic;
SIGNAL Cnts_a1_a_areg0 : std_logic;
SIGNAL Cnts_a1_a_a76 : std_logic;
SIGNAL Cnts_a2_a_areg0 : std_logic;
SIGNAL Cnts_a2_a_a79 : std_logic;
SIGNAL Cnts_a3_a_a82 : std_logic;
SIGNAL Cnts_a4_a_a85 : std_logic;
SIGNAL Cnts_a5_a_areg0 : std_logic;
SIGNAL Cnts_a5_a_a88 : std_logic;
SIGNAL Cnts_a6_a_areg0 : std_logic;
SIGNAL Cnts_a6_a_a91 : std_logic;
SIGNAL Cnts_a7_a_areg0 : std_logic;
SIGNAL Equal_a82 : std_logic;
SIGNAL Equal_a80 : std_logic;
SIGNAL Disc_In_acombout : std_logic;
SIGNAL Disc_In_Vec_a0_a : std_logic;
SIGNAL Disc_In_Vec_a1_a : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL TimeEnable_acombout : std_logic;
SIGNAL clock_proc_a29 : std_logic;
SIGNAL clock_proc_a2 : std_logic;
SIGNAL Cnts_a0_a_areg0 : std_logic;
SIGNAL Cnts_a3_a_areg0 : std_logic;
SIGNAL Cnts_a4_a_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK <= CLK;
ww_PA_Reset <= PA_Reset;
ww_TimeEnable <= TimeEnable;
ww_TimeClr <= TimeClr;
ww_Disc_In <= Disc_In;
Cnts <= ww_Cnts;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK,
	combout => CLK_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

TimeClr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeClr,
	combout => TimeClr_acombout);

Cnts_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a0_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & clock_proc_a2 $ Cnts_a0_a_areg0, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a0_a_a73 = CARRY(clock_proc_a2 & Cnts_a0_a_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => clock_proc_a2,
	datab => Cnts_a0_a_areg0,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a0_a_areg0,
	cout => Cnts_a0_a_a73);

Cnts_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a1_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a1_a_areg0 $ Cnts_a0_a_a73, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a1_a_a76 = CARRY(!Cnts_a0_a_a73 # !Cnts_a1_a_areg0)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cnts_a1_a_areg0,
	cin => Cnts_a0_a_a73,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a1_a_areg0,
	cout => Cnts_a1_a_a76);

Cnts_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a2_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a2_a_areg0 $ !Cnts_a1_a_a76, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a2_a_a79 = CARRY(Cnts_a2_a_areg0 & !Cnts_a1_a_a76)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cnts_a2_a_areg0,
	cin => Cnts_a1_a_a76,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a2_a_areg0,
	cout => Cnts_a2_a_a79);

Cnts_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a3_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a3_a_areg0 $ (Cnts_a2_a_a79), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a3_a_a82 = CARRY(!Cnts_a2_a_a79 # !Cnts_a3_a_areg0)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cnts_a3_a_areg0,
	cin => Cnts_a2_a_a79,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a3_a_areg0,
	cout => Cnts_a3_a_a82);

Cnts_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a4_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a4_a_areg0 $ (!Cnts_a3_a_a82), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a4_a_a85 = CARRY(Cnts_a4_a_areg0 & (!Cnts_a3_a_a82))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cnts_a4_a_areg0,
	cin => Cnts_a3_a_a82,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a4_a_areg0,
	cout => Cnts_a4_a_a85);

Cnts_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a5_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a5_a_areg0 $ Cnts_a4_a_a85, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a5_a_a88 = CARRY(!Cnts_a4_a_a85 # !Cnts_a5_a_areg0)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cnts_a5_a_areg0,
	cin => Cnts_a4_a_a85,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a5_a_areg0,
	cout => Cnts_a5_a_a88);

Cnts_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a6_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a6_a_areg0 $ !Cnts_a5_a_a88, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )
-- Cnts_a6_a_a91 = CARRY(Cnts_a6_a_areg0 & !Cnts_a5_a_a88)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Cnts_a6_a_areg0,
	cin => Cnts_a5_a_a88,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a6_a_areg0,
	cout => Cnts_a6_a_a91);

Cnts_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- Cnts_a7_a_areg0 = DFFE(!GLOBAL(TimeClr_acombout) & Cnts_a7_a_areg0 $ (Cnts_a6_a_a91), GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cnts_a7_a_areg0,
	cin => Cnts_a6_a_a91,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	sclr => TimeClr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Cnts_a7_a_areg0);

Equal_a78_I : apex20ke_lcell
-- Equation(s):
-- Equal_a82 = Cnts_a3_a_areg0 & Cnts_a2_a_areg0 & Cnts_a0_a_areg0 & Cnts_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cnts_a3_a_areg0,
	datab => Cnts_a2_a_areg0,
	datac => Cnts_a0_a_areg0,
	datad => Cnts_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a78,
	cascout => Equal_a82);

Equal_a80_I : apex20ke_lcell
-- Equation(s):
-- Equal_a80 = (Cnts_a4_a_areg0 & Cnts_a6_a_areg0 & Cnts_a7_a_areg0 & Cnts_a5_a_areg0) & CASCADE(Equal_a82)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Cnts_a4_a_areg0,
	datab => Cnts_a6_a_areg0,
	datac => Cnts_a7_a_areg0,
	datad => Cnts_a5_a_areg0,
	cascin => Equal_a82,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a80);

Disc_In_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Disc_In,
	combout => Disc_In_acombout);

Disc_In_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_In_Vec_a0_a = DFFE(Disc_In_acombout, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Disc_In_acombout,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_In_Vec_a0_a);

Disc_In_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Disc_In_Vec_a1_a = DFFE(Disc_In_Vec_a0_a, GLOBAL(CLK_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Disc_In_Vec_a0_a,
	clk => CLK_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Disc_In_Vec_a1_a);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

TimeEnable_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_TimeEnable,
	combout => TimeEnable_acombout);

clock_proc_a29_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a29 = !PA_Reset_acombout & TimeEnable_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => PA_Reset_acombout,
	datad => TimeEnable_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a29);

clock_proc_a2_I : apex20ke_lcell
-- Equation(s):
-- clock_proc_a2 = Disc_In_Vec_a0_a & !Equal_a80 & !Disc_In_Vec_a1_a & clock_proc_a29

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Disc_In_Vec_a0_a,
	datab => Equal_a80,
	datac => Disc_In_Vec_a1_a,
	datad => clock_proc_a29,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => clock_proc_a2);

Cnts_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(0));

Cnts_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(1));

Cnts_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(2));

Cnts_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(3));

Cnts_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(4));

Cnts_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(5));

Cnts_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(6));

Cnts_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => Cnts_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_Cnts(7));
END structure;


