---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ads8321.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--   History:       <date> - <Author>
--		February 8, 2001 - MCS
--			Timing change clock to 20Mhz from 10Mhz
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal IMR		: std_logic := '1';
	signal clK20		: std_logic := '0';
	signal Data_Ld		: std_logic;			-- Strobe to Load the Data
	signal iData		: std_logic_vector( 15 downto 0 );
	signal Data		: std_logic_vector( 15 downto 0 );
	signal DAC_LD		: std_logic;			-- Signal indicating data Load is Done
	signal DAC_CLK		: std_logic;			-- DAC clock
	signal DAC_DATA	: std_logic;			-- DAC Data
	signal we_Dcd		: std_logic;
	signal dac_sr		: std_logic_vector( 15 downto 0 ) := x"0000";
	signal dac		: std_logic_vector( 15 downto 0 ) := x"0000";
---------------------------------------------------------------------------------------------------
	component LTC1595 
		port( 
		     IMR            : in      std_logic;				     -- Master Reset
			CLK20		: in		std_logic;
			Data_Ld		: in		std_logic;			-- Strobe to Load the Data
			Data			: in		std_logic_vector( 15 downto 0 );
			DAC_LD		: out	std_logic;			-- Signal indicating data Load is Done
			DAC_CLK		: out	std_logic;			-- DAC clock
			DAC_DATA		: out	std_logic );			-- DAC Data
	end component LTC1595;

	---------------------------------------------------------------------------------------------------

begin
     ----------------------------------------------------------------------------------------------
	U : LTC1595
		port map(
			IMR		=> IMR,
			CLK20	=> CLK20,
			Data_Ld	=> data_ld,
			Data		=> data,
			DAC_LD	=> dac_LD,
			DAC_CLK	=> dac_clk,
			DAC_DATA	=> dac_data );

	imr	<= '0' after 350 ns;
	CLK20 <= not( CLK20 ) after 25 ns;

	
     ----------------------------------------------------------------------------------------------
	clk_proc : process
	begin
		data_ld 	<= '0';
		data		<= x"0000";
		wait for 1 us;	
		
		for i in 0 to 15 loop
			wait until(( CLK20'Event ) and ( CLK20 = '1' ));
			data_ld 	<= '1';
			data(i)	<= '1';
			wait until(( CLK20'Event ) and ( CLK20 = '1' ));
			data_ld <= '0';
		
		
			wait until(( dac_ld'event ) and ( dac_ld = '1' ));
			data(i) <= '0';
			wait for 50 ns;
		end loop;
		assert false
			report "End of Simulations"
			severity failure;
		
		wait ;
	end process;
     ----------------------------------------------------------------------------------------------
	
     ----------------------------------------------------------------------------------------------
	dacsr_proc : process( dac_clk )
	begin
		if(( dac_clk'event ) and ( dac_clk = '1' )) then
			dac_sr <= dac_sr( 14 downto 0 ) & dac_data;
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

	dac_proc : process( dac_ld )
	begin
		if(( dac_ld'event ) and ( dac_ld = '1' )) then
			dac <= dac_sr;
		end if;
	end process;
		
		
---------------------------------------------------------------------------------------------------
end test_bench;			-- ads8321.VHD
---------------------------------------------------------------------------------------------------

