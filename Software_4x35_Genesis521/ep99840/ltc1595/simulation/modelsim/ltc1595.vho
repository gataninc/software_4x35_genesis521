-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 10:38:40"

--
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	ltc1595 IS
    PORT (
	CLK20 : IN std_logic;
	IMR : IN std_logic;
	Data_Ld : IN std_logic;
	Data : IN std_logic_vector(15 DOWNTO 0);
	DAC_LD : OUT std_logic;
	DAC_CLK : OUT std_logic;
	DAC_DATA : OUT std_logic
	);
END ltc1595;

ARCHITECTURE structure OF ltc1595 IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_Data_Ld : std_logic;
SIGNAL ww_Data : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DAC_LD : std_logic;
SIGNAL ww_DAC_CLK : std_logic;
SIGNAL ww_DAC_DATA : std_logic;
SIGNAL CLK20_apadio : std_logic;
SIGNAL IMR_apadio : std_logic;
SIGNAL Data_Ld_apadio : std_logic;
SIGNAL Data_a15_a_apadio : std_logic;
SIGNAL Data_a14_a_apadio : std_logic;
SIGNAL Data_a13_a_apadio : std_logic;
SIGNAL Data_a12_a_apadio : std_logic;
SIGNAL Data_a11_a_apadio : std_logic;
SIGNAL Data_a10_a_apadio : std_logic;
SIGNAL Data_a9_a_apadio : std_logic;
SIGNAL Data_a8_a_apadio : std_logic;
SIGNAL Data_a7_a_apadio : std_logic;
SIGNAL Data_a6_a_apadio : std_logic;
SIGNAL Data_a5_a_apadio : std_logic;
SIGNAL Data_a4_a_apadio : std_logic;
SIGNAL Data_a3_a_apadio : std_logic;
SIGNAL Data_a2_a_apadio : std_logic;
SIGNAL Data_a1_a_apadio : std_logic;
SIGNAL Data_a0_a_apadio : std_logic;
SIGNAL DAC_LD_apadio : std_logic;
SIGNAL DAC_CLK_apadio : std_logic;
SIGNAL DAC_DATA_apadio : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL Data_Ld_acombout : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL reduce_nor_4_a21 : std_logic;
SIGNAL reduce_nor_4 : std_logic;
SIGNAL shift_en : std_logic;
SIGNAL i_a0 : std_logic;
SIGNAL clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL clk_cnt_rtl_0_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL clk_cnt_rtl_0_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL reduce_nor_7 : std_logic;
SIGNAL shift_cnt_rtl_1_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Pre_Dac_Ld : std_logic;
SIGNAL DAC_LD_areg0 : std_logic;
SIGNAL iDAC_CLK : std_logic;
SIGNAL DAC_CLK_areg0 : std_logic;
SIGNAL Data_a15_a_acombout : std_logic;
SIGNAL i_a158 : std_logic;
SIGNAL Data_a14_a_acombout : std_logic;
SIGNAL i_a162 : std_logic;
SIGNAL Data_a13_a_acombout : std_logic;
SIGNAL Data_a12_a_acombout : std_logic;
SIGNAL Data_a11_a_acombout : std_logic;
SIGNAL Data_a10_a_acombout : std_logic;
SIGNAL Data_a9_a_acombout : std_logic;
SIGNAL Data_a7_a_acombout : std_logic;
SIGNAL i_a190 : std_logic;
SIGNAL Data_a6_a_acombout : std_logic;
SIGNAL Data_a5_a_acombout : std_logic;
SIGNAL i_a198 : std_logic;
SIGNAL Data_a4_a_acombout : std_logic;
SIGNAL i_a206 : std_logic;
SIGNAL Data_a3_a_acombout : std_logic;
SIGNAL Data_a0_a_acombout : std_logic;
SIGNAL i_a217 : std_logic;
SIGNAL i_a218 : std_logic;
SIGNAL dac_sr_a0_a : std_logic;
SIGNAL i_a213 : std_logic;
SIGNAL Data_a1_a_acombout : std_logic;
SIGNAL i_a214 : std_logic;
SIGNAL dac_sr_a1_a : std_logic;
SIGNAL i_a209 : std_logic;
SIGNAL Data_a2_a_acombout : std_logic;
SIGNAL i_a210 : std_logic;
SIGNAL dac_sr_a2_a : std_logic;
SIGNAL i_a205 : std_logic;
SIGNAL dac_sr_a3_a : std_logic;
SIGNAL i_a201 : std_logic;
SIGNAL i_a202 : std_logic;
SIGNAL dac_sr_a4_a : std_logic;
SIGNAL i_a197 : std_logic;
SIGNAL dac_sr_a5_a : std_logic;
SIGNAL i_a193 : std_logic;
SIGNAL i_a194 : std_logic;
SIGNAL dac_sr_a6_a : std_logic;
SIGNAL i_a189 : std_logic;
SIGNAL dac_sr_a7_a : std_logic;
SIGNAL i_a185 : std_logic;
SIGNAL i_a186 : std_logic;
SIGNAL Data_a8_a_acombout : std_logic;
SIGNAL dac_sr_a8_a : std_logic;
SIGNAL i_a181 : std_logic;
SIGNAL i_a182 : std_logic;
SIGNAL dac_sr_a9_a : std_logic;
SIGNAL i_a177 : std_logic;
SIGNAL i_a178 : std_logic;
SIGNAL dac_sr_a10_a : std_logic;
SIGNAL i_a173 : std_logic;
SIGNAL i_a174 : std_logic;
SIGNAL dac_sr_a11_a : std_logic;
SIGNAL i_a169 : std_logic;
SIGNAL i_a170 : std_logic;
SIGNAL dac_sr_a12_a : std_logic;
SIGNAL i_a165 : std_logic;
SIGNAL i_a166 : std_logic;
SIGNAL dac_sr_a13_a : std_logic;
SIGNAL i_a161 : std_logic;
SIGNAL dac_sr_a14_a : std_logic;
SIGNAL i_a157 : std_logic;
SIGNAL dac_sr_a15_a : std_logic;
SIGNAL DAC_DATA_areg0 : std_logic;
SIGNAL NOT_DAC_LD_areg0 : std_logic;
SIGNAL NOT_shift_en : std_logic;

BEGIN

ww_CLK20 <= CLK20;
ww_IMR <= IMR;
ww_Data_Ld <= Data_Ld;
ww_Data <= Data;
DAC_LD <= ww_DAC_LD;
DAC_CLK <= ww_DAC_CLK;
DAC_DATA <= ww_DAC_DATA;
NOT_DAC_LD_areg0 <= NOT DAC_LD_areg0;
NOT_shift_en <= NOT shift_en;

CLK20_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

Data_Ld_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data_Ld,
	combout => Data_Ld_acombout);

shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- shift_cnt_rtl_1_awysi_counter_asload_path_a0_a = DFFE(GLOBAL(shift_en) & reduce_nor_7 $ shift_cnt_rtl_1_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(shift_cnt_rtl_1_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "3CF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reduce_nor_7,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_shift_en,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_cnt_rtl_1_awysi_counter_asload_path_a0_a,
	cout => shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT);

shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- shift_cnt_rtl_1_awysi_counter_asload_path_a1_a = DFFE(GLOBAL(shift_en) & shift_cnt_rtl_1_awysi_counter_asload_path_a1_a $ (reduce_nor_7 & shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT # !shift_cnt_rtl_1_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_cnt_rtl_1_awysi_counter_asload_path_a1_a,
	datab => reduce_nor_7,
	cin => shift_cnt_rtl_1_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_shift_en,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_cnt_rtl_1_awysi_counter_asload_path_a1_a,
	cout => shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT);

shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- shift_cnt_rtl_1_awysi_counter_asload_path_a2_a = DFFE(GLOBAL(shift_en) & shift_cnt_rtl_1_awysi_counter_asload_path_a2_a $ (reduce_nor_7 & !shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(shift_cnt_rtl_1_awysi_counter_asload_path_a2_a & !shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_7,
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a2_a,
	cin => shift_cnt_rtl_1_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_shift_en,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_cnt_rtl_1_awysi_counter_asload_path_a2_a,
	cout => shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT);

shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- shift_cnt_rtl_1_awysi_counter_asload_path_a3_a = DFFE(GLOBAL(shift_en) & shift_cnt_rtl_1_awysi_counter_asload_path_a3_a $ (reduce_nor_7 & shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT # !shift_cnt_rtl_1_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_7,
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a3_a,
	cin => shift_cnt_rtl_1_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_shift_en,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_cnt_rtl_1_awysi_counter_asload_path_a3_a,
	cout => shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT);

shift_cnt_rtl_1_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- shift_cnt_rtl_1_awysi_counter_asload_path_a4_a = DFFE(GLOBAL(shift_en) & shift_cnt_rtl_1_awysi_counter_asload_path_a4_a $ (!shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT & reduce_nor_7), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C3CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a4_a,
	datad => reduce_nor_7,
	cin => shift_cnt_rtl_1_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => NOT_shift_en,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_cnt_rtl_1_awysi_counter_asload_path_a4_a);

reduce_nor_4_a21_I : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_4_a21 = shift_cnt_rtl_1_awysi_counter_asload_path_a3_a # !shift_cnt_rtl_1_awysi_counter_asload_path_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF33",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a4_a,
	datad => shift_cnt_rtl_1_awysi_counter_asload_path_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_4_a21);

reduce_nor_4_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_4 = shift_cnt_rtl_1_awysi_counter_asload_path_a1_a # shift_cnt_rtl_1_awysi_counter_asload_path_a2_a # shift_cnt_rtl_1_awysi_counter_asload_path_a0_a # reduce_nor_4_a21

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_cnt_rtl_1_awysi_counter_asload_path_a1_a,
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a2_a,
	datac => shift_cnt_rtl_1_awysi_counter_asload_path_a0_a,
	datad => reduce_nor_4_a21,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_4);

shift_en_aI : apex20ke_lcell 
-- Equation(s):
-- shift_en = DFFE(Data_Ld_acombout # shift_en & (reduce_nor_4 # !reduce_nor_7), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FABA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => reduce_nor_7,
	datac => shift_en,
	datad => reduce_nor_4,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => shift_en);

i_a0_I : apex20ke_lcell 
-- Equation(s):
-- i_a0 = !shift_en & !Pre_Dac_Ld

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => shift_en,
	datad => Pre_Dac_Ld,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a0);

clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- clk_cnt_rtl_0_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(i_a0) & !clk_cnt_rtl_0_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(clk_cnt_rtl_0_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => i_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => clk_cnt_rtl_0_awysi_counter_asload_path_a0_a,
	cout => clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT);

clk_cnt_rtl_0_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- clk_cnt_rtl_0_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(i_a0) & clk_cnt_rtl_0_awysi_counter_asload_path_a1_a $ clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => clk_cnt_rtl_0_awysi_counter_asload_path_a1_a,
	cin => clk_cnt_rtl_0_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => i_a0,
	devclrn => devclrn,
	devpor => devpor,
	regout => clk_cnt_rtl_0_awysi_counter_asload_path_a1_a);

reduce_nor_7_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_7 = clk_cnt_rtl_0_awysi_counter_asload_path_a1_a & clk_cnt_rtl_0_awysi_counter_asload_path_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => clk_cnt_rtl_0_awysi_counter_asload_path_a1_a,
	datad => clk_cnt_rtl_0_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_7);

Pre_Dac_Ld_aI : apex20ke_lcell 
-- Equation(s):
-- Pre_Dac_Ld = DFFE(!shift_cnt_rtl_1_awysi_counter_asload_path_a0_a & !shift_cnt_rtl_1_awysi_counter_asload_path_a2_a & !shift_cnt_rtl_1_awysi_counter_asload_path_a1_a & !reduce_nor_4_a21, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_cnt_rtl_1_awysi_counter_asload_path_a0_a,
	datab => shift_cnt_rtl_1_awysi_counter_asload_path_a2_a,
	datac => shift_cnt_rtl_1_awysi_counter_asload_path_a1_a,
	datad => reduce_nor_4_a21,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	regout => Pre_Dac_Ld);

DAC_LD_areg0_I : apex20ke_lcell 
-- Equation(s):
-- DAC_LD_areg0 = DFFE(Pre_Dac_Ld, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Pre_Dac_Ld,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => DAC_LD_areg0);

iDAC_CLK_aI : apex20ke_lcell 
-- Equation(s):
-- iDAC_CLK = DFFE(shift_en & !clk_cnt_rtl_0_awysi_counter_asload_path_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => shift_en,
	datad => clk_cnt_rtl_0_awysi_counter_asload_path_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => iDAC_CLK);

DAC_CLK_areg0_I : apex20ke_lcell 
-- Equation(s):
-- DAC_CLK_areg0 = DFFE(iDAC_CLK, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => iDAC_CLK,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => DAC_CLK_areg0);

Data_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(15),
	combout => Data_a15_a_acombout);

i_a158_I : apex20ke_lcell 
-- Equation(s):
-- i_a158 = !Data_Ld_acombout & dac_sr_a15_a & (!shift_en # !reduce_nor_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_7,
	datab => Data_Ld_acombout,
	datac => dac_sr_a15_a,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a158);

Data_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(14),
	combout => Data_a14_a_acombout);

i_a162_I : apex20ke_lcell 
-- Equation(s):
-- i_a162 = !Data_Ld_acombout & dac_sr_a14_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => Data_Ld_acombout,
	datac => dac_sr_a14_a,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a162);

Data_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(13),
	combout => Data_a13_a_acombout);

Data_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(12),
	combout => Data_a12_a_acombout);

Data_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(11),
	combout => Data_a11_a_acombout);

Data_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(10),
	combout => Data_a10_a_acombout);

Data_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(9),
	combout => Data_a9_a_acombout);

Data_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(7),
	combout => Data_a7_a_acombout);

i_a190_I : apex20ke_lcell 
-- Equation(s):
-- i_a190 = !Data_Ld_acombout & dac_sr_a7_a & (!shift_en # !reduce_nor_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0444",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a7_a,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a190);

Data_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(6),
	combout => Data_a6_a_acombout);

Data_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(5),
	combout => Data_a5_a_acombout);

i_a198_I : apex20ke_lcell 
-- Equation(s):
-- i_a198 = dac_sr_a5_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "040C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => dac_sr_a5_a,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a198);

Data_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(4),
	combout => Data_a4_a_acombout);

i_a206_I : apex20ke_lcell 
-- Equation(s):
-- i_a206 = dac_sr_a3_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "020A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a3_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a206);

Data_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(3),
	combout => Data_a3_a_acombout);

Data_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(0),
	combout => Data_a0_a_acombout);

i_a217_I : apex20ke_lcell 
-- Equation(s):
-- i_a217 = dac_sr_a15_a & !Data_Ld_acombout & reduce_nor_7 & shift_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a15_a,
	datab => Data_Ld_acombout,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a217);

i_a218_I : apex20ke_lcell 
-- Equation(s):
-- i_a218 = dac_sr_a0_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "020A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a0_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a218);

dac_sr_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a0_a = DFFE(i_a217 # i_a218 # Data_a0_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a0_a_acombout,
	datab => Data_Ld_acombout,
	datac => i_a217,
	datad => i_a218,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a0_a);

i_a213_I : apex20ke_lcell 
-- Equation(s):
-- i_a213 = dac_sr_a0_a & shift_en & !Data_Ld_acombout & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a0_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a213);

Data_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(1),
	combout => Data_a1_a_acombout);

i_a214_I : apex20ke_lcell 
-- Equation(s):
-- i_a214 = dac_sr_a1_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "020A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a1_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a214);

dac_sr_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a1_a = DFFE(i_a213 # i_a214 # Data_Ld_acombout & Data_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => i_a213,
	datac => Data_a1_a_acombout,
	datad => i_a214,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a1_a);

i_a209_I : apex20ke_lcell 
-- Equation(s):
-- i_a209 = dac_sr_a1_a & shift_en & !Data_Ld_acombout & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a1_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a209);

Data_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(2),
	combout => Data_a2_a_acombout);

i_a210_I : apex20ke_lcell 
-- Equation(s):
-- i_a210 = dac_sr_a2_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "020A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a2_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a210);

dac_sr_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a2_a = DFFE(i_a209 # i_a210 # Data_Ld_acombout & Data_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => i_a209,
	datac => Data_a2_a_acombout,
	datad => i_a210,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a2_a);

i_a205_I : apex20ke_lcell 
-- Equation(s):
-- i_a205 = dac_sr_a2_a & shift_en & !Data_Ld_acombout & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => dac_sr_a2_a,
	datab => shift_en,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a205);

dac_sr_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a3_a = DFFE(i_a206 # i_a205 # Data_Ld_acombout & Data_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => i_a206,
	datac => Data_a3_a_acombout,
	datad => i_a205,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a3_a);

i_a201_I : apex20ke_lcell 
-- Equation(s):
-- i_a201 = !Data_Ld_acombout & shift_en & dac_sr_a3_a & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => shift_en,
	datac => dac_sr_a3_a,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a201);

i_a202_I : apex20ke_lcell 
-- Equation(s):
-- i_a202 = dac_sr_a4_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "040C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => dac_sr_a4_a,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a202);

dac_sr_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a4_a = DFFE(i_a201 # i_a202 # Data_Ld_acombout & Data_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a4_a_acombout,
	datac => i_a201,
	datad => i_a202,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a4_a);

i_a197_I : apex20ke_lcell 
-- Equation(s):
-- i_a197 = shift_en & dac_sr_a4_a & !Data_Ld_acombout & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => dac_sr_a4_a,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a197);

dac_sr_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a5_a = DFFE(i_a198 # i_a197 # Data_Ld_acombout & Data_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a5_a_acombout,
	datac => i_a198,
	datad => i_a197,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a5_a);

i_a193_I : apex20ke_lcell 
-- Equation(s):
-- i_a193 = shift_en & dac_sr_a5_a & !Data_Ld_acombout & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => dac_sr_a5_a,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a193);

i_a194_I : apex20ke_lcell 
-- Equation(s):
-- i_a194 = dac_sr_a6_a & !Data_Ld_acombout & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "040C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => dac_sr_a6_a,
	datac => Data_Ld_acombout,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a194);

dac_sr_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a6_a = DFFE(i_a193 # i_a194 # Data_a6_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a6_a_acombout,
	datab => i_a193,
	datac => Data_Ld_acombout,
	datad => i_a194,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a6_a);

i_a189_I : apex20ke_lcell 
-- Equation(s):
-- i_a189 = !Data_Ld_acombout & dac_sr_a6_a & shift_en & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a6_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a189);

dac_sr_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a7_a = DFFE(i_a190 # i_a189 # Data_Ld_acombout & Data_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a7_a_acombout,
	datac => i_a190,
	datad => i_a189,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a7_a);

i_a185_I : apex20ke_lcell 
-- Equation(s):
-- i_a185 = !Data_Ld_acombout & dac_sr_a7_a & reduce_nor_7 & shift_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a7_a,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a185);

i_a186_I : apex20ke_lcell 
-- Equation(s):
-- i_a186 = !Data_Ld_acombout & dac_sr_a8_a & (!shift_en # !reduce_nor_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0444",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a8_a,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a186);

Data_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Data(8),
	combout => Data_a8_a_acombout);

dac_sr_a8_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a8_a = DFFE(i_a185 # i_a186 # Data_Ld_acombout & Data_a8_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FEFA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => i_a185,
	datab => Data_Ld_acombout,
	datac => i_a186,
	datad => Data_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a8_a);

i_a181_I : apex20ke_lcell 
-- Equation(s):
-- i_a181 = !Data_Ld_acombout & dac_sr_a8_a & reduce_nor_7 & shift_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a8_a,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a181);

i_a182_I : apex20ke_lcell 
-- Equation(s):
-- i_a182 = !Data_Ld_acombout & dac_sr_a9_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0444",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a9_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a182);

dac_sr_a9_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a9_a = DFFE(i_a181 # i_a182 # Data_Ld_acombout & Data_a9_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a9_a_acombout,
	datac => i_a181,
	datad => i_a182,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a9_a);

i_a177_I : apex20ke_lcell 
-- Equation(s):
-- i_a177 = !Data_Ld_acombout & dac_sr_a9_a & shift_en & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a9_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a177);

i_a178_I : apex20ke_lcell 
-- Equation(s):
-- i_a178 = !Data_Ld_acombout & dac_sr_a10_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0444",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a10_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a178);

dac_sr_a10_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a10_a = DFFE(i_a177 # i_a178 # Data_a10_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a10_a_acombout,
	datab => i_a177,
	datac => Data_Ld_acombout,
	datad => i_a178,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a10_a);

i_a173_I : apex20ke_lcell 
-- Equation(s):
-- i_a173 = !Data_Ld_acombout & dac_sr_a10_a & shift_en & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a10_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a173);

i_a174_I : apex20ke_lcell 
-- Equation(s):
-- i_a174 = !Data_Ld_acombout & dac_sr_a11_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => shift_en,
	datac => reduce_nor_7,
	datad => dac_sr_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a174);

dac_sr_a11_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a11_a = DFFE(i_a173 # i_a174 # Data_Ld_acombout & Data_a11_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a11_a_acombout,
	datac => i_a173,
	datad => i_a174,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a11_a);

i_a169_I : apex20ke_lcell 
-- Equation(s):
-- i_a169 = !Data_Ld_acombout & shift_en & reduce_nor_7 & dac_sr_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => shift_en,
	datac => reduce_nor_7,
	datad => dac_sr_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a169);

i_a170_I : apex20ke_lcell 
-- Equation(s):
-- i_a170 = !Data_Ld_acombout & dac_sr_a12_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => shift_en,
	datac => reduce_nor_7,
	datad => dac_sr_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a170);

dac_sr_a12_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a12_a = DFFE(i_a169 # i_a170 # Data_Ld_acombout & Data_a12_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => Data_a12_a_acombout,
	datac => i_a169,
	datad => i_a170,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a12_a);

i_a165_I : apex20ke_lcell 
-- Equation(s):
-- i_a165 = shift_en & !Data_Ld_acombout & dac_sr_a12_a & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => shift_en,
	datab => Data_Ld_acombout,
	datac => dac_sr_a12_a,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a165);

i_a166_I : apex20ke_lcell 
-- Equation(s):
-- i_a166 = !Data_Ld_acombout & dac_sr_a13_a & (!reduce_nor_7 # !shift_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0444",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a13_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a166);

dac_sr_a13_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a13_a = DFFE(i_a165 # i_a166 # Data_a13_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a13_a_acombout,
	datab => Data_Ld_acombout,
	datac => i_a165,
	datad => i_a166,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a13_a);

i_a161_I : apex20ke_lcell 
-- Equation(s):
-- i_a161 = !Data_Ld_acombout & dac_sr_a13_a & shift_en & reduce_nor_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a13_a,
	datac => shift_en,
	datad => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a161);

dac_sr_a14_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a14_a = DFFE(i_a162 # i_a161 # Data_a14_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a14_a_acombout,
	datab => Data_Ld_acombout,
	datac => i_a162,
	datad => i_a161,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a14_a);

i_a157_I : apex20ke_lcell 
-- Equation(s):
-- i_a157 = !Data_Ld_acombout & dac_sr_a14_a & reduce_nor_7 & shift_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_Ld_acombout,
	datab => dac_sr_a14_a,
	datac => reduce_nor_7,
	datad => shift_en,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a157);

dac_sr_a15_a_aI : apex20ke_lcell 
-- Equation(s):
-- dac_sr_a15_a = DFFE(i_a158 # i_a157 # Data_a15_a_acombout & Data_Ld_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Data_a15_a_acombout,
	datab => i_a158,
	datac => Data_Ld_acombout,
	datad => i_a157,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => dac_sr_a15_a);

DAC_DATA_areg0_I : apex20ke_lcell 
-- Equation(s):
-- DAC_DATA_areg0 = DFFE(dac_sr_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , reduce_nor_7)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => dac_sr_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => reduce_nor_7,
	devclrn => devclrn,
	devpor => devpor,
	regout => DAC_DATA_areg0);

DAC_LD_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => NOT_DAC_LD_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DAC_LD);

DAC_CLK_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DAC_CLK_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DAC_CLK);

DAC_DATA_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => DAC_DATA_areg0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DAC_DATA);
END structure;


