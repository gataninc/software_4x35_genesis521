onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/data_ld
add wave -noupdate -format Literal -radix hexadecimal /tb/data
add wave -noupdate -format Logic -radix hexadecimal /tb/dac_data
add wave -noupdate -format Logic -radix hexadecimal /tb/dac_clk
add wave -noupdate -format Logic -radix hexadecimal /tb/dac_ld
add wave -noupdate -format Logic -radix hexadecimal /tb/dac_sr
add wave -noupdate -format Logic -radix hexadecimal /tb/dac

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {1882755 ps} {19165170 ps} {8937182 ps}
WaveRestoreZoom {16323016 ps} {20193526 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
