---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ltc1595.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		Tclk High Min 							= 60ns
--		Tclk Low Min  							= 60ns
--		Load PW Min 							= 60ns
--		Dac Output ( Current Mode ) Settling Time	= 1 us
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

library LPM;
     use lpm.lpm_components.all;

LIBRARY altera;
	USE altera.maxplus2.ALL;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity ltc1595 is port( 
	     IMR            : in      std_logic;				     -- Master Reset
		CLK20		: in		std_logic;
		Data_Ld		: in		std_logic;			-- Strobe to Load the Data
		Data			: in		std_logic_vector( 15 downto 0 );
		DAC_LD		: buffer	std_logic;			-- Signal indicating data Load is Done
		DAC_CLK		: buffer	std_logic;			-- DAC clock
		DAC_DATA		: buffer	std_logic );			-- DAC Data
	end ltc1595;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of ltc1595 is
     -- Constant Declarations
	constant shift_cnt_max 	: integer := 16;
	constant clk_cnt_max 	: std_logic_vector( 1 downto 0 ) := "11";
	-- Any Signal Declarations

     -- Combinatorial Signals

	signal shift_cnt 			: integer range 0 to shift_cnt_max;
	signal shift_en			: std_logic;
	signal shift_tc			: std_logic;
	signal dac_sr				: std_Logic_Vector( 15 downto 0 );
	signal iDAC_CLK			: std_logic;
	signal Pre_Dac_Ld			: std_Logic;
	
	signal clk_cnt				: std_logic_Vector( 1 downto 0 );
	signal clk_cnt_tc			: std_logic;
	signal data_ld_del			: std_logic;
begin
	shift_tc		<= '1' when ( shift_cnt = shift_cnt_max ) else '0';
	clk_cnt_tc	<= '1' when ( clk_cnt = clk_cnt_max ) else '0';
     ----------------------------------------------------------------------------------------------
	CLK_Proc : process( IMR, CLK20 )
	begin
		if( IMR = '1' ) then
			clk_cnt			<= "00";
			shift_en 			<= '0';
			dac_sr			<= x"0000";
			shift_cnt 		<= 0;
			iDAC_CLK			<= '0';
			dac_data			<= '0';
			dac_clk			<= '0';
			Pre_Dac_Ld		<= '0';
			dac_ld			<= '1';
			data_ld_del		<= '0';
		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			-- Load is pipelined since it is the same signal that loads "DAC_DATA"
			data_ld_del		<= data_ld;		

			-- Clock divisor to generate a 5Mhz clock from 20Mhz
			if(( shift_en = '1' ) or ( pre_dac_ld = '1' ))
				then clk_cnt <= clk_cnt + 1;
				else clk_cnt <= "00";
			end if;
		
			-- If Load, generate "Shift Enable"
			if( data_ld_del = '1' )
				then shift_en <= '1';
			elsif(( clk_cnt_tc = '1' ) and ( shift_tc = '1' ))
				then shift_en <= '0';
			end if;
			
			-- If Load Strobe, copy data into shift register
			if( data_ld_del = '1' )
				then dac_sr	<= data;
			elsif(( clk_cnt_tc = '1' ) and ( Shift_En = '1' ))
				then dac_sr <= dac_sr( 14 downto 0 ) & dac_sr(15 );
			end if;
			
			-- Bit Counter
			if( shift_en = '0' )
				then shift_cnt <= 0;
			elsif( clk_cnt_tc = '1' )
				then shift_cnt <= shift_cnt + 1;
			end if;
			
			-- Shift Clock
			if( shift_en = '0' )
				then iDAC_CLK	<= '0';
			elsif(( CLK_CNT = "10" ) or ( CLK_CNT = "11" ))
				then iDAC_CLK <= '0';
				else iDAC_CLK <= '1';
			end if;

			-- Final Data Output
			if( clk_cnt_tc = '1' ) 
				then DAC_DATA 		<= dac_sr(15);
			end if;
			
			-- Final Clock Output
			dac_clk		<= iDAC_CLK;

			-- Delay Load Strobe
			if( clk_cnt_tc = '1' )
				then Pre_Dac_Ld <= shift_tc;
			end if;
			
			
			-- Now Generate the Load Strobe
			dac_ld <= not( pre_dac_ld );
		end if;
	end process;
     ----------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
end behavioral;			-- LTC1596.VHD
---------------------------------------------------------------------------------------------------

