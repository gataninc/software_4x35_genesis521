-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_scactrl.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module contains the necessary interface circuit for the 
--		Digital and Analog SCA outputs.
--		It has some static controls to define polarity, enables, etc
--		some additional BIT type of controls to load the parameters
--		directly.
-- 		The real input is the MEAS_DONE signal with the CPEAK vector
--		It then looks-up the appropriate value from the memory to output
--		the SCA Vector and the Analog Output, if in the SCA Mode, or 
--		Outputs the ROI Vector and ROI Load signal to the Cross Port.
-- 		The Interface to the DAC also is containd in this module and 
--		a state machine is used to update the DAC with the ON voltage 
--		at the time when the Digital SCA is fired, and outputs the OFF
--		voltage immediately following. When the Analog SCA Pulse Width 
--		as expired, then the DAC is updated and the ON voltage is loaded
--		to the DAC.
--		Additionally at the leading edge of "TIME_ENABLE" the Off voltage
--		is loaded to the DAC, it is strobed and the ON voltage is also loaded
--		in preparation of analog SCA events.
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity fir_scactrl is 
     port( 
		IMR       	: in      std_logic;					-- Master Reset
          CLK20     	: in      std_logic;                         -- Master Clock
          DSCA_POL		: in		std_logic;					-- Digital SCA Polarity
          DSP_SCA_WE	: in		std_logic;					-- DSP Write DSCA Directly
		Meas_Done		: in		std_logic;					-- Measurement Done
		Time_Enable	: in		std_logic;					-- EDS Acquisition is active
          DSCA_PW		: in		std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse WIdth
		DSP_D		: in		std_logic;					-- Look-up Table Write Data
		Lu_Data		: in		std_logic;					-- Look-UP Table Read Data
          SCA_OUT		: out	std_logic );					-- Digital Output SCA
end fir_scactrl;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture BEHAVIORAL of fir_scactrl is
	constant MDV_MAX : integer := 2;
	-- All Constant Declarations ---------------------------------------------


	-- All Signal Declarations -----------------------------------------------
	signal DSCA_PW_CEN		: std_logic;					-- Digital SCA Pulse Width Counter
	signal DSCA_PW_CLR		: std_logic;					-- Digital SCA Pulsw Width Clear
	signal DSCA_PW_TC		: std_logic;					-- Digital SCA Pulse Width TC
	signal DSCA_PW_CNT		: std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse Width Counter
	signal Meas_Done_Vec	: std_logic_vector( MDV_MAX downto 0 );

begin
	--------------------------------------------------------------------------
	-- Digital SCA Interface -------------------------------------------------
	DSCA_PW_CLR	<= '1' when (( Time_Enable 	= '1' ) and ( Meas_Done_Vec(MDV_MAX) = '1' ) and ( Lu_Data = '1' )) else
				   '1' when (( Time_Enable 	= '0' ) and ( DSP_SCA_WE 	= '1' )) else 
				   '1' when ( DSCA_PW_CEN 	= '0' ) else
				   '1' when ( DSCA_PW_TC 	= '1' ) else
				   '0';
		
	DSCA_CNT : LPM_COUNTER
				generic map(
					LPM_WIDTH 		=> 16,
					LPM_TYPE			=> "LPM_COUNTER" )
				port map(
					aclr				=> IMR,
					clock			=> CLK20,
					cnt_en			=> DSCA_PW_CEN,
					sclr				=> DSCA_PW_CLR,
					q				=> DSCA_PW_CNT );
	
	DSCA_CMP : LPM_COMPARE
				generic map(
					LPM_WIDTH 		=> 16,
					LPM_REPRESENTATION	=> "UNSIGNED",
					LPM_TYPE			=> "LPM_COMPARE" )
				port map(
					dataa			=> DSCA_PW,
					datab			=> DSCA_PW_CNT,
					aeb				=> DSCA_PW_TC );
	-- Digital SCA Interface -------------------------------------------------
	--------------------------------------------------------------------------
	
	--------------------------------------------------------------------------
	clock_proc : process( CLK20, IMR )
     begin
		if( IMR = '1' ) then
			for i in 0 to MDV_MAX loop
				Meas_Done_Vec(i) <= '0';
			end loop;
			DSCA_PW_CEN 		<= '0';
			SCA_OUT			<= '0';
		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			Meas_Done_Vec	<= Meas_Done_Vec( MDV_MAX-1 downto 0 ) & Meas_Done;

			-- Digital SCA Pulse Width Counter -----------------------------
			if(( Time_Enable = '1' ) and ( Meas_Done_Vec(MDV_MAX) = '1' ) and ( Lu_Data = '1' ))
				then DSCA_PW_CEN <= '1';
			elsif(( Time_Enable   = '0' ) and ( DSP_SCA_WE 	= '1' )) 
				then DSCA_PW_CEN <= '1';
			elsif( DSCA_PW_TC = '1' )
				then DSCA_PW_CEN <= '0';
			end if;
			-- Digital SCA Pulse Width Counter -----------------------------		

			-- Digital SCA Output ------------------------------------------
			if(( Time_Enable = '0' ) and ( DSP_SCA_WE = '1' ) and ( DSCA_POL = '1' )) 
				then SCA_OUT <= DSP_D;
			elsif(( Time_Enable = '0' ) and ( DSP_SCA_WE = '1' ) and ( DSCA_POL = '0' )) 
				then SCA_Out <= not( DSP_D );
			elsif(( Time_Enable = '1' ) and ( Meas_Done_Vec(MDV_MAX) = '1' ) and ( Lu_Data = '1' ) and ( DSCA_POL = '1' )) 
				then SCA_Out <= Lu_Data;
			elsif(( Time_Enable = '1' ) and ( Meas_Done_Vec(MDV_MAX) = '1' ) and ( Lu_Data = '1' ) and ( DSCA_POL = '0' )) 
				then SCA_Out <= not( Lu_Data );
			elsif(( DSCA_PW_TC = '1' ) and ( DSCA_POL = '1' ))
				then SCA_Out <= '0';
			elsif(( DSCA_PW_TC = '1' ) and ( DSCA_POL = '0' ))
				then SCA_Out <= '1';
			end if;
			-- Digital SCA Output ------------------------------------------
		end if;                  -- MR/RED Clock
	end process;                  -- clock_Proc
     --------------------------------------------------------------------------
-------------------------------------------------------------------------------
end BEHAVIORAL;          -- fir_scactrl.VHD
-------------------------------------------------------------------------------

