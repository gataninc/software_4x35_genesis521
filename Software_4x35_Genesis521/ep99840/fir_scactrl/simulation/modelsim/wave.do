onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/dsca_pol
add wave -noupdate -format Logic -radix binary /tb/dsp_sca_we
add wave -noupdate -format Logic -radix binary /tb/meas_done
add wave -noupdate -format Logic -radix binary /tb/time_enable
add wave -noupdate -format Literal -radix hexadecimal /tb/dsca_pw
add wave -noupdate -format Logic -radix binary /tb/dsp_d
add wave -noupdate -format Logic -radix binary /tb/lu_data
add wave -noupdate -format Logic -radix binary /tb/sca_out
add wave -noupdate -format Logic -radix binary /tb/sca
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {39706000 ps} {149839471 ps} {5741000 ps} {757018 ps}
WaveRestoreZoom {2675560 ps} {3814883 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
