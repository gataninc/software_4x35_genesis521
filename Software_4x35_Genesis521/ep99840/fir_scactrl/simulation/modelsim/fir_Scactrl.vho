-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:24:44"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_scactrl IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	DSCA_POL : IN std_logic;
	DSP_SCA_WE : IN std_logic;
	Meas_Done : IN std_logic;
	Time_Enable : IN std_logic;
	DSCA_PW : IN std_logic_vector(15 DOWNTO 0);
	DSP_D : IN std_logic;
	Lu_Data : IN std_logic;
	SCA_OUT : OUT std_logic
	);
END fir_scactrl;

ARCHITECTURE structure OF fir_scactrl IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_DSCA_POL : std_logic;
SIGNAL ww_DSP_SCA_WE : std_logic;
SIGNAL ww_Meas_Done : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_DSCA_PW : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DSP_D : std_logic;
SIGNAL ww_Lu_Data : std_logic;
SIGNAL ww_SCA_OUT : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a2_a_acombout : std_logic;
SIGNAL DSCA_PW_a3_a_acombout : std_logic;
SIGNAL DSCA_PW_a8_a_acombout : std_logic;
SIGNAL DSCA_PW_a9_a_acombout : std_logic;
SIGNAL Meas_Done_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL Meas_Done_Vec_a0_a : std_logic;
SIGNAL Meas_Done_Vec_a1_a : std_logic;
SIGNAL Meas_Done_Vec_a2_a : std_logic;
SIGNAL DSP_SCA_WE_acombout : std_logic;
SIGNAL Lu_Data_acombout : std_logic;
SIGNAL SCA_OUT_a450 : std_logic;
SIGNAL Time_Enable_acombout : std_logic;
SIGNAL DSCA_POL_acombout : std_logic;
SIGNAL SCA_OUT_a453 : std_logic;
SIGNAL DSP_D_acombout : std_logic;
SIGNAL SCA_OUT_a449 : std_logic;
SIGNAL DSCA_PW_CEN : std_logic;
SIGNAL DSCA_PW_CLR_a13 : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL DSCA_PW_a10_a_acombout : std_logic;
SIGNAL DSCA_PW_a11_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL DSCA_PW_a13_a_acombout : std_logic;
SIGNAL DSCA_PW_a12_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL DSCA_PW_a15_a_acombout : std_logic;
SIGNAL DSCA_PW_a14_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL DSCA_PW_a0_a_acombout : std_logic;
SIGNAL DSCA_PW_a1_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a5_a_acombout : std_logic;
SIGNAL DSCA_PW_a4_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a7_a_acombout : std_logic;
SIGNAL DSCA_PW_a6_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out : std_logic;
SIGNAL SCA_OUT_a451 : std_logic;
SIGNAL SCA_OUT_areg0 : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_DSCA_POL <= DSCA_POL;
ww_DSP_SCA_WE <= DSP_SCA_WE;
ww_Meas_Done <= Meas_Done;
ww_Time_Enable <= Time_Enable;
ww_DSCA_PW <= DSCA_PW;
ww_DSP_D <= DSP_D;
ww_Lu_Data <= Lu_Data;
SCA_OUT <= ww_SCA_OUT;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

DSCA_PW_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(2),
	combout => DSCA_PW_a2_a_acombout);

DSCA_PW_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(3),
	combout => DSCA_PW_a3_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a3_a & DSCA_PW_a3_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a2_a $ !DSCA_PW_a2_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a3_a & 
-- !DSCA_PW_a3_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a2_a $ !DSCA_PW_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a3_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a2_a,
	datac => DSCA_PW_a3_a_acombout,
	datad => DSCA_PW_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out);

DSCA_CNT_awysi_counter_acounter_cell_a14_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a14_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a14_a & (!DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a14_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a14_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT);

DSCA_PW_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(8),
	combout => DSCA_PW_a8_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a8_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a8_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a8_a & (!DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A60A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a8_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a8_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT);

DSCA_PW_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(9),
	combout => DSCA_PW_a9_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a9_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a9_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a9_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a9_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a8_a & DSCA_PW_a8_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a9_a $ !DSCA_PW_a9_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a8_a & 
-- !DSCA_PW_a8_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a9_a $ !DSCA_PW_a9_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a8_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a9_a,
	datac => DSCA_PW_a8_a_acombout,
	datad => DSCA_PW_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out);

Meas_Done_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Meas_Done,
	combout => Meas_Done_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

Meas_Done_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Meas_Done_Vec_a0_a = DFFE(Meas_Done_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Meas_Done_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Meas_Done_Vec_a0_a);

Meas_Done_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Meas_Done_Vec_a1_a = DFFE(Meas_Done_Vec_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Meas_Done_Vec_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Meas_Done_Vec_a1_a);

Meas_Done_Vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Meas_Done_Vec_a2_a = DFFE(Meas_Done_Vec_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => Meas_Done_Vec_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Meas_Done_Vec_a2_a);

DSP_SCA_WE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_SCA_WE,
	combout => DSP_SCA_WE_acombout);

Lu_Data_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data,
	combout => Lu_Data_acombout);

SCA_OUT_a450_I : apex20ke_lcell
-- Equation(s):
-- SCA_OUT_a450 = Time_Enable_acombout & (!Lu_Data_acombout # !Meas_Done_Vec_a2_a) # !Time_Enable_acombout & (!DSP_SCA_WE_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "27AF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Time_Enable_acombout,
	datab => Meas_Done_Vec_a2_a,
	datac => DSP_SCA_WE_acombout,
	datad => Lu_Data_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SCA_OUT_a450);

Time_Enable_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => Time_Enable_acombout);

DSCA_POL_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_POL,
	combout => DSCA_POL_acombout);

SCA_OUT_a453_I : apex20ke_lcell
-- Equation(s):
-- SCA_OUT_a453 = Lu_Data_acombout & Time_Enable_acombout & Meas_Done_Vec_a2_a & DSCA_POL_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_acombout,
	datab => Time_Enable_acombout,
	datac => Meas_Done_Vec_a2_a,
	datad => DSCA_POL_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SCA_OUT_a453);

DSP_D_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D,
	combout => DSP_D_acombout);

SCA_OUT_a449_I : apex20ke_lcell
-- Equation(s):
-- SCA_OUT_a449 = !Time_Enable_acombout & DSP_SCA_WE_acombout & (DSCA_POL_acombout $ !DSP_D_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Time_Enable_acombout,
	datab => DSCA_POL_acombout,
	datac => DSP_SCA_WE_acombout,
	datad => DSP_D_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SCA_OUT_a449);

DSCA_PW_CEN_aI : apex20ke_lcell
-- Equation(s):
-- DSCA_PW_CEN = DFFE(DSCA_PW_CEN & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !SCA_OUT_a450, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33F3",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SCA_OUT_a450,
	datac => DSCA_PW_CEN,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_PW_CEN);

DSCA_PW_CLR_a13_I : apex20ke_lcell
-- Equation(s):
-- DSCA_PW_CLR_a13 = DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !DSCA_PW_CEN # !SCA_OUT_a450

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF3F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => SCA_OUT_a450,
	datac => DSCA_PW_CEN,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_PW_CLR_a13);

DSCA_CNT_awysi_counter_acounter_cell_a0_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_PW_CEN $ DSCA_CNT_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3CF0",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSCA_PW_CEN,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a0_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a1_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a1_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a1_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a1_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a2_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a2_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a2_a & !DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a2_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a2_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a3_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a3_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a3_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a3_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a4_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a4_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a4_a & !DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a4_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a4_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a5_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a5_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a5_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a5_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a6_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a6_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a6_a & !DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a6_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a6_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a7_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a7_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a7_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a7_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a10_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a10_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a10_a & !DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a10_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a10_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT);

DSCA_PW_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(10),
	combout => DSCA_PW_a10_a_acombout);

DSCA_PW_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(11),
	combout => DSCA_PW_a11_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a11_a & DSCA_PW_a11_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a10_a $ !DSCA_PW_a10_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a11_a & 
-- !DSCA_PW_a11_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a10_a $ !DSCA_PW_a10_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a11_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a10_a,
	datac => DSCA_PW_a10_a_acombout,
	datad => DSCA_PW_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out);

DSCA_CNT_awysi_counter_acounter_cell_a11_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a11_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a11_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a11_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a12_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a12_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a12_a & !DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C60C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a12_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a12_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT);

DSCA_PW_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(13),
	combout => DSCA_PW_a13_a_acombout);

DSCA_PW_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(12),
	combout => DSCA_PW_a12_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a13_a & DSCA_PW_a13_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a12_a $ !DSCA_PW_a12_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a13_a & 
-- !DSCA_PW_a13_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a12_a $ !DSCA_PW_a12_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a13_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a12_a,
	datac => DSCA_PW_a13_a_acombout,
	datad => DSCA_PW_a12_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out);

DSCA_CNT_awysi_counter_acounter_cell_a13_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a13_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a13_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a13_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a15_a : apex20ke_lcell
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(DSCA_PW_CLR_a13) & DSCA_CNT_awysi_counter_asload_path_a15_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5FA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datad => DSCA_CNT_awysi_counter_asload_path_a15_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => DSCA_PW_CLR_a13,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a15_a);

DSCA_PW_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(15),
	combout => DSCA_PW_a15_a_acombout);

DSCA_PW_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(14),
	combout => DSCA_PW_a14_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a14_a & DSCA_PW_a14_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a15_a $ !DSCA_PW_a15_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a14_a & 
-- !DSCA_PW_a14_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a15_a $ !DSCA_PW_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a14_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a15_a,
	datac => DSCA_PW_a15_a_acombout,
	datad => DSCA_PW_a14_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out_a25 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out & 
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out,
	datab => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out,
	datac => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out);

DSCA_PW_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(0),
	combout => DSCA_PW_a0_a_acombout);

DSCA_PW_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(1),
	combout => DSCA_PW_a1_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a1_a & DSCA_PW_a1_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a0_a $ !DSCA_PW_a0_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a1_a & 
-- !DSCA_PW_a1_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a0_a $ !DSCA_PW_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8241",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a1_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a0_a,
	datac => DSCA_PW_a0_a_acombout,
	datad => DSCA_PW_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

DSCA_PW_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(5),
	combout => DSCA_PW_a5_a_acombout);

DSCA_PW_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(4),
	combout => DSCA_PW_a4_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a5_a & DSCA_PW_a5_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a4_a $ !DSCA_PW_a4_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a5_a & 
-- !DSCA_PW_a5_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a4_a $ !DSCA_PW_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a5_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a4_a,
	datac => DSCA_PW_a5_a_acombout,
	datad => DSCA_PW_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out);

DSCA_PW_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(7),
	combout => DSCA_PW_a7_a_acombout);

DSCA_PW_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(6),
	combout => DSCA_PW_a6_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a7_a & DSCA_PW_a7_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a6_a $ !DSCA_PW_a6_a_acombout) # !DSCA_CNT_awysi_counter_asload_path_a7_a & 
-- !DSCA_PW_a7_a_acombout & (DSCA_CNT_awysi_counter_asload_path_a6_a $ !DSCA_PW_a6_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8421",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a7_a,
	datab => DSCA_CNT_awysi_counter_asload_path_a6_a,
	datac => DSCA_PW_a7_a_acombout,
	datad => DSCA_PW_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out_a25 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out & 
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out,
	datab => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	datac => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out_a0 : apex20ke_lcell
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out & (DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out);

SCA_OUT_a451_I : apex20ke_lcell
-- Equation(s):
-- SCA_OUT_a451 = DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out & !DSCA_POL_acombout # !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out & (SCA_OUT_areg0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSCA_POL_acombout,
	datac => SCA_OUT_areg0,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => SCA_OUT_a451);

SCA_OUT_areg0_I : apex20ke_lcell
-- Equation(s):
-- SCA_OUT_areg0 = DFFE(SCA_OUT_a453 # SCA_OUT_a449 # SCA_OUT_a450 & SCA_OUT_a451, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FEFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => SCA_OUT_a450,
	datab => SCA_OUT_a453,
	datac => SCA_OUT_a449,
	datad => SCA_OUT_a451,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => SCA_OUT_areg0);

SCA_OUT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT);
END structure;


