-- Copyright (C) 1991-2002 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 2.1 Build 189 09/05/2002 Service Pack 1 SJ Full Version"

-- DATE "06/18/2003 14:36:51"

--
-- Device: Altera EP20K300EQC240-1X Package PQFP240
-- 

-- 
-- This VHDL file should be used for MODELSIM-ALTERA (VHDL OUTPUT FROM QUARTUS II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	Scactrl IS
    PORT (
	CLK20 : IN std_logic;
	DSCA_POL : IN std_logic;
	DSCA_PW : IN std_logic_vector(15 DOWNTO 0);
	DSP_D : IN std_logic_vector(7 DOWNTO 0);
	DSP_SCA_WE : IN std_logic;
	IMR : IN std_logic;
	Lu_Data : IN std_logic_vector(7 DOWNTO 0);
	Meas_Done : IN std_logic;
	Time_Enable : IN std_logic;
	SCA_OUT : OUT std_logic_vector(7 DOWNTO 0)
	);
END Scactrl;

ARCHITECTURE structure OF Scactrl IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_DSCA_POL : std_logic;
SIGNAL ww_DSCA_PW : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_DSP_D : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_DSP_SCA_WE : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_Lu_Data : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_Meas_Done : std_logic;
SIGNAL ww_Time_Enable : std_logic;
SIGNAL ww_SCA_OUT : std_logic_vector(7 DOWNTO 0);
SIGNAL DSP_D_a0_a_acombout : std_logic;
SIGNAL Time_Enable_acombout : std_logic;
SIGNAL DSP_SCA_WE_acombout : std_logic;
SIGNAL nx595 : std_logic;
SIGNAL Lu_Data_a0_a_acombout : std_logic;
SIGNAL DSCA_PW_a4_a_acombout : std_logic;
SIGNAL Meas_Done_acombout : std_logic;
SIGNAL nx606 : std_logic;
SIGNAL Lu_Data_a1_a_acombout : std_logic;
SIGNAL Lu_Data_a2_a_acombout : std_logic;
SIGNAL nx533 : std_logic;
SIGNAL Lu_Data_a7_a_acombout : std_logic;
SIGNAL Lu_Data_a6_a_acombout : std_logic;
SIGNAL nx68 : std_logic;
SIGNAL nx535 : std_logic;
SIGNAL Lu_Data_a3_a_acombout : std_logic;
SIGNAL Lu_Data_a5_a_acombout : std_logic;
SIGNAL Lu_Data_a4_a_acombout : std_logic;
SIGNAL nx532 : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL nx101 : std_logic;
SIGNAL nx100 : std_logic;
SIGNAL NOT_nx64 : std_logic;
SIGNAL nx102 : std_logic;
SIGNAL nx258 : std_logic;
SIGNAL DSCA_PW_CEN : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL DSCA_PW_a5_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a2_a_acombout : std_logic;
SIGNAL DSCA_PW_a3_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a7_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL DSCA_PW_a6_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a0_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL DSCA_PW_a1_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a8_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL DSCA_PW_a9_a_acombout : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a11_a_acombout : std_logic;
SIGNAL DSCA_PW_a10_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a13_a_acombout : std_logic;
SIGNAL DSCA_PW_a12_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out : std_logic;
SIGNAL DSCA_PW_a15_a_acombout : std_logic;
SIGNAL DSCA_PW_a14_a_acombout : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL DSCA_CNT_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out : std_logic;
SIGNAL DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out : std_logic;
SIGNAL iSCA_OUT_0 : std_logic;
SIGNAL DSCA_POL_acombout : std_logic;
SIGNAL SCA_OUT_dup0_0 : std_logic;
SIGNAL DSP_D_a1_a_acombout : std_logic;
SIGNAL iSCA_OUT_1 : std_logic;
SIGNAL SCA_OUT_dup0_1 : std_logic;
SIGNAL DSP_D_a2_a_acombout : std_logic;
SIGNAL iSCA_OUT_2 : std_logic;
SIGNAL SCA_OUT_dup0_2 : std_logic;
SIGNAL DSP_D_a3_a_acombout : std_logic;
SIGNAL iSCA_OUT_3 : std_logic;
SIGNAL SCA_OUT_dup0_3 : std_logic;
SIGNAL DSP_D_a4_a_acombout : std_logic;
SIGNAL iSCA_OUT_4 : std_logic;
SIGNAL SCA_OUT_dup0_4 : std_logic;
SIGNAL DSP_D_a5_a_acombout : std_logic;
SIGNAL iSCA_OUT_5 : std_logic;
SIGNAL SCA_OUT_dup0_5 : std_logic;
SIGNAL DSP_D_a6_a_acombout : std_logic;
SIGNAL iSCA_OUT_6 : std_logic;
SIGNAL SCA_OUT_dup0_6 : std_logic;
SIGNAL DSP_D_a7_a_acombout : std_logic;
SIGNAL iSCA_OUT_7 : std_logic;
SIGNAL SCA_OUT_dup0_7 : std_logic;

BEGIN

ww_CLK20 <= CLK20;
ww_DSCA_POL <= DSCA_POL;
ww_DSCA_PW <= DSCA_PW;
ww_DSP_D <= DSP_D;
ww_DSP_SCA_WE <= DSP_SCA_WE;
ww_IMR <= IMR;
ww_Lu_Data <= Lu_Data;
ww_Meas_Done <= Meas_Done;
ww_Time_Enable <= Time_Enable;
SCA_OUT <= ww_SCA_OUT;

DSP_D_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(0),
	combout => DSP_D_a0_a_acombout);

Time_Enable_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Time_Enable,
	combout => Time_Enable_acombout);

DSP_SCA_WE_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_SCA_WE,
	combout => DSP_SCA_WE_acombout);

ix98 : apex20ke_lcell 
-- Equation(s):
-- nx595 = Time_Enable_acombout # !DSP_SCA_WE_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Time_Enable_acombout,
	datad => DSP_SCA_WE_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx595);

Lu_Data_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(0),
	combout => Lu_Data_a0_a_acombout);

DSCA_PW_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(4),
	combout => DSCA_PW_a4_a_acombout);

Meas_Done_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Meas_Done,
	combout => Meas_Done_acombout);

ix94 : apex20ke_lcell 
-- Equation(s):
-- nx606 = Time_Enable_acombout & Meas_Done_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Time_Enable_acombout,
	datad => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx606);

Lu_Data_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(1),
	combout => Lu_Data_a1_a_acombout);

Lu_Data_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(2),
	combout => Lu_Data_a2_a_acombout);

ix79 : apex20ke_lcell 
-- Equation(s):
-- nx533 = Lu_Data_a0_a_acombout # Lu_Data_a1_a_acombout # Lu_Data_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_a0_a_acombout,
	datac => Lu_Data_a1_a_acombout,
	datad => Lu_Data_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx533);

Lu_Data_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(7),
	combout => Lu_Data_a7_a_acombout);

Lu_Data_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(6),
	combout => Lu_Data_a6_a_acombout);

ix90 : apex20ke_lcell 
-- Equation(s):
-- nx68 = !Lu_Data_a7_a_acombout & !Lu_Data_a6_a_acombout # !Meas_Done_acombout # !Time_Enable_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_a7_a_acombout,
	datab => Lu_Data_a6_a_acombout,
	datac => Time_Enable_acombout,
	datad => Meas_Done_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx68);

ix80 : apex20ke_lcell 
-- Equation(s):
-- nx535 = nx68 & nx595

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => nx68,
	datad => nx595,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx535);

Lu_Data_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(3),
	combout => Lu_Data_a3_a_acombout);

Lu_Data_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(5),
	combout => Lu_Data_a5_a_acombout);

Lu_Data_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Lu_Data(4),
	combout => Lu_Data_a4_a_acombout);

ix97 : apex20ke_lcell 
-- Equation(s):
-- nx532 = Lu_Data_a3_a_acombout # Lu_Data_a5_a_acombout # Lu_Data_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Lu_Data_a3_a_acombout,
	datac => Lu_Data_a5_a_acombout,
	datad => Lu_Data_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx532);

CLK20_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

ix93 : apex20ke_lcell 
-- Equation(s):
-- nx101 = !Lu_Data_a7_a_acombout & !Lu_Data_a6_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Lu_Data_a7_a_acombout,
	datad => Lu_Data_a6_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx101);

ix91 : apex20ke_lcell 
-- Equation(s):
-- nx100 = !Lu_Data_a1_a_acombout & !Lu_Data_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datac => Lu_Data_a1_a_acombout,
	datad => Lu_Data_a2_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	cascout => nx100);

ix92 : apex20ke_lcell 
-- Equation(s):
-- NOT_nx64 = (!Lu_Data_a5_a_acombout & !Lu_Data_a3_a_acombout & !Lu_Data_a0_a_acombout & !Lu_Data_a4_a_acombout) & CASCADE(nx100)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_a5_a_acombout,
	datab => Lu_Data_a3_a_acombout,
	datac => Lu_Data_a0_a_acombout,
	datad => Lu_Data_a4_a_acombout,
	cascin => nx100,
	devclrn => devclrn,
	devpor => devpor,
	combout => NOT_nx64);

ix95 : apex20ke_lcell 
-- Equation(s):
-- nx102 = !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out & (Time_Enable_acombout # !DSP_SCA_WE_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F3",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => DSP_SCA_WE_acombout,
	datac => Time_Enable_acombout,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx102);

ix96 : apex20ke_lcell 
-- Equation(s):
-- nx258 = nx606 & (!NOT_nx64 # !nx101) # !nx102

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "70FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => nx101,
	datab => NOT_nx64,
	datac => nx606,
	datad => nx102,
	devclrn => devclrn,
	devpor => devpor,
	combout => nx258);

reg_DSCA_PW_CEN : apex20ke_lcell 
-- Equation(s):
-- DSCA_PW_CEN = DFFE(nx606 & (nx533 # nx532) # !nx535, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AF8F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => nx606,
	datab => nx533,
	datac => nx535,
	datad => nx532,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_PW_CEN);

DSCA_CNT_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(nx258) & DSCA_PW_CEN $ DSCA_CNT_awysi_counter_asload_path_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a0_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a1_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a1_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a1_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a2_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a2_a & !DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a2_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a2_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a3_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a3_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a3_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a4_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a4_a & !DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a4_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a4_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a5_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a5_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a5_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT);

DSCA_PW_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(5),
	combout => DSCA_PW_a5_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out = DSCA_PW_a4_a_acombout & DSCA_CNT_awysi_counter_asload_path_a4_a & (DSCA_CNT_awysi_counter_asload_path_a5_a $ !DSCA_PW_a5_a_acombout) # !DSCA_PW_a4_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a4_a & (DSCA_CNT_awysi_counter_asload_path_a5_a $ !DSCA_PW_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a4_a_acombout,
	datab => DSCA_CNT_awysi_counter_asload_path_a5_a,
	datac => DSCA_PW_a5_a_acombout,
	datad => DSCA_CNT_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out);

DSCA_PW_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(2),
	combout => DSCA_PW_a2_a_acombout);

DSCA_PW_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(3),
	combout => DSCA_PW_a3_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out = DSCA_PW_a2_a_acombout & DSCA_CNT_awysi_counter_asload_path_a2_a & (DSCA_CNT_awysi_counter_asload_path_a3_a $ !DSCA_PW_a3_a_acombout) # !DSCA_PW_a2_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a2_a & (DSCA_CNT_awysi_counter_asload_path_a3_a $ !DSCA_PW_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a2_a_acombout,
	datab => DSCA_CNT_awysi_counter_asload_path_a3_a,
	datac => DSCA_PW_a3_a_acombout,
	datad => DSCA_CNT_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out);

DSCA_PW_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(7),
	combout => DSCA_PW_a7_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a6_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a6_a & !DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a6_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a6_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a7_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a7_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a7_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT);

DSCA_PW_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(6),
	combout => DSCA_PW_a6_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out = DSCA_PW_a7_a_acombout & DSCA_CNT_awysi_counter_asload_path_a7_a & (DSCA_PW_a6_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a6_a) # !DSCA_PW_a7_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a7_a & (DSCA_PW_a6_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "9009",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a7_a_acombout,
	datab => DSCA_CNT_awysi_counter_asload_path_a7_a,
	datac => DSCA_PW_a6_a_acombout,
	datad => DSCA_CNT_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out);

DSCA_PW_ibuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(0),
	combout => DSCA_PW_a0_a_acombout);

DSCA_PW_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(1),
	combout => DSCA_PW_a1_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out = DSCA_CNT_awysi_counter_asload_path_a1_a & DSCA_PW_a1_a_acombout & (DSCA_PW_a0_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a0_a) # !DSCA_CNT_awysi_counter_asload_path_a1_a & !DSCA_PW_a1_a_acombout & (DSCA_PW_a0_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a1_a,
	datab => DSCA_PW_a0_a_acombout,
	datac => DSCA_CNT_awysi_counter_asload_path_a0_a,
	datad => DSCA_PW_a1_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_a5_a7 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a2_a_aaeb_out,
	datab => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a1_a_aaeb_out,
	datac => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a3_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a0_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out);

DSCA_PW_ibuf_8 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(8),
	combout => DSCA_PW_a8_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a8_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a8_a & !DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a8_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a8_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a9_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CNT_awysi_counter_asload_path_a9_a,
	datab => DSCA_PW_CEN,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a9_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT);

DSCA_PW_ibuf_9 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(9),
	combout => DSCA_PW_a9_a_acombout);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out = DSCA_PW_a8_a_acombout & DSCA_CNT_awysi_counter_asload_path_a8_a & (DSCA_CNT_awysi_counter_asload_path_a9_a $ !DSCA_PW_a9_a_acombout) # !DSCA_PW_a8_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a8_a & (DSCA_CNT_awysi_counter_asload_path_a9_a $ !DSCA_PW_a9_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8241",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a8_a_acombout,
	datab => DSCA_CNT_awysi_counter_asload_path_a9_a,
	datac => DSCA_PW_a9_a_acombout,
	datad => DSCA_CNT_awysi_counter_asload_path_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out);

DSCA_PW_ibuf_11 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(11),
	combout => DSCA_PW_a11_a_acombout);

DSCA_PW_ibuf_10 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(10),
	combout => DSCA_PW_a10_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a10_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a10_a & !DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a10_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a10_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a11_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a11_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a11_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out = DSCA_PW_a11_a_acombout & DSCA_CNT_awysi_counter_asload_path_a11_a & (DSCA_PW_a10_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a10_a) # !DSCA_PW_a11_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a11_a & (DSCA_PW_a10_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a11_a_acombout,
	datab => DSCA_PW_a10_a_acombout,
	datac => DSCA_CNT_awysi_counter_asload_path_a11_a,
	datad => DSCA_CNT_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out);

DSCA_PW_ibuf_13 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(13),
	combout => DSCA_PW_a13_a_acombout);

DSCA_PW_ibuf_12 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(12),
	combout => DSCA_PW_a12_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a12_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a12_a & !DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a12_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a12_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a13_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT # !DSCA_CNT_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a13_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a13_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out = DSCA_PW_a13_a_acombout & DSCA_CNT_awysi_counter_asload_path_a13_a & (DSCA_PW_a12_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a12_a) # !DSCA_PW_a13_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a13_a & (DSCA_PW_a12_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a13_a_acombout,
	datab => DSCA_PW_a12_a_acombout,
	datac => DSCA_CNT_awysi_counter_asload_path_a13_a,
	datad => DSCA_CNT_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out);

DSCA_PW_ibuf_15 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(15),
	combout => DSCA_PW_a15_a_acombout);

DSCA_PW_ibuf_14 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_PW(14),
	combout => DSCA_PW_a14_a_acombout);

DSCA_CNT_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a14_a $ (DSCA_PW_CEN & !DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(DSCA_CNT_awysi_counter_asload_path_a14_a & !DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datab => DSCA_CNT_awysi_counter_asload_path_a14_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a14_a,
	cout => DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT);

DSCA_CNT_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- DSCA_CNT_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(nx258) & DSCA_CNT_awysi_counter_asload_path_a15_a $ (DSCA_PW_CEN & DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_CEN,
	datad => DSCA_CNT_awysi_counter_asload_path_a15_a,
	cin => DSCA_CNT_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => DSCA_CNT_awysi_counter_asload_path_a15_a);

DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out = DSCA_PW_a15_a_acombout & DSCA_CNT_awysi_counter_asload_path_a15_a & (DSCA_PW_a14_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a14_a) # !DSCA_PW_a15_a_acombout & !DSCA_CNT_awysi_counter_asload_path_a15_a & (DSCA_PW_a14_a_acombout $ !DSCA_CNT_awysi_counter_asload_path_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8421",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_PW_a15_a_acombout,
	datab => DSCA_PW_a14_a_acombout,
	datac => DSCA_CNT_awysi_counter_asload_path_a15_a,
	datad => DSCA_CNT_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_a5_a7 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a4_a_aaeb_out,
	datab => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a5_a_aaeb_out,
	datac => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a6_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_acmp_a7_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out);

DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_a1 : apex20ke_lcell 
-- Equation(s):
-- DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out = DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out & DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a0_a_aaeb_out,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_acmp_a1_a_aaeb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out);

reg_iSCA_OUT_0 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_0 = DFFE(nx595 & Lu_Data_a0_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22E2",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a0_a_acombout,
	datab => nx595,
	datac => Lu_Data_a0_a_acombout,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_0);

DSCA_POL_ibuf : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSCA_POL,
	combout => DSCA_POL_acombout);

ix78 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_0 = iSCA_OUT_0 $ !DSCA_POL_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSCA_OUT_0,
	datac => DSCA_POL_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_0);

DSP_D_ibuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(1),
	combout => DSP_D_a1_a_acombout);

reg_iSCA_OUT_1 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_1 = DFFE(nx595 & Lu_Data_a1_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22E2",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a1_a_acombout,
	datab => nx595,
	datac => Lu_Data_a1_a_acombout,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_1);

ix77 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_1 = iSCA_OUT_1 $ !DSCA_POL_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => iSCA_OUT_1,
	datac => DSCA_POL_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_1);

DSP_D_ibuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(2),
	combout => DSP_D_a2_a_acombout);

reg_iSCA_OUT_2 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_2 = DFFE(nx595 & Lu_Data_a2_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22E2",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a2_a_acombout,
	datab => nx595,
	datac => Lu_Data_a2_a_acombout,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_2);

ix76 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_2 = DSCA_POL_acombout $ !iSCA_OUT_2

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_2,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_2);

DSP_D_ibuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(3),
	combout => DSP_D_a3_a_acombout);

reg_iSCA_OUT_3 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_3 = DFFE(nx595 & Lu_Data_a3_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0ACA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a3_a_acombout,
	datab => Lu_Data_a3_a_acombout,
	datac => nx595,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_3);

ix75 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_3 = DSCA_POL_acombout $ !iSCA_OUT_3

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_3,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_3);

DSP_D_ibuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(4),
	combout => DSP_D_a4_a_acombout);

reg_iSCA_OUT_4 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_4 = DFFE(nx595 & Lu_Data_a4_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_a4_a_acombout,
	datab => DSP_D_a4_a_acombout,
	datac => nx595,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_4);

ix74 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_4 = DSCA_POL_acombout $ !iSCA_OUT_4

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_4,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_4);

DSP_D_ibuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(5),
	combout => DSP_D_a5_a_acombout);

reg_iSCA_OUT_5 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_5 = DFFE(nx595 & Lu_Data_a5_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0ACA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a5_a_acombout,
	datab => Lu_Data_a5_a_acombout,
	datac => nx595,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_5);

ix73 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_5 = DSCA_POL_acombout $ !iSCA_OUT_5

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_5,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_5);

DSP_D_ibuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(6),
	combout => DSP_D_a6_a_acombout);

reg_iSCA_OUT_6 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_6 = DFFE(nx595 & Lu_Data_a6_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0ACA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => DSP_D_a6_a_acombout,
	datab => Lu_Data_a6_a_acombout,
	datac => nx595,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_6);

ix72 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_6 = DSCA_POL_acombout $ !iSCA_OUT_6

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_6,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_6);

DSP_D_ibuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(7),
	combout => DSP_D_a7_a_acombout);

reg_iSCA_OUT_7 : apex20ke_lcell 
-- Equation(s):
-- iSCA_OUT_7 = DFFE(nx595 & Lu_Data_a7_a_acombout & !DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out # !nx595 & DSP_D_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , nx258)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Lu_Data_a7_a_acombout,
	datab => DSP_D_a7_a_acombout,
	datac => nx595,
	datad => DSCA_CMP_acomparator_acmp_end_acomp_asub_comptree_asub_comptree_acmp_end_aaeb_out,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => nx258,
	devclrn => devclrn,
	devpor => devpor,
	regout => iSCA_OUT_7);

ix71 : apex20ke_lcell 
-- Equation(s):
-- SCA_OUT_dup0_7 = DSCA_POL_acombout $ !iSCA_OUT_7

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => DSCA_POL_acombout,
	datad => iSCA_OUT_7,
	devclrn => devclrn,
	devpor => devpor,
	combout => SCA_OUT_dup0_7);

SCA_OUT_obuf_0 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_0,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(0));

SCA_OUT_obuf_1 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_1,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(1));

SCA_OUT_obuf_2 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_2,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(2));

SCA_OUT_obuf_3 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_3,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(3));

SCA_OUT_obuf_4 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_4,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(4));

SCA_OUT_obuf_5 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_5,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(5));

SCA_OUT_obuf_6 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_6,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(6));

SCA_OUT_obuf_7 : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => SCA_OUT_dup0_7,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_SCA_OUT(7));
END structure;


