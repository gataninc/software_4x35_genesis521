-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_scactrl.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module contains the necessary interface circuit for the 
--		Digital and Analog SCA outputs.
--		It has some static controls to define polarity, enables, etc
--		some additional BIT type of controls to load the parameters
--		directly.
-- 		The real input is the MEAS_DONE signal with the CPEAK vector
--		It then looks-up the appropriate value from the memory to output
--		the SCA Vector and the Analog Output, if in the SCA Mode, or 
--		Outputs the ROI Vector and ROI Load signal to the Cross Port.
-- 		The Interface to the DAC also is containd in this module and 
--		a state machine is used to update the DAC with the ON voltage 
--		at the time when the Digital SCA is fired, and outputs the OFF
--		voltage immediately following. When the Analog SCA Pulse Width 
--		as expired, then the DAC is updated and the ON voltage is loaded
--		to the DAC.
--		Additionally at the leading edge of "TIME_ENABLE" the Off voltage
--		is loaded to the DAC, it is strobed and the ON voltage is also loaded
--		in preparation of analog SCA events.
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
entity tb is
end tb;

architecture test_bench of tb is

	signal imr	: std_logic := '1';
	signal clk20	: std_logic := '0';
	
	signal DSCA_POL	: std_Logic;
	signal DSP_SCA_WE	: std_logic;
	signal Meas_Done	: std_logic;
	signal Time_Enable	: std_logic;
	signal DSCA_PW		: std_logic_vector( 15 downto 0 );
	signal DSP_D		: std_logic;
	signal LU_DATA		: std_logic;
	signal SCA_OUT		: Std_logic;
	signal SCA		: std_logic;

	
	-------------------------------------------------------------------------------
	component fir_scactrl 
	     port( 
			IMR       	: in      std_logic;					-- Master Reset
	          CLK20     	: in      std_logic;                         -- Master Clock
	          DSCA_POL		: in		std_logic;					-- Digital SCA Polarity
	          DSP_SCA_WE	: in		std_logic;					-- DSP Write DSCA Directly
			Meas_Done		: in		std_logic;					-- Measurement Done
			Time_Enable	: in		std_logic;					-- EDS Acquisition is active
	          DSCA_PW		: in		std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse WIdth
			DSP_D		: in		std_logic;	-- Look-up Table Write Data
			Lu_Data		: in		std_logic;	-- Look-UP Table Read Data
	          SCA_OUT		: out	std_logic );	-- Digital Output SCA
	end component fir_scactrl;
	-------------------------------------------------------------------------------
begin
	imr		<= '0' after 555 ns;
	clk20	<= not( clk20 ) after 25 ns;

	U : fir_scactrl	
	     port map(
			IMR       	=> imr,
	          CLK20     	=> clk20,
	          DSCA_POL		=> DSCA_POL,
	          DSP_SCA_WE	=> DSP_SCA_WE,
			Meas_Done		=> Meas_Done,
			Time_Enable	=> Time_Enable,
	          DSCA_PW		=> DSCA_PW,
			DSP_D		=> DSP_D,
			Lu_Data		=> LU_Data,
	          SCA_OUT		=> SCA_Out );

	SCA <= '1' when (( DSCA_POL = '1' ) and ( SCA_OUT = '1' )) else
	       '1' when (( DSCA_POL = '0' ) and ( SCA_OUT = '0' )) else 
	       '0';
	tb_proc : process
	begin
		DSCA_POL		<= '1';
		DSCA_PW		<= x"0002";

		DSP_SCA_WE	<= '0';
		DSP_D		<= '0';
		
		Meas_Done		<= '0';
		Time_Enable	<= '0';
		LU_DATA		<= '0';
		
	
		wait for 2 us;
		--------------------------------------------------------------------------------
		assert false
			report "test SCA direct from DSP"
			severity note;
			
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		DSP_D <= '1';
		DSP_SCA_WE <= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		DSP_SCA_WE 	<= '0';
		DSP_D 		<= '0';
		wait until(( SCA'Event ) and ( SCA = '0' ));
		wait for 100 ns;

		--------------------------------------------------------------------------------
		assert false
			report "test SCA from MEAS_DONE"
			severity note;
		
		Time_Enable	<= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		Meas_Done <= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		Meas_Done <= '0';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		LU_Data <= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		wait for 200 ns;
		LU_Data <= '0';
		wait until(( SCA'Event ) and ( SCA = '0' ));
		wait for 100 ns;

		--------------------------------------------------------------------------------
		assert false
			report "test SCA PW from MEAS_DONE"
			severity note;
		
		Time_Enable	<= '1';
		DSCA_PW		<= x"0000";
		wait for 100 ns;
		for i in 0 to 7 loop
			DSCA_PW(i) <= '1';
			wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
			Meas_Done <= '1';
			wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
			Meas_Done <= '0';
			wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
			LU_Data <= '1';
			wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
			wait for 200 ns;
			LU_Data <= '0';
			if( SCA = '1' ) then 
				wait until(( SCA'Event ) and ( SCA = '0' ));
			end if;
			DSCA_PW(i) <= '0';
			wait for 100 ns;
		end loop;
		wait for 200 ns;
			--------------------------------------------------------------------------------
		assert false
			report "test SCA Polarity from MEAS_DONE"
			severity note;
		
		DSCA_POL		<= '0';	
		Time_Enable	<= '1';
		DSCA_PW		<= x"0002";
		
		wait for 200 ns;
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		Meas_Done <= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		Meas_Done <= '0';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		LU_Data <= '1';
		wait until ((clk20'event ) and ( clk20 = '1' )); wait for 7 ns;
		wait for 200 ns;
		LU_Data <= '0';
			
		wait until(( SCA'Event ) and ( SCA = '0' ));
		wait for 100 ns;

		assert false
			report "End of Simulations"
			severity failure;
		wait ;
	end process;
-------------------------------------------------------------------------------
end test_bench;          -- fir_scactrl.VHD
-------------------------------------------------------------------------------

