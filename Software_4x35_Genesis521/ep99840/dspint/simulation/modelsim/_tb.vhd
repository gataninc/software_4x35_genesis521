

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
	
entity tb is
end tb;
	
architecture TEST_BENCH of tb is

	signal IMR     	: std_logic := '1';
	signal CLK20		: std_logic := '0';					 -- master Clock
	signal clk40 		: std_logic := '0';
	signal DSP_WR		: std_logic;						 -- DSP Write Strobe
	signal DSP_STB		: std_logic_vector(   3 downto  0 );		 -- DSP Strobes
	signal DSP_A		: std_logic_Vector(  15 downto 13 );		 -- DSP Address
	signal DSP_A1		: std_logic_Vector(   7 downto  0 );
	signal DSP_D		: std_logic_vector(  15 downto  0 );		 -- DSP Data Bus
	signal FF_OE		: std_logic;
	signal STAT_OE		: std_logic;
	signal FF_REN		: std_logic;	
	signal FF_RCK		: std_logic;
	signal DSP_READ	: std_logic;
	signal DSP_WD		: std_Logic_Vector(  15 downto 0 );
	signal WE_DCD		: std_logic_Vector( 127 downto 0 ); -- DSP Write Decode

	---------------------------------------------------------------------------------------------------
	component DSPINT
	     port( 	
			IMR     		: in      std_logic;						 -- Master Reset
			CLK20		: in		std_logic;						 -- master Clock
			clk40 		: in		std_logic;
			DSP_WR		: in		std_logic;						 -- DSP Write Strobe
			DSP_STB		: in		std_logic_vector(   3 downto  0 );		 -- DSP Strobes
			DSP_A		: in		std_logic_Vector(  15 downto 13 );		 -- DSP Address
			DSP_A1		: in		std_logic_Vector(   7 downto  0 );
			DSP_D		: in		std_logic_vector(  15 downto  0 );		 -- DSP Data Bus
			STAT_OE		: out	std_logic;
			FF_OE		: out	std_logic;
			FF_REN		: out	std_logic;	
			FF_RCK		: out	std_logic;
			DSP_READ		: out	std_logic;
			DSP_WD		: out	std_Logic_Vector(  15 downto 0 );
			WE_DCD		: out 	std_logic_Vector( 127 downto 0 )); -- DSP Write Decode
	     end component;-- DSPINT;
	---------------------------------------------------------------------------------------------------
begin
	U : DSPINT
	     port map( 	
			IMR     	=> IMR,	
			CLK20	=> CLK20,
			CLK40	=> CLK40,
			DSP_WR	=> DSP_WR,
			DSP_STB	=> DSP_STB,
			DSP_A	=> DSP_A,
			DSP_A1	=> DSP_A1,
			DSP_D	=> DSP_D,
			STAT_OE	=> STAT_OE,
			FF_OE	=> FF_OE,
			FF_REN	=> FF_REN,
			FF_RCK	=> FF_RCK,
			DSP_READ	=> DSP_READ,
			DSP_WD	=> DSP_WD,
			WE_DCD	=> WE_DCD );
	
	imr 		<= '0' after 375 ns;
	
	---------------------------------------------------------	
	clk40_proc : process
	begin
		clk40 <= '0';
		wait for 12 ns;
		clk40 <= '1';
		wait for 13 ns;
	end process;
	---------------------------------------------------------	

	---------------------------------------------------------	
	clk20_proc : process( clk40 )
	begin
		if(( clk40'event ) and ( clk40 = '1' )) then
			clk20 <= not( clk20 );
		end if;
	end process;
	---------------------------------------------------------	
			
	---------------------------------------------------------	
	dsp_proc : process	
	begin
		DSP_WR		<= '1';
		DSP_STB		<= x"F";
		DSP_A		<= "111";
		DSP_A1		<= x"FF";
		DSP_D		<= x"0000";
		wait for 2 us;
		

		-- Write to the FPGA
		assert false
			report "Write to the FPGA"
			severity note;
		for i in 0 to 7 loop
			DSP_A		<= "011";
			DSP_A1		<= conv_std_logic_Vector( i, 8 );
			DSP_WR		<= '0';
			DSP_D		<= conv_std_logic_vector( i, 16 );
			wait for 15 ns;
			DSP_STB		<= x"0";
			wait for 60 ns;
			DSP_STB		<= x"F";
			wait for 10 ns;
			DSP_WR		<= '1';
			DSP_A		<= "111";
			DSP_A1		<= x"FF";
			DSP_D		<= "ZZZZZZZZZZZZZZZZ";
			wait for 2 us;
		end loop;

		-- Read from the FPGA
		assert false
			report "Read From the FPGA"
			severity note;
		for i in 0 to 7 loop
			DSP_A		<= "011";
			DSP_A1		<= conv_std_logic_Vector( i, 8 );
			DSP_WR		<= '1';
			DSP_D		<= conv_std_logic_vector( i, 16 );
			wait for 15 ns;
			DSP_STB		<= x"0";
			wait for 60 ns;
			DSP_STB		<= x"F";
			wait for 10 ns;
			DSP_WR		<= '1';
			DSP_A		<= "111";
			DSP_A1		<= x"FF";
			DSP_D		<= "ZZZZZZZZZZZZZZZZ";
			wait for 2 us;
		end loop;
		
		-- Read the Status
		assert false
			report "Read the FIFO Status"
			severity note;
			
		DSP_A		<= "100";
		DSP_A1		<= x"00";
		DSP_WR		<= '1';
		DSP_D		<= x"1234";
		wait for 15 ns;
		DSP_STB		<= x"0";
		wait for 60 ns;
		DSP_STB		<= x"F";
		wait for 10 ns;
		DSP_A		<= "111";
		DSP_A1		<= x"FF";
		DSP_D		<= "ZZZZZZZZZZZZZZZZ";
		wait for 2 us;
		
		-- Read the Fifo
		assert false
			report "Read the FIFO"
			severity note;
		DSP_A		<= "100";
		DSP_A1		<= x"01";
		DSP_WR		<= '1';
		DSP_D		<= x"5678";
		wait for 15 ns;
		DSP_STB		<= x"0";
		wait for 60 ns;
		DSP_STB		<= x"F";
		wait for 10 ns;
		DSP_A		<= "111";
		DSP_A1		<= x"FF";
		DSP_D		<= "ZZZZZZZZZZZZZZZZ";
		wait for 2 us;
		
		wait ;
		assert false
			report "End of Simulation"
			severity failure;
		
	end process;
	---------------------------------------------------------	
		
		
---------------------------------------------------------------------------------------------------
end test_bench;			-- DSPINT
---------------------------------------------------------------------------------------------------


