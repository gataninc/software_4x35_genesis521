# vcom -reportprogress 300 -work work {gauss_rom.vho}
vcom -reportprogress 300 -work work {dspint.vho}
vcom -reportprogress 300 -work work {_tb.vhd}
vsim -sdftyp /U=dspint_vhd.sdo work.tb
do wave.do
run 40 us