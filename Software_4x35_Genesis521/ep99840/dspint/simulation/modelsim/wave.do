onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/clk40
add wave -noupdate -format Logic -radix hexadecimal /tb/DSP_STB
add wave -noupdate -format Logic -radix binary /tb/DSP_WR
add wave -noupdate -format Logic -radix hexadecimal /tb/DSP_A
add wave -noupdate -format Logic -radix hexadecimal /tb/DSP_A1
add wave -noupdate -format Logic -radix hexadecimal /tb/DSP_D
add wave -noupdate -format Logic -radix binary /tb/STAT_OE
add wave -noupdate -format Logic -radix binary /tb/FF_OE
add wave -noupdate -format Logic -radix binary /tb/FF_REN
add wave -noupdate -format Logic -radix binary /tb/FF_RCK
add wave -noupdate -format Logic -radix binary /tb/DSP_READ
add wave -noupdate -format Logic -radix hexadecimal /tb/DSP_WD
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_DCD
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {3764361 ns} {65731 ns}
WaveRestoreZoom {0 ns} {315 us}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
