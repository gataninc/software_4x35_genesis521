-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:20:46"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	dspint IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	clk40 : IN std_logic;
	DSP_WR : IN std_logic;
	DSP_STB : IN std_logic_vector(3 DOWNTO 0);
	DSP_A : IN std_logic_vector(15 DOWNTO 13);
	DSP_A1 : IN std_logic_vector(7 DOWNTO 0);
	DSP_D : IN std_logic_vector(15 DOWNTO 0);
	FF_FF : IN std_logic;
	FF_DONE : IN std_logic;
	DSO_ENABLE : IN std_logic;
	int_en : IN std_logic;
	ms100_tc : IN std_logic;
	STAT_OE : OUT std_logic;
	FF_OE : OUT std_logic;
	FF_REN : OUT std_logic;
	FF_RCK : OUT std_logic;
	DSP_READ : OUT std_logic;
	DSP_IRQ : OUT std_logic;
	DSP_WD : OUT std_logic_vector(15 DOWNTO 0);
	WE_DCD : OUT std_logic_vector(127 DOWNTO 0)
	);
END dspint;

ARCHITECTURE structure OF dspint IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_clk40 : std_logic;
SIGNAL ww_DSP_WR : std_logic;
SIGNAL ww_DSP_STB : std_logic_vector(3 DOWNTO 0);
SIGNAL ww_DSP_A : std_logic_vector(15 DOWNTO 13);
SIGNAL ww_DSP_A1 : std_logic_vector(7 DOWNTO 0);
SIGNAL ww_DSP_D : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_FF_FF : std_logic;
SIGNAL ww_FF_DONE : std_logic;
SIGNAL ww_DSO_ENABLE : std_logic;
SIGNAL ww_int_en : std_logic;
SIGNAL ww_ms100_tc : std_logic;
SIGNAL ww_STAT_OE : std_logic;
SIGNAL ww_FF_OE : std_logic;
SIGNAL ww_FF_REN : std_logic;
SIGNAL ww_FF_RCK : std_logic;
SIGNAL ww_DSP_READ : std_logic;
SIGNAL ww_DSP_IRQ : std_logic;
SIGNAL ww_DSP_WD : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_WE_DCD : std_logic_vector(127 DOWNTO 0);
SIGNAL DSP_WADR_adffs_a1_a : std_logic;
SIGNAL DSP_WEN_VEC_a1_a : std_logic;
SIGNAL DSP_WEN_VEC_a0_a : std_logic;
SIGNAL Equal_a4477 : std_logic;
SIGNAL FF_OE_a101 : std_logic;
SIGNAL DSP_STB_a2_a_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL FF_DONE_acombout : std_logic;
SIGNAL CLK20_PROC_a131 : std_logic;
SIGNAL DSP_IRQ_CNT_a0_a_a52 : std_logic;
SIGNAL DSP_IRQ_CNT_a1_a : std_logic;
SIGNAL DSP_IRQ_CNT_a1_a_a58 : std_logic;
SIGNAL DSP_IRQ_CNT_a2_a : std_logic;
SIGNAL DSP_IRQ_CNT_a2_a_a49 : std_logic;
SIGNAL DSP_IRQ_CNT_a3_a : std_logic;
SIGNAL iDSP_IRQ_a147 : std_logic;
SIGNAL DSP_IRQ_CNT_a0_a : std_logic;
SIGNAL DSP_IRQ_CNT_a3_a_a55 : std_logic;
SIGNAL DSP_IRQ_CNT_a4_a : std_logic;
SIGNAL iDSP_IRQ_a146 : std_logic;
SIGNAL FF_FF_acombout : std_logic;
SIGNAL IFF_FFV_a0_a : std_logic;
SIGNAL IFF_FFV_a1_a : std_logic;
SIGNAL DSO_ENABLE_acombout : std_logic;
SIGNAL ms100_tc_acombout : std_logic;
SIGNAL iDSP_IRQ_a145 : std_logic;
SIGNAL iDSP_IRQ : std_logic;
SIGNAL int_en_acombout : std_logic;
SIGNAL DSP_IRQ_areg0 : std_logic;
SIGNAL DSP_A1_a0_a_acombout : std_logic;
SIGNAL DSP_STB_a0_a_acombout : std_logic;
SIGNAL DSP_STB_a1_a_acombout : std_logic;
SIGNAL DSP_STB_a3_a_acombout : std_logic;
SIGNAL DSP_READ_a58 : std_logic;
SIGNAL DSP_A1_a5_a_acombout : std_logic;
SIGNAL DSP_A_a14_a_acombout : std_logic;
SIGNAL DSP_A_a15_a_acombout : std_logic;
SIGNAL FF_OE_a96 : std_logic;
SIGNAL DSP_A1_a7_a_acombout : std_logic;
SIGNAL DSP_WR_acombout : std_logic;
SIGNAL DSP_A1_a3_a_acombout : std_logic;
SIGNAL DSP_A1_a6_a_acombout : std_logic;
SIGNAL DSP_A1_a1_a_acombout : std_logic;
SIGNAL DSP_A1_a4_a_acombout : std_logic;
SIGNAL FF_OE_a105 : std_logic;
SIGNAL FF_OE_a103 : std_logic;
SIGNAL STAT_OE_a2 : std_logic;
SIGNAL FF_OE_a0 : std_logic;
SIGNAL clk40_acombout : std_logic;
SIGNAL FF_RCK_areg0 : std_logic;
SIGNAL STAT_OE_VEC_a0_a : std_logic;
SIGNAL STAT_OE_VEC_a1_a : std_logic;
SIGNAL FF_REN_areg0 : std_logic;
SIGNAL DSP_A_a13_a_acombout : std_logic;
SIGNAL DSP_READ_a66 : std_logic;
SIGNAL DSP_READ_a65 : std_logic;
SIGNAL DSP_READ_a60 : std_logic;
SIGNAL DSP_D_a0_a_acombout : std_logic;
SIGNAL DSP_WEN_a11 : std_logic;
SIGNAL DSP_WDATA_adffs_a0_a : std_logic;
SIGNAL DSP_D_a1_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a1_a : std_logic;
SIGNAL DSP_D_a2_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a2_a : std_logic;
SIGNAL DSP_D_a3_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a3_a : std_logic;
SIGNAL DSP_D_a4_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a4_a : std_logic;
SIGNAL DSP_D_a5_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a5_a : std_logic;
SIGNAL DSP_D_a6_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a6_a : std_logic;
SIGNAL DSP_D_a7_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a7_a : std_logic;
SIGNAL DSP_D_a8_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a8_a : std_logic;
SIGNAL DSP_D_a9_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a9_a : std_logic;
SIGNAL DSP_D_a10_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a10_a : std_logic;
SIGNAL DSP_D_a11_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a11_a : std_logic;
SIGNAL DSP_D_a12_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a12_a : std_logic;
SIGNAL DSP_D_a13_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a13_a : std_logic;
SIGNAL DSP_D_a14_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a14_a : std_logic;
SIGNAL DSP_D_a15_a_acombout : std_logic;
SIGNAL DSP_WDATA_adffs_a15_a : std_logic;
SIGNAL DSP_WADR_adffs_a0_a : std_logic;
SIGNAL DSP_A1_a2_a_acombout : std_logic;
SIGNAL DSP_WADR_adffs_a2_a : std_logic;
SIGNAL DSP_WADR_adffs_a3_a : std_logic;
SIGNAL Equal_a4476 : std_logic;
SIGNAL DSP_WADR_adffs_a5_a : std_logic;
SIGNAL DSP_WADR_adffs_a6_a : std_logic;
SIGNAL DSP_WADR_adffs_a4_a : std_logic;
SIGNAL Equal_a4478 : std_logic;
SIGNAL WE_DCD_a0_a_areg0 : std_logic;
SIGNAL Equal_a4480 : std_logic;
SIGNAL WE_DCD_a1_a_areg0 : std_logic;
SIGNAL Equal_a4482 : std_logic;
SIGNAL WE_DCD_a2_a_areg0 : std_logic;
SIGNAL Equal_a4484 : std_logic;
SIGNAL WE_DCD_a3_a_areg0 : std_logic;
SIGNAL Equal_a4486 : std_logic;
SIGNAL WE_DCD_a4_a_areg0 : std_logic;
SIGNAL Equal_a4488 : std_logic;
SIGNAL WE_DCD_a5_a_areg0 : std_logic;
SIGNAL Equal_a4490 : std_logic;
SIGNAL WE_DCD_a6_a_areg0 : std_logic;
SIGNAL Equal_a4492 : std_logic;
SIGNAL WE_DCD_a7_a_areg0 : std_logic;
SIGNAL Equal_a4494 : std_logic;
SIGNAL WE_DCD_a8_a_areg0 : std_logic;
SIGNAL Equal_a4496 : std_logic;
SIGNAL WE_DCD_a9_a_areg0 : std_logic;
SIGNAL Equal_a4498 : std_logic;
SIGNAL WE_DCD_a10_a_areg0 : std_logic;
SIGNAL Equal_a4500 : std_logic;
SIGNAL WE_DCD_a11_a_areg0 : std_logic;
SIGNAL Equal_a4502 : std_logic;
SIGNAL WE_DCD_a12_a_areg0 : std_logic;
SIGNAL Equal_a4504 : std_logic;
SIGNAL WE_DCD_a13_a_areg0 : std_logic;
SIGNAL Equal_a4506 : std_logic;
SIGNAL WE_DCD_a14_a_areg0 : std_logic;
SIGNAL Equal_a4508 : std_logic;
SIGNAL WE_DCD_a15_a_areg0 : std_logic;
SIGNAL Equal_a4510 : std_logic;
SIGNAL WE_DCD_a16_a_areg0 : std_logic;
SIGNAL WE_DCD_a17_a_areg0 : std_logic;
SIGNAL WE_DCD_a18_a_areg0 : std_logic;
SIGNAL WE_DCD_a19_a_areg0 : std_logic;
SIGNAL WE_DCD_a20_a_areg0 : std_logic;
SIGNAL WE_DCD_a21_a_areg0 : std_logic;
SIGNAL WE_DCD_a22_a_areg0 : std_logic;
SIGNAL WE_DCD_a23_a_areg0 : std_logic;
SIGNAL WE_DCD_a24_a_areg0 : std_logic;
SIGNAL WE_DCD_a25_a_areg0 : std_logic;
SIGNAL WE_DCD_a26_a_areg0 : std_logic;
SIGNAL WE_DCD_a27_a_areg0 : std_logic;
SIGNAL WE_DCD_a28_a_areg0 : std_logic;
SIGNAL WE_DCD_a29_a_areg0 : std_logic;
SIGNAL WE_DCD_a30_a_areg0 : std_logic;
SIGNAL WE_DCD_a31_a_areg0 : std_logic;
SIGNAL Equal_a4527 : std_logic;
SIGNAL WE_DCD_a32_a_areg0 : std_logic;
SIGNAL WE_DCD_a33_a_areg0 : std_logic;
SIGNAL WE_DCD_a34_a_areg0 : std_logic;
SIGNAL WE_DCD_a35_a_areg0 : std_logic;
SIGNAL WE_DCD_a36_a_areg0 : std_logic;
SIGNAL WE_DCD_a37_a_areg0 : std_logic;
SIGNAL WE_DCD_a38_a_areg0 : std_logic;
SIGNAL WE_DCD_a39_a_areg0 : std_logic;
SIGNAL WE_DCD_a40_a_areg0 : std_logic;
SIGNAL WE_DCD_a41_a_areg0 : std_logic;
SIGNAL WE_DCD_a42_a_areg0 : std_logic;
SIGNAL WE_DCD_a43_a_areg0 : std_logic;
SIGNAL WE_DCD_a44_a_areg0 : std_logic;
SIGNAL WE_DCD_a45_a_areg0 : std_logic;
SIGNAL WE_DCD_a46_a_areg0 : std_logic;
SIGNAL WE_DCD_a47_a_areg0 : std_logic;
SIGNAL Equal_a4544 : std_logic;
SIGNAL WE_DCD_a48_a_areg0 : std_logic;
SIGNAL WE_DCD_a49_a_areg0 : std_logic;
SIGNAL WE_DCD_a50_a_areg0 : std_logic;
SIGNAL WE_DCD_a51_a_areg0 : std_logic;
SIGNAL WE_DCD_a52_a_areg0 : std_logic;
SIGNAL WE_DCD_a53_a_areg0 : std_logic;
SIGNAL WE_DCD_a54_a_areg0 : std_logic;
SIGNAL WE_DCD_a55_a_areg0 : std_logic;
SIGNAL WE_DCD_a56_a_areg0 : std_logic;
SIGNAL WE_DCD_a57_a_areg0 : std_logic;
SIGNAL WE_DCD_a58_a_areg0 : std_logic;
SIGNAL WE_DCD_a59_a_areg0 : std_logic;
SIGNAL WE_DCD_a60_a_areg0 : std_logic;
SIGNAL WE_DCD_a61_a_areg0 : std_logic;
SIGNAL WE_DCD_a62_a_areg0 : std_logic;
SIGNAL WE_DCD_a63_a_areg0 : std_logic;
SIGNAL Equal_a4561 : std_logic;
SIGNAL WE_DCD_a64_a_areg0 : std_logic;
SIGNAL WE_DCD_a65_a_areg0 : std_logic;
SIGNAL WE_DCD_a66_a_areg0 : std_logic;
SIGNAL WE_DCD_a67_a_areg0 : std_logic;
SIGNAL WE_DCD_a68_a_areg0 : std_logic;
SIGNAL WE_DCD_a69_a_areg0 : std_logic;
SIGNAL WE_DCD_a70_a_areg0 : std_logic;
SIGNAL WE_DCD_a71_a_areg0 : std_logic;
SIGNAL WE_DCD_a72_a_areg0 : std_logic;
SIGNAL WE_DCD_a73_a_areg0 : std_logic;
SIGNAL WE_DCD_a74_a_areg0 : std_logic;
SIGNAL WE_DCD_a75_a_areg0 : std_logic;
SIGNAL WE_DCD_a76_a_areg0 : std_logic;
SIGNAL WE_DCD_a77_a_areg0 : std_logic;
SIGNAL WE_DCD_a78_a_areg0 : std_logic;
SIGNAL WE_DCD_a79_a_areg0 : std_logic;
SIGNAL Equal_a4578 : std_logic;
SIGNAL WE_DCD_a80_a_areg0 : std_logic;
SIGNAL WE_DCD_a81_a_areg0 : std_logic;
SIGNAL WE_DCD_a82_a_areg0 : std_logic;
SIGNAL WE_DCD_a83_a_areg0 : std_logic;
SIGNAL WE_DCD_a84_a_areg0 : std_logic;
SIGNAL WE_DCD_a85_a_areg0 : std_logic;
SIGNAL WE_DCD_a86_a_areg0 : std_logic;
SIGNAL WE_DCD_a87_a_areg0 : std_logic;
SIGNAL WE_DCD_a88_a_areg0 : std_logic;
SIGNAL WE_DCD_a89_a_areg0 : std_logic;
SIGNAL WE_DCD_a90_a_areg0 : std_logic;
SIGNAL WE_DCD_a91_a_areg0 : std_logic;
SIGNAL WE_DCD_a92_a_areg0 : std_logic;
SIGNAL WE_DCD_a93_a_areg0 : std_logic;
SIGNAL WE_DCD_a94_a_areg0 : std_logic;
SIGNAL WE_DCD_a95_a_areg0 : std_logic;
SIGNAL Equal_a4595 : std_logic;
SIGNAL WE_DCD_a96_a_areg0 : std_logic;
SIGNAL WE_DCD_a97_a_areg0 : std_logic;
SIGNAL WE_DCD_a98_a_areg0 : std_logic;
SIGNAL WE_DCD_a99_a_areg0 : std_logic;
SIGNAL WE_DCD_a100_a_areg0 : std_logic;
SIGNAL WE_DCD_a101_a_areg0 : std_logic;
SIGNAL WE_DCD_a102_a_areg0 : std_logic;
SIGNAL WE_DCD_a103_a_areg0 : std_logic;
SIGNAL WE_DCD_a104_a_areg0 : std_logic;
SIGNAL WE_DCD_a105_a_areg0 : std_logic;
SIGNAL WE_DCD_a106_a_areg0 : std_logic;
SIGNAL WE_DCD_a107_a_areg0 : std_logic;
SIGNAL WE_DCD_a108_a_areg0 : std_logic;
SIGNAL WE_DCD_a109_a_areg0 : std_logic;
SIGNAL WE_DCD_a110_a_areg0 : std_logic;
SIGNAL WE_DCD_a111_a_areg0 : std_logic;
SIGNAL Equal_a4612 : std_logic;
SIGNAL WE_DCD_a112_a_areg0 : std_logic;
SIGNAL WE_DCD_a113_a_areg0 : std_logic;
SIGNAL WE_DCD_a114_a_areg0 : std_logic;
SIGNAL WE_DCD_a115_a_areg0 : std_logic;
SIGNAL WE_DCD_a116_a_areg0 : std_logic;
SIGNAL WE_DCD_a117_a_areg0 : std_logic;
SIGNAL WE_DCD_a118_a_areg0 : std_logic;
SIGNAL WE_DCD_a119_a_areg0 : std_logic;
SIGNAL WE_DCD_a120_a_areg0 : std_logic;
SIGNAL WE_DCD_a121_a_areg0 : std_logic;
SIGNAL WE_DCD_a122_a_areg0 : std_logic;
SIGNAL WE_DCD_a123_a_areg0 : std_logic;
SIGNAL WE_DCD_a124_a_areg0 : std_logic;
SIGNAL WE_DCD_a125_a_areg0 : std_logic;
SIGNAL WE_DCD_a126_a_areg0 : std_logic;
SIGNAL WE_DCD_a127_a_areg0 : std_logic;
SIGNAL ALT_INV_FF_OE_a0 : std_logic;
SIGNAL ALT_INV_FF_REN_areg0 : std_logic;
SIGNAL ALT_INV_DSP_IRQ_areg0 : std_logic;
SIGNAL ALT_INV_IMR_acombout : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_clk40 <= clk40;
ww_DSP_WR <= DSP_WR;
ww_DSP_STB <= DSP_STB;
ww_DSP_A <= DSP_A;
ww_DSP_A1 <= DSP_A1;
ww_DSP_D <= DSP_D;
ww_FF_FF <= FF_FF;
ww_FF_DONE <= FF_DONE;
ww_DSO_ENABLE <= DSO_ENABLE;
ww_int_en <= int_en;
ww_ms100_tc <= ms100_tc;
STAT_OE <= ww_STAT_OE;
FF_OE <= ww_FF_OE;
FF_REN <= ww_FF_REN;
FF_RCK <= ww_FF_RCK;
DSP_READ <= ww_DSP_READ;
DSP_IRQ <= ww_DSP_IRQ;
DSP_WD <= ww_DSP_WD;
WE_DCD <= ww_WE_DCD;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_FF_OE_a0 <= NOT FF_OE_a0;
ALT_INV_FF_REN_areg0 <= NOT FF_REN_areg0;
ALT_INV_DSP_IRQ_areg0 <= NOT DSP_IRQ_areg0;
ALT_INV_IMR_acombout <= NOT IMR_acombout;

DSP_WADR_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a1_a = DFFE(DSP_A1_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a1_a);

DSP_WEN_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WEN_VEC_a1_a = DFFE(DSP_WEN_VEC_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DSP_WEN_VEC_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WEN_VEC_a1_a);

DSP_WEN_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WEN_VEC_a0_a = DFFE(!DSP_WR_acombout & DSP_READ_a65, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DSP_WR_acombout,
	datad => DSP_READ_a65,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WEN_VEC_a0_a);

Equal_a4477_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4477 = !DSP_WEN_VEC_a0_a & DSP_WEN_VEC_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DSP_WEN_VEC_a0_a,
	datad => DSP_WEN_VEC_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4477);

DSP_STB_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB(2),
	combout => DSP_STB_a2_a_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

FF_DONE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_DONE,
	combout => FF_DONE_acombout);

CLK20_PROC_a131_I : apex20ke_lcell
-- Equation(s):
-- CLK20_PROC_a131 = FF_DONE_acombout # !iDSP_IRQ

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iDSP_IRQ,
	datad => FF_DONE_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => CLK20_PROC_a131);

DSP_IRQ_CNT_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_CNT_a0_a = DFFE(!GLOBAL(CLK20_PROC_a131) & !DSP_IRQ_CNT_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_IRQ_CNT_a0_a_a52 = CARRY(DSP_IRQ_CNT_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "55AA",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_IRQ_CNT_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => CLK20_PROC_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_CNT_a0_a,
	cout => DSP_IRQ_CNT_a0_a_a52);

DSP_IRQ_CNT_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_CNT_a1_a = DFFE(!GLOBAL(CLK20_PROC_a131) & DSP_IRQ_CNT_a1_a $ DSP_IRQ_CNT_a0_a_a52, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_IRQ_CNT_a1_a_a58 = CARRY(!DSP_IRQ_CNT_a0_a_a52 # !DSP_IRQ_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_IRQ_CNT_a1_a,
	cin => DSP_IRQ_CNT_a0_a_a52,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => CLK20_PROC_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_CNT_a1_a,
	cout => DSP_IRQ_CNT_a1_a_a58);

DSP_IRQ_CNT_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_CNT_a2_a = DFFE(!GLOBAL(CLK20_PROC_a131) & DSP_IRQ_CNT_a2_a $ !DSP_IRQ_CNT_a1_a_a58, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_IRQ_CNT_a2_a_a49 = CARRY(DSP_IRQ_CNT_a2_a & !DSP_IRQ_CNT_a1_a_a58)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_IRQ_CNT_a2_a,
	cin => DSP_IRQ_CNT_a1_a_a58,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => CLK20_PROC_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_CNT_a2_a,
	cout => DSP_IRQ_CNT_a2_a_a49);

DSP_IRQ_CNT_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_CNT_a3_a = DFFE(!GLOBAL(CLK20_PROC_a131) & DSP_IRQ_CNT_a3_a $ DSP_IRQ_CNT_a2_a_a49, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- DSP_IRQ_CNT_a3_a_a55 = CARRY(!DSP_IRQ_CNT_a2_a_a49 # !DSP_IRQ_CNT_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_IRQ_CNT_a3_a,
	cin => DSP_IRQ_CNT_a2_a_a49,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => CLK20_PROC_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_CNT_a3_a,
	cout => DSP_IRQ_CNT_a3_a_a55);

iDSP_IRQ_a147_I : apex20ke_lcell
-- Equation(s):
-- iDSP_IRQ_a147 = FF_DONE_acombout # iDSP_IRQ & (!DSP_IRQ_CNT_a3_a # !DSP_IRQ_CNT_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF2A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_IRQ,
	datab => DSP_IRQ_CNT_a1_a,
	datac => DSP_IRQ_CNT_a3_a,
	datad => FF_DONE_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => iDSP_IRQ_a147);

DSP_IRQ_CNT_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_CNT_a4_a = DFFE(!GLOBAL(CLK20_PROC_a131) & DSP_IRQ_CNT_a3_a_a55 $ !DSP_IRQ_CNT_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_IRQ_CNT_a4_a,
	cin => DSP_IRQ_CNT_a3_a_a55,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => CLK20_PROC_a131,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_CNT_a4_a);

iDSP_IRQ_a146_I : apex20ke_lcell
-- Equation(s):
-- iDSP_IRQ_a146 = iDSP_IRQ & (!DSP_IRQ_CNT_a4_a # !DSP_IRQ_CNT_a0_a # !DSP_IRQ_CNT_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2AAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => iDSP_IRQ,
	datab => DSP_IRQ_CNT_a2_a,
	datac => DSP_IRQ_CNT_a0_a,
	datad => DSP_IRQ_CNT_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => iDSP_IRQ_a146);

FF_FF_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_FF_FF,
	combout => FF_FF_acombout);

IFF_FFV_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- IFF_FFV_a0_a = DFFE(!FF_FF_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => FF_FF_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => IFF_FFV_a0_a);

IFF_FFV_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- IFF_FFV_a1_a = DFFE(IFF_FFV_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => IFF_FFV_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => IFF_FFV_a1_a);

DSO_ENABLE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSO_ENABLE,
	combout => DSO_ENABLE_acombout);

ms100_tc_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ms100_tc,
	combout => ms100_tc_acombout);

iDSP_IRQ_a145_I : apex20ke_lcell
-- Equation(s):
-- iDSP_IRQ_a145 = IFF_FFV_a0_a & !DSO_ENABLE_acombout & (ms100_tc_acombout # !IFF_FFV_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A02",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => IFF_FFV_a0_a,
	datab => IFF_FFV_a1_a,
	datac => DSO_ENABLE_acombout,
	datad => ms100_tc_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => iDSP_IRQ_a145);

iDSP_IRQ_aI : apex20ke_lcell
-- Equation(s):
-- iDSP_IRQ = DFFE(iDSP_IRQ_a147 # iDSP_IRQ_a146 # iDSP_IRQ_a145, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => iDSP_IRQ_a147,
	datac => iDSP_IRQ_a146,
	datad => iDSP_IRQ_a145,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => iDSP_IRQ);

int_en_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_int_en,
	combout => int_en_acombout);

DSP_IRQ_areg0_I : apex20ke_lcell
-- Equation(s):
-- DSP_IRQ_areg0 = DFFE(iDSP_IRQ & int_en_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => iDSP_IRQ,
	datad => int_en_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_IRQ_areg0);

DSP_A1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(0),
	combout => DSP_A1_a0_a_acombout);

DSP_STB_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB(0),
	combout => DSP_STB_a0_a_acombout);

DSP_STB_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB(1),
	combout => DSP_STB_a1_a_acombout);

DSP_STB_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_STB(3),
	combout => DSP_STB_a3_a_acombout);

DSP_READ_a58_I : apex20ke_lcell
-- Equation(s):
-- DSP_READ_a58 = !DSP_STB_a2_a_acombout & !DSP_STB_a0_a_acombout & !DSP_STB_a1_a_acombout & !DSP_STB_a3_a_acombout
-- DSP_READ_a66 = !DSP_STB_a2_a_acombout & !DSP_STB_a0_a_acombout & !DSP_STB_a1_a_acombout & !DSP_STB_a3_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_STB_a2_a_acombout,
	datab => DSP_STB_a0_a_acombout,
	datac => DSP_STB_a1_a_acombout,
	datad => DSP_STB_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_READ_a58,
	cascout => DSP_READ_a66);

DSP_A1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(5),
	combout => DSP_A1_a5_a_acombout);

DSP_A_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(14),
	combout => DSP_A_a14_a_acombout);

DSP_A_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(15),
	combout => DSP_A_a15_a_acombout);

FF_OE_a96_I : apex20ke_lcell
-- Equation(s):
-- FF_OE_a96 = !DSP_A1_a2_a_acombout & !DSP_A1_a5_a_acombout & !DSP_A_a14_a_acombout & DSP_A_a15_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a2_a_acombout,
	datab => DSP_A1_a5_a_acombout,
	datac => DSP_A_a14_a_acombout,
	datad => DSP_A_a15_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FF_OE_a96);

DSP_A1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(7),
	combout => DSP_A1_a7_a_acombout);

DSP_WR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_WR,
	combout => DSP_WR_acombout);

DSP_A1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(3),
	combout => DSP_A1_a3_a_acombout);

DSP_A1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(6),
	combout => DSP_A1_a6_a_acombout);

DSP_A1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(1),
	combout => DSP_A1_a1_a_acombout);

DSP_A1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(4),
	combout => DSP_A1_a4_a_acombout);

FF_OE_a101_I : apex20ke_lcell
-- Equation(s):
-- FF_OE_a105 = !DSP_A_a13_a_acombout & !DSP_A1_a6_a_acombout & !DSP_A1_a1_a_acombout & !DSP_A1_a4_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A_a13_a_acombout,
	datab => DSP_A1_a6_a_acombout,
	datac => DSP_A1_a1_a_acombout,
	datad => DSP_A1_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FF_OE_a101,
	cascout => FF_OE_a105);

FF_OE_a103_I : apex20ke_lcell
-- Equation(s):
-- FF_OE_a103 = (!DSP_A1_a7_a_acombout & DSP_WR_acombout & !DSP_A1_a3_a_acombout) & CASCADE(FF_OE_a105)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0030",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DSP_A1_a7_a_acombout,
	datac => DSP_WR_acombout,
	datad => DSP_A1_a3_a_acombout,
	cascin => FF_OE_a105,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FF_OE_a103);

STAT_OE_a2_I : apex20ke_lcell
-- Equation(s):
-- STAT_OE_a2 = !DSP_A1_a0_a_acombout & DSP_READ_a58 & FF_OE_a96 & FF_OE_a103

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_READ_a58,
	datac => FF_OE_a96,
	datad => FF_OE_a103,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => STAT_OE_a2);

FF_OE_a0_I : apex20ke_lcell
-- Equation(s):
-- FF_OE_a0 = DSP_A1_a0_a_acombout & DSP_READ_a58 & FF_OE_a96 & FF_OE_a103

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A1_a0_a_acombout,
	datab => DSP_READ_a58,
	datac => FF_OE_a96,
	datad => FF_OE_a103,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => FF_OE_a0);

clk40_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk40,
	combout => clk40_acombout);

FF_RCK_areg0_I : apex20ke_lcell
-- Equation(s):
-- FF_RCK_areg0 = DFFE(!FF_RCK_areg0, GLOBAL(clk40_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => FF_RCK_areg0,
	clk => clk40_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_RCK_areg0);

STAT_OE_VEC_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- STAT_OE_VEC_a0_a = DFFE(FF_OE_a96 & DSP_READ_a58 & !DSP_A1_a0_a_acombout & FF_OE_a103, GLOBAL(FF_RCK_areg0), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => FF_OE_a96,
	datab => DSP_READ_a58,
	datac => DSP_A1_a0_a_acombout,
	datad => FF_OE_a103,
	clk => FF_RCK_areg0,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => STAT_OE_VEC_a0_a);

STAT_OE_VEC_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- STAT_OE_VEC_a1_a = DFFE(STAT_OE_VEC_a0_a, GLOBAL(FF_RCK_areg0), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => STAT_OE_VEC_a0_a,
	clk => FF_RCK_areg0,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => STAT_OE_VEC_a1_a);

FF_REN_areg0_I : apex20ke_lcell
-- Equation(s):
-- FF_REN_areg0 = DFFE(!STAT_OE_VEC_a1_a & STAT_OE_VEC_a0_a, GLOBAL(FF_RCK_areg0), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => STAT_OE_VEC_a1_a,
	datad => STAT_OE_VEC_a0_a,
	clk => FF_RCK_areg0,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => FF_REN_areg0);

DSP_A_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A(13),
	combout => DSP_A_a13_a_acombout);

DSP_READ_a65_I : apex20ke_lcell
-- Equation(s):
-- DSP_READ_a65 = (!DSP_A_a15_a_acombout & (DSP_A_a13_a_acombout & DSP_A_a14_a_acombout)) & CASCADE(DSP_READ_a66)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_A_a15_a_acombout,
	datac => DSP_A_a13_a_acombout,
	datad => DSP_A_a14_a_acombout,
	cascin => DSP_READ_a66,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_READ_a65);

DSP_READ_a60_I : apex20ke_lcell
-- Equation(s):
-- DSP_READ_a60 = DSP_WR_acombout & DSP_READ_a65

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DSP_WR_acombout,
	datad => DSP_READ_a65,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_READ_a60);

DSP_D_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(0),
	combout => DSP_D_a0_a_acombout);

DSP_WEN_a11_I : apex20ke_lcell
-- Equation(s):
-- DSP_WEN_a11 = !DSP_WR_acombout & DSP_READ_a65

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => DSP_WR_acombout,
	datad => DSP_READ_a65,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DSP_WEN_a11);

DSP_WDATA_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a0_a = DFFE(DSP_D_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a0_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a0_a);

DSP_D_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(1),
	combout => DSP_D_a1_a_acombout);

DSP_WDATA_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a1_a = DFFE(DSP_D_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a1_a);

DSP_D_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(2),
	combout => DSP_D_a2_a_acombout);

DSP_WDATA_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a2_a = DFFE(DSP_D_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a2_a);

DSP_D_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(3),
	combout => DSP_D_a3_a_acombout);

DSP_WDATA_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a3_a = DFFE(DSP_D_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a3_a);

DSP_D_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(4),
	combout => DSP_D_a4_a_acombout);

DSP_WDATA_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a4_a = DFFE(DSP_D_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a4_a);

DSP_D_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(5),
	combout => DSP_D_a5_a_acombout);

DSP_WDATA_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a5_a = DFFE(DSP_D_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a5_a);

DSP_D_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(6),
	combout => DSP_D_a6_a_acombout);

DSP_WDATA_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a6_a = DFFE(DSP_D_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a6_a);

DSP_D_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(7),
	combout => DSP_D_a7_a_acombout);

DSP_WDATA_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a7_a = DFFE(DSP_D_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a7_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a7_a);

DSP_D_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(8),
	combout => DSP_D_a8_a_acombout);

DSP_WDATA_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a8_a = DFFE(DSP_D_a8_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a8_a);

DSP_D_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(9),
	combout => DSP_D_a9_a_acombout);

DSP_WDATA_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a9_a = DFFE(DSP_D_a9_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a9_a);

DSP_D_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(10),
	combout => DSP_D_a10_a_acombout);

DSP_WDATA_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a10_a = DFFE(DSP_D_a10_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a10_a);

DSP_D_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(11),
	combout => DSP_D_a11_a_acombout);

DSP_WDATA_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a11_a = DFFE(DSP_D_a11_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a11_a);

DSP_D_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(12),
	combout => DSP_D_a12_a_acombout);

DSP_WDATA_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a12_a = DFFE(DSP_D_a12_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a12_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a12_a);

DSP_D_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(13),
	combout => DSP_D_a13_a_acombout);

DSP_WDATA_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a13_a = DFFE(DSP_D_a13_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a13_a);

DSP_D_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(14),
	combout => DSP_D_a14_a_acombout);

DSP_WDATA_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a14_a = DFFE(DSP_D_a14_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a14_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a14_a);

DSP_D_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_D(15),
	combout => DSP_D_a15_a_acombout);

DSP_WDATA_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WDATA_adffs_a15_a = DFFE(DSP_D_a15_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_D_a15_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WDATA_adffs_a15_a);

DSP_WADR_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a0_a = DFFE(DSP_A1_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a0_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a0_a);

DSP_A1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DSP_A1(2),
	combout => DSP_A1_a2_a_acombout);

DSP_WADR_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a2_a = DFFE(DSP_A1_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a2_a);

DSP_WADR_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a3_a = DFFE(DSP_A1_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a3_a);

Equal_a4476_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4476 = !DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4476);

DSP_WADR_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a5_a = DFFE(DSP_A1_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a5_a);

DSP_WADR_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a6_a = DFFE(DSP_A1_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a6_a);

DSP_WADR_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DSP_WADR_adffs_a4_a = DFFE(DSP_A1_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , DSP_WEN_a11)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DSP_A1_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => DSP_WEN_a11,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DSP_WADR_adffs_a4_a);

Equal_a4478_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4478 = Equal_a4477 & !DSP_WADR_adffs_a5_a & !DSP_WADR_adffs_a6_a & !DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4478);

WE_DCD_a0_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a0_a_areg0 = DFFE(Equal_a4476 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4476,
	datad => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a0_a_areg0);

Equal_a4480_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4480 = !DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0004",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4480);

WE_DCD_a1_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a1_a_areg0 = DFFE(Equal_a4480 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4480,
	datac => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a1_a_areg0);

Equal_a4482_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4482 = DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0002",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4482);

WE_DCD_a2_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a2_a_areg0 = DFFE(Equal_a4482 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4482,
	datad => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a2_a_areg0);

Equal_a4484_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4484 = DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4484);

WE_DCD_a3_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a3_a_areg0 = DFFE(Equal_a4478 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a3_a_areg0);

Equal_a4486_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4486 = !DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0010",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4486);

WE_DCD_a4_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a4_a_areg0 = DFFE(Equal_a4478 & Equal_a4486, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a4_a_areg0);

Equal_a4488_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4488 = !DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4488);

WE_DCD_a5_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a5_a_areg0 = DFFE(Equal_a4488 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4488,
	datad => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a5_a_areg0);

Equal_a4490_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4490 = DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4490);

WE_DCD_a6_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a6_a_areg0 = DFFE(Equal_a4478 & Equal_a4490, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a6_a_areg0);

Equal_a4492_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4492 = DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & !DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4492);

WE_DCD_a7_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a7_a_areg0 = DFFE(Equal_a4478 & Equal_a4492, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4492,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a7_a_areg0);

Equal_a4494_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4494 = !DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0100",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4494);

WE_DCD_a8_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a8_a_areg0 = DFFE(Equal_a4478 & (Equal_a4494), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4478,
	datad => Equal_a4494,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a8_a_areg0);

Equal_a4496_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4496 = !DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0400",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4496);

WE_DCD_a9_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a9_a_areg0 = DFFE(Equal_a4496 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4496,
	datac => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a9_a_areg0);

Equal_a4498_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4498 = DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4498);

WE_DCD_a10_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a10_a_areg0 = DFFE(Equal_a4478 & Equal_a4498, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a10_a_areg0);

Equal_a4500_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4500 = DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & !DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4500);

WE_DCD_a11_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a11_a_areg0 = DFFE(Equal_a4500 & Equal_a4478, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4500,
	datad => Equal_a4478,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a11_a_areg0);

Equal_a4502_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4502 = !DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4502);

WE_DCD_a12_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a12_a_areg0 = DFFE(Equal_a4478 & Equal_a4502, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4502,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a12_a_areg0);

Equal_a4504_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4504 = !DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4504);

WE_DCD_a13_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a13_a_areg0 = DFFE(Equal_a4478 & Equal_a4504, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4504,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a13_a_areg0);

Equal_a4506_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4506 = DSP_WADR_adffs_a1_a & !DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4506);

WE_DCD_a14_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a14_a_areg0 = DFFE(Equal_a4478 & Equal_a4506, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4506,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a14_a_areg0);

Equal_a4508_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4508 = DSP_WADR_adffs_a1_a & DSP_WADR_adffs_a0_a & DSP_WADR_adffs_a2_a & DSP_WADR_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DSP_WADR_adffs_a1_a,
	datab => DSP_WADR_adffs_a0_a,
	datac => DSP_WADR_adffs_a2_a,
	datad => DSP_WADR_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4508);

WE_DCD_a15_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a15_a_areg0 = DFFE(Equal_a4478 & Equal_a4508, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4478,
	datad => Equal_a4508,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a15_a_areg0);

Equal_a4510_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4510 = Equal_a4477 & !DSP_WADR_adffs_a5_a & !DSP_WADR_adffs_a6_a & DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0200",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4510);

WE_DCD_a16_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a16_a_areg0 = DFFE(Equal_a4476 & Equal_a4510, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4476,
	datad => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a16_a_areg0);

WE_DCD_a17_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a17_a_areg0 = DFFE(Equal_a4480 & Equal_a4510, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4480,
	datad => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a17_a_areg0);

WE_DCD_a18_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a18_a_areg0 = DFFE(Equal_a4482 & Equal_a4510, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4482,
	datad => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a18_a_areg0);

WE_DCD_a19_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a19_a_areg0 = DFFE(Equal_a4510 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a19_a_areg0);

WE_DCD_a20_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a20_a_areg0 = DFFE(Equal_a4510 & Equal_a4486, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a20_a_areg0);

WE_DCD_a21_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a21_a_areg0 = DFFE(Equal_a4510 & (Equal_a4488), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4510,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a21_a_areg0);

WE_DCD_a22_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a22_a_areg0 = DFFE(Equal_a4510 & (Equal_a4490), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4510,
	datac => Equal_a4490,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a22_a_areg0);

WE_DCD_a23_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a23_a_areg0 = DFFE(Equal_a4510 & Equal_a4492, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4492,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a23_a_areg0);

WE_DCD_a24_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a24_a_areg0 = DFFE(Equal_a4494 & Equal_a4510, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4494,
	datad => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a24_a_areg0);

WE_DCD_a25_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a25_a_areg0 = DFFE(Equal_a4496 & Equal_a4510, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4496,
	datac => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a25_a_areg0);

WE_DCD_a26_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a26_a_areg0 = DFFE(Equal_a4510 & Equal_a4498, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a26_a_areg0);

WE_DCD_a27_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a27_a_areg0 = DFFE(Equal_a4500 & (Equal_a4510), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4500,
	datad => Equal_a4510,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a27_a_areg0);

WE_DCD_a28_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a28_a_areg0 = DFFE(Equal_a4510 & (Equal_a4502), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4510,
	datad => Equal_a4502,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a28_a_areg0);

WE_DCD_a29_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a29_a_areg0 = DFFE(Equal_a4510 & Equal_a4504, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4504,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a29_a_areg0);

WE_DCD_a30_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a30_a_areg0 = DFFE(Equal_a4510 & Equal_a4506, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4506,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a30_a_areg0);

WE_DCD_a31_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a31_a_areg0 = DFFE(Equal_a4510 & Equal_a4508, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4510,
	datad => Equal_a4508,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a31_a_areg0);

Equal_a4527_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4527 = Equal_a4477 & DSP_WADR_adffs_a5_a & !DSP_WADR_adffs_a6_a & !DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0008",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4527);

WE_DCD_a32_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a32_a_areg0 = DFFE(Equal_a4527 & Equal_a4476, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4476,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a32_a_areg0);

WE_DCD_a33_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a33_a_areg0 = DFFE(Equal_a4480 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4480,
	datad => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a33_a_areg0);

WE_DCD_a34_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a34_a_areg0 = DFFE(Equal_a4482 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4482,
	datad => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a34_a_areg0);

WE_DCD_a35_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a35_a_areg0 = DFFE(Equal_a4527 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a35_a_areg0);

WE_DCD_a36_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a36_a_areg0 = DFFE(Equal_a4527 & Equal_a4486, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a36_a_areg0);

WE_DCD_a37_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a37_a_areg0 = DFFE(Equal_a4527 & Equal_a4488, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a37_a_areg0);

WE_DCD_a38_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a38_a_areg0 = DFFE(Equal_a4527 & Equal_a4490, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a38_a_areg0);

WE_DCD_a39_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a39_a_areg0 = DFFE(Equal_a4492 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4492,
	datac => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a39_a_areg0);

WE_DCD_a40_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a40_a_areg0 = DFFE(Equal_a4527 & (Equal_a4494), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4527,
	datad => Equal_a4494,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a40_a_areg0);

WE_DCD_a41_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a41_a_areg0 = DFFE(Equal_a4496 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4496,
	datac => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a41_a_areg0);

WE_DCD_a42_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a42_a_areg0 = DFFE(Equal_a4527 & Equal_a4498, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a42_a_areg0);

WE_DCD_a43_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a43_a_areg0 = DFFE(Equal_a4527 & Equal_a4500, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4500,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a43_a_areg0);

WE_DCD_a44_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a44_a_areg0 = DFFE(Equal_a4502 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4502,
	datac => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a44_a_areg0);

WE_DCD_a45_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a45_a_areg0 = DFFE(Equal_a4527 & Equal_a4504, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4527,
	datad => Equal_a4504,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a45_a_areg0);

WE_DCD_a46_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a46_a_areg0 = DFFE(Equal_a4506 & Equal_a4527, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4506,
	datad => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a46_a_areg0);

WE_DCD_a47_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a47_a_areg0 = DFFE(Equal_a4508 & (Equal_a4527), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4508,
	datad => Equal_a4527,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a47_a_areg0);

Equal_a4544_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4544 = Equal_a4477 & DSP_WADR_adffs_a5_a & !DSP_WADR_adffs_a6_a & DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0800",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4544);

WE_DCD_a48_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a48_a_areg0 = DFFE(Equal_a4476 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4476,
	datac => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a48_a_areg0);

WE_DCD_a49_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a49_a_areg0 = DFFE(Equal_a4480 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4480,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a49_a_areg0);

WE_DCD_a50_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a50_a_areg0 = DFFE(Equal_a4482 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4482,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a50_a_areg0);

WE_DCD_a51_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a51_a_areg0 = DFFE(Equal_a4544 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4544,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a51_a_areg0);

WE_DCD_a52_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a52_a_areg0 = DFFE(Equal_a4544 & Equal_a4486, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4544,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a52_a_areg0);

WE_DCD_a53_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a53_a_areg0 = DFFE(Equal_a4544 & (Equal_a4488), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4544,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a53_a_areg0);

WE_DCD_a54_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a54_a_areg0 = DFFE(Equal_a4544 & Equal_a4490, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4544,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a54_a_areg0);

WE_DCD_a55_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a55_a_areg0 = DFFE(Equal_a4492 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4492,
	datac => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a55_a_areg0);

WE_DCD_a56_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a56_a_areg0 = DFFE(Equal_a4494 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4494,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a56_a_areg0);

WE_DCD_a57_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a57_a_areg0 = DFFE(Equal_a4496 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4496,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a57_a_areg0);

WE_DCD_a58_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a58_a_areg0 = DFFE(Equal_a4544 & Equal_a4498, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4544,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a58_a_areg0);

WE_DCD_a59_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a59_a_areg0 = DFFE(Equal_a4500 & (Equal_a4544), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4500,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a59_a_areg0);

WE_DCD_a60_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a60_a_areg0 = DFFE(Equal_a4544 & (Equal_a4502), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4544,
	datad => Equal_a4502,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a60_a_areg0);

WE_DCD_a61_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a61_a_areg0 = DFFE(Equal_a4504 & Equal_a4544, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4504,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a61_a_areg0);

WE_DCD_a62_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a62_a_areg0 = DFFE(Equal_a4506 & (Equal_a4544), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4506,
	datad => Equal_a4544,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a62_a_areg0);

WE_DCD_a63_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a63_a_areg0 = DFFE(Equal_a4544 & Equal_a4508, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4544,
	datad => Equal_a4508,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a63_a_areg0);

Equal_a4561_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4561 = Equal_a4477 & !DSP_WADR_adffs_a5_a & DSP_WADR_adffs_a6_a & !DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4561);

WE_DCD_a64_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a64_a_areg0 = DFFE(Equal_a4476 & Equal_a4561, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4476,
	datac => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a64_a_areg0);

WE_DCD_a65_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a65_a_areg0 = DFFE(Equal_a4561 & Equal_a4480, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4480,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a65_a_areg0);

WE_DCD_a66_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a66_a_areg0 = DFFE(Equal_a4561 & Equal_a4482, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4482,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a66_a_areg0);

WE_DCD_a67_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a67_a_areg0 = DFFE(Equal_a4561 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a67_a_areg0);

WE_DCD_a68_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a68_a_areg0 = DFFE(Equal_a4561 & Equal_a4486, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a68_a_areg0);

WE_DCD_a69_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a69_a_areg0 = DFFE(Equal_a4561 & Equal_a4488, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a69_a_areg0);

WE_DCD_a70_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a70_a_areg0 = DFFE(Equal_a4490 & Equal_a4561, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4490,
	datad => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a70_a_areg0);

WE_DCD_a71_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a71_a_areg0 = DFFE(Equal_a4561 & Equal_a4492, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4492,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a71_a_areg0);

WE_DCD_a72_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a72_a_areg0 = DFFE(Equal_a4494 & Equal_a4561, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4494,
	datad => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a72_a_areg0);

WE_DCD_a73_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a73_a_areg0 = DFFE(Equal_a4561 & Equal_a4496, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4496,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a73_a_areg0);

WE_DCD_a74_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a74_a_areg0 = DFFE(Equal_a4498 & (Equal_a4561), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4498,
	datad => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a74_a_areg0);

WE_DCD_a75_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a75_a_areg0 = DFFE(Equal_a4500 & Equal_a4561, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4500,
	datac => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a75_a_areg0);

WE_DCD_a76_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a76_a_areg0 = DFFE(Equal_a4502 & Equal_a4561, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4502,
	datad => Equal_a4561,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a76_a_areg0);

WE_DCD_a77_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a77_a_areg0 = DFFE(Equal_a4561 & Equal_a4504, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4504,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a77_a_areg0);

WE_DCD_a78_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a78_a_areg0 = DFFE(Equal_a4561 & Equal_a4506, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4506,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a78_a_areg0);

WE_DCD_a79_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a79_a_areg0 = DFFE(Equal_a4561 & Equal_a4508, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4561,
	datad => Equal_a4508,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a79_a_areg0);

Equal_a4578_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4578 = Equal_a4477 & !DSP_WADR_adffs_a5_a & DSP_WADR_adffs_a6_a & DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4578);

WE_DCD_a80_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a80_a_areg0 = DFFE(Equal_a4578 & Equal_a4476, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4476,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a80_a_areg0);

WE_DCD_a81_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a81_a_areg0 = DFFE(Equal_a4578 & Equal_a4480, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4480,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a81_a_areg0);

WE_DCD_a82_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a82_a_areg0 = DFFE(Equal_a4482 & (Equal_a4578), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4482,
	datad => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a82_a_areg0);

WE_DCD_a83_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a83_a_areg0 = DFFE(Equal_a4484 & Equal_a4578, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4484,
	datad => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a83_a_areg0);

WE_DCD_a84_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a84_a_areg0 = DFFE(Equal_a4486 & Equal_a4578, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4486,
	datad => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a84_a_areg0);

WE_DCD_a85_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a85_a_areg0 = DFFE(Equal_a4488 & Equal_a4578, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4488,
	datad => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a85_a_areg0);

WE_DCD_a86_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a86_a_areg0 = DFFE(Equal_a4578 & Equal_a4490, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a86_a_areg0);

WE_DCD_a87_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a87_a_areg0 = DFFE(Equal_a4492 & Equal_a4578, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4492,
	datad => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a87_a_areg0);

WE_DCD_a88_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a88_a_areg0 = DFFE(Equal_a4578 & Equal_a4494, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4494,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a88_a_areg0);

WE_DCD_a89_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a89_a_areg0 = DFFE(Equal_a4578 & Equal_a4496, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4496,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a89_a_areg0);

WE_DCD_a90_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a90_a_areg0 = DFFE(Equal_a4578 & Equal_a4498, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a90_a_areg0);

WE_DCD_a91_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a91_a_areg0 = DFFE(Equal_a4578 & Equal_a4500, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4500,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a91_a_areg0);

WE_DCD_a92_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a92_a_areg0 = DFFE(Equal_a4502 & (Equal_a4578), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4502,
	datac => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a92_a_areg0);

WE_DCD_a93_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a93_a_areg0 = DFFE(Equal_a4504 & Equal_a4578, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4504,
	datac => Equal_a4578,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a93_a_areg0);

WE_DCD_a94_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a94_a_areg0 = DFFE(Equal_a4578 & Equal_a4506, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4506,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a94_a_areg0);

WE_DCD_a95_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a95_a_areg0 = DFFE(Equal_a4578 & Equal_a4508, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4578,
	datad => Equal_a4508,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a95_a_areg0);

Equal_a4595_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4595 = Equal_a4477 & DSP_WADR_adffs_a5_a & DSP_WADR_adffs_a6_a & !DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0080",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4595);

WE_DCD_a96_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a96_a_areg0 = DFFE(Equal_a4595 & Equal_a4476, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4476,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a96_a_areg0);

WE_DCD_a97_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a97_a_areg0 = DFFE(Equal_a4595 & Equal_a4480, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4480,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a97_a_areg0);

WE_DCD_a98_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a98_a_areg0 = DFFE(Equal_a4595 & Equal_a4482, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4482,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a98_a_areg0);

WE_DCD_a99_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a99_a_areg0 = DFFE(Equal_a4595 & Equal_a4484, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a99_a_areg0);

WE_DCD_a100_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a100_a_areg0 = DFFE(Equal_a4595 & Equal_a4486, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a100_a_areg0);

WE_DCD_a101_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a101_a_areg0 = DFFE(Equal_a4595 & Equal_a4488, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a101_a_areg0);

WE_DCD_a102_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a102_a_areg0 = DFFE(Equal_a4595 & Equal_a4490, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a102_a_areg0);

WE_DCD_a103_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a103_a_areg0 = DFFE(Equal_a4492 & Equal_a4595, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4492,
	datad => Equal_a4595,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a103_a_areg0);

WE_DCD_a104_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a104_a_areg0 = DFFE(Equal_a4595 & Equal_a4494, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4494,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a104_a_areg0);

WE_DCD_a105_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a105_a_areg0 = DFFE(Equal_a4595 & Equal_a4496, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4496,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a105_a_areg0);

WE_DCD_a106_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a106_a_areg0 = DFFE(Equal_a4595 & Equal_a4498, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4498,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a106_a_areg0);

WE_DCD_a107_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a107_a_areg0 = DFFE(Equal_a4595 & Equal_a4500, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4500,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a107_a_areg0);

WE_DCD_a108_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a108_a_areg0 = DFFE(Equal_a4502 & (Equal_a4595), GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4502,
	datac => Equal_a4595,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a108_a_areg0);

WE_DCD_a109_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a109_a_areg0 = DFFE(Equal_a4595 & Equal_a4504, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4595,
	datad => Equal_a4504,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a109_a_areg0);

WE_DCD_a110_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a110_a_areg0 = DFFE(Equal_a4506 & (Equal_a4595), GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4506,
	datac => Equal_a4595,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a110_a_areg0);

WE_DCD_a111_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a111_a_areg0 = DFFE(Equal_a4508 & Equal_a4595, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4508,
	datac => Equal_a4595,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a111_a_areg0);

Equal_a4612_I : apex20ke_lcell
-- Equation(s):
-- Equal_a4612 = Equal_a4477 & DSP_WADR_adffs_a5_a & DSP_WADR_adffs_a6_a & DSP_WADR_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4477,
	datab => DSP_WADR_adffs_a5_a,
	datac => DSP_WADR_adffs_a6_a,
	datad => DSP_WADR_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a4612);

WE_DCD_a112_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a112_a_areg0 = DFFE(Equal_a4612 & Equal_a4476, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4476,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a112_a_areg0);

WE_DCD_a113_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a113_a_areg0 = DFFE(Equal_a4612 & Equal_a4480, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4480,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a113_a_areg0);

WE_DCD_a114_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a114_a_areg0 = DFFE(Equal_a4482 & (Equal_a4612), GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4482,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a114_a_areg0);

WE_DCD_a115_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a115_a_areg0 = DFFE(Equal_a4612 & Equal_a4484, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4484,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a115_a_areg0);

WE_DCD_a116_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a116_a_areg0 = DFFE(Equal_a4612 & Equal_a4486, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4486,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a116_a_areg0);

WE_DCD_a117_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a117_a_areg0 = DFFE(Equal_a4612 & Equal_a4488, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4488,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a117_a_areg0);

WE_DCD_a118_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a118_a_areg0 = DFFE(Equal_a4612 & Equal_a4490, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4490,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a118_a_areg0);

WE_DCD_a119_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a119_a_areg0 = DFFE(Equal_a4492 & Equal_a4612, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4492,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a119_a_areg0);

WE_DCD_a120_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a120_a_areg0 = DFFE(Equal_a4612 & Equal_a4494, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4494,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a120_a_areg0);

WE_DCD_a121_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a121_a_areg0 = DFFE(Equal_a4612 & Equal_a4496, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4496,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a121_a_areg0);

WE_DCD_a122_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a122_a_areg0 = DFFE(Equal_a4498 & Equal_a4612, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4498,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a122_a_areg0);

WE_DCD_a123_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a123_a_areg0 = DFFE(Equal_a4612 & Equal_a4500, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => Equal_a4612,
	datad => Equal_a4500,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a123_a_areg0);

WE_DCD_a124_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a124_a_areg0 = DFFE(Equal_a4502 & Equal_a4612, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4502,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a124_a_areg0);

WE_DCD_a125_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a125_a_areg0 = DFFE(Equal_a4504 & Equal_a4612, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4504,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a125_a_areg0);

WE_DCD_a126_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a126_a_areg0 = DFFE(Equal_a4506 & (Equal_a4612), GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a4506,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a126_a_areg0);

WE_DCD_a127_a_areg0_I : apex20ke_lcell
-- Equation(s):
-- WE_DCD_a127_a_areg0 = DFFE(Equal_a4508 & Equal_a4612, GLOBAL(CLK20_acombout), , , !IMR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C0C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => Equal_a4508,
	datac => Equal_a4612,
	clk => CLK20_acombout,
	ena => ALT_INV_IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_DCD_a127_a_areg0);

DSP_IRQ_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_DSP_IRQ_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_IRQ);

STAT_OE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => STAT_OE_a2,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_STAT_OE);

FF_OE_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_FF_OE_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_OE);

FF_REN_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_FF_REN_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_REN);

FF_RCK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => FF_RCK_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_FF_RCK);

DSP_READ_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_READ_a60,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_READ);

DSP_WD_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(0));

DSP_WD_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(1));

DSP_WD_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(2));

DSP_WD_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(3));

DSP_WD_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(4));

DSP_WD_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(5));

DSP_WD_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(6));

DSP_WD_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(7));

DSP_WD_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(8));

DSP_WD_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(9));

DSP_WD_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(10));

DSP_WD_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(11));

DSP_WD_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(12));

DSP_WD_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(13));

DSP_WD_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(14));

DSP_WD_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => DSP_WDATA_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DSP_WD(15));

WE_DCD_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a0_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(0));

WE_DCD_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a1_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(1));

WE_DCD_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a2_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(2));

WE_DCD_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a3_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(3));

WE_DCD_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a4_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(4));

WE_DCD_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a5_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(5));

WE_DCD_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a6_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(6));

WE_DCD_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a7_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(7));

WE_DCD_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a8_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(8));

WE_DCD_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a9_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(9));

WE_DCD_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a10_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(10));

WE_DCD_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a11_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(11));

WE_DCD_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a12_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(12));

WE_DCD_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a13_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(13));

WE_DCD_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a14_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(14));

WE_DCD_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a15_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(15));

WE_DCD_a16_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a16_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(16));

WE_DCD_a17_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a17_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(17));

WE_DCD_a18_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a18_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(18));

WE_DCD_a19_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a19_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(19));

WE_DCD_a20_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a20_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(20));

WE_DCD_a21_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a21_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(21));

WE_DCD_a22_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a22_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(22));

WE_DCD_a23_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a23_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(23));

WE_DCD_a24_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a24_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(24));

WE_DCD_a25_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a25_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(25));

WE_DCD_a26_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a26_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(26));

WE_DCD_a27_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a27_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(27));

WE_DCD_a28_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a28_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(28));

WE_DCD_a29_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a29_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(29));

WE_DCD_a30_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a30_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(30));

WE_DCD_a31_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a31_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(31));

WE_DCD_a32_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a32_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(32));

WE_DCD_a33_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a33_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(33));

WE_DCD_a34_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a34_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(34));

WE_DCD_a35_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a35_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(35));

WE_DCD_a36_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a36_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(36));

WE_DCD_a37_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a37_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(37));

WE_DCD_a38_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a38_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(38));

WE_DCD_a39_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a39_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(39));

WE_DCD_a40_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a40_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(40));

WE_DCD_a41_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a41_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(41));

WE_DCD_a42_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a42_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(42));

WE_DCD_a43_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a43_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(43));

WE_DCD_a44_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a44_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(44));

WE_DCD_a45_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a45_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(45));

WE_DCD_a46_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a46_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(46));

WE_DCD_a47_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a47_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(47));

WE_DCD_a48_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a48_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(48));

WE_DCD_a49_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a49_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(49));

WE_DCD_a50_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a50_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(50));

WE_DCD_a51_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a51_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(51));

WE_DCD_a52_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a52_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(52));

WE_DCD_a53_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a53_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(53));

WE_DCD_a54_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a54_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(54));

WE_DCD_a55_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a55_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(55));

WE_DCD_a56_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a56_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(56));

WE_DCD_a57_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a57_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(57));

WE_DCD_a58_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a58_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(58));

WE_DCD_a59_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a59_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(59));

WE_DCD_a60_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a60_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(60));

WE_DCD_a61_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a61_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(61));

WE_DCD_a62_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a62_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(62));

WE_DCD_a63_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a63_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(63));

WE_DCD_a64_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a64_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(64));

WE_DCD_a65_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a65_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(65));

WE_DCD_a66_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a66_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(66));

WE_DCD_a67_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a67_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(67));

WE_DCD_a68_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a68_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(68));

WE_DCD_a69_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a69_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(69));

WE_DCD_a70_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a70_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(70));

WE_DCD_a71_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a71_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(71));

WE_DCD_a72_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a72_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(72));

WE_DCD_a73_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a73_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(73));

WE_DCD_a74_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a74_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(74));

WE_DCD_a75_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a75_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(75));

WE_DCD_a76_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a76_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(76));

WE_DCD_a77_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a77_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(77));

WE_DCD_a78_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a78_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(78));

WE_DCD_a79_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a79_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(79));

WE_DCD_a80_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a80_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(80));

WE_DCD_a81_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a81_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(81));

WE_DCD_a82_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a82_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(82));

WE_DCD_a83_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a83_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(83));

WE_DCD_a84_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a84_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(84));

WE_DCD_a85_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a85_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(85));

WE_DCD_a86_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a86_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(86));

WE_DCD_a87_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a87_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(87));

WE_DCD_a88_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a88_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(88));

WE_DCD_a89_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a89_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(89));

WE_DCD_a90_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a90_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(90));

WE_DCD_a91_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a91_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(91));

WE_DCD_a92_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a92_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(92));

WE_DCD_a93_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a93_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(93));

WE_DCD_a94_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a94_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(94));

WE_DCD_a95_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a95_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(95));

WE_DCD_a96_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a96_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(96));

WE_DCD_a97_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a97_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(97));

WE_DCD_a98_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a98_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(98));

WE_DCD_a99_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a99_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(99));

WE_DCD_a100_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a100_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(100));

WE_DCD_a101_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a101_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(101));

WE_DCD_a102_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a102_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(102));

WE_DCD_a103_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a103_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(103));

WE_DCD_a104_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a104_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(104));

WE_DCD_a105_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a105_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(105));

WE_DCD_a106_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a106_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(106));

WE_DCD_a107_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a107_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(107));

WE_DCD_a108_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a108_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(108));

WE_DCD_a109_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a109_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(109));

WE_DCD_a110_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a110_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(110));

WE_DCD_a111_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a111_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(111));

WE_DCD_a112_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a112_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(112));

WE_DCD_a113_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a113_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(113));

WE_DCD_a114_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a114_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(114));

WE_DCD_a115_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a115_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(115));

WE_DCD_a116_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a116_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(116));

WE_DCD_a117_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a117_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(117));

WE_DCD_a118_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a118_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(118));

WE_DCD_a119_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a119_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(119));

WE_DCD_a120_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a120_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(120));

WE_DCD_a121_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a121_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(121));

WE_DCD_a122_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a122_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(122));

WE_DCD_a123_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a123_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(123));

WE_DCD_a124_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a124_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(124));

WE_DCD_a125_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a125_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(125));

WE_DCD_a126_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a126_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(126));

WE_DCD_a127_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => WE_DCD_a127_a_areg0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_WE_DCD(127));
END structure;


