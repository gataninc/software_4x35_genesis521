---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	dspint.VHD
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:	
--		This module interfaces the FPGA to the TI TMP320C32 DSP PRocessor
--	History
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*****************************************************************************************
--		September 22, 2000 - MCS:
--			Final Release
--		*****************************************************************************************
---------------------------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity dspint is 
     port( 	
		IMR     		: in      std_logic;						 -- Master Reset
		CLK20		: in		std_logic;						 -- master Clock
		clk40 		: in		std_logic;
		DSP_WR		: in		std_logic;						 -- DSP Write Strobe
		DSP_STB		: in		std_logic_vector(   3 downto  0 );		 -- DSP Strobes
		DSP_A		: in		std_logic_Vector(  15 downto 13 );		 -- DSP Address
		DSP_A1		: in		std_logic_Vector(   7 downto  0 );
		DSP_D		: in		std_logic_vector(  15 downto  0 );		 -- DSP Data Bus
		FF_FF		: in		std_logic;
		FF_DONE		: in		std_logic;
		DSO_ENABLE	: in		std_logic;
		int_en		: in		std_logic;
		ms100_tc		: in		std_logic;
		STAT_OE		: buffer	std_logic;
		FF_OE		: buffer	std_logic;
		FF_REN		: buffer	std_logic;	
		FF_RCK		: buffer	std_logic;
		DSP_READ		: buffer	std_logic;
		DSP_IRQ		: buffer	std_logic;
		DSP_WD		: buffer	std_Logic_Vector(  15 downto 0 );
		WE_DCD		: buffer 	std_logic_Vector( 127 downto 0 )); -- DSP Write Decode
     end dspint;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture BEHAVIORAL of dspint is

	constant BOARD_ID 			: integer := 3;
	constant FIFO_ID  			: integer := BOARD_ID + 1;
	constant DSP_IRQ_CNT_MAX 	: integer := 31;
	
	type FF_TYPE is (
		FF_IDLE,
		FF_RD1,
		FF_RD2,
		FF_RD3 );
		
	signal FF_RD_STATE			: FF_TYPE;
	signal STAT_OE_VEC			: std_logic_vector( 1 downto 0 );

	signal DSP_WEN				: std_logic;
	signal DSP_WA				: std_logic_Vector( 6 downto 0 );
	signal DSP_WEN_VEC			: std_logic_Vector( 1 downto 0 );

	
	signal IFF_FFV				: std_logic_vector( 1 downto 0 );
	signal iDSP_IRQ			: std_logic;
	signal DSP_IRQ_CNT 			: integer range 0 to DSP_IRQ_CNT_MAX;

begin
	-- FIFO Output Enable ----------------------------------------------------
	-- Status Register, used to create FF_REN but outputs the FIFO Flags to the DSP
	STAT_OE		<= '1' when(  ( DSP_A 		= FIFO_ID ) 
					 	and ( DSP_STB 		= "0000" 	)
					 	and ( DSP_WR 		= '1' 	)
						and ( DSP_A1		= x"00"	)) else '0';	

	-- FIFO Data Output Enable, does not generate FF_REN
	FF_OE		<= '0' when(  ( DSP_A 		= FIFO_ID ) 
					 	and ( DSP_STB 		= "0000" 	)
					 	and ( DSP_WR 		= '1' 	)
						and ( DSP_A1		= x"01"	)) else '1';	

	-- DSP Reading from the FPGA, enable its Data on the output bus ----------
 	DSP_READ		<= '1' when ( ( DSP_A 		= Board_ID ) 
						and ( DSP_STB 		= "0000" 	 ) 
						and ( DSP_WR 		= '1' 	 )) else '0';
	
	-- Register for a Write data from the DSP ( DSP Write Data Enable )
	DSP_WEN		<= '1' when ( ( DSP_A 		= Board_ID )
						and ( DSP_STB 		= "0000" 	 ) 
						and ( DSP_WR 		= '0' 	 )) else '0';

	-- Latch the Data on the Data-Bus as the appropriate time
	DSP_WDATA : LPM_FF
		generic map(
			LPM_WIDTH	=> 16 )
		port map(
			aclr		=> IMR,
			clock	=> CLK20,
			enable	=> DSP_WEN,
			data		=> DSP_D,
			q		=> DSP_WD );
			
	-- Address Bus - Lower Bits
	DSP_WADR	: LPM_FF
		generic map(
			LPM_WIDTH	=> 7 )
		port map(
			aclr		=> IMR,
			clock	=> CLK20,
			enable	=> DSP_WEN,
			data		=> DSP_A1( 6 downto 0 ),
			q		=> DSP_WA );
	-- Register for a Write data from the DSP
	--------------------------------------------------------------------------
	
	--------------------------------------------------------------------------
	CLK20_PROC : Process( IMR, CLK20 )
	begin
		if( IMR = '1' ) then
			DSP_WEN_VEC	<= "00";
			for i in 0 to 99 loop
				WE_DCD(i) <= '0';
			end loop;
			iFF_FFV			<= "00";			
			iDSP_IRQ			<= '0';
			DSP_IRQ_CNT		<= 0;
			DSP_IRQ			<= '1';

		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			DSP_WEN_VEC		<= DSP_WEN_VEC(0) & DSP_WEN;

			for i in 0 to 127 loop
				if(( DSP_WEN_VEC = "10" ) and ( i = DSP_WA ))
					then WE_DCD(i) <= '1';
					else WE_DCD(i) <= '0';
				end if;
			end loop;

			iFF_FFV		<= iFF_FFV(0) & not( FF_FF );
			
			if( FF_DONE = '1' )
				then iDSP_IRQ	<= '1';
			elsif(( DSO_Enable = '0' ) and ( iFF_FFV = "01" ))					-- FIfo Full Interrupt
				then iDSP_IRQ	<= '1';
			elsif(( DSO_Enable = '0' ) and ( iFF_FFV = "11" ) and ( ms100_tc = '1' )) 	-- FIFO is Full 
				then iDSP_IRQ 	<= '1';
			elsif( DSP_IRQ_CNT = DSP_IRQ_CNT_MAX )
				then iDSP_IRQ	<= '0';
			end if;

			if(( iDSP_IRQ = '0' ) or ( FF_DONE = '1' ))
				then DSP_IRQ_CNT <= 0;
				else DSP_IRQ_CNT <= DSP_IRQ_CNT + 1;
			end if;
			
			if(( INT_EN = '1' ) and ( iDSP_IRQ = '1' ))
				then DSP_IRQ <= '0';
				else DSP_IRQ <= '1';
			end if;

		end if;
	end process;
	--------------------------------------------------------------------------

	------------------------------------------------------------------------------
	FF_RCK_PROC : process( FF_RCK, IMR )
	begin
		if( IMR = '1' )  then
			STAT_OE_VEC		<= "00";
			FF_RD_STATE		<= FF_IDLE;
			FF_REN			<= '1';
		elsif(( FF_RCK'Event ) and ( FF_RCK = '1' )) then
			STAT_OE_VEC		<= STAT_OE_VEC(0) & STAT_OE;
			
			if( STAT_OE_VEC = "01" )
				then FF_REN <= '0';
				else FF_REN <= '1';
			end if;
			
--	case FF_RD_STATE is
--		when FF_IDLE => if( STAT_OE_VEC = "01" )
--						then FF_RD_STATE 	<= FF_RD1;
---				  end if;
--		when FF_RD1 => if( FF_RCK = '1' ) 
---					then FF_RD_STATE 	<= FF_RD2;
--					end if;
--		when FF_RD2 => FF_RD_STATE 			<= FF_RD3;
--		when FF_RD3 => FF_RD_STATE 			<= FF_IDLE;
--	end case;
--	
--	if(( FF_RD_STATE = FF_RD1 ) and ( FF_RCK = '1' ))
--		then FF_REN <= '0';
--	elsif( FF_RD_STATE = FF_RD2 )
--		then FF_REN <= '0';
--		else FF_REN <= '1';
--	end if;
		end if;
	end process;
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	CLK40_PRoc : process( CLK40, IMR )
	begin
		if( imr = '1' ) then
			FF_RCK			<= '0';
		elsif(( CLK40'Event ) and ( CLK40 = '1' )) then
			FF_RCK			<= not( FF_RCK );
		end if;
	end process;
	-------------------------------------------------------------------------------
	
---------------------------------------------------------------------------------------------------
end behavioral;			-- dspint
---------------------------------------------------------------------------------------------------


