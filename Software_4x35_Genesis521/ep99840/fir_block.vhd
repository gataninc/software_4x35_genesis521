------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	FIR_BLOCK.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module contains the Adaptive DPP-II Code for the DPP-II Board
--                  
--   History:
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

-------------------------------------------------------------------------------
entity fir_block is 
	port(
		imr				: in		std_logic;					-- Master Reset
		clk20			: in		std_Logic;					-- Master 20Mhz Clock
		clk40			: in		std_logic;					-- Master 40Mhz clock
		ADisc_en			: in		std_logic;					-- Adaptive Disc Enable
		AD_CLR			: in		std_logic;
		otr				: in		std_logic;
		ad_data_in		: in		std_logic_vector( 13 downto 0 );
		fad_data			: in		std_logic_Vector( 15 downto 0 );	-- A/D Data to the Filters
		ad_data 			: in		std_logic_Vector( 15 downto 0 );	-- A/D Data to the Discriminators
		blank_mode		: in		std_logic_vector(  1 downto 0 );	-- Beam Blanking Mode
		blank_Delay		: in		std_logic_Vector( 15 downto 0 );	-- Beam Blanking Delay
		blank_Width		: in		std_Logic_Vector( 15 downto 0 );	-- Beam Blanking Width
		CLIP_MIN			: in		std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
		CLIP_MAX			: in		std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
		debug_sel			: in		std_logic_Vector(  5 downto 0 );	-- Debug Select Code
		decay			: in		std_logic_vector( 15 downto 0 );	-- ADisc Decay
		DSCA_POL			: in		std_logic;					-- Digital SCA Polarity
		DSCA_PW			: in		std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse Width
		Disc_en			: in		std_logic_Vector(  5 downto 0 );	-- Discriminator enabled
		DSP_RAMA			: in		std_logic_Vector( 15 downto 0 );	-- DSP Address Bus
		DSP_WD			: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
		EDX_XFER			: in		std_Logic;					-- XFer flag for all/ROI data
		fgain			: in      std_logic_Vector( 15 downto 0 );	-- Fine Gain
		fir_dly_len		: in		std_logic_Vector(  3 downto 0 );	-- Flat-Top Selection
		fir_offset_inc		: in		std_logic_Vector( 11 downto 0 );	-- Additional Settling Time for PUR
		IPD_End			: in		std_logic;
		hc_rate			: in		std_logic_Vector( 20 downto 0 );	-- High Count Rate Threshold
		hc_threshold		: in		std_logic_Vector( 15 downto 0 );	-- High Count Rate Time Threshold
		memory_access		: in		std_logic;					-- Memory Access Flag
		offset			: in      std_logic_Vector( 15 downto 0 );	-- Offset (Zero)
		PA_Reset			: in		std_Logic;
		Preset_Mode		: in		std_logic_vector(  3 downto 0 );	-- Acquisition Preset Mode
		Preset			: in		std_logic_Vector( 31 downto 0 );	-- Acquisition Preset Value
		PS_SEL			: in		std_logic_Vector(  1 downto 0 );	-- Peak Shift Select (BLR)
		rtd_test			: in		std_logic;
		RMS_Thr			: in		std_logic_Vector( 15 downto 0 );	-- RMS Threshold
		SCA_EN			: in		std_logic;					-- SCA Enable bit
		SHDisc_Dly_Sel 	: in		std_logic_Vector(  8 downto 0 );	-- Super High Speed Discriminator Delay
		SHDisc_In			: in		std_logic;					-- Super High Speed Discriminator Input
		TC_SEL			: in		std_logic_vector(  3 downto 0 );	-- Time Const Select
		Time_Clr			: in		std_logic;					-- Time Clear
		Time_Start		: in		std_logic;					-- Time Start
		Time_Stop			: in		std_logic;					-- Time Stop
		tlevel 			: in		std_logic_vector( 63 downto 0 );	-- Threshold Level
		WE_FIR_MR			: in		std_logic;					-- Filter Master Reset
		WE_DSP_SCA		: in		std_logic;					-- Write to SCA Directly (test)
		WE_SCA_LU			: in		std_logic;					-- Write to SCA Look-up memory
		WE_ROI			: in		std_logic;					-- Write to ROI look-up Memory
		WE_Preset_Ack		: in		std_logic;
		Video_Abort		: in		std_logic;
		BLEVEL			: buffer	std_logic_vector(  7 downto 0 );	-- Baseline Level 
		BLR_PBusy			: buffer	std_logic;
		blevel_upd		: buffer	std_logic;					-- Baseline level Update (FIFO)
		cpeak   			: buffer  std_Logic_Vector( 11 downto 0 );	-- Converted Peak
		CPS 				: buffer	std_logic_Vector( 20 downto 0 );	-- Input Count Rate
		CTIME			: buffer	std_logic_Vector( 31 downto 0 );	-- CLock Time
		DiscCnts			: buffer	std_logic_Vector( 39 downto 0 );	-- Discriminator Counts
		DSO_TRIG_VEC		: buffer	std_logic_vector(  5 downto 0 );	-- DSO Trigger Vector
		FDisc			: buffer	std_logic;					-- Fast Disc Output ( for CPS )
		LTIME			: buffer	std_logic_Vector( 31 downto 0 );	-- Live Time
		LS_MAP_EN			: buffer	std_logic;
		NPS				: buffer	std_logic_Vector( 20 downto 0 );	-- Stored Count Rate
		Meas_Done			: buffer	std_logic;					-- Converted Peak Done (FIFO)
		ms100_tc			: buffer	std_logic;					-- 100ms Timer (RTEM)
		LS_Abort			: buffer	std_logic;
		net_cps			: buffer	std_logic_Vector( 31 downto 0 );	-- Net Input Counts
		net_nps			: buffer	std_logic_Vector( 31 downto 0 );	-- Net Stored Counts
		Preset_Out		: buffer	std_logic;					-- Preset Done (FIFO)
		ROI_LU			: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
		ROI_SUM			: buffer	std_logic_vector( 31 downto 0 );
		SCA				: buffer	std_logic_vector(  7 downto 0 );	-- SCA Output
		sca_lu_data		: buffer	std_logic_vector(  7 downto 0 );	-- SCA LookUp Data
		SHDisc_MDly		: buffer	std_logic_vector(  8 downto 0 );	-- SHDisc Measued Delay
		Time_Enable		: buffer	std_logic;					-- Acquisition enable (FIFO)
		debug			: buffer	std_logic_Vector( 15 downto 0 );	-- Debug Bus
		rms_out			: buffer	std_logic_vector( 15 downto 0 ));
end fir_block;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_block is
	constant Signal_Tap			: integer := 0;
	
	constant DFilter_Gen		: integer := 1;

	constant Beam_BlanK_Gen 		: integer := 1;	
	constant RMS_GEN 			: integer := 1;
	constant SCA_GEN 			: integer := 1;
	constant CntRate_Gen 		: integer := 1;
	
	signal fir_data			: std_logic_Vector( 103 downto 0 );
	
	signal Disc_In  			: std_logic_Vector(  4 downto 0 );

	signal BLR_PDONE			: std_logic;
	signal BLR_PUR				: std_logic;

	signal Signed_AD_Input		: std_logic_vector( 15 downto 0 );
	signal Sh_Disc_dly 			: std_logic;

	signal SCA_LU				: std_logic_Vector( 7 downto 0 );
	signal iSCA				: std_logic_vector( 7 downto 0 );
	signal SCA_RAM_ADDR 		: std_logic_vector( 11 downto 0 );			-- 3571
--	signal offset_lu   			: std_logiC_vector(  8 downto 0 );			-- 11/5/99
	signal beam_blank			: std_logic;
	

	signal SHDisc_Men			: std_logic_Vector( 3 downto 0 );
	signal disc_in1_del			: std_logic;
	signal HDisc_Out			: std_logic;
	signal Time_Clear			: std_logic;
	signal WDS_BUSY			: std_logic;

	signal rtd_done			: std_logic;					-- Ver 4x17
	signal rtd_time			: std_logic_vector( 11 downto 0 );	-- Ver 4x17
	signal PProc_Cpeak			: std_logic_vector( 11 downto 0 );	-- Ver 4x17
	signal PProc_Meas_Done 		: std_logic;					-- Ver 4x17
	
	
	-------------------------------------------------------------------------------
	component fir_Beam_Blank is 
		port(
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_Logic;					-- Master Clock
			fdisc		: in		std_logic;
			Pol			: in		std_logic;
			Delay		: in		std_logic_Vector( 15 downto 0 );
			Width		: in		std_Logic_Vector( 15 downto 0 );
			Blank		: buffer	std_logic );
	end component fir_Beam_Blank;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component FIR_CNTRATE 
	     port( 
			IMR       		: in      std_logic;								-- Master Reset
	          CLK20			: in      std_logic;                                   	-- Master Clock
			OTR				: in		std_logic;
			IFDISC	 		: in		std_logic;
			IPD_END			: in		std_logic;
			MOVE_FLAG			: in		std_logic;
			Video_Abort		: in		std_logic;
			PEAK_DONE			: in		std_logic;
			PA_Reset			: in		std_logic;
			Time_Clr			: in		std_logic;								-- Clear Clock Time and Live Time
			Time_Start		: in		std_logic;
			Time_Stop			: in		std_logic;
			PRESET_MODE		: in		std_logic_Vector(  3 downto 0 );
			PRESET			: in		std_logic_Vector( 31 downto 0 );
			ROI_SUM			: in		std_logic_vector( 31 downto 0 );
			Time_Clear		: buffer	std_logic;
			Time_Enable		: buffer	std_logic;
			CPS				: buffer	std_logic_vector( 20 downto 0 );		-- Input Count Rate
			NPS				: buffer	std_logic_Vector( 20 downto 0 );		-- Output Count Rate
			CTIME			: buffer	std_logic_Vector( 31 downto 0 );
			LTIME			: buffer	std_logic_Vector( 31 downto 0 );
			LS_MAP_EN			: buffer	std_logic;
			net_cps			: buffer	std_logic_Vector( 31 downto 0 );
			net_nps			: buffer	std_logic_Vector( 31 downto 0 );
			Preset_Out		: buffer	std_logic;
			ms100_tc			: buffer	std_logic;
			LS_Abort			: buffer	std_logic;
			WDS_BUSY			: buffer	std_logic );
	     end component FIR_CNTRATE;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Digital Filter for a "Pulsed Reset" Ramp
	component fir_dfilter is 
		port(
			imr				: in		std_logic;					-- Master Reset
			clk20			: in		std_Logic;					-- Master 20Mhz Clock
			AD_CLR			: in		std_logic;
			ADisc_en			: in		std_logic;					-- Adaptive Disc Enable
			Disc_en			: in		std_logic_Vector(  5 downto 0 );	-- Discriminator enabled
			PA_Reset			: in		std_logic;
			SH_Disc_In		: in		std_logic;
			OTR				: in		std_logic;
			ad_data			: in		std_logic_vector(  15 downto 0 );	-- A/D Input for Discriminators Data
			fad_data			: in		std_logic_vector(  15 downto 0 );	-- A/D Input for Filter
			decay			: in		std_logic_vector(  15 downto 0 );	-- ADisc Decay Factor
			fir_dly_len		: in		std_logic_Vector(   3 downto 0 );	-- Flat-Top Selection
			fir_offset_inc		: in		std_logic_Vector( 11 downto 0 );	-- Additional Settling Time for PUR
			offset			: in		std_logic_Vector( 15 downto 0 );
			PS_SEL			: in		std_logic_Vector(   1 downto 0 );	-- Peak Shift Select (BLR)
			TC_SEL			: in		std_logic_vector(   3 downto 0 );	-- Time Const Select
			tlevel 			: in		std_logic_vector(  63 downto 0 );	-- Threshold Level
			BLR_PDONE			: buffer	std_logic;					-- Peak DOne after BLR
			BLR_PUR			: buffer	std_logic;					-- Pile-UP Reject after BLR
			BLR_PBUSY			: buffer	std_logic;					-- Pulse Busy after BLR
			FDisc			: buffer	std_logic;
			blevel_upd		: buffer	std_logic;					-- Baseline level Update (FIFO)
			BLEVEL			: buffer	std_logic_vector(   7 downto 0 );	-- Baseline Level 
			Disc_In  			: buffer	std_logic_Vector(   4 downto 0 );	-- Discriminator Input
			rtd_done 			: buffer	std_logic;
			rtd_time			: buffer	std_logic_vector( 11 downto 0 );
			fir_data			: buffer	std_logic_Vector( 103 downto 0 ));	-- Filter Data ( All of it )
	end component fir_dfilter;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_disccnts is 
		port( 
			IMR				: in		std_Logic;					-- Master Reset
			CLK				: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			PA_Reset			: in		std_logic;
			TimeEnable		: in		std_logic;
			TimeClr			: in		std_logic;
			Disc_In			: in		std_logic;
			Cnts				: buffer	std_logic_Vector(  7 downto 0 ) );
	end component fir_disccnts;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_pproc
		port( 
			imr				: in		std_logic;					-- Master Reset
			clk				: in		std_logic;					-- MAster Clock
			Time_CLr			: in		std_logic;
			Time_Enable		: in		std_logic;
			EDisc_Out			: in		std_logic;					-- Discriminator Level Exceeded
			PUR				: in		std_logic;
			PBUSY			: in		std_logic;
			Peak_Done			: in		std_logic;
			memory_access		: in		std_logic;					-- Memory Access Flag
			ROI_WE			: in		std_Logic;					-- Write to ROI Look-up Memory
--			OFFSET_LU			: in		std_logic_Vector(  8 downto 0 );		
			EDX_XFER			: in		std_Logic;
			CLIP_MIN			: in		std_logic_Vector( 11 downto 0 );	-- Minimum CLIP Value
			CLIP_MAX			: in		std_logic_Vector( 11 downto 0 );	-- Maximum CLIP Value
			fir_out			: in		std_logic_vector( 15 downto 0 );
--			offset			: in		std_logic_Vector( 15 downto 0 );
			DSP_D			: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
			DSP_RAMA			: in		std_logic_Vector( 10 downto 0 );	-- DSP Address Bus
			ROI_LU			: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
			Meas_Done			: buffer	std_logic;					-- Measurement is Done
			ROI_SUM			: buffer	std_logic_vector( 31 downto 0 );
			CPEak			: buffer	std_logic_Vector( 11 downto 0 ) );
	end component fir_pproc;
	-------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	component fir_rms 
		port( 
			IMR            	: in      std_logic;				     -- Master Reset
	     	CLK20          	: in      std_logic;                         -- Master Clock
			OTR				: in		std_logic;
			PA_Reset			: in		std_logic;
			RMS_Thr			: in		std_logic_vector( 15 downto 0 );
			fir_in			: in		std_logic_vector( 15 downto 0 );
			rms_out			: buffer	std_logic_vector( 15 downto 0 ) );	
	end component fir_rms;
	---------------------------------------------------------------------------------------------------


	-------------------------------------------------------------------------------
	component fir_scactrl is 
	     port( 
			IMR       		: in      std_logic;					-- Master Reset
	          CLK20     		: in      std_logic;                         -- Master Clock
	          DSCA_POL			: in		std_logic;					-- Digital SCA Polarity
	          DSP_SCA_WE		: in		std_logic;					-- DSP Write DSCA Directly
			Meas_Done			: in		std_logic;					-- Measurement Done
			Time_Enable		: in		std_logic;					-- EDS Acquisition is active
	          DSCA_PW			: in		std_logic_vector( 15 downto 0 );	-- Digital SCA Pulse WIdth
			DSP_D			: in		std_logic;                      	-- Look-up Table Write Data
			Lu_Data			: in		std_logic;                         -- Look-UP Table Read Data
	          SCA_OUT			: buffer	std_logic );                      	-- Digital Output SCA
	end component fir_scactrl;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_shdisc_dly is 
		port( 
		     imr            	: in      std_logic;                    -- Master Reset
			clkf           	: in      std_logic;			     -- Master Clock Input
			clks				: in		std_logic;
			PA_Reset			: in		std_logic;
			FIR_MR			: in		std_logic;
			dly_len			: in		std_logic_Vector( 8 downto 0 );
			Disc_En			: in		std_logic;
			HDisc_Out			: in		std_logic;
		     Disc_In        	: in      std_logic;
			SHDisc_MEN		: buffer	std_logic_vector(  3 downto 0 );
			SHDisc_MDly 		: buffer	std_logic_vector(  8 downto 0 );		
		     Disc_Out       	: buffer	std_logic );
	end component fir_shdisc_dly;
	-------------------------------------------------------------------------------	

------------------------------------------------------------------------------------
begin
	-------------------------------------------------------------------------------
	-- Digital Filter for a Pulsed Reset Detector
	DF_GEn : if( DFilter_Gen = 1 ) generate
		u_fir_dfilter : Fir_DFilter 
			port map(
				imr				=> imr,				-- Master REset
				clk20			=> CLK20,				-- Master 20Mhz Clock
				AD_CLR			=> AD_CLr,
				ADisc_en			=> ADisc_En,			-- Adaptive Disc Enable
				Disc_en			=> Disc_En,			-- Discriminator enabled
				PA_Reset			=> PA_Reset,
				Sh_Disc_In		=> Sh_Disc_dly,
				OTR				=> otr,
				ad_data			=> Ad_Data,			-- A/D Input for Discriminators
				fad_data			=> Fad_Data,			-- A/D Input for Filter
				decay			=> DecAy,				-- ADisc Decay Factor
				fir_dly_len		=> fir_dly_Len,		-- Flat-Top Selection
				fir_offset_inc		=> fir_offset_inc,
				offset			=> Offset,
				PS_SEL			=> PS_Sel,			-- Peak Shift Select (BLR)
				TC_SEL			=> TC_Sel, 			-- Time Const Select
				tlevel 			=> Tlevel,			-- Threshold Level
				BLR_PDONE			=> BLR_PDone, 			-- Peak DOne after BLR
				BLR_PUR			=> BLR_PUR,			-- Pile-UP Reject after BLR
				BLR_PBUSY			=> BLR_PBUsy,			-- Pulse Busy after BLR
				fdisc			=> FDisc,
				blevel_upd		=> BLEVEL_UPD,			-- Baseline level Update (FIFO)
				BLEVEL			=> BLEVEL,			-- Baseline Level 
				Disc_In  			=> Disc_In,			-- Discriminator Inputs
				rtd_done 			=> RTD_Done,
				rtd_time			=> Rtd_Time,
				fir_data			=> fir_Data );			-- Filter Data ( All of it )
	end generate;
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Logic to Count "Triggers" for each Disc Pulse
	-- Documented 8/08/03 - MCS
	DiscCntGen : for i in 0 to 4 generate
		U : fir_disccnts
			port map(
				imr		=> imr,							-- Master Clock
				clk		=> clk20,							-- Master Reset
				PA_Reset	=> PA_Reset,						-- Preamp Reset, 						-- PreAmp Busy
				TimeEnable=> time_enable,					-- Acquisition Enabled
				TimeClr	=> Time_Clear,						-- Clear Time
				Disc_In	=> Disc_In(i),						-- Discriminator Input (Pulses)
				Cnts		=> DiscCnts( (8*i)+7 downto 8*i ) );	-- Discriminator Counts
	end generate;
	-------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- Posst Processing Logic, Fine Gain, Zero, Clip Min, Clip Max, etc....
	UFIR_PPROC : fir_pproc
		port map(
			imr				=> IMR,						-- Master Reset
			clk				=> clk20,						-- Mater Clock
			Time_Clr			=> Time_Clear,
			Time_Enable		=> Time_Enable,
			EDisc_Out			=> Disc_In(4), 			-- Disc_in(4),					-- BLM Threshold Output
			Pur				=> BLR_PUR, 				-- PUR from BLR
			PBusy			=> BLR_PBUSY,				-- BLR_PBUSY,					-- Pulse Busy from BLR
			Peak_Done			=> BLR_PDONE,				-- BLR_PDONE,					-- Peak Done from BLR
			memory_access		=> Memory_Access,				-- DSP Access Memory
			ROI_WE			=> WE_ROI,					-- ROI lookup Write Enable
			EDX_XFER			=> EDX_XFER,					-- XFer Flag (0=all,1=ROI)
			CLIP_MIN			=> CLIP_MIN,					-- Clip Min values
			CLIP_MAX			=> CLIP_MAX,					-- Clip Max falues
			fir_out			=> fir_Data( 103 downto 88 ),		-- Filter Output
			DSP_D			=> DSP_WD,					-- DSP Data Bus
			DSP_RAMA			=> DSP_RAMA( 10 downto 0 ),		-- DSP Address Bus				
			ROI_LU			=> ROI_LU,					-- ROI Look_up data
			Meas_Done			=> PProc_meas_done,					-- Measurement Done
			ROI_Sum			=> ROI_Sum,					-- Increment ROI
			CPEak			=> PProc_CPeak );			-- Gain/Zero Product (for lookup)


	     -----------------------------------------------------------------------------------
				
				
		cpeak 	<= rtd_time( 11 downto 0 ) when ( rtd_test = '1' ) else PProc_cpeak;
		Meas_Done <= rtd_Done 			  when ( rtd_test = '1' ) else PProc_Meas_Done;
		

     -----------------------------------------------------------------------------------
	Gen_CntRate : if( CntRate_Gen = 1 ) generate
	     -----------------------------------------------------------------------------------
		-- CountRate related parameters, as well as clock time and live time
		-- Documented 8/08/03 - MCS
	U_FIR_CNTRATE : FIR_CNTRATE
			port map(
				IMR				=> IMR,					-- Master Reset
				CLK20			=> clk20,					-- 10Mhz Clock
				OTR				=> OTR,
				IFDISC	 		=> FDISC,					-- Fast Discriminator Input
				IPD_END			=> IPD_END,				-- Interpixel Delay End
				MOVE_FLAG			=> '0',					-- No WDS Move Flag
				Video_Abort		=> Video_Abort,			-- Video Abort Signal
				PEAK_DONE			=> BLR_PDONE,				-- Peak Meas Done
				PA_Reset			=> PA_Reset,				-- Preamp Reset
				Time_Clr			=> TIME_CLR,				-- Clear Clock and Live Time
				Time_Start		=> TIME_START,				-- Start Acquisition Timer
				Time_Stop			=> Time_Stop,				-- Time Stop
				Preset_Mode		=> Preset_Mode,			-- Preset Mode
				Preset			=> Preset,				-- Preset Value
				ROI_Sum			=> ROI_Sum,				-- Accumulated ROI Sum
				Time_Clear		=> Time_Clear,
				Time_Enable		=> Time_Enable,			-- Acquisition Enabled
				CPS 				=> CPS,					-- Input Count Rate
				NPS				=> NPS, 					-- Output Count Rate
				CTIME			=> CTIME,					-- Clock Time
				LTIME			=> LTIME,					-- Live Time
				LS_MAP_EN			=> LS_Map_En,				-- LSM Mode 
				net_cps			=> net_cps,				-- Net Input Counts
				net_nps			=> net_nps,				-- Net Output Counts
				Preset_Out		=> Preset_Out,				-- Preset DOne Flag
				ms100_tc			=> ms100_tc,				-- 100ms Timer
				LS_Abort			=> LS_Abort,				-- Live Spectrum Mapping Abort
				WDS_BUSY			=> WDS_Busy );				-- Not used
	end generate;
     -----------------------------------------------------------------------------------

     -------------------------------------------------------------------------------
	-- RMS Detection of the Baseline Level
	GEN_RMS : if( RMS_GEN = 1 ) generate
		UFIR_RMS : fir_rms
			port map(
				imr			=> imr,
				clk20		=> clk20,
				OTR			=> OTR,
				PA_Reset		=> PA_Reset, 
				rms_Thr		=> RMS_Thr,
				fir_in		=> fir_data( 88+15 downto 88 ),
				rms_out		=> rms_out );
	end generate;
	------------------------------------------------------------------------------

	------------------------------------------------------------------------------
	-- Beam Blanking Geneation
	-- Documented 7/30/03
	Gen_BB : if( Beam_BlanK_Gen = 1 ) generate
		U_FIR_BB : fir_Beam_Blank 
			port map(
				imr		=> imr,							-- Master Reset
				clk		=> clk20,							-- Master Clock
				fdisc	=> fdisc,							-- FDisc Input
				Pol		=> blank_mode(0),					-- Blank Polarity
				Delay	=> Blank_delay,					-- Blank Delay
				Width	=> Blank_Width,					-- Blank Width
				Blank	=> Beam_blank );					-- Blank Signal
	end generate;
	------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------
	SCA_CTRL_GEN : if( SCA_GEN = 1 ) generate
		sca_ctrl_gen : for i in 0 to 7 generate
			U_SCACTRL : FIR_SCACTRL 
				port map( 
				IMR       	=> IMR,							-- Master Reset
				CLK20    		=> clk20,							-- Master Clock
				DSCA_POL		=> DSCA_POL,						-- Digital SCA Polarity Select
				DSP_SCA_WE	=> WE_DSP_SCA,						-- Write SCA Directly
				Meas_Done		=> meas_done,						-- Measurement Done
				Time_Enable	=> time_enable,					-- EDS Acquisition is active
				DSCA_PW		=> DSCA_PW,						-- Digital SCA Pulse WIdth
				DSP_D		=> DSP_WD(i),						-- DSP Write Data Bit
				Lu_Data		=> SCA_LU(i),						-- Look-UP Table Read Data
				SCA_OUT		=> iSCA(i) );						-- Digital Output SCA

			SCA_LU(i) <= '0' when ( SCA_En = '0' ) else Sca_Lu_Data(i);


		end generate;							-- for i..
		-------------------------------------------------------------------------------
		-- Ver 3571 - CHange lookup memory from 2kx16 to 4k x 9
		-- Non-Linearity Offset/SCA Look-Up
		SCA_RAM_ADDR <= DSP_RAMA( 11 downto 0 ) when ( Memory_Access = '1' ) else
				 	 CPeak( 11 downto 0 );

		SCA_RAM : LPM_RAM_DQ
			generic map(
				LPM_WIDTH			=> 8,
				LPM_WIDTHAD		=> 12,
				LPM_INDATA		=> "REGISTERED",
				LPM_ADDRESS_CONTROL	=> "REGISTERED",
				LPM_OUTDATA		=> "REGISTERED",
				LPM_FILE			=> "NL_RAM.MIF",
				LPM_TYPE			=> "LPM_RAM_DQ" )
			port map(
				inclock			=> clk20,
				outclock			=> clk20,
				data				=> DSP_WD( 7 downto 0 ), 
				address			=> SCA_RAM_ADDR,
				we				=> WE_SCA_LU,
				q				=> sca_lu_data( 7 downto 0 ) );
	end generate;			-- SCA Gen

	SCA(7)			<= Beam_blank when ( Blank_Mode(1) = '1' ) else iSCA(7);
	SCA( 6 downto 0 ) 	<= iSCA( 6 downto 0 );
     -----------------------------------------------------------------------------------
	disc_Clk_proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			disc_in1_del <= '0';
			hdisc_out	<= '0';
		elsif(( clk20'event ) and ( clk20 = '1' )) then
			Disc_In1_Del <= Disc_In(1);
			if(( Disc_In(1) = '1' ) or ( Disc_In1_Del = '1' ))
				then HDIsc_Out <= '1';
				else HDisc_Out <= '0';
			end if;
		end if;
	end process;
	------------------------------------------------------------------------------
	-- Super High Speed Discriminator Delay Lines to match up analog delays
	
	ufir_shdisc_dly : fir_shdisc_dly
		port map(
			IMR 					=> IMR,
			CLKF					=> CLK40,
			CLKS					=> clk20,
			PA_Reset				=> PA_Reset,
			FIR_MR				=> WE_FIR_MR,
			DLY_LEN				=> SHDisc_Dly_Sel,
			Disc_En				=> Disc_En(0),		-- Super High Speed Discriminator Enable
			HDisc_Out				=> HDisc_Out,		-- High Speed Discriminator Input
			Disc_In				=> SHDisc_In,	
			SHDisc_MEN			=> SHDisc_Men,
			SHDisc_MDly 			=> SHDisc_MDly,
			Disc_Out				=> Sh_Disc_dly );	
	-- Super High Speed Discriminator Delay Lines to match up analog delays
	-----------------------------------------------------------------------------------
	ad_data_in_offset : lpm_add_sub
		generic map(	
			LPM_WIDTH		=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_DIRECTION		=> "SUB" )
		port map(
			cin				=> '1',
			dataa			=> "00" & ad_data_in,
			datab			=> conv_std_logic_Vector( 8192, 16 ),
			result			=> Signed_Ad_Input );
	-------------------------------------------------------------------------------
	-- DSO Trigger
	-- Address: 2178
	-- Data: 
	--	0 = No Trigger
	--	1 = FDISC
	--	2 = PreAmp Reset    
	DSO_TRIG_VEC	<= "011" & PA_RESET & FDISC	& "1";			-- Bits 5:0
	
	with conv_integer( Debug_Sel ) select
		Debug <=   
			"00" & ad_data_in						when 0,	-- Raw unsigned A/D input
			Signed_Ad_Input						when 1,	-- Raw Signed A/D Input
		     fad_data								when 2,   -- Filter A/D Input Data		
			fir_data(  15 downto  0 ) 				when 3,	-- High FIR
			fir_data(  31 downto 16 ) 				when 4,	-- Medium FIR
			fir_data(  47 downto 32 ) 				when 5,	-- Low FIR
			fir_data(  63 downto 48 )				when 6,	-- Energy BLR FIR
			fir_data(  83 downto 68 ) 				when 7,	-- Energy FIR
			fir_data( 103 downto 88 ) 				when 8,	-- BLR Corrected FIR				
			tlevel(    15 downto  0 )				when 14,
			tlevel(    31 downto 16 )				when 15,
			tlevel(    47 downto 32 )				when 16,
			tlevel(    63 downto 48 )				when	17,
			rms_out								when 20,
			SHDisc_MDly( 6 downto 0 )				-- 
			& '0'								-- 8
			& SHDisc_Men							-- 7:4
			& Disc_In(2)							-- 3
			& Disc_In(1)							-- 2
			& Disc_in(0)							-- 1
			& SHDisc_in							when 30,	-- 0
				
			x"0000" 								when others;
				
------------------------------------------------------------------------------------
end behavioral;               -- Fir_Block
------------------------------------------------------------------------------------