---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_rms04.VHD
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

entity tb is
end tb;

architecture test_bench of tb is

	signal imr 		: std_logic := '1';
	signal clk20		: std_logic := '0';
	signal OTR		: std_logic;
	signal PA_RESET 	: std_logic;
	signal fir_in		: std_logic_Vector( 7 downto 0 );
	signal rms_out		: std_logic_vector( 15 downto 0 );
	signal RMS_En		: std_logic;
	signal rms_upd		: std_logic;
	signal RMS_Thr		: std_logic_Vector( 7 downto 0 );
	signal Fir_In_Range	: std_logic;

     ----------------------------------------------------------------------------------------------
	component fir_rms
		port(
			IMR            : in      std_logic;				     -- Master Reset
	     	CLK20          : in      std_logic;                         -- Master Clock
			OTR			: in		std_logic;
			PA_Reset		: in		std_logic;
			RMS_Thr		: in		std_logic_vector(  7 downto 0 );
			fir_in		: in		std_logic_vector(  7 downto 0 );
			RMS_En		: out	std_logic;
			Fir_In_Range	: out	std_logic;
			rms_out		: out	std_logic_vector( 15 downto 0 ) );	
	end component fir_rms;
     ----------------------------------------------------------------------------------------------
	
begin
	imr	<= '0' after 555 ns;
	clk20	<= not( clk20 ) after 25 ns;
	OTR		<= '0';
	PA_RESET	<= '0';
	RMS_Thr	<= x"20";
	
	U : fir_rms
		port map(
			imr			=> imr,
			clk20		=> clk20,
			OTR			=> OTR,
			PA_Reset		=> PA_Reset,
			RMS_Thr		=> RMS_Thr,
			fir_in		=> fir_in,
			RMS_En		=> RMS_En,
			Fir_In_Range	=> Fir_In_Range,
			rms_out		=> rms_out );

	fir_in_proc : process
	begin
		fir_in	<= x"10";
		wait for 2 us;
		for i in 0 to 20 loop
			wait until (( Clk20'event ) and ( Clk20 = '1' ));
			fir_in <= fir_in + 1;
		end loop;
		wait for 2 us;
		for i in 0 to 20 loop
			wait until (( Clk20'event ) and ( Clk20 = '1' ));
			fir_in <= fir_in - 1;
		end loop;
		wait ;
	end process;
	
---------------------------------------------------------------------------------------------------
end test_bench;			-- fir_rms.vhd
---------------------------------------------------------------------------------------------------

