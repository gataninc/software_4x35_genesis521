-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 10:10:23"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_rms IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	OTR : IN std_logic;
	PA_Reset : IN std_logic;
	RMS_Thr : IN std_logic_vector(15 DOWNTO 0);
	fir_in : IN std_logic_vector(15 DOWNTO 0);
	rms_out : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_rms;

ARCHITECTURE structure OF fir_rms IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_OTR : std_logic;
SIGNAL ww_PA_Reset : std_logic;
SIGNAL ww_RMS_Thr : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_fir_in : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_rms_out : std_logic_vector(15 DOWNTO 0);
SIGNAL rms_cnt_cnt_a3_a : std_logic;
SIGNAL rms_cnt_cnt_a1_a : std_logic;
SIGNAL Equal_a206 : std_logic;
SIGNAL rms_cnt_cnt_a6_a : std_logic;
SIGNAL rms_cnt_cnt_a4_a : std_logic;
SIGNAL Equal_a207 : std_logic;
SIGNAL rms_cnt_cnt_a8_a : std_logic;
SIGNAL Equal_a208 : std_logic;
SIGNAL rms_cnt_cnt_a13_a : std_logic;
SIGNAL Equal_a209 : std_logic;
SIGNAL Equal_a210 : std_logic;
SIGNAL rms_cnt_cnt_a17_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a15_a : std_logic;
SIGNAL abs_input_alcarry_a14_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a14_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a13_a_a1045 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a14_a : std_logic;
SIGNAL abs_input_alcarry_a13_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a13_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a14_a : std_logic;
SIGNAL abs_input_alcarry_a7_a_aCOUT : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_app_carry_node_a3_a_a47 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a261 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259 : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a12_a_a1046 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a13_a : std_logic;
SIGNAL abs_input_alcarry_a12_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a12_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a13_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a2_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a1_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a7_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a3_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a5_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263 : std_logic;
SIGNAL fir_in_reg_ff_adffs_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a11_a_a1047 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a12_a : std_logic;
SIGNAL abs_input_alcarry_a11_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a11_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a12_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a10_a_a1048 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a11_a : std_logic;
SIGNAL abs_input_alcarry_a10_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a10_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a11_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_a182 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_a218 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a9_a_a1049 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a10_a : std_logic;
SIGNAL abs_input_alcarry_a9_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a9_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a10_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_a183 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_a_a2 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_a182 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a8_a_a1050 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a9_a : std_logic;
SIGNAL abs_input_alcarry_a8_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a8_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a9_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a7_a_a1051 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a8_a : std_logic;
SIGNAL fir_in_reg_ff_adffs_a8_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a6_a_a1052 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a7_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_a183 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a5_a_a1053 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_a185 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_a184 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a4_a_a1054 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a5_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_a186 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_a185 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a3_a_a1055 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_a186 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74 : std_logic;
SIGNAL fir_in_square_mult_amult_core_app_carry_node_a2_a_a48 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a2_a_a1056 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a3_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1 : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a1_a_a1057 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a2_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a0_a_a1058 : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a1_a : std_logic;
SIGNAL rms_thr_reg_ff_adffs_a0_a : std_logic;
SIGNAL Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70 : std_logic;
SIGNAL RMS_Thr_a15_a_acombout : std_logic;
SIGNAL RMS_Thr_a14_a_acombout : std_logic;
SIGNAL fir_in_a14_a_acombout : std_logic;
SIGNAL RMS_Thr_a13_a_acombout : std_logic;
SIGNAL fir_in_a13_a_acombout : std_logic;
SIGNAL fir_in_a2_a_acombout : std_logic;
SIGNAL fir_in_a1_a_acombout : std_logic;
SIGNAL fir_in_a7_a_acombout : std_logic;
SIGNAL fir_in_a3_a_acombout : std_logic;
SIGNAL fir_in_a0_a_acombout : std_logic;
SIGNAL fir_in_a5_a_acombout : std_logic;
SIGNAL fir_in_a6_a_acombout : std_logic;
SIGNAL fir_in_a4_a_acombout : std_logic;
SIGNAL RMS_Thr_a12_a_acombout : std_logic;
SIGNAL fir_in_a12_a_acombout : std_logic;
SIGNAL RMS_Thr_a11_a_acombout : std_logic;
SIGNAL fir_in_a11_a_acombout : std_logic;
SIGNAL RMS_Thr_a10_a_acombout : std_logic;
SIGNAL fir_in_a10_a_acombout : std_logic;
SIGNAL RMS_Thr_a9_a_acombout : std_logic;
SIGNAL fir_in_a9_a_acombout : std_logic;
SIGNAL RMS_Thr_a8_a_acombout : std_logic;
SIGNAL fir_in_a8_a_acombout : std_logic;
SIGNAL RMS_Thr_a7_a_acombout : std_logic;
SIGNAL RMS_Thr_a6_a_acombout : std_logic;
SIGNAL RMS_Thr_a5_a_acombout : std_logic;
SIGNAL RMS_Thr_a4_a_acombout : std_logic;
SIGNAL RMS_Thr_a3_a_acombout : std_logic;
SIGNAL RMS_Thr_a2_a_acombout : std_logic;
SIGNAL RMS_Thr_a1_a_acombout : std_logic;
SIGNAL RMS_Thr_a0_a_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL PA_Reset_acombout : std_logic;
SIGNAL OTR_acombout : std_logic;
SIGNAL RMS_En_a15 : std_logic;
SIGNAL rms_cnt_cnt_a0_a : std_logic;
SIGNAL rms_cnt_cnt_a0_a_a1164 : std_logic;
SIGNAL rms_cnt_cnt_a1_a_a1161 : std_logic;
SIGNAL rms_cnt_cnt_a2_a : std_logic;
SIGNAL rms_cnt_cnt_a2_a_a1158 : std_logic;
SIGNAL rms_cnt_cnt_a3_a_a1155 : std_logic;
SIGNAL rms_cnt_cnt_a4_a_a1176 : std_logic;
SIGNAL rms_cnt_cnt_a5_a : std_logic;
SIGNAL rms_cnt_cnt_a5_a_a1173 : std_logic;
SIGNAL rms_cnt_cnt_a6_a_a1170 : std_logic;
SIGNAL rms_cnt_cnt_a7_a : std_logic;
SIGNAL rms_cnt_cnt_a7_a_a1167 : std_logic;
SIGNAL rms_cnt_cnt_a8_a_a1188 : std_logic;
SIGNAL rms_cnt_cnt_a9_a : std_logic;
SIGNAL rms_cnt_cnt_a9_a_a1185 : std_logic;
SIGNAL rms_cnt_cnt_a10_a : std_logic;
SIGNAL rms_cnt_cnt_a10_a_a1182 : std_logic;
SIGNAL rms_cnt_cnt_a11_a : std_logic;
SIGNAL rms_cnt_cnt_a11_a_a1179 : std_logic;
SIGNAL rms_cnt_cnt_a12_a : std_logic;
SIGNAL rms_cnt_cnt_a12_a_a1200 : std_logic;
SIGNAL rms_cnt_cnt_a13_a_a1197 : std_logic;
SIGNAL rms_cnt_cnt_a14_a : std_logic;
SIGNAL rms_cnt_cnt_a14_a_a1194 : std_logic;
SIGNAL rms_cnt_cnt_a15_a : std_logic;
SIGNAL rms_cnt_cnt_a15_a_a1191 : std_logic;
SIGNAL rms_cnt_cnt_a16_a : std_logic;
SIGNAL rms_cnt_cnt_a16_a_a1212 : std_logic;
SIGNAL rms_cnt_cnt_a17_a_a1209 : std_logic;
SIGNAL rms_cnt_cnt_a18_a : std_logic;
SIGNAL rms_cnt_cnt_a18_a_a1206 : std_logic;
SIGNAL rms_cnt_cnt_a19_a : std_logic;
SIGNAL Equal_a211 : std_logic;
SIGNAL rms_cnt_cnt_a19_a_a1203 : std_logic;
SIGNAL rms_cnt_cnt_a20_a : std_logic;
SIGNAL Equal_a212 : std_logic;
SIGNAL rms_upd_a0 : std_logic;
SIGNAL acc_ff_adffs_a0_a : std_logic;
SIGNAL acc_ff_adffs_a0_a_a299 : std_logic;
SIGNAL acc_ff_adffs_a1_a : std_logic;
SIGNAL acc_ff_adffs_a1_a_a296 : std_logic;
SIGNAL acc_ff_adffs_a2_a : std_logic;
SIGNAL acc_ff_adffs_a2_a_a293 : std_logic;
SIGNAL acc_ff_adffs_a3_a : std_logic;
SIGNAL acc_ff_adffs_a3_a_a290 : std_logic;
SIGNAL acc_ff_adffs_a4_a : std_logic;
SIGNAL acc_ff_adffs_a4_a_a287 : std_logic;
SIGNAL acc_ff_adffs_a5_a : std_logic;
SIGNAL acc_ff_adffs_a5_a_a284 : std_logic;
SIGNAL acc_ff_adffs_a6_a : std_logic;
SIGNAL acc_ff_adffs_a6_a_a281 : std_logic;
SIGNAL acc_ff_adffs_a7_a : std_logic;
SIGNAL acc_ff_adffs_a7_a_a278 : std_logic;
SIGNAL acc_ff_adffs_a8_a : std_logic;
SIGNAL acc_ff_adffs_a8_a_a275 : std_logic;
SIGNAL acc_ff_adffs_a9_a : std_logic;
SIGNAL acc_ff_adffs_a9_a_a272 : std_logic;
SIGNAL acc_ff_adffs_a10_a : std_logic;
SIGNAL acc_ff_adffs_a10_a_a269 : std_logic;
SIGNAL acc_ff_adffs_a11_a : std_logic;
SIGNAL acc_ff_adffs_a11_a_a266 : std_logic;
SIGNAL acc_ff_adffs_a12_a : std_logic;
SIGNAL acc_ff_adffs_a12_a_a263 : std_logic;
SIGNAL acc_ff_adffs_a13_a : std_logic;
SIGNAL acc_ff_adffs_a13_a_a260 : std_logic;
SIGNAL acc_ff_adffs_a14_a : std_logic;
SIGNAL rms_out_ff_adffs_a0_a : std_logic;
SIGNAL acc_ff_adffs_a14_a_a212 : std_logic;
SIGNAL acc_ff_adffs_a15_a : std_logic;
SIGNAL rms_out_ff_adffs_a1_a : std_logic;
SIGNAL acc_ff_adffs_a15_a_a215 : std_logic;
SIGNAL acc_ff_adffs_a16_a : std_logic;
SIGNAL rms_out_ff_adffs_a2_a : std_logic;
SIGNAL acc_ff_adffs_a16_a_a218 : std_logic;
SIGNAL acc_ff_adffs_a17_a : std_logic;
SIGNAL rms_out_ff_adffs_a3_a : std_logic;
SIGNAL acc_ff_adffs_a17_a_a221 : std_logic;
SIGNAL acc_ff_adffs_a18_a : std_logic;
SIGNAL rms_out_ff_adffs_a4_a : std_logic;
SIGNAL acc_ff_adffs_a18_a_a224 : std_logic;
SIGNAL acc_ff_adffs_a19_a : std_logic;
SIGNAL rms_out_ff_adffs_a5_a : std_logic;
SIGNAL acc_ff_adffs_a19_a_a227 : std_logic;
SIGNAL acc_ff_adffs_a20_a : std_logic;
SIGNAL rms_out_ff_adffs_a6_a : std_logic;
SIGNAL acc_ff_adffs_a20_a_a230 : std_logic;
SIGNAL acc_ff_adffs_a21_a : std_logic;
SIGNAL rms_out_ff_adffs_a7_a : std_logic;
SIGNAL acc_ff_adffs_a21_a_a233 : std_logic;
SIGNAL acc_ff_adffs_a22_a : std_logic;
SIGNAL rms_out_ff_adffs_a8_a : std_logic;
SIGNAL fir_in_a15_a_acombout : std_logic;
SIGNAL fir_in_reg_ff_adffs_a15_a : std_logic;
SIGNAL abs_input_alcarry_a0_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a1_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a2_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a3_a : std_logic;
SIGNAL abs_input_alcarry_a3_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a4_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a5_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a6_a_aCOUT : std_logic;
SIGNAL abs_input_alcarry_a7_a : std_logic;
SIGNAL abs_input_alcarry_a5_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24 : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a : std_logic;
SIGNAL abs_input_alcarry_a6_a : std_logic;
SIGNAL abs_input_alcarry_a4_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_a219 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_a220 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64 : std_logic;
SIGNAL abs_input_alcarry_a2_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_a221 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65 : std_logic;
SIGNAL abs_input_alcarry_a1_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_a222 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66 : std_logic;
SIGNAL fir_in_reg_ff_adffs_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_a223 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67 : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a256 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_a_a5 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_a184 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_a_a0 : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298 : std_logic;
SIGNAL fir_in_square_mult_amult_core_app_carry_node_a1_a_a49 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a300 : std_logic;
SIGNAL fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 : std_logic;
SIGNAL acc_ff_adffs_a22_a_a236 : std_logic;
SIGNAL acc_ff_adffs_a23_a : std_logic;
SIGNAL rms_out_ff_adffs_a9_a : std_logic;
SIGNAL acc_ff_adffs_a23_a_a239 : std_logic;
SIGNAL acc_ff_adffs_a24_a : std_logic;
SIGNAL rms_out_ff_adffs_a10_a : std_logic;
SIGNAL acc_ff_adffs_a24_a_a242 : std_logic;
SIGNAL acc_ff_adffs_a25_a : std_logic;
SIGNAL rms_out_ff_adffs_a11_a : std_logic;
SIGNAL acc_ff_adffs_a25_a_a245 : std_logic;
SIGNAL acc_ff_adffs_a26_a : std_logic;
SIGNAL rms_out_ff_adffs_a12_a : std_logic;
SIGNAL acc_ff_adffs_a26_a_a248 : std_logic;
SIGNAL acc_ff_adffs_a27_a : std_logic;
SIGNAL rms_out_ff_adffs_a13_a : std_logic;
SIGNAL acc_ff_adffs_a27_a_a251 : std_logic;
SIGNAL acc_ff_adffs_a28_a : std_logic;
SIGNAL rms_out_ff_adffs_a14_a : std_logic;
SIGNAL acc_ff_adffs_a28_a_a254 : std_logic;
SIGNAL acc_ff_adffs_a29_a : std_logic;
SIGNAL rms_out_ff_adffs_a15_a : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_OTR <= OTR;
ww_PA_Reset <= PA_Reset;
ww_RMS_Thr <= RMS_Thr;
ww_fir_in <= fir_in;
rms_out <= ww_rms_out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

rms_cnt_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a3_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a3_a $ (rms_cnt_cnt_a2_a_a1158), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a3_a_a1155 = CARRY(!rms_cnt_cnt_a2_a_a1158 # !rms_cnt_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a3_a,
	cin => rms_cnt_cnt_a2_a_a1158,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a3_a,
	cout => rms_cnt_cnt_a3_a_a1155);

rms_cnt_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a1_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a1_a $ (rms_cnt_cnt_a0_a_a1164), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a1_a_a1161 = CARRY(!rms_cnt_cnt_a0_a_a1164 # !rms_cnt_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a1_a,
	cin => rms_cnt_cnt_a0_a_a1164,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a1_a,
	cout => rms_cnt_cnt_a1_a_a1161);

Equal_a206_I : apex20ke_lcell
-- Equation(s):
-- Equal_a206 = rms_cnt_cnt_a1_a & rms_cnt_cnt_a2_a & rms_cnt_cnt_a3_a & rms_cnt_cnt_a0_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a1_a,
	datab => rms_cnt_cnt_a2_a,
	datac => rms_cnt_cnt_a3_a,
	datad => rms_cnt_cnt_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a206);

rms_cnt_cnt_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a6_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a6_a $ (!rms_cnt_cnt_a5_a_a1173), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a6_a_a1170 = CARRY(rms_cnt_cnt_a6_a & (!rms_cnt_cnt_a5_a_a1173))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a6_a,
	cin => rms_cnt_cnt_a5_a_a1173,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a6_a,
	cout => rms_cnt_cnt_a6_a_a1170);

rms_cnt_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a4_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a4_a $ (!rms_cnt_cnt_a3_a_a1155), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a4_a_a1176 = CARRY(rms_cnt_cnt_a4_a & (!rms_cnt_cnt_a3_a_a1155))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a4_a,
	cin => rms_cnt_cnt_a3_a_a1155,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a4_a,
	cout => rms_cnt_cnt_a4_a_a1176);

Equal_a207_I : apex20ke_lcell
-- Equation(s):
-- Equal_a207 = rms_cnt_cnt_a6_a & rms_cnt_cnt_a7_a & rms_cnt_cnt_a4_a & rms_cnt_cnt_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a6_a,
	datab => rms_cnt_cnt_a7_a,
	datac => rms_cnt_cnt_a4_a,
	datad => rms_cnt_cnt_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a207);

rms_cnt_cnt_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a8_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a8_a $ (!rms_cnt_cnt_a7_a_a1167), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a8_a_a1188 = CARRY(rms_cnt_cnt_a8_a & (!rms_cnt_cnt_a7_a_a1167))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A50A",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a8_a,
	cin => rms_cnt_cnt_a7_a_a1167,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a8_a,
	cout => rms_cnt_cnt_a8_a_a1188);

Equal_a208_I : apex20ke_lcell
-- Equation(s):
-- Equal_a208 = rms_cnt_cnt_a8_a & rms_cnt_cnt_a11_a & rms_cnt_cnt_a10_a & rms_cnt_cnt_a9_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a8_a,
	datab => rms_cnt_cnt_a11_a,
	datac => rms_cnt_cnt_a10_a,
	datad => rms_cnt_cnt_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a208);

rms_cnt_cnt_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a13_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a13_a $ (rms_cnt_cnt_a12_a_a1200), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a13_a_a1197 = CARRY(!rms_cnt_cnt_a12_a_a1200 # !rms_cnt_cnt_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a13_a,
	cin => rms_cnt_cnt_a12_a_a1200,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a13_a,
	cout => rms_cnt_cnt_a13_a_a1197);

Equal_a209_I : apex20ke_lcell
-- Equation(s):
-- Equal_a209 = rms_cnt_cnt_a13_a & rms_cnt_cnt_a12_a & rms_cnt_cnt_a15_a & rms_cnt_cnt_a14_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a13_a,
	datab => rms_cnt_cnt_a12_a,
	datac => rms_cnt_cnt_a15_a,
	datad => rms_cnt_cnt_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a209);

Equal_a210_I : apex20ke_lcell
-- Equation(s):
-- Equal_a210 = Equal_a208 & Equal_a209 & Equal_a206 & Equal_a207

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a208,
	datab => Equal_a209,
	datac => Equal_a206,
	datad => Equal_a207,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a210);

rms_cnt_cnt_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a17_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a17_a $ (rms_cnt_cnt_a16_a_a1212), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a17_a_a1209 = CARRY(!rms_cnt_cnt_a16_a_a1212 # !rms_cnt_cnt_a17_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a17_a,
	cin => rms_cnt_cnt_a16_a_a1212,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a17_a,
	cout => rms_cnt_cnt_a17_a_a1209);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a300 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a300);

rms_thr_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a15_a = DFFE(RMS_Thr_a15_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a15_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a15_a);

abs_input_alcarry_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a14_a = fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a14_a $ !abs_input_alcarry_a13_a_aCOUT
-- abs_input_alcarry_a14_a_aCOUT = CARRY(!abs_input_alcarry_a13_a_aCOUT & (fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a14_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a15_a,
	datab => fir_in_reg_ff_adffs_a14_a,
	cin => abs_input_alcarry_a13_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a14_a,
	cout => abs_input_alcarry_a14_a_aCOUT);

Thr_Compare_acomparator_acmp_end_aagb_out_node : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_aagb_out = rms_thr_reg_ff_adffs_a15_a & (Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088 # !abs_input_alcarry_a14_a_aCOUT) # !rms_thr_reg_ff_adffs_a15_a & !abs_input_alcarry_a14_a_aCOUT & 
-- Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "CF0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_thr_reg_ff_adffs_a15_a,
	datad => Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088,
	cin => abs_input_alcarry_a14_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_aagb_out);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 = fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a $ (fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a261 $ 
-- fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a,
	datad => fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a261,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262 & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & (!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a308);

RMS_Thr_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(15),
	combout => RMS_Thr_a15_a_acombout);

Thr_Compare_acomparator_acmp_end_alcarry_a13_a_a1045_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a13_a = CARRY(rms_thr_reg_ff_adffs_a13_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a12_a # !abs_input_alcarry_a13_a) # !rms_thr_reg_ff_adffs_a13_a & !abs_input_alcarry_a13_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a13_a,
	datab => abs_input_alcarry_a13_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a13_a_a1045,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a13_a);

rms_thr_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a14_a = DFFE(RMS_Thr_a14_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a14_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a14_a);

abs_input_alcarry_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a13_a = fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a13_a $ abs_input_alcarry_a12_a_aCOUT
-- abs_input_alcarry_a13_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a15_a $ !fir_in_reg_ff_adffs_a13_a # !abs_input_alcarry_a12_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a15_a,
	datab => fir_in_reg_ff_adffs_a13_a,
	cin => abs_input_alcarry_a12_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a13_a,
	cout => abs_input_alcarry_a13_a_aCOUT);

fir_in_reg_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a14_a = DFFE(fir_in_a14_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a14_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a14_a);

abs_input_alcarry_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a7_a = fir_in_reg_ff_adffs_a7_a $ fir_in_reg_ff_adffs_a15_a $ abs_input_alcarry_a6_a_aCOUT
-- abs_input_alcarry_a7_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a7_a $ !fir_in_reg_ff_adffs_a15_a # !abs_input_alcarry_a6_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a7_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a6_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a7_a,
	cout => abs_input_alcarry_a7_a_aCOUT);

fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a = abs_input_alcarry_a7_a & !abs_input_alcarry_a3_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a2_a) # !abs_input_alcarry_a7_a & abs_input_alcarry_a3_a & (!abs_input_alcarry_a2_a # 
-- !abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1A58",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a7_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a);

fir_in_square_mult_amult_core_app_carry_node_a3_a_a47_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_app_carry_node_a3_a_a47 = abs_input_alcarry_a7_a & (!abs_input_alcarry_a6_a # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a6_a,
	datad => abs_input_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_app_carry_node_a3_a_a47);

fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 = fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a & (!fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a # 
-- !fir_in_square_mult_amult_core_app_carry_node_a3_a_a47)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0AAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a,
	datac => fir_in_square_mult_amult_core_app_carry_node_a3_a_a47,
	datad => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259 = fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a $ fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a261 = CARRY(fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a & (fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265) # !fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a & fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00037_aout_bit_a0_a,
	datab => fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a261);

fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58 = abs_input_alcarry_a5_a & !abs_input_alcarry_a7_a & (abs_input_alcarry_a3_a $ abs_input_alcarry_a4_a) # !abs_input_alcarry_a5_a & abs_input_alcarry_a7_a & (abs_input_alcarry_a3_a $ 
-- abs_input_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1428",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a312);

Thr_Compare_acomparator_acmp_end_alcarry_a12_a_a1046_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a12_a = CARRY(abs_input_alcarry_a12_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a11_a # !rms_thr_reg_ff_adffs_a12_a) # !abs_input_alcarry_a12_a & !rms_thr_reg_ff_adffs_a12_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a12_a,
	datab => rms_thr_reg_ff_adffs_a12_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a12_a_a1046,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a12_a);

rms_thr_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a13_a = DFFE(RMS_Thr_a13_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a13_a);

RMS_Thr_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(14),
	combout => RMS_Thr_a14_a_acombout);

abs_input_alcarry_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a12_a = fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a12_a $ !abs_input_alcarry_a11_a_aCOUT
-- abs_input_alcarry_a12_a_aCOUT = CARRY(!abs_input_alcarry_a11_a_aCOUT & (fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a12_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a15_a,
	datab => fir_in_reg_ff_adffs_a12_a,
	cin => abs_input_alcarry_a11_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a12_a,
	cout => abs_input_alcarry_a12_a_aCOUT);

fir_in_reg_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a13_a = DFFE(fir_in_a13_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a13_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a13_a);

fir_in_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(14),
	combout => fir_in_a14_a_acombout);

fir_in_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a2_a = DFFE(fir_in_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a2_a);

fir_in_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a1_a = DFFE(fir_in_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a1_a);

fir_in_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a7_a = DFFE(fir_in_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a7_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a7_a);

fir_in_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a3_a = DFFE(fir_in_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a3_a);

fir_in_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a5_a = DFFE(fir_in_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a5_a);

fir_in_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a6_a = DFFE(fir_in_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a6_a);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84 $ fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84 & 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269 # !fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0) # 
-- !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84 & !fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84,
	datab => fir_in_square_mult_amult_core_apadder_abooth_adder_right_aadder_aunreg_res_node_a0_a_a0,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a265);

fir_in_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a4_a = DFFE(fir_in_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a4_a);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a6_a # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5AF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270 & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 & (!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a316);

Thr_Compare_acomparator_acmp_end_alcarry_a11_a_a1047_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a11_a = CARRY(rms_thr_reg_ff_adffs_a11_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a10_a # !abs_input_alcarry_a11_a) # !rms_thr_reg_ff_adffs_a11_a & !abs_input_alcarry_a11_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a11_a,
	datab => abs_input_alcarry_a11_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a11_a_a1047,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a11_a);

rms_thr_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a12_a = DFFE(RMS_Thr_a12_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a12_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a12_a);

RMS_Thr_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(13),
	combout => RMS_Thr_a13_a_acombout);

abs_input_alcarry_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a11_a = fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a11_a $ abs_input_alcarry_a10_a_aCOUT
-- abs_input_alcarry_a11_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a15_a $ !fir_in_reg_ff_adffs_a11_a # !abs_input_alcarry_a10_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a15_a,
	datab => fir_in_reg_ff_adffs_a11_a,
	cin => abs_input_alcarry_a10_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a11_a,
	cout => abs_input_alcarry_a11_a_aCOUT);

fir_in_reg_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a12_a = DFFE(fir_in_a12_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a12_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a12_a);

fir_in_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(13),
	combout => fir_in_a13_a_acombout);

fir_in_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(2),
	combout => fir_in_a2_a_acombout);

fir_in_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(1),
	combout => fir_in_a1_a_acombout);

fir_in_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(7),
	combout => fir_in_a7_a_acombout);

fir_in_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(3),
	combout => fir_in_a3_a_acombout);

fir_in_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(0),
	combout => fir_in_a0_a_acombout);

fir_in_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(5),
	combout => fir_in_a5_a_acombout);

fir_in_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(6),
	combout => fir_in_a6_a_acombout);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85 $ fir_in_square_mult_amult_core_apadder_a_a5 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85 & fir_in_square_mult_amult_core_apadder_a_a5 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85 & (fir_in_square_mult_amult_core_apadder_a_a5 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85,
	datab => fir_in_square_mult_amult_core_apadder_a_a5,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a269);

fir_in_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(4),
	combout => fir_in_a4_a_acombout);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259 & fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a259,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a320);

Thr_Compare_acomparator_acmp_end_alcarry_a10_a_a1048_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a10_a = CARRY(rms_thr_reg_ff_adffs_a10_a & abs_input_alcarry_a10_a & !Thr_Compare_acomparator_acmp_end_alcarry_a9_a # !rms_thr_reg_ff_adffs_a10_a & (abs_input_alcarry_a10_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a10_a,
	datab => abs_input_alcarry_a10_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a10_a_a1048,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a10_a);

rms_thr_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a11_a = DFFE(RMS_Thr_a11_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a11_a);

RMS_Thr_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(12),
	combout => RMS_Thr_a12_a_acombout);

abs_input_alcarry_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a10_a = fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a10_a $ !abs_input_alcarry_a9_a_aCOUT
-- abs_input_alcarry_a10_a_aCOUT = CARRY(!abs_input_alcarry_a9_a_aCOUT & (fir_in_reg_ff_adffs_a15_a $ fir_in_reg_ff_adffs_a10_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a15_a,
	datab => fir_in_reg_ff_adffs_a10_a,
	cin => abs_input_alcarry_a9_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a10_a,
	cout => abs_input_alcarry_a10_a_aCOUT);

fir_in_reg_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a11_a = DFFE(fir_in_a11_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a11_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a11_a);

fir_in_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(12),
	combout => fir_in_a12_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_a182 = abs_input_alcarry_a2_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a6_a # !abs_input_alcarry_a1_a) # !abs_input_alcarry_a2_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a6_a 
-- # !abs_input_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F7EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a2_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_a182);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84 = (abs_input_alcarry_a3_a & (abs_input_alcarry_a7_a # abs_input_alcarry_a1_a $ !abs_input_alcarry_a2_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a1_a $ !abs_input_alcarry_a2_a # 
-- !abs_input_alcarry_a7_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_a182)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EDB7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a2_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a6_a_a182,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a6_a_a84);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_a218 = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a6_a # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5AF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_a218);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62 = (abs_input_alcarry_a5_a & (abs_input_alcarry_a6_a # !abs_input_alcarry_a7_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_a218)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA5F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a5_a_a218,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278 & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263 & (!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a263,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a324);

Thr_Compare_acomparator_acmp_end_alcarry_a9_a_a1049_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a9_a = CARRY(abs_input_alcarry_a9_a & rms_thr_reg_ff_adffs_a9_a & !Thr_Compare_acomparator_acmp_end_alcarry_a8_a # !abs_input_alcarry_a9_a & (rms_thr_reg_ff_adffs_a9_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a9_a,
	datab => rms_thr_reg_ff_adffs_a9_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a9_a_a1049,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a9_a);

rms_thr_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a10_a = DFFE(RMS_Thr_a10_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a10_a);

RMS_Thr_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(11),
	combout => RMS_Thr_a11_a_acombout);

abs_input_alcarry_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a9_a = fir_in_reg_ff_adffs_a9_a $ fir_in_reg_ff_adffs_a15_a $ abs_input_alcarry_a8_a_aCOUT
-- abs_input_alcarry_a9_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a9_a $ !fir_in_reg_ff_adffs_a15_a # !abs_input_alcarry_a8_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a9_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a8_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a9_a,
	cout => abs_input_alcarry_a9_a_aCOUT);

fir_in_reg_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a10_a = DFFE(fir_in_a10_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a10_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a10_a);

fir_in_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(11),
	combout => fir_in_a11_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_a183 = abs_input_alcarry_a5_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a2_a # !abs_input_alcarry_a1_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a2_a 
-- # !abs_input_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F7EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_a183);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85 = (abs_input_alcarry_a3_a & (abs_input_alcarry_a6_a # abs_input_alcarry_a2_a $ !abs_input_alcarry_a1_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a2_a $ !abs_input_alcarry_a1_a # 
-- !abs_input_alcarry_a6_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_a183)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EBD7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a2_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a5_a_a183,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a5_a_a85);

fir_in_square_mult_amult_core_apadder_a_a2_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_a_a2 = fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a $ (fir_in_square_mult_amult_core_app_carry_node_a3_a_a47 & fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5AAA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a,
	datac => fir_in_square_mult_amult_core_app_carry_node_a3_a_a47,
	datad => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_a_a2);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_a182 = abs_input_alcarry_a5_a & (abs_input_alcarry_a6_a # abs_input_alcarry_a3_a # abs_input_alcarry_a4_a) # !abs_input_alcarry_a5_a & (!abs_input_alcarry_a4_a # !abs_input_alcarry_a3_a 
-- # !abs_input_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BFFD",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a6_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_a182);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218 = abs_input_alcarry_a3_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a # 
-- !abs_input_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EDB7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326 = fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282 & fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a267,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a328);

Thr_Compare_acomparator_acmp_end_alcarry_a8_a_a1050_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a8_a = CARRY(rms_thr_reg_ff_adffs_a8_a & abs_input_alcarry_a8_a & !Thr_Compare_acomparator_acmp_end_alcarry_a7_a # !rms_thr_reg_ff_adffs_a8_a & (abs_input_alcarry_a8_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a8_a,
	datab => abs_input_alcarry_a8_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a8_a_a1050,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a8_a);

rms_thr_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a9_a = DFFE(RMS_Thr_a9_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a9_a);

RMS_Thr_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(10),
	combout => RMS_Thr_a10_a_acombout);

abs_input_alcarry_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a8_a = fir_in_reg_ff_adffs_a8_a $ fir_in_reg_ff_adffs_a15_a $ !abs_input_alcarry_a7_a_aCOUT
-- abs_input_alcarry_a8_a_aCOUT = CARRY(!abs_input_alcarry_a7_a_aCOUT & (fir_in_reg_ff_adffs_a8_a $ fir_in_reg_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a8_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a7_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a8_a,
	cout => abs_input_alcarry_a8_a_aCOUT);

fir_in_reg_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a9_a = DFFE(fir_in_a9_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a9_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a9_a);

fir_in_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(10),
	combout => fir_in_a10_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237 = abs_input_alcarry_a1_a & (abs_input_alcarry_a2_a # abs_input_alcarry_a3_a $ !abs_input_alcarry_a4_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a $ !abs_input_alcarry_a4_a # 
-- !abs_input_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F99F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a1_a,
	datab => abs_input_alcarry_a2_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279 = fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a & fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285 # !fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a & (fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a = abs_input_alcarry_a4_a & (abs_input_alcarry_a5_a # !abs_input_alcarry_a3_a) # !abs_input_alcarry_a4_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA5F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71 $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66 $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71 & !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71 & 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6917",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a282,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330 = fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271 & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286 & (!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a332);

Thr_Compare_acomparator_acmp_end_alcarry_a7_a_a1051_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a7_a = CARRY(rms_thr_reg_ff_adffs_a7_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a6_a # !abs_input_alcarry_a7_a) # !rms_thr_reg_ff_adffs_a7_a & !abs_input_alcarry_a7_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a7_a,
	datab => abs_input_alcarry_a7_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a7_a_a1051,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a7_a);

rms_thr_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a8_a = DFFE(RMS_Thr_a8_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a8_a);

RMS_Thr_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(9),
	combout => RMS_Thr_a9_a_acombout);

fir_in_reg_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a8_a = DFFE(fir_in_a8_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a8_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a8_a);

fir_in_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(9),
	combout => fir_in_a9_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a5_a $ abs_input_alcarry_a1_a) # !fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a & !abs_input_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4878",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a4_a);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87 $ fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87 & fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87 & (fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87,
	datab => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a285);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72 $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72 & (fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292) # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72 & fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "968E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a286,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a288);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334 = fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290 & fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a336);

Thr_Compare_acomparator_acmp_end_alcarry_a6_a_a1052_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a6_a = CARRY(abs_input_alcarry_a6_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a5_a # !rms_thr_reg_ff_adffs_a6_a) # !abs_input_alcarry_a6_a & !rms_thr_reg_ff_adffs_a6_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a6_a,
	datab => rms_thr_reg_ff_adffs_a6_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a6_a_a1052,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a6_a);

rms_thr_reg_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a7_a = DFFE(RMS_Thr_a7_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a7_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a7_a);

RMS_Thr_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(8),
	combout => RMS_Thr_a8_a_acombout);

fir_in_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(8),
	combout => fir_in_a8_a_acombout);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88 $ fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88 & 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293 # !fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a) # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88 & 
-- !fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a & !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88,
	datab => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a289);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_a183 = abs_input_alcarry_a5_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a3_a) # !abs_input_alcarry_a5_a & (!abs_input_alcarry_a3_a # !abs_input_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFF3",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_a183);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71 = (abs_input_alcarry_a4_a & (abs_input_alcarry_a3_a # abs_input_alcarry_a5_a) # !abs_input_alcarry_a4_a & (!abs_input_alcarry_a5_a # !abs_input_alcarry_a3_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_a183)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFF5",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a5_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a3_a_a183,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a3_a_a71);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73 $ fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73 & fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73 & (fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73,
	datab => fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a290,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a292);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294 & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279 & (!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a279,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a340);

Thr_Compare_acomparator_acmp_end_alcarry_a5_a_a1053_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a5_a = CARRY(rms_thr_reg_ff_adffs_a5_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a4_a # !abs_input_alcarry_a5_a) # !rms_thr_reg_ff_adffs_a5_a & !abs_input_alcarry_a5_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a5_a,
	datab => abs_input_alcarry_a5_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a5_a_a1053,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a5_a);

rms_thr_reg_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a6_a = DFFE(RMS_Thr_a6_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a6_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a6_a);

RMS_Thr_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(7),
	combout => RMS_Thr_a7_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_a185 = abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a2_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a2_a # !abs_input_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3CF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_a185);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87 = (abs_input_alcarry_a1_a & (abs_input_alcarry_a2_a # !abs_input_alcarry_a3_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a2_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_a185)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC3F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a1_a_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a1_a_a87);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291 = fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199 $ fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199 & (fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297) # !fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199 & fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199,
	datab => fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a293);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_a184 = abs_input_alcarry_a3_a & (abs_input_alcarry_a5_a # !abs_input_alcarry_a4_a # !abs_input_alcarry_a2_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a2_a # abs_input_alcarry_a4_a 
-- # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DFFB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a2_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_a184);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72 = (abs_input_alcarry_a3_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a5_a) # !abs_input_alcarry_a3_a & (!abs_input_alcarry_a5_a # !abs_input_alcarry_a4_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_a184)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFF3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a5_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a2_a_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a2_a_a72);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298 $ !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283 & 
-- (fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298 # !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348) # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283 & fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298 & 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a283,
	datab => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a344);

Thr_Compare_acomparator_acmp_end_alcarry_a4_a_a1054_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a4_a = CARRY(abs_input_alcarry_a4_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a3_a # !rms_thr_reg_ff_adffs_a4_a) # !abs_input_alcarry_a4_a & !rms_thr_reg_ff_adffs_a4_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => rms_thr_reg_ff_adffs_a4_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a4_a_a1054,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a4_a);

rms_thr_reg_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a5_a = DFFE(RMS_Thr_a5_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a5_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a5_a);

RMS_Thr_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(6),
	combout => RMS_Thr_a6_a_acombout);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_a186 = abs_input_alcarry_a3_a & (abs_input_alcarry_a1_a # fir_in_reg_ff_adffs_a0_a # abs_input_alcarry_a2_a) # !abs_input_alcarry_a3_a & (!abs_input_alcarry_a2_a # 
-- !fir_in_reg_ff_adffs_a0_a # !abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BFFD",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a1_a,
	datac => fir_in_reg_ff_adffs_a0_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_a186);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88 = (abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a # abs_input_alcarry_a2_a) # !abs_input_alcarry_a1_a & (!abs_input_alcarry_a2_a # !abs_input_alcarry_a3_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_a186)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFF3",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a0_a_a186,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a0_a_a88);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199 = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a $ abs_input_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0CC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aleft_bit_a1_a_a199);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301 $ (!fir_in_reg_ff_adffs_a0_a & abs_input_alcarry_a1_a)
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297 = CARRY(fir_in_reg_ff_adffs_a0_a # !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301 # !abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "B4BF",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a0_a,
	datab => abs_input_alcarry_a1_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a297);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_a185 = abs_input_alcarry_a4_a & (abs_input_alcarry_a5_a # !abs_input_alcarry_a3_a # !abs_input_alcarry_a1_a) # !abs_input_alcarry_a4_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a3_a 
-- # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DFFB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_a185);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73 = (abs_input_alcarry_a4_a & (abs_input_alcarry_a3_a # abs_input_alcarry_a2_a $ !abs_input_alcarry_a5_a) # !abs_input_alcarry_a4_a & (abs_input_alcarry_a2_a $ !abs_input_alcarry_a5_a # 
-- !abs_input_alcarry_a3_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_a185)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F99F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a2_a,
	datad => abs_input_alcarry_a5_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a1_a_a185,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a1_a_a73);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287 $ 
-- (fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352)
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348 = CARRY(!fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a287,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a348);

Thr_Compare_acomparator_acmp_end_alcarry_a3_a_a1055_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a3_a = CARRY(abs_input_alcarry_a3_a & rms_thr_reg_ff_adffs_a3_a & !Thr_Compare_acomparator_acmp_end_alcarry_a2_a # !abs_input_alcarry_a3_a & (rms_thr_reg_ff_adffs_a3_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => rms_thr_reg_ff_adffs_a3_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a3_a_a1055,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a3_a);

rms_thr_reg_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a4_a = DFFE(RMS_Thr_a4_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a4_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a4_a);

RMS_Thr_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(5),
	combout => RMS_Thr_a5_a_acombout);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299 = fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1 $ abs_input_alcarry_a1_a
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301 = CARRY(fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1 & abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1,
	datab => abs_input_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a301);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_a186 = abs_input_alcarry_a4_a & (abs_input_alcarry_a5_a # !abs_input_alcarry_a3_a # !fir_in_reg_ff_adffs_a0_a) # !abs_input_alcarry_a4_a & (fir_in_reg_ff_adffs_a0_a # 
-- abs_input_alcarry_a3_a # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DFFB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a5_a,
	datac => fir_in_reg_ff_adffs_a0_a,
	datad => abs_input_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_a186);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74 = (abs_input_alcarry_a4_a & (abs_input_alcarry_a3_a # abs_input_alcarry_a1_a $ !abs_input_alcarry_a5_a) # !abs_input_alcarry_a4_a & (abs_input_alcarry_a1_a $ !abs_input_alcarry_a5_a # 
-- !abs_input_alcarry_a3_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_a186)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F99F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a5_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a0_a_a186,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74);

fir_in_square_mult_amult_core_app_carry_node_a2_a_a48_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_app_carry_node_a2_a_a48 = abs_input_alcarry_a5_a & (!abs_input_alcarry_a4_a # !abs_input_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_app_carry_node_a2_a_a48);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291 $ fir_in_square_mult_amult_core_app_carry_node_a1_a_a49
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291 & 
-- fir_in_square_mult_amult_core_app_carry_node_a1_a_a49)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a291,
	datab => fir_in_square_mult_amult_core_app_carry_node_a1_a_a49,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350,
	cout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a352);

Thr_Compare_acomparator_acmp_end_alcarry_a2_a_a1056_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a2_a = CARRY(abs_input_alcarry_a2_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a1_a # !rms_thr_reg_ff_adffs_a2_a) # !abs_input_alcarry_a2_a & !rms_thr_reg_ff_adffs_a2_a & 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "002B",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a2_a,
	datab => rms_thr_reg_ff_adffs_a2_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a2_a_a1056,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a2_a);

rms_thr_reg_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a3_a = DFFE(RMS_Thr_a3_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a3_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a3_a);

RMS_Thr_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(4),
	combout => RMS_Thr_a4_a_acombout);

fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1 = fir_in_reg_ff_adffs_a0_a $ abs_input_alcarry_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => fir_in_reg_ff_adffs_a0_a,
	datad => abs_input_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00031_aout_bit_a0_a_a1);

Thr_Compare_acomparator_acmp_end_alcarry_a1_a_a1057_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a1_a = CARRY(abs_input_alcarry_a1_a & rms_thr_reg_ff_adffs_a1_a & !Thr_Compare_acomparator_acmp_end_alcarry_a0_a # !abs_input_alcarry_a1_a & (rms_thr_reg_ff_adffs_a1_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "004D",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a1_a,
	datab => rms_thr_reg_ff_adffs_a1_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a1_a_a1057,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a1_a);

rms_thr_reg_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a2_a = DFFE(RMS_Thr_a2_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a2_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a2_a);

RMS_Thr_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(3),
	combout => RMS_Thr_a3_a_acombout);

Thr_Compare_acomparator_acmp_end_alcarry_a0_a_a1058_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a0_a = CARRY(fir_in_reg_ff_adffs_a0_a & !rms_thr_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a0_a,
	datab => rms_thr_reg_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a0_a_a1058,
	cout => Thr_Compare_acomparator_acmp_end_alcarry_a0_a);

rms_thr_reg_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a1_a = DFFE(RMS_Thr_a1_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a1_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a1_a);

RMS_Thr_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(2),
	combout => RMS_Thr_a2_a_acombout);

rms_thr_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_thr_reg_ff_adffs_a0_a = DFFE(RMS_Thr_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => RMS_Thr_a0_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_thr_reg_ff_adffs_a0_a);

RMS_Thr_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(1),
	combout => RMS_Thr_a1_a_acombout);

RMS_Thr_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RMS_Thr(0),
	combout => RMS_Thr_a0_a_acombout);

Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088_I : apex20ke_lcell
-- Equation(s):
-- Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088 = rms_thr_reg_ff_adffs_a14_a & (!Thr_Compare_acomparator_acmp_end_alcarry_a13_a & abs_input_alcarry_a14_a) # !rms_thr_reg_ff_adffs_a14_a & (abs_input_alcarry_a14_a # 
-- !Thr_Compare_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5F05",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_thr_reg_ff_adffs_a14_a,
	datad => abs_input_alcarry_a14_a,
	cin => Thr_Compare_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Thr_Compare_acomparator_acmp_end_alcarry_a14_a_a1088);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70 = (abs_input_alcarry_a3_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a7_a $ !abs_input_alcarry_a5_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a7_a $ !abs_input_alcarry_a5_a # 
-- !abs_input_alcarry_a4_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_a182)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EBD7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a5_a,
	datad => abs_input_alcarry_a4_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a6_a_a182,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

PA_Reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_PA_Reset,
	combout => PA_Reset_acombout);

OTR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_OTR,
	combout => OTR_acombout);

RMS_En_a15_I : apex20ke_lcell
-- Equation(s):
-- RMS_En_a15 = !Thr_Compare_acomparator_acmp_end_aagb_out & (!PA_Reset_acombout & !OTR_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0005",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Thr_Compare_acomparator_acmp_end_aagb_out,
	datac => PA_Reset_acombout,
	datad => OTR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => RMS_En_a15);

rms_cnt_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a0_a = DFFE(!GLOBAL(Equal_a212) & !rms_cnt_cnt_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a0_a_a1164 = CARRY(rms_cnt_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a0_a,
	cout => rms_cnt_cnt_a0_a_a1164);

rms_cnt_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a2_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a2_a $ !rms_cnt_cnt_a1_a_a1161, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a2_a_a1158 = CARRY(rms_cnt_cnt_a2_a & !rms_cnt_cnt_a1_a_a1161)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a2_a,
	cin => rms_cnt_cnt_a1_a_a1161,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a2_a,
	cout => rms_cnt_cnt_a2_a_a1158);

rms_cnt_cnt_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a5_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a5_a $ rms_cnt_cnt_a4_a_a1176, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a5_a_a1173 = CARRY(!rms_cnt_cnt_a4_a_a1176 # !rms_cnt_cnt_a5_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a5_a,
	cin => rms_cnt_cnt_a4_a_a1176,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a5_a,
	cout => rms_cnt_cnt_a5_a_a1173);

rms_cnt_cnt_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a7_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a7_a $ rms_cnt_cnt_a6_a_a1170, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a7_a_a1167 = CARRY(!rms_cnt_cnt_a6_a_a1170 # !rms_cnt_cnt_a7_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a7_a,
	cin => rms_cnt_cnt_a6_a_a1170,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a7_a,
	cout => rms_cnt_cnt_a7_a_a1167);

rms_cnt_cnt_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a9_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a9_a $ rms_cnt_cnt_a8_a_a1188, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a9_a_a1185 = CARRY(!rms_cnt_cnt_a8_a_a1188 # !rms_cnt_cnt_a9_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a9_a,
	cin => rms_cnt_cnt_a8_a_a1188,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a9_a,
	cout => rms_cnt_cnt_a9_a_a1185);

rms_cnt_cnt_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a10_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a10_a $ !rms_cnt_cnt_a9_a_a1185, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a10_a_a1182 = CARRY(rms_cnt_cnt_a10_a & !rms_cnt_cnt_a9_a_a1185)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a10_a,
	cin => rms_cnt_cnt_a9_a_a1185,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a10_a,
	cout => rms_cnt_cnt_a10_a_a1182);

rms_cnt_cnt_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a11_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a11_a $ rms_cnt_cnt_a10_a_a1182, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a11_a_a1179 = CARRY(!rms_cnt_cnt_a10_a_a1182 # !rms_cnt_cnt_a11_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a11_a,
	cin => rms_cnt_cnt_a10_a_a1182,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a11_a,
	cout => rms_cnt_cnt_a11_a_a1179);

rms_cnt_cnt_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a12_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a12_a $ !rms_cnt_cnt_a11_a_a1179, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a12_a_a1200 = CARRY(rms_cnt_cnt_a12_a & !rms_cnt_cnt_a11_a_a1179)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a12_a,
	cin => rms_cnt_cnt_a11_a_a1179,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a12_a,
	cout => rms_cnt_cnt_a12_a_a1200);

rms_cnt_cnt_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a14_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a14_a $ !rms_cnt_cnt_a13_a_a1197, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a14_a_a1194 = CARRY(rms_cnt_cnt_a14_a & !rms_cnt_cnt_a13_a_a1197)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a14_a,
	cin => rms_cnt_cnt_a13_a_a1197,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a14_a,
	cout => rms_cnt_cnt_a14_a_a1194);

rms_cnt_cnt_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a15_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a15_a $ rms_cnt_cnt_a14_a_a1194, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a15_a_a1191 = CARRY(!rms_cnt_cnt_a14_a_a1194 # !rms_cnt_cnt_a15_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a15_a,
	cin => rms_cnt_cnt_a14_a_a1194,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a15_a,
	cout => rms_cnt_cnt_a15_a_a1191);

rms_cnt_cnt_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a16_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a16_a $ !rms_cnt_cnt_a15_a_a1191, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a16_a_a1212 = CARRY(rms_cnt_cnt_a16_a & !rms_cnt_cnt_a15_a_a1191)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a16_a,
	cin => rms_cnt_cnt_a15_a_a1191,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a16_a,
	cout => rms_cnt_cnt_a16_a_a1212);

rms_cnt_cnt_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a18_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a18_a $ !rms_cnt_cnt_a17_a_a1209, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a18_a_a1206 = CARRY(rms_cnt_cnt_a18_a & !rms_cnt_cnt_a17_a_a1209)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => rms_cnt_cnt_a18_a,
	cin => rms_cnt_cnt_a17_a_a1209,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a18_a,
	cout => rms_cnt_cnt_a18_a_a1206);

rms_cnt_cnt_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a19_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a19_a $ (rms_cnt_cnt_a18_a_a1206), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- rms_cnt_cnt_a19_a_a1203 = CARRY(!rms_cnt_cnt_a18_a_a1206 # !rms_cnt_cnt_a19_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a19_a,
	cin => rms_cnt_cnt_a18_a_a1206,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a19_a,
	cout => rms_cnt_cnt_a19_a_a1203);

Equal_a211_I : apex20ke_lcell
-- Equation(s):
-- Equal_a211 = rms_cnt_cnt_a17_a & rms_cnt_cnt_a18_a & rms_cnt_cnt_a19_a & rms_cnt_cnt_a16_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => rms_cnt_cnt_a17_a,
	datab => rms_cnt_cnt_a18_a,
	datac => rms_cnt_cnt_a19_a,
	datad => rms_cnt_cnt_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a211);

rms_cnt_cnt_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_cnt_cnt_a20_a = DFFE(!GLOBAL(Equal_a212) & rms_cnt_cnt_a19_a_a1203 $ !rms_cnt_cnt_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => rms_cnt_cnt_a20_a,
	cin => rms_cnt_cnt_a19_a_a1203,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => Equal_a212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_cnt_cnt_a20_a);

Equal_a212_I : apex20ke_lcell
-- Equation(s):
-- Equal_a212 = Equal_a210 & (Equal_a211 & !rms_cnt_cnt_a20_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Equal_a210,
	datac => Equal_a211,
	datad => rms_cnt_cnt_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Equal_a212);

rms_upd_a0_I : apex20ke_lcell
-- Equation(s):
-- rms_upd_a0 = !Thr_Compare_acomparator_acmp_end_aagb_out & Equal_a212 & !PA_Reset_acombout & !OTR_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0004",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => Thr_Compare_acomparator_acmp_end_aagb_out,
	datab => Equal_a212,
	datac => PA_Reset_acombout,
	datad => OTR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rms_upd_a0);

acc_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a0_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299 $ acc_ff_adffs_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- acc_ff_adffs_a0_a_a299 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299 & acc_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a299,
	datab => acc_ff_adffs_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a0_a,
	cout => acc_ff_adffs_a0_a_a299);

acc_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a1_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295 $ acc_ff_adffs_a1_a $ acc_ff_adffs_a0_a_a299, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , RMS_En_a15)
-- acc_ff_adffs_a1_a_a296 = CARRY(fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295 & !acc_ff_adffs_a1_a & !acc_ff_adffs_a0_a_a299 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295 & (!acc_ff_adffs_a0_a_a299 # !acc_ff_adffs_a1_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a295,
	datab => acc_ff_adffs_a1_a,
	cin => acc_ff_adffs_a0_a_a299,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a1_a,
	cout => acc_ff_adffs_a1_a_a296);

acc_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a2_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350 $ acc_ff_adffs_a2_a $ !acc_ff_adffs_a1_a_a296, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a2_a_a293 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350 & (acc_ff_adffs_a2_a # !acc_ff_adffs_a1_a_a296) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350 & acc_ff_adffs_a2_a & !acc_ff_adffs_a1_a_a296)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a350,
	datab => acc_ff_adffs_a2_a,
	cin => acc_ff_adffs_a1_a_a296,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a2_a,
	cout => acc_ff_adffs_a2_a_a293);

acc_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a3_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346 $ acc_ff_adffs_a3_a $ acc_ff_adffs_a2_a_a293, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a3_a_a290 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346 & !acc_ff_adffs_a3_a & !acc_ff_adffs_a2_a_a293 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346 & (!acc_ff_adffs_a2_a_a293 # !acc_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a346,
	datab => acc_ff_adffs_a3_a,
	cin => acc_ff_adffs_a2_a_a293,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a3_a,
	cout => acc_ff_adffs_a3_a_a290);

acc_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a4_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342 $ acc_ff_adffs_a4_a $ !acc_ff_adffs_a3_a_a290, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a4_a_a287 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342 & (acc_ff_adffs_a4_a # !acc_ff_adffs_a3_a_a290) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342 & acc_ff_adffs_a4_a & !acc_ff_adffs_a3_a_a290)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a342,
	datab => acc_ff_adffs_a4_a,
	cin => acc_ff_adffs_a3_a_a290,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a4_a,
	cout => acc_ff_adffs_a4_a_a287);

acc_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a5_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338 $ acc_ff_adffs_a5_a $ acc_ff_adffs_a4_a_a287, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a5_a_a284 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338 & !acc_ff_adffs_a5_a & !acc_ff_adffs_a4_a_a287 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338 & (!acc_ff_adffs_a4_a_a287 # !acc_ff_adffs_a5_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a338,
	datab => acc_ff_adffs_a5_a,
	cin => acc_ff_adffs_a4_a_a287,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a5_a,
	cout => acc_ff_adffs_a5_a_a284);

acc_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a6_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334 $ acc_ff_adffs_a6_a $ !acc_ff_adffs_a5_a_a284, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a6_a_a281 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334 & (acc_ff_adffs_a6_a # !acc_ff_adffs_a5_a_a284) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334 & acc_ff_adffs_a6_a & !acc_ff_adffs_a5_a_a284)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a334,
	datab => acc_ff_adffs_a6_a,
	cin => acc_ff_adffs_a5_a_a284,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a6_a,
	cout => acc_ff_adffs_a6_a_a281);

acc_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a7_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330 $ acc_ff_adffs_a7_a $ acc_ff_adffs_a6_a_a281, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a7_a_a278 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330 & !acc_ff_adffs_a7_a & !acc_ff_adffs_a6_a_a281 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330 & (!acc_ff_adffs_a6_a_a281 # !acc_ff_adffs_a7_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a330,
	datab => acc_ff_adffs_a7_a,
	cin => acc_ff_adffs_a6_a_a281,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a7_a,
	cout => acc_ff_adffs_a7_a_a278);

acc_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a8_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326 $ acc_ff_adffs_a8_a $ !acc_ff_adffs_a7_a_a278, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a8_a_a275 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326 & (acc_ff_adffs_a8_a # !acc_ff_adffs_a7_a_a278) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326 & acc_ff_adffs_a8_a & !acc_ff_adffs_a7_a_a278)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a326,
	datab => acc_ff_adffs_a8_a,
	cin => acc_ff_adffs_a7_a_a278,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a8_a,
	cout => acc_ff_adffs_a8_a_a275);

acc_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a9_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322 $ acc_ff_adffs_a9_a $ acc_ff_adffs_a8_a_a275, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a9_a_a272 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322 & !acc_ff_adffs_a9_a & !acc_ff_adffs_a8_a_a275 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322 & (!acc_ff_adffs_a8_a_a275 # !acc_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a322,
	datab => acc_ff_adffs_a9_a,
	cin => acc_ff_adffs_a8_a_a275,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a9_a,
	cout => acc_ff_adffs_a9_a_a272);

acc_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a10_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318 $ acc_ff_adffs_a10_a $ !acc_ff_adffs_a9_a_a272, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a10_a_a269 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318 & (acc_ff_adffs_a10_a # !acc_ff_adffs_a9_a_a272) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318 & acc_ff_adffs_a10_a & !acc_ff_adffs_a9_a_a272)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a318,
	datab => acc_ff_adffs_a10_a,
	cin => acc_ff_adffs_a9_a_a272,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a10_a,
	cout => acc_ff_adffs_a10_a_a269);

acc_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a11_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314 $ acc_ff_adffs_a11_a $ acc_ff_adffs_a10_a_a269, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a11_a_a266 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314 & !acc_ff_adffs_a11_a & !acc_ff_adffs_a10_a_a269 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314 & (!acc_ff_adffs_a10_a_a269 # !acc_ff_adffs_a11_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a314,
	datab => acc_ff_adffs_a11_a,
	cin => acc_ff_adffs_a10_a_a269,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a11_a,
	cout => acc_ff_adffs_a11_a_a266);

acc_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a12_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310 $ acc_ff_adffs_a12_a $ !acc_ff_adffs_a11_a_a266, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a12_a_a263 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310 & (acc_ff_adffs_a12_a # !acc_ff_adffs_a11_a_a266) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310 & acc_ff_adffs_a12_a & !acc_ff_adffs_a11_a_a266)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a310,
	datab => acc_ff_adffs_a12_a,
	cin => acc_ff_adffs_a11_a_a266,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a12_a,
	cout => acc_ff_adffs_a12_a_a263);

acc_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a13_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306 $ acc_ff_adffs_a13_a $ acc_ff_adffs_a12_a_a263, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a13_a_a260 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306 & !acc_ff_adffs_a13_a & !acc_ff_adffs_a12_a_a263 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306 & (!acc_ff_adffs_a12_a_a263 # !acc_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a306,
	datab => acc_ff_adffs_a13_a,
	cin => acc_ff_adffs_a12_a_a263,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a13_a,
	cout => acc_ff_adffs_a13_a_a260);

acc_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a14_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298 $ acc_ff_adffs_a14_a $ !acc_ff_adffs_a13_a_a260, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a14_a_a212 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298 & (acc_ff_adffs_a14_a # !acc_ff_adffs_a13_a_a260) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298 & acc_ff_adffs_a14_a & !acc_ff_adffs_a13_a_a260)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a298,
	datab => acc_ff_adffs_a14_a,
	cin => acc_ff_adffs_a13_a_a260,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a14_a,
	cout => acc_ff_adffs_a14_a_a212);

rms_out_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a0_a = DFFE(acc_ff_adffs_a14_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a14_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a0_a);

acc_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a15_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a15_a $ acc_ff_adffs_a14_a_a212, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a15_a_a215 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a15_a & !acc_ff_adffs_a14_a_a212 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a14_a_a212 # !acc_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a15_a,
	cin => acc_ff_adffs_a14_a_a212,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a15_a,
	cout => acc_ff_adffs_a15_a_a215);

rms_out_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a1_a = DFFE(acc_ff_adffs_a15_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => acc_ff_adffs_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a1_a);

acc_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a16_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a16_a $ !acc_ff_adffs_a15_a_a215, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a16_a_a218 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (acc_ff_adffs_a16_a # !acc_ff_adffs_a15_a_a215) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & acc_ff_adffs_a16_a & !acc_ff_adffs_a15_a_a215)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a16_a,
	cin => acc_ff_adffs_a15_a_a215,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a16_a,
	cout => acc_ff_adffs_a16_a_a218);

rms_out_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a2_a = DFFE(acc_ff_adffs_a16_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => acc_ff_adffs_a16_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a2_a);

acc_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a17_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a17_a $ acc_ff_adffs_a16_a_a218, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a17_a_a221 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a17_a & !acc_ff_adffs_a16_a_a218 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a16_a_a218 # !acc_ff_adffs_a17_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a17_a,
	cin => acc_ff_adffs_a16_a_a218,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a17_a,
	cout => acc_ff_adffs_a17_a_a221);

rms_out_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a3_a = DFFE(acc_ff_adffs_a17_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => acc_ff_adffs_a17_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a3_a);

acc_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a18_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a18_a $ !acc_ff_adffs_a17_a_a221, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a18_a_a224 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (acc_ff_adffs_a18_a # !acc_ff_adffs_a17_a_a221) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & acc_ff_adffs_a18_a & !acc_ff_adffs_a17_a_a221)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a18_a,
	cin => acc_ff_adffs_a17_a_a221,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a18_a,
	cout => acc_ff_adffs_a18_a_a224);

rms_out_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a4_a = DFFE(acc_ff_adffs_a18_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => acc_ff_adffs_a18_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a4_a);

acc_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a19_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a19_a $ acc_ff_adffs_a18_a_a224, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a19_a_a227 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a19_a & !acc_ff_adffs_a18_a_a224 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a18_a_a224 # !acc_ff_adffs_a19_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a19_a,
	cin => acc_ff_adffs_a18_a_a224,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a19_a,
	cout => acc_ff_adffs_a19_a_a227);

rms_out_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a5_a = DFFE(acc_ff_adffs_a19_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => acc_ff_adffs_a19_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a5_a);

acc_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a20_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a20_a $ !acc_ff_adffs_a19_a_a227, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a20_a_a230 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (acc_ff_adffs_a20_a # !acc_ff_adffs_a19_a_a227) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & acc_ff_adffs_a20_a & !acc_ff_adffs_a19_a_a227)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a20_a,
	cin => acc_ff_adffs_a19_a_a227,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a20_a,
	cout => acc_ff_adffs_a20_a_a230);

rms_out_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a6_a = DFFE(acc_ff_adffs_a20_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a20_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a6_a);

acc_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a21_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a21_a $ acc_ff_adffs_a20_a_a230, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a21_a_a233 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a21_a & !acc_ff_adffs_a20_a_a230 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a20_a_a230 # !acc_ff_adffs_a21_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a21_a,
	cin => acc_ff_adffs_a20_a_a230,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a21_a,
	cout => acc_ff_adffs_a21_a_a233);

rms_out_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a7_a = DFFE(acc_ff_adffs_a21_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a21_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a7_a);

acc_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a22_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a22_a $ !acc_ff_adffs_a21_a_a233, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a22_a_a236 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (acc_ff_adffs_a22_a # !acc_ff_adffs_a21_a_a233) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & acc_ff_adffs_a22_a & !acc_ff_adffs_a21_a_a233)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a22_a,
	cin => acc_ff_adffs_a21_a_a233,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a22_a,
	cout => acc_ff_adffs_a22_a_a236);

rms_out_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a8_a = DFFE(acc_ff_adffs_a22_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a22_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a8_a);

fir_in_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_fir_in(15),
	combout => fir_in_a15_a_acombout);

fir_in_reg_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a15_a = DFFE(fir_in_a15_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => fir_in_a15_a_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a15_a);

fir_in_reg_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_reg_ff_adffs_a0_a = DFFE(fir_in_a0_a_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- abs_input_alcarry_a0_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a15_a & !fir_in_reg_ff_adffs_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA0C",
	operation_mode => "qfbk_counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_a0_a_acombout,
	datab => fir_in_reg_ff_adffs_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => fir_in_reg_ff_adffs_a0_a,
	cout => abs_input_alcarry_a0_a_aCOUT);

abs_input_alcarry_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a1_a = fir_in_reg_ff_adffs_a1_a $ fir_in_reg_ff_adffs_a15_a $ abs_input_alcarry_a0_a_aCOUT
-- abs_input_alcarry_a1_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a1_a $ !fir_in_reg_ff_adffs_a15_a # !abs_input_alcarry_a0_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a1_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a0_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a1_a,
	cout => abs_input_alcarry_a1_a_aCOUT);

abs_input_alcarry_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a2_a = fir_in_reg_ff_adffs_a2_a $ fir_in_reg_ff_adffs_a15_a $ !abs_input_alcarry_a1_a_aCOUT
-- abs_input_alcarry_a2_a_aCOUT = CARRY(!abs_input_alcarry_a1_a_aCOUT & (fir_in_reg_ff_adffs_a2_a $ fir_in_reg_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a2_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a1_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a2_a,
	cout => abs_input_alcarry_a2_a_aCOUT);

abs_input_alcarry_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a3_a = fir_in_reg_ff_adffs_a3_a $ fir_in_reg_ff_adffs_a15_a $ abs_input_alcarry_a2_a_aCOUT
-- abs_input_alcarry_a3_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a3_a $ !fir_in_reg_ff_adffs_a15_a # !abs_input_alcarry_a2_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a3_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a2_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a3_a,
	cout => abs_input_alcarry_a3_a_aCOUT);

abs_input_alcarry_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a4_a = fir_in_reg_ff_adffs_a4_a $ fir_in_reg_ff_adffs_a15_a $ !abs_input_alcarry_a3_a_aCOUT
-- abs_input_alcarry_a4_a_aCOUT = CARRY(!abs_input_alcarry_a3_a_aCOUT & (fir_in_reg_ff_adffs_a4_a $ fir_in_reg_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a4_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a3_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a4_a,
	cout => abs_input_alcarry_a4_a_aCOUT);

abs_input_alcarry_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a5_a = fir_in_reg_ff_adffs_a5_a $ fir_in_reg_ff_adffs_a15_a $ abs_input_alcarry_a4_a_aCOUT
-- abs_input_alcarry_a5_a_aCOUT = CARRY(fir_in_reg_ff_adffs_a5_a $ !fir_in_reg_ff_adffs_a15_a # !abs_input_alcarry_a4_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a5_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a4_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a5_a,
	cout => abs_input_alcarry_a5_a_aCOUT);

abs_input_alcarry_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- abs_input_alcarry_a6_a = fir_in_reg_ff_adffs_a6_a $ fir_in_reg_ff_adffs_a15_a $ !abs_input_alcarry_a5_a_aCOUT
-- abs_input_alcarry_a6_a_aCOUT = CARRY(!abs_input_alcarry_a5_a_aCOUT & (fir_in_reg_ff_adffs_a6_a $ fir_in_reg_ff_adffs_a15_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a6_a,
	datab => fir_in_reg_ff_adffs_a15_a,
	cin => abs_input_alcarry_a5_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_input_alcarry_a6_a,
	cout => abs_input_alcarry_a6_a_aCOUT);

fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24 = abs_input_alcarry_a4_a & abs_input_alcarry_a3_a & abs_input_alcarry_a7_a & !abs_input_alcarry_a5_a # !abs_input_alcarry_a4_a & !abs_input_alcarry_a3_a & !abs_input_alcarry_a7_a & 
-- abs_input_alcarry_a5_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0180",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24);

fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a = abs_input_alcarry_a7_a & !abs_input_alcarry_a5_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a3_a) # !abs_input_alcarry_a7_a & abs_input_alcarry_a5_a & (!abs_input_alcarry_a3_a # 
-- !abs_input_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "07E0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a3_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_a219 = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a # !abs_input_alcarry_a4_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a4_a # abs_input_alcarry_a6_a 
-- # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DFFB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_a219);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63 = (abs_input_alcarry_a7_a & (abs_input_alcarry_a5_a # !abs_input_alcarry_a6_a) # !abs_input_alcarry_a7_a & (abs_input_alcarry_a6_a # !abs_input_alcarry_a5_a)) & 
-- CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_a219)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3CF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a5_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a4_a_a219,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_a220 = abs_input_alcarry_a7_a & (abs_input_alcarry_a5_a # abs_input_alcarry_a3_a # abs_input_alcarry_a6_a) # !abs_input_alcarry_a7_a & (!abs_input_alcarry_a6_a # !abs_input_alcarry_a3_a 
-- # !abs_input_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BFFD",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a7_a,
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_a220);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64 = (abs_input_alcarry_a4_a & (abs_input_alcarry_a7_a # abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a) # !abs_input_alcarry_a4_a & (abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a # 
-- !abs_input_alcarry_a7_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_a220)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EDB7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a4_a,
	datab => abs_input_alcarry_a5_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a3_a_a220,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_a221 = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a # !abs_input_alcarry_a2_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a2_a # abs_input_alcarry_a6_a 
-- # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F7EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a2_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_a221);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65 = (abs_input_alcarry_a3_a & (abs_input_alcarry_a7_a # abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a # 
-- !abs_input_alcarry_a7_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_a221)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F99F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a5_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a2_a_a221,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_a222 = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a # !abs_input_alcarry_a1_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a6_a 
-- # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "DFFB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_a222);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66 = (abs_input_alcarry_a5_a & (abs_input_alcarry_a6_a # abs_input_alcarry_a7_a $ !abs_input_alcarry_a2_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a $ !abs_input_alcarry_a2_a # 
-- !abs_input_alcarry_a6_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_a222)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EBD7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a2_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a1_a_a222,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a1_a_a66);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_a223 = abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a # !abs_input_alcarry_a6_a # !fir_in_reg_ff_adffs_a0_a) # !abs_input_alcarry_a5_a & (fir_in_reg_ff_adffs_a0_a # 
-- abs_input_alcarry_a6_a # !abs_input_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F7EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_a223);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67 = (abs_input_alcarry_a1_a & (abs_input_alcarry_a7_a # abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a5_a $ !abs_input_alcarry_a6_a # 
-- !abs_input_alcarry_a7_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_a223)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F99F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a1_a,
	datab => abs_input_alcarry_a7_a,
	datac => abs_input_alcarry_a5_a,
	datad => abs_input_alcarry_a6_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a0_a_a223,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a0_a_a67);

fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a = abs_input_alcarry_a5_a & !abs_input_alcarry_a6_a & (fir_in_reg_ff_adffs_a0_a $ abs_input_alcarry_a7_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a7_a $ (fir_in_reg_ff_adffs_a0_a & 
-- abs_input_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1478",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a7_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00043_aout_bit_a0_a);

fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a = abs_input_alcarry_a3_a & !abs_input_alcarry_a4_a & (abs_input_alcarry_a5_a $ fir_in_reg_ff_adffs_a0_a) # !abs_input_alcarry_a3_a & (abs_input_alcarry_a5_a $ (fir_in_reg_ff_adffs_a0_a & 
-- abs_input_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "066A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298 = fir_in_square_mult_amult_core_app_carry_node_a2_a_a48 $ fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300 = CARRY(fir_in_square_mult_amult_core_app_carry_node_a2_a_a48 & fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_app_carry_node_a2_a_a48,
	datab => fir_in_square_mult_amult_core_a_a00039_aout_bit_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a298,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74 $ 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300)
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A5AF",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a0_a_a74,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a300,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a294,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a296);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a & (fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284) # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a & fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "968E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aright_bit_a4_a,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a2_a_a65,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a284,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a278,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218 $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64 $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218 & !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218 & 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6917",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aleft_bit_a5_a_a218,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a3_a_a64,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a280,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a274,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70 $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70 & (fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276) # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70 & fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "968E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a2_a_aout_bit_a6_a_a70,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a4_a_a63,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a276,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a270,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62 $ fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62 & fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62 & (fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aout_bit_a5_a_a62,
	datab => fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a272,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a266,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a $ fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a & 
-- (!fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268 # !fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a) # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a & 
-- !fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a & !fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a3_a_aright_bit_a6_a,
	datab => fir_in_square_mult_amult_core_a_a00041_aout_bit_a0_a,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a268,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a262,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254 = fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264 $ (!fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58 & 
-- !fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24)
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a256 = CARRY(!fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264 & (fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58 # 
-- fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "E10E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58,
	datab => fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a264,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a254,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a256);

fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258 = fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a256 $ (fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58 # 
-- fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F5A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_a_a00041_aleft_bit_a0_a_a58,
	datad => fir_in_square_mult_amult_core_a_a00041_aright_bit_a0_a_a24,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a256,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a $ abs_input_alcarry_a6_a) # !fir_in_reg_ff_adffs_a0_a & !abs_input_alcarry_a5_a & abs_input_alcarry_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "34C4",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a1_a,
	datac => fir_in_reg_ff_adffs_a0_a,
	datad => abs_input_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a);

fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a = abs_input_alcarry_a1_a & (!abs_input_alcarry_a7_a) # !abs_input_alcarry_a1_a & fir_in_reg_ff_adffs_a0_a & abs_input_alcarry_a7_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "30CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a1_a,
	datac => fir_in_reg_ff_adffs_a0_a,
	datad => abs_input_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a $ abs_input_alcarry_a7_a) # !fir_in_reg_ff_adffs_a0_a & !abs_input_alcarry_a6_a & abs_input_alcarry_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1CD0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a6_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a);

fir_in_square_mult_amult_core_apadder_a_a5_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_a_a5 = fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a $ (fir_in_square_mult_amult_core_app_carry_node_a3_a_a47 & fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a & 
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "78F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_app_carry_node_a3_a_a47,
	datab => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a,
	datac => fir_in_square_mult_amult_core_a_a00033_aout_bit_a0_a,
	datad => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_a_a5);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_a184 = abs_input_alcarry_a3_a & (abs_input_alcarry_a1_a # abs_input_alcarry_a4_a # abs_input_alcarry_a2_a) # !abs_input_alcarry_a3_a & (!abs_input_alcarry_a2_a # !abs_input_alcarry_a4_a 
-- # !abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "BFFD",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a4_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a,
	cascout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_a184);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86 = (abs_input_alcarry_a5_a & (abs_input_alcarry_a3_a # abs_input_alcarry_a1_a $ !abs_input_alcarry_a2_a) # !abs_input_alcarry_a5_a & (abs_input_alcarry_a1_a $ !abs_input_alcarry_a2_a # 
-- !abs_input_alcarry_a3_a)) & CASCADE(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_a184)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EDB7",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	cascin => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a4_a_a184,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86);

fir_in_square_mult_amult_core_apadder_a_a0_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_a_a0 = fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a $ (abs_input_alcarry_a7_a & (!abs_input_alcarry_a6_a # !abs_input_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "8F70",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a5_a,
	datab => abs_input_alcarry_a6_a,
	datac => abs_input_alcarry_a7_a,
	datad => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_a_a0);

fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a = abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a # !abs_input_alcarry_a2_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a2_a # !abs_input_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3CF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aright_bit_a2_a);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a $ abs_input_alcarry_a4_a) # !fir_in_reg_ff_adffs_a0_a & !abs_input_alcarry_a3_a & abs_input_alcarry_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1CD0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a3_a);

fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a = fir_in_reg_ff_adffs_a0_a & (abs_input_alcarry_a1_a $ abs_input_alcarry_a3_a) # !fir_in_reg_ff_adffs_a0_a & abs_input_alcarry_a1_a & (!abs_input_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "286C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_reg_ff_adffs_a0_a,
	datab => abs_input_alcarry_a1_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_amul_lfrg_first_mod_aout_bit_a2_a);

fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a = abs_input_alcarry_a1_a & !abs_input_alcarry_a2_a & (fir_in_reg_ff_adffs_a0_a $ abs_input_alcarry_a3_a) # !abs_input_alcarry_a1_a & (abs_input_alcarry_a3_a $ (fir_in_reg_ff_adffs_a0_a & 
-- abs_input_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1478",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a1_a,
	datab => fir_in_reg_ff_adffs_a0_a,
	datac => abs_input_alcarry_a3_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_a_a00035_aout_bit_a0_a);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275 = fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237 $ fir_in_square_mult_amult_core_apadder_a_a0 $ 
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277 = CARRY(fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237 & fir_in_square_mult_amult_core_apadder_a_a0 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281 # !fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237 & (fir_in_square_mult_amult_core_apadder_a_a0 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aleft_bit_a3_a_a237,
	datab => fir_in_square_mult_amult_core_apadder_a_a0,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a275,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277);

fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271 = fir_in_square_mult_amult_core_apadder_a_a2 $ fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86 $ 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277
-- fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273 = CARRY(fir_in_square_mult_amult_core_apadder_a_a2 & fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86 & 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277 # !fir_in_square_mult_amult_core_apadder_a_a2 & (fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86 # 
-- !fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_a_a2,
	datab => fir_in_square_mult_amult_core_amul_lfrg_mid_mod_a1_a_aout_bit_a4_a_a86,
	cin => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a277,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a271,
	cout => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a273);

fir_in_square_mult_amult_core_app_carry_node_a1_a_a49_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_app_carry_node_a1_a_a49 = abs_input_alcarry_a3_a & (!abs_input_alcarry_a2_a # !abs_input_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "22AA",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => abs_input_alcarry_a3_a,
	datab => abs_input_alcarry_a1_a,
	datad => abs_input_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_app_carry_node_a1_a_a49);

fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302_I : apex20ke_lcell
-- Equation(s):
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 = fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255 $ 
-- (fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a300 $ fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a255,
	datad => fir_in_square_mult_amult_core_apadder_aadder_a1_a_aadder_aresult_node_acs_buffer_a0_a_a258,
	cin => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a300,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302);

acc_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a23_a = DFFE(!GLOBAL(rms_upd_a0) & acc_ff_adffs_a23_a $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a22_a_a236, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a23_a_a239 = CARRY(acc_ff_adffs_a23_a & !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a22_a_a236 # !acc_ff_adffs_a23_a & (!acc_ff_adffs_a22_a_a236 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => acc_ff_adffs_a23_a,
	datab => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	cin => acc_ff_adffs_a22_a_a236,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a23_a,
	cout => acc_ff_adffs_a23_a_a239);

rms_out_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a9_a = DFFE(acc_ff_adffs_a23_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a23_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a9_a);

acc_ff_adffs_a24_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a24_a = DFFE(!GLOBAL(rms_upd_a0) & acc_ff_adffs_a24_a $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ !acc_ff_adffs_a23_a_a239, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a24_a_a242 = CARRY(acc_ff_adffs_a24_a & (fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 # !acc_ff_adffs_a23_a_a239) # !acc_ff_adffs_a24_a & 
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a23_a_a239)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => acc_ff_adffs_a24_a,
	datab => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	cin => acc_ff_adffs_a23_a_a239,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a24_a,
	cout => acc_ff_adffs_a24_a_a242);

rms_out_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a10_a = DFFE(acc_ff_adffs_a24_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a24_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a10_a);

acc_ff_adffs_a25_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a25_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a25_a $ acc_ff_adffs_a24_a_a242, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a25_a_a245 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a25_a & !acc_ff_adffs_a24_a_a242 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a24_a_a242 # !acc_ff_adffs_a25_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a25_a,
	cin => acc_ff_adffs_a24_a_a242,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a25_a,
	cout => acc_ff_adffs_a25_a_a245);

rms_out_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a11_a = DFFE(acc_ff_adffs_a25_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a25_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a11_a);

acc_ff_adffs_a26_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a26_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a26_a $ !acc_ff_adffs_a25_a_a245, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a26_a_a248 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (acc_ff_adffs_a26_a # !acc_ff_adffs_a25_a_a245) # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & acc_ff_adffs_a26_a & !acc_ff_adffs_a25_a_a245)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a26_a,
	cin => acc_ff_adffs_a25_a_a245,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a26_a,
	cout => acc_ff_adffs_a26_a_a248);

rms_out_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a12_a = DFFE(acc_ff_adffs_a26_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a26_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a12_a);

acc_ff_adffs_a27_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a27_a = DFFE(!GLOBAL(rms_upd_a0) & fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ acc_ff_adffs_a27_a $ acc_ff_adffs_a26_a_a248, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a27_a_a251 = CARRY(fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a27_a & !acc_ff_adffs_a26_a_a248 # 
-- !fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & (!acc_ff_adffs_a26_a_a248 # !acc_ff_adffs_a27_a))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	datab => acc_ff_adffs_a27_a,
	cin => acc_ff_adffs_a26_a_a248,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a27_a,
	cout => acc_ff_adffs_a27_a_a251);

rms_out_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a13_a = DFFE(acc_ff_adffs_a27_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a27_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a13_a);

acc_ff_adffs_a28_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a28_a = DFFE(!GLOBAL(rms_upd_a0) & acc_ff_adffs_a28_a $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 $ !acc_ff_adffs_a27_a_a251, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)
-- acc_ff_adffs_a28_a_a254 = CARRY(acc_ff_adffs_a28_a & (fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 # !acc_ff_adffs_a27_a_a251) # !acc_ff_adffs_a28_a & 
-- fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302 & !acc_ff_adffs_a27_a_a251)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => acc_ff_adffs_a28_a,
	datab => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	cin => acc_ff_adffs_a27_a_a251,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a28_a,
	cout => acc_ff_adffs_a28_a_a254);

rms_out_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a14_a = DFFE(acc_ff_adffs_a28_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a28_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a14_a);

acc_ff_adffs_a29_a_aI : apex20ke_lcell
-- Equation(s):
-- acc_ff_adffs_a29_a = DFFE(!GLOBAL(rms_upd_a0) & acc_ff_adffs_a29_a $ (acc_ff_adffs_a28_a_a254 $ fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , 
-- RMS_En_a15)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => acc_ff_adffs_a29_a,
	datad => fir_in_square_mult_amult_core_apadder_asub_par_add_aadder_a0_a_aadder_aresult_node_acs_buffer_a0_a_a302,
	cin => acc_ff_adffs_a28_a_a254,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => RMS_En_a15,
	sclr => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => acc_ff_adffs_a29_a);

rms_out_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- rms_out_ff_adffs_a15_a = DFFE(acc_ff_adffs_a29_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , rms_upd_a0)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => acc_ff_adffs_a29_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => rms_upd_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => rms_out_ff_adffs_a15_a);

rms_out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(0));

rms_out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(1));

rms_out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(2));

rms_out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(3));

rms_out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(4));

rms_out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(5));

rms_out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(6));

rms_out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(7));

rms_out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(8));

rms_out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(9));

rms_out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(10));

rms_out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(11));

rms_out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(12));

rms_out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(13));

rms_out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(14));

rms_out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => rms_out_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_rms_out(15));
END structure;


