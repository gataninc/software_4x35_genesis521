onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/otr
add wave -noupdate -format Logic -radix binary /tb/pa_reset
add wave -noupdate -format Logic -radix binary /tb/RMS_En
add wave -noupdate -format Logic -radix binary /tb/rms_upd
add wave -noupdate -format Logic -radix binary /tb/fir_in_range
add wave -noupdate -format literal -radix decimal /tb/rms_thr
add wave -noupdate -format Literal -radix decimal /tb/fir_in
add wave -noupdate -format Literal -radix decimal /tb/rms_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {10334192 ps} 0}
WaveRestoreZoom {0 ps} {69406665 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
