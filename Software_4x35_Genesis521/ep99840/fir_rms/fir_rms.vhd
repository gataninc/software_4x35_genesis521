---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_rms04.VHD
--		RMS = sqrt( sum( x^2 ) / n )
-- 	History
--		06/15/05 - MCS
--			Updated for QuartusII Ver 5.0 SP 0.21
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

---------------------------------------------------------------------------------------------------
entity fir_rms is 
	port( 
		IMR            : in      std_logic;				     -- Master Reset
     	CLK20          : in      std_logic;                         -- Master Clock
		OTR			: in		std_logic;
		PA_Reset		: in		std_logic;
		RMS_Thr		: in		std_logic_vector( 15 downto 0 );
		fir_in		: in		std_logic_vector( 15 downto 0 );
		rms_out		: buffer	std_logic_vector( 15 downto 0 ) );	
end fir_rms;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of fir_rms is

--	USE THIS ONE Ver 3566
--	constant ACC_LSB		: integer := 11;		--  6 ms
--	constant ACC_LSB		: integer := 12;		-- 13 ms
--	constant ACC_LSB		: integer := 13;		-- 26 ms
	constant ACC_LSB		: integer := 14;		-- 52 ms Ver 3566
--	constant ACC_LSB		: integer := 15;		-- 104 ms
--	constant ACC_LSB		: integer := 16;		-- 209 ms
--	constant ACC_LSB		: integer := 17;		-- 419 ms	Ver 3565


	constant acc_width		: integer := ACC_LSB+17;
	
	-- Signal Declarations ----------------------------------------------	
	signal abs_fir_in		: std_logic_vector( 15 downto 0 );
	signal acc 			: std_logic_vector( acc_width-1 downto 0 );
	signal acc_sum   		: std_logic_vector( acc_width-1 downto 0 );
	
	signal Fir_Square		: std_logic_vector( 15 downto 0 );
	signal Fir_Square_Ext	: std_logic_vector( acc_width-1 downto 0 );
	signal Fir_In_Range		: std_logic;
		
	signal rms_cnt_tc		: std_logic;
	signal rms_cnt_cnt_max	: integer range 0 to 2097151;
	signal rms_cnt_cnt		: integer range 0 to 2097151;
	signal RMS_En			: std_logic;
	signal rms_upd			: std_logic;	
	
	signal fir_in_reg		: std_logic_Vector( 15 downto 0 );
	signal rms_thr_reg		: std_logic_vector( 15 downto 0 );
	
begin
	with ACC_LSB select
		rms_cnt_cnt_max	<= 	    1023 when 4,
							    2047 when 5,
							    4095 when 6,
							    8191 when 7,
							   16383 when 8,	--   0.52 ms
							   32767 when 9, 	--   1.60 ms
							   65535 when 10,	-- 	3.20 ms
							  131071 when 11,	-- 	6.40 ms
							  242143 when 12,	--  13.80 ms
							  524287 when 13,	--  26 ms
							 1048575 when 14,	--  52 ms		
							 2097151 when others;	-- 104 ms
--							 4194303 when 16,	-- 209 ms
--							 8388607 when 17,	-- 419 ms
---							16777215 when 18,	
--	--						33554431 when others;


	-- Absolute Value of Input
	fir_in_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> fir_in,
			q				=> fir_in_reg );
			
	abs_input : lpm_abs
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			data				=> fir_in_reg,
			result			=> Abs_Fir_in );

	rms_thr_reg_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> Rms_thr,
			q				=> rms_thr_reg );
			
	Thr_Compare : lpm_compare
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			dataa			=> Abs_Fir_in,
			datab			=> rms_thr_reg,
			aleb				=> Fir_In_Range );

	-- Square the Input --------------------------------------------------------------------------
	fir_in_square_mult : lpm_mult
		generic map(
			LPM_WIDTHA		=> 8,
			LPM_WIDTHB		=> 8,
			LPM_WIDTHS		=> 16,
			LPM_WIDTHP		=> 16,
			LPM_REPRESENTATIOn	=> "SIGNED" )
		port map(
			dataa			=> Abs_Fir_in( 7 downto 0 ),
			datab			=> Abs_Fir_in( 7 downto 0 ),
			sum				=> Conv_Std_logic_vector( 0, 16 ),
			result			=> Fir_Square );
			
	-- Sign Extend the FIR_Square to the Acc Width
	Fir_Square_Ext_Gen : for i in 16 to Acc_Width-1 generate
		Fir_Square_Ext(i) <= Fir_Square(15);
	end generate;
	Fir_Square_Ext( 15 downto 0 ) <= Fir_Square;
	-- Square the Input --------------------------------------------------------------------------


	-- Accumulate the "Squared Input -------------------------------------------------------------
	acc_sum_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_Representation 	=> "SIGNED" )
		port map(
			Cin				=> '0',
			dataa			=> Fir_Square_Ext,
			datab			=> acc,
			result			=> acc_sum );
	-- Add fir_in_ext to acc ---------------------------------------------------------------------
			
	-- Accumulator Output ------------------------------------------------------------------------
	RMS_En		<= '1' when (( Fir_In_Range = '1' ) and ( PA_Reset = '0' ) and ( OTR = '0' )) else '0';

	rms_cnt_tc	<= '1' when ( rms_cnt_cnt = rms_cnt_cnt_max ) else '0';

	rms_upd		<= '1' when (( RMS_En = '1' ) and ( rms_cnt_tc = '1' )) else '0';

	acc_ff : lpm_ff
		generic map(
			LPM_WIDTh			=> Acc_Width )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			enable			=> RMS_En,
			sclr				=> rms_upd,
			data				=> acc_sum,
			q				=> acc );

	rms_out_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			enable			=> rms_upd,
			data				=> acc( ACC_LSB+15 downto ACC_LSB ),
			q				=> rms_out  );
	-- Accumulator Output ------------------------------------------------------------------------
	

     ----------------------------------------------------------------------------------------------
	clock_proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			rms_cnt_cnt	<= 0;
		elsif(( clk20'event ) and ( clk20 = '1' )) then

			if(( RMS_En = '1' ) and ( rms_cnt_tc = '0' ))
				then rms_cnt_cnt <= rms_cnt_cnt + 1;
			elsif(( RMS_En = '1' ) and ( rms_cnt_tc = '1' ))
				then rms_cnt_cnt <= 0;
			end if;			
		end if;
	end process;
     ----------------------------------------------------------------------------------------------
          
---------------------------------------------------------------------------------------------------
end behavioral;			-- fir_rms.vhd
---------------------------------------------------------------------------------------------------

