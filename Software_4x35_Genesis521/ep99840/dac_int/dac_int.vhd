-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DAC.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--        Module to interface to the Analog Devices AD8420 DAC
--   History:       <date> - <Author>
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity dac_int is
	port(
		IMR			: in		std_logic;					-- Master Reset
		CLK20		: in		std_logic;					-- Mater Clock
		WE_ADR_DISC1	: in		std_logic;
		WE_ADR_DISC2	: in		std_logic;
		WE_ADR_EVCH	: in		std_logic;
		WE_ADR_BIT	: in		std_logic;
		BIT_DAC_LD	: in		std_logic;
		BIT_DAC_DATA	: in		stD_logic_Vector( 11 downto 0 );
		DISC1		: in		std_logic_Vector( 11 downto 0 );
		DISC2		: in		std_logic_Vector( 11 downto 0 );
		EVCH 		: in		std_logic_Vector( 11 downto 0 );
		BIT   		: in		std_logic_Vector( 11 downto 0 );
		DA_OFS		: buffer	std_logic;
		DA_CLK		: buffer	std_logic;					-- DAC Clock
		DA_DOUT		: buffer	std_logic;					-- DAC Data
		DA_UPD		: buffer	std_logic );					-- DAC Load
end dac_int;
-------------------------------------------------------------------------------

architecture behavioral of dac_int is
	signal DAC_LD			: std_logic;
	signal DAC_data		: std_logic_vector( 15 downto 0 );
	signal WE_ADR_DISC1_DEL 	: std_logic;
	signal WE_ADR_DISC2_DEL 	: std_logic;
	signal WE_ADR_EVCH_DEL  	: std_logic;
	signal WE_ADR_BIT_DEL 	: std_logic;
	signal BIT_DAC_LD_DEL 	: std_logic;

     --------------------------------------------------------------------------
	component DAC8420 is 
		port( 
		     IMR            : in      std_logic;				     -- Master Reset
     		CLK20          : in      std_logic;                         -- Master Clock
			DAC_LD		: in		std_logic;					-- Start to Load DAC
			DAC_DATA		: in		std_logic_vector( 15 downto 0 );	-- 12-bit DAC Data
			DA_OFS		: buffer	std_logic;
			DA_CLK		: buffer	std_logic;					-- DAC Clock
			DA_DOUT		: buffer	std_logic;					-- DAC Data
			DA_UPD		: buffer	std_logic );					-- DAC Load
	end component DAC8420;
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
     -- Constant Declarations

begin

	U_DAC8420 : DAC8420
		port map(
			IMR					=> IMR,
			CLK20				=> CLK20,
			DAC_LD				=> DAC_LD,
			DAC_DATA				=> DAC_DATA,
			DA_OFS				=> DA_OFS,
			DA_CLK				=> DA_CLK,
			DA_DOUT				=> DA_DOUT,
			DA_UPD				=> DA_UPD );

	-------------------------------------------------------------------------------
	DAC8420_Proc : process( clk20, imr )
	begin
		if( imr = '1' ) then 
			DAC_LD			<= '0';
			DAC_DATA			<= x"0000";
			WE_ADR_DISC1_DEL 	<= '0';
			WE_ADR_DISC2_DEL 	<= '0';
			WE_ADR_EVCH_DEL  	<= '0';
			WE_ADR_BIT_DEL 	<= '0';
			BIT_DAC_LD_DEL 	<= '0';
			
		elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			WE_ADR_DISC1_DEL 	<= WE_ADR_DISC1;
			WE_ADR_DISC2_DEL 	<= WE_ADR_DISC2;
			WE_ADR_EVCH_DEL  	<= WE_ADR_EVCH;
			WE_ADR_BIT_DEL 	<= WE_ADR_BIT;
			BIT_DAC_LD_DEL 	<= BIT_DAC_LD;

			if(    WE_ADR_DISC1_DEL = '1' ) 	then DAC_LD <= '1';
			elsif( WE_ADR_DISC2_DEL = '1' ) 	then DAC_LD <= '1';
			elsif( WE_ADR_EVCH_DEL 	= '1' ) 	then DAC_LD <= '1';
			elsif( WE_ADR_BIT_DEL 	= '1' ) 	then DAC_LD <= '1';
			elsif( BIT_DAC_LD_DEL 	= '1' )	then DAC_LD <= '1'; 
								   		else DAC_LD <= '0' ;
			end if;

			if(    WE_ADR_DISC1_DEL = '1' ) then DAC_DATA <= "00" & "00" & DISC1;
			elsif( WE_ADR_DISC2_DEL = '1' ) then DAC_DATA <= "01" & "00" & DISC2;
			elsif( WE_ADR_EVCH_DEL  = '1' ) then DAC_DATA <= "11" & "00" & EVCH;
			elsif( WE_ADR_BIT_DEL   = '1' ) then DAC_DATA <= "10" & "00" & BIT;
			elsif( BIT_DAC_LD_DEL   = '1' ) then DAC_DATA <= "10" & "00" & not( BIT_DAC_DATA(11)) & BIT_DAC_DATA(10 downto 0 );
			end if;
			
		end if;
	end process;


-------------------------------------------------------------------------------
end behavioral;			-- DAC8420.VHD
-------------------------------------------------------------------------------

