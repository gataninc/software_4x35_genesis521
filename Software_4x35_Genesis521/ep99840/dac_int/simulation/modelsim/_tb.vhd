---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ads8321.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--   History:       <date> - <Author>
--		February 8, 2001 - MCS
--			Timing change clock to 20Mhz from 10Mhz
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

entity tb is
end tb;

architecture test_bench of tb is
	signal IMR		: std_logic := '1';
	signal clK20		: std_logic := '0';
	signal WE_ADR_DISC1	: std_logic;
	signal WE_ADR_DISC2	: std_logic;
	signal WE_ADR_EVCH	: std_logic;
	signal WE_ADR_BIT	: std_logic;
	signal BIT_DAC_LD	: std_logic;
	signal BIT_DAC_DATA	: stD_logic_Vector( 11 downto 0 );
	signal DISC1		: std_logic_Vector( 11 downto 0 );
	signal DISC2		: std_logic_Vector( 11 downto 0 );
	signal EVCH 		: std_logic_Vector( 11 downto 0 );
	signal BIT   		: std_logic_Vector( 11 downto 0 );
	signal DA_OFS		: std_logic;
	signal DA_CLK		: std_logic;					-- DAC Clock
	signal DA_DOUT		: std_logic;					-- DAC Data
	signal DA_UPD		: std_logic;					-- DAC Load
	signal DAC		: std_logic_Vector( 15 downto 0 ) := x"0000";
	signal DACA		: std_logic_Vector( 11 downto 0 ) := x"000";
	signal DACB		: std_logic_Vector( 11 downto 0 ) := x"000";
	signal DACC		: std_logic_Vector( 11 downto 0 ) := x"000";
	signal DACD		: std_logic_Vector( 11 downto 0 ) := x"000";
	-------------------------------------------------------------------------------
	component DAC_INT is
		port(
			IMR			: in		std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Mater Clock
			WE_ADR_DISC1	: in		std_logic;
			WE_ADR_DISC2	: in		std_logic;
			WE_ADR_EVCH	: in		std_logic;
			WE_ADR_BIT	: in		std_logic;
			BIT_DAC_LD	: in		std_logic;
			BIT_DAC_DATA	: in		stD_logic_Vector( 11 downto 0 );
			DISC1		: in		std_logic_Vector( 11 downto 0 );
			DISC2		: in		std_logic_Vector( 11 downto 0 );
			EVCH 		: in		std_logic_Vector( 11 downto 0 );
			BIT   		: in		std_logic_Vector( 11 downto 0 );
			DA_OFS		: out	std_logic;
			DA_CLK		: out	std_logic;					-- DAC Clock
			DA_DOUT		: out	std_logic;					-- DAC Data
			DA_UPD		: out	std_logic );					-- DAC Load
	end component DAC_INT;
-------------------------------------------------------------------------------

begin
     ----------------------------------------------------------------------------------------------
	U : DAC_INT
		port map(
			IMR			=> imr,
			CLK20		=> clk20,
			WE_ADR_DISC1	=> we_adr_disc1,
			WE_ADR_DISC2	=> we_adr_disc2,
			WE_ADR_EVCH	=> we_adr_evch,
			WE_ADR_BIT	=> we_adr_bit,
			BIT_DAC_LD	=> bit_dac_Ld,
			BIT_DAC_DATA	=> bit_dac_data,
			DISC1		=> disc1,
			DISC2		=> disc2,
			EVCH 		=> evch,
			BIT   		=> bit,
			DA_OFS		=> da_Ofs,
			DA_CLK		=> da_clk,
			DA_DOUT		=> da_dout,
			DA_UPD		=> da_upd );
-------------------------------------------------------------------------------

	imr	<= '0' after 350 ns;
	CLK20 <= not( CLK20 ) after 25 ns;

	clk_proc : process
	begin
		we_adr_disc1 	<= '0';
		we_adr_disc2 	<= '0';
		we_adr_evch  	<= '0';
		we_adr_bit   	<= '0';
		bit_dac_Ld 	<= '0';
		DISC1		<= x"111";
		DISC2		<= x"222";
		BIT   		<= x"333";
		EVCH 		<= x"444";
		BIT_DAC_DATA	<= x"555";
	
		wait for 1 us;
		for i in 0 to 4 loop
			wait until(( CLK20'event ) and ( CLk20 = '1' ));
			case i is
				when 0 => we_adr_disc1 	<= '1';
				when 1 => we_adr_disc2 	<= '1';
				when 2 => we_adr_evch	<= '1';
				when 3 => we_adr_bit 	<= '1';
				when 4 => bit_dac_ld	<= '1';
			end case;
			wait until(( CLK20'event ) and ( CLk20 = '1' ));
			we_adr_disc1 	<= '0';
			we_adr_disc2 	<= '0';
			we_adr_evch  	<= '0';
			we_adr_bit   	<= '0';
			bit_dac_Ld 	<= '0';
			wait for 10 us;
		end loop;
		wait;
	end process;
	
	DAC_PROC : process( DA_CLK )
	begin
		if(( DA_CLK'event ) and ( DA_CLK = '1' )) then
			if( DA_OFS = '0' ) then
				DAC <= DAC( 14 downto 0 ) & DA_DOUT;
			end if;
		end if;
	end process;
	
	DAC_UPD_PROC : process( DA_UPD )
	begin
		if(( DA_UPD'Event ) and ( DA_UPD = '0' )) then
			case DAC( 15 downto 14 ) is
				when "00" => DACA <= DAC( 11 downto 0 );
				when "01" => DACB <= DAC( 11 downto 0 );
				when "10" => DACC <= DAC( 11 downto 0 );
				when "11" => DACD <= DAC( 11 downto 0 );
				when others =>
			end case;
				
		
		end if;
	end process;
		
---------------------------------------------------------------------------------------------------
end test_bench;			-- ads8321.VHD
---------------------------------------------------------------------------------------------------

