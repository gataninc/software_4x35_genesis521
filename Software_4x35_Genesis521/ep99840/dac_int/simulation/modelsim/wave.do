onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix hexadecimal /tb/imr
add wave -noupdate -format Logic -radix hexadecimal /tb/clk20
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_ADR_DISC1
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_ADR_DISC2
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_ADR_evch
add wave -noupdate -format Logic -radix hexadecimal /tb/WE_ADR_bit
add wave -noupdate -format Logic -radix hexadecimal /tb/BIT_DAC_LD
add wave -noupdate -format Literal -radix hexadecimal /tb/DISC1
add wave -noupdate -format Literal -radix hexadecimal /tb/DISC2
add wave -noupdate -format Literal -radix hexadecimal /tb/EVCH
add wave -noupdate -format Literal -radix hexadecimal /tb/BIT
add wave -noupdate -format Literal -radix hexadecimal /tb/BIT_DAC_DATA
add wave -noupdate -format Logic -radix hexadecimal /tb/da_ofs
add wave -noupdate -format Logic -radix hexadecimal /tb/da_clk
add wave -noupdate -format Logic -radix hexadecimal /tb/da_dout
add wave -noupdate -format Logic -radix hexadecimal /tb/da_upd
add wave -noupdate -format Logic -radix hexadecimal /tb/dac
add wave -noupdate -format Logic -radix hexadecimal /tb/daca
add wave -noupdate -format Logic -radix hexadecimal /tb/dacb
add wave -noupdate -format Logic -radix hexadecimal /tb/dacc
add wave -noupdate -format Logic -radix hexadecimal /tb/dacd


TreeUpdate [SetDefaultTree]
WaveRestoreCursors {755959 ps} {19165170 ps} {8937182 ps}
WaveRestoreZoom {0 ps} {3870510 ps}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2