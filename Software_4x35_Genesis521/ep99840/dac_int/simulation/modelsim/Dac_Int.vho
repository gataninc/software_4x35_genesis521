-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "03/27/2006 09:19:33"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	dac_int IS
    PORT (
	IMR : IN std_logic;
	CLK20 : IN std_logic;
	WE_ADR_DISC1 : IN std_logic;
	WE_ADR_DISC2 : IN std_logic;
	WE_ADR_EVCH : IN std_logic;
	WE_ADR_BIT : IN std_logic;
	BIT_DAC_LD : IN std_logic;
	BIT_DAC_DATA : IN std_logic_vector(11 DOWNTO 0);
	DISC1 : IN std_logic_vector(11 DOWNTO 0);
	DISC2 : IN std_logic_vector(11 DOWNTO 0);
	EVCH : IN std_logic_vector(11 DOWNTO 0);
	BIT : IN std_logic_vector(11 DOWNTO 0);
	DA_OFS : OUT std_logic;
	DA_CLK : OUT std_logic;
	DA_DOUT : OUT std_logic;
	DA_UPD : OUT std_logic
	);
END dac_int;

ARCHITECTURE structure OF dac_int IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_IMR : std_logic;
SIGNAL ww_CLK20 : std_logic;
SIGNAL ww_WE_ADR_DISC1 : std_logic;
SIGNAL ww_WE_ADR_DISC2 : std_logic;
SIGNAL ww_WE_ADR_EVCH : std_logic;
SIGNAL ww_WE_ADR_BIT : std_logic;
SIGNAL ww_BIT_DAC_LD : std_logic;
SIGNAL ww_BIT_DAC_DATA : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_DISC1 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_DISC2 : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_EVCH : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_BIT : std_logic_vector(11 DOWNTO 0);
SIGNAL ww_DA_OFS : std_logic;
SIGNAL ww_DA_CLK : std_logic;
SIGNAL ww_DA_DOUT : std_logic;
SIGNAL ww_DA_UPD : std_logic;
SIGNAL WE_ADR_DISC2_DEL : std_logic;
SIGNAL U_DAC8420_aSR_REG_a14_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a13_a : std_logic;
SIGNAL DAC_data_a14_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a12_a : std_logic;
SIGNAL DAC_data_a14_a_a5145 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a11_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a10_a : std_logic;
SIGNAL DAC_data_a11_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a9_a : std_logic;
SIGNAL DAC_data_a10_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a8_a : std_logic;
SIGNAL DAC_data_a9_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a7_a : std_logic;
SIGNAL DAC_data_a8_a : std_logic;
SIGNAL DAC_data_a11_a_a5156 : std_logic;
SIGNAL DAC_data_a11_a_a5157 : std_logic;
SIGNAL DAC_data_a11_a_a5279 : std_logic;
SIGNAL DAC_data_a11_a_a5158 : std_logic;
SIGNAL DAC_data_a11_a_a5255 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a6_a : std_logic;
SIGNAL DAC_data_a7_a : std_logic;
SIGNAL DAC_data_a10_a_a5164 : std_logic;
SIGNAL DAC_data_a10_a_a5165 : std_logic;
SIGNAL DAC_data_a10_a_a5280 : std_logic;
SIGNAL DAC_data_a10_a_a5166 : std_logic;
SIGNAL DAC_data_a10_a_a5256 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a5_a : std_logic;
SIGNAL DAC_data_a6_a : std_logic;
SIGNAL DAC_data_a9_a_a5172 : std_logic;
SIGNAL DAC_data_a9_a_a5173 : std_logic;
SIGNAL DAC_data_a9_a_a5281 : std_logic;
SIGNAL DAC_data_a9_a_a5174 : std_logic;
SIGNAL DAC_data_a9_a_a5257 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a4_a : std_logic;
SIGNAL DAC_data_a5_a : std_logic;
SIGNAL DAC_data_a8_a_a5180 : std_logic;
SIGNAL DAC_data_a8_a_a5181 : std_logic;
SIGNAL DAC_data_a8_a_a5282 : std_logic;
SIGNAL DAC_data_a8_a_a5182 : std_logic;
SIGNAL DAC_data_a8_a_a5258 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a3_a : std_logic;
SIGNAL DAC_data_a4_a : std_logic;
SIGNAL DAC_data_a7_a_a5188 : std_logic;
SIGNAL DAC_data_a7_a_a5189 : std_logic;
SIGNAL DAC_data_a7_a_a5283 : std_logic;
SIGNAL DAC_data_a7_a_a5190 : std_logic;
SIGNAL DAC_data_a7_a_a5259 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a2_a : std_logic;
SIGNAL DAC_data_a3_a : std_logic;
SIGNAL DAC_data_a6_a_a5196 : std_logic;
SIGNAL DAC_data_a6_a_a5197 : std_logic;
SIGNAL DAC_data_a6_a_a5284 : std_logic;
SIGNAL DAC_data_a6_a_a5198 : std_logic;
SIGNAL DAC_data_a6_a_a5260 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a1_a : std_logic;
SIGNAL DAC_data_a2_a : std_logic;
SIGNAL DAC_data_a5_a_a5204 : std_logic;
SIGNAL DAC_data_a5_a_a5205 : std_logic;
SIGNAL DAC_data_a5_a_a5285 : std_logic;
SIGNAL DAC_data_a5_a_a5206 : std_logic;
SIGNAL DAC_data_a5_a_a5261 : std_logic;
SIGNAL U_DAC8420_aSR_REG_a0_a : std_logic;
SIGNAL DAC_data_a1_a : std_logic;
SIGNAL DAC_data_a4_a_a5212 : std_logic;
SIGNAL DAC_data_a4_a_a5213 : std_logic;
SIGNAL DAC_data_a4_a_a5286 : std_logic;
SIGNAL DAC_data_a4_a_a5214 : std_logic;
SIGNAL DAC_data_a4_a_a5262 : std_logic;
SIGNAL DAC_data_a0_a : std_logic;
SIGNAL DAC_data_a3_a_a5220 : std_logic;
SIGNAL DAC_data_a3_a_a5221 : std_logic;
SIGNAL DAC_data_a3_a_a5287 : std_logic;
SIGNAL DAC_data_a3_a_a5222 : std_logic;
SIGNAL DAC_data_a3_a_a5263 : std_logic;
SIGNAL DAC_data_a2_a_a5228 : std_logic;
SIGNAL DAC_data_a2_a_a5229 : std_logic;
SIGNAL DAC_data_a2_a_a5288 : std_logic;
SIGNAL DAC_data_a2_a_a5230 : std_logic;
SIGNAL DAC_data_a2_a_a5264 : std_logic;
SIGNAL DAC_data_a1_a_a5235 : std_logic;
SIGNAL DAC_data_a1_a_a5236 : std_logic;
SIGNAL DAC_data_a1_a_a5289 : std_logic;
SIGNAL DAC_data_a1_a_a5237 : std_logic;
SIGNAL DAC_data_a1_a_a5265 : std_logic;
SIGNAL DAC_data_a0_a_a5239 : std_logic;
SIGNAL DAC_data_a0_a_a5240 : std_logic;
SIGNAL DAC_data_a0_a_a5290 : std_logic;
SIGNAL DAC_data_a0_a_a5241 : std_logic;
SIGNAL DAC_data_a0_a_a5266 : std_logic;
SIGNAL WE_ADR_DISC2_acombout : std_logic;
SIGNAL EVCH_a11_a_acombout : std_logic;
SIGNAL BIT_a11_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a11_a_acombout : std_logic;
SIGNAL DISC2_a11_a_acombout : std_logic;
SIGNAL DISC1_a11_a_acombout : std_logic;
SIGNAL EVCH_a10_a_acombout : std_logic;
SIGNAL BIT_a10_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a10_a_acombout : std_logic;
SIGNAL DISC2_a10_a_acombout : std_logic;
SIGNAL DISC1_a10_a_acombout : std_logic;
SIGNAL EVCH_a9_a_acombout : std_logic;
SIGNAL BIT_a9_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a9_a_acombout : std_logic;
SIGNAL DISC2_a9_a_acombout : std_logic;
SIGNAL DISC1_a9_a_acombout : std_logic;
SIGNAL EVCH_a8_a_acombout : std_logic;
SIGNAL BIT_a8_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a8_a_acombout : std_logic;
SIGNAL DISC2_a8_a_acombout : std_logic;
SIGNAL DISC1_a8_a_acombout : std_logic;
SIGNAL EVCH_a7_a_acombout : std_logic;
SIGNAL BIT_a7_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a7_a_acombout : std_logic;
SIGNAL DISC2_a7_a_acombout : std_logic;
SIGNAL DISC1_a7_a_acombout : std_logic;
SIGNAL EVCH_a6_a_acombout : std_logic;
SIGNAL BIT_a6_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a6_a_acombout : std_logic;
SIGNAL DISC2_a6_a_acombout : std_logic;
SIGNAL DISC1_a6_a_acombout : std_logic;
SIGNAL EVCH_a5_a_acombout : std_logic;
SIGNAL BIT_a5_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a5_a_acombout : std_logic;
SIGNAL DISC2_a5_a_acombout : std_logic;
SIGNAL DISC1_a5_a_acombout : std_logic;
SIGNAL EVCH_a4_a_acombout : std_logic;
SIGNAL BIT_a4_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a4_a_acombout : std_logic;
SIGNAL DISC2_a4_a_acombout : std_logic;
SIGNAL DISC1_a4_a_acombout : std_logic;
SIGNAL EVCH_a3_a_acombout : std_logic;
SIGNAL BIT_a3_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a3_a_acombout : std_logic;
SIGNAL DISC2_a3_a_acombout : std_logic;
SIGNAL DISC1_a3_a_acombout : std_logic;
SIGNAL EVCH_a2_a_acombout : std_logic;
SIGNAL BIT_a2_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a2_a_acombout : std_logic;
SIGNAL DISC2_a2_a_acombout : std_logic;
SIGNAL DISC1_a2_a_acombout : std_logic;
SIGNAL EVCH_a1_a_acombout : std_logic;
SIGNAL BIT_a1_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a1_a_acombout : std_logic;
SIGNAL DISC2_a1_a_acombout : std_logic;
SIGNAL DISC1_a1_a_acombout : std_logic;
SIGNAL EVCH_a0_a_acombout : std_logic;
SIGNAL BIT_a0_a_acombout : std_logic;
SIGNAL BIT_DAC_DATA_a0_a_acombout : std_logic;
SIGNAL DISC2_a0_a_acombout : std_logic;
SIGNAL DISC1_a0_a_acombout : std_logic;
SIGNAL BIT_DAC_LD_acombout : std_logic;
SIGNAL CLK20_acombout : std_logic;
SIGNAL IMR_acombout : std_logic;
SIGNAL BIT_DAC_LD_DEL : std_logic;
SIGNAL WE_ADR_DISC1_acombout : std_logic;
SIGNAL WE_ADR_DISC1_DEL : std_logic;
SIGNAL WE_ADR_BIT_acombout : std_logic;
SIGNAL WE_ADR_BIT_DEL : std_logic;
SIGNAL WE_ADR_EVCH_acombout : std_logic;
SIGNAL WE_ADR_EVCH_DEL : std_logic;
SIGNAL DAC_data_a0_a_a5142 : std_logic;
SIGNAL DAC_LD : std_logic;
SIGNAL U_DAC8420_asr_cnt_a0_a : std_logic;
SIGNAL U_DAC8420_asr_cnt_a0_a_a55 : std_logic;
SIGNAL U_DAC8420_asr_cnt_a1_a : std_logic;
SIGNAL U_DAC8420_asr_cnt_a1_a_a58 : std_logic;
SIGNAL U_DAC8420_asr_cnt_a2_a : std_logic;
SIGNAL U_DAC8420_asr_cnt_a2_a_a49 : std_logic;
SIGNAL U_DAC8420_asr_cnt_a3_a_a52 : std_logic;
SIGNAL U_DAC8420_asr_cnt_a4_a : std_logic;
SIGNAL U_DAC8420_asr_cnt_a3_a : std_logic;
SIGNAL U_DAC8420_aEqual_a86 : std_logic;
SIGNAL U_DAC8420_aiDA_UPD_a0_a_a35 : std_logic;
SIGNAL U_DAC8420_aclk_cnt_a1_a : std_logic;
SIGNAL U_DAC8420_aEqual_a88 : std_logic;
SIGNAL U_DAC8420_aiDA_UPD_a0_a : std_logic;
SIGNAL U_DAC8420_aiDA_UPD_a1_a : std_logic;
SIGNAL U_DAC8420_aclk_cnt_a0_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a15_a_a627 : std_logic;
SIGNAL U_DAC8420_aEqual_a87 : std_logic;
SIGNAL U_DAC8420_aDA_BUSY : std_logic;
SIGNAL U_DAC8420_aDA_CLK : std_logic;
SIGNAL DAC_data_a15_a_a5143 : std_logic;
SIGNAL DAC_data_a15_a : std_logic;
SIGNAL U_DAC8420_aSR_REG_a15_a : std_logic;
SIGNAL U_DAC8420_aDA_DOUT : std_logic;
SIGNAL U_DAC8420_aDA_UPD : std_logic;
SIGNAL ALT_INV_U_DAC8420_aDA_BUSY : std_logic;
SIGNAL ALT_INV_U_DAC8420_aDA_CLK : std_logic;
SIGNAL ALT_INV_U_DAC8420_aDA_UPD : std_logic;

BEGIN

ww_IMR <= IMR;
ww_CLK20 <= CLK20;
ww_WE_ADR_DISC1 <= WE_ADR_DISC1;
ww_WE_ADR_DISC2 <= WE_ADR_DISC2;
ww_WE_ADR_EVCH <= WE_ADR_EVCH;
ww_WE_ADR_BIT <= WE_ADR_BIT;
ww_BIT_DAC_LD <= BIT_DAC_LD;
ww_BIT_DAC_DATA <= BIT_DAC_DATA;
ww_DISC1 <= DISC1;
ww_DISC2 <= DISC2;
ww_EVCH <= EVCH;
ww_BIT <= BIT;
DA_OFS <= ww_DA_OFS;
DA_CLK <= ww_DA_CLK;
DA_DOUT <= ww_DA_DOUT;
DA_UPD <= ww_DA_UPD;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;
ALT_INV_U_DAC8420_aDA_BUSY <= NOT U_DAC8420_aDA_BUSY;
ALT_INV_U_DAC8420_aDA_CLK <= NOT U_DAC8420_aDA_CLK;
ALT_INV_U_DAC8420_aDA_UPD <= NOT U_DAC8420_aDA_UPD;

WE_ADR_DISC2_DEL_aI : apex20ke_lcell
-- Equation(s):
-- WE_ADR_DISC2_DEL = DFFE(WE_ADR_DISC2_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => WE_ADR_DISC2_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_ADR_DISC2_DEL);

U_DAC8420_aSR_REG_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a14_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a13_a # !U_DAC8420_aDA_BUSY & (DAC_data_a14_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACAC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a13_a,
	datab => DAC_data_a14_a,
	datac => U_DAC8420_aDA_BUSY,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a14_a);

WE_ADR_DISC2_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_ADR_DISC2,
	combout => WE_ADR_DISC2_acombout);

U_DAC8420_aSR_REG_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a13_a = DFFE(U_DAC8420_aSR_REG_a12_a & (U_DAC8420_aDA_BUSY), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AA00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a12_a,
	datad => U_DAC8420_aDA_BUSY,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a13_a);

DAC_data_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a14_a = DFFE(DAC_data_a14_a_a5145 # DAC_data_a14_a & DAC_data_a0_a_a5142 & !BIT_DAC_LD_DEL, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF08",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a14_a,
	datab => DAC_data_a0_a_a5142,
	datac => BIT_DAC_LD_DEL,
	datad => DAC_data_a14_a_a5145,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a14_a);

U_DAC8420_aSR_REG_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a12_a = DFFE(U_DAC8420_aSR_REG_a11_a & (U_DAC8420_aDA_BUSY), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A0A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a11_a,
	datac => U_DAC8420_aDA_BUSY,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a12_a);

DAC_data_a14_a_a5145_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a14_a_a5145 = !WE_ADR_DISC1_DEL & (WE_ADR_DISC2_DEL # WE_ADR_EVCH_DEL)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3330",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_DISC2_DEL,
	datad => WE_ADR_EVCH_DEL,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a14_a_a5145);

U_DAC8420_aSR_REG_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a11_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a10_a) # !U_DAC8420_aDA_BUSY & DAC_data_a11_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DAC_data_a11_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a10_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a11_a);

U_DAC8420_aSR_REG_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a10_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a9_a) # !U_DAC8420_aDA_BUSY & DAC_data_a10_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DAC_data_a10_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a9_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a10_a);

DAC_data_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a11_a = DFFE(!DAC_data_a11_a_a5255, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a11_a_a5255,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a11_a);

U_DAC8420_aSR_REG_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a9_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a8_a) # !U_DAC8420_aDA_BUSY & DAC_data_a9_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a9_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a8_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a9_a);

DAC_data_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a10_a = DFFE(!DAC_data_a10_a_a5256, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a10_a_a5256,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a10_a);

U_DAC8420_aSR_REG_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a8_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a7_a) # !U_DAC8420_aDA_BUSY & DAC_data_a8_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a8_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a7_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a8_a);

DAC_data_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a9_a = DFFE(!DAC_data_a9_a_a5257, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a9_a_a5257,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a9_a);

U_DAC8420_aSR_REG_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a7_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a6_a) # !U_DAC8420_aDA_BUSY & DAC_data_a7_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FC0C",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => DAC_data_a7_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a7_a);

DAC_data_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a8_a = DFFE(!DAC_data_a8_a_a5258, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a8_a_a5258,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a8_a);

EVCH_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(11),
	combout => EVCH_a11_a_acombout);

BIT_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(11),
	combout => BIT_a11_a_acombout);

DAC_data_a11_a_a5156_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a11_a_a5156 = WE_ADR_EVCH_DEL & EVCH_a11_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a11_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a11_a_acombout,
	datab => WE_ADR_EVCH_DEL,
	datac => WE_ADR_BIT_DEL,
	datad => BIT_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a11_a_a5156);

BIT_DAC_DATA_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(11),
	combout => BIT_DAC_DATA_a11_a_acombout);

DAC_data_a11_a_a5157_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a11_a_a5157 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (!BIT_DAC_DATA_a11_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a11_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "20E0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a11_a,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a11_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a11_a_a5157);

DAC_data_a11_a_a5158_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a11_a_a5279 = !DAC_data_a11_a_a5157 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a11_a_a5156)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FD",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a11_a_a5156,
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a11_a_a5157,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a11_a_a5158,
	cascout => DAC_data_a11_a_a5279);

DISC2_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(11),
	combout => DISC2_a11_a_acombout);

DISC1_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(11),
	combout => DISC1_a11_a_acombout);

DAC_data_a11_a_a5255_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a11_a_a5255 = (WE_ADR_DISC1_DEL & !DISC1_a11_a_acombout # !WE_ADR_DISC1_DEL & (!DISC2_a11_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a11_a_a5279)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4777",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DISC1_a11_a_acombout,
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_DISC2_DEL,
	datad => DISC2_a11_a_acombout,
	cascin => DAC_data_a11_a_a5279,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a11_a_a5255);

U_DAC8420_aSR_REG_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a6_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a5_a) # !U_DAC8420_aDA_BUSY & (DAC_data_a6_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F5A0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aSR_REG_a5_a,
	datad => DAC_data_a6_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a6_a);

DAC_data_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a7_a = DFFE(!DAC_data_a7_a_a5259, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a7_a_a5259,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a7_a);

EVCH_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(10),
	combout => EVCH_a10_a_acombout);

BIT_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(10),
	combout => BIT_a10_a_acombout);

DAC_data_a10_a_a5164_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a10_a_a5164 = WE_ADR_EVCH_DEL & EVCH_a10_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a10_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a10_a_acombout,
	datab => WE_ADR_BIT_DEL,
	datac => WE_ADR_EVCH_DEL,
	datad => BIT_a10_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a10_a_a5164);

BIT_DAC_DATA_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(10),
	combout => BIT_DAC_DATA_a10_a_acombout);

DAC_data_a10_a_a5165_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a10_a_a5165 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a10_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a10_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a10_a,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a10_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a10_a_a5165);

DAC_data_a10_a_a5166_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a10_a_a5280 = !DAC_data_a10_a_a5165 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a10_a_a5164)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DAC_data_a10_a_a5164,
	datad => DAC_data_a10_a_a5165,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a10_a_a5166,
	cascout => DAC_data_a10_a_a5280);

DISC2_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(10),
	combout => DISC2_a10_a_acombout);

DISC1_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(10),
	combout => DISC1_a10_a_acombout);

DAC_data_a10_a_a5256_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a10_a_a5256 = (WE_ADR_DISC1_DEL & (!DISC1_a10_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a10_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a10_a_a5280)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "15BF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DISC2_a10_a_acombout,
	datad => DISC1_a10_a_acombout,
	cascin => DAC_data_a10_a_a5280,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a10_a_a5256);

U_DAC8420_aSR_REG_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a5_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a4_a # !U_DAC8420_aDA_BUSY & (DAC_data_a5_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AAF0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a4_a,
	datac => DAC_data_a5_a,
	datad => U_DAC8420_aDA_BUSY,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a5_a);

DAC_data_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a6_a = DFFE(!DAC_data_a6_a_a5260, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a6_a_a5260,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a6_a);

EVCH_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(9),
	combout => EVCH_a9_a_acombout);

BIT_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(9),
	combout => BIT_a9_a_acombout);

DAC_data_a9_a_a5172_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a9_a_a5172 = WE_ADR_EVCH_DEL & EVCH_a9_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a9_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a9_a_acombout,
	datab => WE_ADR_BIT_DEL,
	datac => WE_ADR_EVCH_DEL,
	datad => BIT_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a9_a_a5172);

BIT_DAC_DATA_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(9),
	combout => BIT_DAC_DATA_a9_a_acombout);

DAC_data_a9_a_a5173_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a9_a_a5173 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a9_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a9_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "C808",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a9_a,
	datab => DAC_data_a0_a_a5142,
	datac => BIT_DAC_LD_DEL,
	datad => BIT_DAC_DATA_a9_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a9_a_a5173);

DAC_data_a9_a_a5174_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a9_a_a5281 = !DAC_data_a9_a_a5173 & (WE_ADR_DISC2_DEL # WE_ADR_DISC1_DEL # !DAC_data_a9_a_a5172)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datab => DAC_data_a9_a_a5172,
	datac => WE_ADR_DISC1_DEL,
	datad => DAC_data_a9_a_a5173,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a9_a_a5174,
	cascout => DAC_data_a9_a_a5281);

DISC2_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(9),
	combout => DISC2_a9_a_acombout);

DISC1_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(9),
	combout => DISC1_a9_a_acombout);

DAC_data_a9_a_a5257_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a9_a_a5257 = (WE_ADR_DISC1_DEL & !DISC1_a9_a_acombout # !WE_ADR_DISC1_DEL & (!DISC2_a9_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a9_a_a5281)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "535F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DISC1_a9_a_acombout,
	datab => WE_ADR_DISC2_DEL,
	datac => WE_ADR_DISC1_DEL,
	datad => DISC2_a9_a_acombout,
	cascin => DAC_data_a9_a_a5281,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a9_a_a5257);

U_DAC8420_aSR_REG_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a4_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a3_a # !U_DAC8420_aDA_BUSY & (DAC_data_a4_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aSR_REG_a3_a,
	datad => DAC_data_a4_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a4_a);

DAC_data_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a5_a = DFFE(!DAC_data_a5_a_a5261, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a5_a_a5261,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a5_a);

EVCH_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(8),
	combout => EVCH_a8_a_acombout);

BIT_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(8),
	combout => BIT_a8_a_acombout);

DAC_data_a8_a_a5180_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a8_a_a5180 = WE_ADR_EVCH_DEL & (EVCH_a8_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & (BIT_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_BIT_DEL,
	datab => EVCH_a8_a_acombout,
	datac => WE_ADR_EVCH_DEL,
	datad => BIT_a8_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a8_a_a5180);

BIT_DAC_DATA_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(8),
	combout => BIT_DAC_DATA_a8_a_acombout);

DAC_data_a8_a_a5181_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a8_a_a5181 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a8_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a8_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A820",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5142,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a8_a,
	datad => BIT_DAC_DATA_a8_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a8_a_a5181);

DAC_data_a8_a_a5182_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a8_a_a5282 = !DAC_data_a8_a_a5181 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a8_a_a5180)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FB",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DAC_data_a8_a_a5180,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a8_a_a5181,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a8_a_a5182,
	cascout => DAC_data_a8_a_a5282);

DISC2_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(8),
	combout => DISC2_a8_a_acombout);

DISC1_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(8),
	combout => DISC1_a8_a_acombout);

DAC_data_a8_a_a5258_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a8_a_a5258 = (WE_ADR_DISC1_DEL & (!DISC1_a8_a_acombout) # !WE_ADR_DISC1_DEL & (!WE_ADR_DISC2_DEL # !DISC2_a8_a_acombout)) & CASCADE(DAC_data_a8_a_a5282)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "15BF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DISC2_a8_a_acombout,
	datac => WE_ADR_DISC2_DEL,
	datad => DISC1_a8_a_acombout,
	cascin => DAC_data_a8_a_a5282,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a8_a_a5258);

U_DAC8420_aSR_REG_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a3_a = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aSR_REG_a2_a) # !U_DAC8420_aDA_BUSY & DAC_data_a3_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FA0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a3_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aSR_REG_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a3_a);

DAC_data_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a4_a = DFFE(!DAC_data_a4_a_a5262, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a4_a_a5262,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a4_a);

EVCH_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(7),
	combout => EVCH_a7_a_acombout);

BIT_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(7),
	combout => BIT_a7_a_acombout);

DAC_data_a7_a_a5188_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a7_a_a5188 = WE_ADR_EVCH_DEL & EVCH_a7_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a7_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a7_a_acombout,
	datab => WE_ADR_EVCH_DEL,
	datac => WE_ADR_BIT_DEL,
	datad => BIT_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a7_a_a5188);

BIT_DAC_DATA_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(7),
	combout => BIT_DAC_DATA_a7_a_acombout);

DAC_data_a7_a_a5189_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a7_a_a5189 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a7_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a7_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A820",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5142,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a7_a,
	datad => BIT_DAC_DATA_a7_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a7_a_a5189);

DAC_data_a7_a_a5190_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a7_a_a5283 = !DAC_data_a7_a_a5189 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a7_a_a5188)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3233",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DAC_data_a7_a_a5189,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a7_a_a5188,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a7_a_a5190,
	cascout => DAC_data_a7_a_a5283);

DISC2_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(7),
	combout => DISC2_a7_a_acombout);

DISC1_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(7),
	combout => DISC1_a7_a_acombout);

DAC_data_a7_a_a5259_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a7_a_a5259 = (WE_ADR_DISC1_DEL & (!DISC1_a7_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a7_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a7_a_a5283)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1D3F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datab => WE_ADR_DISC1_DEL,
	datac => DISC1_a7_a_acombout,
	datad => DISC2_a7_a_acombout,
	cascin => DAC_data_a7_a_a5283,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a7_a_a5259);

U_DAC8420_aSR_REG_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a2_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a1_a # !U_DAC8420_aDA_BUSY & (DAC_data_a2_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F3C0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aSR_REG_a1_a,
	datad => DAC_data_a2_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a2_a);

DAC_data_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a3_a = DFFE(!DAC_data_a3_a_a5263, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a3_a_a5263,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a3_a);

EVCH_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(6),
	combout => EVCH_a6_a_acombout);

BIT_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(6),
	combout => BIT_a6_a_acombout);

DAC_data_a6_a_a5196_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a6_a_a5196 = WE_ADR_EVCH_DEL & (EVCH_a6_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & (BIT_a6_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E2C0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_BIT_DEL,
	datab => WE_ADR_EVCH_DEL,
	datac => EVCH_a6_a_acombout,
	datad => BIT_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a6_a_a5196);

BIT_DAC_DATA_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(6),
	combout => BIT_DAC_DATA_a6_a_acombout);

DAC_data_a6_a_a5197_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a6_a_a5197 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a6_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a6_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A820",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5142,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a6_a,
	datad => BIT_DAC_DATA_a6_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a6_a_a5197);

DAC_data_a6_a_a5198_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a6_a_a5284 = !DAC_data_a6_a_a5197 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a6_a_a5196)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00EF",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DAC_data_a6_a_a5196,
	datad => DAC_data_a6_a_a5197,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a6_a_a5198,
	cascout => DAC_data_a6_a_a5284);

DISC2_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(6),
	combout => DISC2_a6_a_acombout);

DISC1_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(6),
	combout => DISC1_a6_a_acombout);

DAC_data_a6_a_a5260_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a6_a_a5260 = (WE_ADR_DISC1_DEL & (!DISC1_a6_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a6_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a6_a_a5284)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1B5F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DISC1_a6_a_acombout,
	datad => DISC2_a6_a_acombout,
	cascin => DAC_data_a6_a_a5284,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a6_a_a5260);

U_DAC8420_aSR_REG_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a1_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a0_a # !U_DAC8420_aDA_BUSY & (DAC_data_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a0_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => DAC_data_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a1_a);

DAC_data_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a2_a = DFFE(!DAC_data_a2_a_a5264, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a2_a_a5264,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a2_a);

EVCH_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(5),
	combout => EVCH_a5_a_acombout);

BIT_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(5),
	combout => BIT_a5_a_acombout);

DAC_data_a5_a_a5204_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a5_a_a5204 = WE_ADR_EVCH_DEL & (EVCH_a5_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & (BIT_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CAC0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_BIT_DEL,
	datab => EVCH_a5_a_acombout,
	datac => WE_ADR_EVCH_DEL,
	datad => BIT_a5_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a5_a_a5204);

BIT_DAC_DATA_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(5),
	combout => BIT_DAC_DATA_a5_a_acombout);

DAC_data_a5_a_a5205_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a5_a_a5205 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a5_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a5_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E040",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_DAC_LD_DEL,
	datab => DAC_data_a5_a,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a5_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a5_a_a5205);

DAC_data_a5_a_a5206_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a5_a_a5285 = !DAC_data_a5_a_a5205 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a5_a_a5204)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3233",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DAC_data_a5_a_a5205,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a5_a_a5204,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a5_a_a5206,
	cascout => DAC_data_a5_a_a5285);

DISC2_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(5),
	combout => DISC2_a5_a_acombout);

DISC1_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(5),
	combout => DISC1_a5_a_acombout);

DAC_data_a5_a_a5261_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a5_a_a5261 = (WE_ADR_DISC1_DEL & (!DISC1_a5_a_acombout) # !WE_ADR_DISC1_DEL & (!WE_ADR_DISC2_DEL # !DISC2_a5_a_acombout)) & CASCADE(DAC_data_a5_a_a5285)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "15BF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DISC2_a5_a_acombout,
	datac => WE_ADR_DISC2_DEL,
	datad => DISC1_a5_a_acombout,
	cascin => DAC_data_a5_a_a5285,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a5_a_a5261);

U_DAC8420_aSR_REG_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a0_a = DFFE(DAC_data_a0_a & (!U_DAC8420_aDA_BUSY), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0A0A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a,
	datac => U_DAC8420_aDA_BUSY,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a0_a);

DAC_data_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a1_a = DFFE(!DAC_data_a1_a_a5265, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a1_a_a5265,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a1_a);

EVCH_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(4),
	combout => EVCH_a4_a_acombout);

BIT_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(4),
	combout => BIT_a4_a_acombout);

DAC_data_a4_a_a5212_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a4_a_a5212 = WE_ADR_EVCH_DEL & EVCH_a4_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACA0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a4_a_acombout,
	datab => WE_ADR_BIT_DEL,
	datac => WE_ADR_EVCH_DEL,
	datad => BIT_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a4_a_a5212);

BIT_DAC_DATA_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(4),
	combout => BIT_DAC_DATA_a4_a_acombout);

DAC_data_a4_a_a5213_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a4_a_a5213 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a4_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a4_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a4_a,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a4_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a4_a_a5213);

DAC_data_a4_a_a5214_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a4_a_a5286 = !DAC_data_a4_a_a5213 & (WE_ADR_DISC2_DEL # WE_ADR_DISC1_DEL # !DAC_data_a4_a_a5212)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5455",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a4_a_a5213,
	datab => WE_ADR_DISC2_DEL,
	datac => WE_ADR_DISC1_DEL,
	datad => DAC_data_a4_a_a5212,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a4_a_a5214,
	cascout => DAC_data_a4_a_a5286);

DISC2_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(4),
	combout => DISC2_a4_a_acombout);

DISC1_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(4),
	combout => DISC1_a4_a_acombout);

DAC_data_a4_a_a5262_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a4_a_a5262 = (WE_ADR_DISC1_DEL & (!DISC1_a4_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a4_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a4_a_a5286)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "15BF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DISC2_a4_a_acombout,
	datad => DISC1_a4_a_acombout,
	cascin => DAC_data_a4_a_a5286,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a4_a_a5262);

DAC_data_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a = DFFE(!DAC_data_a0_a_a5266, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => DAC_data_a0_a_a5266,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a0_a);

EVCH_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(3),
	combout => EVCH_a3_a_acombout);

BIT_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(3),
	combout => BIT_a3_a_acombout);

DAC_data_a3_a_a5220_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a3_a_a5220 = WE_ADR_EVCH_DEL & (EVCH_a3_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & (BIT_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E4A0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_EVCH_DEL,
	datab => WE_ADR_BIT_DEL,
	datac => EVCH_a3_a_acombout,
	datad => BIT_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a3_a_a5220);

BIT_DAC_DATA_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(3),
	combout => BIT_DAC_DATA_a3_a_acombout);

DAC_data_a3_a_a5221_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a3_a_a5221 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a3_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a3_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A820",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5142,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a3_a,
	datad => BIT_DAC_DATA_a3_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a3_a_a5221);

DAC_data_a3_a_a5222_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a3_a_a5287 = !DAC_data_a3_a_a5221 & (WE_ADR_DISC2_DEL # WE_ADR_DISC1_DEL # !DAC_data_a3_a_a5220)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5455",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a3_a_a5221,
	datab => WE_ADR_DISC2_DEL,
	datac => WE_ADR_DISC1_DEL,
	datad => DAC_data_a3_a_a5220,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a3_a_a5222,
	cascout => DAC_data_a3_a_a5287);

DISC2_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(3),
	combout => DISC2_a3_a_acombout);

DISC1_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(3),
	combout => DISC1_a3_a_acombout);

DAC_data_a3_a_a5263_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a3_a_a5263 = (WE_ADR_DISC1_DEL & !DISC1_a3_a_acombout # !WE_ADR_DISC1_DEL & (!DISC2_a3_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a3_a_a5287)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "535F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DISC1_a3_a_acombout,
	datab => WE_ADR_DISC2_DEL,
	datac => WE_ADR_DISC1_DEL,
	datad => DISC2_a3_a_acombout,
	cascin => DAC_data_a3_a_a5287,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a3_a_a5263);

EVCH_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(2),
	combout => EVCH_a2_a_acombout);

BIT_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(2),
	combout => BIT_a2_a_acombout);

DAC_data_a2_a_a5228_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a2_a_a5228 = WE_ADR_EVCH_DEL & (EVCH_a2_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & BIT_a2_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EC20",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_BIT_DEL,
	datab => WE_ADR_EVCH_DEL,
	datac => BIT_a2_a_acombout,
	datad => EVCH_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a2_a_a5228);

BIT_DAC_DATA_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(2),
	combout => BIT_DAC_DATA_a2_a_acombout);

DAC_data_a2_a_a5229_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a2_a_a5229 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a2_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a2_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a2_a,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a2_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a2_a_a5229);

DAC_data_a2_a_a5230_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a2_a_a5288 = !DAC_data_a2_a_a5229 & (WE_ADR_DISC2_DEL # WE_ADR_DISC1_DEL # !DAC_data_a2_a_a5228)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0E0F",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datab => WE_ADR_DISC1_DEL,
	datac => DAC_data_a2_a_a5229,
	datad => DAC_data_a2_a_a5228,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a2_a_a5230,
	cascout => DAC_data_a2_a_a5288);

DISC2_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(2),
	combout => DISC2_a2_a_acombout);

DISC1_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(2),
	combout => DISC1_a2_a_acombout);

DAC_data_a2_a_a5264_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a2_a_a5264 = (WE_ADR_DISC1_DEL & (!DISC1_a2_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a2_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a2_a_a5288)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "13DF",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datab => WE_ADR_DISC1_DEL,
	datac => DISC2_a2_a_acombout,
	datad => DISC1_a2_a_acombout,
	cascin => DAC_data_a2_a_a5288,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a2_a_a5264);

EVCH_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(1),
	combout => EVCH_a1_a_acombout);

BIT_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(1),
	combout => BIT_a1_a_acombout);

DAC_data_a1_a_a5235_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a1_a_a5235 = WE_ADR_EVCH_DEL & (EVCH_a1_a_acombout) # !WE_ADR_EVCH_DEL & WE_ADR_BIT_DEL & BIT_a1_a_acombout

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EC20",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_BIT_DEL,
	datab => WE_ADR_EVCH_DEL,
	datac => BIT_a1_a_acombout,
	datad => EVCH_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a1_a_a5235);

BIT_DAC_DATA_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(1),
	combout => BIT_DAC_DATA_a1_a_acombout);

DAC_data_a1_a_a5236_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a1_a_a5236 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a1_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "A820",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5142,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a1_a,
	datad => BIT_DAC_DATA_a1_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a1_a_a5236);

DAC_data_a1_a_a5237_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a1_a_a5289 = !DAC_data_a1_a_a5236 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a1_a_a5235)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3233",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => DAC_data_a1_a_a5236,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a1_a_a5235,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a1_a_a5237,
	cascout => DAC_data_a1_a_a5289);

DISC2_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(1),
	combout => DISC2_a1_a_acombout);

DISC1_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(1),
	combout => DISC1_a1_a_acombout);

DAC_data_a1_a_a5265_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a1_a_a5265 = (WE_ADR_DISC1_DEL & (!DISC1_a1_a_acombout) # !WE_ADR_DISC1_DEL & (!DISC2_a1_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a1_a_a5289)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1B5F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC1_DEL,
	datab => WE_ADR_DISC2_DEL,
	datac => DISC1_a1_a_acombout,
	datad => DISC2_a1_a_acombout,
	cascin => DAC_data_a1_a_a5289,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a1_a_a5265);

EVCH_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_EVCH(0),
	combout => EVCH_a0_a_acombout);

BIT_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT(0),
	combout => BIT_a0_a_acombout);

DAC_data_a0_a_a5239_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a_a5239 = WE_ADR_EVCH_DEL & EVCH_a0_a_acombout # !WE_ADR_EVCH_DEL & (WE_ADR_BIT_DEL & BIT_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "B888",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => EVCH_a0_a_acombout,
	datab => WE_ADR_EVCH_DEL,
	datac => WE_ADR_BIT_DEL,
	datad => BIT_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a0_a_a5239);

BIT_DAC_DATA_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_DATA(0),
	combout => BIT_DAC_DATA_a0_a_acombout);

DAC_data_a0_a_a5240_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a_a5240 = DAC_data_a0_a_a5142 & (BIT_DAC_LD_DEL & (BIT_DAC_DATA_a0_a_acombout) # !BIT_DAC_LD_DEL & DAC_data_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "E020",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a,
	datab => BIT_DAC_LD_DEL,
	datac => DAC_data_a0_a_a5142,
	datad => BIT_DAC_DATA_a0_a_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a0_a_a5240);

DAC_data_a0_a_a5241_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a_a5290 = !DAC_data_a0_a_a5240 & (WE_ADR_DISC1_DEL # WE_ADR_DISC2_DEL # !DAC_data_a0_a_a5239)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5455",
	operation_mode => "normal",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DAC_data_a0_a_a5240,
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_DISC2_DEL,
	datad => DAC_data_a0_a_a5239,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a0_a_a5241,
	cascout => DAC_data_a0_a_a5290);

DISC2_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC2(0),
	combout => DISC2_a0_a_acombout);

DISC1_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_DISC1(0),
	combout => DISC1_a0_a_acombout);

DAC_data_a0_a_a5266_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a_a5266 = (WE_ADR_DISC1_DEL & !DISC1_a0_a_acombout # !WE_ADR_DISC1_DEL & (!DISC2_a0_a_acombout # !WE_ADR_DISC2_DEL)) & CASCADE(DAC_data_a0_a_a5290)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "4777",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => DISC1_a0_a_acombout,
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_DISC2_DEL,
	datad => DISC2_a0_a_acombout,
	cascin => DAC_data_a0_a_a5290,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a0_a_a5266);

BIT_DAC_LD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_BIT_DAC_LD,
	combout => BIT_DAC_LD_acombout);

CLK20_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CLK20,
	combout => CLK20_acombout);

IMR_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_IMR,
	combout => IMR_acombout);

BIT_DAC_LD_DEL_aI : apex20ke_lcell
-- Equation(s):
-- BIT_DAC_LD_DEL = DFFE(BIT_DAC_LD_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => BIT_DAC_LD_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => BIT_DAC_LD_DEL);

WE_ADR_DISC1_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_ADR_DISC1,
	combout => WE_ADR_DISC1_acombout);

WE_ADR_DISC1_DEL_aI : apex20ke_lcell
-- Equation(s):
-- WE_ADR_DISC1_DEL = DFFE(WE_ADR_DISC1_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => WE_ADR_DISC1_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_ADR_DISC1_DEL);

WE_ADR_BIT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_ADR_BIT,
	combout => WE_ADR_BIT_acombout);

WE_ADR_BIT_DEL_aI : apex20ke_lcell
-- Equation(s):
-- WE_ADR_BIT_DEL = DFFE(WE_ADR_BIT_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => WE_ADR_BIT_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_ADR_BIT_DEL);

WE_ADR_EVCH_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WE_ADR_EVCH,
	combout => WE_ADR_EVCH_acombout);

WE_ADR_EVCH_DEL_aI : apex20ke_lcell
-- Equation(s):
-- WE_ADR_EVCH_DEL = DFFE(WE_ADR_EVCH_acombout, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => WE_ADR_EVCH_acombout,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => WE_ADR_EVCH_DEL);

DAC_data_a0_a_a5142_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a0_a_a5142 = !WE_ADR_DISC2_DEL & !WE_ADR_DISC1_DEL & !WE_ADR_BIT_DEL & !WE_ADR_EVCH_DEL

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datab => WE_ADR_DISC1_DEL,
	datac => WE_ADR_BIT_DEL,
	datad => WE_ADR_EVCH_DEL,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a0_a_a5142);

DAC_LD_aI : apex20ke_lcell
-- Equation(s):
-- DAC_LD = DFFE(BIT_DAC_LD_DEL # !DAC_data_a0_a_a5142, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0FF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => BIT_DAC_LD_DEL,
	datad => DAC_data_a0_a_a5142,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_LD);

U_DAC8420_asr_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_asr_cnt_a0_a = DFFE(GLOBAL(U_DAC8420_aDA_BUSY) & U_DAC8420_aEqual_a88 $ U_DAC8420_asr_cnt_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_DAC8420_asr_cnt_a0_a_a55 = CARRY(U_DAC8420_aEqual_a88 & U_DAC8420_asr_cnt_a0_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aEqual_a88,
	datab => U_DAC8420_asr_cnt_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_asr_cnt_a0_a,
	cout => U_DAC8420_asr_cnt_a0_a_a55);

U_DAC8420_asr_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_asr_cnt_a1_a = DFFE(GLOBAL(U_DAC8420_aDA_BUSY) & U_DAC8420_asr_cnt_a1_a $ U_DAC8420_asr_cnt_a0_a_a55, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_DAC8420_asr_cnt_a1_a_a58 = CARRY(!U_DAC8420_asr_cnt_a0_a_a55 # !U_DAC8420_asr_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_asr_cnt_a1_a,
	cin => U_DAC8420_asr_cnt_a0_a_a55,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_asr_cnt_a1_a,
	cout => U_DAC8420_asr_cnt_a1_a_a58);

U_DAC8420_asr_cnt_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_asr_cnt_a2_a = DFFE(GLOBAL(U_DAC8420_aDA_BUSY) & U_DAC8420_asr_cnt_a2_a $ !U_DAC8420_asr_cnt_a1_a_a58, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_DAC8420_asr_cnt_a2_a_a49 = CARRY(U_DAC8420_asr_cnt_a2_a & !U_DAC8420_asr_cnt_a1_a_a58)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_asr_cnt_a2_a,
	cin => U_DAC8420_asr_cnt_a1_a_a58,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_asr_cnt_a2_a,
	cout => U_DAC8420_asr_cnt_a2_a_a49);

U_DAC8420_asr_cnt_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_asr_cnt_a3_a = DFFE(GLOBAL(U_DAC8420_aDA_BUSY) & U_DAC8420_asr_cnt_a3_a $ (U_DAC8420_asr_cnt_a2_a_a49), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )
-- U_DAC8420_asr_cnt_a3_a_a52 = CARRY(!U_DAC8420_asr_cnt_a2_a_a49 # !U_DAC8420_asr_cnt_a3_a)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5A5F",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_asr_cnt_a3_a,
	cin => U_DAC8420_asr_cnt_a2_a_a49,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_asr_cnt_a3_a,
	cout => U_DAC8420_asr_cnt_a3_a_a52);

U_DAC8420_asr_cnt_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_asr_cnt_a4_a = DFFE(GLOBAL(U_DAC8420_aDA_BUSY) & U_DAC8420_asr_cnt_a3_a_a52 $ !U_DAC8420_asr_cnt_a4_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_DAC8420_asr_cnt_a4_a,
	cin => U_DAC8420_asr_cnt_a3_a_a52,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	sclr => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_asr_cnt_a4_a);

U_DAC8420_aEqual_a86_I : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aEqual_a86 = !U_DAC8420_asr_cnt_a3_a & !U_DAC8420_asr_cnt_a2_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "000F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_DAC8420_asr_cnt_a3_a,
	datad => U_DAC8420_asr_cnt_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DAC8420_aEqual_a86);

U_DAC8420_aiDA_UPD_a0_a_a35_I : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aiDA_UPD_a0_a_a35 = !U_DAC8420_aDA_BUSY & !U_DAC8420_aiDA_UPD_a0_a & !U_DAC8420_aiDA_UPD_a1_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0003",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aiDA_UPD_a0_a,
	datad => U_DAC8420_aiDA_UPD_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DAC8420_aiDA_UPD_a0_a_a35);

U_DAC8420_aclk_cnt_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aclk_cnt_a1_a = DFFE(U_DAC8420_aiDA_UPD_a0_a_a35 # U_DAC8420_aclk_cnt_a0_a $ U_DAC8420_aclk_cnt_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF5A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aclk_cnt_a0_a,
	datac => U_DAC8420_aclk_cnt_a1_a,
	datad => U_DAC8420_aiDA_UPD_a0_a_a35,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aclk_cnt_a1_a);

U_DAC8420_aEqual_a88_I : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aEqual_a88 = U_DAC8420_aclk_cnt_a0_a & (U_DAC8420_aclk_cnt_a1_a)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CC00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_aclk_cnt_a0_a,
	datad => U_DAC8420_aclk_cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DAC8420_aEqual_a88);

U_DAC8420_aiDA_UPD_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aiDA_UPD_a0_a = DFFE(!U_DAC8420_asr_cnt_a0_a & !U_DAC8420_asr_cnt_a1_a & U_DAC8420_asr_cnt_a4_a & U_DAC8420_aEqual_a86, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aEqual_a88)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_asr_cnt_a0_a,
	datab => U_DAC8420_asr_cnt_a1_a,
	datac => U_DAC8420_asr_cnt_a4_a,
	datad => U_DAC8420_aEqual_a86,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aEqual_a88,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aiDA_UPD_a0_a);

U_DAC8420_aiDA_UPD_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aiDA_UPD_a1_a = DFFE(U_DAC8420_aiDA_UPD_a0_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aEqual_a88)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => U_DAC8420_aiDA_UPD_a0_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aEqual_a88,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aiDA_UPD_a1_a);

U_DAC8420_aclk_cnt_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aclk_cnt_a0_a = DFFE(!U_DAC8420_aclk_cnt_a0_a & (U_DAC8420_aiDA_UPD_a0_a # U_DAC8420_aDA_BUSY # U_DAC8420_aiDA_UPD_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F0E",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aiDA_UPD_a0_a,
	datab => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aclk_cnt_a0_a,
	datad => U_DAC8420_aiDA_UPD_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aclk_cnt_a0_a);

U_DAC8420_aSR_REG_a15_a_a627_I : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a15_a_a627 = U_DAC8420_aclk_cnt_a0_a & U_DAC8420_aclk_cnt_a1_a # !U_DAC8420_aDA_BUSY

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CF0F",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => U_DAC8420_aclk_cnt_a0_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aclk_cnt_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DAC8420_aSR_REG_a15_a_a627);

U_DAC8420_aEqual_a87_I : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aEqual_a87 = !U_DAC8420_asr_cnt_a0_a & !U_DAC8420_asr_cnt_a1_a & U_DAC8420_asr_cnt_a4_a & U_DAC8420_aEqual_a86

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1000",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_asr_cnt_a0_a,
	datab => U_DAC8420_asr_cnt_a1_a,
	datac => U_DAC8420_asr_cnt_a4_a,
	datad => U_DAC8420_aEqual_a86,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => U_DAC8420_aEqual_a87);

U_DAC8420_aDA_BUSY_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aDA_BUSY = DFFE(DAC_LD # U_DAC8420_aDA_BUSY & !U_DAC8420_aEqual_a87 # !U_DAC8420_aSR_REG_a15_a_a627, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CFEF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aDA_BUSY,
	datab => DAC_LD,
	datac => U_DAC8420_aSR_REG_a15_a_a627,
	datad => U_DAC8420_aEqual_a87,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aDA_BUSY);

U_DAC8420_aDA_CLK_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aDA_CLK = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aclk_cnt_a0_a & U_DAC8420_aclk_cnt_a1_a & !U_DAC8420_aEqual_a87 # !U_DAC8420_aclk_cnt_a0_a & !U_DAC8420_aclk_cnt_a1_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0484",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aclk_cnt_a0_a,
	datab => U_DAC8420_aDA_BUSY,
	datac => U_DAC8420_aclk_cnt_a1_a,
	datad => U_DAC8420_aEqual_a87,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aDA_CLK);

DAC_data_a15_a_a5143_I : apex20ke_lcell
-- Equation(s):
-- DAC_data_a15_a_a5143 = BIT_DAC_LD_DEL # WE_ADR_BIT_DEL # WE_ADR_EVCH_DEL # DAC_data_a15_a

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FFFE",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => BIT_DAC_LD_DEL,
	datab => WE_ADR_BIT_DEL,
	datac => WE_ADR_EVCH_DEL,
	datad => DAC_data_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => DAC_data_a15_a_a5143);

DAC_data_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- DAC_data_a15_a = DFFE(!WE_ADR_DISC2_DEL & (DAC_data_a15_a_a5143 & !WE_ADR_DISC1_DEL), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0050",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => WE_ADR_DISC2_DEL,
	datac => DAC_data_a15_a_a5143,
	datad => WE_ADR_DISC1_DEL,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => DAC_data_a15_a);

U_DAC8420_aSR_REG_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aSR_REG_a15_a = DFFE(U_DAC8420_aDA_BUSY & U_DAC8420_aSR_REG_a14_a # !U_DAC8420_aDA_BUSY & (DAC_data_a15_a), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , U_DAC8420_aSR_REG_a15_a_a627)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "AFA0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a14_a,
	datac => U_DAC8420_aDA_BUSY,
	datad => DAC_data_a15_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	ena => U_DAC8420_aSR_REG_a15_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aSR_REG_a15_a);

U_DAC8420_aDA_DOUT_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aDA_DOUT = DFFE(U_DAC8420_aDA_BUSY & (U_DAC8420_aEqual_a88 & U_DAC8420_aSR_REG_a15_a # !U_DAC8420_aEqual_a88 & (U_DAC8420_aDA_DOUT)) # !U_DAC8420_aDA_BUSY & (U_DAC8420_aDA_DOUT), GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "ACCC",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => U_DAC8420_aSR_REG_a15_a,
	datab => U_DAC8420_aDA_DOUT,
	datac => U_DAC8420_aDA_BUSY,
	datad => U_DAC8420_aEqual_a88,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aDA_DOUT);

U_DAC8420_aDA_UPD_aI : apex20ke_lcell
-- Equation(s):
-- U_DAC8420_aDA_UPD = DFFE(U_DAC8420_aiDA_UPD_a1_a, GLOBAL(CLK20_acombout), !GLOBAL(IMR_acombout), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => U_DAC8420_aiDA_UPD_a1_a,
	clk => CLK20_acombout,
	aclr => IMR_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => U_DAC8420_aDA_UPD);

DA_OFS_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_DAC8420_aDA_BUSY,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DA_OFS);

DA_CLK_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_DAC8420_aDA_CLK,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DA_CLK);

DA_DOUT_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => U_DAC8420_aDA_DOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DA_DOUT);

DA_UPD_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => ALT_INV_U_DAC8420_aDA_UPD,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_DA_UPD);
END structure;


