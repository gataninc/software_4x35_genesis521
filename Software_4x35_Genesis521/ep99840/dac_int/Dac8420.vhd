-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	DAC.VHD
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--        Module to interface to the Analog Devices AD8420 DAC
--   History:       <date> - <Author>
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		*********************************************************************
--		September 22, 2000 - MCS:
--			Final Release - EDI-II
--		*********************************************************************
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use LPM.LPM_COMPONENTS.ALL;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;

-------------------------------------------------------------------------------
entity DAC8420 is 
	port( 
	     IMR            : in      std_logic;				     -- Master Reset
     	CLK20          : in      std_logic;                         -- Master Clock
		DAC_LD		: in		std_logic;					-- Start to Load DAC
		DAC_DATA		: in		std_logic_vector( 15 downto 0 );	-- 12-bit DAC Data
		DA_OFS		: buffer	std_logic;
		DA_CLK		: buffer	std_logic;					-- DAC Clock
		DA_DOUT		: buffer	std_logic;					-- DAC Data
		DA_UPD		: buffer	std_logic );					-- DAC Load
	end DAC8420;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of DAC8420 is
     -- Constant Declarations
	constant SR_CNT_MAX 	: integer := 31;

	constant FS_STart 		: integer := 2;	-- Frame-Sync Start
	constant DA_Busyd		: integer := 16;	-- Frame-Sync End

	-- Any Signal Declarations
	signal clk_cnt_tc		: std_logic;
	signal sr_cnt_tc		: std_logic;

	signal DA_BUSY			: std_logic;	
	signal clk_cnt			: std_logic_vector( 1 downto 0 );

	signal sr_cnt			: integer range 0 to sr_cnt_max;

     signal SR_REG			: std_logic_vector( 15 downto 0 );
	signal iDA_UPD			: std_logic_vector( 1 downto 0 );
	
begin
	DA_OFS		<= not( DA_BUSY );

	clk_cnt_tc 	<= '1' when ( clk_cnt = "11" ) else '0';
	sr_cnt_tc  	<= '1' when ( SR_CNT = DA_BUSYd ) else '0';

	
	--------------------------------------------------------------------------
     UPD_PROC : process( CLK20, IMR )
     begin
          if( IMR = '1' ) then
			DA_BUSY		<= '0';
			clk_cnt		<= "00";
			DA_CLK		<= '1';
			SR_CNT		<= 0;
               SR_REG         <= x"0000";
			DA_Dout		<= '0';
			DA_UPD		<= '1';
			iDA_UPD		<= "00";
          elsif(( CLK20'Event ) and ( CLK20 = '1' )) then
			if( dac_ld = '1' )
				then DA_BUSY <= '1';
			elsif(( clk_cnt_tc = '1' ) and ( sr_cnt_tc = '1' ))
				then DA_BUSY <= '0';
			end if;

			if(( DA_BUSY = '0' ) and ( iDA_UPD = "00" ))
				then clk_cnt <= "10";
				else clk_cnt <= clk_cnt + 1;
			end if;

			if(( clk_cnt_tc = '1' ) and ( sr_Cnt_tc = '1' ))
				then DA_CLK <= '1';
			elsif(( DA_BUSY = '1' ) and ( clK_cnt = "11" ))
				then DA_CLK <= '0';
			elsif(( DA_BUSY = '1' ) and  ( clk_cnt = "00" ))
				then DA_CLK <= '0';
				else DA_CLK <= '1';
			end if;
			
			if( DA_BUSY = '0' )
				then SR_CNT <= 0;
			elsif( clk_cnt_tc = '1' ) 
				then sr_cnt <= sr_cnt + 1;
			end if;
			
			if( DA_BUSY = '0' )
				then SR_REG <= DAC_DATA;
			elsif( clk_cnt_tc = '1' )
				then sr_reg <= sr_reg( 14 downto 0 ) & '0';
			end if;
		
			if(( DA_BUSY = '1' ) and ( clk_cnt_tc = '1' ))
				then DA_Dout <= SR_REG(15);
			end if;
		
			if(( clk_cnt_tc = '1' ) and ( sr_cnt_tc = '1' )) 
				then iDA_UPD(0) <= '1';
			elsif( clk_cnt_tc = '1' )
				then iDA_UPD(0) <= '0';
			end if;
			
			if( clk_cnt_tc = '1' ) 
				then iDA_UPD(1) <= iDA_UPD(0);
			end if;
			
			DA_UPD <= not( iDA_UPD(1) );
		end if;
     end process;
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
end behavioral;			-- DAC8420.VHD
-------------------------------------------------------------------------------

