-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	WDS.VHD 
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  Digital Interface for the UMS Controller
--
--	History
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity ums2_SpdCtrl is 
	port( 
		imr       	: in      std_logic;				     	-- Master Reset
		clk20       	: in      std_logic;                         	-- Master Clock
		ClkEn		: in		std_logic;
		Vin			: in		std_logic_Vector( 15 downto 0 );
		Vout			: buffer	std_logic_Vector( 15 downto 0 ) );
end ums2_SpdCtrl;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture schematic of ums2_SpdCtrl is
	Constant SF_LSB 	: integer := 18;	

	signal Vin_Vec		: std_logic_vector( 31 downto 0 );
	signal Vacc		: std_logic_vector( 31 downto 0 );
	signal Vdiff		: std_logic_vector( 31 downto 0 );
	signal Vadd		: std_logic_vector( 31 downto 0 );
	signal vcc		: std_logic;
	signal gnd		: std_Logic;
	
begin
	Vcc		<= '1';
	Gnd		<= '0';
	
	--  2^16 Vin
	Vin_Vec	<= Vin & x"0000";		
	
	-- 2^16 Vin - Vacc
	Vdiff_Sub : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_FF" )
		port map(
			add_sub			=> gnd,				-- Subtract
			cin				=> vcc,
			dataa			=> Vin_Vec,
			datab			=> VAcc,
			result			=> Vdiff );

	-- ( 2^16 Vin - Vacc ) / 2^SF_LSB
	VAdd_Gen : for i in SF_LSB to 31 generate
		VAdd(i)	<= Vdiff(31);
	end generate;
	VAdd( SF_LSB-1 downto 0 ) <= Vdiff( 31 downto 32-SF_LSB );

	-- VAcc = Vacc + ( 2^16 Vin - Vacc )/2^SF_LSB
	VAcc_Add : lpm_add_Sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_FF",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			clken			=> ClkEn,
			add_sub			=> Vcc,		-- Add
			cin				=> gnd,
			dataa			=> VAcc,
			datab			=> VAdd,
			result			=> VAcc );
			
	-- Vout = Vacc / 2^16
	Vout_FF : lpm_Ff
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> Vacc( 31 downto 16 ),
			q				=> Vout );
----------------------------------------------------------------------------------------------------
end Schematic;			-- SpdCtrl.VHD
----------------------------------------------------------------------------------------------------

