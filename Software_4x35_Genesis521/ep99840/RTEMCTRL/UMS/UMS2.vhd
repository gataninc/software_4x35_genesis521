-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	UMS2.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  Digital Interface for the UMS Controller
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		November 2, 2000 - MCS
--			Changed Version number ot 0x8332 to correspond to the Rev 2 Copper
--			relase version.
--		September 21, 2000 - MCS
--			Updated for the SEM requirement of the UMS
--			DMC_IN bus has been redefined as:
--			DMC_IN[0] = Interrupt ( Active Low )
--			DMC_IN[7:1] = Encoded Value where:
--				00 = Analyze
--				7F = Retract
--				01-7D moves to some intermediate point.
--
--		TODO
--			with Rev 2 Copper, add the Additional DIP switch inputs to support the 
--			3rd button.
--		March 24, 2000 - MCS
--			Developemnt completed for UMS and RTEM for both manual and computer control
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
-------------------------------------------------------------------------------
entity UMS2 is 
	port( 
		imr       	: in      std_logic;				     	-- Master Reset
		clk20       	: in      std_logic;                         	-- Master Clock
		ms100_Tc		: in		std_logic;
		CNT_100		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		CNT_MAX		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
	    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
		Det_Sat		: in		std_logic;
		CMD_IN		: in		std_Logic;						-- COmmand to Move In
		CMD_OUT		: in		std_logic;						-- Command to Move Out
		CMD_MID_IN	: in		std_logic;
		CMD_MID_OUT	: in		std_logic;
		CMD_WD		: in		std_logic;
		HC_MR		: in		std_logic;						-- High Count Master Reset
		RTEM_RED		: in		std_logic;						-- Analyze Switch Closure/Limit Switch
		RTEM_GRN		: in		std_logic;						-- Retract Switch Closure
		UMS_DIR		: buffer	std_logic;						-- Direction
		UMS_SCLK		: buffer	std_logic;						-- Steps
		UMS_CNT		: buffer	std_logic_Vector( 31 downto 0 );
		ST_RTEM   	: buffer	std_logic_vector(  3 downto 0 )); 		-- RTEM State
end UMS2;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of UMS2 is
	constant Hi_Cnt_Delay_Max : integer := 49;		-- 5 seconds @ 100ms

	constant STR_UMS_INIT		: std_logic_Vector( 3 downto 0 ) := x"0";
	constant STR_UMS_ERROR		: std_logic_Vector( 3 downto 0 ) := x"1";
	constant STR_UMS_STOPPED		: std_logic_Vector( 3 downto 0 ) := x"2";
     constant STR_UMS_IN     		: std_logic_vector( 3 downto 0 ) := x"3";
     constant STR_UMS_OUT    		: std_logic_vector( 3 downto 0 ) := x"4";
	constant STR_UMS_SEM		: std_logic_Vector( 3 downto 0 ) := x"5";
	constant STR_UMS_WD			: std_logic_Vector( 3 downto 0 ) := x"6";
     constant STR_UMS_MV_IN  		: std_logic_vector( 3 downto 0 ) := x"7";
     constant STR_UMS_MV_OUT 		: std_logic_vector( 3 downto 0 ) := x"8";
	constant STR_UMS_MV_SEM_IN	: std_logic_Vector( 3 downto 0 ) := x"9";
	constant STR_UMS_MV_SEM_OUT	: std_logic_Vector( 3 downto 0 ) := x"A";
	constant STR_UMS_MV_WD		: std_logic_Vector( 3 downto 0 ) := x"B";
	constant STR_UMS_MV_ERROR	: std_logic_Vector( 3 downto 0 ) := x"C";
     constant STR_UMS_NO_POW 		: std_logic_vector( 3 downto 0 ) := x"D";
     constant STR_UMS_HC     		: std_logic_vector( 3 downto 0 ) := x"E";
	
	constant ns250_cnt_max	: integer := 4;

	constant init_delay_max 	: integer := 19999;

	constant mmHome		: integer := 300000;	-- 5mm * 1/25.4 * 1/0.05 * 12800 pul/rev
	constant mm5			: integer := 50000;	-- 5mm * 1/25.4 * 1/0.05 * 12800 pul/rev
	constant Lo_Speed 		: integer := 127;
	constant Hi_Speed		: integer := 23;

	-- Any Type Declarations
	type UMS_TYPE is (
		ST_INIT,
		ST_ERROR,
		ST_STOPPED,
		ST_ANALYZE,
		ST_RETRACT,
		ST_SEM,
		ST_WD,
		ST_MOVE_ANALYZE,
		ST_MOVE_RETRACT,
		ST_MOVE_SEM_in,
		ST_MOVE_SEM_out,
		ST_MOVE_WD,
		ST_MOVE_ERROR,
		ST_NO_POWER );

     -- Any Signal Declarations
	signal UMS_STATE		: UMS_TYPE;

	signal init_delay_cnt	: integer range 0 to init_delay_max;
	signal init_delay_tc	: std_logic;

		
	signal CMD_ERROR		: std_logic;
	signal HI_Cnt_En		: std_logic;
	
	signal SW_IN_DB		: std_logic;
	signal SW_OUT_DB		: std_logic;

	signal ns250_cnt		: integer range 0 to ns250_Cnt_max;
	signal ns250_tc		: std_logic;
	signal step_cnt		: std_logic_vector( 15 downto 0 );
	signal step_tc			: std_logic;
	signal Speed_Mux		: std_logic_Vector( 15 downto 0 );
	signal Velocity		: std_logic_Vector( 15 downto 0 );

	signal CNT_CMP_75		: std_logic;
	signal CNT_CMP_100		: std_logic;
	signal CNT_CMP_110		: std_logic;
	signal CNT_CMP_MAX		: std_logic;
	signal Cnt_Cmp_Home		: std_logic;
	signal cnt_100_reg		: std_logic_vector( 31 downto 0 );

	signal UMS2_Enable_Vec 	: std_logic_vector( 1 downto 0 );
	signal Hi_Cnt_Delay 	: std_logic;
	signal Hi_Cnt_Delay_tc	: std_logic;
	signal hi_cnt_delay_cnt	: integer range 0 to Hi_Cnt_delay_max;

	
	signal UMS2_ENABLE		: std_logic;	
	signal RTEM_MOVE_IN		: std_logic;
	signal RTEM_IN_VEC 		: std_logic_Vector( 3 downto 0 );
	signal RTEM_OUT_VEC 	: std_logic_Vector( 3 downto 0 );

	signal CMD_CNT			: std_logic_Vector( 31 downto 0 );
	signal CNT_75			: std_logic_Vector( 31 downto 0 );		-- 75% Cnt Value
	signal CNT_110			: std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
	signal Vel_Cen			: std_logic;
	signal Home_En			: std_logic;
	-------------------------------------------------------------------------------
	component ums2_SpdCtrl is 
		port( 
			imr       	: in      std_logic;				     	-- Master Reset
			clk20       	: in      std_logic;                         	-- Master Clock
			ClkEn		: in		std_logic;
			Vin			: in		std_logic_Vector( 15 downto 0 );
			Vout			: buffer	std_logic_Vector( 15 downto 0 ) );
	end component ums2_SpdCtrl;
	-------------------------------------------------------------------------------

	
begin
	CMD_ERROR		<= '1' when (( UMS_STATE /= ST_ERROR ) and ( HI_Cnt_En = '1' )) else '0';

	ns250_tc		<= '1' when ( ns250_cnt  = ns250_cnt_max ) else '0';
	step_tc		<= '1' when ( step_cnt 	= Velocity	 ) else '0';
	init_delay_tc	<= '1' when ( init_delay_Cnt = init_delay_max ) else '0';

	RTEM_MOVE_IN	<= '1' when (  UMS_STATE = ST_MOVE_ANALYZE ) else 
				   '1' when (  UMS_STATE = ST_MOVE_SEM_IN  ) else
				   '0';

	hi_Cnt_Delay_tc <= '1' when ( hi_cnt_delay_cnt = hi_cnt_delay_max ) else '0';

	-------------------------------------------------------------------------------

	CNT_100_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> cnt_100,
			q				=> cnt_100_reg );
	-------------------------------------------------------------------------------
	-- Position Generators 
	Cnt_75_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '1',
			dataa			=> cnt_100_reg,
			datab			=> conv_std_logic_Vector( mm5, 32 ),
			result			=> Cnt_75 );

	Cnt_100_Add : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> '0',
			dataa			=> cnt_100_reg,
			datab			=> conv_std_logic_Vector( mm5, 32 ),
			result			=> Cnt_110 );
	-- Position Generators 
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Position Comparitors		
	PCT75_CMP : LPM_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> CMD_CNT,
			datab			=> CNT_75,
			agb				=> CNT_CMP_75 );
			
	-- Used for Move In
	PCT100_CMP : LPM_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> CMD_CNT,
			datab			=> cnt_100_reg,
			agb				=> CNT_CMP_100 );
	
	-- Used for Move Out 		
	PCT110_CMP : LPM_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> CMD_CNT,
			datab			=> CNT_110,
			agb				=> CNT_CMP_110 );

	-- Used for Move Error
	PCTMAX_CMP : LPM_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> CMD_CNT,
			datab			=> CNT_MAX,
			agb				=> CNT_CMP_MAX );
	-- Position Comparitors
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Decode Move Home when CNT_100 = 0x7FFF,FFFF
	-- and force a "Home Mode, to change to the low speed after 300,000 counts
	HOME_CMP : LPM_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_COMPARE" )
		port map(
			dataa			=> CMD_CNT,
			datab			=> conv_std_logic_vector( mmHome, 32 ),
			agb				=> CNT_CMP_Home );

	Home_En	<= '1' when ( cnt_100_reg = x"7FFFFFFF" ) else '0';	
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	Speed_Mux		<= Conv_Std_logic_Vector( Lo_Speed, 16 ) when (( Home_en = '1' ) and ( CNT_CMP_Home = '1' )) else
				   Conv_Std_logic_Vector( Hi_Speed, 16 ) when (( UMS2_ENABLE = '1' ) and ( CNT_CMP_75 = '0' )) else
				   Conv_Std_logic_Vector( Lo_Speed, 16 );

	Vel_Cen	<= '1' when (( ns250_tc = '1' ) and ( Step_tc = '1' )) else '0';			

	U_SPDCTRL : ums2_SpdCtrl 
		port map(
			imr		=> imr,
			clk20	=> clk20,
			ClkEn	=> Vel_Cen,
			Vin		=> Speed_Mux,
			Vout		=> Velocity ); 
				
	-------------------------------------------------------------------------------
	-- Enable to the Motor, as well as letting the step pulses out
	with UMS_STATE select
		UMS2_ENABLE <= '1' when ST_MOVE_ANALYZE,
		               '1' when ST_MOVE_RETRACT,
		               '1' when ST_MOVE_ERROR,
					'1' when ST_MOVE_SEM_IN,
					'1' when ST_MOVE_SEM_OUT,
					'1' when ST_MOVE_WD,
				     '0' when others;
	-- Enable to the Motor, as well as letting the step pulses out
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	clk20_proc : process( IMR, clk20 )
	begin
		if( IMR = '1' ) then
			ns250_cnt		<= 0;
			HI_CNT_DELAY 	<= '0';
			UMS2_Enable_Vec <= "00";
			init_delay_cnt <= 0;
			step_cnt		<= x"0000";
			UMS_SCLK		<= '0';
			Hi_Cnt_En      <= '0';
			ST_RTEM		<= STR_UMS_INIT;
			UMS_STATE		<= ST_INIT;
			UMS_CNT		<= x"00000000";
			Sw_In_Db		<= '0';
			Sw_Out_Db		<= '0';
			UMS_DIR		<= '0';
			RTEM_IN_VEC 	<= x"0";
			RTEM_OUT_VEC	<= x"0";
			
		elsif(( clk20'Event ) and ( clk20 = '1' )) then
			RTEM_IN_VEC  <= RTEM_IN_VEC(  2 downto 0 ) & RTEM_GRN; -- Motor is In
			RTEM_OUT_VEC <= RTEM_OUT_VEC( 2 downto 0 ) & RTEM_RED; -- Motor is Out

			if( RTEM_IN_VEC = x"F" )
				then SW_IN_DB <= '1';
				else SW_IN_DB <= '0';
			end if;

			if( RTEM_OUT_VEC = x"F" )
				then SW_OUT_DB <= '1';
				else SW_OUT_DB <= '0';
			end if;
			
			--------------------------------------------------------------------
			-- Step Clock Generation
			-- 250 ns timer - time-base for 2Mhz Max Step Clock
			if( ns250_tc = '1' )
				then ns250_cnt <= 0;
				else ns250_cnt <= ns250_cnt + 1;
			end if;
			if( ns250_tc = '1' ) then
				if( step_tc = '1' )
					then Step_cnt <= x"0000";
					else Step_cnt <= step_cnt + 1;
				end if;
			end if;

			-- Make Step Cloc a Square Wave
			if( UMS2_ENABLE = '0' )
				then UMS_SCLK <= '0';
			elsif(( ns250_tc = '1' ) and ( step_tc = '1' ))
				then UMS_SCLK <= not( UMS_SCLK );
			end if;

			-- Step Clock Generation
			--------------------------------------------------------------------
			
			--------------------------------------------------------------------
			-- Direction 0 = Move In, 1 = Move Out
			case UMS_STATE is									-- Motor In
				when ST_ANALYZE		=> UMS_DIR <= '0';
				when ST_MOVE_ANALYZE	=> UMS_DIR <= '0';
				when ST_MOVE_SEM_IN		=> UMS_DIR <= '0';
				when others			=> UMS_DIR <= '1';
			end case;
			-- Direction 
			--------------------------------------------------------------------

			if(   ( UMS2_ENABLE = '1' ) and ( RTEM_MOVE_IN = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
				then UMS_CNT <= UMS_CNT + 1;
			elsif(( UMS2_ENABLE = '1' ) and ( RTEM_MOVE_IN = '0' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ) and ( SW_OUT_DB = '0' ))
				then UMS_CNT <= UMS_CNT - 1;
			elsif(( UMS2_ENABLE = '1' ) and ( RTEM_MOVE_IN = '0' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ) and ( SW_OUT_DB = '1' ))
				then UMS_CNT <= x"00000000";
			end if;
			
			if(( CMD_IN = '1' ) or ( CMD_OUT = '1' ) or ( CMD_ERROR = '1' ) or ( UMS2_ENABLE = '0' ))
				then CMD_CNT <= x"00000000";
			elsif(( UMS2_ENABLE = '1' ) and ( ns250_tc = '1' ) and ( Step_tc = '1' ) and ( UMS_SCLK = '0' ))
				then CMD_CNT <= CMD_CNT + 1;
			end if;
			-- Step Clock Generation
			--------------------------------------------------------------------

			if(( UMS_STATE = ST_INIT ) and ( Init_Delay_tc = '0' ))
				then init_Delay_cnt <= init_delay_Cnt + 1;
				else init_Delay_Cnt <= 0;
			end if;

			-- State String Output --------------------------------------------
			-- RED == ANALYZE
			-- GRN == RETRACT
			if( Hi_Cnt_En = '1' ) then 
				ST_RTEM <= STR_UMS_HC;
			else
				case UMS_STATE is
					when ST_INIT			=> ST_RTEM <= STR_UMS_INIT;
					when ST_ERROR			=> ST_RTEM <= STR_UMS_ERROR;
					when ST_STOPPED		=> ST_RTEM <= STR_UMS_STOPPED;
					when ST_ANALYZE		=> ST_RTEM <= STR_UMS_IN;
					when ST_RETRACT		=> ST_RTEM <= STR_UMS_OUT;
					when ST_SEM			=> ST_RTEM <= STR_UMS_SEM;
					when ST_WD			=> ST_RTEM <= STR_UMS_WD;
					when ST_MOVE_ANALYZE	=> ST_RTEM <= STR_UMS_MV_IN;
					when ST_MOVE_RETRACT	=> ST_RTEM <= STR_UMS_MV_OUT;
					when ST_MOVE_SEM_IN		=> ST_RTEM <= STR_UMS_MV_SEM_IN;
					when ST_MOVE_SEM_OUT	=> ST_RTEM <= STR_UMS_MV_SEM_OUT;
					when ST_MOVE_WD		=> ST_RTEM <= STR_UMS_MV_WD;
					when ST_MOVE_ERROR		=> ST_RTEM <= STR_UMS_MV_OUT;
					when ST_NO_POWER		=> ST_RTEM <= STR_UMS_NO_POW;
				end case;
			end if;
			-- State String Output --------------------------------------------
			
	
			-- Form a Vector of UMS2_Enable ( When the slide is moving )
			UMS2_Enable_Vec <= UMS2_Enable_Vec(0) & UMS2_Enable;

			-- When the Slide stops moving, start the Hi_cnt_delay to ignore input count rate
			if( UMS2_Enable_Vec = "10" )
				then Hi_Cnt_Delay <= '1';
			elsif(( MS100_TC = '1' ) and ( Hi_Cnt_Delay_Tc = '1' ))
				then Hi_Cnt_Delay <= '0';
			end if;

			if( Hi_Cnt_delay = '0' )
				then hi_cnt_delay_cnt <= 0;
			elsif(( ms100_tc = '1' ) and ( hi_cnt_Delay_tc = '0' ))
				then hi_cnt_delay_cnt <= hi_cnt_delay_cnt + 1;
			end if;				
	
			-- If NOT Moving and Hi_Count or Detector Saturated
               if(( UMS2_ENABLE = '0' ) and ( Hi_Cnt_Delay = '0' ) and ( hi_cnt = '1' ))
                    then Hi_Cnt_En <= '1';
			elsif(( UMS2_ENABLE = '0' ) and ( Hi_Cnt_Delay = '0' ) and ( Det_Sat = '1' ))
				then Hi_Cnt_En <= '1';
               elsif(( hc_mr = '1' ) or ( Cmd_Out = '1' ) or ( Cmd_In = '1' ) or ( CMD_MID_IN = '1' ) or ( CMD_MID_OUT = '1' ))
                   then Hi_Cnt_En <= '0';
               end if;

			case UMS_STATE is
				when ST_INIT 			=>
					if( init_delay_tc = '1' ) then
						if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
							then UMS_STATE <= St_No_Power;
						elsif(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '0' ))		-- In Position Detected
							then UMS_STATE <= ST_ANALYZE;
						elsif(( SW_IN_DB = '0' ) and ( SW_OUT_DB = '1' ))		-- Out Position Detected
							then UMS_STATE <= ST_RETRACT;
							else UMS_STATE <= ST_STOPPED;
						end if;
					end if;

				when ST_NO_POWER		=>
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '0' ))		-- In Position Detected
						then UMS_STATE <= ST_ANALYZE;		
					elsif(( SW_IN_DB = '0' ) and ( SW_OUT_DB = '1' ))		-- Out Position Detected
						then UMS_STATE <= ST_RETRACT;
						else UMS_STATE <= ST_STOPPED;
					end if;
				
				--------------------------------------------------------------------
				-- Static States
				when ST_ERROR			=>
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( Cmd_In = '1' )and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_ANALYZE;
					elsif(( CMD_MID_IN = '1' ) and ( SW_IN_DB = '0' ))					-- Not on Analyze Limit Switch
						then UMS_STATE <= ST_MOVE_SEM_IN;
					elsif(( CMD_MID_OUT = '1' ) and ( SW_OUT_DB = '0' ))					-- Not on Retract Limit Switch
						then UMS_STATE <= ST_MOVE_SEM_OUT;
					elsif(( HC_MR = '1' ) or ( Cmd_Out = '1' ))
						then UMS_STATE <= ST_RETRACT;
					end if;

				when ST_STOPPED		=>
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_OUT = '1' ) and ( SW_OUT_DB = '0' ))						-- Not already on the Limit Switch
						then UMS_STATE <= ST_MOVE_RETRACT;
					elsif(( Hi_Cnt_En = '0' ) and ( CMD_IN = '1' ) and ( SW_IN_DB = '0' ))	-- and not on teh limit switch
						then UMS_STATE <= ST_MOVE_ANALYZE;
					elsif(( CMD_MID_IN = '1' ) and ( SW_IN_DB = '0' ))					-- Not on Analyze Limit Switch
						then UMS_STATE <= ST_MOVE_SEM_IN;
					elsif(( CMD_MID_OUT = '1' ) and ( SW_OUT_DB = '0' ))					-- Not on Retract Limit Switch
						then UMS_STATE <= ST_MOVE_SEM_OUT;
					elsif( CMD_ERROR = '1' ) 
						then UMS_STATE <= ST_MOVE_ERROR;
					elsif(( CMD_WD = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_WD;
					end if;

				-- Analyze ( In ) Position Only react to Out or Error Commands
				when ST_ANALYZE		=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_OUT = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_RETRACT;
					elsif( CMD_ERROR = '1' ) 
						then UMS_STATE <= ST_MOVE_ERROR;
					elsif(( CMD_MID_IN = '1' ) and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_IN;
					elsif(( CMD_MID_OUT = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_OUT;
					elsif(( CMD_WD = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_WD;
					end if;

				-- Retract ( Out ) Position Only react to In or Error Commands
				when ST_RETRACT		=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( Hi_Cnt_En = '0' ) and ( CMD_IN = '1' ) and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_ANALYZE;	 
					elsif(( CMD_MID_IN = '1' ) and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_IN;
					elsif(( CMD_MID_OUT = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_OUT;
					end if;

				-- Retract ( Out ) Position Only react to In or Error Commands
				when ST_SEM		=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( Hi_Cnt_En = '0' ) and ( CMD_IN = '1' ) and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_ANALYZE;	 
					elsif( CMD_ERROR = '1' ) 
						then UMS_STATE <= ST_MOVE_ERROR;
					elsif(( CMD_OUT = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_RETRACT;
					elsif(( CMD_MID_IN = '1' ) and ( SW_IN_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_IN;
					elsif(( CMD_MID_OUT = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_SEM_OUT;
					elsif(( CMD_WD = '1' ) and ( SW_OUT_DB = '0' ))
						then UMS_STATE <= ST_MOVE_WD;
					end if;			
				--------------------------------------------------------------------
						
				--------------------------------------------------------------------
				-- States to wait to get On the appropriate Limit Switch
				when ST_MOVE_ANALYZE	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_OUT = '1' ) or ( CMD_MID_IN = '1' ) or ( CMD_MID_OUT = '1' ))
						then UMS_STATE <= ST_STOPPED;
					elsif(( CNT_CMP_100 = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ANALYZE;
					elsif(( SW_IN_DB = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ANALYZE;
					end if;
						
						
				-- Wait for contact with the Retract Limit Switch
				-- Wait for the Retract Switch to be engaged
				when ST_MOVE_RETRACT	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_IN = '1' ) or ( CMD_MID_IN = '1' ) or ( CMD_MID_OUT = '1' ))
						then UMS_STATE <= ST_STOPPED;
					elsif(( SW_OUT_DB = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_RETRACT;
					elsif(( CNT_CMP_110 = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ERROR;
					end if;

				when ST_MOVE_SEM_IN	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_IN = '1' ) or ( CMD_OUT = '1' ))
						then UMS_STATE <= ST_STOPPED;
					elsif(( SW_IN_DB = '1' )  and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ANALYZE;
					elsif(( CNT_CMP_100 = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_SEM;
					end if;

				when ST_MOVE_SEM_OUT	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( CMD_IN = '1' ) or ( CMD_OUT = '1' ))
						then UMS_STATE <= ST_STOPPED;
					elsif(( SW_OUT_DB = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_RETRACT;
					elsif(( CNT_CMP_100 = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_SEM;
					end if;

				-- Wait for contact with the Retract Limit Switch
				when ST_MOVE_ERROR	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( SW_OUT_DB = '1' )  and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ERROR;
					elsif(( CNT_CMP_MAX = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ERROR;
					end if;

				when ST_MOVE_WD	=> 
					if(( SW_IN_DB = '1' ) and ( SW_OUT_DB = '1' ))		-- aka CMD_NO_POWER
						then UMS_STATE <= St_No_Power;
					elsif(( SW_OUT_DB = '1' )  and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ERROR;
					elsif(( CNT_CMP_MAX = '1' ) and ( ns250_tc = '1' ) and ( step_tc = '1' ) and ( UMS_SCLK = '0' ))
						then UMS_STATE <= ST_ERROR;
					end if;

				--------------------------------------------------------------------
				-- All other States
				when others	=> UMS_STATE <= ST_ERROR;
			end case;
		end if;
	end process;
	-----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
end behavioral;			-- RTEM-II.VHD
----------------------------------------------------------------------------------------------------

