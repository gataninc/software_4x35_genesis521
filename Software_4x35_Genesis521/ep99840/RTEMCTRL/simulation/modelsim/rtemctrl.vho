-- Copyright (C) 1991-2003 Altera Corporation
-- Any  megafunction  design,  and related netlist (encrypted  or  decrypted),
-- support information,  device programming or simulation file,  and any other
-- associated  documentation or information  provided by  Altera  or a partner
-- under  Altera's   Megafunction   Partnership   Program  may  be  used  only
-- to program  PLD  devices (but not masked  PLD  devices) from  Altera.   Any
-- other  use  of such  megafunction  design,  netlist,  support  information,
-- device programming or simulation file,  or any other  related documentation
-- or information  is prohibited  for  any  other purpose,  including, but not
-- limited to  modification,  reverse engineering,  de-compiling, or use  with
-- any other  silicon devices,  unless such use is  explicitly  licensed under
-- a separate agreement with  Altera  or a megafunction partner.  Title to the
-- intellectual property,  including patents,  copyrights,  trademarks,  trade
-- secrets,  or maskworks,  embodied in any such megafunction design, netlist,
-- support  information,  device programming or simulation file,  or any other
-- related documentation or information provided by  Altera  or a megafunction
-- partner, remains with Altera, the megafunction partner, or their respective
-- licensors. No other licenses, including any licenses needed under any third
-- party's intellectual property, are provided herein.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 3.0 Build 199 06/26/2003 SJ Full Version"

-- DATE "07/31/2003 10:45:58"

--
-- Device: Altera EP20K300EFC672-2X Package FBGA672
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL output from Quartus II) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	rtemctrl IS
    PORT (
	RTEM_SEL : IN std_logic;
	clk20 : IN std_logic;
	imr : IN std_logic;
	Low_Speed : IN std_logic_vector(15 DOWNTO 0);
	Med_Speed : IN std_logic_vector(15 DOWNTO 0);
	Hi_Speed : IN std_logic_vector(15 DOWNTO 0);
	Cmd_Mid_In : IN std_logic;
	Hi_Cnt : IN std_logic;
	Det_Sat : IN std_logic;
	CMD_IN : IN std_logic;
	CMD_OUT : IN std_logic;
	Cmd_Mid_Out : IN std_logic;
	CNT_90 : IN std_logic_vector(31 DOWNTO 0);
	CNT_75 : IN std_logic_vector(31 DOWNTO 0);
	CNT_25 : IN std_logic_vector(31 DOWNTO 0);
	CNT_10 : IN std_logic_vector(31 DOWNTO 0);
	HC_MR : IN std_logic;
	RTEM_GRN : IN std_logic;
	rtem_ini : IN std_logic_vector(15 DOWNTO 0);
	RTEM_RED : IN std_logic;
	CNT_100 : IN std_logic_vector(31 DOWNTO 0);
	CNT_MAX : IN std_logic_vector(31 DOWNTO 0);
	CNT_110 : IN std_logic_vector(31 DOWNTO 0);
	RTEM_HS : IN std_logic;
	WD_EN : IN std_logic;
	ms100_tc : IN std_logic;
	WD_MR : IN std_logic;
	RTEM_ANALYZE : OUT std_logic;
	RTEM_RETRACT : OUT std_logic;
	RTEM_CNT : OUT std_logic_vector(31 DOWNTO 0);
	ST_RTEM : OUT std_logic_vector(3 DOWNTO 0)
	);
END rtemctrl;

ARCHITECTURE structure OF rtemctrl IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL ww_RTEM_SEL : std_logic;
SIGNAL ww_clk20 : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_Low_Speed : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Med_Speed : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Hi_Speed : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_Cmd_Mid_In : std_logic;
SIGNAL ww_Hi_Cnt : std_logic;
SIGNAL ww_Det_Sat : std_logic;
SIGNAL ww_CMD_IN : std_logic;
SIGNAL ww_CMD_OUT : std_logic;
SIGNAL ww_Cmd_Mid_Out : std_logic;
SIGNAL ww_CNT_90 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CNT_75 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CNT_25 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CNT_10 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_HC_MR : std_logic;
SIGNAL ww_RTEM_GRN : std_logic;
SIGNAL ww_rtem_ini : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_RTEM_RED : std_logic;
SIGNAL ww_CNT_100 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CNT_MAX : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_CNT_110 : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_RTEM_HS : std_logic;
SIGNAL ww_WD_EN : std_logic;
SIGNAL ww_ms100_tc : std_logic;
SIGNAL ww_WD_MR : std_logic;
SIGNAL ww_RTEM_ANALYZE : std_logic;
SIGNAL ww_RTEM_RETRACT : std_logic;
SIGNAL ww_RTEM_CNT : std_logic_vector(31 DOWNTO 0);
SIGNAL ww_ST_RTEM : std_logic_vector(3 DOWNTO 0);
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a_a998 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a_a999 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a_a1000 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a_a1001 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a_a1002 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a_a1003 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a_a1004 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a_a1005 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a_a1006 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a_a1007 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a_a1008 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a_a1009 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a_a1010 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a_a1011 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a_a1012 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a_a1013 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a_a1014 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a_a1015 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a_a1016 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a_a1017 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a_a1018 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a_a1019 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a_a1020 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a_a1021 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a_a1022 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a_a1023 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a_a1024 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a_a1025 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a_a1026 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a_a1027 : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a_a1028 : std_logic;
SIGNAL URTEM_ai_a1051_1 : std_logic;
SIGNAL UUMS2_aSelect_1486_rtl_11_rtl_27_a131_1 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a29_1 : std_logic;
SIGNAL Cmd_Wd_a48_1 : std_logic;
SIGNAL Cmd_Wd_a79_1 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a550_1 : std_logic;
SIGNAL RTEM_SEL_apadio : std_logic;
SIGNAL clk20_apadio : std_logic;
SIGNAL imr_apadio : std_logic;
SIGNAL Low_Speed_a1_a_apadio : std_logic;
SIGNAL Med_Speed_a1_a_apadio : std_logic;
SIGNAL Hi_Speed_a1_a_apadio : std_logic;
SIGNAL Low_Speed_a9_a_apadio : std_logic;
SIGNAL Med_Speed_a9_a_apadio : std_logic;
SIGNAL Hi_Speed_a9_a_apadio : std_logic;
SIGNAL Low_Speed_a8_a_apadio : std_logic;
SIGNAL Med_Speed_a8_a_apadio : std_logic;
SIGNAL Hi_Speed_a8_a_apadio : std_logic;
SIGNAL Low_Speed_a4_a_apadio : std_logic;
SIGNAL Low_Speed_a2_a_apadio : std_logic;
SIGNAL Med_Speed_a2_a_apadio : std_logic;
SIGNAL Hi_Speed_a2_a_apadio : std_logic;
SIGNAL Low_Speed_a0_a_apadio : std_logic;
SIGNAL Med_Speed_a0_a_apadio : std_logic;
SIGNAL Hi_Speed_a0_a_apadio : std_logic;
SIGNAL Low_Speed_a11_a_apadio : std_logic;
SIGNAL Med_Speed_a11_a_apadio : std_logic;
SIGNAL Hi_Speed_a11_a_apadio : std_logic;
SIGNAL Low_Speed_a10_a_apadio : std_logic;
SIGNAL Med_Speed_a10_a_apadio : std_logic;
SIGNAL Hi_Speed_a10_a_apadio : std_logic;
SIGNAL Low_Speed_a12_a_apadio : std_logic;
SIGNAL Med_Speed_a12_a_apadio : std_logic;
SIGNAL Hi_Speed_a12_a_apadio : std_logic;
SIGNAL Low_Speed_a5_a_apadio : std_logic;
SIGNAL Med_Speed_a5_a_apadio : std_logic;
SIGNAL Hi_Speed_a5_a_apadio : std_logic;
SIGNAL Low_Speed_a3_a_apadio : std_logic;
SIGNAL Med_Speed_a3_a_apadio : std_logic;
SIGNAL Hi_Speed_a3_a_apadio : std_logic;
SIGNAL Low_Speed_a15_a_apadio : std_logic;
SIGNAL Med_Speed_a15_a_apadio : std_logic;
SIGNAL Hi_Speed_a15_a_apadio : std_logic;
SIGNAL Low_Speed_a13_a_apadio : std_logic;
SIGNAL Med_Speed_a13_a_apadio : std_logic;
SIGNAL Hi_Speed_a13_a_apadio : std_logic;
SIGNAL Low_Speed_a7_a_apadio : std_logic;
SIGNAL Med_Speed_a7_a_apadio : std_logic;
SIGNAL Hi_Speed_a7_a_apadio : std_logic;
SIGNAL Low_Speed_a14_a_apadio : std_logic;
SIGNAL Med_Speed_a14_a_apadio : std_logic;
SIGNAL Hi_Speed_a14_a_apadio : std_logic;
SIGNAL Low_Speed_a6_a_apadio : std_logic;
SIGNAL Med_Speed_a6_a_apadio : std_logic;
SIGNAL Hi_Speed_a6_a_apadio : std_logic;
SIGNAL Cmd_Mid_In_apadio : std_logic;
SIGNAL Hi_Cnt_apadio : std_logic;
SIGNAL Det_Sat_apadio : std_logic;
SIGNAL CMD_IN_apadio : std_logic;
SIGNAL CMD_OUT_apadio : std_logic;
SIGNAL Cmd_Mid_Out_apadio : std_logic;
SIGNAL CNT_90_a31_a_apadio : std_logic;
SIGNAL CNT_75_a31_a_apadio : std_logic;
SIGNAL CNT_25_a31_a_apadio : std_logic;
SIGNAL CNT_10_a31_a_apadio : std_logic;
SIGNAL HC_MR_apadio : std_logic;
SIGNAL RTEM_GRN_apadio : std_logic;
SIGNAL rtem_ini_a11_a_apadio : std_logic;
SIGNAL rtem_ini_a4_a_apadio : std_logic;
SIGNAL rtem_ini_a3_a_apadio : std_logic;
SIGNAL rtem_ini_a0_a_apadio : std_logic;
SIGNAL rtem_ini_a6_a_apadio : std_logic;
SIGNAL rtem_ini_a14_a_apadio : std_logic;
SIGNAL rtem_ini_a13_a_apadio : std_logic;
SIGNAL rtem_ini_a5_a_apadio : std_logic;
SIGNAL rtem_ini_a2_a_apadio : std_logic;
SIGNAL rtem_ini_a15_a_apadio : std_logic;
SIGNAL rtem_ini_a1_a_apadio : std_logic;
SIGNAL rtem_ini_a12_a_apadio : std_logic;
SIGNAL rtem_ini_a10_a_apadio : std_logic;
SIGNAL rtem_ini_a9_a_apadio : std_logic;
SIGNAL rtem_ini_a8_a_apadio : std_logic;
SIGNAL rtem_ini_a7_a_apadio : std_logic;
SIGNAL RTEM_RED_apadio : std_logic;
SIGNAL CNT_100_a31_a_apadio : std_logic;
SIGNAL CNT_90_a30_a_apadio : std_logic;
SIGNAL CNT_75_a30_a_apadio : std_logic;
SIGNAL CNT_25_a30_a_apadio : std_logic;
SIGNAL CNT_10_a30_a_apadio : std_logic;
SIGNAL Med_Speed_a4_a_apadio : std_logic;
SIGNAL Hi_Speed_a4_a_apadio : std_logic;
SIGNAL CNT_MAX_a31_a_apadio : std_logic;
SIGNAL CNT_110_a31_a_apadio : std_logic;
SIGNAL RTEM_HS_apadio : std_logic;
SIGNAL CNT_100_a30_a_apadio : std_logic;
SIGNAL CNT_90_a29_a_apadio : std_logic;
SIGNAL CNT_75_a29_a_apadio : std_logic;
SIGNAL CNT_25_a29_a_apadio : std_logic;
SIGNAL CNT_10_a29_a_apadio : std_logic;
SIGNAL CNT_MAX_a30_a_apadio : std_logic;
SIGNAL CNT_110_a30_a_apadio : std_logic;
SIGNAL CNT_100_a29_a_apadio : std_logic;
SIGNAL WD_EN_apadio : std_logic;
SIGNAL ms100_tc_apadio : std_logic;
SIGNAL CNT_90_a28_a_apadio : std_logic;
SIGNAL CNT_75_a28_a_apadio : std_logic;
SIGNAL CNT_25_a28_a_apadio : std_logic;
SIGNAL CNT_10_a28_a_apadio : std_logic;
SIGNAL CNT_MAX_a29_a_apadio : std_logic;
SIGNAL CNT_110_a29_a_apadio : std_logic;
SIGNAL CNT_100_a28_a_apadio : std_logic;
SIGNAL WD_MR_apadio : std_logic;
SIGNAL CNT_90_a27_a_apadio : std_logic;
SIGNAL CNT_75_a27_a_apadio : std_logic;
SIGNAL CNT_25_a27_a_apadio : std_logic;
SIGNAL CNT_10_a27_a_apadio : std_logic;
SIGNAL CNT_MAX_a28_a_apadio : std_logic;
SIGNAL CNT_110_a28_a_apadio : std_logic;
SIGNAL CNT_100_a27_a_apadio : std_logic;
SIGNAL CNT_90_a26_a_apadio : std_logic;
SIGNAL CNT_75_a26_a_apadio : std_logic;
SIGNAL CNT_25_a26_a_apadio : std_logic;
SIGNAL CNT_10_a26_a_apadio : std_logic;
SIGNAL CNT_MAX_a27_a_apadio : std_logic;
SIGNAL CNT_110_a27_a_apadio : std_logic;
SIGNAL CNT_100_a26_a_apadio : std_logic;
SIGNAL CNT_90_a25_a_apadio : std_logic;
SIGNAL CNT_75_a25_a_apadio : std_logic;
SIGNAL CNT_25_a25_a_apadio : std_logic;
SIGNAL CNT_10_a25_a_apadio : std_logic;
SIGNAL CNT_MAX_a26_a_apadio : std_logic;
SIGNAL CNT_110_a26_a_apadio : std_logic;
SIGNAL CNT_100_a25_a_apadio : std_logic;
SIGNAL CNT_90_a24_a_apadio : std_logic;
SIGNAL CNT_75_a24_a_apadio : std_logic;
SIGNAL CNT_25_a24_a_apadio : std_logic;
SIGNAL CNT_10_a24_a_apadio : std_logic;
SIGNAL CNT_MAX_a25_a_apadio : std_logic;
SIGNAL CNT_110_a25_a_apadio : std_logic;
SIGNAL CNT_100_a24_a_apadio : std_logic;
SIGNAL CNT_90_a23_a_apadio : std_logic;
SIGNAL CNT_75_a23_a_apadio : std_logic;
SIGNAL CNT_25_a23_a_apadio : std_logic;
SIGNAL CNT_10_a23_a_apadio : std_logic;
SIGNAL CNT_MAX_a24_a_apadio : std_logic;
SIGNAL CNT_110_a24_a_apadio : std_logic;
SIGNAL CNT_100_a23_a_apadio : std_logic;
SIGNAL CNT_90_a22_a_apadio : std_logic;
SIGNAL CNT_75_a22_a_apadio : std_logic;
SIGNAL CNT_25_a22_a_apadio : std_logic;
SIGNAL CNT_10_a22_a_apadio : std_logic;
SIGNAL CNT_MAX_a23_a_apadio : std_logic;
SIGNAL CNT_110_a23_a_apadio : std_logic;
SIGNAL CNT_100_a22_a_apadio : std_logic;
SIGNAL CNT_90_a21_a_apadio : std_logic;
SIGNAL CNT_75_a21_a_apadio : std_logic;
SIGNAL CNT_25_a21_a_apadio : std_logic;
SIGNAL CNT_10_a21_a_apadio : std_logic;
SIGNAL CNT_MAX_a22_a_apadio : std_logic;
SIGNAL CNT_110_a22_a_apadio : std_logic;
SIGNAL CNT_100_a21_a_apadio : std_logic;
SIGNAL CNT_90_a20_a_apadio : std_logic;
SIGNAL CNT_75_a20_a_apadio : std_logic;
SIGNAL CNT_25_a20_a_apadio : std_logic;
SIGNAL CNT_10_a20_a_apadio : std_logic;
SIGNAL CNT_MAX_a21_a_apadio : std_logic;
SIGNAL CNT_110_a21_a_apadio : std_logic;
SIGNAL CNT_100_a20_a_apadio : std_logic;
SIGNAL CNT_90_a19_a_apadio : std_logic;
SIGNAL CNT_75_a19_a_apadio : std_logic;
SIGNAL CNT_25_a19_a_apadio : std_logic;
SIGNAL CNT_10_a19_a_apadio : std_logic;
SIGNAL CNT_MAX_a20_a_apadio : std_logic;
SIGNAL CNT_110_a20_a_apadio : std_logic;
SIGNAL CNT_100_a19_a_apadio : std_logic;
SIGNAL CNT_90_a18_a_apadio : std_logic;
SIGNAL CNT_75_a18_a_apadio : std_logic;
SIGNAL CNT_25_a18_a_apadio : std_logic;
SIGNAL CNT_10_a18_a_apadio : std_logic;
SIGNAL CNT_MAX_a19_a_apadio : std_logic;
SIGNAL CNT_110_a19_a_apadio : std_logic;
SIGNAL CNT_100_a18_a_apadio : std_logic;
SIGNAL CNT_90_a17_a_apadio : std_logic;
SIGNAL CNT_75_a17_a_apadio : std_logic;
SIGNAL CNT_25_a17_a_apadio : std_logic;
SIGNAL CNT_10_a17_a_apadio : std_logic;
SIGNAL CNT_MAX_a18_a_apadio : std_logic;
SIGNAL CNT_110_a18_a_apadio : std_logic;
SIGNAL CNT_100_a17_a_apadio : std_logic;
SIGNAL CNT_90_a16_a_apadio : std_logic;
SIGNAL CNT_75_a16_a_apadio : std_logic;
SIGNAL CNT_25_a16_a_apadio : std_logic;
SIGNAL CNT_10_a16_a_apadio : std_logic;
SIGNAL CNT_MAX_a17_a_apadio : std_logic;
SIGNAL CNT_110_a17_a_apadio : std_logic;
SIGNAL CNT_100_a16_a_apadio : std_logic;
SIGNAL CNT_90_a15_a_apadio : std_logic;
SIGNAL CNT_75_a15_a_apadio : std_logic;
SIGNAL CNT_25_a15_a_apadio : std_logic;
SIGNAL CNT_10_a15_a_apadio : std_logic;
SIGNAL CNT_MAX_a16_a_apadio : std_logic;
SIGNAL CNT_110_a16_a_apadio : std_logic;
SIGNAL CNT_100_a15_a_apadio : std_logic;
SIGNAL CNT_90_a14_a_apadio : std_logic;
SIGNAL CNT_75_a14_a_apadio : std_logic;
SIGNAL CNT_25_a14_a_apadio : std_logic;
SIGNAL CNT_10_a14_a_apadio : std_logic;
SIGNAL CNT_MAX_a15_a_apadio : std_logic;
SIGNAL CNT_110_a15_a_apadio : std_logic;
SIGNAL CNT_100_a14_a_apadio : std_logic;
SIGNAL CNT_90_a13_a_apadio : std_logic;
SIGNAL CNT_75_a13_a_apadio : std_logic;
SIGNAL CNT_25_a13_a_apadio : std_logic;
SIGNAL CNT_10_a13_a_apadio : std_logic;
SIGNAL CNT_MAX_a14_a_apadio : std_logic;
SIGNAL CNT_110_a14_a_apadio : std_logic;
SIGNAL CNT_100_a13_a_apadio : std_logic;
SIGNAL CNT_90_a12_a_apadio : std_logic;
SIGNAL CNT_75_a12_a_apadio : std_logic;
SIGNAL CNT_25_a12_a_apadio : std_logic;
SIGNAL CNT_10_a12_a_apadio : std_logic;
SIGNAL CNT_MAX_a13_a_apadio : std_logic;
SIGNAL CNT_110_a13_a_apadio : std_logic;
SIGNAL CNT_100_a12_a_apadio : std_logic;
SIGNAL CNT_90_a11_a_apadio : std_logic;
SIGNAL CNT_75_a11_a_apadio : std_logic;
SIGNAL CNT_25_a11_a_apadio : std_logic;
SIGNAL CNT_10_a11_a_apadio : std_logic;
SIGNAL CNT_MAX_a12_a_apadio : std_logic;
SIGNAL CNT_110_a12_a_apadio : std_logic;
SIGNAL CNT_100_a11_a_apadio : std_logic;
SIGNAL CNT_90_a10_a_apadio : std_logic;
SIGNAL CNT_75_a10_a_apadio : std_logic;
SIGNAL CNT_25_a10_a_apadio : std_logic;
SIGNAL CNT_10_a10_a_apadio : std_logic;
SIGNAL CNT_MAX_a11_a_apadio : std_logic;
SIGNAL CNT_110_a11_a_apadio : std_logic;
SIGNAL CNT_100_a10_a_apadio : std_logic;
SIGNAL CNT_90_a9_a_apadio : std_logic;
SIGNAL CNT_75_a9_a_apadio : std_logic;
SIGNAL CNT_25_a9_a_apadio : std_logic;
SIGNAL CNT_10_a9_a_apadio : std_logic;
SIGNAL CNT_MAX_a10_a_apadio : std_logic;
SIGNAL CNT_110_a10_a_apadio : std_logic;
SIGNAL CNT_100_a9_a_apadio : std_logic;
SIGNAL CNT_90_a8_a_apadio : std_logic;
SIGNAL CNT_75_a8_a_apadio : std_logic;
SIGNAL CNT_25_a8_a_apadio : std_logic;
SIGNAL CNT_10_a8_a_apadio : std_logic;
SIGNAL CNT_MAX_a9_a_apadio : std_logic;
SIGNAL CNT_110_a9_a_apadio : std_logic;
SIGNAL CNT_100_a8_a_apadio : std_logic;
SIGNAL CNT_90_a7_a_apadio : std_logic;
SIGNAL CNT_75_a7_a_apadio : std_logic;
SIGNAL CNT_25_a7_a_apadio : std_logic;
SIGNAL CNT_10_a7_a_apadio : std_logic;
SIGNAL CNT_MAX_a8_a_apadio : std_logic;
SIGNAL CNT_110_a8_a_apadio : std_logic;
SIGNAL CNT_100_a7_a_apadio : std_logic;
SIGNAL CNT_90_a6_a_apadio : std_logic;
SIGNAL CNT_75_a6_a_apadio : std_logic;
SIGNAL CNT_25_a6_a_apadio : std_logic;
SIGNAL CNT_10_a6_a_apadio : std_logic;
SIGNAL CNT_MAX_a7_a_apadio : std_logic;
SIGNAL CNT_110_a7_a_apadio : std_logic;
SIGNAL CNT_100_a6_a_apadio : std_logic;
SIGNAL CNT_90_a5_a_apadio : std_logic;
SIGNAL CNT_75_a5_a_apadio : std_logic;
SIGNAL CNT_25_a5_a_apadio : std_logic;
SIGNAL CNT_10_a5_a_apadio : std_logic;
SIGNAL CNT_MAX_a6_a_apadio : std_logic;
SIGNAL CNT_110_a6_a_apadio : std_logic;
SIGNAL CNT_100_a5_a_apadio : std_logic;
SIGNAL CNT_90_a4_a_apadio : std_logic;
SIGNAL CNT_75_a4_a_apadio : std_logic;
SIGNAL CNT_25_a4_a_apadio : std_logic;
SIGNAL CNT_10_a4_a_apadio : std_logic;
SIGNAL CNT_MAX_a5_a_apadio : std_logic;
SIGNAL CNT_110_a5_a_apadio : std_logic;
SIGNAL CNT_100_a4_a_apadio : std_logic;
SIGNAL CNT_90_a3_a_apadio : std_logic;
SIGNAL CNT_75_a3_a_apadio : std_logic;
SIGNAL CNT_25_a3_a_apadio : std_logic;
SIGNAL CNT_10_a3_a_apadio : std_logic;
SIGNAL CNT_MAX_a4_a_apadio : std_logic;
SIGNAL CNT_110_a4_a_apadio : std_logic;
SIGNAL CNT_100_a3_a_apadio : std_logic;
SIGNAL CNT_90_a2_a_apadio : std_logic;
SIGNAL CNT_75_a2_a_apadio : std_logic;
SIGNAL CNT_25_a2_a_apadio : std_logic;
SIGNAL CNT_10_a2_a_apadio : std_logic;
SIGNAL CNT_MAX_a3_a_apadio : std_logic;
SIGNAL CNT_110_a3_a_apadio : std_logic;
SIGNAL CNT_100_a2_a_apadio : std_logic;
SIGNAL CNT_90_a1_a_apadio : std_logic;
SIGNAL CNT_75_a1_a_apadio : std_logic;
SIGNAL CNT_25_a1_a_apadio : std_logic;
SIGNAL CNT_10_a1_a_apadio : std_logic;
SIGNAL CNT_MAX_a2_a_apadio : std_logic;
SIGNAL CNT_110_a2_a_apadio : std_logic;
SIGNAL CNT_100_a1_a_apadio : std_logic;
SIGNAL CNT_90_a0_a_apadio : std_logic;
SIGNAL CNT_75_a0_a_apadio : std_logic;
SIGNAL CNT_25_a0_a_apadio : std_logic;
SIGNAL CNT_10_a0_a_apadio : std_logic;
SIGNAL CNT_MAX_a1_a_apadio : std_logic;
SIGNAL CNT_110_a1_a_apadio : std_logic;
SIGNAL CNT_100_a0_a_apadio : std_logic;
SIGNAL CNT_MAX_a0_a_apadio : std_logic;
SIGNAL CNT_110_a0_a_apadio : std_logic;
SIGNAL RTEM_ANALYZE_apadio : std_logic;
SIGNAL RTEM_RETRACT_apadio : std_logic;
SIGNAL RTEM_CNT_a31_a_apadio : std_logic;
SIGNAL RTEM_CNT_a30_a_apadio : std_logic;
SIGNAL RTEM_CNT_a29_a_apadio : std_logic;
SIGNAL RTEM_CNT_a28_a_apadio : std_logic;
SIGNAL RTEM_CNT_a27_a_apadio : std_logic;
SIGNAL RTEM_CNT_a26_a_apadio : std_logic;
SIGNAL RTEM_CNT_a25_a_apadio : std_logic;
SIGNAL RTEM_CNT_a24_a_apadio : std_logic;
SIGNAL RTEM_CNT_a23_a_apadio : std_logic;
SIGNAL RTEM_CNT_a22_a_apadio : std_logic;
SIGNAL RTEM_CNT_a21_a_apadio : std_logic;
SIGNAL RTEM_CNT_a20_a_apadio : std_logic;
SIGNAL RTEM_CNT_a19_a_apadio : std_logic;
SIGNAL RTEM_CNT_a18_a_apadio : std_logic;
SIGNAL RTEM_CNT_a17_a_apadio : std_logic;
SIGNAL RTEM_CNT_a16_a_apadio : std_logic;
SIGNAL RTEM_CNT_a15_a_apadio : std_logic;
SIGNAL RTEM_CNT_a14_a_apadio : std_logic;
SIGNAL RTEM_CNT_a13_a_apadio : std_logic;
SIGNAL RTEM_CNT_a12_a_apadio : std_logic;
SIGNAL RTEM_CNT_a11_a_apadio : std_logic;
SIGNAL RTEM_CNT_a10_a_apadio : std_logic;
SIGNAL RTEM_CNT_a9_a_apadio : std_logic;
SIGNAL RTEM_CNT_a8_a_apadio : std_logic;
SIGNAL RTEM_CNT_a7_a_apadio : std_logic;
SIGNAL RTEM_CNT_a6_a_apadio : std_logic;
SIGNAL RTEM_CNT_a5_a_apadio : std_logic;
SIGNAL RTEM_CNT_a4_a_apadio : std_logic;
SIGNAL RTEM_CNT_a3_a_apadio : std_logic;
SIGNAL RTEM_CNT_a2_a_apadio : std_logic;
SIGNAL RTEM_CNT_a1_a_apadio : std_logic;
SIGNAL RTEM_CNT_a0_a_apadio : std_logic;
SIGNAL ST_RTEM_a3_a_apadio : std_logic;
SIGNAL ST_RTEM_a2_a_apadio : std_logic;
SIGNAL ST_RTEM_a1_a_apadio : std_logic;
SIGNAL ST_RTEM_a0_a_apadio : std_logic;
SIGNAL clk20_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a64 : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a86 : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a59 : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a73 : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a134 : std_logic;
SIGNAL URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_35_a103 : std_logic;
SIGNAL Hi_Cnt_acombout : std_logic;
SIGNAL Det_Sat_acombout : std_logic;
SIGNAL URTEM_ai_a1186 : std_logic;
SIGNAL HC_MR_acombout : std_logic;
SIGNAL CMD_OUT_acombout : std_logic;
SIGNAL UUMS2_ai_a5573 : std_logic;
SIGNAL RTEM_GRN_acombout : std_logic;
SIGNAL rtem_ini_a10_a_acombout : std_logic;
SIGNAL rtem_ini_a9_a_acombout : std_logic;
SIGNAL UUMS2_aRTEM_IN_VEC_a0_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a1_a : std_logic;
SIGNAL UUMS2_aRTEM_IN_VEC_a2_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a3_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a4_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a5_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a6_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a7_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_49_a28 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_49_a23 : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a4_a_a109 : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_vec_a4_a_a137 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aRTIN_DB : std_logic;
SIGNAL URTEM_aU_RT_OUT_aRTIN_DB_DEL : std_logic;
SIGNAL URTEM_aU_RT_OUT_ai_a138 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_4_a38 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_4_a33 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_4_a55 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL rtem_ini_a11_a_acombout : std_logic;
SIGNAL rtem_ini_a4_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a16 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a96 : std_logic;
SIGNAL rtl_a1 : std_logic;
SIGNAL rtem_ini_a6_a_acombout : std_logic;
SIGNAL rtem_ini_a14_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a24 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL rtem_ini_a2_a_acombout : std_logic;
SIGNAL rtem_ini_a15_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a40 : std_logic;
SIGNAL rtem_ini_a13_a_acombout : std_logic;
SIGNAL rtem_ini_a5_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a31 : std_logic;
SIGNAL rtem_ini_a0_a_acombout : std_logic;
SIGNAL rtem_ini_a3_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a19 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a99 : std_logic;
SIGNAL URTEM_aU_RT_OUT_ai_a156 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a64 : std_logic;
SIGNAL rtem_ini_a1_a_acombout : std_logic;
SIGNAL rtem_ini_a12_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a51 : std_logic;
SIGNAL rtem_ini_a7_a_acombout : std_logic;
SIGNAL rtem_ini_a8_a_acombout : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a79 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a108 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aflash_tc_a122 : std_logic;
SIGNAL URTEM_aU_RT_OUT_artin_reg : std_logic;
SIGNAL URTEM_aU_RT_OUT_aiAC : std_logic;
SIGNAL RTEM_RED_acombout : std_logic;
SIGNAL UUMS2_aRTEM_OUT_VEC_a0_a : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a1_a : std_logic;
SIGNAL UUMS2_aRTEM_OUT_VEC_a2_a : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a3_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_49_a23 : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a4_a : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a5_a : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a6_a : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a7_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_49_a28 : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a4_a_a109 : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_vec_a4_a_a137 : std_logic;
SIGNAL URTEM_aU_RT_IN_aRTIN_DB : std_logic;
SIGNAL URTEM_aU_RT_IN_aRTIN_DB_DEL : std_logic;
SIGNAL URTEM_aU_RT_IN_ai_a138 : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_4_a33 : std_logic;
SIGNAL URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_4_a38 : std_logic;
SIGNAL URTEM_aU_RT_IN_areduce_nor_4_a55 : std_logic;
SIGNAL rtl_a0 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a96 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a79 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a64 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a51 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a108 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a24 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a40 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a31 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a19 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a99 : std_logic;
SIGNAL URTEM_aU_RT_IN_ai_a156 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a16 : std_logic;
SIGNAL URTEM_aU_RT_IN_aflash_tc_a122 : std_logic;
SIGNAL URTEM_aU_RT_IN_artin_reg : std_logic;
SIGNAL URTEM_aU_RT_IN_aiAC : std_logic;
SIGNAL URTEM_ai_a1192 : std_logic;
SIGNAL URTEM_aRTEM_OUT : std_logic;
SIGNAL ms100_tc_acombout : std_logic;
SIGNAL ms100_tc_vec_a0_a : std_logic;
SIGNAL ms100_tc_vec_a1_a : std_logic;
SIGNAL reduce_nor_24 : std_logic;
SIGNAL WD_MR_acombout : std_logic;
SIGNAL WD_MR_VEC_a0_a : std_logic;
SIGNAL WD_EN_acombout : std_logic;
SIGNAL WD_MR_VEC_a1_a : std_logic;
SIGNAL i_a6 : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL WD_CNT_rtl_32_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Cmd_Wd_a48 : std_logic;
SIGNAL Cmd_Wd_a79 : std_logic;
SIGNAL Cmd_Wd_a78 : std_logic;
SIGNAL URTEM_aRTEM_OUT_CMD_a27 : std_logic;
SIGNAL URTEM_aHi_Cnt_En : std_logic;
SIGNAL URTEM_aU_RT_IN_ai_a264 : std_logic;
SIGNAL URTEM_aMux_31_rtl_94_a9 : std_logic;
SIGNAL URTEM_ai_a1051 : std_logic;
SIGNAL URTEM_aHi_Cnt_State : std_logic;
SIGNAL CMD_IN_acombout : std_logic;
SIGNAL URTEM_aU_RT_IN_ai_a265 : std_logic;
SIGNAL URTEM_aU_RT_IN_amv_cmdb : std_logic;
SIGNAL URTEM_aU_RT_IN_aimove : std_logic;
SIGNAL URTEM_aU_RT_IN_amove_areg0 : std_logic;
SIGNAL RTEM_SEL_acombout : std_logic;
SIGNAL UUMS2_aSW_IN_DB : std_logic;
SIGNAL CNT_100_a31_a_acombout : std_logic;
SIGNAL RTEM_HS_acombout : std_logic;
SIGNAL UUMS2_aRTEM_HS_VEC_a0_a : std_logic;
SIGNAL UUMS2_aRTEM_HS_VEC_a1_a : std_logic;
SIGNAL UUMS2_aRTEM_HS_VEC_a2_a : std_logic;
SIGNAL UUMS2_aRTEM_HS_VEC_a3_a : std_logic;
SIGNAL UUMS2_aSW_HS_DB : std_logic;
SIGNAL UUMS2_aSW_OUT_DB : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a83 : std_logic;
SIGNAL Cmd_Mid_In_acombout : std_logic;
SIGNAL Cmd_Mid_Out_acombout : std_logic;
SIGNAL UUMS2_ai_a5586 : std_logic;
SIGNAL UUMS2_aHI_Cnt_En : std_logic;
SIGNAL UUMS2_ai_a5591 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a87 : std_logic;
SIGNAL UUMS2_ai_a761 : std_logic;
SIGNAL UUMS2_aSelect_1480_rtl_8_rtl_24_a252 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a33 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a309 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a375 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a373 : std_logic;
SIGNAL UUMS2_ai_a3089 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a222 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a264 : std_logic;
SIGNAL UUMS2_ai_a5574 : std_logic;
SIGNAL UUMS2_aSelect_1488_rtl_12_rtl_28_a23 : std_logic;
SIGNAL UUMS2_aCMD_ERROR_a32 : std_logic;
SIGNAL UUMS2_aSelect_1488_rtl_12_rtl_28_a406 : std_logic;
SIGNAL UUMS2_aSelect_1488_rtl_12_rtl_28_a16 : std_logic;
SIGNAL CNT_MAX_a31_a_acombout : std_logic;
SIGNAL CNT_MAX_a30_a_acombout : std_logic;
SIGNAL UUMS2_ai_a5575 : std_logic;
SIGNAL UUMS2_areduce_nor_71_a25 : std_logic;
SIGNAL UUMS2_ai_a5682 : std_logic;
SIGNAL UUMS2_ai_a5678 : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a : std_logic;
SIGNAL CNT_MAX_a29_a_acombout : std_logic;
SIGNAL CNT_MAX_a28_a_acombout : std_logic;
SIGNAL CNT_MAX_a27_a_acombout : std_logic;
SIGNAL CNT_MAX_a26_a_acombout : std_logic;
SIGNAL CNT_MAX_a25_a_acombout : std_logic;
SIGNAL CNT_MAX_a24_a_acombout : std_logic;
SIGNAL CNT_MAX_a23_a_acombout : std_logic;
SIGNAL CNT_MAX_a22_a_acombout : std_logic;
SIGNAL CNT_MAX_a21_a_acombout : std_logic;
SIGNAL CNT_MAX_a20_a_acombout : std_logic;
SIGNAL CNT_MAX_a19_a_acombout : std_logic;
SIGNAL CNT_MAX_a18_a_acombout : std_logic;
SIGNAL CNT_MAX_a17_a_acombout : std_logic;
SIGNAL CNT_MAX_a16_a_acombout : std_logic;
SIGNAL CNT_MAX_a15_a_acombout : std_logic;
SIGNAL CNT_MAX_a14_a_acombout : std_logic;
SIGNAL CNT_MAX_a13_a_acombout : std_logic;
SIGNAL CNT_MAX_a12_a_acombout : std_logic;
SIGNAL CNT_MAX_a11_a_acombout : std_logic;
SIGNAL CNT_MAX_a10_a_acombout : std_logic;
SIGNAL CNT_MAX_a9_a_acombout : std_logic;
SIGNAL CNT_MAX_a8_a_acombout : std_logic;
SIGNAL CNT_MAX_a7_a_acombout : std_logic;
SIGNAL CNT_MAX_a6_a_acombout : std_logic;
SIGNAL CNT_MAX_a5_a_acombout : std_logic;
SIGNAL CNT_MAX_a4_a_acombout : std_logic;
SIGNAL CNT_MAX_a3_a_acombout : std_logic;
SIGNAL CNT_MAX_a2_a_acombout : std_logic;
SIGNAL CNT_MAX_a1_a_acombout : std_logic;
SIGNAL CNT_MAX_a0_a_acombout : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL UUMS2_ai_a1083 : std_logic;
SIGNAL UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL UUMS2_areduce_nor_10_a19 : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL Low_Speed_a10_a_acombout : std_logic;
SIGNAL CNT_90_a31_a_acombout : std_logic;
SIGNAL CNT_90_a30_a_acombout : std_logic;
SIGNAL CNT_90_a29_a_acombout : std_logic;
SIGNAL CNT_90_a28_a_acombout : std_logic;
SIGNAL CNT_90_a27_a_acombout : std_logic;
SIGNAL CNT_90_a26_a_acombout : std_logic;
SIGNAL CNT_90_a25_a_acombout : std_logic;
SIGNAL CNT_90_a24_a_acombout : std_logic;
SIGNAL CNT_90_a23_a_acombout : std_logic;
SIGNAL CNT_90_a22_a_acombout : std_logic;
SIGNAL CNT_90_a21_a_acombout : std_logic;
SIGNAL CNT_90_a20_a_acombout : std_logic;
SIGNAL CNT_90_a19_a_acombout : std_logic;
SIGNAL CNT_90_a18_a_acombout : std_logic;
SIGNAL CNT_90_a17_a_acombout : std_logic;
SIGNAL CNT_90_a16_a_acombout : std_logic;
SIGNAL CNT_90_a15_a_acombout : std_logic;
SIGNAL CNT_90_a14_a_acombout : std_logic;
SIGNAL CNT_90_a13_a_acombout : std_logic;
SIGNAL CNT_90_a12_a_acombout : std_logic;
SIGNAL CNT_90_a11_a_acombout : std_logic;
SIGNAL CNT_90_a10_a_acombout : std_logic;
SIGNAL CNT_90_a9_a_acombout : std_logic;
SIGNAL CNT_90_a8_a_acombout : std_logic;
SIGNAL CNT_90_a7_a_acombout : std_logic;
SIGNAL CNT_90_a6_a_acombout : std_logic;
SIGNAL CNT_90_a5_a_acombout : std_logic;
SIGNAL CNT_90_a4_a_acombout : std_logic;
SIGNAL CNT_90_a3_a_acombout : std_logic;
SIGNAL CNT_90_a2_a_acombout : std_logic;
SIGNAL CNT_90_a1_a_acombout : std_logic;
SIGNAL CNT_90_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL CNT_10_a31_a_acombout : std_logic;
SIGNAL CNT_10_a30_a_acombout : std_logic;
SIGNAL CNT_10_a29_a_acombout : std_logic;
SIGNAL CNT_10_a28_a_acombout : std_logic;
SIGNAL CNT_10_a27_a_acombout : std_logic;
SIGNAL CNT_10_a26_a_acombout : std_logic;
SIGNAL CNT_10_a25_a_acombout : std_logic;
SIGNAL CNT_10_a24_a_acombout : std_logic;
SIGNAL CNT_10_a23_a_acombout : std_logic;
SIGNAL CNT_10_a22_a_acombout : std_logic;
SIGNAL CNT_10_a21_a_acombout : std_logic;
SIGNAL CNT_10_a20_a_acombout : std_logic;
SIGNAL CNT_10_a19_a_acombout : std_logic;
SIGNAL CNT_10_a18_a_acombout : std_logic;
SIGNAL CNT_10_a17_a_acombout : std_logic;
SIGNAL CNT_10_a16_a_acombout : std_logic;
SIGNAL CNT_10_a15_a_acombout : std_logic;
SIGNAL CNT_10_a14_a_acombout : std_logic;
SIGNAL CNT_10_a13_a_acombout : std_logic;
SIGNAL CNT_10_a12_a_acombout : std_logic;
SIGNAL CNT_10_a11_a_acombout : std_logic;
SIGNAL CNT_10_a10_a_acombout : std_logic;
SIGNAL CNT_10_a9_a_acombout : std_logic;
SIGNAL CNT_10_a8_a_acombout : std_logic;
SIGNAL CNT_10_a7_a_acombout : std_logic;
SIGNAL CNT_10_a6_a_acombout : std_logic;
SIGNAL CNT_10_a5_a_acombout : std_logic;
SIGNAL CNT_10_a4_a_acombout : std_logic;
SIGNAL CNT_10_a3_a_acombout : std_logic;
SIGNAL CNT_10_a2_a_acombout : std_logic;
SIGNAL CNT_10_a1_a_acombout : std_logic;
SIGNAL CNT_10_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL rtl_a31 : std_logic;
SIGNAL Med_Speed_a10_a_acombout : std_logic;
SIGNAL CNT_25_a31_a_acombout : std_logic;
SIGNAL CNT_25_a30_a_acombout : std_logic;
SIGNAL CNT_25_a29_a_acombout : std_logic;
SIGNAL CNT_25_a28_a_acombout : std_logic;
SIGNAL CNT_25_a27_a_acombout : std_logic;
SIGNAL CNT_25_a26_a_acombout : std_logic;
SIGNAL CNT_25_a25_a_acombout : std_logic;
SIGNAL CNT_25_a24_a_acombout : std_logic;
SIGNAL CNT_25_a23_a_acombout : std_logic;
SIGNAL CNT_25_a22_a_acombout : std_logic;
SIGNAL CNT_25_a21_a_acombout : std_logic;
SIGNAL CNT_25_a20_a_acombout : std_logic;
SIGNAL CNT_25_a19_a_acombout : std_logic;
SIGNAL CNT_25_a18_a_acombout : std_logic;
SIGNAL CNT_25_a17_a_acombout : std_logic;
SIGNAL CNT_25_a16_a_acombout : std_logic;
SIGNAL CNT_25_a15_a_acombout : std_logic;
SIGNAL CNT_25_a14_a_acombout : std_logic;
SIGNAL CNT_25_a13_a_acombout : std_logic;
SIGNAL CNT_25_a12_a_acombout : std_logic;
SIGNAL CNT_25_a11_a_acombout : std_logic;
SIGNAL CNT_25_a10_a_acombout : std_logic;
SIGNAL CNT_25_a9_a_acombout : std_logic;
SIGNAL CNT_25_a8_a_acombout : std_logic;
SIGNAL CNT_25_a7_a_acombout : std_logic;
SIGNAL CNT_25_a6_a_acombout : std_logic;
SIGNAL CNT_25_a5_a_acombout : std_logic;
SIGNAL CNT_25_a4_a_acombout : std_logic;
SIGNAL CNT_25_a3_a_acombout : std_logic;
SIGNAL CNT_25_a2_a_acombout : std_logic;
SIGNAL CNT_25_a1_a_acombout : std_logic;
SIGNAL CNT_25_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Hi_Speed_a10_a_acombout : std_logic;
SIGNAL CNT_75_a31_a_acombout : std_logic;
SIGNAL CNT_75_a30_a_acombout : std_logic;
SIGNAL CNT_75_a29_a_acombout : std_logic;
SIGNAL CNT_75_a28_a_acombout : std_logic;
SIGNAL CNT_75_a27_a_acombout : std_logic;
SIGNAL CNT_75_a26_a_acombout : std_logic;
SIGNAL CNT_75_a25_a_acombout : std_logic;
SIGNAL CNT_75_a24_a_acombout : std_logic;
SIGNAL CNT_75_a23_a_acombout : std_logic;
SIGNAL CNT_75_a22_a_acombout : std_logic;
SIGNAL CNT_75_a21_a_acombout : std_logic;
SIGNAL CNT_75_a20_a_acombout : std_logic;
SIGNAL CNT_75_a19_a_acombout : std_logic;
SIGNAL CNT_75_a18_a_acombout : std_logic;
SIGNAL CNT_75_a17_a_acombout : std_logic;
SIGNAL CNT_75_a16_a_acombout : std_logic;
SIGNAL CNT_75_a15_a_acombout : std_logic;
SIGNAL CNT_75_a14_a_acombout : std_logic;
SIGNAL CNT_75_a13_a_acombout : std_logic;
SIGNAL CNT_75_a12_a_acombout : std_logic;
SIGNAL CNT_75_a11_a_acombout : std_logic;
SIGNAL CNT_75_a10_a_acombout : std_logic;
SIGNAL CNT_75_a9_a_acombout : std_logic;
SIGNAL CNT_75_a8_a_acombout : std_logic;
SIGNAL CNT_75_a7_a_acombout : std_logic;
SIGNAL CNT_75_a6_a_acombout : std_logic;
SIGNAL CNT_75_a5_a_acombout : std_logic;
SIGNAL CNT_75_a4_a_acombout : std_logic;
SIGNAL CNT_75_a3_a_acombout : std_logic;
SIGNAL CNT_75_a2_a_acombout : std_logic;
SIGNAL CNT_75_a1_a_acombout : std_logic;
SIGNAL CNT_75_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL rtl_a744 : std_logic;
SIGNAL rtl_a484 : std_logic;
SIGNAL rtl_a749 : std_logic;
SIGNAL Low_Speed_a11_a_acombout : std_logic;
SIGNAL Hi_Speed_a11_a_acombout : std_logic;
SIGNAL Med_Speed_a11_a_acombout : std_logic;
SIGNAL rtl_a755 : std_logic;
SIGNAL rtl_a760 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a41 : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL Low_Speed_a0_a_acombout : std_logic;
SIGNAL Med_Speed_a0_a_acombout : std_logic;
SIGNAL Hi_Speed_a0_a_acombout : std_logic;
SIGNAL rtl_a634 : std_logic;
SIGNAL rtl_a639 : std_logic;
SIGNAL Low_Speed_a2_a_acombout : std_logic;
SIGNAL Med_Speed_a2_a_acombout : std_logic;
SIGNAL Hi_Speed_a2_a_acombout : std_logic;
SIGNAL rtl_a656 : std_logic;
SIGNAL rtl_a661 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a34 : std_logic;
SIGNAL Low_Speed_a1_a_acombout : std_logic;
SIGNAL Med_Speed_a1_a_acombout : std_logic;
SIGNAL Hi_Speed_a1_a_acombout : std_logic;
SIGNAL rtl_a645 : std_logic;
SIGNAL rtl_a4104 : std_logic;
SIGNAL UUMS2_ai_a3 : std_logic;
SIGNAL Low_Speed_a4_a_acombout : std_logic;
SIGNAL Med_Speed_a4_a_acombout : std_logic;
SIGNAL Hi_Speed_a4_a_acombout : std_logic;
SIGNAL rtl_a4106 : std_logic;
SIGNAL rtl_a4105 : std_logic;
SIGNAL rtl_a683 : std_logic;
SIGNAL Low_Speed_a8_a_acombout : std_logic;
SIGNAL Hi_Speed_a8_a_acombout : std_logic;
SIGNAL Med_Speed_a8_a_acombout : std_logic;
SIGNAL rtl_a722 : std_logic;
SIGNAL rtl_a727 : std_logic;
SIGNAL Low_Speed_a9_a_acombout : std_logic;
SIGNAL Hi_Speed_a9_a_acombout : std_logic;
SIGNAL Med_Speed_a9_a_acombout : std_logic;
SIGNAL rtl_a733 : std_logic;
SIGNAL rtl_a738 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a29 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a118 : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL Low_Speed_a3_a_acombout : std_logic;
SIGNAL Med_Speed_a3_a_acombout : std_logic;
SIGNAL Hi_Speed_a3_a_acombout : std_logic;
SIGNAL rtl_a667 : std_logic;
SIGNAL rtl_a672 : std_logic;
SIGNAL Low_Speed_a15_a_acombout : std_logic;
SIGNAL Hi_Speed_a15_a_acombout : std_logic;
SIGNAL Med_Speed_a15_a_acombout : std_logic;
SIGNAL rtl_a799 : std_logic;
SIGNAL rtl_a804 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a61 : std_logic;
SIGNAL Low_Speed_a6_a_acombout : std_logic;
SIGNAL Hi_Speed_a6_a_acombout : std_logic;
SIGNAL Med_Speed_a6_a_acombout : std_logic;
SIGNAL rtl_a700 : std_logic;
SIGNAL rtl_a705 : std_logic;
SIGNAL Low_Speed_a14_a_acombout : std_logic;
SIGNAL Med_Speed_a14_a_acombout : std_logic;
SIGNAL Hi_Speed_a14_a_acombout : std_logic;
SIGNAL rtl_a788 : std_logic;
SIGNAL rtl_a793 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a89 : std_logic;
SIGNAL Low_Speed_a7_a_acombout : std_logic;
SIGNAL Med_Speed_a7_a_acombout : std_logic;
SIGNAL Hi_Speed_a7_a_acombout : std_logic;
SIGNAL rtl_a711 : std_logic;
SIGNAL rtl_a716 : std_logic;
SIGNAL Low_Speed_a13_a_acombout : std_logic;
SIGNAL Hi_Speed_a13_a_acombout : std_logic;
SIGNAL Med_Speed_a13_a_acombout : std_logic;
SIGNAL rtl_a777 : std_logic;
SIGNAL rtl_a782 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a74 : std_logic;
SIGNAL Low_Speed_a12_a_acombout : std_logic;
SIGNAL Med_Speed_a12_a_acombout : std_logic;
SIGNAL Hi_Speed_a12_a_acombout : std_logic;
SIGNAL rtl_a766 : std_logic;
SIGNAL rtl_a771 : std_logic;
SIGNAL Low_Speed_a5_a_acombout : std_logic;
SIGNAL Med_Speed_a5_a_acombout : std_logic;
SIGNAL Hi_Speed_a5_a_acombout : std_logic;
SIGNAL rtl_a689 : std_logic;
SIGNAL rtl_a694 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a50 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a111 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a124 : std_logic;
SIGNAL UUMS2_astep_clk : std_logic;
SIGNAL UUMS2_ai_a5589 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a20 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a85 : std_logic;
SIGNAL UUMS2_ai_a5585 : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL UUMS2_areduce_nor_37_a64 : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL UUMS2_areduce_nor_37_a55 : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL UUMS2_areduce_nor_37_a77 : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL UUMS2_areduce_nor_37_a50 : std_logic;
SIGNAL UUMS2_areduce_nor_37_a98 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a8 : std_logic;
SIGNAL UUMS2_aSelect_1490_rtl_13_a196 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a21 : std_logic;
SIGNAL UUMS2_ai_a5689 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a550 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a549 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a12 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a22 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a542 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a132 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a135 : std_logic;
SIGNAL UUMS2_ai_a764 : std_logic;
SIGNAL CNT_110_a31_a_acombout : std_logic;
SIGNAL CNT_110_a30_a_acombout : std_logic;
SIGNAL CNT_110_a29_a_acombout : std_logic;
SIGNAL CNT_110_a28_a_acombout : std_logic;
SIGNAL CNT_110_a27_a_acombout : std_logic;
SIGNAL CNT_110_a26_a_acombout : std_logic;
SIGNAL CNT_110_a25_a_acombout : std_logic;
SIGNAL CNT_110_a24_a_acombout : std_logic;
SIGNAL CNT_110_a23_a_acombout : std_logic;
SIGNAL CNT_110_a22_a_acombout : std_logic;
SIGNAL CNT_110_a21_a_acombout : std_logic;
SIGNAL CNT_110_a20_a_acombout : std_logic;
SIGNAL CNT_110_a19_a_acombout : std_logic;
SIGNAL CNT_110_a18_a_acombout : std_logic;
SIGNAL CNT_110_a17_a_acombout : std_logic;
SIGNAL CNT_110_a16_a_acombout : std_logic;
SIGNAL CNT_110_a15_a_acombout : std_logic;
SIGNAL CNT_110_a14_a_acombout : std_logic;
SIGNAL CNT_110_a13_a_acombout : std_logic;
SIGNAL CNT_110_a12_a_acombout : std_logic;
SIGNAL CNT_110_a11_a_acombout : std_logic;
SIGNAL CNT_110_a10_a_acombout : std_logic;
SIGNAL CNT_110_a9_a_acombout : std_logic;
SIGNAL CNT_110_a8_a_acombout : std_logic;
SIGNAL CNT_110_a7_a_acombout : std_logic;
SIGNAL CNT_110_a6_a_acombout : std_logic;
SIGNAL CNT_110_a5_a_acombout : std_logic;
SIGNAL CNT_110_a4_a_acombout : std_logic;
SIGNAL CNT_110_a3_a_acombout : std_logic;
SIGNAL CNT_110_a2_a_acombout : std_logic;
SIGNAL CNT_110_a1_a_acombout : std_logic;
SIGNAL CNT_110_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL rtl_a650 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a26 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a106 : std_logic;
SIGNAL UUMS2_ai_a5690 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a150 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a125 : std_logic;
SIGNAL UUMS2_areduce_nor_27_a119 : std_logic;
SIGNAL UUMS2_ai_a5583 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a14 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a9 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a86 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a29 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a292 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a285 : std_logic;
SIGNAL UUMS2_ai_a5571 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a243 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a151 : std_logic;
SIGNAL UUMS2_aSelect_1472_rtl_4_rtl_20_a293 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a12 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a340 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a347 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a367 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a333 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a18 : std_logic;
SIGNAL UUMS2_aSelect_1474_rtl_5_rtl_21_a6 : std_logic;
SIGNAL UUMS2_aSelect_1474_rtl_5_rtl_21_a87 : std_logic;
SIGNAL UUMS2_ai_a311 : std_logic;
SIGNAL UUMS2_aSelect_1474_rtl_5_rtl_21_a69 : std_logic;
SIGNAL UUMS2_aSelect_1474_rtl_5_rtl_21_a5 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a13 : std_logic;
SIGNAL UUMS2_aSelect_1474_rtl_5_rtl_21_a91 : std_logic;
SIGNAL UUMS2_ai_a5688 : std_logic;
SIGNAL UUMS2_aSelect_1480_rtl_8_rtl_24_a255 : std_logic;
SIGNAL UUMS2_aSelect_1480_rtl_8_rtl_24_a30 : std_logic;
SIGNAL UUMS2_aSelect_1466_rtl_1_rtl_17_a84 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a16 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a88 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a414 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a421 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a428 : std_logic;
SIGNAL UUMS2_ai_a2900 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a89 : std_logic;
SIGNAL UUMS2_aSelect_1468_rtl_2_rtl_18_a91 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a10 : std_logic;
SIGNAL UUMS2_aSelect_1478_rtl_7_rtl_23_a183 : std_logic;
SIGNAL UUMS2_aSelect_1478_rtl_7_rtl_23_a185 : std_logic;
SIGNAL UUMS2_aSelect_1478_rtl_7_rtl_23_a17 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a15 : std_logic;
SIGNAL UUMS2_aSelect_1478_rtl_7_rtl_23_a184 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a115 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a146 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a145 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a132 : std_logic;
SIGNAL UUMS2_ai_a3082 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a308 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a307 : std_logic;
SIGNAL UUMS2_aSelect_1470_rtl_3_rtl_19_a8 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a11 : std_logic;
SIGNAL UUMS2_aSelect_1484_rtl_10_rtl_26_a374 : std_logic;
SIGNAL UUMS2_aSelect_1486_rtl_11_rtl_27_a17 : std_logic;
SIGNAL UUMS2_aSelect_1486_rtl_11_rtl_27_a131 : std_logic;
SIGNAL UUMS2_aSelect_1486_rtl_11_rtl_27_a151 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a19 : std_logic;
SIGNAL UUMS2_areduce_nor_71 : std_logic;
SIGNAL UUMS2_ai_a5579 : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a : std_logic;
SIGNAL CNT_100_a30_a_acombout : std_logic;
SIGNAL CNT_100_a29_a_acombout : std_logic;
SIGNAL CNT_100_a28_a_acombout : std_logic;
SIGNAL CNT_100_a27_a_acombout : std_logic;
SIGNAL CNT_100_a26_a_acombout : std_logic;
SIGNAL CNT_100_a25_a_acombout : std_logic;
SIGNAL CNT_100_a24_a_acombout : std_logic;
SIGNAL CNT_100_a23_a_acombout : std_logic;
SIGNAL CNT_100_a22_a_acombout : std_logic;
SIGNAL CNT_100_a21_a_acombout : std_logic;
SIGNAL CNT_100_a20_a_acombout : std_logic;
SIGNAL CNT_100_a19_a_acombout : std_logic;
SIGNAL CNT_100_a18_a_acombout : std_logic;
SIGNAL CNT_100_a17_a_acombout : std_logic;
SIGNAL CNT_100_a16_a_acombout : std_logic;
SIGNAL CNT_100_a15_a_acombout : std_logic;
SIGNAL CNT_100_a14_a_acombout : std_logic;
SIGNAL CNT_100_a13_a_acombout : std_logic;
SIGNAL CNT_100_a12_a_acombout : std_logic;
SIGNAL CNT_100_a11_a_acombout : std_logic;
SIGNAL CNT_100_a10_a_acombout : std_logic;
SIGNAL CNT_100_a9_a_acombout : std_logic;
SIGNAL CNT_100_a8_a_acombout : std_logic;
SIGNAL CNT_100_a7_a_acombout : std_logic;
SIGNAL CNT_100_a6_a_acombout : std_logic;
SIGNAL CNT_100_a5_a_acombout : std_logic;
SIGNAL CNT_100_a4_a_acombout : std_logic;
SIGNAL CNT_100_a3_a_acombout : std_logic;
SIGNAL CNT_100_a2_a_acombout : std_logic;
SIGNAL CNT_100_a1_a_acombout : std_logic;
SIGNAL CNT_100_a0_a_acombout : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a : std_logic;
SIGNAL UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL UUMS2_ai_a3076 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a306 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a10 : std_logic;
SIGNAL UUMS2_ai_a5587 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a286 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a270 : std_logic;
SIGNAL UUMS2_aSelect_1482_rtl_9_rtl_25_a299 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a17 : std_logic;
SIGNAL UUMS2_aUMS_DIR_areg0 : std_logic;
SIGNAL i_a25 : std_logic;
SIGNAL URTEM_aU_RT_OUT_amv_cmdb : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a59 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a64 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a73 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a86 : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a134 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a : std_logic;
SIGNAL URTEM_aU_RT_OUT_areduce_nor_35_a103 : std_logic;
SIGNAL URTEM_aU_RT_OUT_aimove : std_logic;
SIGNAL URTEM_aU_RT_OUT_amove_areg0 : std_logic;
SIGNAL i_a28 : std_logic;
SIGNAL UUMS2_ai_a5590 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a0 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a127 : std_logic;
SIGNAL UUMS2_ai_a3075 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a0COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a1 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a124 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a1COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a2 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a121 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a2COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a3 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a118 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a3COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a4 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a115 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a4COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a5 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a112 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a5COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a6 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a109 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a6COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a7 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a106 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a7COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a8 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a103 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a8COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a9 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a100 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a9COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a10 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a97 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a10COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a11 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a94 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a11COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a12 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a91 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a12COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a13 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a88 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a13COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a14 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a85 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a14COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a15 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a82 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a15COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a16 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a79 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a16COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a17 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a76 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a17COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a18 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a73 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a18COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a19 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a70 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a19COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a20 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a67 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a20COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a21 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a64 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a21COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a22 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a61 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a22COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a23 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a58 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a23COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a24 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a55 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a24COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a25 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a52 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a25COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a26 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a49 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a26COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a27 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a46 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a27COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a28 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a43 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a28COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a29 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a40 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a29COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a30 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a37 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a30COUT : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_a31 : std_logic;
SIGNAL UUMS2_aadd_155_rtl_15_rtl_169_a34 : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a_aCOUT : std_logic;
SIGNAL UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a : std_logic;
SIGNAL UUMS2_ai_a5581 : std_logic;
SIGNAL UUMS2_aST_RTEM_a3_a_areg0 : std_logic;
SIGNAL URTEM_ai_a515 : std_logic;
SIGNAL URTEM_ai_a1097 : std_logic;
SIGNAL URTEM_aST_RTEM_a3_a_areg0 : std_logic;
SIGNAL i_a31 : std_logic;
SIGNAL UUMS2_aUMS_STATE_a508 : std_logic;
SIGNAL UUMS2_aST_RTEM_a2_a_areg0 : std_logic;
SIGNAL URTEM_aMux_31_rtl_94_a17 : std_logic;
SIGNAL URTEM_aU_RT_IN_ai_a492 : std_logic;
SIGNAL URTEM_ai_a1196 : std_logic;
SIGNAL URTEM_aST_RTEM_a2_a_areg0 : std_logic;
SIGNAL i_a34 : std_logic;
SIGNAL URTEM_aMux_32_rtl_99_a17 : std_logic;
SIGNAL URTEM_ai_a1138 : std_logic;
SIGNAL URTEM_aST_RTEM_a1_a_areg0 : std_logic;
SIGNAL UUMS2_ai_a5640 : std_logic;
SIGNAL UUMS2_aST_RTEM_a1_a_areg0 : std_logic;
SIGNAL i_a37 : std_logic;
SIGNAL UUMS2_ai_a5652 : std_logic;
SIGNAL UUMS2_aST_RTEM_a0_a_areg0 : std_logic;
SIGNAL URTEM_ai_a761 : std_logic;
SIGNAL URTEM_ai_a767 : std_logic;
SIGNAL URTEM_aST_RTEM_a0_a_areg0 : std_logic;
SIGNAL i_a40 : std_logic;
SIGNAL NOT_URTEM_aU_RT_IN_aimove : std_logic;
SIGNAL NOT_URTEM_aU_RT_OUT_aimove : std_logic;
SIGNAL NOT_imr_acombout : std_logic;

BEGIN

ww_RTEM_SEL <= RTEM_SEL;
ww_clk20 <= clk20;
ww_imr <= imr;
ww_Low_Speed <= Low_Speed;
ww_Med_Speed <= Med_Speed;
ww_Hi_Speed <= Hi_Speed;
ww_Cmd_Mid_In <= Cmd_Mid_In;
ww_Hi_Cnt <= Hi_Cnt;
ww_Det_Sat <= Det_Sat;
ww_CMD_IN <= CMD_IN;
ww_CMD_OUT <= CMD_OUT;
ww_Cmd_Mid_Out <= Cmd_Mid_Out;
ww_CNT_90 <= CNT_90;
ww_CNT_75 <= CNT_75;
ww_CNT_25 <= CNT_25;
ww_CNT_10 <= CNT_10;
ww_HC_MR <= HC_MR;
ww_RTEM_GRN <= RTEM_GRN;
ww_rtem_ini <= rtem_ini;
ww_RTEM_RED <= RTEM_RED;
ww_CNT_100 <= CNT_100;
ww_CNT_MAX <= CNT_MAX;
ww_CNT_110 <= CNT_110;
ww_RTEM_HS <= RTEM_HS;
ww_WD_EN <= WD_EN;
ww_ms100_tc <= ms100_tc;
ww_WD_MR <= WD_MR;
RTEM_ANALYZE <= ww_RTEM_ANALYZE;
RTEM_RETRACT <= ww_RTEM_RETRACT;
RTEM_CNT <= ww_RTEM_CNT;
ST_RTEM <= ww_ST_RTEM;
NOT_URTEM_aU_RT_IN_aimove <= NOT URTEM_aU_RT_IN_aimove;
NOT_URTEM_aU_RT_OUT_aimove <= NOT URTEM_aU_RT_OUT_aimove;
NOT_imr_acombout <= NOT imr_acombout;

clk20_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk20,
	combout => clk20_acombout);

imr_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT);

URTEM_aU_RT_IN_areduce_nor_35_a64_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a64 = URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a # URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a # URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a14_a,
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a13_a,
	datac => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a15_a,
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a64);

URTEM_aU_RT_IN_areduce_nor_35_a86_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a86 = URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a # URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a6_a,
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a5_a,
	datac => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a4_a,
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a86);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a $ !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a & !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a,
	cout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a_aCOUT);

URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a = DFFE(GLOBAL(URTEM_aU_RT_IN_aimove) & URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a_aCOUT $ URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a,
	cin => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_IN_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a);

URTEM_aU_RT_IN_areduce_nor_35_a59_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a59 = !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a18_a,
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a19_a,
	datac => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a17_a,
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a59);

URTEM_aU_RT_IN_areduce_nor_35_a73_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a73 = URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a # URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a # URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a9_a,
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a11_a,
	datac => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a8_a,
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a73);

URTEM_aU_RT_IN_areduce_nor_35_a134_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a134 = URTEM_aU_RT_IN_areduce_nor_35_a64 # URTEM_aU_RT_IN_areduce_nor_35_a86 # URTEM_aU_RT_IN_areduce_nor_35_a59 # URTEM_aU_RT_IN_areduce_nor_35_a73

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_35_a64,
	datab => URTEM_aU_RT_IN_areduce_nor_35_a86,
	datac => URTEM_aU_RT_IN_areduce_nor_35_a59,
	datad => URTEM_aU_RT_IN_areduce_nor_35_a73,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a134);

URTEM_aU_RT_IN_areduce_nor_35_a103_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_35_a103 = !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a # !URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a3_a,
	datab => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a0_a,
	datac => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a1_a,
	datad => URTEM_aU_RT_IN_aMv_Cnt_rtl_34_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_35_a103);

Hi_Cnt_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Cnt,
	combout => Hi_Cnt_acombout);

Det_Sat_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Det_Sat,
	combout => Det_Sat_acombout);

URTEM_ai_a1186_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1186 = Hi_Cnt_acombout # URTEM_aHi_Cnt_En # Det_Sat_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Cnt_acombout,
	datac => URTEM_aHi_Cnt_En,
	datad => Det_Sat_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a1186);

HC_MR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_HC_MR,
	combout => HC_MR_acombout);

CMD_OUT_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CMD_OUT,
	combout => CMD_OUT_acombout);

UUMS2_ai_a5573_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5573 = !Hi_Cnt_acombout & !Det_Sat_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Hi_Cnt_acombout,
	datad => Det_Sat_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5573);

RTEM_GRN_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RTEM_GRN,
	combout => RTEM_GRN_acombout);

rtem_ini_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(10),
	combout => rtem_ini_a10_a_acombout);

rtem_ini_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(9),
	combout => rtem_ini_a9_a_acombout);

UUMS2_aRTEM_IN_VEC_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_IN_VEC_a0_a = DFFE(RTEM_GRN_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => RTEM_GRN_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_IN_VEC_a0_a);

URTEM_aU_RT_OUT_artin_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a1_a = DFFE(UUMS2_aRTEM_IN_VEC_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => UUMS2_aRTEM_IN_VEC_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a1_a);

UUMS2_aRTEM_IN_VEC_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_IN_VEC_a2_a = DFFE(URTEM_aU_RT_OUT_artin_vec_a1_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_OUT_artin_vec_a1_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_IN_VEC_a2_a);

URTEM_aU_RT_OUT_artin_vec_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a3_a = DFFE(UUMS2_aRTEM_IN_VEC_a2_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aRTEM_IN_VEC_a2_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a3_a);

URTEM_aU_RT_OUT_artin_vec_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a4_a = DFFE(URTEM_aU_RT_OUT_artin_vec_a3_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_artin_vec_a3_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a4_a);

URTEM_aU_RT_OUT_artin_vec_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a5_a = DFFE(URTEM_aU_RT_OUT_artin_vec_a4_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_artin_vec_a4_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a5_a);

URTEM_aU_RT_OUT_artin_vec_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a6_a = DFFE(URTEM_aU_RT_OUT_artin_vec_a5_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_OUT_artin_vec_a5_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a6_a);

URTEM_aU_RT_OUT_artin_vec_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a7_a = DFFE(URTEM_aU_RT_OUT_artin_vec_a6_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_artin_vec_a6_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_vec_a7_a);

URTEM_aU_RT_OUT_areduce_nor_49_a28_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_49_a28 = URTEM_aU_RT_OUT_artin_vec_a5_a # URTEM_aU_RT_OUT_artin_vec_a6_a # URTEM_aU_RT_OUT_artin_vec_a7_a # URTEM_aU_RT_OUT_artin_vec_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_vec_a5_a,
	datab => URTEM_aU_RT_OUT_artin_vec_a6_a,
	datac => URTEM_aU_RT_OUT_artin_vec_a7_a,
	datad => URTEM_aU_RT_OUT_artin_vec_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_49_a28);

URTEM_aU_RT_OUT_areduce_nor_49_a23_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_49_a23 = URTEM_aU_RT_OUT_artin_vec_a1_a # URTEM_aU_RT_OUT_artin_vec_a3_a # UUMS2_aRTEM_IN_VEC_a0_a # UUMS2_aRTEM_IN_VEC_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_vec_a1_a,
	datab => URTEM_aU_RT_OUT_artin_vec_a3_a,
	datac => UUMS2_aRTEM_IN_VEC_a0_a,
	datad => UUMS2_aRTEM_IN_VEC_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_49_a23);

UUMS2_aSW_IN_DB_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSW_IN_DB = DFFE(URTEM_aU_RT_OUT_artin_vec_a4_a_a109, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_artin_vec_a4_a_a109 = URTEM_aU_RT_OUT_artin_vec_a1_a & URTEM_aU_RT_OUT_artin_vec_a3_a & UUMS2_aRTEM_IN_VEC_a0_a & UUMS2_aRTEM_IN_VEC_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_vec_a1_a,
	datab => URTEM_aU_RT_OUT_artin_vec_a3_a,
	datac => UUMS2_aRTEM_IN_VEC_a0_a,
	datad => UUMS2_aRTEM_IN_VEC_a2_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aSW_IN_DB,
	cascout => URTEM_aU_RT_OUT_artin_vec_a4_a_a109);

URTEM_aU_RT_OUT_artin_vec_a4_a_a137_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_vec_a4_a_a137 = (URTEM_aU_RT_OUT_artin_vec_a5_a & URTEM_aU_RT_OUT_artin_vec_a6_a & URTEM_aU_RT_OUT_artin_vec_a7_a & URTEM_aU_RT_OUT_artin_vec_a4_a) & CASCADE(URTEM_aU_RT_OUT_artin_vec_a4_a_a109)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_vec_a5_a,
	datab => URTEM_aU_RT_OUT_artin_vec_a6_a,
	datac => URTEM_aU_RT_OUT_artin_vec_a7_a,
	datad => URTEM_aU_RT_OUT_artin_vec_a4_a,
	cascin => URTEM_aU_RT_OUT_artin_vec_a4_a_a109,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_artin_vec_a4_a_a137);

URTEM_aU_RT_OUT_aRTIN_DB_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aRTIN_DB = DFFE(URTEM_aU_RT_OUT_artin_vec_a4_a_a137 # URTEM_aU_RT_OUT_aRTIN_DB & (URTEM_aU_RT_OUT_areduce_nor_49_a28 # URTEM_aU_RT_OUT_areduce_nor_49_a23), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_49_a28,
	datab => URTEM_aU_RT_OUT_aRTIN_DB,
	datac => URTEM_aU_RT_OUT_areduce_nor_49_a23,
	datad => URTEM_aU_RT_OUT_artin_vec_a4_a_a137,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aRTIN_DB);

URTEM_aU_RT_OUT_aRTIN_DB_DEL_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aRTIN_DB_DEL = DFFE(URTEM_aU_RT_OUT_aRTIN_DB, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_OUT_aRTIN_DB,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aRTIN_DB_DEL);

URTEM_aU_RT_OUT_ai_a138_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_ai_a138 = URTEM_aU_RT_OUT_areduce_nor_4_a55 # URTEM_aU_RT_OUT_aRTIN_DB_DEL $ URTEM_aU_RT_OUT_aRTIN_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aRTIN_DB_DEL,
	datac => URTEM_aU_RT_OUT_aRTIN_DB,
	datad => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_ai_a138);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a $ URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a $ !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a $ URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a $ !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_OUT_areduce_nor_4_a38_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_4_a38 = URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a2_a,
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a3_a,
	datac => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a4_a,
	datad => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_4_a38);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a $ URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a $ !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a $ URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a = DFFE(!URTEM_aU_RT_OUT_ai_a138 & URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a_aCOUT $ !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a);

URTEM_aU_RT_OUT_areduce_nor_4_a33_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_4_a33 = URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a # !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFD",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a8_a,
	datab => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a6_a,
	datac => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a7_a,
	datad => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_4_a33);

URTEM_aU_RT_OUT_areduce_nor_4_a55_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_4_a55 = !URTEM_aU_RT_OUT_areduce_nor_4_a38 & !URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a & !URTEM_aU_RT_OUT_areduce_nor_4_a33

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a38,
	datac => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a,
	datad => URTEM_aU_RT_OUT_areduce_nor_4_a33,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_4_a55);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a,
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a,
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT);

rtem_ini_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(11),
	combout => rtem_ini_a11_a_acombout);

rtem_ini_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(4),
	combout => rtem_ini_a4_a_acombout);

URTEM_aU_RT_OUT_aflash_tc_a16_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a16 = URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a $ rtem_ini_a4_a_acombout # !rtem_ini_a11_a_acombout) # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a & (rtem_ini_a11_a_acombout # URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a $ rtem_ini_a4_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a11_a,
	datab => rtem_ini_a11_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a,
	datad => rtem_ini_a4_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a16);

URTEM_aU_RT_OUT_aflash_tc_a96_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a96 = URTEM_aU_RT_OUT_aflash_tc_a16 # URTEM_aU_RT_OUT_areduce_nor_4_a38 # URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a # URTEM_aU_RT_OUT_areduce_nor_4_a33

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_tc_a16,
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a38,
	datac => URTEM_aU_RT_OUT_aFlash_lo_Cnt_rtl_39_awysi_counter_asload_path_a0_a,
	datad => URTEM_aU_RT_OUT_areduce_nor_4_a33,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a96);

rtl_a1_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a1 = URTEM_aU_RT_OUT_aRTIN_DB_DEL $ URTEM_aU_RT_OUT_aRTIN_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3C3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aRTIN_DB_DEL,
	datac => URTEM_aU_RT_OUT_aRTIN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a1);

rtem_ini_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(6),
	combout => rtem_ini_a6_a_acombout);

rtem_ini_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(14),
	combout => rtem_ini_a14_a_acombout);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a,
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a_aCOUT);

URTEM_aU_RT_OUT_aflash_tc_a24_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a24 = rtem_ini_a6_a_acombout & (rtem_ini_a14_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a) # !rtem_ini_a6_a_acombout & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a # rtem_ini_a14_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a6_a_acombout,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a,
	datac => rtem_ini_a14_a_acombout,
	datad => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a24);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a $ (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a_aCOUT & URTEM_aU_RT_OUT_areduce_nor_4_a55), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a,
	datad => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a);

rtem_ini_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(2),
	combout => rtem_ini_a2_a_acombout);

rtem_ini_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(15),
	combout => rtem_ini_a15_a_acombout);

URTEM_aU_RT_OUT_aflash_tc_a40_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a40 = URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a & (rtem_ini_a2_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a # !rtem_ini_a15_a_acombout) # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a & (rtem_ini_a15_a_acombout # rtem_ini_a2_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a15_a,
	datab => rtem_ini_a2_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a,
	datad => rtem_ini_a15_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a40);

rtem_ini_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(13),
	combout => rtem_ini_a13_a_acombout);

rtem_ini_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(5),
	combout => rtem_ini_a5_a_acombout);

URTEM_aU_RT_OUT_aflash_tc_a31_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a31 = URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a $ rtem_ini_a5_a_acombout # !rtem_ini_a13_a_acombout) # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a & (rtem_ini_a13_a_acombout # URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a $ rtem_ini_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a13_a,
	datab => rtem_ini_a13_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a,
	datad => rtem_ini_a5_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a31);

rtem_ini_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(0),
	combout => rtem_ini_a0_a_acombout);

rtem_ini_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(3),
	combout => rtem_ini_a3_a_acombout);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_areduce_nor_4_a55 $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_OUT_aflash_tc_a19_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a19 = rtem_ini_a0_a_acombout & (rtem_ini_a3_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a) # !rtem_ini_a0_a_acombout & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a # rtem_ini_a3_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a0_a_acombout,
	datab => rtem_ini_a3_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a,
	datad => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a19);

URTEM_aU_RT_OUT_aflash_tc_a99_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a99 = URTEM_aU_RT_OUT_aflash_tc_a24 # URTEM_aU_RT_OUT_aflash_tc_a40 # URTEM_aU_RT_OUT_aflash_tc_a31 # URTEM_aU_RT_OUT_aflash_tc_a19

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_tc_a24,
	datab => URTEM_aU_RT_OUT_aflash_tc_a40,
	datac => URTEM_aU_RT_OUT_aflash_tc_a31,
	datad => URTEM_aU_RT_OUT_aflash_tc_a19,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a99);

URTEM_aU_RT_OUT_ai_a156_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_ai_a156 = rtl_a1 # !URTEM_aU_RT_OUT_aflash_tc_a108 & !URTEM_aU_RT_OUT_aflash_tc_a96 & !URTEM_aU_RT_OUT_aflash_tc_a99

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F1",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_tc_a108,
	datab => URTEM_aU_RT_OUT_aflash_tc_a96,
	datac => rtl_a1,
	datad => URTEM_aU_RT_OUT_aflash_tc_a99,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_ai_a156);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a = DFFE(!URTEM_aU_RT_OUT_ai_a156 & URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a $ (URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a & !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datab => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_OUT_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a,
	cout => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_acounter_cell_a8_a_aCOUT);

URTEM_aU_RT_OUT_aflash_tc_a64_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a64 = rtem_ini_a10_a_acombout & (rtem_ini_a9_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a) # !rtem_ini_a10_a_acombout & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a # rtem_ini_a9_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a10_a_acombout,
	datab => rtem_ini_a9_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a9_a,
	datad => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a64);

rtem_ini_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(1),
	combout => rtem_ini_a1_a_acombout);

rtem_ini_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(12),
	combout => rtem_ini_a12_a_acombout);

URTEM_aU_RT_OUT_aflash_tc_a51_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a51 = rtem_ini_a1_a_acombout & (rtem_ini_a12_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a) # !rtem_ini_a1_a_acombout & (URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a # rtem_ini_a12_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a1_a_acombout,
	datab => rtem_ini_a12_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a1_a,
	datad => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a51);

rtem_ini_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(7),
	combout => rtem_ini_a7_a_acombout);

rtem_ini_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_rtem_ini(8),
	combout => rtem_ini_a8_a_acombout);

URTEM_aU_RT_OUT_aflash_tc_a79_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a79 = URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a & (rtem_ini_a7_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a # !rtem_ini_a8_a_acombout) # !URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a & (rtem_ini_a8_a_acombout # rtem_ini_a7_a_acombout $ URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a8_a,
	datab => rtem_ini_a7_a_acombout,
	datac => URTEM_aU_RT_OUT_aflash_cnt_rtl_40_awysi_counter_asload_path_a7_a,
	datad => rtem_ini_a8_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a79);

URTEM_aU_RT_OUT_aflash_tc_a108_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a108 = URTEM_aU_RT_OUT_aflash_tc_a64 # URTEM_aU_RT_OUT_aflash_tc_a51 # URTEM_aU_RT_OUT_aflash_tc_a79

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aflash_tc_a64,
	datac => URTEM_aU_RT_OUT_aflash_tc_a51,
	datad => URTEM_aU_RT_OUT_aflash_tc_a79,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a108);

URTEM_aU_RT_OUT_aflash_tc_a122_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aflash_tc_a122 = !URTEM_aU_RT_OUT_aflash_tc_a108 & URTEM_aU_RT_OUT_areduce_nor_4_a55 & !URTEM_aU_RT_OUT_aflash_tc_a16 & !URTEM_aU_RT_OUT_aflash_tc_a99

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aflash_tc_a108,
	datab => URTEM_aU_RT_OUT_areduce_nor_4_a55,
	datac => URTEM_aU_RT_OUT_aflash_tc_a16,
	datad => URTEM_aU_RT_OUT_aflash_tc_a99,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_aflash_tc_a122);

URTEM_aU_RT_OUT_artin_reg_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_artin_reg = DFFE(RTEM_GRN_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , URTEM_aU_RT_OUT_aflash_tc_a122)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => RTEM_GRN_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => URTEM_aU_RT_OUT_aflash_tc_a122,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_artin_reg);

URTEM_aU_RT_OUT_aiAC_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aiAC = DFFE(URTEM_aU_RT_OUT_artin_reg $ RTEM_GRN_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , URTEM_aU_RT_OUT_aflash_tc_a122)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_OUT_artin_reg,
	datad => RTEM_GRN_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => URTEM_aU_RT_OUT_aflash_tc_a122,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aiAC);

RTEM_RED_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RTEM_RED,
	combout => RTEM_RED_acombout);

UUMS2_aRTEM_OUT_VEC_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_OUT_VEC_a0_a = DFFE(RTEM_RED_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => RTEM_RED_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_OUT_VEC_a0_a);

URTEM_aU_RT_IN_artin_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a1_a = DFFE(UUMS2_aRTEM_OUT_VEC_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aRTEM_OUT_VEC_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a1_a);

UUMS2_aRTEM_OUT_VEC_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_OUT_VEC_a2_a = DFFE(URTEM_aU_RT_IN_artin_vec_a1_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_artin_vec_a1_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_OUT_VEC_a2_a);

URTEM_aU_RT_IN_artin_vec_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a3_a = DFFE(UUMS2_aRTEM_OUT_VEC_a2_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aRTEM_OUT_VEC_a2_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a3_a);

URTEM_aU_RT_IN_areduce_nor_49_a23_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_49_a23 = URTEM_aU_RT_IN_artin_vec_a1_a # UUMS2_aRTEM_OUT_VEC_a2_a # URTEM_aU_RT_IN_artin_vec_a3_a # UUMS2_aRTEM_OUT_VEC_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_artin_vec_a1_a,
	datab => UUMS2_aRTEM_OUT_VEC_a2_a,
	datac => URTEM_aU_RT_IN_artin_vec_a3_a,
	datad => UUMS2_aRTEM_OUT_VEC_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_49_a23);

URTEM_aU_RT_IN_artin_vec_a4_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a4_a = DFFE(URTEM_aU_RT_IN_artin_vec_a3_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_artin_vec_a3_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a4_a);

URTEM_aU_RT_IN_artin_vec_a5_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a5_a = DFFE(URTEM_aU_RT_IN_artin_vec_a4_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_artin_vec_a4_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a5_a);

URTEM_aU_RT_IN_artin_vec_a6_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a6_a = DFFE(URTEM_aU_RT_IN_artin_vec_a5_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_IN_artin_vec_a5_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a6_a);

URTEM_aU_RT_IN_artin_vec_a7_a_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a7_a = DFFE(URTEM_aU_RT_IN_artin_vec_a6_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_artin_vec_a6_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_vec_a7_a);

URTEM_aU_RT_IN_areduce_nor_49_a28_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_49_a28 = URTEM_aU_RT_IN_artin_vec_a5_a # URTEM_aU_RT_IN_artin_vec_a7_a # URTEM_aU_RT_IN_artin_vec_a4_a # URTEM_aU_RT_IN_artin_vec_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_artin_vec_a5_a,
	datab => URTEM_aU_RT_IN_artin_vec_a7_a,
	datac => URTEM_aU_RT_IN_artin_vec_a4_a,
	datad => URTEM_aU_RT_IN_artin_vec_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_49_a28);

UUMS2_aSW_OUT_DB_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSW_OUT_DB = DFFE(URTEM_aU_RT_IN_artin_vec_a4_a_a109, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_artin_vec_a4_a_a109 = URTEM_aU_RT_IN_artin_vec_a1_a & UUMS2_aRTEM_OUT_VEC_a2_a & URTEM_aU_RT_IN_artin_vec_a3_a & UUMS2_aRTEM_OUT_VEC_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_artin_vec_a1_a,
	datab => UUMS2_aRTEM_OUT_VEC_a2_a,
	datac => URTEM_aU_RT_IN_artin_vec_a3_a,
	datad => UUMS2_aRTEM_OUT_VEC_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aSW_OUT_DB,
	cascout => URTEM_aU_RT_IN_artin_vec_a4_a_a109);

URTEM_aU_RT_IN_artin_vec_a4_a_a137_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_vec_a4_a_a137 = (URTEM_aU_RT_IN_artin_vec_a5_a & URTEM_aU_RT_IN_artin_vec_a7_a & URTEM_aU_RT_IN_artin_vec_a4_a & URTEM_aU_RT_IN_artin_vec_a6_a) & CASCADE(URTEM_aU_RT_IN_artin_vec_a4_a_a109)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_artin_vec_a5_a,
	datab => URTEM_aU_RT_IN_artin_vec_a7_a,
	datac => URTEM_aU_RT_IN_artin_vec_a4_a,
	datad => URTEM_aU_RT_IN_artin_vec_a6_a,
	cascin => URTEM_aU_RT_IN_artin_vec_a4_a_a109,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_artin_vec_a4_a_a137);

URTEM_aU_RT_IN_aRTIN_DB_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aRTIN_DB = DFFE(URTEM_aU_RT_IN_artin_vec_a4_a_a137 # URTEM_aU_RT_IN_aRTIN_DB & (URTEM_aU_RT_IN_areduce_nor_49_a23 # URTEM_aU_RT_IN_areduce_nor_49_a28), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFA8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aRTIN_DB,
	datab => URTEM_aU_RT_IN_areduce_nor_49_a23,
	datac => URTEM_aU_RT_IN_areduce_nor_49_a28,
	datad => URTEM_aU_RT_IN_artin_vec_a4_a_a137,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aRTIN_DB);

URTEM_aU_RT_IN_aRTIN_DB_DEL_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aRTIN_DB_DEL = DFFE(URTEM_aU_RT_IN_aRTIN_DB, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_IN_aRTIN_DB,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aRTIN_DB_DEL);

URTEM_aU_RT_IN_ai_a138_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_ai_a138 = URTEM_aU_RT_IN_areduce_nor_4_a55 # URTEM_aU_RT_IN_aRTIN_DB_DEL $ URTEM_aU_RT_IN_aRTIN_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF3C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aRTIN_DB_DEL,
	datac => URTEM_aU_RT_IN_aRTIN_DB,
	datad => URTEM_aU_RT_IN_areduce_nor_4_a55,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_ai_a138);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a $ URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a $ !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a $ URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a $ !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a $ URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a $ !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a $ URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a = DFFE(!URTEM_aU_RT_IN_ai_a138 & URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a_aCOUT $ !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a138,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a);

URTEM_aU_RT_IN_areduce_nor_4_a33_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_4_a33 = URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a # !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a7_a,
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a8_a,
	datac => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a5_a,
	datad => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_4_a33);

URTEM_aU_RT_IN_areduce_nor_4_a38_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_4_a38 = URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a3_a,
	datab => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a4_a,
	datac => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a1_a,
	datad => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_4_a38);

URTEM_aU_RT_IN_areduce_nor_4_a55_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_areduce_nor_4_a55 = !URTEM_aU_RT_IN_areduce_nor_4_a33 & !URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a & !URTEM_aU_RT_IN_areduce_nor_4_a38

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a33,
	datac => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a,
	datad => URTEM_aU_RT_IN_areduce_nor_4_a38,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_areduce_nor_4_a55);

rtl_a0_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a0 = URTEM_aU_RT_IN_aRTIN_DB $ URTEM_aU_RT_IN_aRTIN_DB_DEL

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_IN_aRTIN_DB,
	datad => URTEM_aU_RT_IN_aRTIN_DB_DEL,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a0);

URTEM_aU_RT_IN_aflash_tc_a96_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a96 = URTEM_aU_RT_IN_areduce_nor_4_a33 # URTEM_aU_RT_IN_aflash_tc_a16 # URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a # URTEM_aU_RT_IN_areduce_nor_4_a38

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a33,
	datab => URTEM_aU_RT_IN_aflash_tc_a16,
	datac => URTEM_aU_RT_IN_aFlash_lo_Cnt_rtl_37_awysi_counter_asload_path_a0_a,
	datad => URTEM_aU_RT_IN_areduce_nor_4_a38,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a96);

URTEM_aU_RT_IN_aflash_tc_a79_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a79 = URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a & (rtem_ini_a7_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a # !rtem_ini_a8_a_acombout) # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a & (rtem_ini_a8_a_acombout # rtem_ini_a7_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a,
	datab => rtem_ini_a7_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a,
	datad => rtem_ini_a8_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a79);

URTEM_aU_RT_IN_aflash_tc_a64_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a64 = URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a $ rtem_ini_a9_a_acombout # !rtem_ini_a10_a_acombout) # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a & (rtem_ini_a10_a_acombout # URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a $ rtem_ini_a9_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a,
	datab => rtem_ini_a10_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a,
	datad => rtem_ini_a9_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a64);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT);

URTEM_aU_RT_IN_aflash_tc_a51_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a51 = rtem_ini_a1_a_acombout & (rtem_ini_a12_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a) # !rtem_ini_a1_a_acombout & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a # rtem_ini_a12_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a1_a_acombout,
	datab => rtem_ini_a12_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a,
	datad => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a51);

URTEM_aU_RT_IN_aflash_tc_a108_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a108 = URTEM_aU_RT_IN_aflash_tc_a79 # URTEM_aU_RT_IN_aflash_tc_a64 # URTEM_aU_RT_IN_aflash_tc_a51

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aflash_tc_a79,
	datac => URTEM_aU_RT_IN_aflash_tc_a64,
	datad => URTEM_aU_RT_IN_aflash_tc_a51,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a108);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a_aCOUT);

URTEM_aU_RT_IN_aflash_tc_a24_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a24 = rtem_ini_a6_a_acombout & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a $ rtem_ini_a14_a_acombout # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a) # !rtem_ini_a6_a_acombout & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a # URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a $ rtem_ini_a14_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a6_a_acombout,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a14_a,
	datac => rtem_ini_a14_a_acombout,
	datad => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a24);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datad => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a);

URTEM_aU_RT_IN_aflash_tc_a40_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a40 = URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a & (rtem_ini_a2_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a # !rtem_ini_a15_a_acombout) # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a & (rtem_ini_a15_a_acombout # rtem_ini_a2_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a15_a,
	datab => rtem_ini_a2_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a,
	datad => rtem_ini_a15_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a40);

URTEM_aU_RT_IN_aflash_tc_a31_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a31 = URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a $ rtem_ini_a5_a_acombout # !rtem_ini_a13_a_acombout) # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a & (rtem_ini_a13_a_acombout # URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a $ rtem_ini_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "6FF6",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a13_a,
	datab => rtem_ini_a13_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a,
	datad => rtem_ini_a5_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a31);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_areduce_nor_4_a55 $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_IN_aflash_tc_a19_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a19 = rtem_ini_a0_a_acombout & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a $ rtem_ini_a3_a_acombout # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a) # !rtem_ini_a0_a_acombout & (URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a # URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a $ rtem_ini_a3_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtem_ini_a0_a_acombout,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a,
	datac => rtem_ini_a3_a_acombout,
	datad => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a19);

URTEM_aU_RT_IN_aflash_tc_a99_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a99 = URTEM_aU_RT_IN_aflash_tc_a24 # URTEM_aU_RT_IN_aflash_tc_a40 # URTEM_aU_RT_IN_aflash_tc_a31 # URTEM_aU_RT_IN_aflash_tc_a19

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_tc_a24,
	datab => URTEM_aU_RT_IN_aflash_tc_a40,
	datac => URTEM_aU_RT_IN_aflash_tc_a31,
	datad => URTEM_aU_RT_IN_aflash_tc_a19,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a99);

URTEM_aU_RT_IN_ai_a156_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_ai_a156 = rtl_a0 # !URTEM_aU_RT_IN_aflash_tc_a96 & !URTEM_aU_RT_IN_aflash_tc_a108 & !URTEM_aU_RT_IN_aflash_tc_a99

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAAB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a0,
	datab => URTEM_aU_RT_IN_aflash_tc_a96,
	datac => URTEM_aU_RT_IN_aflash_tc_a108,
	datad => URTEM_aU_RT_IN_aflash_tc_a99,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_ai_a156);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a,
	datab => URTEM_aU_RT_IN_areduce_nor_4_a55,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a8_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a9_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT);

URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a = DFFE(!URTEM_aU_RT_IN_ai_a156 & URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a $ (URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a & !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datab => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a,
	cin => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => URTEM_aU_RT_IN_ai_a156,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a10_a,
	cout => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_acounter_cell_a10_a_aCOUT);

URTEM_aU_RT_IN_aflash_tc_a16_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a16 = URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a & (rtem_ini_a4_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a # !rtem_ini_a11_a_acombout) # !URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a & (rtem_ini_a11_a_acombout # rtem_ini_a4_a_acombout $ URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a11_a,
	datab => rtem_ini_a4_a_acombout,
	datac => URTEM_aU_RT_IN_aflash_cnt_rtl_38_awysi_counter_asload_path_a4_a,
	datad => rtem_ini_a11_a_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a16);

URTEM_aU_RT_IN_aflash_tc_a122_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aflash_tc_a122 = !URTEM_aU_RT_IN_aflash_tc_a16 & URTEM_aU_RT_IN_areduce_nor_4_a55 & !URTEM_aU_RT_IN_aflash_tc_a108 & !URTEM_aU_RT_IN_aflash_tc_a99

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aflash_tc_a16,
	datab => URTEM_aU_RT_IN_areduce_nor_4_a55,
	datac => URTEM_aU_RT_IN_aflash_tc_a108,
	datad => URTEM_aU_RT_IN_aflash_tc_a99,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_aflash_tc_a122);

URTEM_aU_RT_IN_artin_reg_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_artin_reg = DFFE(RTEM_RED_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , URTEM_aU_RT_IN_aflash_tc_a122)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => RTEM_RED_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => URTEM_aU_RT_IN_aflash_tc_a122,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_artin_reg);

URTEM_aU_RT_IN_aiAC_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aiAC = DFFE(URTEM_aU_RT_IN_artin_reg $ RTEM_RED_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , URTEM_aU_RT_IN_aflash_tc_a122)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => URTEM_aU_RT_IN_artin_reg,
	datad => RTEM_RED_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => URTEM_aU_RT_IN_aflash_tc_a122,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aiAC);

URTEM_ai_a1192_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1192 = URTEM_aU_RT_IN_aiAC & URTEM_aRTEM_OUT & URTEM_aU_RT_OUT_artin_reg # !URTEM_aU_RT_IN_aiAC & (URTEM_aU_RT_IN_artin_reg # URTEM_aRTEM_OUT & !URTEM_aU_RT_OUT_artin_reg)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B382",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aRTEM_OUT,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aU_RT_OUT_artin_reg,
	datad => URTEM_aU_RT_IN_artin_reg,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a1192);

URTEM_aRTEM_OUT_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aRTEM_OUT = DFFE(URTEM_aHi_Cnt_State # URTEM_aU_RT_OUT_aiAC # URTEM_ai_a1192, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aHi_Cnt_State,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_ai_a1192,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aRTEM_OUT);

ms100_tc_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_ms100_tc,
	combout => ms100_tc_acombout);

ms100_tc_vec_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- ms100_tc_vec_a0_a = DFFE(ms100_tc_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => ms100_tc_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_tc_vec_a0_a);

ms100_tc_vec_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- ms100_tc_vec_a1_a = DFFE(ms100_tc_vec_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => ms100_tc_vec_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => ms100_tc_vec_a1_a);

reduce_nor_24_aI : apex20ke_lcell 
-- Equation(s):
-- reduce_nor_24 = ms100_tc_vec_a0_a & !ms100_tc_vec_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => ms100_tc_vec_a0_a,
	datad => ms100_tc_vec_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => reduce_nor_24);

WD_MR_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WD_MR,
	combout => WD_MR_acombout);

WD_MR_VEC_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- WD_MR_VEC_a0_a = DFFE(WD_MR_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => WD_MR_acombout,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_MR_VEC_a0_a);

WD_EN_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_WD_EN,
	combout => WD_EN_acombout);

WD_MR_VEC_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- WD_MR_VEC_a1_a = DFFE(WD_MR_VEC_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => WD_MR_VEC_a0_a,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_MR_VEC_a1_a);

i_a6_I : apex20ke_lcell 
-- Equation(s):
-- i_a6 = WD_MR_VEC_a0_a & !WD_MR_VEC_a1_a # !WD_EN_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0FCF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => WD_MR_VEC_a0_a,
	datac => WD_EN_acombout,
	datad => WD_MR_VEC_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a6);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a0_a = DFFE(!i_a6 & reduce_nor_24 $ WD_CNT_rtl_32_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(WD_CNT_rtl_32_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a0_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a1_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a1_a $ (reduce_nor_24 & WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT # !WD_CNT_rtl_32_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a1_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a1_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a2_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a2_a $ (reduce_nor_24 & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(WD_CNT_rtl_32_awysi_counter_asload_path_a2_a & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a2_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a2_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a3_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a3_a $ (reduce_nor_24 & WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT # !WD_CNT_rtl_32_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a3_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a3_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a4_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a4_a $ (reduce_nor_24 & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(WD_CNT_rtl_32_awysi_counter_asload_path_a4_a & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a4_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a4_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a5_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a5_a $ (reduce_nor_24 & WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT # !WD_CNT_rtl_32_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a5_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a5_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a6_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a6_a $ (reduce_nor_24 & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(WD_CNT_rtl_32_awysi_counter_asload_path_a6_a & !WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a6_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a6_a,
	cout => WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a_aCOUT);

WD_CNT_rtl_32_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- WD_CNT_rtl_32_awysi_counter_asload_path_a7_a = DFFE(!i_a6 & WD_CNT_rtl_32_awysi_counter_asload_path_a7_a $ (reduce_nor_24 & WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5FA0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => reduce_nor_24,
	datad => WD_CNT_rtl_32_awysi_counter_asload_path_a7_a,
	cin => WD_CNT_rtl_32_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => i_a6,
	devclrn => devclrn,
	devpor => devpor,
	regout => WD_CNT_rtl_32_awysi_counter_asload_path_a7_a);

Cmd_Wd_a48_I : apex20ke_lcell 
-- Equation(s):
-- Cmd_Wd_a48 = WD_CNT_rtl_32_awysi_counter_asload_path_a2_a & !WD_CNT_rtl_32_awysi_counter_asload_path_a3_a & !WD_CNT_rtl_32_awysi_counter_asload_path_a1_a & WD_CNT_rtl_32_awysi_counter_asload_path_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0200",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => WD_CNT_rtl_32_awysi_counter_asload_path_a2_a,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a3_a,
	datac => WD_CNT_rtl_32_awysi_counter_asload_path_a1_a,
	datad => WD_CNT_rtl_32_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cascout => Cmd_Wd_a48);

Cmd_Wd_a79_I : apex20ke_lcell 
-- Equation(s):
-- Cmd_Wd_a79 = (WD_EN_acombout & ms100_tc_acombout) & CASCADE(Cmd_Wd_a48)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0A0",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => WD_EN_acombout,
	datac => ms100_tc_acombout,
	cascin => Cmd_Wd_a48,
	devclrn => devclrn,
	devpor => devpor,
	cascout => Cmd_Wd_a79);

Cmd_Wd_a78_I : apex20ke_lcell 
-- Equation(s):
-- Cmd_Wd_a78 = (!WD_CNT_rtl_32_awysi_counter_asload_path_a6_a & WD_CNT_rtl_32_awysi_counter_asload_path_a7_a & !WD_CNT_rtl_32_awysi_counter_asload_path_a5_a & WD_CNT_rtl_32_awysi_counter_asload_path_a4_a) & CASCADE(Cmd_Wd_a79)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => WD_CNT_rtl_32_awysi_counter_asload_path_a6_a,
	datab => WD_CNT_rtl_32_awysi_counter_asload_path_a7_a,
	datac => WD_CNT_rtl_32_awysi_counter_asload_path_a5_a,
	datad => WD_CNT_rtl_32_awysi_counter_asload_path_a4_a,
	cascin => Cmd_Wd_a79,
	devclrn => devclrn,
	devpor => devpor,
	combout => Cmd_Wd_a78);

URTEM_aRTEM_OUT_CMD_a27_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aRTEM_OUT_CMD_a27 = !URTEM_aRTEM_OUT & (Cmd_Wd_a78 # !URTEM_aHi_Cnt_En & !UUMS2_ai_a5573)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F01",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aHi_Cnt_En,
	datab => UUMS2_ai_a5573,
	datac => URTEM_aRTEM_OUT,
	datad => Cmd_Wd_a78,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aRTEM_OUT_CMD_a27);

URTEM_aHi_Cnt_En_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aHi_Cnt_En = DFFE(URTEM_ai_a1186 & !HC_MR_acombout & !CMD_OUT_acombout & !URTEM_aRTEM_OUT_CMD_a27, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0002",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_ai_a1186,
	datab => HC_MR_acombout,
	datac => CMD_OUT_acombout,
	datad => URTEM_aRTEM_OUT_CMD_a27,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aHi_Cnt_En);

URTEM_aU_RT_IN_ai_a264_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_ai_a264 = URTEM_aU_RT_IN_artin_reg # URTEM_aU_RT_IN_aiAC
-- URTEM_aU_RT_IN_ai_a492 = URTEM_aU_RT_IN_artin_reg # URTEM_aU_RT_IN_aiAC

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAFA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_artin_reg,
	datac => URTEM_aU_RT_IN_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_ai_a264,
	cascout => URTEM_aU_RT_IN_ai_a492);

URTEM_aMux_31_rtl_94_a9_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aMux_31_rtl_94_a9 = !URTEM_aU_RT_IN_aiAC & !URTEM_aU_RT_OUT_artin_reg & !URTEM_aU_RT_OUT_aiAC

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0101",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aiAC,
	datab => URTEM_aU_RT_OUT_artin_reg,
	datac => URTEM_aU_RT_OUT_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aMux_31_rtl_94_a9);

URTEM_ai_a1051_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1051 = !HC_MR_acombout & !CMD_OUT_acombout & !URTEM_aRTEM_OUT_CMD_a27

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => HC_MR_acombout,
	datac => CMD_OUT_acombout,
	datad => URTEM_aRTEM_OUT_CMD_a27,
	devclrn => devclrn,
	devpor => devpor,
	cascout => URTEM_ai_a1051);

URTEM_aHi_Cnt_State_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aHi_Cnt_State = DFFE((URTEM_aHi_Cnt_State # URTEM_aHi_Cnt_En & URTEM_aU_RT_IN_ai_a264 & URTEM_aMux_31_rtl_94_a9) & CASCADE(URTEM_ai_a1051), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F8F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aHi_Cnt_En,
	datab => URTEM_aU_RT_IN_ai_a264,
	datac => URTEM_aHi_Cnt_State,
	datad => URTEM_aMux_31_rtl_94_a9,
	cascin => URTEM_ai_a1051,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aHi_Cnt_State);

CMD_IN_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CMD_IN,
	combout => CMD_IN_acombout);

URTEM_aU_RT_IN_ai_a265_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_ai_a265 = !URTEM_aU_RT_IN_aimove & URTEM_aU_RT_IN_amv_cmdb

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aimove,
	datad => URTEM_aU_RT_IN_amv_cmdb,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_IN_ai_a265);

URTEM_aU_RT_IN_amv_cmdb_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_amv_cmdb = DFFE(URTEM_aU_RT_IN_ai_a265 # !URTEM_aHi_Cnt_State & !URTEM_aHi_Cnt_En & CMD_IN_acombout, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF10",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aHi_Cnt_State,
	datab => URTEM_aHi_Cnt_En,
	datac => CMD_IN_acombout,
	datad => URTEM_aU_RT_IN_ai_a265,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_amv_cmdb);

URTEM_aU_RT_IN_aimove_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_aimove = DFFE(URTEM_aU_RT_IN_amv_cmdb # URTEM_aU_RT_IN_aimove & (URTEM_aU_RT_IN_areduce_nor_35_a134 # URTEM_aU_RT_IN_areduce_nor_35_a103), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFC8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_areduce_nor_35_a134,
	datab => URTEM_aU_RT_IN_aimove,
	datac => URTEM_aU_RT_IN_areduce_nor_35_a103,
	datad => URTEM_aU_RT_IN_amv_cmdb,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_aimove);

URTEM_aU_RT_IN_amove_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_IN_amove_areg0 = DFFE(URTEM_aU_RT_IN_aimove, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_IN_aimove,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_IN_amove_areg0);

RTEM_SEL_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RTEM_SEL,
	combout => RTEM_SEL_acombout);

CNT_100_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(31),
	combout => CNT_100_a31_a_acombout);

RTEM_HS_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_RTEM_HS,
	combout => RTEM_HS_acombout);

UUMS2_aRTEM_HS_VEC_a0_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_HS_VEC_a0_a = DFFE(RTEM_HS_acombout, GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => RTEM_HS_acombout,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_HS_VEC_a0_a);

UUMS2_aRTEM_HS_VEC_a1_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_HS_VEC_a1_a = DFFE(UUMS2_aRTEM_HS_VEC_a0_a, GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aRTEM_HS_VEC_a0_a,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_HS_VEC_a1_a);

UUMS2_aRTEM_HS_VEC_a2_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_HS_VEC_a2_a = DFFE(UUMS2_aRTEM_HS_VEC_a1_a, GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => UUMS2_aRTEM_HS_VEC_a1_a,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_HS_VEC_a2_a);

UUMS2_aRTEM_HS_VEC_a3_a_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aRTEM_HS_VEC_a3_a = DFFE(UUMS2_aRTEM_HS_VEC_a2_a, GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => UUMS2_aRTEM_HS_VEC_a2_a,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aRTEM_HS_VEC_a3_a);

UUMS2_aSW_HS_DB_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSW_HS_DB = DFFE(!UUMS2_aRTEM_HS_VEC_a2_a # !UUMS2_aRTEM_HS_VEC_a0_a # !UUMS2_aRTEM_HS_VEC_a1_a # !UUMS2_aRTEM_HS_VEC_a3_a, GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aRTEM_HS_VEC_a3_a,
	datab => UUMS2_aRTEM_HS_VEC_a1_a,
	datac => UUMS2_aRTEM_HS_VEC_a0_a,
	datad => UUMS2_aRTEM_HS_VEC_a2_a,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aSW_HS_DB);

UUMS2_aSelect_1466_rtl_1_rtl_17_a83_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a83 = !UUMS2_aSW_HS_DB & (!UUMS2_aSW_OUT_DB # !UUMS2_aSW_IN_DB)
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a151 = !UUMS2_aSW_HS_DB & (!UUMS2_aSW_OUT_DB # !UUMS2_aSW_IN_DB)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1313",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_IN_DB,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	cascout => UUMS2_aSelect_1466_rtl_1_rtl_17_a151);

Cmd_Mid_In_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Cmd_Mid_In,
	combout => Cmd_Mid_In_acombout);

Cmd_Mid_Out_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Cmd_Mid_Out,
	combout => Cmd_Mid_Out_acombout);

UUMS2_ai_a5586_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5586 = !Cmd_Mid_In_acombout & !Cmd_Mid_Out_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Cmd_Mid_In_acombout,
	datad => Cmd_Mid_Out_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5586);

UUMS2_aHI_Cnt_En_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aHI_Cnt_En = DFFE(!CMD_OUT_acombout & !HC_MR_acombout & UUMS2_aHI_Cnt_En # !UUMS2_ai_a5573, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1F0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_OUT_acombout,
	datab => HC_MR_acombout,
	datac => UUMS2_ai_a5573,
	datad => UUMS2_aHI_Cnt_En,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aHI_Cnt_En);

UUMS2_ai_a5591_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5591 = CMD_IN_acombout & !UUMS2_aHI_Cnt_En & !UUMS2_aSW_IN_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0022",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => UUMS2_aHI_Cnt_En,
	datad => UUMS2_aSW_IN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5591);

UUMS2_aSelect_1468_rtl_2_rtl_18_a87_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a87 = UUMS2_aUMS_STATE_a15 & (Cmd_Mid_In_acombout # CMD_OUT_acombout # Cmd_Mid_Out_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AAA8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a15,
	datab => Cmd_Mid_In_acombout,
	datac => CMD_OUT_acombout,
	datad => Cmd_Mid_Out_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a87);

UUMS2_ai_a761_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a761 = CMD_IN_acombout # Cmd_Mid_Out_acombout # Cmd_Mid_In_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => Cmd_Mid_Out_acombout,
	datad => Cmd_Mid_In_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a761);

UUMS2_aSelect_1480_rtl_8_rtl_24_a252_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1480_rtl_8_rtl_24_a252 = !UUMS2_aUMS_STATE_a10 & !UUMS2_aUMS_STATE_a11

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a10,
	datac => UUMS2_aUMS_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1480_rtl_8_rtl_24_a252);

UUMS2_aSelect_1484_rtl_10_rtl_26_a33_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a33 = !UUMS2_aSW_OUT_DB & !UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a33);

UUMS2_aSelect_1482_rtl_9_rtl_25_a306_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a306 = !UUMS2_aSW_HS_DB & (UUMS2_aHI_Cnt_En # !Hi_Cnt_acombout & !Det_Sat_acombout)
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a309 = !UUMS2_aSW_HS_DB & (UUMS2_aHI_Cnt_En # !Hi_Cnt_acombout & !Det_Sat_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F01",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Cnt_acombout,
	datab => Det_Sat_acombout,
	datac => UUMS2_aSW_HS_DB,
	datad => UUMS2_aHI_Cnt_En,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a306,
	cascout => UUMS2_aSelect_1482_rtl_9_rtl_25_a309);

UUMS2_aSelect_1484_rtl_10_rtl_26_a374_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a374 = (!UUMS2_aSW_OUT_DB & !CMD_OUT_acombout & (UUMS2_aSW_IN_DB # !Cmd_Mid_In_acombout)) & CASCADE(UUMS2_aSelect_1482_rtl_9_rtl_25_a309)
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a375 = (!UUMS2_aSW_OUT_DB & !CMD_OUT_acombout & (UUMS2_aSW_IN_DB # !Cmd_Mid_In_acombout)) & CASCADE(UUMS2_aSelect_1482_rtl_9_rtl_25_a309)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0045",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aSW_IN_DB,
	datac => Cmd_Mid_In_acombout,
	datad => CMD_OUT_acombout,
	cascin => UUMS2_aSelect_1482_rtl_9_rtl_25_a309,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a374,
	cascout => UUMS2_aSelect_1484_rtl_10_rtl_26_a375);

UUMS2_aSelect_1484_rtl_10_rtl_26_a373_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a373 = (UUMS2_aUMS_STATE_a11 # UUMS2_aUMS_STATE_a13 & !UUMS2_ai_a5591) & CASCADE(UUMS2_aSelect_1484_rtl_10_rtl_26_a375)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a13,
	datac => UUMS2_aUMS_STATE_a11,
	datad => UUMS2_ai_a5591,
	cascin => UUMS2_aSelect_1484_rtl_10_rtl_26_a375,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a373);

UUMS2_ai_a3089_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a3089 = !UUMS2_aSW_IN_DB & (Cmd_Mid_In_acombout # !UUMS2_aHI_Cnt_En & CMD_IN_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00DC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aHI_Cnt_En,
	datab => Cmd_Mid_In_acombout,
	datac => CMD_IN_acombout,
	datad => UUMS2_aSW_IN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a3089);

UUMS2_aSelect_1472_rtl_4_rtl_20_a222_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a222 = UUMS2_aSW_OUT_DB & (UUMS2_aSW_IN_DB # !Cmd_Mid_In_acombout) # !UUMS2_aSW_OUT_DB & !Cmd_Mid_Out_acombout & (UUMS2_aSW_IN_DB # !Cmd_Mid_In_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "BB0B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => Cmd_Mid_Out_acombout,
	datac => Cmd_Mid_In_acombout,
	datad => UUMS2_aSW_IN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1472_rtl_4_rtl_20_a222);

UUMS2_aSelect_1472_rtl_4_rtl_20_a264_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a264 = UUMS2_aUMS_STATE_a12 & !UUMS2_ai_a5591 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aSelect_1472_rtl_4_rtl_20_a222

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a12,
	datab => UUMS2_ai_a5591,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datad => UUMS2_aSelect_1472_rtl_4_rtl_20_a222,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1472_rtl_4_rtl_20_a264);

UUMS2_ai_a5574_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5574 = !CMD_OUT_acombout & !HC_MR_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => CMD_OUT_acombout,
	datad => HC_MR_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5574);

UUMS2_aSelect_1488_rtl_12_rtl_28_a23_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1488_rtl_12_rtl_28_a23 = !UUMS2_ai_a5591 & (UUMS2_aUMS_STATE_a13 # UUMS2_aSelect_1472_rtl_4_rtl_20_a222 & UUMS2_aSelect_1478_rtl_7_rtl_23_a183)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F08",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1472_rtl_4_rtl_20_a222,
	datab => UUMS2_aSelect_1478_rtl_7_rtl_23_a183,
	datac => UUMS2_ai_a5591,
	datad => UUMS2_aUMS_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1488_rtl_12_rtl_28_a23);

UUMS2_aCMD_ERROR_a32_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_ERROR_a32 = !UUMS2_aHI_Cnt_En & (Det_Sat_acombout # Hi_Cnt_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Det_Sat_acombout,
	datac => Hi_Cnt_acombout,
	datad => UUMS2_aHI_Cnt_En,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aCMD_ERROR_a32);

UUMS2_aSelect_1488_rtl_12_rtl_28_a406_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1488_rtl_12_rtl_28_a406 = UUMS2_aUMS_STATE_a11 & (UUMS2_aSW_OUT_DB # !CMD_OUT_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F500",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_OUT_acombout,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aUMS_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1488_rtl_12_rtl_28_a406);

UUMS2_aSelect_1488_rtl_12_rtl_28_a16_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1488_rtl_12_rtl_28_a16 = UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aCMD_ERROR_a32 & (UUMS2_aSelect_1488_rtl_12_rtl_28_a23 # UUMS2_aSelect_1488_rtl_12_rtl_28_a406)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A080",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datab => UUMS2_aSelect_1488_rtl_12_rtl_28_a23,
	datac => UUMS2_aCMD_ERROR_a32,
	datad => UUMS2_aSelect_1488_rtl_12_rtl_28_a406,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1488_rtl_12_rtl_28_a16);

CNT_MAX_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(31),
	combout => CNT_MAX_a31_a_acombout);

CNT_MAX_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(30),
	combout => CNT_MAX_a30_a_acombout);

UUMS2_ai_a5575_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5575 = !UUMS2_aUMS_STATE_a17 & !UUMS2_aUMS_STATE_a15
-- UUMS2_ai_a5689 = !UUMS2_aUMS_STATE_a17 & !UUMS2_aUMS_STATE_a15

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a17,
	datad => UUMS2_aUMS_STATE_a15,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5575,
	cascout => UUMS2_ai_a5689);

UUMS2_areduce_nor_71_a25_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_71_a25 = !UUMS2_aUMS_STATE_a20 & !UUMS2_aUMS_STATE_a18 & !UUMS2_aUMS_STATE_a16

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a20,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_aUMS_STATE_a16,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_71_a25);

UUMS2_ai_a5682_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5682 = CMD_IN_acombout # CMD_OUT_acombout # !UUMS2_ai_a5573 & !UUMS2_aHI_Cnt_En

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => UUMS2_ai_a5573,
	datac => CMD_OUT_acombout,
	datad => UUMS2_aHI_Cnt_En,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5682);

UUMS2_ai_a5678_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5678 = UUMS2_ai_a5682 # UUMS2_ai_a5575 & !UUMS2_aUMS_STATE_a19 & UUMS2_areduce_nor_71_a25

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF20",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5575,
	datab => UUMS2_aUMS_STATE_a19,
	datac => UUMS2_areduce_nor_71_a25,
	datad => UUMS2_ai_a5682,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5678);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_ai_a5579 $ UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "5AF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C60C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a $ (UUMS2_ai_a5579 & UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5579,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a $ (UUMS2_ai_a5579 & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(clk20_acombout), , , !imr_acombout)
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A60A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	datab => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a_aCOUT);

CNT_MAX_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(29),
	combout => CNT_MAX_a29_a_acombout);

CNT_MAX_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(28),
	combout => CNT_MAX_a28_a_acombout);

CNT_MAX_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(27),
	combout => CNT_MAX_a27_a_acombout);

CNT_MAX_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(26),
	combout => CNT_MAX_a26_a_acombout);

CNT_MAX_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(25),
	combout => CNT_MAX_a25_a_acombout);

CNT_MAX_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(24),
	combout => CNT_MAX_a24_a_acombout);

CNT_MAX_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(23),
	combout => CNT_MAX_a23_a_acombout);

CNT_MAX_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(22),
	combout => CNT_MAX_a22_a_acombout);

CNT_MAX_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(21),
	combout => CNT_MAX_a21_a_acombout);

CNT_MAX_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(20),
	combout => CNT_MAX_a20_a_acombout);

CNT_MAX_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(19),
	combout => CNT_MAX_a19_a_acombout);

CNT_MAX_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(18),
	combout => CNT_MAX_a18_a_acombout);

CNT_MAX_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(17),
	combout => CNT_MAX_a17_a_acombout);

CNT_MAX_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(16),
	combout => CNT_MAX_a16_a_acombout);

CNT_MAX_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(15),
	combout => CNT_MAX_a15_a_acombout);

CNT_MAX_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(14),
	combout => CNT_MAX_a14_a_acombout);

CNT_MAX_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(13),
	combout => CNT_MAX_a13_a_acombout);

CNT_MAX_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(12),
	combout => CNT_MAX_a12_a_acombout);

CNT_MAX_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(11),
	combout => CNT_MAX_a11_a_acombout);

CNT_MAX_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(10),
	combout => CNT_MAX_a10_a_acombout);

CNT_MAX_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(9),
	combout => CNT_MAX_a9_a_acombout);

CNT_MAX_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(8),
	combout => CNT_MAX_a8_a_acombout);

CNT_MAX_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(7),
	combout => CNT_MAX_a7_a_acombout);

CNT_MAX_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(6),
	combout => CNT_MAX_a6_a_acombout);

CNT_MAX_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(5),
	combout => CNT_MAX_a5_a_acombout);

CNT_MAX_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(4),
	combout => CNT_MAX_a4_a_acombout);

CNT_MAX_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(3),
	combout => CNT_MAX_a3_a_acombout);

CNT_MAX_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(2),
	combout => CNT_MAX_a2_a_acombout);

CNT_MAX_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(1),
	combout => CNT_MAX_a1_a_acombout);

CNT_MAX_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_MAX(0),
	combout => CNT_MAX_a0_a_acombout);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_MAX_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & CNT_MAX_a1_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & (CNT_MAX_a1_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	datab => CNT_MAX_a1_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_MAX_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_MAX_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & CNT_MAX_a3_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & (CNT_MAX_a3_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	datab => CNT_MAX_a3_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_MAX_a4_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !CNT_MAX_a4_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	datab => CNT_MAX_a4_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & CNT_MAX_a5_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & (CNT_MAX_a5_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	datab => CNT_MAX_a5_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_MAX_a6_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !CNT_MAX_a6_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	datab => CNT_MAX_a6_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & CNT_MAX_a7_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & (CNT_MAX_a7_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	datab => CNT_MAX_a7_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_MAX_a8_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !CNT_MAX_a8_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	datab => CNT_MAX_a8_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & CNT_MAX_a9_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & (CNT_MAX_a9_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	datab => CNT_MAX_a9_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(CNT_MAX_a10_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_MAX_a10_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a10_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(CNT_MAX_a11_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a) # !CNT_MAX_a11_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a11_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_MAX_a12_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !CNT_MAX_a12_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	datab => CNT_MAX_a12_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(CNT_MAX_a13_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a) # !CNT_MAX_a13_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a13_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(CNT_MAX_a14_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_MAX_a14_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a14_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(CNT_MAX_a15_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a) # !CNT_MAX_a15_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a15_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(CNT_MAX_a16_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_MAX_a16_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a16_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(CNT_MAX_a17_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a) # !CNT_MAX_a17_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a17_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(CNT_MAX_a18_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_MAX_a18_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a18_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(CNT_MAX_a19_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a) # !CNT_MAX_a19_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a19_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(CNT_MAX_a20_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_MAX_a20_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a20_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & CNT_MAX_a21_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & (CNT_MAX_a21_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => CNT_MAX_a21_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_MAX_a22_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !CNT_MAX_a22_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	datab => CNT_MAX_a22_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(CNT_MAX_a23_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a) # !CNT_MAX_a23_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a23_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(CNT_MAX_a24_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_MAX_a24_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a24_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(CNT_MAX_a25_a_acombout & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a) # !CNT_MAX_a25_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a25_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_MAX_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_MAX_a26_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_MAX_a26_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & CNT_MAX_a27_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & (CNT_MAX_a27_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	datab => CNT_MAX_a27_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & (!UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_MAX_a28_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !CNT_MAX_a28_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	datab => CNT_MAX_a28_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_MAX_a29_a_acombout & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_MAX_a29_a_acombout # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_MAX_a29_a_acombout,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(CNT_MAX_a30_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_MAX_a30_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a # !UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_MAX_a30_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out = CNT_MAX_a31_a_acombout & UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a # !CNT_MAX_a31_a_acombout & (UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a # UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F330",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CNT_MAX_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out);

UUMS2_ai_a1083_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a1083 = UUMS2_aSW_OUT_DB # UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aPCTMAX_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a1083);

UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a = DFFE(!UUMS2_areduce_nor_10_a19 & !UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_areduce_nor_10_a19,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a,
	cout => UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT);

UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a = DFFE(!UUMS2_areduce_nor_10_a19 & UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a $ UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT # !UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a,
	cin => UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_areduce_nor_10_a19,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a,
	cout => UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a_aCOUT);

UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a = DFFE(!UUMS2_areduce_nor_10_a19 & UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a $ !UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a,
	cin => UUMS2_ans250_cnt_rtl_31_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_areduce_nor_10_a19,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a);

UUMS2_areduce_nor_10_a19_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_10_a19 = !UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a & UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a & !UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a1_a,
	datac => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a2_a,
	datad => UUMS2_ans250_cnt_rtl_31_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_10_a19);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a = DFFE(!UUMS2_areduce_nor_27_a124 & !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT);

Low_Speed_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(10),
	combout => Low_Speed_a10_a_acombout);

CNT_90_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(31),
	combout => CNT_90_a31_a_acombout);

CNT_90_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(30),
	combout => CNT_90_a30_a_acombout);

CNT_90_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(29),
	combout => CNT_90_a29_a_acombout);

CNT_90_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(28),
	combout => CNT_90_a28_a_acombout);

CNT_90_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(27),
	combout => CNT_90_a27_a_acombout);

CNT_90_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(26),
	combout => CNT_90_a26_a_acombout);

CNT_90_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(25),
	combout => CNT_90_a25_a_acombout);

CNT_90_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(24),
	combout => CNT_90_a24_a_acombout);

CNT_90_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(23),
	combout => CNT_90_a23_a_acombout);

CNT_90_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(22),
	combout => CNT_90_a22_a_acombout);

CNT_90_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(21),
	combout => CNT_90_a21_a_acombout);

CNT_90_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(20),
	combout => CNT_90_a20_a_acombout);

CNT_90_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(19),
	combout => CNT_90_a19_a_acombout);

CNT_90_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(18),
	combout => CNT_90_a18_a_acombout);

CNT_90_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(17),
	combout => CNT_90_a17_a_acombout);

CNT_90_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(16),
	combout => CNT_90_a16_a_acombout);

CNT_90_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(15),
	combout => CNT_90_a15_a_acombout);

CNT_90_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(14),
	combout => CNT_90_a14_a_acombout);

CNT_90_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(13),
	combout => CNT_90_a13_a_acombout);

CNT_90_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(12),
	combout => CNT_90_a12_a_acombout);

CNT_90_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(11),
	combout => CNT_90_a11_a_acombout);

CNT_90_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(10),
	combout => CNT_90_a10_a_acombout);

CNT_90_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(9),
	combout => CNT_90_a9_a_acombout);

CNT_90_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(8),
	combout => CNT_90_a8_a_acombout);

CNT_90_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(7),
	combout => CNT_90_a7_a_acombout);

CNT_90_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(6),
	combout => CNT_90_a6_a_acombout);

CNT_90_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(5),
	combout => CNT_90_a5_a_acombout);

CNT_90_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(4),
	combout => CNT_90_a4_a_acombout);

CNT_90_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(3),
	combout => CNT_90_a3_a_acombout);

CNT_90_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(2),
	combout => CNT_90_a2_a_acombout);

CNT_90_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(1),
	combout => CNT_90_a1_a_acombout);

CNT_90_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_90(0),
	combout => CNT_90_a0_a_acombout);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_90_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(CNT_90_a1_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a) # !CNT_90_a1_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a1_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_90_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_90_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(CNT_90_a3_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a) # !CNT_90_a3_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a3_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_90_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_90_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(CNT_90_a5_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a) # !CNT_90_a5_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a5_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_90_a6_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !CNT_90_a6_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	datab => CNT_90_a6_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & CNT_90_a7_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & (CNT_90_a7_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	datab => CNT_90_a7_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_90_a8_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !CNT_90_a8_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	datab => CNT_90_a8_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & CNT_90_a9_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & (CNT_90_a9_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	datab => CNT_90_a9_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(CNT_90_a10_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_90_a10_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a10_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(CNT_90_a11_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a) # !CNT_90_a11_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a11_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(CNT_90_a12_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_90_a12_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a12_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(CNT_90_a13_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a) # !CNT_90_a13_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a13_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(CNT_90_a14_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_90_a14_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a14_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(CNT_90_a15_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a) # !CNT_90_a15_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a15_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_90_a16_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !CNT_90_a16_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	datab => CNT_90_a16_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & CNT_90_a17_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & (CNT_90_a17_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	datab => CNT_90_a17_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_90_a18_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !CNT_90_a18_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	datab => CNT_90_a18_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & CNT_90_a19_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & (CNT_90_a19_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	datab => CNT_90_a19_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(CNT_90_a20_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_90_a20_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a20_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(CNT_90_a21_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a) # !CNT_90_a21_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a21_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(CNT_90_a22_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_90_a22_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a22_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(CNT_90_a23_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a) # !CNT_90_a23_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a23_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(CNT_90_a24_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_90_a24_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a24_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(CNT_90_a25_a_acombout & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a) # !CNT_90_a25_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a25_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_90_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_90_a26_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_90_a26_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & CNT_90_a27_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & (CNT_90_a27_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	datab => CNT_90_a27_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & (!UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_90_a28_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !CNT_90_a28_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	datab => CNT_90_a28_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_90_a29_a_acombout & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_90_a29_a_acombout # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_90_a29_a_acombout,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(CNT_90_a30_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_90_a30_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a # !UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a30_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out = CNT_90_a31_a_acombout & (UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a) # !CNT_90_a31_a_acombout & UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A0FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CNT_90_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT90_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out);

CNT_10_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(31),
	combout => CNT_10_a31_a_acombout);

CNT_10_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(30),
	combout => CNT_10_a30_a_acombout);

CNT_10_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(29),
	combout => CNT_10_a29_a_acombout);

CNT_10_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(28),
	combout => CNT_10_a28_a_acombout);

CNT_10_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(27),
	combout => CNT_10_a27_a_acombout);

CNT_10_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(26),
	combout => CNT_10_a26_a_acombout);

CNT_10_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(25),
	combout => CNT_10_a25_a_acombout);

CNT_10_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(24),
	combout => CNT_10_a24_a_acombout);

CNT_10_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(23),
	combout => CNT_10_a23_a_acombout);

CNT_10_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(22),
	combout => CNT_10_a22_a_acombout);

CNT_10_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(21),
	combout => CNT_10_a21_a_acombout);

CNT_10_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(20),
	combout => CNT_10_a20_a_acombout);

CNT_10_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(19),
	combout => CNT_10_a19_a_acombout);

CNT_10_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(18),
	combout => CNT_10_a18_a_acombout);

CNT_10_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(17),
	combout => CNT_10_a17_a_acombout);

CNT_10_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(16),
	combout => CNT_10_a16_a_acombout);

CNT_10_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(15),
	combout => CNT_10_a15_a_acombout);

CNT_10_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(14),
	combout => CNT_10_a14_a_acombout);

CNT_10_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(13),
	combout => CNT_10_a13_a_acombout);

CNT_10_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(12),
	combout => CNT_10_a12_a_acombout);

CNT_10_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(11),
	combout => CNT_10_a11_a_acombout);

CNT_10_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(10),
	combout => CNT_10_a10_a_acombout);

CNT_10_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(9),
	combout => CNT_10_a9_a_acombout);

CNT_10_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(8),
	combout => CNT_10_a8_a_acombout);

CNT_10_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(7),
	combout => CNT_10_a7_a_acombout);

CNT_10_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(6),
	combout => CNT_10_a6_a_acombout);

CNT_10_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(5),
	combout => CNT_10_a5_a_acombout);

CNT_10_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(4),
	combout => CNT_10_a4_a_acombout);

CNT_10_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(3),
	combout => CNT_10_a3_a_acombout);

CNT_10_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(2),
	combout => CNT_10_a2_a_acombout);

CNT_10_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(1),
	combout => CNT_10_a1_a_acombout);

CNT_10_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_10(0),
	combout => CNT_10_a0_a_acombout);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_10_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(CNT_10_a1_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a) # !CNT_10_a1_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a1_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_10_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_10_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(CNT_10_a3_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a) # !CNT_10_a3_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a3_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_10_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_10_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(CNT_10_a5_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a) # !CNT_10_a5_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a5_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(CNT_10_a6_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_10_a6_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a6_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(CNT_10_a7_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a) # !CNT_10_a7_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a7_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(CNT_10_a8_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_10_a8_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a8_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(CNT_10_a9_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a) # !CNT_10_a9_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a9_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(CNT_10_a10_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_10_a10_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a10_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(CNT_10_a11_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a) # !CNT_10_a11_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a11_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(CNT_10_a12_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_10_a12_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a12_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(CNT_10_a13_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a) # !CNT_10_a13_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a13_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(CNT_10_a14_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_10_a14_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a14_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(CNT_10_a15_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a) # !CNT_10_a15_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a15_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(CNT_10_a16_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_10_a16_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a16_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(CNT_10_a17_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a) # !CNT_10_a17_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a17_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(CNT_10_a18_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_10_a18_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a18_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(CNT_10_a19_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a) # !CNT_10_a19_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a19_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(CNT_10_a20_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_10_a20_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a20_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & CNT_10_a21_a_acombout & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & (CNT_10_a21_a_acombout # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => CNT_10_a21_a_acombout,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(CNT_10_a22_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_10_a22_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a22_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(CNT_10_a23_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a) # !CNT_10_a23_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a23_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(CNT_10_a24_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_10_a24_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a24_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(CNT_10_a25_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a) # !CNT_10_a25_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a25_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_10_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_10_a26_a_acombout & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_10_a26_a_acombout,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & CNT_10_a27_a_acombout & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & (CNT_10_a27_a_acombout # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	datab => CNT_10_a27_a_acombout,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(CNT_10_a28_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_10_a28_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a # !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a28_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(CNT_10_a29_a_acombout & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a) # !CNT_10_a29_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_10_a29_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & (!UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_10_a30_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !CNT_10_a30_a_acombout & !UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	datab => CNT_10_a30_a_acombout,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out = CNT_10_a31_a_acombout & (UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a) # !CNT_10_a31_a_acombout & UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C0FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CNT_10_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT10_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out);

rtl_a31_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a31 = !UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out & UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out
-- rtl_a4106 = !UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out & UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a31,
	cascout => rtl_a4106);

Med_Speed_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(10),
	combout => Med_Speed_a10_a_acombout);

CNT_25_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(31),
	combout => CNT_25_a31_a_acombout);

CNT_25_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(30),
	combout => CNT_25_a30_a_acombout);

CNT_25_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(29),
	combout => CNT_25_a29_a_acombout);

CNT_25_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(28),
	combout => CNT_25_a28_a_acombout);

CNT_25_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(27),
	combout => CNT_25_a27_a_acombout);

CNT_25_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(26),
	combout => CNT_25_a26_a_acombout);

CNT_25_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(25),
	combout => CNT_25_a25_a_acombout);

CNT_25_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(24),
	combout => CNT_25_a24_a_acombout);

CNT_25_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(23),
	combout => CNT_25_a23_a_acombout);

CNT_25_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(22),
	combout => CNT_25_a22_a_acombout);

CNT_25_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(21),
	combout => CNT_25_a21_a_acombout);

CNT_25_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(20),
	combout => CNT_25_a20_a_acombout);

CNT_25_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(19),
	combout => CNT_25_a19_a_acombout);

CNT_25_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(18),
	combout => CNT_25_a18_a_acombout);

CNT_25_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(17),
	combout => CNT_25_a17_a_acombout);

CNT_25_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(16),
	combout => CNT_25_a16_a_acombout);

CNT_25_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(15),
	combout => CNT_25_a15_a_acombout);

CNT_25_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(14),
	combout => CNT_25_a14_a_acombout);

CNT_25_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(13),
	combout => CNT_25_a13_a_acombout);

CNT_25_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(12),
	combout => CNT_25_a12_a_acombout);

CNT_25_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(11),
	combout => CNT_25_a11_a_acombout);

CNT_25_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(10),
	combout => CNT_25_a10_a_acombout);

CNT_25_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(9),
	combout => CNT_25_a9_a_acombout);

CNT_25_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(8),
	combout => CNT_25_a8_a_acombout);

CNT_25_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(7),
	combout => CNT_25_a7_a_acombout);

CNT_25_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(6),
	combout => CNT_25_a6_a_acombout);

CNT_25_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(5),
	combout => CNT_25_a5_a_acombout);

CNT_25_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(4),
	combout => CNT_25_a4_a_acombout);

CNT_25_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(3),
	combout => CNT_25_a3_a_acombout);

CNT_25_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(2),
	combout => CNT_25_a2_a_acombout);

CNT_25_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(1),
	combout => CNT_25_a1_a_acombout);

CNT_25_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_25(0),
	combout => CNT_25_a0_a_acombout);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_25_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(CNT_25_a1_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a) # !CNT_25_a1_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a1_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_25_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_25_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(CNT_25_a3_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a) # !CNT_25_a3_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a3_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_25_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_25_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(CNT_25_a5_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a) # !CNT_25_a5_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a5_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(CNT_25_a6_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_25_a6_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a6_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(CNT_25_a7_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a) # !CNT_25_a7_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a7_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(CNT_25_a8_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_25_a8_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a8_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(CNT_25_a9_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a) # !CNT_25_a9_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a9_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(CNT_25_a10_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_25_a10_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a10_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(CNT_25_a11_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a) # !CNT_25_a11_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a11_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_25_a12_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !CNT_25_a12_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	datab => CNT_25_a12_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & CNT_25_a13_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & (CNT_25_a13_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	datab => CNT_25_a13_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(CNT_25_a14_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_25_a14_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a14_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(CNT_25_a15_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a) # !CNT_25_a15_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a15_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_25_a16_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !CNT_25_a16_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	datab => CNT_25_a16_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(CNT_25_a17_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a) # !CNT_25_a17_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a17_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(CNT_25_a18_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_25_a18_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a18_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(CNT_25_a19_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a) # !CNT_25_a19_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a19_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(CNT_25_a20_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_25_a20_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a20_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & CNT_25_a21_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & (CNT_25_a21_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => CNT_25_a21_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(CNT_25_a22_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_25_a22_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a22_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(CNT_25_a23_a_acombout & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a) # !CNT_25_a23_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a23_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(CNT_25_a24_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_25_a24_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a24_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & CNT_25_a25_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & (CNT_25_a25_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	datab => CNT_25_a25_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_25_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_25_a26_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_25_a26_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & CNT_25_a27_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & (CNT_25_a27_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	datab => CNT_25_a27_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(CNT_25_a28_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_25_a28_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_25_a28_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_25_a29_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_25_a29_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_25_a29_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & (!UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_25_a30_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !CNT_25_a30_a_acombout & !UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	datab => CNT_25_a30_a_acombout,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out = CNT_25_a31_a_acombout & (UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a) # !CNT_25_a31_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C0FC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CNT_25_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT25_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out);

Hi_Speed_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(10),
	combout => Hi_Speed_a10_a_acombout);

CNT_75_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(31),
	combout => CNT_75_a31_a_acombout);

CNT_75_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(30),
	combout => CNT_75_a30_a_acombout);

CNT_75_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(29),
	combout => CNT_75_a29_a_acombout);

CNT_75_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(28),
	combout => CNT_75_a28_a_acombout);

CNT_75_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(27),
	combout => CNT_75_a27_a_acombout);

CNT_75_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(26),
	combout => CNT_75_a26_a_acombout);

CNT_75_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(25),
	combout => CNT_75_a25_a_acombout);

CNT_75_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(24),
	combout => CNT_75_a24_a_acombout);

CNT_75_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(23),
	combout => CNT_75_a23_a_acombout);

CNT_75_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(22),
	combout => CNT_75_a22_a_acombout);

CNT_75_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(21),
	combout => CNT_75_a21_a_acombout);

CNT_75_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(20),
	combout => CNT_75_a20_a_acombout);

CNT_75_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(19),
	combout => CNT_75_a19_a_acombout);

CNT_75_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(18),
	combout => CNT_75_a18_a_acombout);

CNT_75_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(17),
	combout => CNT_75_a17_a_acombout);

CNT_75_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(16),
	combout => CNT_75_a16_a_acombout);

CNT_75_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(15),
	combout => CNT_75_a15_a_acombout);

CNT_75_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(14),
	combout => CNT_75_a14_a_acombout);

CNT_75_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(13),
	combout => CNT_75_a13_a_acombout);

CNT_75_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(12),
	combout => CNT_75_a12_a_acombout);

CNT_75_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(11),
	combout => CNT_75_a11_a_acombout);

CNT_75_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(10),
	combout => CNT_75_a10_a_acombout);

CNT_75_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(9),
	combout => CNT_75_a9_a_acombout);

CNT_75_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(8),
	combout => CNT_75_a8_a_acombout);

CNT_75_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(7),
	combout => CNT_75_a7_a_acombout);

CNT_75_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(6),
	combout => CNT_75_a6_a_acombout);

CNT_75_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(5),
	combout => CNT_75_a5_a_acombout);

CNT_75_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(4),
	combout => CNT_75_a4_a_acombout);

CNT_75_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(3),
	combout => CNT_75_a3_a_acombout);

CNT_75_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(2),
	combout => CNT_75_a2_a_acombout);

CNT_75_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(1),
	combout => CNT_75_a1_a_acombout);

CNT_75_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_75(0),
	combout => CNT_75_a0_a_acombout);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_75_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(CNT_75_a1_a_acombout & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a) # !CNT_75_a1_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a1_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_75_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_75_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(CNT_75_a3_a_acombout & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a) # !CNT_75_a3_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a3_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_75_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_75_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & CNT_75_a5_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & (CNT_75_a5_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	datab => CNT_75_a5_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_75_a6_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !CNT_75_a6_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	datab => CNT_75_a6_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & CNT_75_a7_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & (CNT_75_a7_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	datab => CNT_75_a7_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_75_a8_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !CNT_75_a8_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	datab => CNT_75_a8_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(CNT_75_a9_a_acombout & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a) # !CNT_75_a9_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a9_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_75_a10_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !CNT_75_a10_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	datab => CNT_75_a10_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & CNT_75_a11_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & (CNT_75_a11_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	datab => CNT_75_a11_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_75_a12_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !CNT_75_a12_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	datab => CNT_75_a12_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & CNT_75_a13_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & (CNT_75_a13_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	datab => CNT_75_a13_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_75_a14_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !CNT_75_a14_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	datab => CNT_75_a14_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & CNT_75_a15_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & (CNT_75_a15_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	datab => CNT_75_a15_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_75_a16_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !CNT_75_a16_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	datab => CNT_75_a16_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & CNT_75_a17_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & (CNT_75_a17_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	datab => CNT_75_a17_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_75_a18_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !CNT_75_a18_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	datab => CNT_75_a18_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & CNT_75_a19_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & (CNT_75_a19_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	datab => CNT_75_a19_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_75_a20_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !CNT_75_a20_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	datab => CNT_75_a20_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & CNT_75_a21_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & (CNT_75_a21_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => CNT_75_a21_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_75_a22_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !CNT_75_a22_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	datab => CNT_75_a22_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & CNT_75_a23_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & (CNT_75_a23_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	datab => CNT_75_a23_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_75_a24_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !CNT_75_a24_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	datab => CNT_75_a24_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & CNT_75_a25_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & (CNT_75_a25_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	datab => CNT_75_a25_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(CNT_75_a26_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_75_a26_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a26_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(CNT_75_a27_a_acombout & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a) # !CNT_75_a27_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a27_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & (!UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_75_a28_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !CNT_75_a28_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	datab => CNT_75_a28_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_75_a29_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_75_a29_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_75_a29_a_acombout,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(CNT_75_a30_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_75_a30_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a # !UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a30_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out = CNT_75_a31_a_acombout & (UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a) # !CNT_75_a31_a_acombout & UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A0FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CNT_75_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT75_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out);

rtl_a744_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a744 = UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a10_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a10_a_acombout) # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a10_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88E2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a10_a_acombout,
	datab => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datac => Hi_Speed_a10_a_acombout,
	datad => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a744);

rtl_a484_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a484 = UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out # UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CEFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datab => UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out,
	datac => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a484);

rtl_a749_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a749 = Low_Speed_a10_a_acombout & (rtl_a484 # rtl_a31 & rtl_a744) # !Low_Speed_a10_a_acombout & rtl_a31 & rtl_a744

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a10_a_acombout,
	datab => rtl_a31,
	datac => rtl_a744,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a749);

Low_Speed_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(11),
	combout => Low_Speed_a11_a_acombout);

Hi_Speed_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(11),
	combout => Hi_Speed_a11_a_acombout);

Med_Speed_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(11),
	combout => Med_Speed_a11_a_acombout);

rtl_a755_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a755 = UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a11_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a11_a_acombout) # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a11_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0B8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a11_a_acombout,
	datab => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datac => Med_Speed_a11_a_acombout,
	datad => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a755);

rtl_a760_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a760 = Low_Speed_a11_a_acombout & (rtl_a484 # rtl_a31 & rtl_a755) # !Low_Speed_a11_a_acombout & rtl_a31 & rtl_a755

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a11_a_acombout,
	datab => rtl_a31,
	datac => rtl_a755,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a760);

UUMS2_areduce_nor_27_a41_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a41 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a $ rtl_a749 # !rtl_a760) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a & (rtl_a760 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a $ rtl_a749)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a11_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a10_a,
	datac => rtl_a749,
	datad => rtl_a760,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a41);

Low_Speed_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(0),
	combout => Low_Speed_a0_a_acombout);

Med_Speed_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(0),
	combout => Med_Speed_a0_a_acombout);

Hi_Speed_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(0),
	combout => Hi_Speed_a0_a_acombout);

rtl_a634_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a634 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a0_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a0_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a0_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AC0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a0_a_acombout,
	datab => Hi_Speed_a0_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a634);

rtl_a639_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a639 = Low_Speed_a0_a_acombout & (rtl_a484 # rtl_a634 & rtl_a31) # !Low_Speed_a0_a_acombout & rtl_a634 & rtl_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a0_a_acombout,
	datab => rtl_a634,
	datac => rtl_a31,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a639);

Low_Speed_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(2),
	combout => Low_Speed_a2_a_acombout);

Med_Speed_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(2),
	combout => Med_Speed_a2_a_acombout);

Hi_Speed_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(2),
	combout => Hi_Speed_a2_a_acombout);

rtl_a656_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a656 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a2_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a2_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a2_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AC0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a2_a_acombout,
	datab => Hi_Speed_a2_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a656);

rtl_a661_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a661 = rtl_a31 & (rtl_a656 # Low_Speed_a2_a_acombout & rtl_a484) # !rtl_a31 & Low_Speed_a2_a_acombout & rtl_a484

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a31,
	datab => Low_Speed_a2_a_acombout,
	datac => rtl_a656,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a661);

UUMS2_areduce_nor_27_a34_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a34 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a $ rtl_a639 # !rtl_a661) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a & (rtl_a661 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a $ rtl_a639)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a2_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a0_a,
	datac => rtl_a639,
	datad => rtl_a661,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a34);

Low_Speed_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(1),
	combout => Low_Speed_a1_a_acombout);

Med_Speed_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(1),
	combout => Med_Speed_a1_a_acombout);

Hi_Speed_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(1),
	combout => Hi_Speed_a1_a_acombout);

rtl_a645_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a645 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a1_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a1_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a1_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AC0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a1_a_acombout,
	datab => Hi_Speed_a1_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a645);

rtl_a4104_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a4104 = UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out & !UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out & rtl_a645

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aPCT10_CMP_acomparator_acmp_end_aagb_out,
	datac => UUMS2_aPCT90_CMP_acomparator_acmp_end_aagb_out,
	datad => rtl_a645,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a4104);

UUMS2_ai_a3_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a3 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a $ (rtl_a4104 # Low_Speed_a1_a_acombout & rtl_a484)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "336C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a1_a_acombout,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a,
	datac => rtl_a484,
	datad => rtl_a4104,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a3);

Low_Speed_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(4),
	combout => Low_Speed_a4_a_acombout);

Med_Speed_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(4),
	combout => Med_Speed_a4_a_acombout);

Hi_Speed_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(4),
	combout => Hi_Speed_a4_a_acombout);

rtl_a4105_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a4105 = (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a4_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a4_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a4_a_acombout)) & CASCADE(rtl_a4106)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B822",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a4_a_acombout,
	datab => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datac => Hi_Speed_a4_a_acombout,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	cascin => rtl_a4106,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a4105);

rtl_a683_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a683 = rtl_a4105 # Low_Speed_a4_a_acombout & rtl_a484

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Low_Speed_a4_a_acombout,
	datac => rtl_a4105,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a683);

Low_Speed_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(8),
	combout => Low_Speed_a8_a_acombout);

Hi_Speed_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(8),
	combout => Hi_Speed_a8_a_acombout);

Med_Speed_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(8),
	combout => Med_Speed_a8_a_acombout);

rtl_a722_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a722 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a8_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a8_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a8_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CA0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a8_a_acombout,
	datab => Med_Speed_a8_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a722);

rtl_a727_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a727 = rtl_a31 & (rtl_a722 # Low_Speed_a8_a_acombout & rtl_a484) # !rtl_a31 & Low_Speed_a8_a_acombout & rtl_a484

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a31,
	datab => Low_Speed_a8_a_acombout,
	datac => rtl_a722,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a727);

Low_Speed_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(9),
	combout => Low_Speed_a9_a_acombout);

Hi_Speed_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(9),
	combout => Hi_Speed_a9_a_acombout);

Med_Speed_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(9),
	combout => Med_Speed_a9_a_acombout);

rtl_a733_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a733 = UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a9_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a9_a_acombout) # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a9_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0AC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a9_a_acombout,
	datab => Med_Speed_a9_a_acombout,
	datac => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a733);

rtl_a738_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a738 = Low_Speed_a9_a_acombout & (rtl_a484 # rtl_a31 & rtl_a733) # !Low_Speed_a9_a_acombout & rtl_a31 & rtl_a733

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a9_a_acombout,
	datab => rtl_a31,
	datac => rtl_a484,
	datad => rtl_a733,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a738);

UUMS2_areduce_nor_27_a29_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a29 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a $ rtl_a738 # !rtl_a727) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a & (rtl_a727 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a $ rtl_a738)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a8_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a9_a,
	datac => rtl_a727,
	datad => rtl_a738,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a29);

UUMS2_areduce_nor_27_a118_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a118 = UUMS2_ai_a3 # UUMS2_areduce_nor_27_a29 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a $ rtl_a683

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a3,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a,
	datac => rtl_a683,
	datad => UUMS2_areduce_nor_27_a29,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a118);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a $ UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a $ !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)
-- UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a & !UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a,
	cout => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a_aCOUT);

UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a = DFFE(!UUMS2_areduce_nor_27_a124 & UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a_aCOUT $ UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , UUMS2_areduce_nor_10_a19)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a,
	cin => UUMS2_astep_cnt_rtl_30_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	ena => UUMS2_areduce_nor_10_a19,
	sclr => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a);

Low_Speed_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(3),
	combout => Low_Speed_a3_a_acombout);

Med_Speed_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(3),
	combout => Med_Speed_a3_a_acombout);

Hi_Speed_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(3),
	combout => Hi_Speed_a3_a_acombout);

rtl_a667_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a667 = UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a3_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a3_a_acombout) # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a3_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88E2",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a3_a_acombout,
	datab => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datac => Hi_Speed_a3_a_acombout,
	datad => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a667);

rtl_a672_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a672 = rtl_a31 & (rtl_a667 # Low_Speed_a3_a_acombout & rtl_a484) # !rtl_a31 & Low_Speed_a3_a_acombout & rtl_a484

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a31,
	datab => Low_Speed_a3_a_acombout,
	datac => rtl_a667,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a672);

Low_Speed_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(15),
	combout => Low_Speed_a15_a_acombout);

Hi_Speed_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(15),
	combout => Hi_Speed_a15_a_acombout);

Med_Speed_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(15),
	combout => Med_Speed_a15_a_acombout);

rtl_a799_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a799 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a15_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a15_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a15_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CA0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a15_a_acombout,
	datab => Med_Speed_a15_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a799);

rtl_a804_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a804 = Low_Speed_a15_a_acombout & (rtl_a484 # rtl_a799 & rtl_a31) # !Low_Speed_a15_a_acombout & rtl_a799 & rtl_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a15_a_acombout,
	datab => rtl_a799,
	datac => rtl_a484,
	datad => rtl_a31,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a804);

UUMS2_areduce_nor_27_a61_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a61 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a $ rtl_a672 # !rtl_a804) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a & (rtl_a804 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a $ rtl_a672)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a15_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a3_a,
	datac => rtl_a672,
	datad => rtl_a804,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a61);

Low_Speed_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(6),
	combout => Low_Speed_a6_a_acombout);

Hi_Speed_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(6),
	combout => Hi_Speed_a6_a_acombout);

Med_Speed_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(6),
	combout => Med_Speed_a6_a_acombout);

rtl_a700_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a700 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a6_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a6_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a6_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CA0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a6_a_acombout,
	datab => Med_Speed_a6_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a700);

rtl_a705_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a705 = Low_Speed_a6_a_acombout & (rtl_a484 # rtl_a31 & rtl_a700) # !Low_Speed_a6_a_acombout & rtl_a31 & rtl_a700

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a6_a_acombout,
	datab => rtl_a484,
	datac => rtl_a31,
	datad => rtl_a700,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a705);

Low_Speed_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(14),
	combout => Low_Speed_a14_a_acombout);

Med_Speed_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(14),
	combout => Med_Speed_a14_a_acombout);

Hi_Speed_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(14),
	combout => Hi_Speed_a14_a_acombout);

rtl_a788_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a788 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a14_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a14_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a14_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B822",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a14_a_acombout,
	datab => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datac => Hi_Speed_a14_a_acombout,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a788);

rtl_a793_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a793 = Low_Speed_a14_a_acombout & (rtl_a484 # rtl_a788 & rtl_a31) # !Low_Speed_a14_a_acombout & rtl_a788 & rtl_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a14_a_acombout,
	datab => rtl_a788,
	datac => rtl_a31,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a793);

UUMS2_areduce_nor_27_a89_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a89 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a $ rtl_a705 # !rtl_a793) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a & (rtl_a793 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a $ rtl_a705)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a14_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a6_a,
	datac => rtl_a705,
	datad => rtl_a793,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a89);

Low_Speed_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(7),
	combout => Low_Speed_a7_a_acombout);

Med_Speed_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(7),
	combout => Med_Speed_a7_a_acombout);

Hi_Speed_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(7),
	combout => Hi_Speed_a7_a_acombout);

rtl_a711_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a711 = UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a7_a_acombout # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a7_a_acombout) # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a7_a_acombout & !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "A0CA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a7_a_acombout,
	datab => Hi_Speed_a7_a_acombout,
	datac => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a711);

rtl_a716_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a716 = rtl_a31 & (rtl_a711 # Low_Speed_a7_a_acombout & rtl_a484) # !rtl_a31 & Low_Speed_a7_a_acombout & rtl_a484

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => rtl_a31,
	datab => Low_Speed_a7_a_acombout,
	datac => rtl_a711,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a716);

Low_Speed_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(13),
	combout => Low_Speed_a13_a_acombout);

Hi_Speed_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(13),
	combout => Hi_Speed_a13_a_acombout);

Med_Speed_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(13),
	combout => Med_Speed_a13_a_acombout);

rtl_a777_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a777 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a13_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a13_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a13_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CA0C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Hi_Speed_a13_a_acombout,
	datab => Med_Speed_a13_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a777);

rtl_a782_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a782 = Low_Speed_a13_a_acombout & (rtl_a484 # rtl_a31 & rtl_a777) # !Low_Speed_a13_a_acombout & rtl_a31 & rtl_a777

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a13_a_acombout,
	datab => rtl_a31,
	datac => rtl_a484,
	datad => rtl_a777,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a782);

UUMS2_areduce_nor_27_a74_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a74 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a $ rtl_a782 # !rtl_a716) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a & (rtl_a716 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a $ rtl_a782)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a7_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a13_a,
	datac => rtl_a716,
	datad => rtl_a782,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a74);

Low_Speed_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(12),
	combout => Low_Speed_a12_a_acombout);

Med_Speed_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(12),
	combout => Med_Speed_a12_a_acombout);

Hi_Speed_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(12),
	combout => Hi_Speed_a12_a_acombout);

rtl_a766_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a766 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a12_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a12_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a12_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B822",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a12_a_acombout,
	datab => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datac => Hi_Speed_a12_a_acombout,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a766);

rtl_a771_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a771 = Low_Speed_a12_a_acombout & (rtl_a484 # rtl_a31 & rtl_a766) # !Low_Speed_a12_a_acombout & rtl_a31 & rtl_a766

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F888",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a12_a_acombout,
	datab => rtl_a484,
	datac => rtl_a31,
	datad => rtl_a766,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a771);

Low_Speed_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Low_Speed(5),
	combout => Low_Speed_a5_a_acombout);

Med_Speed_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Med_Speed(5),
	combout => Med_Speed_a5_a_acombout);

Hi_Speed_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Hi_Speed(5),
	combout => Hi_Speed_a5_a_acombout);

rtl_a689_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a689 = UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a5_a_acombout & UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out # !UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out & (UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Hi_Speed_a5_a_acombout # !UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out & Med_Speed_a5_a_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AC0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Med_Speed_a5_a_acombout,
	datab => Hi_Speed_a5_a_acombout,
	datac => UUMS2_aPCT75_CMP_acomparator_acmp_end_aagb_out,
	datad => UUMS2_aPCT25_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a689);

rtl_a694_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a694 = Low_Speed_a5_a_acombout & (rtl_a484 # rtl_a689 & rtl_a31) # !Low_Speed_a5_a_acombout & rtl_a689 & rtl_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a5_a_acombout,
	datab => rtl_a689,
	datac => rtl_a31,
	datad => rtl_a484,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a694);

UUMS2_areduce_nor_27_a50_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a50 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a $ rtl_a694 # !rtl_a771) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a & (rtl_a771 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a $ rtl_a694)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7BDE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a12_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a5_a,
	datac => rtl_a771,
	datad => rtl_a694,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a50);

UUMS2_areduce_nor_27_a111_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a111 = UUMS2_areduce_nor_27_a61 # UUMS2_areduce_nor_27_a89 # UUMS2_areduce_nor_27_a74 # UUMS2_areduce_nor_27_a50

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_27_a61,
	datab => UUMS2_areduce_nor_27_a89,
	datac => UUMS2_areduce_nor_27_a74,
	datad => UUMS2_areduce_nor_27_a50,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a111);

UUMS2_areduce_nor_27_a124_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a124 = !UUMS2_areduce_nor_27_a41 & !UUMS2_areduce_nor_27_a34 & !UUMS2_areduce_nor_27_a118 & !UUMS2_areduce_nor_27_a111

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_27_a41,
	datab => UUMS2_areduce_nor_27_a34,
	datac => UUMS2_areduce_nor_27_a118,
	datad => UUMS2_areduce_nor_27_a111,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a124);

UUMS2_astep_clk_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_astep_clk = DFFE(UUMS2_areduce_nor_71 & (UUMS2_astep_clk $ (UUMS2_areduce_nor_10_a19 & UUMS2_areduce_nor_27_a124)), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "28A0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_71,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_astep_clk,
	datad => UUMS2_areduce_nor_27_a124,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_astep_clk);

UUMS2_ai_a5589_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5589 = UUMS2_ai_a1083 & UUMS2_areduce_nor_10_a19 & !UUMS2_astep_clk & UUMS2_areduce_nor_27_a124

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a1083,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_astep_clk,
	datad => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5589);

UUMS2_aUMS_STATE_a20_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a20 = DFFE(UUMS2_aSelect_1488_rtl_12_rtl_28_a16 # UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aUMS_STATE_a20 & !UUMS2_ai_a5589, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F8",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datab => UUMS2_aUMS_STATE_a20,
	datac => UUMS2_aSelect_1488_rtl_12_rtl_28_a16,
	datad => UUMS2_ai_a5589,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a20);

UUMS2_aSelect_1466_rtl_1_rtl_17_a85_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a85 = !UUMS2_aUMS_STATE_a20 & !UUMS2_aUMS_STATE_a19

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a20,
	datad => UUMS2_aUMS_STATE_a19,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a85);

UUMS2_ai_a5585_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5585 = UUMS2_aSW_IN_DB & UUMS2_aSW_OUT_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0C0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aSW_IN_DB,
	datac => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5585);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a = DFFE(!UUMS2_aUMS_STATE_a8 & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT);

UUMS2_areduce_nor_37_a64_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_37_a64 = UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a # UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a5_a,
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a3_a,
	datac => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a4_a,
	datad => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_37_a64);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT);

UUMS2_areduce_nor_37_a55_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_37_a55 = UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a # UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a9_a,
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a7_a,
	datac => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a8_a,
	datad => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_37_a55);

UUMS2_areduce_nor_37_a77_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_37_a77 = !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a2_a,
	datac => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a1_a,
	datad => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_37_a77);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a & !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a $ UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a,
	cout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a_aCOUT);

UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a = DFFE(!UUMS2_aUMS_STATE_a8 & UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a $ !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A5A5",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a,
	cin => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => UUMS2_aUMS_STATE_a8,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a);

UUMS2_areduce_nor_37_a50_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_37_a50 = UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a # UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a # !UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a12_a,
	datab => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a13_a,
	datac => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a14_a,
	datad => UUMS2_ainit_delay_cnt_rtl_33_awysi_counter_asload_path_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_37_a50);

UUMS2_areduce_nor_37_a98_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_37_a98 = UUMS2_areduce_nor_37_a64 # UUMS2_areduce_nor_37_a55 # UUMS2_areduce_nor_37_a77 # UUMS2_areduce_nor_37_a50

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_37_a64,
	datab => UUMS2_areduce_nor_37_a55,
	datac => UUMS2_areduce_nor_37_a77,
	datad => UUMS2_areduce_nor_37_a50,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_37_a98);

UUMS2_aUMS_STATE_a8_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a8 = DFFE(UUMS2_aUMS_STATE_a8 # !UUMS2_areduce_nor_37_a98, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CFCF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a8,
	datac => UUMS2_areduce_nor_37_a98,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a8);

UUMS2_aSelect_1490_rtl_13_a196_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1490_rtl_13_a196 = UUMS2_aUMS_STATE_a549 & (UUMS2_aUMS_STATE_a8 # UUMS2_areduce_nor_37_a98)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCC0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a549,
	datac => UUMS2_aUMS_STATE_a8,
	datad => UUMS2_areduce_nor_37_a98,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1490_rtl_13_a196);

UUMS2_aUMS_STATE_a21_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a21 = DFFE(!UUMS2_aSW_HS_DB & UUMS2_ai_a5585 & (!UUMS2_aSelect_1490_rtl_13_a196 # !UUMS2_aSelect_1466_rtl_1_rtl_17_a85), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1050",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_HS_DB,
	datab => UUMS2_aSelect_1466_rtl_1_rtl_17_a85,
	datac => UUMS2_ai_a5585,
	datad => UUMS2_aSelect_1490_rtl_13_a196,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a21);

UUMS2_aUMS_STATE_a550_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a550 = (!UUMS2_aUMS_STATE_a9 & !UUMS2_aUMS_STATE_a11 & !UUMS2_aUMS_STATE_a10 & !UUMS2_aUMS_STATE_a18) & CASCADE(UUMS2_ai_a5689)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a9,
	datab => UUMS2_aUMS_STATE_a11,
	datac => UUMS2_aUMS_STATE_a10,
	datad => UUMS2_aUMS_STATE_a18,
	cascin => UUMS2_ai_a5689,
	devclrn => devclrn,
	devpor => devpor,
	cascout => UUMS2_aUMS_STATE_a550);

UUMS2_aUMS_STATE_a549_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a549 = (!UUMS2_aUMS_STATE_a21 & !UUMS2_aUMS_STATE_a13 & !UUMS2_aUMS_STATE_a16 & !UUMS2_aUMS_STATE_a12) & CASCADE(UUMS2_aUMS_STATE_a550)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a21,
	datab => UUMS2_aUMS_STATE_a13,
	datac => UUMS2_aUMS_STATE_a16,
	datad => UUMS2_aUMS_STATE_a12,
	cascin => UUMS2_aUMS_STATE_a550,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aUMS_STATE_a549);

UUMS2_aSelect_1470_rtl_3_rtl_19_a12_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a12 = !UUMS2_aUMS_STATE_a8 & !UUMS2_areduce_nor_37_a98

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a8,
	datac => UUMS2_areduce_nor_37_a98,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a12);

UUMS2_aUMS_STATE_a22_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a22 = DFFE(UUMS2_aSW_HS_DB & (UUMS2_aSelect_1470_rtl_3_rtl_19_a12 # !UUMS2_aUMS_STATE_a542 # !UUMS2_aUMS_STATE_a549), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC4C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a549,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aUMS_STATE_a542,
	datad => UUMS2_aSelect_1470_rtl_3_rtl_19_a12,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a22);

UUMS2_aUMS_STATE_a542_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a542 = !UUMS2_aUMS_STATE_a19 & !UUMS2_aUMS_STATE_a22 & !UUMS2_aUMS_STATE_a20

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0101",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a19,
	datab => UUMS2_aUMS_STATE_a22,
	datac => UUMS2_aUMS_STATE_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aUMS_STATE_a542);

UUMS2_aSelect_1466_rtl_1_rtl_17_a132_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a132 = UUMS2_aSelect_1466_rtl_1_rtl_17_a86 & (UUMS2_ai_a5574 # UUMS2_aUMS_STATE_a22 & !UUMS2_aSW_HS_DB) # !UUMS2_aSelect_1466_rtl_1_rtl_17_a86 & UUMS2_aUMS_STATE_a22 & !UUMS2_aSW_HS_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "88F8",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a86,
	datab => UUMS2_ai_a5574,
	datac => UUMS2_aUMS_STATE_a22,
	datad => UUMS2_aSW_HS_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a132);

UUMS2_aSelect_1466_rtl_1_rtl_17_a135_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a135 = UUMS2_aSelect_1466_rtl_1_rtl_17_a132 # UUMS2_aUMS_STATE_a542 & UUMS2_aUMS_STATE_a8 & UUMS2_aUMS_STATE_a549

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF80",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a542,
	datab => UUMS2_aUMS_STATE_a8,
	datac => UUMS2_aUMS_STATE_a549,
	datad => UUMS2_aSelect_1466_rtl_1_rtl_17_a132,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a135);

UUMS2_ai_a764_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a764 = !UUMS2_astep_clk & UUMS2_areduce_nor_10_a19 & UUMS2_aSW_OUT_DB & UUMS2_areduce_nor_27_a124

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_clk,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a764);

CNT_110_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(31),
	combout => CNT_110_a31_a_acombout);

CNT_110_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(30),
	combout => CNT_110_a30_a_acombout);

CNT_110_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(29),
	combout => CNT_110_a29_a_acombout);

CNT_110_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(28),
	combout => CNT_110_a28_a_acombout);

CNT_110_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(27),
	combout => CNT_110_a27_a_acombout);

CNT_110_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(26),
	combout => CNT_110_a26_a_acombout);

CNT_110_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(25),
	combout => CNT_110_a25_a_acombout);

CNT_110_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(24),
	combout => CNT_110_a24_a_acombout);

CNT_110_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(23),
	combout => CNT_110_a23_a_acombout);

CNT_110_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(22),
	combout => CNT_110_a22_a_acombout);

CNT_110_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(21),
	combout => CNT_110_a21_a_acombout);

CNT_110_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(20),
	combout => CNT_110_a20_a_acombout);

CNT_110_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(19),
	combout => CNT_110_a19_a_acombout);

CNT_110_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(18),
	combout => CNT_110_a18_a_acombout);

CNT_110_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(17),
	combout => CNT_110_a17_a_acombout);

CNT_110_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(16),
	combout => CNT_110_a16_a_acombout);

CNT_110_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(15),
	combout => CNT_110_a15_a_acombout);

CNT_110_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(14),
	combout => CNT_110_a14_a_acombout);

CNT_110_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(13),
	combout => CNT_110_a13_a_acombout);

CNT_110_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(12),
	combout => CNT_110_a12_a_acombout);

CNT_110_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(11),
	combout => CNT_110_a11_a_acombout);

CNT_110_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(10),
	combout => CNT_110_a10_a_acombout);

CNT_110_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(9),
	combout => CNT_110_a9_a_acombout);

CNT_110_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(8),
	combout => CNT_110_a8_a_acombout);

CNT_110_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(7),
	combout => CNT_110_a7_a_acombout);

CNT_110_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(6),
	combout => CNT_110_a6_a_acombout);

CNT_110_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(5),
	combout => CNT_110_a5_a_acombout);

CNT_110_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(4),
	combout => CNT_110_a4_a_acombout);

CNT_110_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(3),
	combout => CNT_110_a3_a_acombout);

CNT_110_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(2),
	combout => CNT_110_a2_a_acombout);

CNT_110_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(1),
	combout => CNT_110_a1_a_acombout);

CNT_110_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_110(0),
	combout => CNT_110_a0_a_acombout);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_110_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(CNT_110_a1_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a) # !CNT_110_a1_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a1_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(CNT_110_a2_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_110_a2_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a2_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & CNT_110_a3_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & (CNT_110_a3_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	datab => CNT_110_a3_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_110_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_110_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & CNT_110_a5_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & (CNT_110_a5_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	datab => CNT_110_a5_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(CNT_110_a6_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_110_a6_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a6_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(CNT_110_a7_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a) # !CNT_110_a7_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a7_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_110_a8_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !CNT_110_a8_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	datab => CNT_110_a8_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(CNT_110_a9_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a) # !CNT_110_a9_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a9_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_110_a10_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !CNT_110_a10_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	datab => CNT_110_a10_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(CNT_110_a11_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a) # !CNT_110_a11_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a11_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(CNT_110_a12_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_110_a12_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a12_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & CNT_110_a13_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & (CNT_110_a13_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	datab => CNT_110_a13_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_110_a14_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !CNT_110_a14_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	datab => CNT_110_a14_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & CNT_110_a15_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & (CNT_110_a15_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	datab => CNT_110_a15_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(CNT_110_a16_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_110_a16_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a16_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(CNT_110_a17_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a) # !CNT_110_a17_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a17_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(CNT_110_a18_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_110_a18_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a18_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(CNT_110_a19_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a) # !CNT_110_a19_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a19_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(CNT_110_a20_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_110_a20_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a20_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(CNT_110_a21_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a) # !CNT_110_a21_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a21_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_110_a22_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !CNT_110_a22_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	datab => CNT_110_a22_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(CNT_110_a23_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a) # !CNT_110_a23_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a23_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_110_a24_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !CNT_110_a24_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	datab => CNT_110_a24_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & CNT_110_a25_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & (CNT_110_a25_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	datab => CNT_110_a25_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_110_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_110_a26_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_110_a26_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(CNT_110_a27_a_acombout & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a) # !CNT_110_a27_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a27_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & (!UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_110_a28_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !CNT_110_a28_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	datab => CNT_110_a28_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_110_a29_a_acombout & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_110_a29_a_acombout # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_110_a29_a_acombout,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(CNT_110_a30_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_110_a30_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a # !UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_110_a30_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out = CNT_110_a31_a_acombout & UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a # !CNT_110_a31_a_acombout & (UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a # UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F330",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CNT_110_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT110_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out);

rtl_a650_I : apex20ke_lcell 
-- Equation(s):
-- rtl_a650 = Low_Speed_a1_a_acombout & (rtl_a484 # rtl_a645 & rtl_a31) # !Low_Speed_a1_a_acombout & rtl_a645 & rtl_a31

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECA0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Low_Speed_a1_a_acombout,
	datab => rtl_a645,
	datac => rtl_a484,
	datad => rtl_a31,
	devclrn => devclrn,
	devpor => devpor,
	combout => rtl_a650);

UUMS2_areduce_nor_27_a26_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a26 = UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a & (UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a $ rtl_a650 # !rtl_a683) # !UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a & (rtl_a683 # UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a $ rtl_a650)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7DBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a4_a,
	datab => UUMS2_astep_cnt_rtl_30_awysi_counter_asload_path_a1_a,
	datac => rtl_a650,
	datad => rtl_a683,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a26);

UUMS2_areduce_nor_27_a106_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a106 = UUMS2_areduce_nor_27_a41 # UUMS2_areduce_nor_27_a29 # UUMS2_areduce_nor_27_a34 # UUMS2_areduce_nor_27_a26

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_27_a41,
	datab => UUMS2_areduce_nor_27_a29,
	datac => UUMS2_areduce_nor_27_a34,
	datad => UUMS2_areduce_nor_27_a26,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a106);

UUMS2_ai_a5571_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5571 = !UUMS2_astep_clk & UUMS2_areduce_nor_10_a19 & !UUMS2_areduce_nor_27_a111 & !UUMS2_areduce_nor_27_a106
-- UUMS2_ai_a5690 = !UUMS2_astep_clk & UUMS2_areduce_nor_10_a19 & !UUMS2_areduce_nor_27_a111 & !UUMS2_areduce_nor_27_a106

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_clk,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_areduce_nor_27_a111,
	datad => UUMS2_areduce_nor_27_a106,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5571,
	cascout => UUMS2_ai_a5690);

UUMS2_aSelect_1466_rtl_1_rtl_17_a150_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a150 = (UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aUMS_STATE_a16 & !UUMS2_ai_a761 & UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out) & CASCADE(UUMS2_ai_a5690)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datab => UUMS2_aUMS_STATE_a16,
	datac => UUMS2_ai_a761,
	datad => UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out,
	cascin => UUMS2_ai_a5690,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a150);

UUMS2_aSelect_1466_rtl_1_rtl_17_a125_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a125 = !UUMS2_ai_a5585 & !UUMS2_aSW_HS_DB & (UUMS2_aUMS_STATE_a19 # UUMS2_aUMS_STATE_a20)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0504",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5585,
	datab => UUMS2_aUMS_STATE_a19,
	datac => UUMS2_aSW_HS_DB,
	datad => UUMS2_aUMS_STATE_a20,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a125);

UUMS2_areduce_nor_27_a119_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_27_a119 = UUMS2_areduce_nor_27_a41 # UUMS2_areduce_nor_27_a34

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_areduce_nor_27_a41,
	datad => UUMS2_areduce_nor_27_a34,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_27_a119);

UUMS2_ai_a5583_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5583 = !UUMS2_areduce_nor_27_a119 & UUMS2_areduce_nor_10_a19 & !UUMS2_areduce_nor_27_a118 & !UUMS2_areduce_nor_27_a111

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_27_a119,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_areduce_nor_27_a118,
	datad => UUMS2_areduce_nor_27_a111,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5583);

UUMS2_aSelect_1466_rtl_1_rtl_17_a14_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a14 = UUMS2_ai_a1083 & UUMS2_aSelect_1466_rtl_1_rtl_17_a125 & !UUMS2_astep_clk & UUMS2_ai_a5583

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a1083,
	datab => UUMS2_aSelect_1466_rtl_1_rtl_17_a125,
	datac => UUMS2_astep_clk,
	datad => UUMS2_ai_a5583,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a14);

UUMS2_aUMS_STATE_a9_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a9 = DFFE(UUMS2_aSelect_1466_rtl_1_rtl_17_a135 # UUMS2_aSelect_1466_rtl_1_rtl_17_a14 # !UUMS2_ai_a764 & UUMS2_aSelect_1466_rtl_1_rtl_17_a150, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a135,
	datab => UUMS2_ai_a764,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a150,
	datad => UUMS2_aSelect_1466_rtl_1_rtl_17_a14,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a9);

UUMS2_aSelect_1466_rtl_1_rtl_17_a86_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a86 = UUMS2_aUMS_STATE_a9 & !UUMS2_aSW_HS_DB & (!UUMS2_aSW_OUT_DB # !UUMS2_aSW_IN_DB)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0222",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a9,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aSW_IN_DB,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a86);

UUMS2_aSelect_1472_rtl_4_rtl_20_a29_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a29 = UUMS2_aUMS_STATE_a21 # !UUMS2_areduce_nor_37_a98 & !UUMS2_aUMS_STATE_a8

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF03",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_areduce_nor_37_a98,
	datac => UUMS2_aUMS_STATE_a8,
	datad => UUMS2_aUMS_STATE_a21,
	devclrn => devclrn,
	devpor => devpor,
	cascout => UUMS2_aSelect_1472_rtl_4_rtl_20_a29);

UUMS2_aSelect_1472_rtl_4_rtl_20_a292_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a292 = (!UUMS2_aSW_IN_DB & !UUMS2_aSW_HS_DB & UUMS2_aSW_OUT_DB) & CASCADE(UUMS2_aSelect_1472_rtl_4_rtl_20_a29)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1010",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_IN_DB,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aSW_OUT_DB,
	cascin => UUMS2_aSelect_1472_rtl_4_rtl_20_a29,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1472_rtl_4_rtl_20_a292);

UUMS2_aSelect_1472_rtl_4_rtl_20_a285_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a285 = UUMS2_aSelect_1472_rtl_4_rtl_20_a264 # UUMS2_aSelect_1472_rtl_4_rtl_20_a292 # !UUMS2_ai_a5574 & UUMS2_aSelect_1466_rtl_1_rtl_17_a86

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1472_rtl_4_rtl_20_a264,
	datab => UUMS2_ai_a5574,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a86,
	datad => UUMS2_aSelect_1472_rtl_4_rtl_20_a292,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1472_rtl_4_rtl_20_a285);

UUMS2_aSelect_1482_rtl_9_rtl_25_a243_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a243 = !CMD_OUT_acombout & !CMD_IN_acombout
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a308 = !CMD_OUT_acombout & !CMD_IN_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => CMD_OUT_acombout,
	datad => CMD_IN_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a243,
	cascout => UUMS2_aSelect_1482_rtl_9_rtl_25_a308);

UUMS2_aSelect_1472_rtl_4_rtl_20_a293_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1472_rtl_4_rtl_20_a293 = (UUMS2_aUMS_STATE_a16 & (UUMS2_aSelect_1482_rtl_9_rtl_25_a243 & UUMS2_aUMS_STATE_a18 # !UUMS2_ai_a761) # !UUMS2_aUMS_STATE_a16 & UUMS2_aSelect_1482_rtl_9_rtl_25_a243 & UUMS2_aUMS_STATE_a18) & CASCADE(UUMS2_aSelect_1466_rtl_1_rtl_17_a151)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "C0EA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a16,
	datab => UUMS2_aSelect_1482_rtl_9_rtl_25_a243,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_ai_a761,
	cascin => UUMS2_aSelect_1466_rtl_1_rtl_17_a151,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1472_rtl_4_rtl_20_a293);

UUMS2_aUMS_STATE_a12_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a12 = DFFE(UUMS2_aSelect_1472_rtl_4_rtl_20_a285 # UUMS2_aSW_OUT_DB & UUMS2_ai_a5571 & UUMS2_aSelect_1472_rtl_4_rtl_20_a293, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECCC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aSelect_1472_rtl_4_rtl_20_a285,
	datac => UUMS2_ai_a5571,
	datad => UUMS2_aSelect_1472_rtl_4_rtl_20_a293,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a12);

UUMS2_aSelect_1484_rtl_10_rtl_26_a340_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a340 = Cmd_Mid_Out_acombout & !UUMS2_aSW_OUT_DB & !UUMS2_aSW_HS_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Cmd_Mid_Out_acombout,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aSW_HS_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a340);

UUMS2_aSelect_1484_rtl_10_rtl_26_a347_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a347 = !UUMS2_ai_a3089 & UUMS2_aSelect_1484_rtl_10_rtl_26_a340 & (UUMS2_aSelect_1478_rtl_7_rtl_23_a183 # UUMS2_aUMS_STATE_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a3089,
	datab => UUMS2_aSelect_1478_rtl_7_rtl_23_a183,
	datac => UUMS2_aUMS_STATE_a12,
	datad => UUMS2_aSelect_1484_rtl_10_rtl_26_a340,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a347);

UUMS2_aSelect_1484_rtl_10_rtl_26_a367_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a367 = UUMS2_aSelect_1484_rtl_10_rtl_26_a347 # UUMS2_aSelect_1484_rtl_10_rtl_26_a373 & Cmd_Mid_Out_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF88",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1484_rtl_10_rtl_26_a373,
	datab => Cmd_Mid_Out_acombout,
	datad => UUMS2_aSelect_1484_rtl_10_rtl_26_a347,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a367);

UUMS2_aSelect_1484_rtl_10_rtl_26_a333_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1484_rtl_10_rtl_26_a333 = !CMD_IN_acombout & !CMD_OUT_acombout & UUMS2_aUMS_STATE_a18 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => CMD_OUT_acombout,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1484_rtl_10_rtl_26_a333);

UUMS2_aUMS_STATE_a18_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a18 = DFFE(UUMS2_aSelect_1484_rtl_10_rtl_26_a367 # UUMS2_aSelect_1484_rtl_10_rtl_26_a333 & (UUMS2_aSelect_1484_rtl_10_rtl_26_a33 # !UUMS2_ai_a5571), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "ECFC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1484_rtl_10_rtl_26_a33,
	datab => UUMS2_aSelect_1484_rtl_10_rtl_26_a367,
	datac => UUMS2_aSelect_1484_rtl_10_rtl_26_a333,
	datad => UUMS2_ai_a5571,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a18);

UUMS2_aSelect_1474_rtl_5_rtl_21_a6_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1474_rtl_5_rtl_21_a6 = UUMS2_aSW_OUT_DB & !UUMS2_aSW_IN_DB & UUMS2_aUMS_STATE_a17 # !UUMS2_aSW_OUT_DB & (UUMS2_aUMS_STATE_a18 # !UUMS2_aSW_IN_DB & UUMS2_aUMS_STATE_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4F44",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aUMS_STATE_a18,
	datac => UUMS2_aSW_IN_DB,
	datad => UUMS2_aUMS_STATE_a17,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1474_rtl_5_rtl_21_a6);

UUMS2_aSelect_1474_rtl_5_rtl_21_a87_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1474_rtl_5_rtl_21_a87 = UUMS2_aSelect_1474_rtl_5_rtl_21_a6 & !UUMS2_aSW_HS_DB & UUMS2_aSelect_1482_rtl_9_rtl_25_a243 & UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1474_rtl_5_rtl_21_a6,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aSelect_1482_rtl_9_rtl_25_a243,
	datad => UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1474_rtl_5_rtl_21_a87);

UUMS2_ai_a311_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a311 = CMD_OUT_acombout & !UUMS2_aSW_OUT_DB
-- UUMS2_ai_a5688 = CMD_OUT_acombout & !UUMS2_aSW_OUT_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CMD_OUT_acombout,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a311,
	cascout => UUMS2_ai_a5688);

UUMS2_aSelect_1474_rtl_5_rtl_21_a69_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1474_rtl_5_rtl_21_a69 = !UUMS2_ai_a311 & !UUMS2_ai_a5591 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aSelect_1474_rtl_5_rtl_21_a91

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "1000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a311,
	datab => UUMS2_ai_a5591,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datad => UUMS2_aSelect_1474_rtl_5_rtl_21_a91,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1474_rtl_5_rtl_21_a69);

UUMS2_aSelect_1474_rtl_5_rtl_21_a5_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1474_rtl_5_rtl_21_a5 = UUMS2_aSelect_1472_rtl_4_rtl_20_a222 & UUMS2_aSelect_1474_rtl_5_rtl_21_a69 & (UUMS2_aSW_OUT_DB # !Cmd_Wd_a78)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8A00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1472_rtl_4_rtl_20_a222,
	datab => UUMS2_aSW_OUT_DB,
	datac => Cmd_Wd_a78,
	datad => UUMS2_aSelect_1474_rtl_5_rtl_21_a69,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1474_rtl_5_rtl_21_a5);

UUMS2_aUMS_STATE_a13_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a13 = DFFE(UUMS2_aSelect_1474_rtl_5_rtl_21_a5 # UUMS2_aSelect_1474_rtl_5_rtl_21_a87 & !UUMS2_astep_clk & UUMS2_ai_a5583, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F2F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1474_rtl_5_rtl_21_a87,
	datab => UUMS2_astep_clk,
	datac => UUMS2_aSelect_1474_rtl_5_rtl_21_a5,
	datad => UUMS2_ai_a5583,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a13);

UUMS2_aSelect_1474_rtl_5_rtl_21_a91_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1474_rtl_5_rtl_21_a91 = UUMS2_aUMS_STATE_a13 & (UUMS2_aHI_Cnt_En # !Det_Sat_acombout & !Hi_Cnt_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "888C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aHI_Cnt_En,
	datab => UUMS2_aUMS_STATE_a13,
	datac => Det_Sat_acombout,
	datad => Hi_Cnt_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1474_rtl_5_rtl_21_a91);

UUMS2_aSelect_1480_rtl_8_rtl_24_a255_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1480_rtl_8_rtl_24_a255 = (UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & (!UUMS2_ai_a5591 & UUMS2_aSelect_1474_rtl_5_rtl_21_a91 # !UUMS2_aSelect_1480_rtl_8_rtl_24_a252)) & CASCADE(UUMS2_ai_a5688)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1480_rtl_8_rtl_24_a252,
	datab => UUMS2_ai_a5591,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datad => UUMS2_aSelect_1474_rtl_5_rtl_21_a91,
	cascin => UUMS2_ai_a5688,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1480_rtl_8_rtl_24_a255);

UUMS2_aSelect_1480_rtl_8_rtl_24_a30_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1480_rtl_8_rtl_24_a30 = !UUMS2_aSW_OUT_DB & !UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0033",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aPCT110_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1480_rtl_8_rtl_24_a30);

UUMS2_aSelect_1466_rtl_1_rtl_17_a84_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1466_rtl_1_rtl_17_a84 = !UUMS2_ai_a5585 & !UUMS2_aSW_HS_DB & UUMS2_aUMS_STATE_a16 & !UUMS2_ai_a761

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0010",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5585,
	datab => UUMS2_aSW_HS_DB,
	datac => UUMS2_aUMS_STATE_a16,
	datad => UUMS2_ai_a761,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1466_rtl_1_rtl_17_a84);

UUMS2_aUMS_STATE_a16_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a16 = DFFE(UUMS2_aSelect_1480_rtl_8_rtl_24_a255 # UUMS2_aSelect_1466_rtl_1_rtl_17_a84 & (UUMS2_aSelect_1480_rtl_8_rtl_24_a30 # !UUMS2_ai_a5571), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EAFA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1480_rtl_8_rtl_24_a255,
	datab => UUMS2_aSelect_1480_rtl_8_rtl_24_a30,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a84,
	datad => UUMS2_ai_a5571,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a16);

UUMS2_aSelect_1468_rtl_2_rtl_18_a88_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a88 = CMD_OUT_acombout & (UUMS2_aUMS_STATE_a18 # UUMS2_aUMS_STATE_a17) # !CMD_OUT_acombout & CMD_IN_acombout & (UUMS2_aUMS_STATE_a18 # UUMS2_aUMS_STATE_a17)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EEE0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_OUT_acombout,
	datab => CMD_IN_acombout,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_aUMS_STATE_a17,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a88);

UUMS2_aSelect_1468_rtl_2_rtl_18_a414_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a414 = UUMS2_aSelect_1468_rtl_2_rtl_18_a87 # UUMS2_aSelect_1468_rtl_2_rtl_18_a88 # UUMS2_ai_a761 & UUMS2_aUMS_STATE_a16

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1468_rtl_2_rtl_18_a87,
	datab => UUMS2_ai_a761,
	datac => UUMS2_aUMS_STATE_a16,
	datad => UUMS2_aSelect_1468_rtl_2_rtl_18_a88,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a414);

UUMS2_aSelect_1468_rtl_2_rtl_18_a421_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a421 = UUMS2_aSW_IN_DB & !UUMS2_aSW_OUT_DB # !UUMS2_aSW_IN_DB & (UUMS2_aHI_Cnt_En # !CMD_IN_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3F1D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => UUMS2_aSW_IN_DB,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aHI_Cnt_En,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a421);

UUMS2_aSelect_1468_rtl_2_rtl_18_a428_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a428 = UUMS2_aSelect_1468_rtl_2_rtl_18_a421 & UUMS2_aSelect_1472_rtl_4_rtl_20_a222 & !UUMS2_aCMD_ERROR_a32 & UUMS2_aSelect_1478_rtl_7_rtl_23_a183

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1468_rtl_2_rtl_18_a421,
	datab => UUMS2_aSelect_1472_rtl_4_rtl_20_a222,
	datac => UUMS2_aCMD_ERROR_a32,
	datad => UUMS2_aSelect_1478_rtl_7_rtl_23_a183,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a428);

UUMS2_ai_a2900_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a2900 = !UUMS2_aSW_OUT_DB & Cmd_Wd_a78

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aSW_OUT_DB,
	datad => Cmd_Wd_a78,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a2900);

UUMS2_aSelect_1470_rtl_3_rtl_19_a89_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a89 = !UUMS2_aSW_OUT_DB & (UUMS2_aUMS_STATE_a21 # !UUMS2_aUMS_STATE_a8 & !UUMS2_areduce_nor_37_a98)
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a146 = !UUMS2_aSW_OUT_DB & (UUMS2_aUMS_STATE_a21 # !UUMS2_aUMS_STATE_a8 & !UUMS2_areduce_nor_37_a98)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0C0D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a8,
	datab => UUMS2_aUMS_STATE_a21,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_areduce_nor_37_a98,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a89,
	cascout => UUMS2_aSelect_1470_rtl_3_rtl_19_a146);

UUMS2_aSelect_1468_rtl_2_rtl_18_a91_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1468_rtl_2_rtl_18_a91 = UUMS2_aSelect_1468_rtl_2_rtl_18_a428 & (!UUMS2_aSW_IN_DB & UUMS2_aSelect_1470_rtl_3_rtl_19_a89 # !UUMS2_ai_a2900) # !UUMS2_aSelect_1468_rtl_2_rtl_18_a428 & !UUMS2_aSW_IN_DB & UUMS2_aSelect_1470_rtl_3_rtl_19_a89

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3B0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1468_rtl_2_rtl_18_a428,
	datab => UUMS2_aSW_IN_DB,
	datac => UUMS2_ai_a2900,
	datad => UUMS2_aSelect_1470_rtl_3_rtl_19_a89,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1468_rtl_2_rtl_18_a91);

UUMS2_aUMS_STATE_a10_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a10 = DFFE(!UUMS2_aSW_HS_DB & (UUMS2_aSelect_1468_rtl_2_rtl_18_a91 # UUMS2_aSelect_1468_rtl_2_rtl_18_a414 & !UUMS2_ai_a5585), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5504",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_HS_DB,
	datab => UUMS2_aSelect_1468_rtl_2_rtl_18_a414,
	datac => UUMS2_ai_a5585,
	datad => UUMS2_aSelect_1468_rtl_2_rtl_18_a91,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a10);

UUMS2_aSelect_1478_rtl_7_rtl_23_a183_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1478_rtl_7_rtl_23_a183 = UUMS2_aUMS_STATE_a10 & (UUMS2_aSW_OUT_DB # !CMD_OUT_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F030",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => CMD_OUT_acombout,
	datac => UUMS2_aUMS_STATE_a10,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1478_rtl_7_rtl_23_a183);

UUMS2_aSelect_1478_rtl_7_rtl_23_a185_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1478_rtl_7_rtl_23_a185 = !UUMS2_aUMS_STATE_a12 & !UUMS2_aUMS_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a12,
	datac => UUMS2_aUMS_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1478_rtl_7_rtl_23_a185);

UUMS2_aSelect_1478_rtl_7_rtl_23_a17_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1478_rtl_7_rtl_23_a17 = UUMS2_ai_a5591 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & (UUMS2_aSelect_1478_rtl_7_rtl_23_a183 # !UUMS2_aSelect_1478_rtl_7_rtl_23_a185)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "80A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5591,
	datab => UUMS2_aSelect_1478_rtl_7_rtl_23_a183,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datad => UUMS2_aSelect_1478_rtl_7_rtl_23_a185,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1478_rtl_7_rtl_23_a17);

UUMS2_aUMS_STATE_a15_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a15 = DFFE(UUMS2_aSelect_1478_rtl_7_rtl_23_a17 # UUMS2_aSelect_1478_rtl_7_rtl_23_a184 & (!UUMS2_ai_a5571 # !UUMS2_ai_a3076), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F2FA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1478_rtl_7_rtl_23_a184,
	datab => UUMS2_ai_a3076,
	datac => UUMS2_aSelect_1478_rtl_7_rtl_23_a17,
	datad => UUMS2_ai_a5571,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a15);

UUMS2_aSelect_1478_rtl_7_rtl_23_a184_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1478_rtl_7_rtl_23_a184 = !CMD_OUT_acombout & UUMS2_ai_a5586 & UUMS2_aUMS_STATE_a15 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_OUT_acombout,
	datab => UUMS2_ai_a5586,
	datac => UUMS2_aUMS_STATE_a15,
	datad => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1478_rtl_7_rtl_23_a184);

UUMS2_aSelect_1470_rtl_3_rtl_19_a115_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a115 = !UUMS2_aCMD_ERROR_a32 & UUMS2_aUMS_STATE_a11 & UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & !UUMS2_ai_a311

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0040",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_ERROR_a32,
	datab => UUMS2_aUMS_STATE_a11,
	datac => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datad => UUMS2_ai_a311,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a115);

UUMS2_aSelect_1470_rtl_3_rtl_19_a145_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a145 = (!UUMS2_aSW_HS_DB & UUMS2_aSW_IN_DB & !UUMS2_aSW_OUT_DB) & CASCADE(UUMS2_aSelect_1470_rtl_3_rtl_19_a146)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0404",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_HS_DB,
	datab => UUMS2_aSW_IN_DB,
	datac => UUMS2_aSW_OUT_DB,
	cascin => UUMS2_aSelect_1470_rtl_3_rtl_19_a146,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a145);

UUMS2_aSelect_1470_rtl_3_rtl_19_a132_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a132 = UUMS2_aSelect_1470_rtl_3_rtl_19_a145 # !UUMS2_ai_a2900 & UUMS2_aSelect_1472_rtl_4_rtl_20_a222 & UUMS2_aSelect_1470_rtl_3_rtl_19_a115

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF40",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a2900,
	datab => UUMS2_aSelect_1472_rtl_4_rtl_20_a222,
	datac => UUMS2_aSelect_1470_rtl_3_rtl_19_a115,
	datad => UUMS2_aSelect_1470_rtl_3_rtl_19_a145,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a132);

UUMS2_ai_a3082_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a3082 = !UUMS2_astep_clk & UUMS2_ai_a5583 & (UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out # UUMS2_aSW_IN_DB)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5400",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_clk,
	datab => UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out,
	datac => UUMS2_aSW_IN_DB,
	datad => UUMS2_ai_a5583,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a3082);

UUMS2_aSelect_1482_rtl_9_rtl_25_a307_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a307 = (UUMS2_aUMS_STATE_a17 & !UUMS2_aSW_HS_DB & (!UUMS2_aSW_OUT_DB # !UUMS2_aSW_IN_DB)) & CASCADE(UUMS2_aSelect_1482_rtl_9_rtl_25_a308)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "004C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_IN_DB,
	datab => UUMS2_aUMS_STATE_a17,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_aSW_HS_DB,
	cascin => UUMS2_aSelect_1482_rtl_9_rtl_25_a308,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a307);

UUMS2_aSelect_1470_rtl_3_rtl_19_a8_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1470_rtl_3_rtl_19_a8 = !UUMS2_astep_clk & UUMS2_aSelect_1482_rtl_9_rtl_25_a307 & UUMS2_aSW_IN_DB & UUMS2_ai_a5583

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "4000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_clk,
	datab => UUMS2_aSelect_1482_rtl_9_rtl_25_a307,
	datac => UUMS2_aSW_IN_DB,
	datad => UUMS2_ai_a5583,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1470_rtl_3_rtl_19_a8);

UUMS2_aUMS_STATE_a11_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a11 = DFFE(UUMS2_aSelect_1470_rtl_3_rtl_19_a132 # UUMS2_aSelect_1470_rtl_3_rtl_19_a8 # UUMS2_aSelect_1478_rtl_7_rtl_23_a184 & UUMS2_ai_a3082, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1478_rtl_7_rtl_23_a184,
	datab => UUMS2_aSelect_1470_rtl_3_rtl_19_a132,
	datac => UUMS2_ai_a3082,
	datad => UUMS2_aSelect_1470_rtl_3_rtl_19_a8,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a11);

UUMS2_aSelect_1486_rtl_11_rtl_27_a17_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1486_rtl_11_rtl_27_a17 = UUMS2_aUMS_STATE_a10 # UUMS2_aUMS_STATE_a13

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FCFC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a10,
	datac => UUMS2_aUMS_STATE_a13,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1486_rtl_11_rtl_27_a17);

UUMS2_aSelect_1486_rtl_11_rtl_27_a131_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1486_rtl_11_rtl_27_a131 = !Cmd_Mid_Out_acombout & Cmd_Wd_a78

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => Cmd_Mid_Out_acombout,
	datad => Cmd_Wd_a78,
	devclrn => devclrn,
	devpor => devpor,
	cascout => UUMS2_aSelect_1486_rtl_11_rtl_27_a131);

UUMS2_aSelect_1486_rtl_11_rtl_27_a151_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1486_rtl_11_rtl_27_a151 = (UUMS2_aSelect_1484_rtl_10_rtl_26_a374 & (UUMS2_aUMS_STATE_a11 # !UUMS2_ai_a5591 & UUMS2_aSelect_1486_rtl_11_rtl_27_a17)) & CASCADE(UUMS2_aSelect_1486_rtl_11_rtl_27_a131)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "B0A0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a11,
	datab => UUMS2_ai_a5591,
	datac => UUMS2_aSelect_1484_rtl_10_rtl_26_a374,
	datad => UUMS2_aSelect_1486_rtl_11_rtl_27_a17,
	cascin => UUMS2_aSelect_1486_rtl_11_rtl_27_a131,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1486_rtl_11_rtl_27_a151);

UUMS2_aUMS_STATE_a19_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a19 = DFFE(UUMS2_aSelect_1486_rtl_11_rtl_27_a151 # UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & UUMS2_aUMS_STATE_a19 & !UUMS2_ai_a5589, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CCEC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datab => UUMS2_aSelect_1486_rtl_11_rtl_27_a151,
	datac => UUMS2_aUMS_STATE_a19,
	datad => UUMS2_ai_a5589,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a19);

UUMS2_areduce_nor_71_aI : apex20ke_lcell 
-- Equation(s):
-- UUMS2_areduce_nor_71 = UUMS2_aUMS_STATE_a17 # UUMS2_aUMS_STATE_a19 # UUMS2_aUMS_STATE_a15 # !UUMS2_areduce_nor_71_a25

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFEF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a17,
	datab => UUMS2_aUMS_STATE_a19,
	datac => UUMS2_areduce_nor_71_a25,
	datad => UUMS2_aUMS_STATE_a15,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_areduce_nor_71);

UUMS2_ai_a5579_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5579 = UUMS2_areduce_nor_71 & UUMS2_areduce_nor_10_a19 & !UUMS2_astep_clk & UUMS2_areduce_nor_27_a124

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_71,
	datab => UUMS2_areduce_nor_10_a19,
	datac => UUMS2_astep_clk,
	datad => UUMS2_areduce_nor_27_a124,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5579);

UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a31_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a = DFFE(!GLOBAL(UUMS2_ai_a5678) & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a $ (UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a_aCOUT & UUMS2_ai_a5579), GLOBAL(clk20_acombout), , , !imr_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	datad => UUMS2_ai_a5579,
	cin => UUMS2_aCMD_CNT_rtl_36_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => clk20_acombout,
	ena => NOT_imr_acombout,
	sclr => UUMS2_ai_a5678,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a);

CNT_100_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(30),
	combout => CNT_100_a30_a_acombout);

CNT_100_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(29),
	combout => CNT_100_a29_a_acombout);

CNT_100_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(28),
	combout => CNT_100_a28_a_acombout);

CNT_100_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(27),
	combout => CNT_100_a27_a_acombout);

CNT_100_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(26),
	combout => CNT_100_a26_a_acombout);

CNT_100_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(25),
	combout => CNT_100_a25_a_acombout);

CNT_100_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(24),
	combout => CNT_100_a24_a_acombout);

CNT_100_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(23),
	combout => CNT_100_a23_a_acombout);

CNT_100_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(22),
	combout => CNT_100_a22_a_acombout);

CNT_100_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(21),
	combout => CNT_100_a21_a_acombout);

CNT_100_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(20),
	combout => CNT_100_a20_a_acombout);

CNT_100_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(19),
	combout => CNT_100_a19_a_acombout);

CNT_100_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(18),
	combout => CNT_100_a18_a_acombout);

CNT_100_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(17),
	combout => CNT_100_a17_a_acombout);

CNT_100_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(16),
	combout => CNT_100_a16_a_acombout);

CNT_100_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(15),
	combout => CNT_100_a15_a_acombout);

CNT_100_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(14),
	combout => CNT_100_a14_a_acombout);

CNT_100_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(13),
	combout => CNT_100_a13_a_acombout);

CNT_100_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(12),
	combout => CNT_100_a12_a_acombout);

CNT_100_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(11),
	combout => CNT_100_a11_a_acombout);

CNT_100_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(10),
	combout => CNT_100_a10_a_acombout);

CNT_100_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(9),
	combout => CNT_100_a9_a_acombout);

CNT_100_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(8),
	combout => CNT_100_a8_a_acombout);

CNT_100_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(7),
	combout => CNT_100_a7_a_acombout);

CNT_100_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(6),
	combout => CNT_100_a6_a_acombout);

CNT_100_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(5),
	combout => CNT_100_a5_a_acombout);

CNT_100_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(4),
	combout => CNT_100_a4_a_acombout);

CNT_100_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(3),
	combout => CNT_100_a3_a_acombout);

CNT_100_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(2),
	combout => CNT_100_a2_a_acombout);

CNT_100_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(1),
	combout => CNT_100_a1_a_acombout);

CNT_100_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => GND,
	ena => VCC,
	padio => ww_CNT_100(0),
	combout => CNT_100_a0_a_acombout);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a_a1028_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a = CARRY(!CNT_100_a0_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "0044",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a0_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a_a1027_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & CNT_100_a1_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a & (CNT_100_a1_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a1_a,
	datab => CNT_100_a1_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a_a1026_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a # !CNT_100_a2_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a & !CNT_100_a2_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a2_a,
	datab => CNT_100_a2_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a_a1025_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a = CARRY(CNT_100_a3_a_acombout & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a) # !CNT_100_a3_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a3_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a3_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a_a1024_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a = CARRY(CNT_100_a4_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a # !CNT_100_a4_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a4_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a4_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a_a1023_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a = CARRY(CNT_100_a5_a_acombout & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a) # !CNT_100_a5_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a5_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a5_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a_a1022_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a # !CNT_100_a6_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a & !CNT_100_a6_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a6_a,
	datab => CNT_100_a6_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a_a1021_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & CNT_100_a7_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a & (CNT_100_a7_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a7_a,
	datab => CNT_100_a7_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a_a1020_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a = CARRY(CNT_100_a8_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a # !CNT_100_a8_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a8_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a8_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a_a1019_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & CNT_100_a9_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a & (CNT_100_a9_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a9_a,
	datab => CNT_100_a9_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a_a1018_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a # !CNT_100_a10_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a & !CNT_100_a10_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a10_a,
	datab => CNT_100_a10_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a_a1017_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & CNT_100_a11_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a & (CNT_100_a11_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a11_a,
	datab => CNT_100_a11_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a_a1016_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a # !CNT_100_a12_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a & !CNT_100_a12_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a12_a,
	datab => CNT_100_a12_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a_a1015_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & CNT_100_a13_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a & (CNT_100_a13_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a13_a,
	datab => CNT_100_a13_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a_a1014_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a # !CNT_100_a14_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a & !CNT_100_a14_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a14_a,
	datab => CNT_100_a14_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a_a1013_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & CNT_100_a15_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a & (CNT_100_a15_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a15_a,
	datab => CNT_100_a15_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a_a1012_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a # !CNT_100_a16_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a & !CNT_100_a16_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a16_a,
	datab => CNT_100_a16_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a_a1011_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & CNT_100_a17_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a & (CNT_100_a17_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a17_a,
	datab => CNT_100_a17_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a_a1010_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a # !CNT_100_a18_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a & !CNT_100_a18_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a18_a,
	datab => CNT_100_a18_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a_a1009_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & CNT_100_a19_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a & (CNT_100_a19_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a19_a,
	datab => CNT_100_a19_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a_a1008_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a # !CNT_100_a20_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a & !CNT_100_a20_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a20_a,
	datab => CNT_100_a20_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a_a1007_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & CNT_100_a21_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a & (CNT_100_a21_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a21_a,
	datab => CNT_100_a21_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a_a1006_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a # !CNT_100_a22_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a & !CNT_100_a22_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a22_a,
	datab => CNT_100_a22_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a_a1005_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & CNT_100_a23_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a & (CNT_100_a23_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a23_a,
	datab => CNT_100_a23_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a_a1004_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a # !CNT_100_a24_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a & !CNT_100_a24_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a24_a,
	datab => CNT_100_a24_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a_a1003_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & CNT_100_a25_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a & (CNT_100_a25_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a25_a,
	datab => CNT_100_a25_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a_a1002_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a # !CNT_100_a26_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a & !CNT_100_a26_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a26_a,
	datab => CNT_100_a26_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a_a1001_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a = CARRY(CNT_100_a27_a_acombout & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a) # !CNT_100_a27_a_acombout & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a27_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a27_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a_a1000_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & (!UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a # !CNT_100_a28_a_acombout) # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a & !CNT_100_a28_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "002B",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a28_a,
	datab => CNT_100_a28_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a_a999_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a = CARRY(UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & CNT_100_a29_a_acombout & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a & (CNT_100_a29_a_acombout # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a29_a,
	datab => CNT_100_a29_a_acombout,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a_a998_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a = CARRY(CNT_100_a30_a_acombout & UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a & !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a # !CNT_100_a30_a_acombout & (UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a # !UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "004D",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a30_a_acombout,
	datab => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a30_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	cout => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a);

UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out_node : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out = CNT_100_a31_a_acombout & (UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a # !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a) # !CNT_100_a31_a_acombout & UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a & !UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A0FA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CNT_100_a31_a_acombout,
	datad => UUMS2_aCMD_CNT_rtl_36_awysi_counter_asload_path_a31_a,
	cin => UUMS2_aPCT100_CMP_acomparator_acmp_end_alcarry_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out);

UUMS2_ai_a3076_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a3076 = UUMS2_aSW_IN_DB # UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aSW_IN_DB,
	datad => UUMS2_aPCT100_CMP_acomparator_acmp_end_aagb_out,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a3076);

UUMS2_aSelect_1482_rtl_9_rtl_25_a10_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a10 = UUMS2_aUMS_STATE_a11 # UUMS2_aUMS_STATE_a13 & (UUMS2_aHI_Cnt_En # !CMD_IN_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFD0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => CMD_IN_acombout,
	datab => UUMS2_aHI_Cnt_En,
	datac => UUMS2_aUMS_STATE_a13,
	datad => UUMS2_aUMS_STATE_a11,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a10);

UUMS2_ai_a5587_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5587 = Cmd_Mid_In_acombout & !UUMS2_aSW_IN_DB

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Cmd_Mid_In_acombout,
	datad => UUMS2_aSW_IN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5587);

UUMS2_aSelect_1482_rtl_9_rtl_25_a286_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a286 = UUMS2_aSelect_1482_rtl_9_rtl_25_a306 & !UUMS2_ai_a311 & UUMS2_aSelect_1482_rtl_9_rtl_25_a10 & UUMS2_ai_a5587

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "2000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1482_rtl_9_rtl_25_a306,
	datab => UUMS2_ai_a311,
	datac => UUMS2_aSelect_1482_rtl_9_rtl_25_a10,
	datad => UUMS2_ai_a5587,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a286);

UUMS2_aSelect_1482_rtl_9_rtl_25_a270_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a270 = Cmd_Mid_In_acombout & !UUMS2_aSW_IN_DB & (UUMS2_aSelect_1478_rtl_7_rtl_23_a183 # UUMS2_aUMS_STATE_a12)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00E0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1478_rtl_7_rtl_23_a183,
	datab => UUMS2_aUMS_STATE_a12,
	datac => Cmd_Mid_In_acombout,
	datad => UUMS2_aSW_IN_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a270);

UUMS2_aSelect_1482_rtl_9_rtl_25_a299_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aSelect_1482_rtl_9_rtl_25_a299 = UUMS2_aSelect_1482_rtl_9_rtl_25_a286 # UUMS2_aSelect_1466_rtl_1_rtl_17_a83 & !UUMS2_ai_a5591 & UUMS2_aSelect_1482_rtl_9_rtl_25_a270

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AEAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSelect_1482_rtl_9_rtl_25_a286,
	datab => UUMS2_aSelect_1466_rtl_1_rtl_17_a83,
	datac => UUMS2_ai_a5591,
	datad => UUMS2_aSelect_1482_rtl_9_rtl_25_a270,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aSelect_1482_rtl_9_rtl_25_a299);

UUMS2_aUMS_STATE_a17_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a17 = DFFE(UUMS2_aSelect_1482_rtl_9_rtl_25_a299 # UUMS2_aSelect_1482_rtl_9_rtl_25_a307 & (!UUMS2_ai_a5571 # !UUMS2_ai_a3076), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F4FC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a3076,
	datab => UUMS2_aSelect_1482_rtl_9_rtl_25_a307,
	datac => UUMS2_aSelect_1482_rtl_9_rtl_25_a299,
	datad => UUMS2_ai_a5571,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_STATE_a17);

UUMS2_aUMS_DIR_areg0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_DIR_areg0 = DFFE(!UUMS2_aUMS_STATE_a17 & !UUMS2_aUMS_STATE_a11 & !UUMS2_aUMS_STATE_a15, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a17,
	datac => UUMS2_aUMS_STATE_a11,
	datad => UUMS2_aUMS_STATE_a15,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aUMS_DIR_areg0);

i_a25_I : apex20ke_lcell 
-- Equation(s):
-- i_a25 = URTEM_aU_RT_IN_amove_areg0 & RTEM_SEL_acombout & UUMS2_aUMS_DIR_areg0 # !URTEM_aU_RT_IN_amove_areg0 & (UUMS2_aUMS_DIR_areg0 # !RTEM_SEL_acombout)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_amove_areg0,
	datac => RTEM_SEL_acombout,
	datad => UUMS2_aUMS_DIR_areg0,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a25);

URTEM_aU_RT_OUT_amv_cmdb_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_amv_cmdb = DFFE(CMD_OUT_acombout # URTEM_aRTEM_OUT_CMD_a27 # URTEM_aU_RT_OUT_amv_cmdb & !URTEM_aU_RT_OUT_aimove, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFCE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_amv_cmdb,
	datab => CMD_OUT_acombout,
	datac => URTEM_aU_RT_OUT_aimove,
	datad => URTEM_aRTEM_OUT_CMD_a27,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_amv_cmdb);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a $ !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a & !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a,
	cout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a_aCOUT);

URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a = DFFE(GLOBAL(URTEM_aU_RT_OUT_aimove) & URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a_aCOUT $ URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a,
	cin => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sclr => NOT_URTEM_aU_RT_OUT_aimove,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a);

URTEM_aU_RT_OUT_areduce_nor_35_a59_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a59 = !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a18_a,
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a19_a,
	datac => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a16_a,
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a59);

URTEM_aU_RT_OUT_areduce_nor_35_a64_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a64 = URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a # URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a # URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a15_a,
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a14_a,
	datac => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a13_a,
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a64);

URTEM_aU_RT_OUT_areduce_nor_35_a73_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a73 = URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a # URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a # URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a8_a,
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a9_a,
	datac => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a11_a,
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a73);

URTEM_aU_RT_OUT_areduce_nor_35_a86_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a86 = URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a # URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FBFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a6_a,
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a5_a,
	datac => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a7_a,
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a86);

URTEM_aU_RT_OUT_areduce_nor_35_a134_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a134 = URTEM_aU_RT_OUT_areduce_nor_35_a59 # URTEM_aU_RT_OUT_areduce_nor_35_a64 # URTEM_aU_RT_OUT_areduce_nor_35_a73 # URTEM_aU_RT_OUT_areduce_nor_35_a86

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_areduce_nor_35_a59,
	datab => URTEM_aU_RT_OUT_areduce_nor_35_a64,
	datac => URTEM_aU_RT_OUT_areduce_nor_35_a73,
	datad => URTEM_aU_RT_OUT_areduce_nor_35_a86,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a134);

URTEM_aU_RT_OUT_areduce_nor_35_a103_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_areduce_nor_35_a103 = !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a # !URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "7FFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a2_a,
	datab => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a1_a,
	datac => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a3_a,
	datad => URTEM_aU_RT_OUT_aMv_Cnt_rtl_35_awysi_counter_asload_path_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aU_RT_OUT_areduce_nor_35_a103);

URTEM_aU_RT_OUT_aimove_aI : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_aimove = DFFE(URTEM_aU_RT_OUT_amv_cmdb # URTEM_aU_RT_OUT_aimove & (URTEM_aU_RT_OUT_areduce_nor_35_a134 # URTEM_aU_RT_OUT_areduce_nor_35_a103), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FAEA",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_amv_cmdb,
	datab => URTEM_aU_RT_OUT_areduce_nor_35_a134,
	datac => URTEM_aU_RT_OUT_aimove,
	datad => URTEM_aU_RT_OUT_areduce_nor_35_a103,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_aimove);

URTEM_aU_RT_OUT_amove_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aU_RT_OUT_amove_areg0 = DFFE(URTEM_aU_RT_OUT_aimove, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => URTEM_aU_RT_OUT_aimove,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aU_RT_OUT_amove_areg0);

i_a28_I : apex20ke_lcell 
-- Equation(s):
-- i_a28 = UUMS2_astep_clk & (RTEM_SEL_acombout # !URTEM_aU_RT_OUT_amove_areg0) # !UUMS2_astep_clk & !URTEM_aU_RT_OUT_amove_areg0 & !RTEM_SEL_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AA0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_astep_clk,
	datac => URTEM_aU_RT_OUT_amove_areg0,
	datad => RTEM_SEL_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a28);

UUMS2_ai_a5590_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5590 = UUMS2_areduce_nor_71 & UUMS2_ai_a5575 & !UUMS2_astep_clk & UUMS2_ai_a5583

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0800",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_71,
	datab => UUMS2_ai_a5575,
	datac => UUMS2_astep_clk,
	datad => UUMS2_ai_a5583,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5590);

UUMS2_aadd_155_rtl_15_a0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a0 = !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a
-- UUMS2_aadd_155_rtl_15_a0COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "33CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a0,
	cout => UUMS2_aadd_155_rtl_15_a0COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a127_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a127 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a0 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a0,
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a,
	datac => UUMS2_ai_a5590,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a127);

UUMS2_ai_a3075_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a3075 = UUMS2_ai_a5575 # UUMS2_astep_clk # !UUMS2_ai_a5583 # !UUMS2_areduce_nor_71

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FDFF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_areduce_nor_71,
	datab => UUMS2_ai_a5575,
	datac => UUMS2_astep_clk,
	datad => UUMS2_ai_a5583,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a3075);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a127) # (!UUMS2_ai_a3075 & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "qfbk_counter",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a127,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT);

UUMS2_aadd_155_rtl_15_a1_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a1 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a $ !UUMS2_aadd_155_rtl_15_a0COUT
-- UUMS2_aadd_155_rtl_15_a1COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a & !UUMS2_aadd_155_rtl_15_a0COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a,
	cin => UUMS2_aadd_155_rtl_15_a0COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a1,
	cout => UUMS2_aadd_155_rtl_15_a1COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a124_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a124 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a1 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a,
	datab => UUMS2_aadd_155_rtl_15_a1,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a124);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a124) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a124,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a0_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT);

UUMS2_aadd_155_rtl_15_a2_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a2 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a $ UUMS2_aadd_155_rtl_15_a1COUT
-- UUMS2_aadd_155_rtl_15_a2COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a # !UUMS2_aadd_155_rtl_15_a1COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a,
	cin => UUMS2_aadd_155_rtl_15_a1COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a2,
	cout => UUMS2_aadd_155_rtl_15_a2COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a121_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a121 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a2 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "30AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aadd_155_rtl_15_a2,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a121);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a121) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a121,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a1_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT);

UUMS2_aadd_155_rtl_15_a3_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a3 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a $ !UUMS2_aadd_155_rtl_15_a2COUT
-- UUMS2_aadd_155_rtl_15_a3COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a & !UUMS2_aadd_155_rtl_15_a2COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a,
	cin => UUMS2_aadd_155_rtl_15_a2COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a3,
	cout => UUMS2_aadd_155_rtl_15_a3COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a118_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a118 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a3 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a3,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a118);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a118) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a118,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a2_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT);

UUMS2_aadd_155_rtl_15_a4_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a4 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a $ UUMS2_aadd_155_rtl_15_a3COUT
-- UUMS2_aadd_155_rtl_15_a4COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a # !UUMS2_aadd_155_rtl_15_a3COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a,
	cin => UUMS2_aadd_155_rtl_15_a3COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a4,
	cout => UUMS2_aadd_155_rtl_15_a4COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a115_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a115 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a4 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a4,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a115);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a115) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a115,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a3_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT);

UUMS2_aadd_155_rtl_15_a5_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a5 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a $ !UUMS2_aadd_155_rtl_15_a4COUT
-- UUMS2_aadd_155_rtl_15_a5COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a & !UUMS2_aadd_155_rtl_15_a4COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a,
	cin => UUMS2_aadd_155_rtl_15_a4COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a5,
	cout => UUMS2_aadd_155_rtl_15_a5COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a112_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a112 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a5 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3A0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_ai_a5590,
	datad => UUMS2_aadd_155_rtl_15_a5,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a112);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a112) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a112,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a4_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT);

UUMS2_aadd_155_rtl_15_a6_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a6 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a $ UUMS2_aadd_155_rtl_15_a5COUT
-- UUMS2_aadd_155_rtl_15_a6COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a # !UUMS2_aadd_155_rtl_15_a5COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a,
	cin => UUMS2_aadd_155_rtl_15_a5COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a6,
	cout => UUMS2_aadd_155_rtl_15_a6COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a109_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a109 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a6 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a6,
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a,
	datac => UUMS2_ai_a5590,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a109);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a109) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a109,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a5_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT);

UUMS2_aadd_155_rtl_15_a7_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a7 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a $ !UUMS2_aadd_155_rtl_15_a6COUT
-- UUMS2_aadd_155_rtl_15_a7COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a & !UUMS2_aadd_155_rtl_15_a6COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a,
	cin => UUMS2_aadd_155_rtl_15_a6COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a7,
	cout => UUMS2_aadd_155_rtl_15_a7COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a106_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a106 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a7 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a7,
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a,
	datac => UUMS2_ai_a5590,
	datad => UUMS2_aSW_OUT_DB,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a106);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a106) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a106,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a6_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT);

UUMS2_aadd_155_rtl_15_a8_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a8 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a $ UUMS2_aadd_155_rtl_15_a7COUT
-- UUMS2_aadd_155_rtl_15_a8COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a # !UUMS2_aadd_155_rtl_15_a7COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a,
	cin => UUMS2_aadd_155_rtl_15_a7COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a8,
	cout => UUMS2_aadd_155_rtl_15_a8COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a103_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a103 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a8 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "30AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aadd_155_rtl_15_a8,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a103);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a103) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a103,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a7_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT);

UUMS2_aadd_155_rtl_15_a9_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a9 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a $ !UUMS2_aadd_155_rtl_15_a8COUT
-- UUMS2_aadd_155_rtl_15_a9COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a & !UUMS2_aadd_155_rtl_15_a8COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a,
	cin => UUMS2_aadd_155_rtl_15_a8COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a9,
	cout => UUMS2_aadd_155_rtl_15_a9COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a100_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a100 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a9 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "30AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aadd_155_rtl_15_a9,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a100);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a100) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a100,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a8_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT);

UUMS2_aadd_155_rtl_15_a10_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a10 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a $ UUMS2_aadd_155_rtl_15_a9COUT
-- UUMS2_aadd_155_rtl_15_a10COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a # !UUMS2_aadd_155_rtl_15_a9COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a,
	cin => UUMS2_aadd_155_rtl_15_a9COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a10,
	cout => UUMS2_aadd_155_rtl_15_a10COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a97_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a97 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a10 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a10,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a97);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a97) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a97,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a9_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT);

UUMS2_aadd_155_rtl_15_a11_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a11 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a $ !UUMS2_aadd_155_rtl_15_a10COUT
-- UUMS2_aadd_155_rtl_15_a11COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a & !UUMS2_aadd_155_rtl_15_a10COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a,
	cin => UUMS2_aadd_155_rtl_15_a10COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a11,
	cout => UUMS2_aadd_155_rtl_15_a11COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a94_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a94 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a11 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a11,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a94);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a94) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a94,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a10_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT);

UUMS2_aadd_155_rtl_15_a12_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a12 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a $ UUMS2_aadd_155_rtl_15_a11COUT
-- UUMS2_aadd_155_rtl_15_a12COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a # !UUMS2_aadd_155_rtl_15_a11COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a,
	cin => UUMS2_aadd_155_rtl_15_a11COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a12,
	cout => UUMS2_aadd_155_rtl_15_a12COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a91_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a91 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a12 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a12,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a91);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a91) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a91,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a11_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT);

UUMS2_aadd_155_rtl_15_a13_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a13 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a $ !UUMS2_aadd_155_rtl_15_a12COUT
-- UUMS2_aadd_155_rtl_15_a13COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a & !UUMS2_aadd_155_rtl_15_a12COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C303",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a,
	cin => UUMS2_aadd_155_rtl_15_a12COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a13,
	cout => UUMS2_aadd_155_rtl_15_a13COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a88_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a88 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a13 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a13,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a88);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a88) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a88,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a12_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT);

UUMS2_aadd_155_rtl_15_a14_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a14 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a $ UUMS2_aadd_155_rtl_15_a13COUT
-- UUMS2_aadd_155_rtl_15_a14COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a # !UUMS2_aadd_155_rtl_15_a13COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3CCF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a,
	cin => UUMS2_aadd_155_rtl_15_a13COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a14,
	cout => UUMS2_aadd_155_rtl_15_a14COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a85_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a85 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a14 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a14,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a85);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a85) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a85,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a13_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT);

UUMS2_aadd_155_rtl_15_a15_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a15 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a $ !UUMS2_aadd_155_rtl_15_a14COUT
-- UUMS2_aadd_155_rtl_15_a15COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a & !UUMS2_aadd_155_rtl_15_a14COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a,
	cin => UUMS2_aadd_155_rtl_15_a14COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a15,
	cout => UUMS2_aadd_155_rtl_15_a15COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a82_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a82 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a15 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a15,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a82);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a82) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a82,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a14_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT);

UUMS2_aadd_155_rtl_15_a16_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a16 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a $ UUMS2_aadd_155_rtl_15_a15COUT
-- UUMS2_aadd_155_rtl_15_a16COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a # !UUMS2_aadd_155_rtl_15_a15COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a,
	cin => UUMS2_aadd_155_rtl_15_a15COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a16,
	cout => UUMS2_aadd_155_rtl_15_a16COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a79_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a79 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a16 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a,
	datab => UUMS2_aadd_155_rtl_15_a16,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a79);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a79) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a79,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a15_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT);

UUMS2_aadd_155_rtl_15_a17_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a17 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a $ !UUMS2_aadd_155_rtl_15_a16COUT
-- UUMS2_aadd_155_rtl_15_a17COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a & !UUMS2_aadd_155_rtl_15_a16COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a,
	cin => UUMS2_aadd_155_rtl_15_a16COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a17,
	cout => UUMS2_aadd_155_rtl_15_a17COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a76_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a76 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a17 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a,
	datab => UUMS2_aadd_155_rtl_15_a17,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a76);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a76) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a76,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a16_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT);

UUMS2_aadd_155_rtl_15_a18_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a18 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a $ UUMS2_aadd_155_rtl_15_a17COUT
-- UUMS2_aadd_155_rtl_15_a18COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a # !UUMS2_aadd_155_rtl_15_a17COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a,
	cin => UUMS2_aadd_155_rtl_15_a17COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a18,
	cout => UUMS2_aadd_155_rtl_15_a18COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a73_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a73 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a18 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a,
	datab => UUMS2_aadd_155_rtl_15_a18,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a73);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a73) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a73,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a17_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT);

UUMS2_aadd_155_rtl_15_a19_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a19 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a $ !UUMS2_aadd_155_rtl_15_a18COUT
-- UUMS2_aadd_155_rtl_15_a19COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a & !UUMS2_aadd_155_rtl_15_a18COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a,
	cin => UUMS2_aadd_155_rtl_15_a18COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a19,
	cout => UUMS2_aadd_155_rtl_15_a19COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a70_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a70 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a19 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a,
	datab => UUMS2_aadd_155_rtl_15_a19,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a70);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a70) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a70,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a18_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT);

UUMS2_aadd_155_rtl_15_a20_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a20 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a $ UUMS2_aadd_155_rtl_15_a19COUT
-- UUMS2_aadd_155_rtl_15_a20COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a # !UUMS2_aadd_155_rtl_15_a19COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a,
	cin => UUMS2_aadd_155_rtl_15_a19COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a20,
	cout => UUMS2_aadd_155_rtl_15_a20COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a67_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a67 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a20 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a20,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a67);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a67) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a67,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a19_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT);

UUMS2_aadd_155_rtl_15_a21_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a21 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a $ !UUMS2_aadd_155_rtl_15_a20COUT
-- UUMS2_aadd_155_rtl_15_a21COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a & !UUMS2_aadd_155_rtl_15_a20COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a,
	cin => UUMS2_aadd_155_rtl_15_a20COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a21,
	cout => UUMS2_aadd_155_rtl_15_a21COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a64_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a64 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a21 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a21,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a64);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a64) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a64,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a20_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT);

UUMS2_aadd_155_rtl_15_a22_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a22 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a $ UUMS2_aadd_155_rtl_15_a21COUT
-- UUMS2_aadd_155_rtl_15_a22COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a # !UUMS2_aadd_155_rtl_15_a21COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a,
	cin => UUMS2_aadd_155_rtl_15_a21COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a22,
	cout => UUMS2_aadd_155_rtl_15_a22COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a61_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a61 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a22 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0CAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a,
	datab => UUMS2_aadd_155_rtl_15_a22,
	datac => UUMS2_aSW_OUT_DB,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a61);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a61) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a61,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a21_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT);

UUMS2_aadd_155_rtl_15_a23_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a23 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a $ !UUMS2_aadd_155_rtl_15_a22COUT
-- UUMS2_aadd_155_rtl_15_a23COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a & !UUMS2_aadd_155_rtl_15_a22COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a,
	cin => UUMS2_aadd_155_rtl_15_a22COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a23,
	cout => UUMS2_aadd_155_rtl_15_a23COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a58_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a58 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a23 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a23,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a58);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a58) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5A5F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a58,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a22_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT);

UUMS2_aadd_155_rtl_15_a24_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a24 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a $ UUMS2_aadd_155_rtl_15_a23COUT
-- UUMS2_aadd_155_rtl_15_a24COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a # !UUMS2_aadd_155_rtl_15_a23COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a,
	cin => UUMS2_aadd_155_rtl_15_a23COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a24,
	cout => UUMS2_aadd_155_rtl_15_a24COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a55_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a55 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a24 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a24,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a55);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a55) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a55,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a23_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT);

UUMS2_aadd_155_rtl_15_a25_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a25 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a $ !UUMS2_aadd_155_rtl_15_a24COUT
-- UUMS2_aadd_155_rtl_15_a25COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a & !UUMS2_aadd_155_rtl_15_a24COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a,
	cin => UUMS2_aadd_155_rtl_15_a24COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a25,
	cout => UUMS2_aadd_155_rtl_15_a25COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a52_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a52 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a25 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a25,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a52);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a52) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a52,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a24_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT);

UUMS2_aadd_155_rtl_15_a26_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a26 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a $ UUMS2_aadd_155_rtl_15_a25COUT
-- UUMS2_aadd_155_rtl_15_a26COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a # !UUMS2_aadd_155_rtl_15_a25COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a,
	cin => UUMS2_aadd_155_rtl_15_a25COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a26,
	cout => UUMS2_aadd_155_rtl_15_a26COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a49_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a49 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a26 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a26,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a49);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a49) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a49,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a25_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT);

UUMS2_aadd_155_rtl_15_a27_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a27 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a $ !UUMS2_aadd_155_rtl_15_a26COUT
-- UUMS2_aadd_155_rtl_15_a27COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a & !UUMS2_aadd_155_rtl_15_a26COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a,
	cin => UUMS2_aadd_155_rtl_15_a26COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a27,
	cout => UUMS2_aadd_155_rtl_15_a27COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a46_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a46 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a27 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a27,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a46);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a46) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a46,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a26_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT);

UUMS2_aadd_155_rtl_15_a28_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a28 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a $ UUMS2_aadd_155_rtl_15_a27COUT
-- UUMS2_aadd_155_rtl_15_a28COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a # !UUMS2_aadd_155_rtl_15_a27COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a,
	cin => UUMS2_aadd_155_rtl_15_a27COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a28,
	cout => UUMS2_aadd_155_rtl_15_a28COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a43_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a43 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a28 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a28,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a43);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a43) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a43,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a27_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT);

UUMS2_aadd_155_rtl_15_a29_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a29 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a $ !UUMS2_aadd_155_rtl_15_a28COUT
-- UUMS2_aadd_155_rtl_15_a29COUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a & !UUMS2_aadd_155_rtl_15_a28COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A505",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a,
	cin => UUMS2_aadd_155_rtl_15_a28COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a29,
	cout => UUMS2_aadd_155_rtl_15_a29COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a40_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a40 = UUMS2_ai_a5590 & UUMS2_aadd_155_rtl_15_a29 & !UUMS2_aSW_OUT_DB # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "22F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aadd_155_rtl_15_a29,
	datab => UUMS2_aSW_OUT_DB,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a40);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a40) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT = CARRY(!UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT # !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a40,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a28_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT);

UUMS2_aadd_155_rtl_15_a30_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a30 = UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a $ UUMS2_aadd_155_rtl_15_a29COUT
-- UUMS2_aadd_155_rtl_15_a30COUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a # !UUMS2_aadd_155_rtl_15_a29COUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AAF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a,
	cin => UUMS2_aadd_155_rtl_15_a29COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a30,
	cout => UUMS2_aadd_155_rtl_15_a30COUT);

UUMS2_aadd_155_rtl_15_rtl_169_a37_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a37 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a30 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "44F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aadd_155_rtl_15_a30,
	datac => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a37);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a37) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a_aCOUT = CARRY(UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a & !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "counter",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A50A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a,
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a37,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a29_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a,
	cout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a_aCOUT);

UUMS2_aadd_155_rtl_15_a31_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_a31 = UUMS2_aadd_155_rtl_15_a30COUT $ !UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F00F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a,
	cin => UUMS2_aadd_155_rtl_15_a30COUT,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_a31);

UUMS2_aadd_155_rtl_15_rtl_169_a34_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aadd_155_rtl_15_rtl_169_a34 = UUMS2_ai_a5590 & !UUMS2_aSW_OUT_DB & UUMS2_aadd_155_rtl_15_a31 # !UUMS2_ai_a5590 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "50CC",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aSW_OUT_DB,
	datab => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a,
	datac => UUMS2_aadd_155_rtl_15_a31,
	datad => UUMS2_ai_a5590,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aadd_155_rtl_15_rtl_169_a34);

UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a31_a : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a = DFFE((UUMS2_ai_a3075 & UUMS2_aadd_155_rtl_15_rtl_169_a34) # (!UUMS2_ai_a3075 & UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a_aCOUT $ UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => UUMS2_aadd_155_rtl_15_rtl_169_a34,
	datad => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a,
	cin => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_acounter_cell_a30_a_aCOUT,
	clk => clk20_acombout,
	aclr => imr_acombout,
	sload => UUMS2_ai_a3075,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a);

UUMS2_ai_a5581_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5581 = !UUMS2_aUMS_STATE_a22 & !UUMS2_aUMS_STATE_a19 & !UUMS2_aHI_Cnt_En

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aUMS_STATE_a22,
	datac => UUMS2_aUMS_STATE_a19,
	datad => UUMS2_aHI_Cnt_En,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5581);

UUMS2_aST_RTEM_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aST_RTEM_a3_a_areg0 = DFFE(UUMS2_aUMS_STATE_a17 # UUMS2_aUMS_STATE_a21 # !UUMS2_ai_a5581 # !UUMS2_areduce_nor_71_a25, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFFF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a17,
	datab => UUMS2_aUMS_STATE_a21,
	datac => UUMS2_areduce_nor_71_a25,
	datad => UUMS2_ai_a5581,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aST_RTEM_a3_a_areg0);

URTEM_ai_a515_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a515 = URTEM_aU_RT_OUT_aiAC # URTEM_aU_RT_OUT_artin_reg & URTEM_aU_RT_IN_artin_reg # !URTEM_aU_RT_OUT_artin_reg & !URTEM_aU_RT_IN_artin_reg & !URTEM_aST_RTEM_a3_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF89",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_reg,
	datab => URTEM_aU_RT_IN_artin_reg,
	datac => URTEM_aST_RTEM_a3_a_areg0,
	datad => URTEM_aU_RT_OUT_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a515);

URTEM_ai_a1097_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1097 = URTEM_aU_RT_OUT_artin_reg & URTEM_aU_RT_IN_aiAC & !URTEM_aST_RTEM_a3_a_areg0 & !URTEM_aU_RT_OUT_aiAC

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0008",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_reg,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aST_RTEM_a3_a_areg0,
	datad => URTEM_aU_RT_OUT_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a1097);

URTEM_aST_RTEM_a3_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aST_RTEM_a3_a_areg0 = DFFE(!URTEM_aHi_Cnt_State & !URTEM_ai_a1097 & (URTEM_aU_RT_IN_aiAC # !URTEM_ai_a515), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000D",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_ai_a515,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aHi_Cnt_State,
	datad => URTEM_ai_a1097,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aST_RTEM_a3_a_areg0);

i_a31_I : apex20ke_lcell 
-- Equation(s):
-- i_a31 = UUMS2_aST_RTEM_a3_a_areg0 & (RTEM_SEL_acombout # !URTEM_aST_RTEM_a3_a_areg0) # !UUMS2_aST_RTEM_a3_a_areg0 & !URTEM_aST_RTEM_a3_a_areg0 & !RTEM_SEL_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "AA0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aST_RTEM_a3_a_areg0,
	datac => URTEM_aST_RTEM_a3_a_areg0,
	datad => RTEM_SEL_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a31);

UUMS2_aUMS_STATE_a508_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aUMS_STATE_a508 = !UUMS2_aUMS_STATE_a13 & !UUMS2_aUMS_STATE_a21 & !UUMS2_aUMS_STATE_a12

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a13,
	datac => UUMS2_aUMS_STATE_a21,
	datad => UUMS2_aUMS_STATE_a12,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_aUMS_STATE_a508);

UUMS2_aST_RTEM_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aST_RTEM_a2_a_areg0 = DFFE(UUMS2_aHI_Cnt_En # UUMS2_aUMS_STATE_a22 # UUMS2_aUMS_STATE_a15 # !UUMS2_aUMS_STATE_a508, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFD",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a508,
	datab => UUMS2_aHI_Cnt_En,
	datac => UUMS2_aUMS_STATE_a22,
	datad => UUMS2_aUMS_STATE_a15,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aST_RTEM_a2_a_areg0);

URTEM_aMux_31_rtl_94_a17_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aMux_31_rtl_94_a17 = !URTEM_aU_RT_OUT_artin_reg & !URTEM_aU_RT_IN_aiAC & !URTEM_aU_RT_OUT_aiAC & !URTEM_aST_RTEM_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_reg,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_aST_RTEM_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aMux_31_rtl_94_a17);

URTEM_ai_a1196_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1196 = (!URTEM_aU_RT_OUT_aiAC & (!URTEM_aU_RT_OUT_artin_reg # !URTEM_aST_RTEM_a2_a_areg0) # !URTEM_aU_RT_IN_aiAC) & CASCADE(URTEM_aU_RT_IN_ai_a492)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "575F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_IN_aiAC,
	datab => URTEM_aST_RTEM_a2_a_areg0,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_aU_RT_OUT_artin_reg,
	cascin => URTEM_aU_RT_IN_ai_a492,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a1196);

URTEM_aST_RTEM_a2_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aST_RTEM_a2_a_areg0 = DFFE(!URTEM_aHi_Cnt_State & !URTEM_ai_a1196 & (URTEM_aU_RT_OUT_aiAC # !URTEM_aMux_31_rtl_94_a17), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0031",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aMux_31_rtl_94_a17,
	datab => URTEM_aHi_Cnt_State,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_ai_a1196,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aST_RTEM_a2_a_areg0);

i_a34_I : apex20ke_lcell 
-- Equation(s):
-- i_a34 = RTEM_SEL_acombout & UUMS2_aST_RTEM_a2_a_areg0 # !RTEM_SEL_acombout & !URTEM_aST_RTEM_a2_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8D8D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => RTEM_SEL_acombout,
	datab => UUMS2_aST_RTEM_a2_a_areg0,
	datac => URTEM_aST_RTEM_a2_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a34);

URTEM_aMux_32_rtl_99_a17_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aMux_32_rtl_99_a17 = !URTEM_aU_RT_IN_aiAC & URTEM_aU_RT_OUT_aiAC & URTEM_aU_RT_IN_artin_reg

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3000",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_aU_RT_IN_artin_reg,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_aMux_32_rtl_99_a17);

URTEM_ai_a1138_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a1138 = URTEM_aU_RT_IN_aiAC & (URTEM_aST_RTEM_a1_a_areg0 # !URTEM_aU_RT_OUT_artin_reg) # !URTEM_aU_RT_IN_aiAC & !URTEM_aU_RT_IN_artin_reg & (URTEM_aST_RTEM_a1_a_areg0 # URTEM_aU_RT_OUT_artin_reg)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "8CBE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aST_RTEM_a1_a_areg0,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aU_RT_OUT_artin_reg,
	datad => URTEM_aU_RT_IN_artin_reg,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a1138);

URTEM_aST_RTEM_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aST_RTEM_a1_a_areg0 = DFFE(URTEM_aMux_32_rtl_99_a17 # URTEM_aHi_Cnt_State # !URTEM_aU_RT_OUT_aiAC & URTEM_ai_a1138, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "EFEE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aMux_32_rtl_99_a17,
	datab => URTEM_aHi_Cnt_State,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_ai_a1138,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aST_RTEM_a1_a_areg0);

UUMS2_ai_a5640_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5640 = UUMS2_aUMS_STATE_a19 # UUMS2_aHI_Cnt_En # UUMS2_aUMS_STATE_a15 # UUMS2_aUMS_STATE_a22

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a19,
	datab => UUMS2_aHI_Cnt_En,
	datac => UUMS2_aUMS_STATE_a15,
	datad => UUMS2_aUMS_STATE_a22,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5640);

UUMS2_aST_RTEM_a1_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aST_RTEM_a1_a_areg0 = DFFE(UUMS2_ai_a5640 # UUMS2_aUMS_STATE_a10 # UUMS2_aUMS_STATE_a18 # UUMS2_aUMS_STATE_a11, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_ai_a5640,
	datab => UUMS2_aUMS_STATE_a10,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_aUMS_STATE_a11,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aST_RTEM_a1_a_areg0);

i_a37_I : apex20ke_lcell 
-- Equation(s):
-- i_a37 = RTEM_SEL_acombout & UUMS2_aST_RTEM_a1_a_areg0 # !RTEM_SEL_acombout & URTEM_aST_RTEM_a1_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FC30",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => RTEM_SEL_acombout,
	datac => URTEM_aST_RTEM_a1_a_areg0,
	datad => UUMS2_aST_RTEM_a1_a_areg0,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a37);

UUMS2_ai_a5652_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_ai_a5652 = UUMS2_aUMS_STATE_a20 # UUMS2_aHI_Cnt_En # UUMS2_aUMS_STATE_a18 # UUMS2_aUMS_STATE_a16

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFFE",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a20,
	datab => UUMS2_aHI_Cnt_En,
	datac => UUMS2_aUMS_STATE_a18,
	datad => UUMS2_aUMS_STATE_a16,
	devclrn => devclrn,
	devpor => devpor,
	combout => UUMS2_ai_a5652);

UUMS2_aST_RTEM_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- UUMS2_aST_RTEM_a0_a_areg0 = DFFE(!UUMS2_aUMS_STATE_a12 & UUMS2_aUMS_STATE_a8 & !UUMS2_aUMS_STATE_a10 & !UUMS2_ai_a5652, GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0004",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => UUMS2_aUMS_STATE_a12,
	datab => UUMS2_aUMS_STATE_a8,
	datac => UUMS2_aUMS_STATE_a10,
	datad => UUMS2_ai_a5652,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => UUMS2_aST_RTEM_a0_a_areg0);

URTEM_ai_a761_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a761 = URTEM_aU_RT_OUT_artin_reg & (!URTEM_aU_RT_IN_artin_reg & !URTEM_aST_RTEM_a0_a_areg0 # !URTEM_aU_RT_IN_aiAC) # !URTEM_aU_RT_OUT_artin_reg & !URTEM_aU_RT_IN_artin_reg & !URTEM_aST_RTEM_a0_a_areg0

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "03AB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_reg,
	datab => URTEM_aU_RT_IN_artin_reg,
	datac => URTEM_aST_RTEM_a0_a_areg0,
	datad => URTEM_aU_RT_IN_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a761);

URTEM_ai_a767_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_ai_a767 = URTEM_aU_RT_IN_aiAC & (URTEM_aU_RT_OUT_aiAC # !URTEM_aST_RTEM_a0_a_areg0 # !URTEM_aU_RT_OUT_artin_reg)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC4C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_aU_RT_OUT_artin_reg,
	datab => URTEM_aU_RT_IN_aiAC,
	datac => URTEM_aST_RTEM_a0_a_areg0,
	datad => URTEM_aU_RT_OUT_aiAC,
	devclrn => devclrn,
	devpor => devpor,
	combout => URTEM_ai_a767);

URTEM_aST_RTEM_a0_a_areg0_I : apex20ke_lcell 
-- Equation(s):
-- URTEM_aST_RTEM_a0_a_areg0 = DFFE(URTEM_aHi_Cnt_State # !URTEM_ai_a767 & (URTEM_aU_RT_OUT_aiAC # !URTEM_ai_a761), GLOBAL(clk20_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF31",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => URTEM_ai_a761,
	datab => URTEM_ai_a767,
	datac => URTEM_aU_RT_OUT_aiAC,
	datad => URTEM_aHi_Cnt_State,
	clk => clk20_acombout,
	aclr => imr_acombout,
	devclrn => devclrn,
	devpor => devpor,
	regout => URTEM_aST_RTEM_a0_a_areg0);

i_a40_I : apex20ke_lcell 
-- Equation(s):
-- i_a40 = UUMS2_aST_RTEM_a0_a_areg0 & (RTEM_SEL_acombout # !URTEM_aST_RTEM_a0_a_areg0) # !UUMS2_aST_RTEM_a0_a_areg0 & !URTEM_aST_RTEM_a0_a_areg0 & !RTEM_SEL_acombout

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "CC0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => UUMS2_aST_RTEM_a0_a_areg0,
	datac => URTEM_aST_RTEM_a0_a_areg0,
	datad => RTEM_SEL_acombout,
	devclrn => devclrn,
	devpor => devpor,
	combout => i_a40);

RTEM_ANALYZE_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a25,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_ANALYZE);

RTEM_RETRACT_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a28,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_RETRACT);

RTEM_CNT_a31_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a31_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(31));

RTEM_CNT_a30_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a30_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(30));

RTEM_CNT_a29_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a29_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(29));

RTEM_CNT_a28_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a28_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(28));

RTEM_CNT_a27_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a27_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(27));

RTEM_CNT_a26_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a26_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(26));

RTEM_CNT_a25_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a25_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(25));

RTEM_CNT_a24_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a24_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(24));

RTEM_CNT_a23_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a23_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(23));

RTEM_CNT_a22_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a22_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(22));

RTEM_CNT_a21_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a21_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(21));

RTEM_CNT_a20_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a20_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(20));

RTEM_CNT_a19_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a19_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(19));

RTEM_CNT_a18_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a18_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(18));

RTEM_CNT_a17_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a17_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(17));

RTEM_CNT_a16_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a16_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(16));

RTEM_CNT_a15_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a15_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(15));

RTEM_CNT_a14_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a14_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(14));

RTEM_CNT_a13_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a13_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(13));

RTEM_CNT_a12_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a12_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(12));

RTEM_CNT_a11_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a11_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(11));

RTEM_CNT_a10_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a10_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(10));

RTEM_CNT_a9_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a9_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(9));

RTEM_CNT_a8_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a8_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(8));

RTEM_CNT_a7_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a7_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(7));

RTEM_CNT_a6_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a6_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(6));

RTEM_CNT_a5_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a5_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(5));

RTEM_CNT_a4_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a4_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(4));

RTEM_CNT_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a3_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(3));

RTEM_CNT_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a2_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(2));

RTEM_CNT_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a1_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(1));

RTEM_CNT_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => UUMS2_aiUMS_CNT_rtl_29_awysi_counter_aq_a0_a,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_RTEM_CNT(0));

ST_RTEM_a3_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a31,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ST_RTEM(3));

ST_RTEM_a2_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a34,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ST_RTEM(2));

ST_RTEM_a1_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a37,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ST_RTEM(1));

ST_RTEM_a0_a_aI : apex20ke_io 
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => i_a40,
	devclrn => devclrn,
	devpor => devpor,
	devoe => devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_ST_RTEM(0));
END structure;


