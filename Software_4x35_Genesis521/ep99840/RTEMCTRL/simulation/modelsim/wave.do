onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic -radix binary /tb/imr
add wave -noupdate -format Logic -radix binary /tb/clk20
add wave -noupdate -format Logic -radix binary /tb/RTEM_SEL
add wave -noupdate -format Literal -radix hexadecimal /tb/rtem_ini
add wave -noupdate -format Logic -radix binary /tb/Hi_Cnt
add wave -noupdate -format Logic -radix binary /tb/Det_Sat
add wave -noupdate -format Logic -radix binary /tb/CMD_IN
add wave -noupdate -format Logic -radix binary /tb/CMD_OUT
add wave -noupdate -format Logic -radix binary /tb/HC_MR
add wave -noupdate -format Logic -radix binary /tb/RTEM_RED
add wave -noupdate -format Logic -radix binary /tb/RTEM_GRN
add wave -noupdate -format Logic -radix binary /tb/RTEM_ANALYZE
add wave -noupdate -format Logic -radix binary /tb/RTEM_RETRACT
add wave -noupdate -format Literal -radix hexadecimal /tb/ST_RTEM
add wave -noupdate -format Literal -radix hexadecimal /tb/RTEM_CNT
add wave -noupdate /tb/RTEM_STATE

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {20962 ns}
WaveRestoreZoom {17520 ns} {25977 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
