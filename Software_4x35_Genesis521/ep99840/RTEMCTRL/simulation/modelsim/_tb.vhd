-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	rtemctrl.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  Digital Interface for the UMS Controller
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		November 2, 2000 - MCS
--			Changed Version number ot 0x8332 to correspond to the Rev 2 Copper
--			relase version.
--		September 21, 2000 - MCS
--			Updated for the SEM requirement of the UMS
--			DMC_IN bus has been redefined as:
--			DMC_IN[0] = Interrupt ( Active Low )
--			DMC_IN[7:1] = Encoded Value where:
--				00 = Analyze
--				7F = Retract
--				01-7D moves to some intermediate point.
--
--		TODO
--			with Rev 2 Copper, add the Additional DIP switch inputs to support the 
--			3rd button.
--		March 24, 2000 - MCS
--			Developemnt completed for UMS and RTEM for both manual and computer control
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
	
entity tb is
end  tb;

architecture test_bench of tb is
	Constant SW_Open 	: std_logic := '1';
	Constant Sw_Close 	: std_logic := '0';
	-- String is : ID_FPGA & ID_ID & ID_REV
	constant STR_RTEM_ERROR	: std_logic_Vector( 7 downto 0 ) := x"00";
     constant STR_RTEM_OUT    : std_logic_vector( 7 downto 0 ) := x"01";
     constant STR_RTEM_IN     : std_logic_vector( 7 downto 0 ) := x"02";
     constant STR_RTEM_MV_IN  : std_logic_vector( 7 downto 0 ) := x"04";
     constant STR_RTEM_MV_OUT : std_logic_vector( 7 downto 0 ) := x"08";
     constant sTR_RTEM_HC     : std_logic_vector( 7 downto 0 ) := x"10";
     constant sTR_RTEM_NO_POW : std_logic_vector( 7 downto 0 ) := x"20";
	constant STR_RTEM_SEM	: std_logic_Vector( 7 downto 0 ) := x"40";
	constant STR_RTEM_MV_SEM	: std_logic_Vector( 7 downto 0 ) := x"80";
	constant STR_RTEM_INIT	: std_logic_Vector( 7 downto 0 ) := x"21";

	-- Any Type Declarations
	type RTEM_TYPE is (
		ST_ERROR,
		ST_OUT,
		ST_IN,
		ST_MV_IN,
		ST_MV_OUT,
		ST_HC,
		ST_NO_PWR,
		ST_SEM,
		ST_MV_SEM,
		ST_RTEM_INIT,
		ST_UND	);
		
	signal RTEM_STATE 	: RTEM_TYPE;

	signal imr       	: std_logic := '1';				     	-- Master Reset
	signal clk20       	: std_logic := '0';                         	-- Master Clock
	signal RTEM_SEL	: std_logic;
	signal rtem_ini  	: std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
	signal Hi_Cnt    	: std_logic;						-- High Count Rate Retract
	signal Det_Sat		: std_logic;
	signal CMD_IN		: std_Logic;						-- COmmand to Move In
	signal CMD_OUT		: std_logic;						-- Command to Move Out
	signal HC_MR		: std_logic;						-- High Count Master Reset
	signal RTEM_RED	: std_logic;						-- Input Status for In/Moving in
	signal RTEM_GRN	: std_logic;						-- Input Status for Out/Moving Out
	signal RTEM_ANALYZE	: std_logic;						-- Output to Move RTEM In
	signal RTEM_RETRACT	: std_logic;						-- Output to Move RTEM Out
	signal ST_RTEM   	: std_logic_vector(  7 downto 0 );		-- RTEM Status
	signal ms100_tc	: std_logic;
	signal WD_EN		: std_logic;
	signal WD_MR		: std_logic;
	signal Low_Speed  	: std_logic_Vector( 15 downto 0 );		-- Low Speed Value
	signal Med_Speed  	: std_logic_Vector( 15 downto 0 );		-- High Speed Value
	signal Hi_Speed  	: std_logic_Vector( 15 downto 0 );		-- High Speed Value
	signal CNT_10		: std_logic_Vector( 31 downto 0 );		-- 25% Cnt Value
	signal CNT_25		: std_logic_Vector( 31 downto 0 );		-- 25% Cnt Value
	signal CNT_75		: std_logic_Vector( 31 downto 0 );		-- 75% Cnt Value
	signal CNT_90		: std_logic_Vector( 31 downto 0 );		-- 75% Cnt Value
	signal CNT_110		: std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
	signal CNT_ANALYZE	: std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
	signal rtem_cnt	: std_logic_Vector( 31 downto 0 );

	-------------------------------------------------------------------------------
	component rtemctrl 
		port( 
			imr       	: in      std_logic;				     	-- Master Reset
			clk20       	: in      std_logic;                         	-- Master Clock
			RTEM_SEL		: in		std_logic;
			ms100_tc		: in		std_logic;
			WD_EN		: in		std_logic;
			WD_MR		: in		std_logic;
			rtem_ini  	: in		std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
			Low_Speed  	: in		std_logic_Vector( 15 downto 0 );		-- Low Speed Value
			Med_Speed  	: in		std_logic_Vector( 15 downto 0 );		-- High Speed Value
			Hi_Speed  	: in		std_logic_Vector( 15 downto 0 );		-- High Speed Value
			CNT_10		: in		std_logic_Vector( 31 downto 0 );		-- 25% Cnt Value
			CNT_25		: in		std_logic_Vector( 31 downto 0 );		-- 25% Cnt Value
			CNT_75		: in		std_logic_Vector( 31 downto 0 );		-- 75% Cnt Value
			CNT_90		: in		std_logic_Vector( 31 downto 0 );		-- 75% Cnt Value
			CNT_110		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			CNT_ANALYZE	: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
			Det_Sat		: in		std_logic;
			CMD_IN		: in		std_Logic;						-- COmmand to Move In
			CMD_OUT		: in		std_logic;						-- Command to Move Out
			HC_MR		: in		std_logic;						-- High Count Master Reset
			RTEM_RED		: in		std_logic;						-- Input Status for In/Moving in
			RTEM_GRN		: in		std_logic;						-- Input Status for Out/Moving Out
			RTEM_ANALYZE	: out	std_logic;						-- Output to Move RTEM In
			RTEM_RETRACT	: out	std_logic;						-- Output to Move RTEM Out
			RTEM_CNT		: out	std_logic_Vector( 31 downto 0 );
			ST_RTEM   	: out	std_logic_vector(  7 downto 0 ) );		-- RTEM Status
	end component rtemctrl;
	-------------------------------------------------------------------------------
begin

	-------------------------------------------------------------------------------
	
	U : rtemctrl 
		port map( 
			imr       	=> imr, 
			clk20       	=> clk20,
			RTEM_SEL		=> rtem_sel,
			ms100_tc		=> ms100_tc,
			WD_EN		=> wd_en,
			WD_MR		=> wd_mr,
			rtem_ini  	=> rtem_ini,
			Low_Speed  	=> low_speed,
			Med_Speed  	=> med_speed,
			Hi_Speed  	=> hi_speed,
			CNT_10		=> cnt_10,
			CNT_25		=> cnt_25,
			CNT_75		=> cnt_75,
			CNT_90		=> cnt_90,
			CNT_110		=> cnt_110,
			CNT_ANALYZE	=> cnt_analyze,
		    	Hi_Cnt    	=> hi_cnt,
			Det_Sat		=> det_sat,
			CMD_IN		=> cmd_in,
			CMD_OUT		=> cmd_out,
			HC_MR		=> hc_mr,
			RTEM_RED		=> rtem_red,
			RTEM_GRN		=> rtem_grn,
			RTEM_ANALYZE	=> rtem_analyze,
			RTEM_RETRACT	=> rtem_retract,
			RTEM_CNT		=> RTem_Cnt,
			ST_RTEM   	=> st_rtem );


	-------------------------------------------------------------------------------
	with ST_RTEM select
		RTEM_STATE <= 	ST_ERROR 		when STR_RTEM_ERROR,
					ST_OUT 		when STR_RTEM_OUT,
					ST_IN 		when STR_RTEM_IN,
					ST_MV_IN		when STR_RTEM_MV_IN,
					ST_MV_OUT		when STR_RTEM_MV_OUT,
					ST_HC		when sTR_RTEM_HC,
					ST_NO_PWR		when sTR_RTEM_NO_POW,
					ST_SEM		when STR_RTEM_SEM,
					ST_MV_SEM		when STR_RTEM_MV_SEM,
					ST_RTEM_INIT	when STR_RTEM_INIT,
					ST_UND		when others;


	IMR 		<= '0' after 800 ns;
	CLK20 	<= not( CLK20 ) after 25 ns;
	ms100_tc 	<= '0';
	wd_en	<= '0';
	wd_mr	<= '0';
	RTEM_SEL	<= '1';

	RTEM_INI 	<= x"FFFF";
	HI_CNT	<= '0';
	DET_SAT 	<= '0';


	Low_Speed  	<= conv_std_logic_vector( 65535 - 4, 16 );
	Med_Speed  	<= conv_std_logic_vector( 65535 - 2, 16 );
	Hi_Speed  	<= conv_std_logic_vector( 65535 - 1, 16 );
	CNT_10		<= x"7FFFFFFF";
	CNT_25		<= x"7FFFFFFF";
	CNT_75		<= x"7FFFFFFF";
	CNT_90		<= x"7FFFFFFF";
	CNT_110		<= x"7FFFFFFF";
	CNT_ANALYZE	<= x"7FFFFFFF";
	
	RTEM_PROC : process
	begin
		CMD_IN 	<= '0';
		CMD_OUT	<= '0';
		HC_MR	<= '0';
		
		-- RED is single limit switch for RTEM
		-- Red is analyze for TSL and UMS
		-- Green is Retract for TSL and UMS
		RTEM_RED	<= SW_Open;	-- Retract
		RTEM_GRN	<= SW_Close;	-- Analyze
		wait for 2 us;
		
		wait until(( RTEM_STATE'Event ) and ( RTEM_STATE /= ST_RTEM_INIT ));
		
		wait for 2 us;
		
		---------------------------------------------------------------
		assert( false )
			report "RTEM: Now move In"
			severity note;
		wait until(( CLK20'event ) and ( CLK20 = '0' ));
		CMD_IN	<= '1';
		wait until(( CLK20'event ) and ( CLK20 = '0' ));
		CMD_IN	<= '0';
		wait for 10 us;
		RTEM_RED	<= SW_Close;

		wait for 200 us;
		RTEM_GRN	<= SW_Open;
		
		wait for 2 us;

		---------------------------------------------------------------

--		assert( false )
--			report "End of Simulation"
--			severity failure;
		wait ;
	end process;
	
----------------------------------------------------------------------------------------------------
end test_bench;			-- rtemctrl.VHD
----------------------------------------------------------------------------------------------------

	
	
