-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--   File:     RTEMDB.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  This is the RTEMDB Input Interface for the I/O Board
--                  It controls and monitors the status of one of the RTEMDB
--                  Inputs and determine if On/Off or blinking
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		September 21, 2000 - MCS
--			Updated for the SEM requirement of the UMS
--			DMC_IN bus has been redefined as:
--			DMC_IN[0] = Interrupt ( Active Low )
--			DMC_IN[7:1] = Encoded Value where:
--				00 = Analyze
--				7F = Retract
--				7E = Fault
--				01-7D moves to some intermediate point.
--			March 20, 2000 - MCS
--				Master Reset changed from "OMR" to "IMR"
--			February 22, 2000 - MCS
--				Updated for UMS
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Flash State Diagram
library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity RTEMDB is port(
	Imr       	: in      std_logic;                         -- Master Reset
    	clk20       	: in      std_logic;                         -- Master Clock
    	rtin      	: in      std_logic;                         -- Led = On ( Active High )
    	mv_cmd    	: in      std_logic;                         -- Input Move Command ( From Serial Bus )
	RTEM_ini  	: in      std_logic_vector( 15 downto 0 );   -- Flast Count Initialization
	move      	: out  	std_logic;                         -- Output Move Command - to RTEMDB
    	ac        	: out  	std_logic;                         -- Flashing Output
    	dc        	: out  	std_logic );                        -- Steady On Output
end RTEMDB;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of RTEMDB is
	constant Mv_Cnt_Max		: integer :=  999999;		-- 50mSec @ 20Mhz
	constant Flash_lo_max	: integer := 256;
	
	signal Mv_Cnt_Tc		: std_logic;
	signal Mv_Cnt			: integer range 0 to Mv_Cnt_Max;
	
	signal Flash_lo_Cnt 	: integer range 0 to Flash_Lo_Max;
	signal Flash_Lo_Tc		: std_logic;

     -- Any Signal Declarations
     signal rtin_vec     	: std_logic_Vector( 7 downto 0 );
     signal mv_cmdb      	: std_logic;                            -- Move Command pipeline
	signal flash_cnt		: std_logic_vector( 15 downto 0 );
	signal flash_tc		: std_logic;
	signal rtin_reg		: std_logic;
	signal imove			: std_logic;
	signal RTIN_DB			: std_logic;
	signal RTIN_DB_DEL		: std_logic;
	signal iAC			: std_logic;

begin
	Flash_Lo_Tc 	<= '1' 			when ( Flash_lo_Cnt = Flash_lo_Max ) else '0';
	flash_tc		<= '1'			when (( flash_cnt = Rtem_Ini ) and ( Flash_lo_tc = '1' )) else '0';
	Mv_Cnt_Tc		<= '1'			when ( Mv_Cnt = Mv_cnt_Max ) else '0';
     dc   		<= not( rtin_reg )	when ( iac = '0' ) else '0';
	ac			<= iac;

     ---------------------------------------------------------------------
     reg_proc : process( clk20, IMR )
     begin
          if( IMR = '1' ) then
               rtin_vec          	<= x"00";
			Flash_Lo_cnt		<= 0;
			Flash_Cnt			<= x"0000";
               mv_cmdb        	<= '0';
               imove           	<= '0';
			move				<= '1';
               iac             	<= '0';
			rtin_reg			<= '0';
               mv_cnt         	<= 0;
			RTIN_DB			<= '0';
			RTIN_DB_DEL		<= '0';
          elsif( ( CLK20'Event ) and ( CLK20 = '1' )) then
			move		<= not( imove );
			rtin_vec	<= rtin_vec(6 downto 0) & rtin;
			
			if( RTIN_VEC = x"FF" )
				then RTIN_DB <= '1';
			elsif( RTIN_VEC = x"00" )
				then RTIN_DB <= '0';
			end if;
			
			RTIN_DB_DEL	<= RTIN_DB;

			-- Lower Bits of the Flash Count --------------
			if( Flash_Lo_Tc = '1' ) 
				then Flash_Lo_Cnt <= 0;
			elsif(( rtin_db = '1' ) and ( rtin_Db_del = '0' ))
				then Flash_Lo_Cnt <= 0;
			elsif(( rtin_db = '0' ) and ( rtin_Db_del = '1' ))
				then Flash_Lo_Cnt <= 0;
				else Flash_lo_Cnt <= Flash_lo_Cnt + 1;
			end if;

			-- Upper Bits of the Flash Count --------------
			if( Flash_tc = '1' ) 
				then flash_cnt <= x"0000";
			elsif(( rtin_db = '1' ) and ( rtin_Db_del = '0' ))
				then flash_cnt <= x"0000";
			elsif(( rtin_db = '0' ) and ( rtin_Db_del = '1' ))
				then flash_cnt <= x"0000";
			elsif( Flash_Lo_tc = '1' )
				then flash_cnt <= flash_cnt + 1;
			end if;

               -- Retain Mv_Cmd until Move is started
               if( mv_cmd = '1' ) 
                    then mv_cmdb <= '1';
               elsif( imove = '1' )
                    then mv_cmdb <= '0';
               end if;

               -- If not Moving and cmd to move
			if( Mv_Cmdb = '1' )
				then imove <= '1';
               elsif( mv_cnt_tc = '1' )
				then imove <= '0';
			end if;
			
               if( imove = '0' )
                    then mv_cnt <= 0;
                    else mv_cnt <= mv_cnt + 1;
               end if;

               if( flash_tc = '1' )then
                    rtin_reg  <= rtin;
                    if( rtin_reg = rtin ) 
					then	iac <= '0'; 	-- DC not flashing
	                    else iac <= '1';	-- AC  	flashing
                    end if;
			end if;
          end if;        -- rising edge
     end process;        -- process
-------------------------------------------------------------------------------
end behavioral;          -- RTEMDB.VHD
-------------------------------------------------------------------------------          

