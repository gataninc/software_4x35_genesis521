-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	rtem.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  Digital Interface for the UMS Controller
--
--	History
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		November 2, 2000 - MCS
--			Changed Version number ot 0x8332 to correspond to the Rev 2 Copper
--			relase version.
--		September 21, 2000 - MCS
--			Updated for the SEM requirement of the UMS
--			DMC_IN bus has been redefined as:
--			DMC_IN[0] = Interrupt ( Active Low )
--			DMC_IN[7:1] = Encoded Value where:
--				00 = Analyze
--				7F = Retract
--				01-7D moves to some intermediate point.
--
--		TODO
--			with Rev 2 Copper, add the Additional DIP switch inputs to support the 
--			3rd button.
--		March 24, 2000 - MCS
--			Developemnt completed for UMS and RTEM for both manual and computer control
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity rtem is 
	port( 
		imr       	: in      std_logic;				     	-- Master Reset
		clk20       	: in      std_logic;                         	-- Master Clock
		rtem_ini  	: in		std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
	    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
		Det_Sat		: in		std_logic;
		CMD_IN		: in		std_Logic;						-- COmmand to Move In
		CMD_OUT		: in		std_logic;						-- Command to Move Out
		Cmd_Wd		: in		std_logic;
		HC_MR		: in		std_logic;						-- High Count Master Reset
		RTEM_RED		: in		std_logic;						-- Input Status for In/Moving in
		RTEM_GRN		: in		std_logic;						-- Input Status for Out/Moving Out
		RTEM_ANALYZE	: out	std_logic;						-- Output to Move RTEM In
		RTEM_RETRACT	: out	std_logic;						-- Output to Move RTEM Out
		ST_RTEM   	: out	std_logic_vector(  3 downto 0 ));		-- RTEM Status
end rtem;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture schematic of rtem is

	-- Rtem_Vec <= dc_red & dc_grn & ac_red & ac_grn; -- RTEM State Vector
	constant RTV_No_Power 	: std_logic_vector( 3 downto 0 ) := "0000";
	constant RTV_Out 		: std_logic_vector( 3 downto 0 ) := "0100";
	constant RTV_In 		: std_logic_vector( 3 downto 0 ) := "1000";
	constant RTV_Mv_In 		: std_logic_vector( 3 downto 0 ) := "0110";
	constant RTV_Mv_Out 	: std_logic_vector( 3 downto 0 ) := "1001";
	constant RTV_HC		: std_logic_vector( 3 downto 0 ) := "0001";
	constant RTV_Error 		: std_logic_vector( 3 downto 0 ) := "0011";


	constant ST_RTEM_ERROR	: std_logic_Vector( 3 downto 0 ) := x"1";
     constant ST_RTEM_OUT     : std_logic_vector( 3 downto 0 ) := x"4";
     constant ST_RTEM_IN      : std_logic_vector( 3 downto 0 ) := x"3";
     constant ST_RTEM_MV_IN   : std_logic_vector( 3 downto 0 ) := X"7";
     constant ST_RTEM_MV_OUT  : std_logic_vector( 3 downto 0 ) := x"8";
     constant sT_RTEM_NO_POW  : std_logic_vector( 3 downto 0 ) := x"D";
     constant sT_RTEM_HC      : std_logic_vector( 3 downto 0 ) := x"E";

	constant ZEROES		: std_logic_Vector( 15 downto 0 ) := "0000000000000000";

	signal ac_red    		: std_logic;                      				-- RTEM Moving Indication
	signal ac_grn    		: std_logic;                      				-- RTEM Moving Indication
	signal dc_red    		: std_logic;                      				-- RTEM Static Indication
	signal dc_grn    		: std_logic;                      				-- RTEM Static Indication

	signal Hi_Cnt_En		: std_logic;					
	signal RTEM_OUT  		: std_logic;                      				-- RTEM Out Indicator
	signal RTEM_IN_CMD		: std_logic;
	signal RTEM_OUT_CMD		: std_logic;

	signal Hi_Cnt_State		: std_logic;

  	signal Rtem_Vec      	: std_logic_vector(  3 downto 0 ); 		-- RTEM State Vector
   -- Any Signal Declarations

	-----------------------------------------------------------------------------------------------

	-----------------------------------------------------------------------------------------------
	component RTEMdb 
		port(
			IMR       : in      std_logic;                         -- Master Reset
		    	clk20     : in      std_logic;                         -- Master Clock
		    	rtin      : in      std_logic;                         -- Led = On ( Active Low )
		    	mv_cmd    : in      std_logic;                         -- Input Move Command ( From Serial Bus )
			rtem_ini  : in      std_logic_vector( 15 downto 0 );   -- Flast Count Initialization
		     move      : out 	std_logic;                         -- Output Move Command - to RTEM
		     ac        : out  	std_logic;                         -- Flashing Output
		     dc        : out  	std_logic );                       -- Steady On Output
		end component RTEMdb;
	-----------------------------------------------------------------------------------------------
begin
	-- RED == ANALYZE
	-- GRN == RETRACT
	Rtem_Vec <= dc_red & dc_grn & ac_red & ac_grn; -- RTEM State Vector

	-----------------------------------------------------------------------------------------------
	RTEM_IN_CMD	<= '1' when (( Hi_Cnt_State = '0' ) and ( Hi_Cnt_En = '0' ) and ( CMD_IN = '1' )) else '0';

	U_RT_IN	: RTEMdb
		port map(
			IMR       	=> IMR,
			clk20       	=> clk20,
			rtin			=> RTEM_RED,	
			mv_cmd    	=> RTEM_IN_CMD,
			rtem_ini		=> RTEM_INI,
			move			=> RTEM_ANALYZE,
			ac			=> ac_red,
			dc			=> dc_red );

	-----------------------------------------------------------------------------------------------
	RTEM_OUT_CMD <= '1' when ( CMD_OUT = '1' ) else
				 '1' when (( RTEM_OUT = '0' ) and ( CMD_WD	= '1' )) else
				 '1' when (( RTEM_OUT = '0' ) and ( Hi_Cnt_En = '0' ) and ( HI_CNT  = '1' )) else
				 '1' when (( RTEM_OUT = '0' ) and ( hi_cnt_en = '0' ) and ( Det_Sat = '1' )) else
				 '0';

	U_RT_OUT	: RTEMdb
		port map(
			IMR       	=> IMR,
			clk20       	=> clk20,
			rtin			=> RTEM_GRN,	
			mv_cmd    	=> RTEM_OUT_CMD,
			rtem_ini		=> RTEM_INI,
			move			=> RTEM_RETRACT,
			ac			=> ac_grn,
			dc			=> dc_grn );

	-----------------------------------------------------------------------------------------------
	clk20_proc : process( IMR, clk20 )
	begin
		if( IMR = '1' ) then
			Hi_Cnt_En      <= '0';
		 	Hi_Cnt_State 	<= '0';
               RTEM_OUT       <= '0';
			ST_RTEM		<= ST_RTEM_NO_POW;
			Hi_Cnt_State	<= '0';
			
		elsif(( clk20'Event ) and ( clk20 = '1' )) then
		
               if(( hc_mr = '1' ) or ( RTEM_OUT_CMD = '1' ))
				then Hi_Cnt_En <= '0';
               elsif(( hi_cnt = '1' ) or ( Det_Sat = '1' ))
                    then Hi_Cnt_En <= '1';
               end if;

               if(( hc_mr = '1' ) or ( RTEM_OUT_CMD = '1' ))
				then Hi_Cnt_State <= '0';
			elsif(( Hi_Cnt_En = '1' ) and ( RTem_Vec = RTV_Out ))	-- Out
				then Hi_Cnt_State <= '1';
			end if;

			if( Hi_Cnt_State = '1' ) then
				ST_RTEM <= ST_RTEM_HC; RTEM_OUT <= '1';
			else 
				-- From the RTEM State Vector, determine the RTEM State
     	          case Rtem_Vec is
          	          when RTV_No_Power 	=> ST_RTEM <= ST_RTEM_NO_POW; RTEM_OUT <= '1'; -- No Power
               	     when RTV_Out		=> ST_RTEM <= ST_RTEM_OUT;    RTEM_OUT <= '1'; -- Out
	                    when RTV_In 		=> ST_RTEM <= ST_RTEM_IN;     RTEM_OUT <= '0'; -- In
     	               when RTV_Mv_In 	=> ST_RTEM <= ST_RTEM_MV_IN;  RTEM_OUT <= '0'; -- Move In
          	          when RTV_Mv_Out 	=> ST_RTEM <= ST_RTEM_MV_OUT; RTEM_OUT <= '1'; -- Move Out
               	     when RTV_HC 		=> ST_RTEM <= ST_RTEM_HC;     RTEM_OUT <= '1'; -- High Counts
               	     when RTV_Error  	=> ST_RTEM <= ST_RTEM_ERROR;  RTEM_OUT <= '1'; -- High Counts
	                    when others  =>                              -- No Change 
				end case;
			end if;
			-- RTEM State Vector ---------------------------------------------------------------
               ------------------------------------------------------------------------------------

		end if;
	end process;
	-----------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
end Schematic;			-- rtem.VHD
----------------------------------------------------------------------------------------------------

