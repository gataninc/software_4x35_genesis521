-------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	rtemctrl.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--                  Digital Interface for the UMS Controller
--
--	History
--		06/15/05 - MCS
--			Updated for QuartusII Version 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
--		November 2, 2000 - MCS
--			Changed Version number ot 0x8332 to correspond to the Rev 2 Copper
--			relase version.
--		September 21, 2000 - MCS
--			Updated for the SEM requirement of the UMS
--			DMC_IN bus has been redefined as:
--			DMC_IN[0] = Interrupt ( Active Low )
--			DMC_IN[7:1] = Encoded Value where:
--				00 = Analyze
--				7F = Retract
--				01-7D moves to some intermediate point.
--
--		TODO
--			with Rev 2 Copper, add the Additional DIP switch inputs to support the 
--			3rd button.
--		March 24, 2000 - MCS
--			Developemnt completed for UMS and RTEM for both manual and computer control
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------

library lpm;
	use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity rtemctrl is 
	port( 
		imr       	: in      std_logic;				     	-- Master Reset
		clk20       	: in      std_logic;                         	-- Master Clock
		RTEM_SEL		: in		std_logic_VECTOR( 1 downto 0 );
		ms100_tc		: in		std_logic;
		WD_EN		: in		std_logic;
		WD_MR		: in		std_logic;
		rtem_ini  	: in		std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
		CNT_100		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		CNT_MAX		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
	    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
		Det_Sat		: in		std_logic;
		CMD_IN		: in		std_Logic;						-- COmmand to Move In
		CMD_OUT		: in		std_logic;						-- Command to Move Out
		Cmd_Mid_In	: in		std_logic;
		Cmd_Mid_Out	: in		std_logic;
		CMD_HC_MR		: in		std_logic;						-- High Count Master Reset
		RTEM_RED		: in		std_logic;						-- Input Status for In/Moving in
		RTEM_GRN		: in		std_logic;						-- Input Status for Out/Moving Out
		RTEM_HS		: in		std_Logic;
		RTEM_ANALYZE	: buffer	std_logic;						-- Output to Move RTEM In
		RTEM_RETRACT	: buffer	std_logic;						-- Output to Move RTEM Out
		RTEM_CNT		: buffer	std_logic_Vector( 31 downto 0 );
		ST_RTEM   	: buffer	std_logic_vector(  3 downto 0 ) );		-- RTEM Status
end rtemctrl;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture schematic of rtemctrl is

	constant WD_CNT_MAX 	: integer := 149;						-- 15 Seconds @ 100ms resolution
	
	signal WD_CNT			: integer range 0 to WD_CNT_MAX;
	signal WD_CNT_TC		: std_logic;
	signal ms100_tc_vec 	: std_logic_Vector( 1 downto 0 );
	signal WD_MR_VEC		: std_logic_Vector( 1 downto 0 );
	
	signal RTEM_OUT		: std_logic_vector( 1 downto 0 );
	signal RTEM_IN			: std_logic_vector( 1 downto 0 );

	signal RTEM_CNT1		: std_logic_Vector( 31 downto 0 );
	signal ST_RTEM0		: std_logic_Vector( 3 downto 0 );
	signal ST_RTEM1		: std_logic_Vector( 3 downto 0 );

	signal Cmd_Wd			: std_logic;

     --------------------------------------------------------------------------
	component rtem
		port( 
			imr       	: in      std_logic;				     	-- Master Reset
			clk20       	: in      std_logic;                         	-- Master Clock
			rtem_ini  	: in		std_logic_Vector( 15 downto 0 );		-- RTEM Test Clock Control
		    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
			Det_Sat		: in		std_logic;
			CMD_IN		: in		std_Logic;						-- COmmand to Move In
			CMD_OUT		: in		std_logic;						-- Command to Move Out
			Cmd_Wd		: in		std_logic;
			HC_MR		: in		std_logic;						-- High Count Master Reset
			RTEM_RED		: in		std_logic;						-- Input Status for In/Moving in
			RTEM_GRN		: in		std_logic;						-- Input Status for Out/Moving Out
			RTEM_ANALYZE	: out	std_logic;						-- Output to Move RTEM In
			RTEM_RETRACT	: out	std_logic;						-- Output to Move RTEM Out
			ST_RTEM   	: out	std_logic_vector(  3 downto 0 ));		-- RTEM Status
	end component rtem;
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	component UMS2
		port( 
			imr       	: in      std_logic;				     	-- Master Reset
			clk20       	: in      std_logic;                         	-- Master Clock
			ms100_tc		: in		std_logic;
			CNT_100		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
			CNT_MAX		: in		std_logic_Vector( 31 downto 0 );		-- 100% Cnt Value
		    	Hi_Cnt    	: in      std_logic;						-- High Count Rate Retract
			Det_Sat		: in		std_logic;
			CMD_IN		: in		std_Logic;						-- COmmand to Move In
			CMD_OUT		: in		std_logic;						-- Command to Move Out
			Cmd_Mid_In	: in		std_logic;
			Cmd_Mid_out	: in		std_logic;
			Cmd_WD		: in		std_logic;
			HC_MR		: in		std_logic;						-- High Count Master Reset
			RTEM_RED		: in		std_logic;						-- Analyze Switch Closure/Limit Switch
			RTEM_GRN		: in		std_logic;						-- Retract Switch Closure
			UMS_DIR		: out	std_logic;						-- Direction
			UMS_SCLK		: out	std_logic;						-- Steps
			UMS_CNT		: out	std_logic_Vector( 31 downto 0 );
			ST_RTEM   	: out	std_logic_vector(  3 downto 0 )); 		-- RTEM State
	end component UMS2;
     --------------------------------------------------------------------------

begin
	with conv_integer( RTEM_SEL ) select
		RTEM_ANALYZE 	<= RTEM_OUT(0) when 0,
					   RTEM_OuT(1) when others;

	with conv_integer( RTEM_SEL ) select
		RTEM_RETRACT 	<= RTEM_IN(0) when 0,
					   RTEM_IN(1) when others;

	with conv_integer( RTEM_SEL ) select
		ST_RTEM		<= ST_RTEM0 when 0,
					   ST_RTEM1 when others;

	with conv_integer( RTEM_SEL ) select
		RTEM_CNT		<= RTEM_CNT1 when 1,
					   x"00000000" when others;

	Cmd_Wd		<= '1' when (( WD_EN = '1' ) and ( WD_CNT_TC = '1' )) else '0';
	
	WD_CNT_TC		<= '1' when (( WD_CNT = WD_CNT_MAX ) and ( ms100_tc = '1' )) else '0';

     --------------------------------------------------------------------------
	URTEM : RTEM
		port map(
			imr       	=> imr,			-- Master Reset
			clk20       	=> Clk20,			-- Master Clock
			rtem_ini  	=> Rtem_ini,
		    	Hi_Cnt    	=> Hi_Cnt,
			Det_Sat		=> Det_Sat,
			CMD_IN		=> CmD_In,
			CMD_OUT		=> Cmd_Out,
			Cmd_wd		=> Cmd_Wd,
			HC_MR		=> CMD_HC_Mr,
			RTEM_RED		=> RTEM_Red,		-- Analyze Switch Closure
			RTEM_GRN		=> RTEM_Grn,		-- Retract Switch Closure
			RTEM_ANALYZE	=> RTEM_Out(0),
			RTEM_RETRACT	=> RTEM_IN(0),
			ST_RTEM   	=> ST_RTEM0 );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	UUMS2 : UMS2
		port map(
			imr       	=> imr,				-- Master Reset
			clk20       	=> Clk20,				-- Master Clock
			ms100_tc		=> ms100_tc,
			CNT_100		=> CNT_100,
			CNT_MAX		=> CNT_MAX,
		    	Hi_Cnt    	=> Hi_Cnt,
			Det_Sat		=> Det_Sat,
			CMD_IN		=> CmD_In,
			CMD_OUT		=> Cmd_Out,
			CMD_Mid_In	=> Cmd_Mid_In,
			Cmd_Mid_Out	=> Cmd_Mid_Out,
			Cmd_Wd		=> Cmd_wd,
			HC_MR		=> CMD_HC_Mr,
			RTEM_RED		=> RTEM_RED,			-- Analyze Switch Closure/Limit Switch
			RTEM_GRN		=> RTEM_GRN,			-- Retract Switch Closure
			UMS_DIR		=> RTEM_Out(1),		-- Direction
			UMS_SCLK		=> RTEM_In(1), 		-- Steps
			UMS_CNT		=> RTEM_Cnt1,			-- CUrrent COunt Position
			ST_RTEM   	=> ST_RTEM1 );			-- RTEM Status
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	clock_proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			ms100_Tc_Vec 	<= "00";
			WD_MR_VEC		<= "00";	
			WD_CNT		<= 0;
		elsif(( clk20'event ) and ( clk20 = '1' )) then
			ms100_Tc_vec 	<= ms100_tc_Vec(0) & ms100_Tc;
			WD_MR_VEC		<= WD_MR_VEC(0) & WD_MR;

			if(( WD_MR_VEC = "01" ) or ( WD_EN = '0' ))
				then WD_CNT	<= 0;
			elsif( ms100_tc_Vec = "01" )
				then WD_CNT	<= WD_CNT + 1;
			end if;
		end if;
	end process;
     --------------------------------------------------------------------------
	
----------------------------------------------------------------------------------------------------
end Schematic;			-- rtemctrl.VHD
----------------------------------------------------------------------------------------------------

