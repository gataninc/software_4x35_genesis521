----------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	EP99841.VHD ( aka EDSFPGA.VHD for WDS )
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev D
--
--	Top Level		: 4035.009.99841 S/W, DPP-II, EDS FPGA, WDS ONLY
--
--   Description:	
--		This module takes monitors the positive/negative slope as well as the Baseline Trigger
--        It looks for a rising slope above the baseline trigger and makes sure that the pulse
--        width is greater that the specified risetime parameter.
--        It has an output that indicates Invalid Rise Time so the stream will be rejected.
--
--
--	
--
-- See EDI-II Notebook page 72 
-- DSP Data Read Timeing:
--	1 Wait State
--					 |<-- 33ns-->|
--            +-----+     +-----+     +-----+     +-----+     +-----+     +-----+     
-- DSP_H3     |     |     |     |     |     |     |     |     |     |     |     |
--       -----+     +-----+     +-----+     +-----+     +-----+     +-----+     +-----
--
--                   ---->|    |<--- 7ns          |
--
--	    -------------------\ /----------------------\ /------------------------------
-- ADDR                      x                        x
--       -------------------/ \----------------------/ \------------------------------
--
--                             |            -->|  |<--- 10ns
--                             |             /---------\
-- DATA  -----------------------------------x           x-----------------------------
--                             |             \---------/
--                             |<-- Tpd max -->| = ( 2x 33ns ) - 7ns - 10ns = 49ns, 80% = 40ns
--						
--
--
--   History:       <date> - <Author> - Ver
--		04/24/07 - MCS - Ver 4x30
--			Apollo 10 Disable 2X Gain Switch
--		04/23/07 - MCS - Ver 4x2F
--			Apollo 10 Enable 2X Gain Switch ( again )
--		04/03/07 - MCS - Ver 4x2C
--			Apollo 10 Enable 2x gain switch
--		03/21/07 - MCS - Ver 4x2B
--			Disc is inverted ( confirmend with scope on U10-1 on DPP2 FR board ).
--
--		03/14/07 - MCS - Ver 4x2A
--			New Detector type DET_204NFR with a code of 5. This type will not throw the x2 gain switch
--			type DET_204 was renamed to DET_204FR
--			NO CHANGES TO THIS FILE
--		03/02/07 - MCS - Ver 4x29
--			PA_MODE(3) had to be inverted for Det type DET_204
--			Disc_Inv(0) is to not be inverted from AD_Invert
--		02/22/07 - MCS - Ver 4x28
--			New Register DET_TYPE at address 0x63 in the FPGA used to contain more detector types	
-- 				DET_204			= 0 ( Existing )
-- 				DET_UNICORN		= 1 ( Existing )
-- 				DET_AP40 			= 2 ( Existing )
-- 				DET_WDS( LEX or TEX ) 	= 3 ( Existing )
-- 				DET_AP10			= 4 � New Decode
--			Added "AD_INVERT" to be set when the A/D input is to be inverted, 
--			which will invert to module AD9240 as well as set the appropraite bits
--			for the SHDisc Inversion
-- 		08/23/06 - MCS - Ver 4x27
--			Added a RESET_GEN_CNT_MAX to limit the pulse width for the reset set to 200us
--		08/22/06 - MCS - Ver 4x26: Module AD9240.VHD
--			Max/Min Adjust threshold changed to original method due to reset being stuck active with a ramp that is too small
--			seen by increased gain from VItus SDD

--		08/11/06 - MCS - Ver 4x25: Module AD9240.VHD
--			Added the circuit to force a reset generate if the ramp is saturated,
--			ie no reset for some time..
--
--		***** 08/10/06 - Ver 4x24 ***** GENESIS 5.1 Release Candidate *******************************************
--
--		08/10/06 - MCS Ver 4x24 - Genesis 5.1 Release Candidate
--			ADDED OTR_EN which is disabled (0) when Preset Mode is Live Spectrum Mapping or WDS Modes
--			otherwise it is enabled ( Per Laszlo's request )
--		08/08/06 : MCS Ver 4x23 
--			Module AD9240: Added Hysterysis to Preamp Reset
--		08/09/06 - DSP: MCS Ver 4x23													
--			Edsutil.C : SetROISCARM( int ROI_NUM ) changed the address to the SCA Look-up memory 
--			to be just the index not 1/2 the index										
--		07/05/06 : MCS Ver 4x22
--			Changes made in FIFO in DSO mode to facilitate better DSO and FFT Modes of operation
--			DSO Mode given its own keys in the FIFO
--			FF_CNT incoroprated to count the number of words written to the FIFO only for DSO Mode
--			each 1/8 of full fifo will cause na interrupt to the DSP to start reading out the FIFO Data
--		06/30/06 : MCS Ver 4x20
--			Not a complete version
--		06/26/06 : MCS Ver 4x19
--			Preset_DOne_Cnt_Reg is enabled when PRESET_OUT = 0 AND PS_OUT_LSM = 0
--			in other words, allowed to be updated when not near the Preset Done Condition
--			Set order of Peak Data higher priority then Preset Done
--			FIFO Word counter added to maintain the number of words written to the FIFO
--			during DSO Mode
--		5/22/06: MCS Ver 4x16
--			RT_IN(0) held at a logic low to force the RT Bus pin 5 to be tristate ( by nature 
--		5/16/06: MCS Ver 4015
--			Added logic to stop clock/live time in fir_cntrate when the ADC is out-of-range
--			Added logic to prevent DET_SAT in PA_RESET_DET when ADC is less than 1/2 scale
--		3/27/06: MCS Ver 4011
--			Rebuilt to Quartus 5.1 SP 2
--		1/12/06 : MCS : Ver 4004
--			Pile-Up Reject disabled in module WDS
--		12/15/05 - MCS: Ver 3581
--			Module AD9240: AD_MAXMIN incorporated into this module
--			Fine Gain added 25%
--		12/13/05 - MCS: Ver 3580
--			Module AD9240: AD_Sel removed from module, and debug_ad always A/D output
--			Module AD_MAXMIN: Ramp_Mode = 0 added "Clamp off" any PA_RESET
--
--		12/12/05 - MCS - Ver 357F
--			AD9240 module as altered to keep the sum of 2 samples @ 20Mhz from a 10MHZ convereter
--			delivering 15 bits at 20Mhz instead of 14 bits at 10Mhz
--			FIR_GEN2 module: Mux_Start returned back to 1
--			FIR_PPROC Module: eliminated FIne Gain Multiplier which was moved to ad9240
--			AD_MAXMin Module: Ramp Mode added to use "PA_RESET" for edge detects for MAx/Min else just the decay
--			Side affect, Coarse Gain at 102.4us set to 50K, Preamp Gain and thresholds must be changed
--
--		12/08/05 - MCS - Ver 357E
--			Fine Gain was removed ( or held fixed ) by mult max_fir by 16384 (4000hex)
--			but the multiplier was moved to the AD9240 module. 
--			Apply the fine gain to the input ramp, then run it through the filter
--			but leave the zero at the tail end 
-----------------------------------------------------------------------------------------


library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;
----------------------------------------------------------------------------------------
-- ENTITY FOR REV D
----------------------------------------------------------------------------------------
entity EP99841 is
     port( 
		-- System Signals -----------------------------------------------------------------------
		OMR       	: in      std_logic;							-- Master Reset
          CLK20_IN  	: in      std_logic_vector( 1 downto 0 );              -- Master Clock 10 Mhz

		-- DSP Signals --------------------------------------------------------------------------
		DSP_H3		: in		std_logic;							-- Clock from DSP 33ns period
          DSP_WR		: in		std_logic;							-- DSP Write Signal
          DSP_STB		: in		std_logic_vector(  3 downto 0 );			-- DSP Strobes
          DSP_A		: in		std_logic_vector( 15 downto 13 );			-- DSP Address
		DSP_A1		: in		std_logic_vector(  7 downto 0 );			-- DSP Address ( Low Bits )
          DSP_IRQ		: buffer	std_logic;							-- Interrupt to DSP
          DSP_D		: inout	std_logic_vector( 15 downto 0 );			-- DSP Data[15:00]

		-- PreAmp Releated Signals --------------------------------------------------------------
		OHV_Enable	: in		std_logic_vector( 1 downto 0 );
		IHV_Dis		: buffer	std_logic_Vector( 1 downto 0 );
		PA_MODE		: buffer	std_logic_Vector( 3 downto 0 );

		-- Analog Discriminator Signals ----------------------------------------------------------
		SHDisc		: in		std_logic;							-- 50ns Discriminator
		DISC_INV		: buffer	std_logic_Vector( 1 downto 0 );
		DISC_EVCH		: buffer	std_logic_Vector( 1 downto 0 );

		-- A/D Data Signals ---------------------------------------------------------------------
		CLK_FAD		: buffer	std_logic;
		OTR			: in		std_logic;							-- Out of Range input from A/D
		adata		: in		std_logic_Vector( 13 downto 0 );			-- Slow A/D Data

		-- Real Time Signals --------------------------------------------------------------------
		RT_OUT		: in		std_logic_Vector( 1 downto 0 );			-- AS seen by Scan Generator
		RT_IN		: buffer	std_logic_vector( 1 downto 0 );			-- AS seen by Scan Generator

		-- SCA Output 
		SCA			: buffer	std_logic_Vector( 7 downto 0 );
		-- DSP FiFo -----------------------------------------------------------------------------
		-- Writes at 10Mhz (CLK10_Out)
		-- Reads at DSP_H3
		FF_EF		: in		std_logic;							-- Fifo Empty Flag
		FF_FF		: in		std_logic;							-- Fifo Full Flag
		FF_OE		: buffer	std_logic;							-- FIFO Output Enable
		FF_RD		: buffer	std_logic;							-- Fifo Read
		FF_WR		: buffer	std_logic;							-- Fifo Write
		FF_RS		: buffer	std_logic;							-- Fifo Reset
		CLK20		: buffer	std_logic;
		FF_RCK		: buffer	std_logic;
		FF_WDATA		: buffer	std_logic_Vector( 17 downto 0 );			-- FIFO Write Data

		-- RTEM Support Signals ---------------------------------------------------------------
		RTEM_TILT		: in		std_logic;
		RTEM_RED		: in		std_logic;
		RTEM_GRN		: in		std_logic;
		RTEM_ANALYZE	: buffer	std_logic;
		RTEM_RETRACT	: buffer	std_logic;			

		-- DAC8420 Interface -----------------------------------------------------
		DA_CLK		: buffer	std_logic;
		DA_DOUT		: buffer	std_logic;
		DA_UPD		: buffer	std_logic;
		DA_OFS		: buffer	std_logic;
		-- DAC8420 Interface -----------------------------------------------------

		-- LTC1595 Interface ( Coarse Gain ) -------------------------------------
		CG_LD		: buffer	std_logic;
		CG_CLK		: buffer	std_logic;
		CG_Data		: buffer	std_logic;
		-- LTC1595 Interface ( Coarse Gain ) -------------------------------------

		-- ADS8321 BITDAC Interface ----------------------------------------------
		AD_Dout		: in		std_logic;	-- A/D Data Output
		AD_CS		: buffer	std_logic;	-- A/D Chip Select
		AD_CLK		: buffer	std_logic;	-- A/D Clock ( 2Mhz )
		-- ADS8321 BITDAC Interface ----------------------------------------------

		-- Misc Signals -------------------------------------------------------------------------
		ATP_SEL		: buffer	std_logic_Vector( 3 downto 0 );
	     SERIAL_NO		: inout	std_logic;					-- Electronic Serial Number
     	TMP_SENSE      : in      std_logic_vector(  2 downto 0 );	-- Temperature Sensor Input (TMP_SENSE(1) = MOVE_FLAG for WDS)
          TP			: buffer	std_logic_vector(  7 downto 0 ));	-- Output TP
     end EP99841;
----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------
architecture BEHAVIORAL of EP99841 is
	constant VER					: integer := 16#4130#;
	constant ZEROES				: std_logic_vector( 31 downto 0 ) := x"00000000";
	constant fast_gen				: integer := 0;

	constant PA_DET0				: std_logic_vector( 1 downto 0 ) := "00";
	constant PA_DET1				: std_logic_vector( 1 downto 0 ) := "01";
	constant PA_BITA				: std_logic_vector( 1 downto 0 ) := "10";
	constant PA_BITD				: std_logic_vector( 1 downto 0 ) := "11";

	
     ------------------------------------------------------------------------------------
	-- DSP Decode Related --------------------------------------------------------------
     ------------------------------------------------------------------------------------
	-- DSP Decode Related --------------------------------------------------------------
	constant iADR_ID    			: integer := 16#0000#;	-- ID Word
	constant iADR_STAT   			: integer := 16#0001#;	-- Status Word
	constant iADR_CW   				: integer := 16#0002#;	-- Control Word
--	constant iADR_SN0				: integer := 16#0003#;	-- Serial Number Bits 15:0
--	constant iADR_SN1				: integer := 16#0004#;	-- Serial Number Bits 31:16
--	constant iADR_SN2				: integer := 16#0005#;	-- Serial Number Bits 47:32
--	constant iADR_SN3				: integer := 16#0006#;	-- Serial Number Bits 63:48
	constant iADR_DSO_START			: integer := 16#0007#;	-- Start DSO
	constant iADR_TP_SEL			: integer := 16#0008#;	
	constant iADR_INT_EN			: integer := 16#0009#;	-- Interrupt Enable/Disable
	constant iADR_CGain				: integer := 16#000A#;	-- Coarse Gain (Analog)
--	constant iADR_FGain				: integer := 16#000B#;	-- Fine Gain (Digital)
--	constant iADR_OFFSET   			: integer := 16#000C#;	-- Offset ( Digital )
--	constant iADR_CLIP_MIN			: integer := 16#000D#;
--	constant iADR_CLIP_MAX			: integer := 16#000E#;
	constant iADR_DISC1				: integer := 16#000F#;	
	constant iADR_DISC2				: integer := 16#0010#;
	constant iADR_EVCH				: integer := 16#0011#;
	constant iADR_BIT				: integer := 16#0012#;
	constant iADR_TIME_START			: integer := 16#0013#;	
	constant iADR_TIME_STOP			: integer := 16#0014#;	
	constant iADR_TIME_CLR			: integer := 16#0015#;	
	constant iADR_PRESET_MODE 		: integer := 16#0016#;	
	constant iADR_PRESET_LO			: integer := 16#0017#;	
	constant iADR_PRESET_HI  		: integer := 16#0018#;
	constant iADR_PRESET_ACK			: integer := 16#0019#;
--	constant iADR_RTEM_INI			: integer := 16#001A#;
--	constant iADR_RTEM_IN			: integer := 16#001B#;
--	constant iADR_RTEM_OUT			: integer := 16#001C#;
--	constant iADR_RTEM_HCMR			: integer := 16#001D#;
---	constant iADR_RTEM_hc_RATE_lo		: integer := 16#001E#;
--	constant iADR_RTEM_hc_RATE_hi		: integer := 16#001F#;
--	constant iADR_RTEM_hc_THRESHOLD	: integer := 16#0020#;
	constant iADR_RAMA				: integer := 16#0021#;		-- 0x23, Look-Up Ram Address
--	constant iADR_SCA				: integer := 16#0022#;		-- 0x08, Analog SCA/Digital ( Direct )
--	constant iADR_SCA_LU			: integer := 16#0023#;		-- 0x09, Digital SCA Look-Up Data
--	constant iADR_DSCA_PW			: integer := 16#0024#;		-- 0x0A, Digital SCA Pulse Width
	constant iADR_ROI_LU			: integer := 16#0025#;		-- 0x10, ROI Lookup Data 
--	constant iADR_PA_RESET_NTHR		: integer := 16#0026#;
--	constant iADR_PA_RESET_PTHR		: integer := 16#0027#;
	constant iADR_FIFO_LO			: integer := 16#0028#;		-- 0x11, FIFO Low Word
	constant iADR_FIFO_HI			: integer := 16#0029#;		-- 0x12, FIFO High Word
	constant iADR_FIFO_MR			: integer := 16#002A#;		-- 0x13, FIFO Master Reset
	constant iAdr_Disc_En			: integer := 16#002B#;			-- Discriminator Enables
--	constant iADR_TC_SEL			: integer := 16#002C#;		-- Adaptive Mode/Time Constant Select
--	constant iADR_DLEVEL0			: integer := 16#002D#;		-- High
--	constant iADR_DLEVEL1			: integer := 16#002E#;		-- Medium
--	constant iADR_DLEVEL2			: integer := 16#002F#;		-- Low
	
	constant iADR_DLEVEL3			: integer := 16#0030#;		-- Energy
--	constant iADR_BLANK_Mode			: integer := 16#0031#;		-- Energy
--	constant iADR_BLANK_DELAY		: integer := 16#0032#;		-- Energy
--	constant iADR_BLANK_WIDTH		: integer := 16#0033#;		-- Energy
--	constant iADR_PS_SEL			: integer := 16#0034#;
	constant iADR_BIT_PEAK1			: integer := 16#0035#;		-- Normalization Select
	constant iADR_BIT_PEAK2			: integer := 16#0036#;		-- Normalization Select
	constant iADR_BIT_CPS			: integer := 16#0037#;		-- Normalization Select
	constant iADR_BIT_PK2D			: integer := 16#0038#;		-- Normalization Select
	constant iADR_TLO  				: integer := 16#0039#;		-- 100ms Timer
	constant iADR_THI				: integer := 16#003A#;		-- 100ms Timer
--	constant iADR_DU0_TLO			: integer := 16#003B#;		-- 100ms Timer
--	constant iADR_DU0_THI			: integer := 16#003C#;		-- 100ms Timer
--	constant iADR_DU1_TLO			: integer := 16#003D#;		-- 100ms Timer
--	constant iADR_DU1_THI			: integer := 16#003E#;		-- 100ms Timer
	constant iADR_IN_CPSL			: integer := 16#003F#;		-- 100ms Timer
	
	constant iADR_IN_CPSH			: integer := 16#0040#;		-- 100ms Timer
	constant iADR_OUT_CPSL			: integer := 16#0041#;		-- 100ms Timer
	constant iADR_OUT_CPSH			: integer := 16#0042#;		-- 100ms Timer
	
	constant iADR_ROI_SUML			: integer := 16#0043#;		-- 100ms Timer
	constant iADR_ROI_SUMH			: integer := 16#0044#;		-- 100ms Timer

	constant iADR_LTIME_LO			: integer := 16#0045#;		-- 100ms Timer
	constant iADR_LTIME_HI			: integer := 16#0046#;		-- 100ms Timer
	constant iADR_CTIME_LO			: integer := 16#0047#;		-- 100ms Timer
	constant iADR_CTIME_HI			: integer := 16#0048#;		-- 100ms Timer
	constant iADR_RAMP_LO 			: integer := 16#0049#;		-- 100ms Timer
	constant iADR_RAMP_HI  			: integer := 16#004A#;		-- 100ms Timer
--	constant iADR_Reset_Width		: integer := 16#004B#;		-- 100ms Timer
	constant iADR_NET_CPSL			: integer := 16#004C#;		-- 100ms Timer
	constant iADR_NET_CPSH			: integer := 16#004D#;		-- 100ms Timer
	constant iADR_NET_NPSL			: integer := 16#004E#;		-- 100ms Timer
	constant iADR_NET_NPSH			: integer := 16#004F#;		-- 100ms Timer
	
--	constant iADR_rms_out			: integer := 16#0050#;
--	constant iADR_RMS_THR			: integer := 16#0051#;
--	constant iADR_PA_TIME_LO			: integer := 16#0052#;		-- 100ms Timer
--	constant iADR_PA_TIME_HI			: integer := 16#0053#;		-- 100ms Timer
--	constant iADR_AUTO_SHDISC		: integer := 16#0054#;
		
--	constant iADR_RTEM_SEL			: integer := 16#0055#;		-- 100ms Timer
--	constant iADR_RTEM_Low			: integer := 16#0056#;	-- was 54
--	constant iADR_RTEM_Med			: integer := 16#0057#;	-- was 55
--	constant iADR_RTEM_High			: integer := 16#0058#;	-- was 56
--	constant iADR_RTEM_CNT_L			: integer := 16#0059#;	-- was 57
--	constant iADR_RTEM_CNT_H			: integer := 16#005A#;	-- was 58
--	constant iADR_SH_Disc_Cnts		: integer := 16#005B#;		
--	constant iADR_H_Disc_Cnts		: integer := 16#005C#;
--	constant iADR_M_Disc_Cnts		: integer := 16#005D#;
--	constant iADR_L_Disc_Cnts		: integer := 16#005E#;
--	constant iADR_E_Disc_Cnts		: integer := 16#005F#;
	
--	constant iADR_DECAY 			: integer := 16#0060#;
	constant iADR_BIT_ADC			: integer := 16#0061#;	
	constant iADR_FIR_MR			: integer := 16#0062#;
	constant iADR_DET_TYPE			: integer := 16#0063#;
	
--	constant iADR_RTEM_CNT_100_L		: integer := 16#006B#;
--	constant iADR_RTEM_CNT_100_H		: integer := 16#006C#;
--	constant iADR_RTEM_CNT_MAX_L		: integer := 16#006F#;
--	constant iADR_RTEM_CNT_MAX_H		: integer := 16#0070#;
--	constant iADR_RTEM_CMD_MID_IN		: integer := 16#0071#;
--	constant iADR_RTEM_CMD_MID_OUT	: integer := 16#0072#;
--	constant iADR_RTEM_WD_MR			: integer := 16#0073#;
	constant iADR_FADC				: integer := 16#0074#;
	constant iADR_DSO_TRIG			: integer := 16#0075#;
	constant iADR_DSO_INT			: integer := 16#0076#;
--	constant iADR_SHDisc_Dly_Sel		: integer := 16#0077#;
--	constant iADR_RTEM_TEST_REG		: integer := 16#0078#;
--	constant iADR_FIR_DLY_LEN		: integer := 16#0079#;
--	constant iADR_FIR_DLY_INC		: integer := 16#007A#;
	constant iADR_Disc_Evch			: integer := 16#007B#;
	constant iADR_PRESET_DONE_CNT		: integer := 16#007C#;
	constant iADR_Reset_Mr			: integer := 16#007E#;
	constant iADR_FF_DSO_CNT			: integer := 16#007F#;
	-- DSP Decode Related ------------------------------------------------------------------------

	-- COntrol Word Bit Positions ----------------------------------------------------------------
	constant CW_BIT_ENABLE 			: integer := 0;
--	constant CW_DSCA_POL			: integer := 1;
	constant CW_BLEVEL_EN			: integer := 2;
	constant CW_EDX_XFER			: integer := 3;
	constant CW_MEMORY_ACCESS		: integer := 4;
--	constant CW_SCA_EN				: integer := 5;
	constant CW_DSO_EN				: integer := 6;
	constant CW_ADisc_En			: integer := 7;
--	constant CW_ME_FET				: integer := 8;
	constant CW_LSMAP_TEST			: integer := 9;

	constant CW_VIDEO_THR_MODE		: integer := 10;
	constant CW_PA_SOURCE_LO			: integer := 11;
	constant CW_PA_SOURCE_HI			: integer := 12;
	constant CW_GAIN_2X				: integer := 13;
	constant CW_Det_Type_Lo 			: integer := 14;
	constant CW_Det_Type_Hi 			: integer := 15;
	-- COntrol Word Bit Positions ----------------------------------------------------------------

	-- CW_Det_Type_Hi downto CW_Det_Type_Lo definitions
	constant DET_204FR			: integer := 0;
	constant DET_UNICORN		: integer := 1;		-- Unicorn Detector
	constant DET_AP40			: integer := 2;
	constant DET_WDS			: integer := 3;		-- 3574
	constant DET_AP10			: integer := 4;
	constant DET_204NFR			: integer := 5;

	-- Type Declarations -------------------------------------------------------------------------

	-- Signal Declarations ------------------------------------------------------------------------
	signal AD_AC_data 		: std_logic_Vector( 15 downto 0 );
	signal AD_BUSY			: std_logic;
	signal AD_Data 		: std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
	signal AD_data_mux		: std_logic_Vector( 15 downto 0 );
	signal AD_Max			: std_logic_Vector( 15 downto 0 );
	signal AD_Min			: std_logic_Vector( 15 downto 0 );
	
	signal BIT_ADATA		: std_logic_Vector( 15 downto 0 );
	signal BIT_CPS			: std_logic_Vector( 15 downto 0 );
	signal BIT_DATA		: std_logic_Vector( 15 downto 0 );
	signal BIT_Peak1		: std_logic_Vector( 11 downto 0 );
	signal BIT_Peak2		: std_logic_Vector( 11 downto 0 );
	signal BIT_PK2D		: std_logic_Vector( 15 downto 0 );
	signal BLEVEL			: std_logic_Vector(  7 downto 0 );	-- Baseline Level
	signal BLEVEL_UPD		: std_logic;
	
	signal CLK40			: std_logic;
	signal CGain			: std_logic_Vector( 15 downto 0 );
	signal CPEAK			: std_logic_Vector( 13 downto 0 );	-- Corrected Peak Value	12/10/99
	signal CPS			: std_logic_vector( 20 downto 0 );	-- Input CPS
	signal CTIME			: std_logic_Vector( 31 downto 0 );
	signal CWORD			: std_logic_vector( 15 downto 0 );		-- Control Word

	signal DEbug			: std_logic_Vector( 15 downto 0 );
	signal debug_ad		: std_logic_vector( 15 downto 0 );
	signal Disc_En			: std_logic_vector( 5 downto 0 );
	signal DAC			: std_logic_Vector( 47 downto 0 );
	signal DSO_INT			: std_Logic_Vector( 15 downto 0 );
	signal dso_sel			: std_logic_Vector( 5 downto 0 );
	signal DSO_TRIG		: std_logic_Vector( 3 downto 0 );
	signal DSO_TRIG_VEC		: std_logic_vector( 5 downto 0 );
	signal DSP_DATA_EN		: std_logic;
	signal DSP_RAMA 		: std_logic_vector( 15 downto 0 );			-- DSP Ram Address
	signal DSP_READ		: std_Logic;
	signal DSP_WD			: std_Logic_Vector( 15 downto 0 );
	signal DET_TYPE		: std_logic_Vector(  3 downto 0 );
	
	signal fdisc			: std_logic;
	signal FF_DONE			: std_logic;
	signal FF_DSO_CNT		: std_logic_Vector( 15 downto 0 );

	signal HV_Enable 		: std_logic_vector( 1 downto 0 );

	signal iDSP_D 			: Std_logic_vector( 15 downto 0 );				-- Internal DSP Read Bus
	signal iDSP_D_MUX		: std_logic_Vector( 15 downto 0 );
	signal INT_EN			: std_logic_vector( 1 downto 0 );				-- Interrupt Enable
	signal IMR			: std_logic;
	signal IPD_END			: std_logic;

	signal LS_Abort		: std_logic;
	signal LTIME			: std_logic_vector( 31 downto 0 );
	signal LS_MAP_EN		: std_logic;

	signal MEAS_DONE		: std_logic;
	signal ms100_tc		: std_logic;								-- 100ms Timer
	
	signal NET_CPS			: std_logic_Vector( 31 downto 0 );
	signal NET_NPS			: std_logic_vector( 31 downto 0 );
	signal NPS			: std_logic_vector( 20 downto 0 );	-- Output CPA

	signal PBusy			: std_logic;
	signal PRESET			: std_logic_Vector( 31 downto 0 );
	signal Preset_Done_Cnt	: std_logic_vector( 15 downto 0 );
	signal PRESET_MODE		: std_logic_vector(  3 downto 0 );		-- 0 = None, 1 = Clock, 2 = Live
	signal Preset_Out		: std_logic;
	
	signal ROI_LU_DATA		: std_logic_Vector( 15 downto 0 );
	signal ROI_SUM			: std_logic_Vector( 31 downto 0 );

	signal STAT_OE			: std_Logic;
	signal Status_Vec 		: std_logic_Vector( 15 downto 0 );
	signal Status_OE		: std_logic;
	
	signal Time_Enable		: std_logic;
	signal time_clear		: std_logic;
	signal TEST_REG		: std_logic_Vector( 31 downto 0 );
	signal tlevel3			: std_logic_vector( 15 downto 0 );
	signal TMP_LO			: std_logic_vector( 15 downto 0 );
	signal TMP_HI			: std_logic_vector( 15 downto 0 );
	signal TP_SEL			: std_logic_vector( 15 downto 0 );

	signal VIDEO_ABORT		: std_logic;

	signal WDS_Dout			: std_logic_Vector( 15 downto 0 );
	signal WDS_MF				: std_logic;
	signal WDS_Busy			: std_logic;
	signal WE_DCD				: std_logic_vector( 127 downto 0 );-- Number of DSP Decodes

	
     -----------------------------------------------------------------------------------
     -- Start of Component Declarations ------------------------------------------------
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- MEGAFUNCTION WIZARD Component Declarations -------------------------------------
     -----------------------------------------------------------------------------------
     -----------------------------------------------------------------------------------
	component altclklock
		generic(
			inclock_period			: NATURAL;
			clock0_boost			: NATURAL;
			clock1_boost			: NATURAL;
			operation_mode			: STRING;
			intended_device_family	: STRING;
			valid_lock_cycles		: NATURAL;
			invalid_lock_cycles		: NATURAL;
			valid_lock_multiplier	: NATURAL;
			invalid_lock_multiplier	: NATURAL;
			clock0_divide			: NATURAL;
			clock1_divide			: NATURAL;
			outclock_phase_shift	: NATURAL;
			lpm_type				: STRING );
		port(
			inclock				: IN STD_LOGIC;
			clock0				: buffer STD_LOGIC;
			clock1				: buffer STD_LOGIC );
	end component;
     -----------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component AC_COUPLE is 
		port( 	
		     imr       	: in      std_logic;                    -- Master Reset
			clk       	: in      std_logic;			     -- Master Clock Input
			reset		: in		std_logic;
			Din			: in		std_logic_Vector( 15 downto 0 );
			AC_Out		: buffer 	std_logic_Vector( 15 downto 0 ) );
	end component AC_COUPLE;
	-------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component AD9240 is
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK20		: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
			OTR			: in		std_logic;					-- Out of Range
			ADout		: in		std_logic_vector( 13 downto 0 );	-- A/D Data Output
			raw_ad		: buffer	std_Logic_Vector( 15 downto 0 );
			CLK_FAD		: buffer	std_logic;					-- Clock to A/D Converter
			Dout			: buffer	std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
			Max			: buffer	std_logic_Vector( 15 downto 0 );
			Min			: buffer	std_logic_vector( 15 downto 0 ) );
	end component AD9240;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component ads8321 is 
		port( 
			IMR			: in		std_Logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Master Clock					
			AD_Dout		: in		std_logic;					-- A/D Data Output from ADS8321
			AD_Start		: in		std_logic;					-- Start Command from DSP
			AD_CS		: buffer	std_logic;					-- Chip Select to ADS8321
			AD_CLK		: buffer	std_logic;	 				-- clock to ADS8321
			AD_BUSY		: buffer	std_Logic;					-- Busy Indication
			ADATA		: buffer	std_logic_Vector( 15 downto 0 ));	-- ADS8321 Parallel Data
	end component ads8321;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component BIT_DAC is 
		port( 
		     IMR            : in      std_logic;				 	-- Master Reset
	     	clk20          : in      std_logic;                     	-- Master Clock
		     peak1     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 1 Value
	     	peak2     	: in      std_logic_vector( 11 downto 0 ); 	-- Peak 2 Value
		     cps       	: in      std_logic_vector( 15 downto 0 );	-- CPS Value
	     	pk2dly    	: in      std_logic_vector( 15 downto 0 );   -- Peak2 Delay Ver 523
			BIT_DATA		: buffer	std_logic_vector( 15 downto 0 ) );	-- BIT DAC Data
	end component BIT_DAC;
     -----------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	component fir_CNTRATE 
	     port( 
			IMR       		: in      std_logic;								-- Master Reset
	          CLK20			: in      std_logic;                                   	-- Master Clock
			OTR				: in		std_Logic;
			IFDISC	 		: in		std_logic;
			IPD_END			: in		std_logic;
			MOVE_FLAG			: in		std_logic;
			Video_Abort		: in		std_logic;
			PEAK_DONE			: in		std_logic;
			PA_Reset			: in		std_logic;
			Time_Clr			: in		std_logic;								-- Clear Clock Time and Live Time
			Time_Start		: in		std_logic;
			Time_Stop			: in		std_logic;
			PRESET_MODE		: in		std_logic_Vector(  3 downto 0 );
			PRESET			: in		std_logic_Vector( 31 downto 0 );
			ROI_SUM			: in		std_logic_vector( 31 downto 0 );
			Time_Clear		: buffer	std_logic;
			Time_Enable		: buffer	std_logic;
			CPS				: buffer	std_logic_vector( 20 downto 0 );		-- Input Count Rate
			NPS				: buffer	std_logic_Vector( 20 downto 0 );		-- Output Count Rate
			CTIME			: buffer	std_logic_Vector( 31 downto 0 );
			LTIME			: buffer	std_logic_Vector( 31 downto 0 );
			net_cps			: buffer	std_logic_Vector( 31 downto 0 );
			net_nps			: buffer	std_logic_Vector( 31 downto 0 );
			Preset_Out		: buffer	std_logic;
			ms100_tc			: buffer	std_logic;
			LS_Abort			: buffer	std_logic;
			LS_MAP_EN			: buffer	std_logic;
			WDS_BUSY			: buffer	std_logic );
	end component fir_cntrate;
	-------------------------------------------------------------------------------


     -----------------------------------------------------------------------------------
	component DAC_INT
		port(
			IMR			: in		std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- Mater Clock
			WE_ADR_DISC1	: in		std_logic;
			WE_ADR_DISC2	: in		std_logic;
			WE_ADR_EVCH	: in		std_logic;
			WE_ADR_BIT	: in		std_logic;
			BIT_DAC_LD	: in		std_logic;
			BIT_DAC_DATA	: in		stD_logic_Vector( 11 downto 0 );
			DISC1		: in		std_logic_Vector( 11 downto 0 );
			DISC2		: in		std_logic_Vector( 11 downto 0 );
			EVCH 		: in		std_logic_Vector( 11 downto 0 );
			BIT   		: in		std_logic_Vector( 11 downto 0 );
			DA_OFS		: buffer	std_logic;
			DA_CLK		: buffer	std_logic;					-- DAC Clock
			DA_DOUT		: buffer	std_logic;					-- DAC Data
			DA_UPD		: buffer	std_logic );					-- DAC Load
	end component DAC_INT;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component DSPINT 
	     port( 	
			IMR     		: in      std_logic;					-- Master Reset
			CLK20		: in		std_logic;					-- master Clock
			clk40 		: in		std_logic;					-- Master 40Mhz clock
			DSP_WR		: in		std_logic;					-- DSP Write Strobe
			DSP_STB		: in		std_logic_vector(   3 downto  0 );	-- DSP Data Select Strobes
			DSP_A		: in		std_logic_Vector(  15 downto 13 );	-- DSP Address bits 15-13
			DSP_A1		: in		std_logic_Vector(   7 downto  0 ); -- DSP Address bits 7-0
			DSP_D		: in		std_logic_vector(  15 downto  0 );	-- DSP Data Bus
			FF_FF		: in		std_logic;
			FF_DONE		: in		std_logic;
			DSO_ENABLE	: in		std_logic;
			int_en		: in		std_logic;
			ms100_tc		: in		std_logic;
			STAT_OE		: buffer	std_logic;					-- FIFO Status Output Enable
			FF_OE		: buffer	std_logic;					-- FIRO Output Enable
			FF_REN		: buffer	std_logic;					-- FIFO Read Enable
			FF_RCK		: buffer	std_logic;					-- FIFO Read Clock
			DSP_READ		: buffer	std_logic;					-- DSP Read Signal, Enable tri-state buffers
			DSP_IRQ		: buffer	std_logic;
			DSP_WD		: buffer	std_Logic_Vector(  15 downto 0 );	-- Registerd DSP Write Data Bus
			WE_DCD		: buffer 	std_logic_Vector( 127 downto 0 )); -- DSP Write Decode
     end component DSPINT;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component FIFO 
		port( 
			IMR				: in		std_logic;					-- Master Reset
			CLK20			: in		std_logic;					-- Master Clock
			FF_FF			: in		std_logic;					-- FIFO Full Flag
			LS_MAP_EN			: in		std_Logic;
			WE_FIFO_HI		: in		std_logic;					-- FIFO Write Hi
			BIT_ENABLE		: in		std_logic;					-- BIT Mode Enable
			DSO_Enable		: in		std_logic;					-- DSO Mode Enable
			Time_Enable		: in		std_logic;					-- Acquisition Enabled
			Blevel_En			: in		std_logic;					-- Baseline Histogram Mode
			Meas_Done			: in		std_logic;					-- Measurement Done - Normal pulse processing
			Preset_out		: in		std_logic;					-- Preset Done	- Normal pulse processing
			Blevel_Upd		: in		std_logic;					-- Baseline Level Update - during Normal Pulse Processing
			WE_DSO_START		: in		std_logic;					-- DSO Start
			PBusy			: in		std_logic;					-- Pulse Busy	- DSO
			WE_PRESET_DONE_CNT	: in		std_logic;
			DSO_TRIG_VEC 		: in		std_logic_Vector( 5 downto 0 );
			DSO_Trig			: in		std_logic_vector(  3 downto 0 );	-- DSO Trigger Select
			DSO_INT			: in		std_logic_Vector( 15 downto 0 );	-- DSO Sampling Interval
			TEST_REG			: in		std_logic_Vector( 17 downto 0 );
			CPEAK			: in		std_logic_vector( 11 downto 0 );	-- Current Peak Value
			BLEVEL			: in		std_logic_vector(  7 downto 0 );	-- Baseline Level
			Debug			: in		std_logic_vector( 15 downto 0 );	-- DSO Data Port
			Preset_DOne_Cnt	: buffer	std_logic_Vector( 15 downto 0 );
			FF_DONE			: buffer	std_logic;					-- FIFO Write Done
			FF_CNT			: buffer	std_logic_Vector( 15 downto 0 );
			FF_WR			: buffer	std_logic;					-- FIFO Write Signal
			FF_WDATA			: buffer	std_logic_vector( 17 downto 0 ) );	-- FIFO Data
	end component FIFO;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	component LTC1595 
		port( 
		     IMR            : in      std_logic;				     -- Master Reset
			CLK20		: in		std_logic;
			Data_Ld		: in		std_logic;			-- Strobe to Load the Data
			Data			: in		std_logic_vector( 15 downto 0 );
			DAC_LD		: buffer	std_logic;			-- Signal indicating data Load is Done
			DAC_CLK		: buffer	std_logic;			-- DAC clock
			DAC_DATA		: buffer	std_logic );			-- DAC Data
	end component LTC1595;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	Component TMP04 
		port( 
			IMR            : in      std_logic;				     -- Master Reset
		     CLK20          : in      std_logic;                         -- Master Clock
     		TMP_SENSE      : in      std_logic;                         -- Pipelined   
		     TMP_LO         : buffer  std_logic_vector( 15 downto 0 );	-- Temperature Low
		     TMP_HI		: buffer	std_logic_vector( 15 downto 0 ));	-- Temperature Hi
	end component TMP04;
     -----------------------------------------------------------------------------------

  	-------------------------------------------------------------------------------
	component wds is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			Time_Clr		: in		std_logic;
			Time_Enable	: in		std_logic;		
			Memory_Access	: in		std_logic;
			ROI_WE		: in		std_logic;
			DSP_D		: in		std_logic_vector( 15 downto 0 );	-- DSP Data Bus
			DSP_RAMA		: in		std_logic_Vector(  7 downto 0 );	-- DSP Address Bus
			Disc_en		: in		std_logic;
			Din			: in		std_logic_vector( 15 downto 0 );
			Tlevel		: in		std_logic_vector( 15 downto 0 );
			BLEVEL		: buffer	std_logic_vector(  7 downto 0 );
			BLEVEL_UPD	: buffer	std_logic;
			FDISC		: buffer	std_logic;
			Meas_Done		: buffer	std_logic;
			PBusy		: buffer	std_Logic;
			ROI_SUM		: buffer	std_logic_vector( 31 downto 0 );
			ROI_LU		: buffer	std_logic_Vector( 15 downto 0 );	-- ROI Lookup Data
			Dout			: buffer	std_logic_Vector( 15 downto 0 );
			CPeak		: buffer	std_Logic_Vector( 13 downto 0 ) );
	end component wds;
	-------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
     -- End of Component Declarations --------------------------------------------------
     -----------------------------------------------------------------------------------
begin
     -----------------------------------------------------------------------------------
	-- Altera PLL megafunctions used to take the reference input clock
	-- and generate two internal clocks based on the input reference clock
	CLK_GEN0 : altclklock
		generic map(
			inclock_period 		=> 50000,
			clock0_boost 			=> 1,
			clock1_boost 			=> 2,
			operation_mode 		=> "NORMAL",
			intended_device_family	=> "APEX20KE",
			valid_lock_cycles 		=> 1,
			invalid_lock_cycles 	=> 5,
			valid_lock_multiplier 	=> 1,
			invalid_lock_multiplier 	=> 5,
			clock0_divide 			=> 1,
			clock1_divide 			=> 1,
			outclock_phase_shift 	=> 0,
			lpm_type 				=> "altclklock" )
		port map(
			inclock 				=> clk20_IN(1),	-- clk1p 		(pin 31 )
			clock0 				=> clk20,			-- clk1out 	(pin 23)
			clock1 				=> CLK40 );		-- Internal Node

     -----------------------------------------------------------------------------------
	-- Start of EDAX Defined Component INSTANTIATIONS ---------------------------------
     -----------------------------------------------------------------------------------
     -----------------------------------------------------------------------------------
	-- Fast A/D Converter Interface - Taylored for WDS
	U_AD9240 : AD9240
		port map(
			IMR					=> IMR,					-- Master Reset
			clk20				=> clk20,					-- Master Clock
			OTR					=> OTR,					-- A/D Out-of-Range
			Adout				=> not( adata ), 			-- Inverted A/D Data
			CLK_FAD				=> CLK_FAD,				-- Convert Clock to Fast A/D (10Mhz)
			Raw_ad				=> debug_AD, 
			Dout					=> AD_DATA,				-- Converted Data	(20Mhz Rate)
			Max					=> AD_Max,
			Min					=> AD_Min );
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- BIT DAC Data Generator - Specifically for WDS
	bitdac_gen : if( fast_gen = 0 ) generate
		U_BITDAC : BIT_DAC
			port map(
				IMR					=> IMR,					-- Master Reset
				CLK20				=> clk20,					-- 20Mhz Clock
		     	peak1     			=> BIT_Peak1,				-- Peak 1 Size
		  		peak2     			=> BIT_Peak2,				-- Peak 2 Size
			     cps       			=> BIT_CPS,				-- CPS Time
   				pk2dly    			=> BIT_Pk2D,				-- Peak2 Delay Time
				BIT_DATA				=> BIT_DATA );			-- Generated Data
	end generate;

     -----------------------------------------------------------------------------------
	-- Multiplex the input data ( ADC or BIT Data )
	ad_data_mux	<= BIT_DATA	when ( CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) = PA_BITD ) else 
				   AD_DATA;

     -----------------------------------------------------------------------------------
	-- Eliminate any DC component to the data stream
	U_AC_COUPLE : AC_couple
		port map(
		     imr   	    			=> Imr,							-- Master Reset
			clk       			=> clk20,							-- Master Clock
			reset				=> WE_DCD( iADR_FIR_MR ), 			-- Filter Reset
			Din					=> AD_DATA_Mux,					-- INput Data (Inverted/Non-Inverted)
			AC_Out				=> ad_ac_data );					-- Max Detected Input

     -------------------------------------------------------------------------------
	-- Filter the data via WDS methods and determine the Pulse Height 
	U_WDS : wds
		port map(
			imr				=> imr,
			clk				=> clk20,
			Time_Clr			=> Time_Clear,
			Time_Enable		=> Time_Enable,
			Memory_Access		=> Cword( CW_MEMORY_ACCESS ),
			ROI_WE			=> WE_DCD( iADR_ROI_LU ),
			DSP_D			=> DSP_WD,
			DSP_RAMA			=> DSP_RAMA( 7 downto 0 ),
			Disc_en			=> Disc_En(4),
			Din				=> ad_ac_data,				-- AC Coupled Input
			Tlevel			=> tlevel3,				-- BLM Threshold Level
			BLEVEL			=> BLEVEL,
			BLEVEL_UPD		=> BLEVEL_UPD,
			fdisc			=> FDISC,					-- For CPS
			Meas_Done			=> Meas_Done,				-- for NPS
			PBUSY			=> PBUSY,
			ROI_SUM			=> ROI_SUM,
			ROI_LU			=> ROI_LU_Data,
			Dout				=> WDS_Dout,
			Cpeak			=> CPeak );		

     -------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- BIT A/D Interface
	-- Documented 7/24/03 - MCS
	ads8321_gen : if( fast_gen = 0 ) generate
		U_ADS8321 : ads8321
			port map(
				IMR					=> IMR,					-- Master Reset
				clk20				=> clk20,					-- Master Clock
				AD_Dout				=> AD_Dout,				-- A/D Input Data from ADS8321
				AD_Start				=> WE_DCD( iADR_BIT_ADC ),  	-- Start Command from DSP
				AD_CS				=> AD_CS,					-- Chip Select to ADS8321
				AD_CLK				=> AD_CLK,				-- Clock to ADS8321
				AD_BUSY				=> AD_BUSY,				-- Busy Indication
				ADATA				=> BIT_ADATA );			-- ADS8321 Parallel Data
	end generate;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- DAC8420 Interface
	-- Documented 7/28/03 - MCS
	dac_gen : if( fast_gen = 0 ) generate
		U_DAC_INT : DAC_INT
			port map(
				IMR					=> IMR,								-- Master Reset
				CLK20				=> clk20,								-- Master Clock
				WE_ADR_DISC1			=> WE_DCD( iADR_DISC1+0),	
				WE_ADR_DISC2			=> WE_DCD( iADR_DISC1+1),	
				WE_ADR_EVCH			=> WE_DCD( iADR_DISC1+2),	
				WE_ADR_BIT			=> WE_DCD( iADR_DISC1+3),	
				BIT_DAC_LD			=> '0',
				BIT_DAC_DATA			=> conv_Std_logic_vector( 0, 12 ),
				DISC1				=> DAC( 11 downto  0 ),
				DISC2				=> DAC( 23 downto 12 ),
				EVCH					=> DAC( 35 downto 24 ),
				BIT					=> DAC( 47 downto 36 ),
				DA_OFS				=> DA_OFS,							-- DAC Busy output
				DA_CLK				=> DA_CLK,							-- DAC Clock
				DA_DOUT				=> DA_DOUT,							-- DAC Data
				DA_UPD				=> DA_UPD );							-- DAC Load Strobe
	end generate;
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- DSP Interface (TI TMS320C32 )
	-- Documented 7/28/03 - MCS
	U_DSPINT : DSPINT
		port map(
			IMR       			=> IMR,			-- Master Reset
			CLK20				=> clk20,			-- Master clock
			clk40				=> clk40,			-- Master 40Mhz clock
			DSP_WR				=> DSP_WR,		-- DSP Write Strobe
			DSP_STB				=> DSP_STB,		-- DSP Dataselect Strobes
			DSP_A				=> DSP_A,			-- DSP Address Bus -bits 15:13
			DSP_A1				=> DSP_A1,		-- DSP Address bus -bits 7:0
			DSP_D				=> DSP_D,			-- DSP DAta Bus
			ff_ff				=> ff_ff,
			ff_done				=> ff_done,
			DSO_ENABLE			=> CWORD( CW_DSO_EN ),
			int_en				=> Int_En(0),
			ms100_tc				=> ms100_tc,
			STAT_OE				=> STAT_OE,		-- FIFO Status Output Enable
			FF_OE				=> FF_OE,			-- FIFO Output Enable
			FF_REN				=> FF_RD,			-- FIFO Read Enable
			FF_RCK				=> FF_RCK,		-- FIFO Read Clock
			DSP_READ				=> DSP_READ,		-- DSP Read signal, enable tri-state buffers
			DSP_IRQ				=> DSP_IRQ,
			DSP_WD				=> DSP_WD,		-- Registered DSP Write DAta Bus
			WE_DCD				=> WE_DCD );		-- DSP Decoded Vector
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- FIFO Interface
	-- Documented 7/28/03 - MCS
	U_FIFO : FIFO
		port map(
			IMR					=> IMR,						-- Master Reset
			CLK20				=> clk20,						-- Master Clock
			FF_FF				=> FF_FF,						-- FIFO FUll Flag
			LS_MAP_EN				=> LS_MAp_En,
			WE_FIFO_HI			=> WE_DCD( iADR_FIFO_HI ),		-- DSO Write to FIFO
			BIT_ENABLE			=> CWORD( CW_BIT_ENABLE ),		-- BIT Mode Enable
			DSO_Enable			=> CWORD( CW_DSO_EN ),					-- DSO Mode Enable
			Time_Enable			=> Time_Enable,				-- TIme Enable
			Blevel_en				=> CWORD( CW_BLEVEL_EN ),		-- Baseline Histogram Mode
			Meas_Done				=> MEas_Done,					-- Measurement Done
			Preset_out			=> Preset_out,					-- Preset TIme Done
			Blevel_Upd			=> blevel_upd,					-- Baseine Level Update
			WE_DSO_START			=> WE_DCD( iADR_DSO_START ),		-- Start DSO Mode
			PBusy				=> PBusy,						-- Used for DSO
			WE_Preset_Done_Cnt		=> WE_DCD( iADR_Preset_Done_Cnt ),	
			DSO_TRIG_VEC			=> DSO_TRIG_VEC,				-- DSO Trigger Vector
			DSO_Trig				=> DSO_Trig,					-- DSO Trigger Code
			DSO_INT				=> DSO_INT,					-- DSO Sample Interval
			TEST_REG				=> TEST_REG( 17 downto 0 ),		-- Test Register used for DSP-FIFO Write
			CPEAK				=> CPeak( 11 downto 0 ),			-- Converted Peak Value
			BLEVEL				=> Blevel,					-- Baseline Level
			Debug				=> Debug,						-- DSO data mux
			Preset_DOne_Cnt		=> Preset_Done_Cnt,
			FF_DONE				=> FF_DONE,					-- FIFO Sequence Done
			FF_CNT				=> FF_DSO_CNT,
			FF_WR				=> FF_WR,						-- FIFO Write Signal
			FF_WDATA				=> FF_WData );					-- FIFO Data Signal
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- CountRate related parameters, as well as clock time and live time
	U_CNTRATE : FIR_CNTRATE
		port map(
			IMR				=> IMR,					-- Master Reset
			CLK20			=> clk20,					-- 10Mhz Clock
			OTR				=> OTR,
			IFDISC	 		=> FDISC,					-- Fast Discriminator Input
			IPD_END			=> IPD_END,				-- Interpixel Delay End
			MOve_Flag			=> WDS_MF,				-- WDS Moving Flag
			Video_Abort		=> Video_Abort,			-- Video Abort Signal
			PEAK_DONE			=> Meas_Done,				-- Peak Meas Done
			PA_Reset			=> '0',					-- Not used
			Time_Clr			=> WE_DCD( iADR_TIME_CLR ),				-- Clear Clock and Live Time
			Time_Start		=> WE_DCD( iADR_TIME_START ),				-- Start Acquisition Timer
			Time_Stop			=> WE_DCD( iADR_TIME_STOP ),				-- Time Stop
			Preset_Mode		=> Preset_Mode,			-- Preset Mode
			Preset			=> Preset,				-- Preset Value
			ROI_Sum			=> ROI_Sum,				-- Accumulated ROI Sum
			Time_Clear		=> Time_Clear,
			Time_Enable		=> Time_Enable,			-- Acquisition Enabled
			CPS 				=> CPS,					-- Input Count Rate
			NPS				=> NPS, 					-- Output Count Rate
			CTIME			=> CTIME,					-- Clock Time
			LTIME			=> LTIME,					-- Live Time
			net_cps			=> net_cps,				-- Net Input Counts
			net_nps			=> net_nps,				-- Net Output Counts
			Preset_Out		=> Preset_Out,				-- Preset DOne Flag
			ms100_tc			=> ms100_tc,				-- 100ms Timer
			LS_Abort			=> LS_Abort,
			LS_MAP_EN			=> LS_MAP_EN,
			WDS_Busy			=> WDS_Busy );				-- WDS Status Move Flag
     -------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- Interface to the LTC1595 SDAC used for the Coarse Gain
	--	Documented 7/28/03 - MCS
	U_LTC1595	: LTC1595
		port map(
		     IMR           		=> IMR,
			CLK20			=> clk20,
			Data_Ld			=> WE_DCD( iADR_CGain ),
			Data				=> CGain,
			DAC_LD			=> CG_LD,
			DAC_CLK			=> CG_CLK,
			DAC_DATA			=> CG_Data );
     -----------------------------------------------------------------------------------

     -----------------------------------------------------------------------------------
	-- Temperature Sensor interface to the Analog Devices TMP04
	-- Instance 0 is for the 0n-board
	-- Instance 1 is for the 204 PreAmp
	-- Documentd 7/28/03
	-- The TMP_SENSE from the PreAmp is inverted, so invert it here to correct the Thi/Tlo
	tmp_gen : if( fast_gen = 0 ) generate
	     U_TMP04 : TMP04
			port map(
          	 	IMR            => IMR,							-- Master Reset
       	     	CLK20          => clk20,							-- Master Clock
	  	    		TMP_SENSE      => TMP_SENSE(0),					-- Temperature In
     	        	TMP_LO    	=> TMP_LO,						-- Temperature PW Lo
          	   	TMP_HI		=> TMP_HI);						-- Tempeature PW Hi
	end generate;
     -----------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- END of EDAX Defined Component INSTANTIATIONS -----------------------------------
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- Start of ALTERA LPM PRIMATIVE INSTANTIATIONS -----------------------------------
     ----------------------------------------------------------------------------------------------

	CWORD_LO_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_CW ),
			data			=> DSP_WD,
			q			=> CWORD( 15 downto 0 ) );


	tlevel_ff : lpm_ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_DLEVEL3 ),
			data			=> DSP_WD,
			q			=> TLEVEL3 );
	
	PRESET_MODE_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH 	=> 4)
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_PRESET_MODE ),
			data			=> DSP_WD( 3 downto 0 ),
			q			=> PRESET_MODE );

	DSP_RAMA_CNTR : LPM_FF
		generic map(
			LPM_WIDTH		=> 16,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,					-- Asynchronous Clear
			clock		=> clk20,					-- Master Clock
			enable		=> WE_DCD( iADR_RAMA ),		-- Synchronous Load
			data			=> DSP_WD,				-- Load Data
			q			=> DSP_RAMA );				-- COunter Output
			
	TP_SEL_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,					-- DSP Write Clock
			enable		=> WE_DCD( iADR_TP_SEL ),
			data			=> DSP_WD,
			q			=> TP_SEL );

		
	CGain_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			enable		=> WE_DCD( iADR_CGain ),
			data			=> DSP_WD,
			q			=> CGain );


	Disc_En_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 6 )
		port map(
			aclr			=> IMR,
			clock		=> clk20,
			Enable		=> WE_DCD( iAdr_Disc_En),
			data			=> DSP_WD( 5 downto 0 ),
			q			=> Disc_En );
			
	DSO_TRIG_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 4 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iadr_dso_trig ),
			data			=> dsp_wd( 3 downto 0 ),
			q			=> dso_trig );

	DSO_INT_FF : LPM_FF
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_DSO_INT ),
			data			=> dsp_wd,
			q			=> DSO_INT );
			
	INT_EN_REG_FF : lpm_Ff
		generic map(
			LPM_TYPE		=> "LPM_FF",
			LPM_WIDTH		=> 2 )
		port map(
			aclr			=> imr,
			clock		=> clk20,
			enable		=> we_dcd( iADR_INT_EN ),
			data			=> DSP_WD( 1 downto 0 ),
			q			=> INT_EN );

	DET_TYPE_FF : LPM_FF
		generic map(
			LPM_WIDTH		=> 4,
			LPM_TYPE		=> "LPM_FF" )
		port map(
			aclr			=> IMR,						-- Master Reset
			clock		=> clk20,					-- DSP Clock
			enable		=> WE_DCD( iADR_DET_TYPE ),
			data			=> DSP_WD( 3 downto 0 ),
			q			=> DET_TYPE );

	two_loop_a : for i in 0 to 1 generate
		PRESET_LO_FF : LPM_FF
			generic map(
				LPM_WIDTH		=> 16,
				LPM_TYPE		=> "LPM_FF" )
			port map(
				aclr			=> IMR,						-- Master Reset
				clock		=> clk20,					-- DSP Clock
				enable		=> WE_DCD( iADR_PRESET_LO+i ),
				data			=> DSP_WD,
				q			=> PRESET( (16*i)+15 downto 16*i ) );
	end generate;

     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- Full set of Parameters ( not used for "fast synthesis )
	full_par_gen : if( fast_gen = 0 ) generate
		BIT_PEAK1_FF : lpm_ff
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 12 )
			port map(
				aclr			=> IMR,
				clock		=> clk20,
				enable		=> WE_DCD( iADR_BIT_PEAK1 ),
				data			=> DSP_WD( 11 downto 0 ),
				q			=> BIT_PEAK1 );
				
		BIT_PEAK2_FF : lpm_ff
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 12 )
			port map(
				aclr			=> IMR,
				clock		=> clk20,
				enable		=> WE_DCD( iADR_BIT_PEAK2 ),
				data			=> DSP_WD( 11 downto 0 ),
				q			=> BIT_PEAK2 );
				
		BIT_CPS_FF : lpm_ff
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 16 )
			port map(
				aclr			=> IMR,
				clock		=> clk20,
				enable		=> WE_DCD( iADR_BIT_CPS ),
				data			=> DSP_WD,
				q			=> BIT_CPS );
		
		BIT_PK2D_FF : lpm_ff
			generic map(
				LPM_TYPE		=> "LPM_FF",
				LPM_WIDTH		=> 16 )
			port map(
				aclr			=> IMR,
				clock		=> clk20,
				enable		=> WE_DCD( iADR_BIT_PK2D ),
				data			=> DSP_WD,
				q			=> BIT_PK2D );

		two_loop_b : for i in 0 to 1 generate
			TEST_REG_FF_LO : LPM_FF
				generic map(
					LPM_WIDTH		=> 16,
					LPM_TYPE		=> "LPM_FF" )
				port map(
					aclr			=> IMR,
					clock		=> clk20,					-- DSP Clock
					enable		=> WE_DCD( iADR_FIFO_LO+i ),
					data			=> DSP_WD,
					q			=> TEST_REG( (16*i)+15 downto 16*i ) );
					
		end generate;

		four_loop : for i in 0 to 3 generate
			DAC_FF : LPM_FF
				generic map(	
					LPM_WIDTH		=> 12 )
				port map(
					aclr			=> IMR,
					clock		=> clk20,
					enable		=> WE_DCD( iADR_DISC1+i ),
					data			=> DSP_WD( 11 downto 0 ),
					q			=> DAC( 11+(i*12) downto i*12 ));
		end generate;			
	end generate;
	-- Full set of Parameters ( not used for "fast synthesis )
     ----------------------------------------------------------------------------------------------

     ----------------------------------------------------------------------------------------------
	-- END of ALTERA LPM PRIMATIVE INSTANTIATIONS ------------------------------------------------
     ----------------------------------------------------------------------------------------------
	IMR			<= not( OMR );
	RTEM_ANALYZE	<= '1';
	RTEM_RETRACT	<= '1';
	SCA			<= x"FF";
	IHV_Dis		<= "00";
	Disc_Inv(0) 	<= '0';				-- for WDS
	Disc_Inv(1) 	<= '1';				-- for WDS
	PA_Mode(3) 	<= '1';				-- Gain x2
	DISC_EVCH		<= "00";

	IPD_END		<= not( RT_OUT(1) );
	VIDEO_ABORT	<= not( RT_OUT(0) ) when ( Cword( CW_VIDEO_THR_MODE ) = '1' ) else '0';

	-- Control Word Parameters -------------------------------------------------------------------

	-- Input Steering Mux
	with CWORD( CW_PA_SOURCE_HI downto CW_PA_SOURCE_LO ) select
		PA_MODE( 2 downto 0 ) <= 
				   "001" when PA_DET0,			-- DET1
				   "010" when PA_DET1,			-- DET2
				   "100" when others; 			-- BIT DAC Analog and Digital

	
	WDS_MF 		<= not( TMP_SENSE(1) );

	-- WDS Mode, invert the OHV_ENABLE signal	
	HV_Enable		<= not( OHV_ENABLE   );

	-- FIFO Interface ----------------------------------------------------------------------------
	-- FIFO Master Reset -----------------------------------------------------
	FF_RS  	<= '0' when ( IMR 				= '1' ) else
		        '0' when ( WE_DCD( iADR_FIFO_MR ) = '1' ) else '1';
	
	-- FIFO Interface ----------------------------------------------------------------------------

	-- Real Time Bus to the Scan Generator ------------------------------------------------------
	RT_IN(0)		<= '0';			-- Unused, when 0 - Output is tristate due to open collector
	RT_IN(1)		<= Time_Enable;	
	-- Real Time Bus to the Scan Generator ------------------------------------------------------

	-- DSP Read Back Mux -------------------------------------------------------------------------
	-- The two additional DSP Bits to output the FIFO Flag Bits when the FIFO Data is read
	DSP_DATA_EN	<= '1' when ( DSP_READ = '1' ) or ( STAT_OE = '1' ) else '0';
	
				-- Status, generated data
	status_oe <= '1' when ( conv_integer( DSP_A1 ) = iADR_STAT ) else '0';
	
	Status_Vec <= 	WDS_Busy				-- Bit 15		-- Ver 3565
				& OTR				-- Bit 14
				& '0'				-- Bit 13
				& '0'				-- Bit 12
				& '0'				-- Bit 11
				& '0'				-- Bit 10
				& '0'				-- Bit 9
				& '0'				-- Bit 8
				& '0'				-- Bit 7
				& not( DA_OFS )		-- Bit 6
				& HV_Enable(1)			-- Bit 5
				& HV_Enable(0)			-- Bit 4
				& Time_Enable 			-- Bit 3
				& AD_BUSY				-- Bit 2
				& not( FF_FF )			-- Bit 1
				& not( FF_EF );		-- Bit 0

	DSP_D	<= iDSP_D_MUX 		when ( DSP_DATA_EN = '1' ) else 
	        	   "ZZZZZZZZZZZZZZZZ";

	iDSP_D_Mux	<= Status_Vec 	 when ( STAT_OE = '1' ) or ( Status_OE = '1' ) else
				   iDSP_D;

	with CONV_INTEGER( DSP_A1 ) select
	    iDSP_D <= CONV_STD_LOGIC_VECTOR( Ver, 16 ) 					when iADR_ID,
				TEST_REG( 15 downto 0 )							when iADR_FIFO_LO,
				TEST_REG( 31 downto 16 )							when iADR_FIFO_HI,
				CWORD( 15 downto 0 ) 							when iADR_CW,
				PRESET( 15 downto 0 )							when iADR_PRESET_LO,
				PRESET( 31 downto 16 )							when iADR_PRESET_HI,
				ZEROES( 15 downto 4 ) & PRESET_MODE				when iADR_PRESET_MODE,
				DSP_RAMA										when iADR_RAMA,
				TP_SEL										when iADR_TP_SEL,
				x"0" & DAC( 11 downto 0 )						when iADR_DISC1,
				x"0" & DAC( 23 downto 12 )						when iADR_DISC2,
				x"0" & DAC( 35 downto 24 )						when iADR_EVCH, 
				x"0" & DAC( 47 downto 36 )						when iADR_BIT, 
				CGain										when iADR_CGain,
				Zeroes( 15 downto 6 ) & Disc_En					when iADR_Disc_En,
				TLEVEL3			 							when iADR_DLEVEL3,
				Zeroes( 15 downto 2 ) & INT_EN 					when iADR_INT_EN,
			   	TMP_LO										when iADR_TLO,
				TMP_HI										when iADR_THI,
				x"0" & BIT_Peak1								when iADR_BIT_PEAK1,
				x"0" & BIT_Peak2								when iADR_BIT_Peak2,
				BIT_CPS										when iADR_BIT_CPS,
				bIT_PK2D										when iADR_BIT_PK2D,
				ROI_LU_DATA									when iADR_ROI_LU,
  				CPS( 15 downto 0 ) 								when iADR_IN_CPSL,
				ZEROES( 15 downto 5 ) & CPS( 20 downto 16 ) 			when iADR_IN_CPSH,
				NPS( 15 downto 0 ) 								when iADR_OUT_CPSL,
				zeroes( 15 downto 5 ) & NPS( 20 downto 16 ) 			when iADR_OUT_CPSH,								
				NET_CPS( 15 downto  0 ) 							when iADR_NET_CPSL,
				NET_CPS( 31 downto 16 ) 							when iADR_NET_CPSH,
				NET_NPS( 15 downto  0 ) 							when iADR_NET_NPSL,
				NET_NPS( 31 downto 16 ) 							when iADR_NET_NPSH,
				LTIME( 15 downto  0 ) 							when iADR_LTIME_LO,
				LTIME( 31 downto 16 ) 							when iADR_LTIME_HI,
				CTIME( 15 downto  0 ) 							when iADR_CTIME_LO,
				CTIME( 31 downto 16 ) 							when iADR_CTIME_HI,
				ROI_SUM( 15 downto 0 )							when iADR_ROI_SUML,
				ROI_SUM( 31 downto 16 )							when iADR_ROI_SUMH,				
				AD_MIN										when iADR_RAMP_LO,	-- Ver > 3580
				AD_MAX										when iADR_RAMP_HI,	-- Ver > 3580
				BIT_ADATA										when iADR_BIT_ADC,
				debug_ad										when iADR_FADC,
				zeroes( 15 downto 4 ) & dso_trig					when iADR_DSO_TRIG,
				DSO_INT										when iADR_DSO_INT,
				Preset_Done_Cnt								when iADR_Preset_Done_Cnt,
				FF_DSO_CNT									when iADR_FF_DSO_CNT,
				x"000" & DET_TYPE								when iADR_DET_TYPE,
				X"1971" when others;


	----- TP Output Mux --------------------------------------------------------------------------
	dso_sel	<= tp_sel( 5 downto 0 );

	ATP_SEL 	<= tp_sel( 11 downto 8 );

	
	with conv_integer( TP_SEL( 15 downto 12 )) select
	     TP <= 
			'0'    									-- Bit 7
			& DSP_IRQ									-- Bit 6
			& DSP_STB( 3 downto 0 )						-- Bits 5 downto 2
			& WE_DCD( iADR_Reset_Mr )					-- Bit 1 was 
			& WE_DCD( iADR_FIR_MR )			when 0,		-- Bit 0


			DAC( 26 downto 24 )							-- 7:5
			& WE_DCD( iADR_EVCH )						-- 4
			& DA_OFS									-- 3
			& DA_CLK									-- 2
			& DA_DOUT									-- 1
			& DA_UPD 						when 1,		-- 0	
						

			TIME_ENABLE								-- 7
			& MEAS_DONE								-- 6
			& DSP_IRQ									-- 5
			& INT_EN(0)								-- 4
			& FF_OE									-- 3
			& FF_RD									-- 2
			& FF_EF									-- 1
			& FF_WR						when 2,		-- 0

			-- THis Decode is used for the FIFO Interface Debugging
			FF_FF 									-- 7
			& FF_EF									-- 6
			& FF_RS									-- 5
			& FF_OE									-- 4
			& FF_RD									-- 3	
			& FF_WR									-- 2
			& FF_RCK									-- 1
			& DSP_READ					when 3,		-- 0
			-- THis Decode is used for the FIFO Interface Debugging
		
			-- This decode is used for the ADS8321 Interface Debugging		
			BIT_ADATA( 2 downto 0 )						-- 7:5
			& AD_BUSY									-- 4
			& WE_DCD( iADR_BIT_ADC )    					-- 3
			& AD_Dout									-- 2
			& AD_CS									-- 1
			& AD_CLK						when 4,   	-- 0
			-- This decode is used for the ADS8321 Interface Debugging		

	
			Preset_Out								-- 7
			& RT_IN(0)								-- 6 EDX_ANAL
			& LS_Abort								-- 5
			& RT_OUT(1)								-- 4 IPD_END
			& RT_OUT(0)								-- 3 VIDEO_ABORT
			& IPD_END									-- 2
			& VIDEO_ABORT								-- 1
			& TIme_Enable					when 6,		-- 0

			FF_WDATA( 17 downto 16 )						-- 7:6	FIFO Key
			& FF_WDATA( 3  downto  0 )					-- 5:1
			& Meas_Done
			& FF_WR						when 8,		-- 0
			
			-- This decode is used for the DAC8420 Interface Debugging		
			'0'										-- 7
			& '0'									-- 6
			& BIT_DATA( 1 downto 0 )						-- 5: 4
			& DA_CLK									-- 3
			& DA_DOUT									-- 2
			& DA_UPD									-- 1
			& DA_OFS						when 9,		-- 0
			-- This decode is used for the DAC8420 Interface Debugging		

			Debug( 7 downto 0 )				when 10,
			Debug( 15 downto 8 ) 			when 11,

		X"12" 		when others;

	---- TP Output Mux ------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- DSO Trigger
	-- Address: 2178
	-- Data: 
	--	0 = No Trigger
	--	1 = FDISC
	--	2 = PreAmp Reset    
	DSO_TRIG_VEC	<= "011" & '0' & FDISC	& "1";			-- Bits 5:0
	
	with conv_integer( DSO_SEL ) select
		Debug <=   
			not( ad_data(15)) & ad_data( 14 downto 0 ) 			when 0,	-- "Raw A/D"
			NOT( ad_data_mux( 15)) & ad_data_mux( 14 downto 0 ) 	when 1,	-- "A/D Data Mux"
			cpeak(13) & cpeak(13) & CPeak 					when 8,	-- "Captured Peak"
			ad_ac_data									when 9,	-- "Gaussian Stage 1" ( AC Coupled input 	)
			WDS_Dout										when 10,	-- "Gaussian Stage 2"
			x"0000" when others;

----------------------------------------------------------------------------------------
end BEHAVIORAL;          -- EP99841.VHD
----------------------------------------------------------------------------------------


