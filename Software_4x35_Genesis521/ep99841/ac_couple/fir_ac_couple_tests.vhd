------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_ac_couple.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules AC Couples the input bus only when the input is near baseline. When it is above baseline
--		the Baseline Correction is disabled, to prevent a downward shift of the output bus.
--		Those that are enabled and not blocked.
--	History
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity fir_ac_couple is 
	port( 	
	     imr       	: in      std_logic;                    -- Master Reset
		clk       	: in      std_logic;			     -- Master Clock Input
		reset		: in		std_logic;
		KSDD_Mode		: in		std_Logic;
		WDS_Mode		: in		std_Logic;
		Din			: in		std_logic_Vector( 15 downto 0 );
		AC_Out		: buffer 	std_logic_Vector( 15 downto 0 ) );
end fir_ac_couple;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_ac_couple is
	constant Acc_Width : integer := 24;
	
	signal Dif_reg_Mult : std_logic_vector( Acc_Width-1 downto 0 );

--	constant Acc_Width		: integer := 34;
	constant THR_SDD		: integer := 10; -- was 25;	-- Originally 30;
	constant THR_WDS		: integer := 10;
	constant THR_OTHER		: integer := 50;
--	constant Div_Bit 		: integer := 10;
--	constant Acc_Width		: integer := Div_Bit+16;	
	
	signal Vcc			: std_logic;
	signal Gnd			: std_logic;
	signal Din_Dly1		: std_logic_Vector( 15 downto 0 );
	signal Din_Dly2		: std_logic_Vector( 15 downto 0 );
	signal diff_reg		: std_logic_Vector( 15 downto 0 );
	signal Abs_diff		: std_logic_vector( 15 downto 0 );
	signal Threshold		: std_logic_vector( 15 downto 0 );

	signal abs_thr			: std_logic;
	signal Abs_Thr_Vec		: std_Logic_Vector(  7 downto 0 );
	signal offset_en		: std_logic;

--	signal din_vec		: std_logic_vector( 31 downto 0 );
--	signal DCorr		: std_logic_Vector( 31 downto 0 );
--	signal Acc 		: std_Logic_Vector( 31 downto 0 );
--	signal Acc_Mux		: std_logic_Vector( 31 downto 0 );
--	signal Acc_sum		: std_logic_Vector( 31 downto 0 );
--	signal Acc_dif		: std_logic_Vector( 31 downto 0 );


signal Din_vec		: std_logic_vector( Acc_Width-1 downto 0 );
signal DinD32		: std_logic_vector( Acc_Width-1 downto 0 );
signal DinSum		: std_logic_vector( Acc_Width-1 downto 0 );

signal Err_Mult 	: std_logic_Vector( Acc_Width-1 downto 0 );
signal Err		: std_logic_Vector( Acc_Width-1 downto 0 );
signal Err_Dif		: std_logic_Vector( Acc_Width-1 downto 0 );
signal Err_In		: std_logic_vector( Acc_Width-1 downto 0 );
signal Err_Sum		: std_logic_Vector( Acc_Width-1 downto 0 );

signal Dout_sum	: std_logic_Vector( Acc_Width-1 downto 0 );
signal Dout		: std_logic_Vector( Acc_Width-1 downto 0 );

--	constant Mult_Bit	: integer := 20;
--	constant Acc_Width	: integer := 16 + Mult_Bit;
--	signal Diff_Mult 	: std_logic_Vector( Acc_Width-1 downto 0 );
--	signal sum1		: std_Logic_Vector( Acc_Width-1 downto 0 );
--	signal sub1		: std_Logic_Vector( Acc_Width-1 downto 0 );
--	signal dif2		: std_Logic_Vector( Acc_Width-1 downto 0 );
--	signal dout		: std_Logic_Vector( Acc_Width-1 downto 0 );

begin
	Threshold	<= Conv_Std_logic_Vector( THR_SDD, 	16 ) when ( KSDD_Mode = '1' ) else
			   Conv_Std_logic_Vector( THR_WDS, 	16 ) when ( WDS_Mode = '1' ) else
			   conv_Std_logic_Vector( THR_OTHER, 	16 );

     --------------------------------------------------------------------------
	-- First Generate diff_regerence Vector from the Preset input and the previous input
	Vcc	<= '1';
	Gnd	<= '0';

	Din_Del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din,
			q				=> Din_Dly1 );
			
	Din_Del2_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din_Dly1,
			q				=> Din_Dly2 );

	data_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			add_sub			=> GND,	-- Subtract
			Cin				=> Vcc,
			dataa 			=> din_dly1,
			datab			=> din_dly2,
			result			=> diff_reg );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Threshold Comparison
	diff_reg_Abs : lpm_abs
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			data				=> diff_reg,
			result			=> abs_diff );
	
	abs_cmp : lpm_compare
		generic map(
			lpm_width			=> 16,
			LPM_representation	=> "signed" )
		port map(
			dataa			=> abs_diff,
			datab			=> Threshold,
			agb				=> abs_thr );
	-- First Generate diff_regerence Vector from the Preset input and the previous input
     --------------------------------------------------------------------------

-------------------------------------------------------------------------------
     --------------------------------------------------------------------------
--	-- Input/Offset difference scaled up by 16 bits -----------------------------
--	Din_Vec <= Diff_Reg & x"0000";
--
--	-- Accumulate Diff_Reg with Past Accumulate output	
--	Acc_Sum_as : lpm_add_Sub
--		generic map(
--			LPM_WIDTH			=> 32,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_sub			=> Vcc, 	-- Add
--			Cin				=> Gnd,	
--			dataa			=> Acc,
--			datab			=> Din_Vec,
--			result			=> Acc_Sum );
--		
--	-- Offset input mux
--	-- When above threshold, disable any adjustment to the offset
--	dca: for i in 0 to 23 generate
--		DCorr(i) <= Acc(i+8);
--	end generate;
--	
--	dcb: for i in 24 to 31 generate
--		DCorr(i) <= Acc(31);
--	end generate;
--
--	omgc : for i in 0 to 31 generate
--		acc_mux(i) <= DCorr(i) when ( offset_en = '0' ) else '0';
--	end generate;
--	
--	-- offset accumulator -------------------------------------------------------
--	acc_diff_as : lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> 32,
--			LPM_REPRESENTATION 	=> "SIGNED" )
--		port map(
--			add_Sub			=> Gnd,			-- Subtract
--			cin				=> Vcc,
--			dataa			=> acc_sum,
--			datab			=> acc_mux,
--			result			=> Acc_dif );
--		
--	-- Accumulator Flip-Flop;	
--	acc_ff : lpm_ff
--		generic map(
--			LPM_WIDTh			=> 32 )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			sclr				=> Reset,
--			data				=> Acc_dif,
--			q				=> acc );
--	-- offset accumulator -------------------------------------------------------
--	
--	-- Final output -------------------------------------------------------------
--	ac_out_ff : lpm_ff
--		generic map(
--			LPM_WIDTH			=> 16 )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			data				=> Acc( 31 downto 16 ),
--			q				=> Ac_out );
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
	-- Sign Extend Input ---------------------------------------------
--	Din_Vec_Gen : for i in 16 to Acc_Width-1 generate
--		Din_Vec(i) <= Din(15);
--	end generate;
--	Din_Vec( 15 downto 0 ) <= Din;
	-- Sign Extend Input ---------------------------------------------

	-- Now calculate Din/32 ------------------------------------------
--	DinD32A :	for i in 0 to Acc_Width-1-5 generate
--		DinD32(i) <= Din_Vec(i+5);
--	end generate;

--	DinD32B :	for i in Acc_Width-5 to Acc_Width-1 generate
--		DinD32(i) <= Din_Vec(Acc_Width-1);
--	end generate;
	-- Now calculate Din/32 ------------------------------------------

	-- Now calculate Din + Din/32 ------------------------------------
--	Din_Sum_as : lpm_add_Sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_Sub			=> Vcc,	-- Add
--			cin				=> Gnd,
--			dataa			=> Din_Vec,
--			datab			=> DinD32,
--			result			=> DinSum );	
	-- Now calculate Din + Din/32 ------------------------------------

	-- Now take the Difference ---------------------------------------
--	ED_AS : lpm_add_Sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_Sub			=> Gnd,	-- Subtract
--			cin				=> Vcc,
--			dataa			=> Din_Vec, -- DinSum,
--			datab			=> Err,
--			result			=> Err_Dif );
	-- Now take the Difference ---------------------------------------

--	EIG0 : for i in 0 to Acc_Width-1-Div_Bit generate
--		Err_In(i) <= Err_Dif(i+Div_Bit) when ( Offset_En = '0' ) else '0';
--	end generate;

--	EIG1 : for i in ( Acc_Width-1-Div_Bit) + 1 to Acc_Width-1 generate
--		Err_In(i) <= Err_Dif(Acc_Width-1) when ( Offset_En = '0' ) else '0';
--	end generate;
--	
--	Err_Sum_As : lpm_add_sub	
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_sub			=> Vcc,	-- Add
--			cin				=> Gnd,	
--			dataa			=> Err,
--			datab			=> Err_In,
--			result			=> Err_Sum );
--			
--	Err_ff : lpm_ff
--		generic map(
--			LPM_WIDTH			=> Acc_Width )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			sclr				=> reset,
--			data				=> Err_Sum,
--			q				=> Err );
--
--	Ac_Out_As : lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED",
--			LPM_PIPELINE		=> 1 )			
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			add_sub			=> Gnd,	-- Subtract
--			cin				=> Vcc,
--			dataa			=> Din_Vec,
--			datab			=> Err,
--			result			=> DOut );
--
--	AC_Out_FF : lpm_ff
--		generic map(
--			LPM_WIDTH			=> 16 )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			data				=> Dout( 15 downto 0 ),
--			q				=> AC_Out );
--
-------------------------------------------------------------------------------
--	Ac_Out_As : lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> 16,
--			LPM_REPRESENTATION	=> "SIGNED",
--			LPM_PIPELINE		=> 1 )			
---		port map(
--	--		aclr				=> imr,
--			clock			=> clk,
--			add_sub			=> Gnd,	-- Subtract
--			cin				=> Vcc,
--			dataa			=> Din,
--			datab			=> Err( 15 downto 0 ),
--			result			=> Ac_Out );

-- NG-- 
--	-- Mult Diff by 2^Mult_Bit
--	dmg0 : for i in 0 to Mult_Bit-1 generate
--		diff_mult( i ) <= '0';
--	end generate;
--	diff_mult( 15+Mult_Bit downto Mult_Bit ) <= diff_reg;
--	
--	sum1_as : lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_sub			=> Vcc,		-- Add
--			Cin				=> Gnd,
--			dataa			=> Dout,
--			datab			=> diff_mult,
--			result			=> Sum1 );
--
--	sub1_gen : for i in 0 to acc_Width-1 generate
--		sub1(i) <= dout(i) when ( offset_en = '0' ) else '0';
--	end generate;
--	
--	dif1_as: lpm_add_sub
--		generic map(
--			LPM_WIDTH			=> Acc_Width,
--			LPM_REPRESENTATION	=> "SIGNED" )
--		port map(
--			add_sub			=> Gnd,		-- Sub
--			Cin				=> Vcc,
--			dataa			=> sum1,
--			datab			=> sub1,
--			result			=> dif2 );
--			
--	dout_ff : lpm_ff
--		generic map(
--			LPM_WIDTH			=> Acc_Width )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			sclr				=> reset,
--			data				=> dif2,
--			q				=> dout );
--
--	AC_Out_FF : lpm_ff
--		generic map(
--			LPM_WIDTH			=> 16 )
--		port map(
--			aclr				=> imr,
--			clock			=> clk,
--			data				=> Dout( Mult_Bit+15 downto Mult_Bit ),
--			q				=> AC_Out );


	Dif_reg_Mult <= Diff_reg & x"00";
	
	
	Dout_add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			add_sub			=> Vcc,		-- Add
			cin				=> Gnd,
			dataa			=> Dout,
			datab			=> Dif_reg_Mult,
			result			=> Dout_Sum );
			
	Dout_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Width )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> reset,
			data				=> Dout_sum,
			q				=> Dout );
	ac_out	<= dout( Acc_Width-1 downto Acc_Width-1-15 );
    --------------------------------------------------------------------------
	-- Generate a Vector when it is OK to adjust the Baseline in the absense of an input pulse.
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			Abs_Thr_Vec 	<= x"00";
			offset_en		<= '0';
		elsif(( Clk'Event ) and ( CLK = '1' )) then
			Abs_Thr_Vec	<= Abs_Thr_Vec( 6 downto 0 ) & Abs_Thr;
			if( conv_integer( abs_thr_vec ) = 0 )
				then offset_en <= '0';
				else offset_en <= '1';
			end if;
		end if;
	end process;
-------------------------------------------------------------------------------
end behavioral;               -- fir_ac_couple
-------------------------------------------------------------------------------
	

