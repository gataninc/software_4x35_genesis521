	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

entity tb is
end tb;

architecture test_bench of tb is

	signal imr 		: std_Logic := '1';
	signal clk 		: std_Logic := '0';
	signal fir_mr		: std_Logic := '1';
	signal ksdd_mode	: std_logic := '1';
	signal wds_Mode	: std_logic := '0';
	signal din		: std_Logic_vector( 15 downto 0 );
	signal iac_out		: std_logic_vector( 15 downto 0 );
	signal ac_out		: std_logic_vector( 15 downto 0 );
	
	
-------------------------------------------------------------------------------
component ksdd_rom is
     port(
          imr 	: in std_logic;
          clk 	: in std_logic;
          dout : out std_logic_Vector( 15 downto 0 ) );
end component ksdd_rom      ;
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
component fir_ac_couple is 
	port( 	
      imr       	: in      std_logic;                    -- Master Reset
		clk       	: in      std_logic;			     -- Master Clock Input
		reset		: in		std_logic;
		Din			: in		std_logic_Vector( 15 downto 0 );
		AC_Out		: out 	std_logic_Vector( 15 downto 0 ) );
end component fir_ac_couple;
-------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
begin
	imr <= '0' after 555 ns;
	clk <= not( clk ) after 25 ns;
	fir_mr <= '0' after 999 ns;
	
	UR : ksdd_rom
		port map(
			imr		=> imr,			
			clk		=> clk,
			dout		=> din );

	U : fir_ac_couple
		port map(
			imr		=> imr,			
			clk		=> clk,
			reset	=> fir_mr,
			Din		=> din,
			AC_Out	=> iAc_Out );
-------------------------------------------------------------------------------

			
clock_proc : process( clk )
begin
	if(( clk'event ) and ( Clk = '0' )) then
		ac_out	 	<= iac_out;
	end if;
end process;

tb_proc : process
begin	
	wait for 800 us;
	assert false
		report "End of Simulation"
		severity failure;
end process;
	
	
------------------------------------------------------------------------------------
end test_bench;               -- fir_ksdd
------------------------------------------------------------------------------------
			
