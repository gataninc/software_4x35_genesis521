onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -format Logic /tb/imr
add wave -noupdate -format Logic /tb/clk
add wave -noupdate -format Logic /tb/fir_mr
add wave -noupdate -format Logic /tb/ksdd_mode
add wave -noupdate -format Logic /tb/wds_mode
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.01 /tb/din
add wave -noupdate -format Analog-Step -height 200 -radix decimal -scale 0.01 /tb/ac_out
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
WaveRestoreZoom {0 ps} {10500 ns}
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
