-- Copyright (C) 1991-2005 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 176 10/26/2005 Service Pack 0.15 SJ Full Version"

-- DATE "12/15/2005 09:54:31"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	fir_ac_couple IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	reset : IN std_logic;
	Din : IN std_logic_vector(15 DOWNTO 0);
	AC_Out : OUT std_logic_vector(15 DOWNTO 0)
	);
END fir_ac_couple;

ARCHITECTURE structure OF fir_ac_couple IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_Din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_AC_Out : std_logic_vector(15 DOWNTO 0);
SIGNAL Dout_Mult_a8_a_a158 : std_logic;
SIGNAL Dout_Mult_a9_a_a159 : std_logic;
SIGNAL Dout_Mult_a10_a_a160 : std_logic;
SIGNAL Dout_Mult_a11_a_a161 : std_logic;
SIGNAL Dout_Mult_a12_a_a162 : std_logic;
SIGNAL Dout_Mult_a13_a_a163 : std_logic;
SIGNAL Dout_Mult_a14_a_a164 : std_logic;
SIGNAL Dout_Mult_a15_a_a165 : std_logic;
SIGNAL Dout_Mult_a16_a_a166 : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a0_a : std_logic;
SIGNAL Dout_Mult_a7_a_a167 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_a88 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_acout_a0_a : std_logic;
SIGNAL Dout_ff_adffs_a6_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a90COMB : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a : std_logic;
SIGNAL diff_reg_Abs_alcarry_a14_a : std_logic;
SIGNAL Dout_Mult_a5_a_a169 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609 : std_logic;
SIGNAL Din_Del2_ff_adffs_a3_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a13_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a13_a_a100 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a13_a : std_logic;
SIGNAL Dout_ff_adffs_a4_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a9_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a13_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a12_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a12_a_a101 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a12_a : std_logic;
SIGNAL Dout_Mult_a3_a_a171 : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a11_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a11_a_a102 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a11_a : std_logic;
SIGNAL Dout_Mult_a2_a_a172 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621 : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a10_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a10_a_a103 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a10_a : std_logic;
SIGNAL Dout_ff_adffs_a1_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625 : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a9_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a9_a_a104 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a9_a : std_logic;
SIGNAL Dout_ff_adffs_a0_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a8_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a8_a_a105 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a8_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a7_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a7_a_a106 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a7_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a6_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a6_a_a107 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a6_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a5_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a5_a_a108 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a5_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a4_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a4_a_a109 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a4_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a3_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a3_a_a110 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a3_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a2_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a2_a_a111 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a2_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a1_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a1_a_a112 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a1_a : std_logic;
SIGNAL diff_reg_Abs_alcarry_a0_a : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126 : std_logic;
SIGNAL data_add_sub_adatab_node_a3_a : std_logic;
SIGNAL data_add_sub_adatab_node_a9_a : std_logic;
SIGNAL data_add_sub_adatab_node_a13_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97 : std_logic;
SIGNAL reset_acombout : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342 : std_logic;
SIGNAL Din_a5_a_acombout : std_logic;
SIGNAL clk_acombout : std_logic;
SIGNAL imr_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a5_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a5_a : std_logic;
SIGNAL data_add_sub_adatab_node_a5_a : std_logic;
SIGNAL Din_a4_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a4_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a4_a : std_logic;
SIGNAL data_add_sub_adatab_node_a4_a : std_logic;
SIGNAL Din_a3_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a3_a : std_logic;
SIGNAL Din_a2_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a2_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a2_a : std_logic;
SIGNAL data_add_sub_adatab_node_a2_a : std_logic;
SIGNAL Din_a1_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a1_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a1_a : std_logic;
SIGNAL data_add_sub_adatab_node_a1_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541 : std_logic;
SIGNAL Dout_ff_adffs_a10_a : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553 : std_logic;
SIGNAL Dout_ff_adffs_a13_a : std_logic;
SIGNAL Din_a15_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a15_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a15_a : std_logic;
SIGNAL data_add_sub_adatab_node_a15_a : std_logic;
SIGNAL Din_a14_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a14_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a14_a : std_logic;
SIGNAL data_add_sub_adatab_node_a14_a : std_logic;
SIGNAL Din_a13_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a13_a : std_logic;
SIGNAL Din_a12_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a12_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a12_a : std_logic;
SIGNAL data_add_sub_adatab_node_a12_a : std_logic;
SIGNAL Din_a11_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a11_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a11_a : std_logic;
SIGNAL data_add_sub_adatab_node_a11_a : std_logic;
SIGNAL Din_a10_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a10_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a10_a : std_logic;
SIGNAL data_add_sub_adatab_node_a10_a : std_logic;
SIGNAL Din_a9_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a9_a : std_logic;
SIGNAL Din_a8_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a8_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a8_a : std_logic;
SIGNAL data_add_sub_adatab_node_a8_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a : std_logic;
SIGNAL data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a234 : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 : std_logic;
SIGNAL Din_a7_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a7_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a7_a : std_logic;
SIGNAL data_add_sub_adatab_node_a7_a : std_logic;
SIGNAL Din_a6_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a6_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a6_a : std_logic;
SIGNAL data_add_sub_adatab_node_a6_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85 : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a : std_logic;
SIGNAL Din_a0_a_acombout : std_logic;
SIGNAL Din_Del1_ff_adffs_a0_a : std_logic;
SIGNAL Din_Del2_ff_adffs_a0_a : std_logic;
SIGNAL data_add_sub_adatab_node_a0_a : std_logic;
SIGNAL data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a : std_logic;
SIGNAL diff_reg_Abs_alcarry_a0_a_a0 : std_logic;
SIGNAL diff_reg_Abs_alcarry_a0_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a1_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a2_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a3_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a4_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a5_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a6_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a7_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a8_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a9_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a10_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a11_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a12_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a13_a_aCOUT : std_logic;
SIGNAL diff_reg_Abs_alcarry_a14_a_aCOUT : std_logic;
SIGNAL abs_cmp_acomparator_acmp_end_aagb_out : std_logic;
SIGNAL Abs_Thr_Vec_a0_a : std_logic;
SIGNAL Abs_Thr_Vec_a1_a : std_logic;
SIGNAL Abs_Thr_Vec_a2_a : std_logic;
SIGNAL Abs_Thr_Vec_a3_a : std_logic;
SIGNAL Abs_Thr_Vec_a4_a : std_logic;
SIGNAL Abs_Thr_Vec_a5_a : std_logic;
SIGNAL Abs_Thr_Vec_a6_a : std_logic;
SIGNAL Abs_Thr_Vec_a7_a : std_logic;
SIGNAL rtl_a47 : std_logic;
SIGNAL rtl_a48 : std_logic;
SIGNAL offset_en : std_logic;
SIGNAL Dout_Mult_a6_a_a168 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545 : std_logic;
SIGNAL Dout_ff_adffs_a11_a : std_logic;
SIGNAL Dout_Mult_a4_a_a170 : std_logic;
SIGNAL Dout_Mult_a1_a_a173 : std_logic;
SIGNAL Dout_Mult_a0_a_a174 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617 : std_logic;
SIGNAL Dout_ff_adffs_a2_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613 : std_logic;
SIGNAL Dout_ff_adffs_a3_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605 : std_logic;
SIGNAL Dout_ff_adffs_a5_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593 : std_logic;
SIGNAL Dout_ff_adffs_a7_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533 : std_logic;
SIGNAL Dout_ff_adffs_a8_a : std_logic;
SIGNAL AC_Out_FF_adffs_a0_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537 : std_logic;
SIGNAL Dout_ff_adffs_a9_a : std_logic;
SIGNAL AC_Out_FF_adffs_a1_a : std_logic;
SIGNAL AC_Out_FF_adffs_a2_a : std_logic;
SIGNAL AC_Out_FF_adffs_a3_a : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549 : std_logic;
SIGNAL Dout_ff_adffs_a12_a : std_logic;
SIGNAL AC_Out_FF_adffs_a4_a : std_logic;
SIGNAL AC_Out_FF_adffs_a5_a : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557 : std_logic;
SIGNAL Dout_ff_adffs_a14_a : std_logic;
SIGNAL AC_Out_FF_adffs_a6_a : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561 : std_logic;
SIGNAL Dout_ff_adffs_a15_a : std_logic;
SIGNAL AC_Out_FF_adffs_a7_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565 : std_logic;
SIGNAL Dout_ff_adffs_a16_a : std_logic;
SIGNAL AC_Out_FF_adffs_a8_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569 : std_logic;
SIGNAL Dout_ff_adffs_a17_a : std_logic;
SIGNAL AC_Out_FF_adffs_a9_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573 : std_logic;
SIGNAL Dout_ff_adffs_a18_a : std_logic;
SIGNAL AC_Out_FF_adffs_a10_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577 : std_logic;
SIGNAL Dout_ff_adffs_a19_a : std_logic;
SIGNAL AC_Out_FF_adffs_a11_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581 : std_logic;
SIGNAL Dout_ff_adffs_a20_a : std_logic;
SIGNAL AC_Out_FF_adffs_a12_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585 : std_logic;
SIGNAL Dout_ff_adffs_a21_a : std_logic;
SIGNAL AC_Out_FF_adffs_a13_a : std_logic;
SIGNAL data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589 : std_logic;
SIGNAL Dout_ff_adffs_a22_a : std_logic;
SIGNAL AC_Out_FF_adffs_a14_a : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a400 : std_logic;
SIGNAL Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a591 : std_logic;
SIGNAL Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597 : std_logic;
SIGNAL Dout_ff_adffs_a23_a : std_logic;
SIGNAL AC_Out_FF_adffs_a15_a : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_reset <= reset;
ww_Din <= Din;
AC_Out <= ww_AC_Out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

Dout_Mult_a8_a_a158_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a8_a_a158 = !offset_en & (Dout_ff_adffs_a15_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => offset_en,
	datac => Dout_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a8_a_a158);

Dout_Mult_a9_a_a159_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a9_a_a159 = !offset_en & Dout_ff_adffs_a16_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a16_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a9_a_a159);

Dout_Mult_a10_a_a160_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a10_a_a160 = !offset_en & Dout_ff_adffs_a17_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a17_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a10_a_a160);

Dout_Mult_a11_a_a161_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a11_a_a161 = Dout_ff_adffs_a18_a & !offset_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a18_a,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a11_a_a161);

Dout_Mult_a12_a_a162_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a12_a_a162 = Dout_ff_adffs_a19_a & (!offset_en)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00AA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a19_a,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a12_a_a162);

Dout_Mult_a13_a_a163_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a13_a_a163 = !offset_en & Dout_ff_adffs_a20_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a20_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a13_a_a163);

Dout_Mult_a14_a_a164_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a14_a_a164 = !offset_en & Dout_ff_adffs_a21_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a21_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a14_a_a164);

Dout_Mult_a15_a_a165_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a15_a_a165 = !offset_en & Dout_ff_adffs_a22_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a22_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a15_a_a165);

Dout_Mult_a16_a_a166_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a16_a_a166 = !offset_en & (Dout_ff_adffs_a23_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "5050",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => offset_en,
	datac => Dout_ff_adffs_a23_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a16_a_a166);

abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113 = data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a
-- abs_cmp_acomparator_acmp_end_alcarry_a0_a = CARRY(data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "AAAA",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a0_a);

Dout_Mult_a7_a_a167_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a7_a_a167 = !offset_en & Dout_ff_adffs_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a7_a_a167);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601 = Dout_ff_adffs_a6_a $ Dout_Mult_a6_a_a168 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603 = CARRY(Dout_ff_adffs_a6_a & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607 # !Dout_Mult_a6_a_a168) # !Dout_ff_adffs_a6_a & !Dout_Mult_a6_a_a168 & 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a6_a,
	datab => Dout_Mult_a6_a_a168,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a = DFFE(Din_Del1_ff_adffs_a7_a $ data_add_sub_adatab_node_a7_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_a88 = CARRY(Din_Del1_ff_adffs_a7_a & !data_add_sub_adatab_node_a7_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85 # !Din_Del1_ff_adffs_a7_a & 
-- (!data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85 # !data_add_sub_adatab_node_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a7_a,
	datab => data_add_sub_adatab_node_a7_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_a88);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a = DFFE(Din_Del1_ff_adffs_a0_a $ !data_add_sub_adatab_node_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_acout_a0_a = CARRY(Din_Del1_ff_adffs_a0_a # data_add_sub_adatab_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "99EE",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a0_a,
	datab => data_add_sub_adatab_node_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_acout_a0_a);

Dout_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a6_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601 & (!reset_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a601,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a6_a);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a = DFFE(!data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_a88, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a_a88,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a);

diff_reg_Abs_alcarry_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a14_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !diff_reg_Abs_alcarry_a13_a_aCOUT
-- diff_reg_Abs_alcarry_a14_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a13_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a13_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a14_a,
	cout => diff_reg_Abs_alcarry_a14_a_aCOUT);

Dout_Mult_a5_a_a169_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a5_a_a169 = !offset_en & (Dout_ff_adffs_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => Dout_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a5_a_a169);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609 = Dout_ff_adffs_a4_a $ Dout_Mult_a4_a_a170 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611 = CARRY(Dout_ff_adffs_a4_a & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615 # !Dout_Mult_a4_a_a170) # !Dout_ff_adffs_a4_a & !Dout_Mult_a4_a_a170 & 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "962B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a4_a,
	datab => Dout_Mult_a4_a_a170,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611);

Din_Del2_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a3_a = DFFE(Din_Del1_ff_adffs_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del1_ff_adffs_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a3_a);

abs_cmp_acomparator_acmp_end_alcarry_a13_a_a100_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a13_a = CARRY(!diff_reg_Abs_alcarry_a13_a & !abs_cmp_acomparator_acmp_end_alcarry_a12_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a13_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a13_a_a100,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a13_a);

diff_reg_Abs_alcarry_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a13_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ diff_reg_Abs_alcarry_a12_a_aCOUT
-- diff_reg_Abs_alcarry_a13_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 # !diff_reg_Abs_alcarry_a12_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a12_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a13_a,
	cout => diff_reg_Abs_alcarry_a13_a_aCOUT);

Dout_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a4_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609 & (!reset_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a609,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a4_a);

Din_Del2_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a9_a = DFFE(Din_Del1_ff_adffs_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a9_a);

Din_Del2_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a13_a = DFFE(Din_Del1_ff_adffs_a13_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a13_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a13_a);

abs_cmp_acomparator_acmp_end_alcarry_a12_a_a101_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a12_a = CARRY(diff_reg_Abs_alcarry_a12_a # !abs_cmp_acomparator_acmp_end_alcarry_a11_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a12_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a12_a_a101,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a12_a);

diff_reg_Abs_alcarry_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a12_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !diff_reg_Abs_alcarry_a11_a_aCOUT
-- diff_reg_Abs_alcarry_a12_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a11_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a11_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a12_a,
	cout => diff_reg_Abs_alcarry_a12_a_aCOUT);

Dout_Mult_a3_a_a171_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a3_a_a171 = !offset_en & (Dout_ff_adffs_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => Dout_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a3_a_a171);

abs_cmp_acomparator_acmp_end_alcarry_a11_a_a102_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a11_a = CARRY(!diff_reg_Abs_alcarry_a11_a & !abs_cmp_acomparator_acmp_end_alcarry_a10_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a11_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a11_a_a102,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a11_a);

diff_reg_Abs_alcarry_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a11_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ diff_reg_Abs_alcarry_a10_a_aCOUT
-- diff_reg_Abs_alcarry_a11_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 # !diff_reg_Abs_alcarry_a10_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a10_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a11_a,
	cout => diff_reg_Abs_alcarry_a11_a_aCOUT);

Dout_Mult_a2_a_a172_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a2_a_a172 = Dout_ff_adffs_a9_a & !offset_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a9_a,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a2_a_a172);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621 = Dout_ff_adffs_a1_a $ Dout_Mult_a1_a_a173 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623 = CARRY(Dout_ff_adffs_a1_a & Dout_Mult_a1_a_a173 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627 # !Dout_ff_adffs_a1_a & (Dout_Mult_a1_a_a173 # 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "694D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a1_a,
	datab => Dout_Mult_a1_a_a173,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623);

abs_cmp_acomparator_acmp_end_alcarry_a10_a_a103_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a10_a = CARRY(diff_reg_Abs_alcarry_a10_a # !abs_cmp_acomparator_acmp_end_alcarry_a9_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a10_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a10_a_a103,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a10_a);

diff_reg_Abs_alcarry_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a10_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !diff_reg_Abs_alcarry_a9_a_aCOUT
-- diff_reg_Abs_alcarry_a10_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a9_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a9_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a10_a,
	cout => diff_reg_Abs_alcarry_a10_a_aCOUT);

Dout_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a1_a = DFFE(!reset_acombout & (Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a621,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a1_a);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625 = Dout_ff_adffs_a0_a $ Dout_Mult_a0_a_a174
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627 = CARRY(Dout_ff_adffs_a0_a # !Dout_Mult_a0_a_a174)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "66BB",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a0_a,
	datab => Dout_Mult_a0_a_a174,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a627);

abs_cmp_acomparator_acmp_end_alcarry_a9_a_a104_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a9_a = CARRY(!diff_reg_Abs_alcarry_a9_a & !abs_cmp_acomparator_acmp_end_alcarry_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a9_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a9_a_a104,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a9_a);

diff_reg_Abs_alcarry_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a9_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ diff_reg_Abs_alcarry_a8_a_aCOUT
-- diff_reg_Abs_alcarry_a9_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 # !diff_reg_Abs_alcarry_a8_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a8_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a9_a,
	cout => diff_reg_Abs_alcarry_a9_a_aCOUT);

Dout_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a0_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a625,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a0_a);

abs_cmp_acomparator_acmp_end_alcarry_a8_a_a105_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a8_a = CARRY(diff_reg_Abs_alcarry_a8_a # !abs_cmp_acomparator_acmp_end_alcarry_a7_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a8_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a8_a_a105,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a8_a);

diff_reg_Abs_alcarry_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a8_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !diff_reg_Abs_alcarry_a7_a_aCOUT
-- diff_reg_Abs_alcarry_a8_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a7_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => diff_reg_Abs_alcarry_a7_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a8_a,
	cout => diff_reg_Abs_alcarry_a8_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a7_a_a106_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a7_a = CARRY(!diff_reg_Abs_alcarry_a7_a & (!abs_cmp_acomparator_acmp_end_alcarry_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0005",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => diff_reg_Abs_alcarry_a7_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a7_a_a106,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a7_a);

diff_reg_Abs_alcarry_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a7_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a $ diff_reg_Abs_alcarry_a6_a_aCOUT
-- diff_reg_Abs_alcarry_a7_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a # !diff_reg_Abs_alcarry_a6_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cin => diff_reg_Abs_alcarry_a6_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a7_a,
	cout => diff_reg_Abs_alcarry_a7_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a6_a_a107_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a6_a = CARRY(diff_reg_Abs_alcarry_a6_a # !abs_cmp_acomparator_acmp_end_alcarry_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a6_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a6_a_a107,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a6_a);

diff_reg_Abs_alcarry_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a6_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a $ !diff_reg_Abs_alcarry_a5_a_aCOUT
-- diff_reg_Abs_alcarry_a6_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a5_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cin => diff_reg_Abs_alcarry_a5_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a6_a,
	cout => diff_reg_Abs_alcarry_a6_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a5_a_a108_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a5_a = CARRY(!diff_reg_Abs_alcarry_a5_a & !abs_cmp_acomparator_acmp_end_alcarry_a4_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0003",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a5_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a5_a_a108,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a5_a);

diff_reg_Abs_alcarry_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a5_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a $ diff_reg_Abs_alcarry_a4_a_aCOUT
-- diff_reg_Abs_alcarry_a5_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a # !diff_reg_Abs_alcarry_a4_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cin => diff_reg_Abs_alcarry_a4_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a5_a,
	cout => diff_reg_Abs_alcarry_a5_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a4_a_a109_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a4_a = CARRY(diff_reg_Abs_alcarry_a4_a # !abs_cmp_acomparator_acmp_end_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a4_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a4_a_a109,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a4_a);

diff_reg_Abs_alcarry_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a4_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a $ !diff_reg_Abs_alcarry_a3_a_aCOUT
-- diff_reg_Abs_alcarry_a4_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a3_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cin => diff_reg_Abs_alcarry_a3_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a4_a,
	cout => diff_reg_Abs_alcarry_a4_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a3_a_a110_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a3_a = CARRY(!abs_cmp_acomparator_acmp_end_alcarry_a2_a # !diff_reg_Abs_alcarry_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a3_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a3_a_a110,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a3_a);

diff_reg_Abs_alcarry_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a3_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a $ diff_reg_Abs_alcarry_a2_a_aCOUT
-- diff_reg_Abs_alcarry_a3_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a # !diff_reg_Abs_alcarry_a2_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cin => diff_reg_Abs_alcarry_a2_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a3_a,
	cout => diff_reg_Abs_alcarry_a3_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a2_a_a111_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a2_a = CARRY(diff_reg_Abs_alcarry_a2_a # !abs_cmp_acomparator_acmp_end_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "00CF",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a2_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a2_a_a111,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a2_a);

diff_reg_Abs_alcarry_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a2_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a $ !diff_reg_Abs_alcarry_a1_a_aCOUT
-- diff_reg_Abs_alcarry_a2_a_aCOUT = CARRY(!diff_reg_Abs_alcarry_a1_a_aCOUT & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "6906",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	cin => diff_reg_Abs_alcarry_a1_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a2_a,
	cout => diff_reg_Abs_alcarry_a2_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a1_a_a112_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a1_a = CARRY(!abs_cmp_acomparator_acmp_end_alcarry_a0_a # !diff_reg_Abs_alcarry_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "003F",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	datab => diff_reg_Abs_alcarry_a1_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a1_a_a112,
	cout => abs_cmp_acomparator_acmp_end_alcarry_a1_a);

diff_reg_Abs_alcarry_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a1_a = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a $ diff_reg_Abs_alcarry_a0_a_aCOUT
-- diff_reg_Abs_alcarry_a1_a_aCOUT = CARRY(data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a # !diff_reg_Abs_alcarry_a0_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "969F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cin => diff_reg_Abs_alcarry_a0_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a1_a,
	cout => diff_reg_Abs_alcarry_a1_a_aCOUT);

abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126_I : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126 = diff_reg_Abs_alcarry_a14_a # !abs_cmp_acomparator_acmp_end_alcarry_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "FF0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => diff_reg_Abs_alcarry_a14_a,
	cin => abs_cmp_acomparator_acmp_end_alcarry_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126);

data_add_sub_adatab_node_a3_a_a27 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a3_a = !Din_Del2_ff_adffs_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a3_a);

data_add_sub_adatab_node_a9_a_a34 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a9_a = !Din_Del2_ff_adffs_a9_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a9_a);

data_add_sub_adatab_node_a13_a_a38 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a13_a = !Din_Del2_ff_adffs_a13_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a13_a);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97 = data_add_sub_aadder1_a0_a_aresult_node_acout_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	cin => data_add_sub_aadder1_a0_a_aresult_node_acout_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97);

reset_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_reset,
	combout => reset_acombout);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342 = Dout_ff_adffs_a8_a $ abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344 = CARRY(Dout_ff_adffs_a8_a & abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a8_a,
	datab => abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344);

Din_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(5),
	combout => Din_a5_a_acombout);

clk_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => clk_acombout);

imr_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => imr_acombout);

Din_Del1_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a5_a = DFFE(Din_a5_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a5_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a5_a);

Din_Del2_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a5_a = DFFE(Din_Del1_ff_adffs_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a5_a);

data_add_sub_adatab_node_a5_a_a29 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a5_a = !Din_Del2_ff_adffs_a5_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a5_a);

Din_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(4),
	combout => Din_a4_a_acombout);

Din_Del1_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a4_a = DFFE(Din_a4_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a4_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a4_a);

Din_Del2_ff_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a4_a = DFFE(Din_Del1_ff_adffs_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a4_a);

data_add_sub_adatab_node_a4_a_a28 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a4_a = !Din_Del2_ff_adffs_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a4_a);

Din_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(3),
	combout => Din_a3_a_acombout);

Din_Del1_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a3_a = DFFE(Din_a3_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a3_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a3_a);

Din_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(2),
	combout => Din_a2_a_acombout);

Din_Del1_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a2_a = DFFE(Din_a2_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a2_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a2_a);

Din_Del2_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a2_a = DFFE(Din_Del1_ff_adffs_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a2_a);

data_add_sub_adatab_node_a2_a_a26 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a2_a = !Din_Del2_ff_adffs_a2_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a2_a);

Din_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(1),
	combout => Din_a1_a_acombout);

Din_Del1_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a1_a = DFFE(Din_a1_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a1_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a1_a);

Din_Del2_ff_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a1_a = DFFE(Din_Del1_ff_adffs_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a1_a);

data_add_sub_adatab_node_a1_a_a25 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a1_a = !Din_Del2_ff_adffs_a1_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a1_a);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92 = CARRY(data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "00AA",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a97,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a90COMB,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a = DFFE(Din_Del1_ff_adffs_a1_a $ data_add_sub_adatab_node_a1_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70 = CARRY(Din_Del1_ff_adffs_a1_a & !data_add_sub_adatab_node_a1_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92 # !Din_Del1_ff_adffs_a1_a & 
-- (!data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92 # !data_add_sub_adatab_node_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a1_a,
	datab => data_add_sub_adatab_node_a1_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a92,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a = DFFE(Din_Del1_ff_adffs_a2_a $ data_add_sub_adatab_node_a2_a $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73 = CARRY(Din_Del1_ff_adffs_a2_a & (data_add_sub_adatab_node_a2_a # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70) # !Din_Del1_ff_adffs_a2_a & data_add_sub_adatab_node_a2_a & 
-- !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a2_a,
	datab => data_add_sub_adatab_node_a2_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a_a70,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a = DFFE(data_add_sub_adatab_node_a3_a $ Din_Del1_ff_adffs_a3_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76 = CARRY(data_add_sub_adatab_node_a3_a & !Din_Del1_ff_adffs_a3_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73 # !data_add_sub_adatab_node_a3_a & 
-- (!data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73 # !Din_Del1_ff_adffs_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_adatab_node_a3_a,
	datab => Din_Del1_ff_adffs_a3_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a_a73,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a = DFFE(Din_Del1_ff_adffs_a4_a $ data_add_sub_adatab_node_a4_a $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79 = CARRY(Din_Del1_ff_adffs_a4_a & (data_add_sub_adatab_node_a4_a # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76) # !Din_Del1_ff_adffs_a4_a & data_add_sub_adatab_node_a4_a & 
-- !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a4_a,
	datab => data_add_sub_adatab_node_a4_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a_a76,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a = DFFE(Din_Del1_ff_adffs_a5_a $ data_add_sub_adatab_node_a5_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82 = CARRY(Din_Del1_ff_adffs_a5_a & !data_add_sub_adatab_node_a5_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79 # !Din_Del1_ff_adffs_a5_a & 
-- (!data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79 # !data_add_sub_adatab_node_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a5_a,
	datab => data_add_sub_adatab_node_a5_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a_a79,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346 = Dout_ff_adffs_a9_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348 = CARRY(Dout_ff_adffs_a9_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344 # !Dout_ff_adffs_a9_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344 # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a9_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a1_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a344,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350 = data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a $ Dout_ff_adffs_a10_a $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352 = CARRY(data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a & (Dout_ff_adffs_a10_a # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348) # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a 
-- & Dout_ff_adffs_a10_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a2_a,
	datab => Dout_ff_adffs_a10_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a348,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533 = Dout_Mult_a8_a_a158 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535 = CARRY(Dout_Mult_a8_a_a158 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595 # !Dout_Mult_a8_a_a158 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a8_a_a158,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a342,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537 = Dout_Mult_a9_a_a159 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539 = CARRY(Dout_Mult_a9_a_a159 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346) # !Dout_Mult_a9_a_a159 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a9_a_a159,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a346,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a535,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541 = Dout_Mult_a10_a_a160 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543 = CARRY(Dout_Mult_a10_a_a160 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539 # !Dout_Mult_a10_a_a160 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a10_a_a160,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a350,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a539,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543);

Dout_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a10_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a541,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a10_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354 = Dout_ff_adffs_a11_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356 = CARRY(Dout_ff_adffs_a11_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352 # !Dout_ff_adffs_a11_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352 # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a11_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a3_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a352,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358 = Dout_ff_adffs_a12_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360 = CARRY(Dout_ff_adffs_a12_a & (data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356) # !Dout_ff_adffs_a12_a & 
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a12_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a4_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a356,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362 = Dout_ff_adffs_a13_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364 = CARRY(Dout_ff_adffs_a13_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360 # !Dout_ff_adffs_a13_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360 # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a13_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a360,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545 = Dout_Mult_a11_a_a161 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547 = CARRY(Dout_Mult_a11_a_a161 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354) # !Dout_Mult_a11_a_a161 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a11_a_a161,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a354,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a543,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549 = Dout_Mult_a12_a_a162 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551 = CARRY(Dout_Mult_a12_a_a162 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547 # !Dout_Mult_a12_a_a162 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a12_a_a162,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a358,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a547,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553 = Dout_Mult_a13_a_a163 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555 = CARRY(Dout_Mult_a13_a_a163 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362) # !Dout_Mult_a13_a_a163 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a13_a_a163,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a362,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a551,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555);

Dout_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a13_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a553,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a13_a);

Din_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(15),
	combout => Din_a15_a_acombout);

Din_Del1_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a15_a = DFFE(Din_a15_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a15_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a15_a);

Din_Del2_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a15_a = DFFE(Din_Del1_ff_adffs_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del1_ff_adffs_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a15_a);

data_add_sub_adatab_node_a15_a_a40 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a15_a = !Din_Del2_ff_adffs_a15_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a15_a);

Din_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(14),
	combout => Din_a14_a_acombout);

Din_Del1_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a14_a = DFFE(Din_a14_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a14_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a14_a);

Din_Del2_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a14_a = DFFE(Din_Del1_ff_adffs_a14_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del1_ff_adffs_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a14_a);

data_add_sub_adatab_node_a14_a_a39 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a14_a = !Din_Del2_ff_adffs_a14_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a14_a);

Din_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(13),
	combout => Din_a13_a_acombout);

Din_Del1_ff_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a13_a = DFFE(Din_a13_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a13_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a13_a);

Din_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(12),
	combout => Din_a12_a_acombout);

Din_Del1_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a12_a = DFFE(Din_a12_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a12_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a12_a);

Din_Del2_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a12_a = DFFE(Din_Del1_ff_adffs_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a12_a);

data_add_sub_adatab_node_a12_a_a37 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a12_a = !Din_Del2_ff_adffs_a12_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a12_a);

Din_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(11),
	combout => Din_a11_a_acombout);

Din_Del1_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a11_a = DFFE(Din_a11_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a11_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a11_a);

Din_Del2_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a11_a = DFFE(Din_Del1_ff_adffs_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a11_a);

data_add_sub_adatab_node_a11_a_a36 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a11_a = !Din_Del2_ff_adffs_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F0F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del2_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a11_a);

Din_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(10),
	combout => Din_a10_a_acombout);

Din_Del1_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a10_a = DFFE(Din_a10_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a10_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a10_a);

Din_Del2_ff_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a10_a = DFFE(Din_Del1_ff_adffs_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del1_ff_adffs_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a10_a);

data_add_sub_adatab_node_a10_a_a35 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a10_a = !Din_Del2_ff_adffs_a10_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a10_a);

Din_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(9),
	combout => Din_a9_a_acombout);

Din_Del1_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a9_a = DFFE(Din_a9_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a9_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a9_a);

Din_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(8),
	combout => Din_a8_a_acombout);

Din_Del1_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a8_a = DFFE(Din_a8_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a8_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a8_a);

Din_Del2_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a8_a = DFFE(Din_Del1_ff_adffs_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Din_Del1_ff_adffs_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a8_a);

data_add_sub_adatab_node_a8_a_a33 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a8_a = !Din_Del2_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a8_a);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a = DFFE(Din_Del1_ff_adffs_a8_a $ data_add_sub_adatab_node_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 = CARRY(Din_Del1_ff_adffs_a8_a & data_add_sub_adatab_node_a8_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a8_a,
	datab => data_add_sub_adatab_node_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a = DFFE(data_add_sub_adatab_node_a9_a $ Din_Del1_ff_adffs_a9_a $ data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52 = CARRY(data_add_sub_adatab_node_a9_a & !Din_Del1_ff_adffs_a9_a & !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 # !data_add_sub_adatab_node_a9_a & 
-- (!data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49 # !Din_Del1_ff_adffs_a9_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_adatab_node_a9_a,
	datab => Din_Del1_ff_adffs_a9_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a_a49,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a = DFFE(Din_Del1_ff_adffs_a10_a $ data_add_sub_adatab_node_a10_a $ !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 = CARRY(Din_Del1_ff_adffs_a10_a & (data_add_sub_adatab_node_a10_a # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52) # !Din_Del1_ff_adffs_a10_a & data_add_sub_adatab_node_a10_a & 
-- !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a10_a,
	datab => data_add_sub_adatab_node_a10_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a_a52,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a = DFFE(Din_Del1_ff_adffs_a11_a $ data_add_sub_adatab_node_a11_a $ data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58 = CARRY(Din_Del1_ff_adffs_a11_a & !data_add_sub_adatab_node_a11_a & !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 # !Din_Del1_ff_adffs_a11_a & 
-- (!data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55 # !data_add_sub_adatab_node_a11_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a11_a,
	datab => data_add_sub_adatab_node_a11_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a_a55,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a = DFFE(Din_Del1_ff_adffs_a12_a $ data_add_sub_adatab_node_a12_a $ !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 = CARRY(Din_Del1_ff_adffs_a12_a & (data_add_sub_adatab_node_a12_a # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58) # !Din_Del1_ff_adffs_a12_a & data_add_sub_adatab_node_a12_a & 
-- !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a12_a,
	datab => data_add_sub_adatab_node_a12_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a_a58,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a = DFFE(data_add_sub_adatab_node_a13_a $ Din_Del1_ff_adffs_a13_a $ data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64 = CARRY(data_add_sub_adatab_node_a13_a & !Din_Del1_ff_adffs_a13_a & !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 # !data_add_sub_adatab_node_a13_a & 
-- (!data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61 # !Din_Del1_ff_adffs_a13_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_adatab_node_a13_a,
	datab => Din_Del1_ff_adffs_a13_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a_a61,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a = DFFE(Din_Del1_ff_adffs_a14_a $ data_add_sub_adatab_node_a14_a $ !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 = CARRY(Din_Del1_ff_adffs_a14_a & (data_add_sub_adatab_node_a14_a # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64) # !Din_Del1_ff_adffs_a14_a & data_add_sub_adatab_node_a14_a & 
-- !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a14_a,
	datab => data_add_sub_adatab_node_a14_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a_a64,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67);

data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a = DFFE(Din_Del1_ff_adffs_a15_a $ (data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67 $ data_add_sub_adatab_node_a15_a), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "A55A",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a15_a,
	datad => data_add_sub_adatab_node_a15_a,
	cin => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a_a67,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 = data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a $ data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210 = CARRY(data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a & data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "6688",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a8_a,
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214 = CARRY(!data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210 # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a1_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a210,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218 = CARRY(data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a2_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a214,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222 = CARRY(!data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218 # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a3_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a218,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226 = CARRY(data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a4_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a222,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230 = CARRY(!data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226 # !data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "3C3F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a5_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a226,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 = data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a $ !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a234 = CARRY(data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C30C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a6_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a230,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232,
	cout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a234);

data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236_I : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236 = data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a234 $ data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0FF0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => data_add_sub_aadder1_0_a1_a_aresult_node_asout_node_a7_a,
	cin => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a234,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236);

Din_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(7),
	combout => Din_a7_a_acombout);

Din_Del1_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a7_a = DFFE(Din_a7_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a7_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a7_a);

Din_Del2_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a7_a = DFFE(Din_Del1_ff_adffs_a7_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a7_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a7_a);

data_add_sub_adatab_node_a7_a_a31 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a7_a = !Din_Del2_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a7_a);

Din_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(6),
	combout => Din_a6_a_acombout);

Din_Del1_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a6_a = DFFE(Din_a6_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a6_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a6_a);

Din_Del2_ff_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a6_a = DFFE(Din_Del1_ff_adffs_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a6_a);

data_add_sub_adatab_node_a6_a_a30 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a6_a = !Din_Del2_ff_adffs_a6_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a6_a);

data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a = DFFE(Din_Del1_ff_adffs_a6_a $ data_add_sub_adatab_node_a6_a $ !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85 = CARRY(Din_Del1_ff_adffs_a6_a & (data_add_sub_adatab_node_a6_a # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82) # !Din_Del1_ff_adffs_a6_a & data_add_sub_adatab_node_a6_a & 
-- !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Din_Del1_ff_adffs_a6_a,
	datab => data_add_sub_adatab_node_a6_a,
	cin => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a5_a_a82,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cout => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a_a85);

Din_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "input",
	reg_source_mode => "none",
	feedback_mode => "from_pin",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(0),
	combout => Din_a0_a_acombout);

Din_Del1_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del1_ff_adffs_a0_a = DFFE(Din_a0_a_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_a0_a_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del1_ff_adffs_a0_a);

Din_Del2_ff_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Din_Del2_ff_adffs_a0_a = DFFE(Din_Del1_ff_adffs_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del1_ff_adffs_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Din_Del2_ff_adffs_a0_a);

data_add_sub_adatab_node_a0_a_a32 : apex20ke_lcell
-- Equation(s):
-- data_add_sub_adatab_node_a0_a = !Din_Del2_ff_adffs_a0_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00FF",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datad => Din_Del2_ff_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => data_add_sub_adatab_node_a0_a);

diff_reg_Abs_alcarry_a0_a_a0_I : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a0_a_a0 = !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a & data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a0_a,
	datad => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a0_a_a0);

diff_reg_Abs_alcarry_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- diff_reg_Abs_alcarry_a0_a_aCOUT = CARRY(diff_reg_Abs_alcarry_a0_a_a0)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "none")
-- pragma translate_on
PORT MAP (
	dataa => abs_cmp_acomparator_acmp_end_alcarry_a0_a_a113,
	datab => diff_reg_Abs_alcarry_a0_a_a0,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => diff_reg_Abs_alcarry_a0_a,
	cout => diff_reg_Abs_alcarry_a0_a_aCOUT);

abs_cmp_acomparator_acmp_end_aagb_out_node_a1 : apex20ke_lcell
-- Equation(s):
-- abs_cmp_acomparator_acmp_end_aagb_out = abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126 & (!diff_reg_Abs_alcarry_a14_a_aCOUT)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "0A0A",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => abs_cmp_acomparator_acmp_end_alcarry_a14_a_a126,
	cin => diff_reg_Abs_alcarry_a14_a_aCOUT,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => abs_cmp_acomparator_acmp_end_aagb_out);

Abs_Thr_Vec_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a0_a = DFFE(abs_cmp_acomparator_acmp_end_aagb_out, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => abs_cmp_acomparator_acmp_end_aagb_out,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a0_a);

Abs_Thr_Vec_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a1_a = DFFE(Abs_Thr_Vec_a0_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Abs_Thr_Vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a1_a);

Abs_Thr_Vec_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a2_a = DFFE(Abs_Thr_Vec_a1_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Abs_Thr_Vec_a1_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a2_a);

Abs_Thr_Vec_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a3_a = DFFE(Abs_Thr_Vec_a2_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Abs_Thr_Vec_a2_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a3_a);

Abs_Thr_Vec_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a4_a = DFFE(Abs_Thr_Vec_a3_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Abs_Thr_Vec_a3_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a4_a);

Abs_Thr_Vec_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a5_a = DFFE(Abs_Thr_Vec_a4_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Abs_Thr_Vec_a4_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a5_a);

Abs_Thr_Vec_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a6_a = DFFE(Abs_Thr_Vec_a5_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Abs_Thr_Vec_a5_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a6_a);

Abs_Thr_Vec_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Abs_Thr_Vec_a7_a = DFFE(Abs_Thr_Vec_a6_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Abs_Thr_Vec_a6_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Abs_Thr_Vec_a7_a);

rtl_a47_I : apex20ke_lcell
-- Equation(s):
-- rtl_a47 = !Abs_Thr_Vec_a6_a & !Abs_Thr_Vec_a5_a & !Abs_Thr_Vec_a7_a & !Abs_Thr_Vec_a4_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0001",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Abs_Thr_Vec_a6_a,
	datab => Abs_Thr_Vec_a5_a,
	datac => Abs_Thr_Vec_a7_a,
	datad => Abs_Thr_Vec_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a47);

rtl_a48_I : apex20ke_lcell
-- Equation(s):
-- rtl_a48 = !Abs_Thr_Vec_a2_a & !Abs_Thr_Vec_a3_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "000F",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Abs_Thr_Vec_a2_a,
	datad => Abs_Thr_Vec_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => rtl_a48);

offset_en_aI : apex20ke_lcell
-- Equation(s):
-- offset_en = DFFE(Abs_Thr_Vec_a1_a # Abs_Thr_Vec_a0_a # !rtl_a48 # !rtl_a47, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FFBF",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	dataa => Abs_Thr_Vec_a1_a,
	datab => rtl_a47,
	datac => rtl_a48,
	datad => Abs_Thr_Vec_a0_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_en);

Dout_Mult_a6_a_a168_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a6_a_a168 = Dout_ff_adffs_a13_a & !offset_en

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a13_a,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a6_a_a168);

Dout_ff_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a11_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a545,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a11_a);

Dout_Mult_a4_a_a170_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a4_a_a170 = !offset_en & Dout_ff_adffs_a11_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a4_a_a170);

Dout_Mult_a1_a_a173_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a1_a_a173 = !offset_en & Dout_ff_adffs_a8_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a1_a_a173);

Dout_Mult_a0_a_a174_I : apex20ke_lcell
-- Equation(s):
-- Dout_Mult_a0_a_a174 = !offset_en & Dout_ff_adffs_a7_a

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => Dout_ff_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Mult_a0_a_a174);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617 = Dout_Mult_a2_a_a172 $ Dout_ff_adffs_a2_a $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619 = CARRY(Dout_Mult_a2_a_a172 & Dout_ff_adffs_a2_a & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623 # !Dout_Mult_a2_a_a172 & (Dout_ff_adffs_a2_a # 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a2_a_a172,
	datab => Dout_ff_adffs_a2_a,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a623,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619);

Dout_ff_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a2_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617 & (!reset_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a617,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a2_a);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613 = Dout_Mult_a3_a_a171 $ Dout_ff_adffs_a3_a $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615 = CARRY(Dout_Mult_a3_a_a171 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619 # !Dout_ff_adffs_a3_a) # !Dout_Mult_a3_a_a171 & !Dout_ff_adffs_a3_a & 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a3_a_a171,
	datab => Dout_ff_adffs_a3_a,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a619,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a615);

Dout_ff_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a3_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613 & (!reset_acombout), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00CC",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a613,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a3_a);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605 = Dout_Mult_a5_a_a169 $ Dout_ff_adffs_a5_a $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607 = CARRY(Dout_Mult_a5_a_a169 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611 # !Dout_ff_adffs_a5_a) # !Dout_Mult_a5_a_a169 & !Dout_ff_adffs_a5_a & 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a5_a_a169,
	datab => Dout_ff_adffs_a5_a,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a611,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a607);

Dout_ff_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a5_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a605,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a5_a);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593 = Dout_Mult_a7_a_a167 $ Dout_ff_adffs_a7_a $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595 = CARRY(Dout_Mult_a7_a_a167 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603 # !Dout_ff_adffs_a7_a) # !Dout_Mult_a7_a_a167 & !Dout_ff_adffs_a7_a & 
-- !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a7_a_a167,
	datab => Dout_ff_adffs_a7_a,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a603,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a595);

Dout_ff_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a7_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a593,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a7_a);

Dout_ff_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a8_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a533,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a8_a);

AC_Out_FF_adffs_a0_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a0_a = DFFE(Dout_ff_adffs_a8_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a8_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a0_a);

Dout_ff_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a9_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a537,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a9_a);

AC_Out_FF_adffs_a1_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a1_a = DFFE(Dout_ff_adffs_a9_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a9_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a1_a);

AC_Out_FF_adffs_a2_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a2_a = DFFE(Dout_ff_adffs_a10_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a10_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a2_a);

AC_Out_FF_adffs_a3_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a3_a = DFFE(Dout_ff_adffs_a11_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a11_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a3_a);

Dout_ff_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a12_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a549,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a12_a);

AC_Out_FF_adffs_a4_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a4_a = DFFE(Dout_ff_adffs_a12_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a12_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a4_a);

AC_Out_FF_adffs_a5_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a5_a = DFFE(Dout_ff_adffs_a13_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a13_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a5_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366 = Dout_ff_adffs_a14_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368 = CARRY(Dout_ff_adffs_a14_a & (data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364) # !Dout_ff_adffs_a14_a & 
-- data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a14_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a6_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a364,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557 = Dout_Mult_a14_a_a164 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559 = CARRY(Dout_Mult_a14_a_a164 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555 # !Dout_Mult_a14_a_a164 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a14_a_a164,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a366,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a555,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559);

Dout_ff_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a14_a = DFFE(Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557 & !reset_acombout, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "00F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a557,
	datad => reset_acombout,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a14_a);

AC_Out_FF_adffs_a6_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a6_a = DFFE(Dout_ff_adffs_a14_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a14_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a6_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370 = Dout_ff_adffs_a15_a $ data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372 = CARRY(Dout_ff_adffs_a15_a & !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368 # !Dout_ff_adffs_a15_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368 # !data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a15_a,
	datab => data_add_sub_aadder1_a0_a_aresult_node_asout_node_a7_a,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a368,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561 = Dout_Mult_a15_a_a165 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563 = CARRY(Dout_Mult_a15_a_a165 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370) # !Dout_Mult_a15_a_a165 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a15_a_a165,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a370,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a559,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563);

Dout_ff_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a15_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a561,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a15_a);

AC_Out_FF_adffs_a7_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a7_a = DFFE(Dout_ff_adffs_a15_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a15_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a7_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374 = Dout_ff_adffs_a16_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376 = CARRY(Dout_ff_adffs_a16_a & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372) # !Dout_ff_adffs_a16_a & 
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a16_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a208,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a372,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567 = CARRY(Dout_Mult_a16_a_a166 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563 # !Dout_Mult_a16_a_a166 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a374,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a563,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567);

Dout_ff_adffs_a16_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a16_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a565,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a16_a);

AC_Out_FF_adffs_a8_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a8_a = DFFE(Dout_ff_adffs_a16_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a16_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a8_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378 = Dout_ff_adffs_a17_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380 = CARRY(Dout_ff_adffs_a17_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376 # !Dout_ff_adffs_a17_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376 # !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a17_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a212,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a376,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571 = CARRY(Dout_Mult_a16_a_a166 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378) # !Dout_Mult_a16_a_a166 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a378,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a567,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571);

Dout_ff_adffs_a17_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a17_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a569,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a17_a);

AC_Out_FF_adffs_a9_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a9_a = DFFE(Dout_ff_adffs_a17_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a17_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a9_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382 = Dout_ff_adffs_a18_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384 = CARRY(Dout_ff_adffs_a18_a & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380) # !Dout_ff_adffs_a18_a & 
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a18_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a216,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a380,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575 = CARRY(Dout_Mult_a16_a_a166 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571 # !Dout_Mult_a16_a_a166 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a382,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a571,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575);

Dout_ff_adffs_a18_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a18_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a573,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a18_a);

AC_Out_FF_adffs_a10_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a10_a = DFFE(Dout_ff_adffs_a18_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a18_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a10_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386 = Dout_ff_adffs_a19_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388 = CARRY(Dout_ff_adffs_a19_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384 # !Dout_ff_adffs_a19_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384 # !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a19_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a220,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a384,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579 = CARRY(Dout_Mult_a16_a_a166 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386) # !Dout_Mult_a16_a_a166 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a386,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a575,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579);

Dout_ff_adffs_a19_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a19_a = DFFE(!reset_acombout & (Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a577,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a19_a);

AC_Out_FF_adffs_a11_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a11_a = DFFE(Dout_ff_adffs_a19_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a19_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a11_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390 = Dout_ff_adffs_a20_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392 = CARRY(Dout_ff_adffs_a20_a & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388) # !Dout_ff_adffs_a20_a & 
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a20_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a224,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a388,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583 = CARRY(Dout_Mult_a16_a_a166 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579 # !Dout_Mult_a16_a_a166 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a390,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a579,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583);

Dout_ff_adffs_a20_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a20_a = DFFE(!reset_acombout & (Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a581,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a20_a);

AC_Out_FF_adffs_a12_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a12_a = DFFE(Dout_ff_adffs_a20_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "F0F0",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => Dout_ff_adffs_a20_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a12_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394 = Dout_ff_adffs_a21_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396 = CARRY(Dout_ff_adffs_a21_a & !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392 # !Dout_ff_adffs_a21_a & 
-- (!Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392 # !data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "9617",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a21_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a228,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a392,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394 $ !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587 = CARRY(Dout_Mult_a16_a_a166 & (!Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394) # !Dout_Mult_a16_a_a166 & 
-- !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "692B",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a394,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a583,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587);

Dout_ff_adffs_a21_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a21_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a585,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a21_a);

AC_Out_FF_adffs_a13_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a13_a = DFFE(Dout_ff_adffs_a21_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a21_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a13_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398 = Dout_ff_adffs_a22_a $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a400 = CARRY(Dout_ff_adffs_a22_a & (data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 # !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396) # !Dout_ff_adffs_a22_a & 
-- data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232 & !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "698E",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_ff_adffs_a22_a,
	datab => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a232,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a396,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398,
	cout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a400);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589 = Dout_Mult_a16_a_a166 $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398 $ Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a591 = CARRY(Dout_Mult_a16_a_a166 & Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398 & !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587 # !Dout_Mult_a16_a_a166 & 
-- (Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398 # !Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587))

-- pragma translate_off
GENERIC MAP (
	operation_mode => "arithmetic",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "964D",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datab => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a398,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a587,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589,
	cout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a591);

Dout_ff_adffs_a22_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a22_a = DFFE(!reset_acombout & (Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589), GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "3300",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datab => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a589,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a22_a);

AC_Out_FF_adffs_a14_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a14_a = DFFE(Dout_ff_adffs_a22_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a22_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a14_a);

Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402_I : apex20ke_lcell
-- Equation(s):
-- Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402 = Dout_ff_adffs_a23_a $ Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a400 $ data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "C33C",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	datab => Dout_ff_adffs_a23_a,
	datad => data_add_sub_aadder1_a1_a_aresult_node_acs_buffer_a0_a_a236,
	cin => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a400,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402);

Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597_I : apex20ke_lcell
-- Equation(s):
-- Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597 = Dout_Mult_a16_a_a166 $ (Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a591 $ !Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402)

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	cin_used => "true",
	packed_mode => "false",
	lut_mask => "5AA5",
	output_mode => "comb_only")
-- pragma translate_on
PORT MAP (
	dataa => Dout_Mult_a16_a_a166,
	datad => Dout_add_Sub_aadder_aresult_node_acs_buffer_a8_a_a402,
	cin => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a591,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597);

Dout_ff_adffs_a23_a_aI : apex20ke_lcell
-- Equation(s):
-- Dout_ff_adffs_a23_a = DFFE(!reset_acombout & Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "0F00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datac => reset_acombout,
	datad => Dout_Dif_as_aadder_aresult_node_acs_buffer_a0_a_a597,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => Dout_ff_adffs_a23_a);

AC_Out_FF_adffs_a15_a_aI : apex20ke_lcell
-- Equation(s):
-- AC_Out_FF_adffs_a15_a = DFFE(Dout_ff_adffs_a23_a, GLOBAL(clk_acombout), !GLOBAL(imr_acombout), , )

-- pragma translate_off
GENERIC MAP (
	operation_mode => "normal",
	packed_mode => "false",
	lut_mask => "FF00",
	output_mode => "reg_only")
-- pragma translate_on
PORT MAP (
	datad => Dout_ff_adffs_a23_a,
	clk => clk_acombout,
	aclr => imr_acombout,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => AC_Out_FF_adffs_a15_a);

AC_Out_a0_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a0_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(0));

AC_Out_a1_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a1_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(1));

AC_Out_a2_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a2_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(2));

AC_Out_a3_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a3_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(3));

AC_Out_a4_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a4_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(4));

AC_Out_a5_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a5_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(5));

AC_Out_a6_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a6_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(6));

AC_Out_a7_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a7_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(7));

AC_Out_a8_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a8_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(8));

AC_Out_a9_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a9_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(9));

AC_Out_a10_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a10_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(10));

AC_Out_a11_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a11_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(11));

AC_Out_a12_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a12_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(12));

AC_Out_a13_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a13_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(13));

AC_Out_a14_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a14_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(14));

AC_Out_a15_a_aI : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	operation_mode => "output",
	reg_source_mode => "none",
	feedback_mode => "none",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	datain => AC_Out_FF_adffs_a15_a,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(15));
END structure;


