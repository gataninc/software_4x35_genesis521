-- Copyright (C) 1991-2006 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II"
-- VERSION "Version 5.1 Build 216 03/06/2006 Service Pack 2 SJ Full Version"

-- DATE "04/12/2006 16:24:22"

-- 
-- Device: Altera EP20K300EQC240-2X Package PQFP240
-- 

-- 
-- This VHDL file should be used for ModelSim (VHDL) only
-- 

LIBRARY IEEE, apex20ke;
USE IEEE.std_logic_1164.all;
USE apex20ke.apex20ke_components.all;

ENTITY 	ac_couple IS
    PORT (
	imr : IN std_logic;
	clk : IN std_logic;
	reset : IN std_logic;
	Din : IN std_logic_vector(15 DOWNTO 0);
	AC_Out : OUT std_logic_vector(15 DOWNTO 0)
	);
END ac_couple;

ARCHITECTURE structure OF ac_couple IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL devoe : std_logic := '0';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_imr : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_reset : std_logic;
SIGNAL ww_Din : std_logic_vector(15 DOWNTO 0);
SIGNAL ww_AC_Out : std_logic_vector(15 DOWNTO 0);
SIGNAL \Dout_ff|dffs[23]~276\ : std_logic;
SIGNAL \Dout_Mult[8]~231\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~522\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~526\ : std_logic;
SIGNAL \Dout_Mult[11]~234\ : std_logic;
SIGNAL \Dout_Mult[12]~235\ : std_logic;
SIGNAL \Dout_Mult[13]~236\ : std_logic;
SIGNAL \Dout_Mult[14]~237\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~546\ : std_logic;
SIGNAL \Dout_Mult[16]~239\ : std_logic;
SIGNAL \Dout_ff|dffs[24]~282\ : std_logic;
SIGNAL \Dout_ff|dffs[24]\ : std_logic;
SIGNAL \Dout_Mult[17]~240\ : std_logic;
SIGNAL \Dout_ff|dffs[25]~285\ : std_logic;
SIGNAL \Dout_ff|dffs[25]\ : std_logic;
SIGNAL \Dout_Mult[18]~241\ : std_logic;
SIGNAL \Dout_ff|dffs[26]~288\ : std_logic;
SIGNAL \Dout_ff|dffs[26]\ : std_logic;
SIGNAL \Dout_Mult[19]~242\ : std_logic;
SIGNAL \Dout_ff|dffs[27]~291\ : std_logic;
SIGNAL \Dout_ff|dffs[27]\ : std_logic;
SIGNAL \Dout_Mult[20]~243\ : std_logic;
SIGNAL \Dout_ff|dffs[28]~294\ : std_logic;
SIGNAL \Dout_ff|dffs[28]\ : std_logic;
SIGNAL \Dout_Mult[21]~244\ : std_logic;
SIGNAL \Dout_ff|dffs[29]~297\ : std_logic;
SIGNAL \Dout_ff|dffs[29]\ : std_logic;
SIGNAL \Dout_Mult[22]~245\ : std_logic;
SIGNAL \Dout_ff|dffs[30]~300\ : std_logic;
SIGNAL \Dout_ff|dffs[30]\ : std_logic;
SIGNAL \Dout_Mult[23]~246\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~580\ : std_logic;
SIGNAL \Dout_ff|dffs[6]\ : std_logic;
SIGNAL \Dout_ff|dffs[31]\ : std_logic;
SIGNAL \Dout_Mult[24]~248\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~584\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~588\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~586\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~592\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~596\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~594\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~600\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~604\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~602\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~608\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\ : std_logic;
SIGNAL \Dout_ff|dffs[5]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[1]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[6]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[7]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[0]\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~610\ : std_logic;
SIGNAL \Dout_ff|dffs[4]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[8]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|_~463\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[10]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[11]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[12]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[14]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[15]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[13]~100\ : std_logic;
SIGNAL \Dout_ff|dffs[3]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[12]~101\ : std_logic;
SIGNAL \Dout_ff|dffs[2]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[11]~102\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[10]~103\ : std_logic;
SIGNAL \Dout_Mult[1]~254\ : std_logic;
SIGNAL \Dout_ff|dffs[0]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[9]~104\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[8]~105\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[7]~106\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[6]~107\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[5]~108\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[4]~109\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[3]~110\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[2]~111\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[1]~112\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[0]\ : std_logic;
SIGNAL \Din[0]~combout\ : std_logic;
SIGNAL \clk~combout\ : std_logic;
SIGNAL \imr~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[0]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[0]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[0]\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\ : std_logic;
SIGNAL \Din[14]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[14]\ : std_logic;
SIGNAL \Din[13]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[13]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[13]\ : std_logic;
SIGNAL \Din[12]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[12]\ : std_logic;
SIGNAL \Din[11]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[11]\ : std_logic;
SIGNAL \Din[10]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[10]\ : std_logic;
SIGNAL \Din[9]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[9]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[9]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|unreg_res_node[1]~148\ : std_logic;
SIGNAL \Din[8]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[8]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[1]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[1]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[2]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[3]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[4]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[5]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[6]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[5]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[4]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[3]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[2]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\ : std_logic;
SIGNAL \Din[7]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[7]\ : std_logic;
SIGNAL \Din[6]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[6]\ : std_logic;
SIGNAL \Din[5]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[5]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[5]\ : std_logic;
SIGNAL \Din[4]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[4]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[4]\ : std_logic;
SIGNAL \Din[3]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[3]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[3]\ : std_logic;
SIGNAL \Din[2]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[2]\ : std_logic;
SIGNAL \Din_Del2_ff|dffs[2]\ : std_logic;
SIGNAL \Din[1]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[1]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[0]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[1]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[2]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[3]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[4]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[5]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[6]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|cout[7]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[8]\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[7]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[6]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[5]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[4]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[3]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[2]\ : std_logic;
SIGNAL \data_add_sub|adder1[0]|result_node|sout_node[1]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[0]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[1]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[2]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[3]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[4]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[5]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[6]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[7]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[8]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[9]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[10]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[11]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[12]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[13]~COUT\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[14]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[13]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[12]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[11]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[10]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[9]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[8]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[7]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[6]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[5]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[4]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[3]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[2]\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[1]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[1]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[2]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[3]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[4]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[5]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[6]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[7]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[8]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[9]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[10]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[11]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[12]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[13]\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|lcarry[14]~125\ : std_logic;
SIGNAL \diff_reg_Abs|lcarry[14]~COUT\ : std_logic;
SIGNAL \abs_cmp|comparator|cmp_end|agb_out\ : std_logic;
SIGNAL \Abs_Thr_Vec[0]\ : std_logic;
SIGNAL \Abs_Thr_Vec[1]\ : std_logic;
SIGNAL \Abs_Thr_Vec[2]\ : std_logic;
SIGNAL \Abs_Thr_Vec[3]\ : std_logic;
SIGNAL \Equal~66\ : std_logic;
SIGNAL \Abs_Thr_Vec[4]\ : std_logic;
SIGNAL \Abs_Thr_Vec[5]\ : std_logic;
SIGNAL \Abs_Thr_Vec[6]\ : std_logic;
SIGNAL \Abs_Thr_Vec[7]\ : std_logic;
SIGNAL \Equal~65\ : std_logic;
SIGNAL offset_en : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~520\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~524\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~528\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~532\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~536\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~540\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~538\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~530\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~544\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~548\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~552\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~554\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~556\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~560\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~564\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~568\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~572\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~570\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~562\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\ : std_logic;
SIGNAL \reset~combout\ : std_logic;
SIGNAL \Dout_ff|dffs[17]~258\ : std_logic;
SIGNAL \Dout_ff|dffs[18]~261\ : std_logic;
SIGNAL \Dout_ff|dffs[19]~264\ : std_logic;
SIGNAL \Dout_ff|dffs[20]~267\ : std_logic;
SIGNAL \Dout_ff|dffs[21]~270\ : std_logic;
SIGNAL \Dout_ff|dffs[22]\ : std_logic;
SIGNAL \Dout_Mult[15]~238\ : std_logic;
SIGNAL \Dout_ff|dffs[14]~249\ : std_logic;
SIGNAL \Dout_ff|dffs[15]~252\ : std_logic;
SIGNAL \Dout_ff|dffs[16]~255\ : std_logic;
SIGNAL \Dout_ff|dffs[17]\ : std_logic;
SIGNAL \Dout_Mult[10]~233\ : std_logic;
SIGNAL \Dout_ff|dffs[16]\ : std_logic;
SIGNAL \Dout_Mult[9]~232\ : std_logic;
SIGNAL \Dout_ff|dffs[8]~231\ : std_logic;
SIGNAL \Dout_ff|dffs[9]~234\ : std_logic;
SIGNAL \Dout_ff|dffs[10]~237\ : std_logic;
SIGNAL \Dout_ff|dffs[11]~240\ : std_logic;
SIGNAL \Dout_ff|dffs[12]~243\ : std_logic;
SIGNAL \Dout_ff|dffs[13]~246\ : std_logic;
SIGNAL \Dout_ff|dffs[14]\ : std_logic;
SIGNAL \Dout_Mult[7]~247\ : std_logic;
SIGNAL \Dout_ff|dffs[13]\ : std_logic;
SIGNAL \Dout_Mult[6]~249\ : std_logic;
SIGNAL \Dout_ff|dffs[12]\ : std_logic;
SIGNAL \Dout_Mult[5]~250\ : std_logic;
SIGNAL \Dout_ff|dffs[11]\ : std_logic;
SIGNAL \Dout_Mult[4]~251\ : std_logic;
SIGNAL \Dout_ff|dffs[10]\ : std_logic;
SIGNAL \Dout_Mult[3]~252\ : std_logic;
SIGNAL \Dout_ff|dffs[9]\ : std_logic;
SIGNAL \Dout_Mult[2]~253\ : std_logic;
SIGNAL \Dout_ff|dffs[7]\ : std_logic;
SIGNAL \Dout_Mult[0]~255\ : std_logic;
SIGNAL \Dout_ff|dffs[0]~324\ : std_logic;
SIGNAL \Dout_ff|dffs[1]\ : std_logic;
SIGNAL \Dout_ff|dffs[1]~321\ : std_logic;
SIGNAL \Dout_ff|dffs[2]~318\ : std_logic;
SIGNAL \Dout_ff|dffs[3]~315\ : std_logic;
SIGNAL \Dout_ff|dffs[4]~312\ : std_logic;
SIGNAL \Dout_ff|dffs[5]~309\ : std_logic;
SIGNAL \Dout_ff|dffs[6]~303\ : std_logic;
SIGNAL \Dout_ff|dffs[7]~279\ : std_logic;
SIGNAL \Dout_ff|dffs[8]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[0]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[1]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[2]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[3]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[4]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[5]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[6]\ : std_logic;
SIGNAL \Dout_ff|dffs[15]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[7]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[8]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[9]\ : std_logic;
SIGNAL \Dout_ff|dffs[18]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[10]\ : std_logic;
SIGNAL \Dout_ff|dffs[19]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[11]\ : std_logic;
SIGNAL \Dout_ff|dffs[20]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[12]\ : std_logic;
SIGNAL \Dout_ff|dffs[21]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[13]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[14]\ : std_logic;
SIGNAL \Din[15]~combout\ : std_logic;
SIGNAL \Din_Del1_ff|dffs[15]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|cout[6]\ : std_logic;
SIGNAL \data_add_sub|adder1_0[1]|result_node|sout_node[7]\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~234\ : std_logic;
SIGNAL \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~576\ : std_logic;
SIGNAL \Dout_add_Sub|adder|result_node|cs_buffer[8]~578\ : std_logic;
SIGNAL \Dout_ff|dffs[22]~273\ : std_logic;
SIGNAL \Dout_ff|dffs[23]\ : std_logic;
SIGNAL \AC_Out_FF|dffs[15]\ : std_logic;

BEGIN

ww_imr <= imr;
ww_clk <= clk;
ww_reset <= reset;
ww_Din <= Din;
AC_Out <= ww_AC_Out;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\Dout_ff|dffs[23]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[23]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[23]~246\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~578\ $ !\Dout_ff|dffs[22]~273\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[23]~276\ = CARRY(\Dout_Mult[23]~246\ & (!\Dout_ff|dffs[22]~273\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~578\) # !\Dout_Mult[23]~246\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~578\ & !\Dout_ff|dffs[22]~273\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[23]~246\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~578\,
	cin => \Dout_ff|dffs[22]~273\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[23]\,
	cout => \Dout_ff|dffs[23]~276\);

\Dout_Mult[8]~231_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[8]~231\ = !offset_en & \Dout_ff|dffs[15]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[8]~231\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~522_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~522\ = \Dout_ff|dffs[9]\ $ \data_add_sub|adder1[0]|result_node|sout_node[1]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~520\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~524\ = CARRY(\Dout_ff|dffs[9]\ & !\data_add_sub|adder1[0]|result_node|sout_node[1]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~520\ # !\Dout_ff|dffs[9]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~520\ # !\data_add_sub|adder1[0]|result_node|sout_node[1]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[9]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[1]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~520\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~522\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~524\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~526_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~526\ = \Dout_ff|dffs[10]\ $ \data_add_sub|adder1[0]|result_node|sout_node[2]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~524\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~528\ = CARRY(\Dout_ff|dffs[10]\ & (\data_add_sub|adder1[0]|result_node|sout_node[2]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~524\) # !\Dout_ff|dffs[10]\ & 
-- \data_add_sub|adder1[0]|result_node|sout_node[2]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~524\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[10]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[2]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~524\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~526\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~528\);

\Dout_Mult[11]~234_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[11]~234\ = !offset_en & (\Dout_ff|dffs[18]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[18]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[11]~234\);

\Dout_Mult[12]~235_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[12]~235\ = !offset_en & \Dout_ff|dffs[19]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[19]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[12]~235\);

\Dout_Mult[13]~236_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[13]~236\ = !offset_en & (\Dout_ff|dffs[20]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[20]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[13]~236\);

\Dout_Mult[14]~237_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[14]~237\ = !offset_en & (\Dout_ff|dffs[21]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[21]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[14]~237\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~546_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~546\ = \Dout_ff|dffs[15]\ $ \data_add_sub|adder1[0]|result_node|sout_node[7]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~544\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~548\ = CARRY(\Dout_ff|dffs[15]\ & !\data_add_sub|adder1[0]|result_node|sout_node[7]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~544\ # !\Dout_ff|dffs[15]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~544\ # !\data_add_sub|adder1[0]|result_node|sout_node[7]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[15]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[7]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~544\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~546\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~548\);

\Dout_Mult[16]~239_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[16]~239\ = !offset_en & (\Dout_ff|dffs[23]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[23]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[16]~239\);

\Dout_ff|dffs[24]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[24]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\ $ \Dout_ff|dffs[23]~276\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[24]~282\ = CARRY(\Dout_Mult[24]~248\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\ & !\Dout_ff|dffs[23]~276\ # !\Dout_Mult[24]~248\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~582\ # !\Dout_ff|dffs[23]~276\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\,
	cin => \Dout_ff|dffs[23]~276\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[24]\,
	cout => \Dout_ff|dffs[24]~282\);

\Dout_Mult[17]~240_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[17]~240\ = !offset_en & (\Dout_ff|dffs[24]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[24]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[17]~240\);

\Dout_ff|dffs[25]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[25]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~586\ $ !\Dout_ff|dffs[24]~282\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[25]~285\ = CARRY(\Dout_Mult[24]~248\ & (!\Dout_ff|dffs[24]~282\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~586\) # !\Dout_Mult[24]~248\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~586\ & !\Dout_ff|dffs[24]~282\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~586\,
	cin => \Dout_ff|dffs[24]~282\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[25]\,
	cout => \Dout_ff|dffs[25]~285\);

\Dout_Mult[18]~241_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[18]~241\ = !offset_en & \Dout_ff|dffs[25]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[25]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[18]~241\);

\Dout_ff|dffs[26]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[26]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\ $ \Dout_ff|dffs[25]~285\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[26]~288\ = CARRY(\Dout_Mult[24]~248\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\ & !\Dout_ff|dffs[25]~285\ # !\Dout_Mult[24]~248\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~590\ # !\Dout_ff|dffs[25]~285\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\,
	cin => \Dout_ff|dffs[25]~285\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[26]\,
	cout => \Dout_ff|dffs[26]~288\);

\Dout_Mult[19]~242_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[19]~242\ = !offset_en & \Dout_ff|dffs[26]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3030",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datac => \Dout_ff|dffs[26]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[19]~242\);

\Dout_ff|dffs[27]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[27]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~594\ $ !\Dout_ff|dffs[26]~288\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[27]~291\ = CARRY(\Dout_Mult[24]~248\ & (!\Dout_ff|dffs[26]~288\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~594\) # !\Dout_Mult[24]~248\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~594\ & !\Dout_ff|dffs[26]~288\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~594\,
	cin => \Dout_ff|dffs[26]~288\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[27]\,
	cout => \Dout_ff|dffs[27]~291\);

\Dout_Mult[20]~243_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[20]~243\ = \Dout_ff|dffs[27]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[27]\,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[20]~243\);

\Dout_ff|dffs[28]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[28]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\ $ \Dout_ff|dffs[27]~291\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[28]~294\ = CARRY(\Dout_Mult[24]~248\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\ & !\Dout_ff|dffs[27]~291\ # !\Dout_Mult[24]~248\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~598\ # !\Dout_ff|dffs[27]~291\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\,
	cin => \Dout_ff|dffs[27]~291\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[28]\,
	cout => \Dout_ff|dffs[28]~294\);

\Dout_Mult[21]~244_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[21]~244\ = \Dout_ff|dffs[28]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[28]\,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[21]~244\);

\Dout_ff|dffs[29]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[29]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~602\ $ !\Dout_ff|dffs[28]~294\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[29]~297\ = CARRY(\Dout_Mult[24]~248\ & (!\Dout_ff|dffs[28]~294\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~602\) # !\Dout_Mult[24]~248\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~602\ & !\Dout_ff|dffs[28]~294\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~602\,
	cin => \Dout_ff|dffs[28]~294\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[29]\,
	cout => \Dout_ff|dffs[29]~297\);

\Dout_Mult[22]~245_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[22]~245\ = \Dout_ff|dffs[29]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[29]\,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[22]~245\);

\Dout_ff|dffs[30]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[30]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\ $ \Dout_ff|dffs[29]~297\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[30]~300\ = CARRY(\Dout_Mult[24]~248\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\ & !\Dout_ff|dffs[29]~297\ # !\Dout_Mult[24]~248\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~606\ # !\Dout_ff|dffs[29]~297\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\,
	cin => \Dout_ff|dffs[29]~297\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[30]\,
	cout => \Dout_ff|dffs[30]~300\);

\Dout_Mult[23]~246_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[23]~246\ = !offset_en & (\Dout_ff|dffs[30]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_en,
	datad => \Dout_ff|dffs[30]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[23]~246\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~578_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~578\ = \Dout_ff|dffs[23]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~576\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~580\ = CARRY(\Dout_ff|dffs[23]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~576\ # !\Dout_ff|dffs[23]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~576\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[23]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~576\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~578\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~580\);

\Dout_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[6]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[6]\ $ \Dout_Mult[6]~249\ $ \Dout_ff|dffs[5]~309\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[6]~303\ = CARRY(\Dout_ff|dffs[6]\ & (!\Dout_ff|dffs[5]~309\ # !\Dout_Mult[6]~249\) # !\Dout_ff|dffs[6]\ & !\Dout_Mult[6]~249\ & !\Dout_ff|dffs[5]~309\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[6]\,
	datab => \Dout_Mult[6]~249\,
	cin => \Dout_ff|dffs[5]~309\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[6]\,
	cout => \Dout_ff|dffs[6]~303\);

\Dout_ff|dffs[31]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[31]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[24]~248\ $ (\Dout_ff|dffs[30]~300\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~610\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "5AA5",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[24]~248\,
	datad => \Dout_add_Sub|adder|result_node|cs_buffer[8]~610\,
	cin => \Dout_ff|dffs[30]~300\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[31]\);

\Dout_Mult[24]~248_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[24]~248\ = !offset_en & (\Dout_ff|dffs[31]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5500",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => offset_en,
	datad => \Dout_ff|dffs[31]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[24]~248\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~582_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\ = \Dout_ff|dffs[24]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~580\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~584\ = CARRY(\Dout_ff|dffs[24]\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~580\) # !\Dout_ff|dffs[24]\ & 
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~580\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[24]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~580\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~582\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~584\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~586_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~586\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[25]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~584\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~588\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\Dout_ff|dffs[25]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~584\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~584\ # !\Dout_ff|dffs[25]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[25]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~584\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~586\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~588\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~590_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[26]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~588\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~592\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & (\Dout_ff|dffs[26]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~588\) # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- \Dout_ff|dffs[26]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~588\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[26]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~588\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~590\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~592\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~594_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~594\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[27]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~592\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~596\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\Dout_ff|dffs[27]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~592\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~592\ # !\Dout_ff|dffs[27]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[27]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~592\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~594\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~596\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~598_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[28]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~596\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~600\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & (\Dout_ff|dffs[28]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~596\) # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- \Dout_ff|dffs[28]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~596\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[28]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~596\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~598\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~600\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~602_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~602\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[29]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~600\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~604\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\Dout_ff|dffs[29]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~600\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~600\ # !\Dout_ff|dffs[29]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[29]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~600\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~602\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~604\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~606_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \Dout_ff|dffs[30]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~604\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~608\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & (\Dout_ff|dffs[30]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~604\) # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & 
-- \Dout_ff|dffs[30]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~604\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \Dout_ff|dffs[30]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~604\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~606\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~608\);

\Dout_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[5]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[5]\ $ \Dout_Mult[5]~250\ $ !\Dout_ff|dffs[4]~312\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[5]~309\ = CARRY(\Dout_ff|dffs[5]\ & \Dout_Mult[5]~250\ & !\Dout_ff|dffs[4]~312\ # !\Dout_ff|dffs[5]\ & (\Dout_Mult[5]~250\ # !\Dout_ff|dffs[4]~312\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[5]\,
	datab => \Dout_Mult[5]~250\,
	cin => \Dout_ff|dffs[4]~312\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[5]\,
	cout => \Dout_ff|dffs[5]~309\);

\Din_Del2_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[1]\ = DFFE(\Din_Del1_ff|dffs[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[1]\);

\Din_Del2_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[6]\ = DFFE(\Din_Del1_ff|dffs[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[6]\);

\Din_Del2_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[7]\ = DFFE(\Din_Del1_ff|dffs[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Din_Del1_ff|dffs[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[7]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[0]\ = DFFE(\Din_Del1_ff|dffs[8]\ $ !\Din_Del2_ff|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Din_Del1_ff|dffs[8]\,
	datad => \Din_Del2_ff|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[0]\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~610_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~610\ = \Dout_ff|dffs[31]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~608\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C33C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Dout_ff|dffs[31]\,
	datad => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~608\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~610\);

\Dout_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[4]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[4]\ $ \Dout_Mult[4]~251\ $ \Dout_ff|dffs[3]~315\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[4]~312\ = CARRY(\Dout_ff|dffs[4]\ & (!\Dout_ff|dffs[3]~315\ # !\Dout_Mult[4]~251\) # !\Dout_ff|dffs[4]\ & !\Dout_Mult[4]~251\ & !\Dout_ff|dffs[3]~315\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[4]\,
	datab => \Dout_Mult[4]~251\,
	cin => \Dout_ff|dffs[3]~315\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[4]\,
	cout => \Dout_ff|dffs[4]~312\);

\Din_Del2_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[8]\ = DFFE(\Din_Del1_ff|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[8]\);

\data_add_sub|adder1_0[1]|_~463_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|_~463\ = \Din_Del1_ff|dffs[9]\ & (!\Din_Del2_ff|dffs[8]\ & \Din_Del1_ff|dffs[8]\ # !\Din_Del2_ff|dffs[9]\) # !\Din_Del1_ff|dffs[9]\ & !\Din_Del2_ff|dffs[8]\ & !\Din_Del2_ff|dffs[9]\ & \Din_Del1_ff|dffs[8]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "2B0A",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[9]\,
	datab => \Din_Del2_ff|dffs[8]\,
	datac => \Din_Del2_ff|dffs[9]\,
	datad => \Din_Del1_ff|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1_0[1]|_~463\);

\Din_Del2_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[10]\ = DFFE(\Din_Del1_ff|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[10]\);

\Din_Del2_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[11]\ = DFFE(\Din_Del1_ff|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[11]\);

\Din_Del2_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[12]\ = DFFE(\Din_Del1_ff|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[12]\);

\Din_Del2_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[14]\ = DFFE(\Din_Del1_ff|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[14]\);

\Din_Del2_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[15]\ = DFFE(\Din_Del1_ff|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Din_Del1_ff|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[15]\);

\Dout_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[3]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[3]\ $ \Dout_Mult[3]~252\ $ !\Dout_ff|dffs[2]~318\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[3]~315\ = CARRY(\Dout_ff|dffs[3]\ & \Dout_Mult[3]~252\ & !\Dout_ff|dffs[2]~318\ # !\Dout_ff|dffs[3]\ & (\Dout_Mult[3]~252\ # !\Dout_ff|dffs[2]~318\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[3]\,
	datab => \Dout_Mult[3]~252\,
	cin => \Dout_ff|dffs[2]~318\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[3]\,
	cout => \Dout_ff|dffs[3]~315\);

\Dout_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[2]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[2]\ $ \Dout_Mult[2]~253\ $ \Dout_ff|dffs[1]~321\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[2]~318\ = CARRY(\Dout_ff|dffs[2]\ & (!\Dout_ff|dffs[1]~321\ # !\Dout_Mult[2]~253\) # !\Dout_ff|dffs[2]\ & !\Dout_Mult[2]~253\ & !\Dout_ff|dffs[1]~321\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[2]\,
	datab => \Dout_Mult[2]~253\,
	cin => \Dout_ff|dffs[1]~321\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[2]\,
	cout => \Dout_ff|dffs[2]~318\);

\Dout_Mult[1]~254_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[1]~254\ = \Dout_ff|dffs[8]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[8]\,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[1]~254\);

\Dout_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[0]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[0]\ $ \Dout_Mult[0]~255\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[0]~324\ = CARRY(\Dout_ff|dffs[0]\ # !\Dout_Mult[0]~255\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[0]\,
	datab => \Dout_Mult[0]~255\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[0]\,
	cout => \Dout_ff|dffs[0]~324\);

\Din[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(0),
	combout => \Din[0]~combout\);

\clk~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_clk,
	combout => \clk~combout\);

\imr~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_imr,
	combout => \imr~combout\);

\Din_Del1_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[0]\ = DFFE(\Din[0]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[0]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[0]\);

\Din_Del2_ff|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[0]\ = DFFE(\Din_Del1_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[0]\);

\data_add_sub|adder1[0]|result_node|sout_node[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[0]\ = DFFE(\Din_Del1_ff|dffs[0]\ $ \Din_Del2_ff|dffs[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[0]\ = CARRY(\Din_Del1_ff|dffs[0]\ # !\Din_Del2_ff|dffs[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "66BB",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[0]\,
	datab => \Din_Del2_ff|dffs[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[0]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[0]\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~518_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\ = \Dout_ff|dffs[8]\ $ \data_add_sub|adder1[0]|result_node|sout_node[0]\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~520\ = CARRY(\Dout_ff|dffs[8]\ & \data_add_sub|adder1[0]|result_node|sout_node[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[8]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~520\);

\Din[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(14),
	combout => \Din[14]~combout\);

\Din_Del1_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[14]\ = DFFE(\Din[14]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[14]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[14]\);

\Din[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(13),
	combout => \Din[13]~combout\);

\Din_Del1_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[13]\ = DFFE(\Din[13]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[13]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[13]\);

\Din_Del2_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[13]\ = DFFE(\Din_Del1_ff|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Din_Del1_ff|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[13]\);

\Din[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(12),
	combout => \Din[12]~combout\);

\Din_Del1_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[12]\ = DFFE(\Din[12]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[12]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[12]\);

\Din[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(11),
	combout => \Din[11]~combout\);

\Din_Del1_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[11]\ = DFFE(\Din[11]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[11]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[11]\);

\Din[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(10),
	combout => \Din[10]~combout\);

\Din_Del1_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[10]\ = DFFE(\Din[10]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[10]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[10]\);

\Din[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(9),
	combout => \Din[9]~combout\);

\Din_Del1_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[9]\ = DFFE(\Din[9]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[9]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[9]\);

\Din_Del2_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[9]\ = DFFE(\Din_Del1_ff|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[9]\);

\data_add_sub|adder1_0[1]|unreg_res_node[1]~148_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|unreg_res_node[1]~148\ = \Din_Del1_ff|dffs[9]\ $ (\Din_Del2_ff|dffs[9]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "33CC",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Din_Del1_ff|dffs[9]\,
	datad => \Din_Del2_ff|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1_0[1]|unreg_res_node[1]~148\);

\Din[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(8),
	combout => \Din[8]~combout\);

\Din_Del1_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[8]\ = DFFE(\Din[8]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[8]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[8]\);

\data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64\ = !\Din_Del2_ff|dffs[8]\ & (\Din_Del1_ff|dffs[8]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "5050",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[8]\,
	datac => \Din_Del1_ff|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64\);

\data_add_sub|adder1_0[1]|result_node|sout_node[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[1]\ = DFFE(\data_add_sub|adder1_0[1]|unreg_res_node[1]~148\ $ !\data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F00F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \data_add_sub|adder1_0[1]|unreg_res_node[1]~148\,
	datad => \data_add_sub|adder1_0[1]|result_node|cs_buffer[0]~64\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[1]\);

\data_add_sub|adder1_0[1]|result_node|cs_buffer[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\ = \data_add_sub|adder1_0[1]|result_node|sout_node[1]\
-- \data_add_sub|adder1_0[1]|result_node|cout[1]\ = CARRY(\data_add_sub|adder1_0[1]|_~463\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "CCAA",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1_0[1]|_~463\,
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[1]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[2]\ = DFFE(\Din_Del2_ff|dffs[10]\ $ \Din_Del1_ff|dffs[10]\ $ !\data_add_sub|adder1_0[1]|result_node|cout[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1_0[1]|result_node|cout[2]\ = CARRY(\Din_Del2_ff|dffs[10]\ & (!\data_add_sub|adder1_0[1]|result_node|cout[1]\ # !\Din_Del1_ff|dffs[10]\) # !\Din_Del2_ff|dffs[10]\ & !\Din_Del1_ff|dffs[10]\ & 
-- !\data_add_sub|adder1_0[1]|result_node|cout[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[10]\,
	datab => \Din_Del1_ff|dffs[10]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[2]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[2]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[3]\ = DFFE(\Din_Del2_ff|dffs[11]\ $ \Din_Del1_ff|dffs[11]\ $ \data_add_sub|adder1_0[1]|result_node|cout[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1_0[1]|result_node|cout[3]\ = CARRY(\Din_Del2_ff|dffs[11]\ & \Din_Del1_ff|dffs[11]\ & !\data_add_sub|adder1_0[1]|result_node|cout[2]\ # !\Din_Del2_ff|dffs[11]\ & (\Din_Del1_ff|dffs[11]\ # 
-- !\data_add_sub|adder1_0[1]|result_node|cout[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[11]\,
	datab => \Din_Del1_ff|dffs[11]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[3]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[3]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[4]\ = DFFE(\Din_Del2_ff|dffs[12]\ $ \Din_Del1_ff|dffs[12]\ $ !\data_add_sub|adder1_0[1]|result_node|cout[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1_0[1]|result_node|cout[4]\ = CARRY(\Din_Del2_ff|dffs[12]\ & (!\data_add_sub|adder1_0[1]|result_node|cout[3]\ # !\Din_Del1_ff|dffs[12]\) # !\Din_Del2_ff|dffs[12]\ & !\Din_Del1_ff|dffs[12]\ & 
-- !\data_add_sub|adder1_0[1]|result_node|cout[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[12]\,
	datab => \Din_Del1_ff|dffs[12]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[4]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[4]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[5]\ = DFFE(\Din_Del1_ff|dffs[13]\ $ \Din_Del2_ff|dffs[13]\ $ \data_add_sub|adder1_0[1]|result_node|cout[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1_0[1]|result_node|cout[5]\ = CARRY(\Din_Del1_ff|dffs[13]\ & (!\data_add_sub|adder1_0[1]|result_node|cout[4]\ # !\Din_Del2_ff|dffs[13]\) # !\Din_Del1_ff|dffs[13]\ & !\Din_Del2_ff|dffs[13]\ & 
-- !\data_add_sub|adder1_0[1]|result_node|cout[4]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[13]\,
	datab => \Din_Del2_ff|dffs[13]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[5]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[5]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[6]\ = DFFE(\Din_Del2_ff|dffs[14]\ $ \Din_Del1_ff|dffs[14]\ $ !\data_add_sub|adder1_0[1]|result_node|cout[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1_0[1]|result_node|cout[6]\ = CARRY(\Din_Del2_ff|dffs[14]\ & (!\data_add_sub|adder1_0[1]|result_node|cout[5]\ # !\Din_Del1_ff|dffs[14]\) # !\Din_Del2_ff|dffs[14]\ & !\Din_Del1_ff|dffs[14]\ & 
-- !\data_add_sub|adder1_0[1]|result_node|cout[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[14]\,
	datab => \Din_Del1_ff|dffs[14]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[6]\,
	cout => \data_add_sub|adder1_0[1]|result_node|cout[6]\);

\Din[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(7),
	combout => \Din[7]~combout\);

\Din_Del1_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[7]\ = DFFE(\Din[7]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[7]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[7]\);

\Din[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(6),
	combout => \Din[6]~combout\);

\Din_Del1_ff|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[6]\ = DFFE(\Din[6]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[6]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[6]\);

\Din[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(5),
	combout => \Din[5]~combout\);

\Din_Del1_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[5]\ = DFFE(\Din[5]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[5]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[5]\);

\Din_Del2_ff|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[5]\ = DFFE(\Din_Del1_ff|dffs[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Din_Del1_ff|dffs[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[5]\);

\Din[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(4),
	combout => \Din[4]~combout\);

\Din_Del1_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[4]\ = DFFE(\Din[4]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[4]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[4]\);

\Din_Del2_ff|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[4]\ = DFFE(\Din_Del1_ff|dffs[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[4]\);

\Din[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(3),
	combout => \Din[3]~combout\);

\Din_Del1_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[3]\ = DFFE(\Din[3]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[3]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[3]\);

\Din_Del2_ff|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[3]\ = DFFE(\Din_Del1_ff|dffs[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[3]\);

\Din[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(2),
	combout => \Din[2]~combout\);

\Din_Del1_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[2]\ = DFFE(\Din[2]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[2]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[2]\);

\Din_Del2_ff|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del2_ff|dffs[2]\ = DFFE(\Din_Del1_ff|dffs[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din_Del1_ff|dffs[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del2_ff|dffs[2]\);

\Din[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(1),
	combout => \Din[1]~combout\);

\Din_Del1_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[1]\ = DFFE(\Din[1]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[1]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[1]\);

\data_add_sub|adder1[0]|result_node|sout_node[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[1]\ = DFFE(\Din_Del2_ff|dffs[1]\ $ \Din_Del1_ff|dffs[1]\ $ !\data_add_sub|adder1[0]|result_node|cout[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[1]\ = CARRY(\Din_Del2_ff|dffs[1]\ & (!\data_add_sub|adder1[0]|result_node|cout[0]\ # !\Din_Del1_ff|dffs[1]\) # !\Din_Del2_ff|dffs[1]\ & !\Din_Del1_ff|dffs[1]\ & !\data_add_sub|adder1[0]|result_node|cout[0]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[1]\,
	datab => \Din_Del1_ff|dffs[1]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[1]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[1]\);

\data_add_sub|adder1[0]|result_node|sout_node[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[2]\ = DFFE(\Din_Del1_ff|dffs[2]\ $ \Din_Del2_ff|dffs[2]\ $ \data_add_sub|adder1[0]|result_node|cout[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[2]\ = CARRY(\Din_Del1_ff|dffs[2]\ & (!\data_add_sub|adder1[0]|result_node|cout[1]\ # !\Din_Del2_ff|dffs[2]\) # !\Din_Del1_ff|dffs[2]\ & !\Din_Del2_ff|dffs[2]\ & !\data_add_sub|adder1[0]|result_node|cout[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[2]\,
	datab => \Din_Del2_ff|dffs[2]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[2]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[2]\);

\data_add_sub|adder1[0]|result_node|sout_node[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[3]\ = DFFE(\Din_Del1_ff|dffs[3]\ $ \Din_Del2_ff|dffs[3]\ $ !\data_add_sub|adder1[0]|result_node|cout[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[3]\ = CARRY(\Din_Del1_ff|dffs[3]\ & \Din_Del2_ff|dffs[3]\ & !\data_add_sub|adder1[0]|result_node|cout[2]\ # !\Din_Del1_ff|dffs[3]\ & (\Din_Del2_ff|dffs[3]\ # !\data_add_sub|adder1[0]|result_node|cout[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[3]\,
	datab => \Din_Del2_ff|dffs[3]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[3]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[3]\);

\data_add_sub|adder1[0]|result_node|sout_node[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[4]\ = DFFE(\Din_Del1_ff|dffs[4]\ $ \Din_Del2_ff|dffs[4]\ $ \data_add_sub|adder1[0]|result_node|cout[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[4]\ = CARRY(\Din_Del1_ff|dffs[4]\ & (!\data_add_sub|adder1[0]|result_node|cout[3]\ # !\Din_Del2_ff|dffs[4]\) # !\Din_Del1_ff|dffs[4]\ & !\Din_Del2_ff|dffs[4]\ & !\data_add_sub|adder1[0]|result_node|cout[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[4]\,
	datab => \Din_Del2_ff|dffs[4]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[4]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[4]\);

\data_add_sub|adder1[0]|result_node|sout_node[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[5]\ = DFFE(\Din_Del1_ff|dffs[5]\ $ \Din_Del2_ff|dffs[5]\ $ !\data_add_sub|adder1[0]|result_node|cout[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[5]\ = CARRY(\Din_Del1_ff|dffs[5]\ & \Din_Del2_ff|dffs[5]\ & !\data_add_sub|adder1[0]|result_node|cout[4]\ # !\Din_Del1_ff|dffs[5]\ & (\Din_Del2_ff|dffs[5]\ # !\data_add_sub|adder1[0]|result_node|cout[4]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del1_ff|dffs[5]\,
	datab => \Din_Del2_ff|dffs[5]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[5]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[5]\);

\data_add_sub|adder1[0]|result_node|sout_node[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[6]\ = DFFE(\Din_Del2_ff|dffs[6]\ $ \Din_Del1_ff|dffs[6]\ $ \data_add_sub|adder1[0]|result_node|cout[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[6]\ = CARRY(\Din_Del2_ff|dffs[6]\ & \Din_Del1_ff|dffs[6]\ & !\data_add_sub|adder1[0]|result_node|cout[5]\ # !\Din_Del2_ff|dffs[6]\ & (\Din_Del1_ff|dffs[6]\ # !\data_add_sub|adder1[0]|result_node|cout[5]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[6]\,
	datab => \Din_Del1_ff|dffs[6]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[6]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[6]\);

\data_add_sub|adder1[0]|result_node|sout_node[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[7]\ = DFFE(\Din_Del2_ff|dffs[7]\ $ \Din_Del1_ff|dffs[7]\ $ !\data_add_sub|adder1[0]|result_node|cout[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \data_add_sub|adder1[0]|result_node|cout[7]\ = CARRY(\Din_Del2_ff|dffs[7]\ & (!\data_add_sub|adder1[0]|result_node|cout[6]\ # !\Din_Del1_ff|dffs[7]\) # !\Din_Del2_ff|dffs[7]\ & !\Din_Del1_ff|dffs[7]\ & !\data_add_sub|adder1[0]|result_node|cout[6]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "arithmetic",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[7]\,
	datab => \Din_Del1_ff|dffs[7]\,
	cin => \data_add_sub|adder1[0]|result_node|cout[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[7]\,
	cout => \data_add_sub|adder1[0]|result_node|cout[7]\);

\data_add_sub|adder1[0]|result_node|sout_node[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[0]|result_node|sout_node[8]\ = DFFE(!\data_add_sub|adder1[0]|result_node|cout[7]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F0F",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	cin => \data_add_sub|adder1[0]|result_node|cout[7]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1[0]|result_node|sout_node[8]\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~208_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ = \data_add_sub|adder1_0[1]|result_node|sout_node[0]\ $ \data_add_sub|adder1[0]|result_node|sout_node[8]\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\ = CARRY(\data_add_sub|adder1_0[1]|result_node|sout_node[0]\ & \data_add_sub|adder1[0]|result_node|sout_node[8]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "6688",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1_0[1]|result_node|sout_node[0]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~212_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ = \data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\ = CARRY(!\data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\ # !\data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|cs_buffer[1]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~210\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~216_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ = \data_add_sub|adder1_0[1]|result_node|sout_node[2]\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\ = CARRY(\data_add_sub|adder1_0[1]|result_node|sout_node[2]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[2]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~214\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~220_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ = \data_add_sub|adder1_0[1]|result_node|sout_node[3]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\ = CARRY(!\data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\ # !\data_add_sub|adder1_0[1]|result_node|sout_node[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[3]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~218\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~224_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ = \data_add_sub|adder1_0[1]|result_node|sout_node[4]\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\ = CARRY(\data_add_sub|adder1_0[1]|result_node|sout_node[4]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[4]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~222\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~228_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ = \data_add_sub|adder1_0[1]|result_node|sout_node[5]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\ = CARRY(!\data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\ # !\data_add_sub|adder1_0[1]|result_node|sout_node[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "3C3F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[5]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~226\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~232_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ = \data_add_sub|adder1_0[1]|result_node|sout_node[6]\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~234\ = CARRY(\data_add_sub|adder1_0[1]|result_node|sout_node[6]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "C30C",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \data_add_sub|adder1_0[1]|result_node|sout_node[6]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~230\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\,
	cout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~234\);

\diff_reg_Abs|lcarry[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[0]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ & !\data_add_sub|adder1[0]|result_node|sout_node[0]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0022",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[0]\,
	cout => \diff_reg_Abs|lcarry[0]~COUT\);

\diff_reg_Abs|lcarry[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[1]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[1]\ $ \diff_reg_Abs|lcarry[0]~COUT\
-- \diff_reg_Abs|lcarry[1]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[0]|result_node|sout_node[1]\ # !\diff_reg_Abs|lcarry[0]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[1]\,
	cin => \diff_reg_Abs|lcarry[0]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[1]\,
	cout => \diff_reg_Abs|lcarry[1]~COUT\);

\diff_reg_Abs|lcarry[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[2]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[2]\ $ !\diff_reg_Abs|lcarry[1]~COUT\
-- \diff_reg_Abs|lcarry[2]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[1]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[2]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[2]\,
	cin => \diff_reg_Abs|lcarry[1]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[2]\,
	cout => \diff_reg_Abs|lcarry[2]~COUT\);

\diff_reg_Abs|lcarry[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[3]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[3]\ $ \diff_reg_Abs|lcarry[2]~COUT\
-- \diff_reg_Abs|lcarry[3]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[0]|result_node|sout_node[3]\ # !\diff_reg_Abs|lcarry[2]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[3]\,
	cin => \diff_reg_Abs|lcarry[2]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[3]\,
	cout => \diff_reg_Abs|lcarry[3]~COUT\);

\diff_reg_Abs|lcarry[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[4]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[4]\ $ !\diff_reg_Abs|lcarry[3]~COUT\
-- \diff_reg_Abs|lcarry[4]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[3]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[4]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[4]\,
	cin => \diff_reg_Abs|lcarry[3]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[4]\,
	cout => \diff_reg_Abs|lcarry[4]~COUT\);

\diff_reg_Abs|lcarry[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[5]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[5]\ $ \diff_reg_Abs|lcarry[4]~COUT\
-- \diff_reg_Abs|lcarry[5]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[0]|result_node|sout_node[5]\ # !\diff_reg_Abs|lcarry[4]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[5]\,
	cin => \diff_reg_Abs|lcarry[4]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[5]\,
	cout => \diff_reg_Abs|lcarry[5]~COUT\);

\diff_reg_Abs|lcarry[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[6]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[6]\ $ !\diff_reg_Abs|lcarry[5]~COUT\
-- \diff_reg_Abs|lcarry[6]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[5]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[6]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[6]\,
	cin => \diff_reg_Abs|lcarry[5]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[6]\,
	cout => \diff_reg_Abs|lcarry[6]~COUT\);

\diff_reg_Abs|lcarry[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[7]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[0]|result_node|sout_node[7]\ $ \diff_reg_Abs|lcarry[6]~COUT\
-- \diff_reg_Abs|lcarry[7]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[0]|result_node|sout_node[7]\ # !\diff_reg_Abs|lcarry[6]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[7]\,
	cin => \diff_reg_Abs|lcarry[6]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[7]\,
	cout => \diff_reg_Abs|lcarry[7]~COUT\);

\diff_reg_Abs|lcarry[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[8]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ $ !\diff_reg_Abs|lcarry[7]~COUT\
-- \diff_reg_Abs|lcarry[8]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[7]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\,
	cin => \diff_reg_Abs|lcarry[7]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[8]\,
	cout => \diff_reg_Abs|lcarry[8]~COUT\);

\diff_reg_Abs|lcarry[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[9]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ $ \diff_reg_Abs|lcarry[8]~COUT\
-- \diff_reg_Abs|lcarry[9]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ # !\diff_reg_Abs|lcarry[8]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\,
	cin => \diff_reg_Abs|lcarry[8]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[9]\,
	cout => \diff_reg_Abs|lcarry[9]~COUT\);

\diff_reg_Abs|lcarry[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[10]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ $ !\diff_reg_Abs|lcarry[9]~COUT\
-- \diff_reg_Abs|lcarry[10]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[9]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\,
	cin => \diff_reg_Abs|lcarry[9]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[10]\,
	cout => \diff_reg_Abs|lcarry[10]~COUT\);

\diff_reg_Abs|lcarry[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[11]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ $ \diff_reg_Abs|lcarry[10]~COUT\
-- \diff_reg_Abs|lcarry[11]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ # !\diff_reg_Abs|lcarry[10]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\,
	cin => \diff_reg_Abs|lcarry[10]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[11]\,
	cout => \diff_reg_Abs|lcarry[11]~COUT\);

\diff_reg_Abs|lcarry[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[12]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ $ !\diff_reg_Abs|lcarry[11]~COUT\
-- \diff_reg_Abs|lcarry[12]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[11]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\,
	cin => \diff_reg_Abs|lcarry[11]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[12]\,
	cout => \diff_reg_Abs|lcarry[12]~COUT\);

\diff_reg_Abs|lcarry[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[13]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ $ \diff_reg_Abs|lcarry[12]~COUT\
-- \diff_reg_Abs|lcarry[13]~COUT\ = CARRY(\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ # !\diff_reg_Abs|lcarry[12]~COUT\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "969F",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\,
	cin => \diff_reg_Abs|lcarry[12]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[13]\,
	cout => \diff_reg_Abs|lcarry[13]~COUT\);

\diff_reg_Abs|lcarry[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \diff_reg_Abs|lcarry[14]\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ $ !\diff_reg_Abs|lcarry[13]~COUT\
-- \diff_reg_Abs|lcarry[14]~COUT\ = CARRY(!\diff_reg_Abs|lcarry[13]~COUT\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "6906",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\,
	cin => \diff_reg_Abs|lcarry[13]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \diff_reg_Abs|lcarry[14]\,
	cout => \diff_reg_Abs|lcarry[14]~COUT\);

\abs_cmp|comparator|cmp_end|lcarry[1]~112_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[1]\ = CARRY(\diff_reg_Abs|lcarry[1]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00CC",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[1]~112\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[1]\);

\abs_cmp|comparator|cmp_end|lcarry[2]~111_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[2]\ = CARRY(!\abs_cmp|comparator|cmp_end|lcarry[1]\ # !\diff_reg_Abs|lcarry[2]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "003F",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[2]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[2]~111\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[2]\);

\abs_cmp|comparator|cmp_end|lcarry[3]~110_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[3]\ = CARRY(\diff_reg_Abs|lcarry[3]\ # !\abs_cmp|comparator|cmp_end|lcarry[2]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[3]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[3]~110\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[3]\);

\abs_cmp|comparator|cmp_end|lcarry[4]~109_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[4]\ = CARRY(!\diff_reg_Abs|lcarry[4]\ & !\abs_cmp|comparator|cmp_end|lcarry[3]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[4]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[4]~109\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[4]\);

\abs_cmp|comparator|cmp_end|lcarry[5]~108_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[5]\ = CARRY(\diff_reg_Abs|lcarry[5]\ # !\abs_cmp|comparator|cmp_end|lcarry[4]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[5]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[5]~108\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[5]\);

\abs_cmp|comparator|cmp_end|lcarry[6]~107_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[6]\ = CARRY(!\diff_reg_Abs|lcarry[6]\ & !\abs_cmp|comparator|cmp_end|lcarry[5]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[6]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[6]~107\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[6]\);

\abs_cmp|comparator|cmp_end|lcarry[7]~106_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[7]\ = CARRY(\diff_reg_Abs|lcarry[7]\ # !\abs_cmp|comparator|cmp_end|lcarry[6]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[7]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[7]~106\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[7]\);

\abs_cmp|comparator|cmp_end|lcarry[8]~105_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[8]\ = CARRY(!\diff_reg_Abs|lcarry[8]\ & !\abs_cmp|comparator|cmp_end|lcarry[7]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[8]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[8]~105\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[8]\);

\abs_cmp|comparator|cmp_end|lcarry[9]~104_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[9]\ = CARRY(\diff_reg_Abs|lcarry[9]\ # !\abs_cmp|comparator|cmp_end|lcarry[8]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[9]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[9]~104\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[9]\);

\abs_cmp|comparator|cmp_end|lcarry[10]~103_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[10]\ = CARRY(!\diff_reg_Abs|lcarry[10]\ & !\abs_cmp|comparator|cmp_end|lcarry[9]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[10]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[10]~103\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[10]\);

\abs_cmp|comparator|cmp_end|lcarry[11]~102_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[11]\ = CARRY(\diff_reg_Abs|lcarry[11]\ # !\abs_cmp|comparator|cmp_end|lcarry[10]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[11]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[11]~102\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[11]\);

\abs_cmp|comparator|cmp_end|lcarry[12]~101_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[12]\ = CARRY(!\diff_reg_Abs|lcarry[12]\ & !\abs_cmp|comparator|cmp_end|lcarry[11]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0003",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[12]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[12]~101\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[12]\);

\abs_cmp|comparator|cmp_end|lcarry[13]~100_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[13]\ = CARRY(\diff_reg_Abs|lcarry[13]\ # !\abs_cmp|comparator|cmp_end|lcarry[12]\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "00CF",
	operation_mode => "arithmetic",
	output_mode => "none",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \diff_reg_Abs|lcarry[13]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[13]~100\,
	cout => \abs_cmp|comparator|cmp_end|lcarry[13]\);

\abs_cmp|comparator|cmp_end|lcarry[14]~125_I\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|lcarry[14]~125\ = \abs_cmp|comparator|cmp_end|lcarry[13]\ # \diff_reg_Abs|lcarry[14]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "FFF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \diff_reg_Abs|lcarry[14]\,
	cin => \abs_cmp|comparator|cmp_end|lcarry[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|lcarry[14]~125\);

\abs_cmp|comparator|cmp_end|agb_out_node~1\ : apex20ke_lcell
-- Equation(s):
-- \abs_cmp|comparator|cmp_end|agb_out\ = !\diff_reg_Abs|lcarry[14]~COUT\ & \abs_cmp|comparator|cmp_end|lcarry[14]~125\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \abs_cmp|comparator|cmp_end|lcarry[14]~125\,
	cin => \diff_reg_Abs|lcarry[14]~COUT\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \abs_cmp|comparator|cmp_end|agb_out\);

\Abs_Thr_Vec[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[0]\ = DFFE(\abs_cmp|comparator|cmp_end|agb_out\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \abs_cmp|comparator|cmp_end|agb_out\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[0]\);

\Abs_Thr_Vec[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[1]\ = DFFE(\Abs_Thr_Vec[0]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Abs_Thr_Vec[0]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[1]\);

\Abs_Thr_Vec[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[2]\ = DFFE(\Abs_Thr_Vec[1]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Abs_Thr_Vec[1]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[2]\);

\Abs_Thr_Vec[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[3]\ = DFFE(\Abs_Thr_Vec[2]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Abs_Thr_Vec[2]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[3]\);

\Equal~66_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~66\ = !\Abs_Thr_Vec[2]\ & (!\Abs_Thr_Vec[3]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0033",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Abs_Thr_Vec[2]\,
	datad => \Abs_Thr_Vec[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~66\);

\Abs_Thr_Vec[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[4]\ = DFFE(\Abs_Thr_Vec[3]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Abs_Thr_Vec[3]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[4]\);

\Abs_Thr_Vec[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[5]\ = DFFE(\Abs_Thr_Vec[4]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Abs_Thr_Vec[4]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[5]\);

\Abs_Thr_Vec[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[6]\ = DFFE(\Abs_Thr_Vec[5]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Abs_Thr_Vec[5]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[6]\);

\Abs_Thr_Vec[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Abs_Thr_Vec[7]\ = DFFE(\Abs_Thr_Vec[6]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Abs_Thr_Vec[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Abs_Thr_Vec[7]\);

\Equal~65_I\ : apex20ke_lcell
-- Equation(s):
-- \Equal~65\ = !\Abs_Thr_Vec[5]\ & !\Abs_Thr_Vec[6]\ & !\Abs_Thr_Vec[4]\ & !\Abs_Thr_Vec[7]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0001",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Abs_Thr_Vec[5]\,
	datab => \Abs_Thr_Vec[6]\,
	datac => \Abs_Thr_Vec[4]\,
	datad => \Abs_Thr_Vec[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Equal~65\);

\offset_en~I\ : apex20ke_lcell
-- Equation(s):
-- offset_en = DFFE(\Abs_Thr_Vec[0]\ # \Abs_Thr_Vec[1]\ # !\Equal~65\ # !\Equal~66\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "EFFF",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Abs_Thr_Vec[0]\,
	datab => \Abs_Thr_Vec[1]\,
	datac => \Equal~66\,
	datad => \Equal~65\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => offset_en);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~530_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~530\ = \Dout_ff|dffs[11]\ $ \data_add_sub|adder1[0]|result_node|sout_node[3]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~528\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~532\ = CARRY(\Dout_ff|dffs[11]\ & !\data_add_sub|adder1[0]|result_node|sout_node[3]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~528\ # !\Dout_ff|dffs[11]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~528\ # !\data_add_sub|adder1[0]|result_node|sout_node[3]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[11]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[3]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~528\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~530\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~532\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~534_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\ = \Dout_ff|dffs[12]\ $ \data_add_sub|adder1[0]|result_node|sout_node[4]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~532\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~536\ = CARRY(\Dout_ff|dffs[12]\ & (\data_add_sub|adder1[0]|result_node|sout_node[4]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~532\) # !\Dout_ff|dffs[12]\ & 
-- \data_add_sub|adder1[0]|result_node|sout_node[4]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~532\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[12]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[4]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~532\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~536\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~538_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~538\ = \Dout_ff|dffs[13]\ $ \data_add_sub|adder1[0]|result_node|sout_node[5]\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~536\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~540\ = CARRY(\Dout_ff|dffs[13]\ & !\data_add_sub|adder1[0]|result_node|sout_node[5]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~536\ # !\Dout_ff|dffs[13]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~536\ # !\data_add_sub|adder1[0]|result_node|sout_node[5]\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[13]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[5]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~536\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~538\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~540\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~542_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\ = \Dout_ff|dffs[14]\ $ \data_add_sub|adder1[0]|result_node|sout_node[6]\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~540\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~544\ = CARRY(\Dout_ff|dffs[14]\ & (\data_add_sub|adder1[0]|result_node|sout_node[6]\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~540\) # !\Dout_ff|dffs[14]\ & 
-- \data_add_sub|adder1[0]|result_node|sout_node[6]\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~540\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[14]\,
	datab => \data_add_sub|adder1[0]|result_node|sout_node[6]\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~540\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~544\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~550_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\ = \Dout_ff|dffs[16]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~548\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~552\ = CARRY(\Dout_ff|dffs[16]\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~548\) # !\Dout_ff|dffs[16]\ & 
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~548\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[16]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~208\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~548\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~552\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~554_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~554\ = \Dout_ff|dffs[17]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~552\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~556\ = CARRY(\Dout_ff|dffs[17]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~552\ # !\Dout_ff|dffs[17]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~552\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[17]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~212\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~552\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~554\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~556\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~558_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\ = \Dout_ff|dffs[18]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~556\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~560\ = CARRY(\Dout_ff|dffs[18]\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~556\) # !\Dout_ff|dffs[18]\ & 
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~556\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[18]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~216\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~556\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~560\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~562_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~562\ = \Dout_ff|dffs[19]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~560\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~564\ = CARRY(\Dout_ff|dffs[19]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~560\ # !\Dout_ff|dffs[19]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~560\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[19]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~220\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~560\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~562\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~564\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~566_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\ = \Dout_ff|dffs[20]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~564\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~568\ = CARRY(\Dout_ff|dffs[20]\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~564\) # !\Dout_ff|dffs[20]\ & 
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~564\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[20]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~224\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~564\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~568\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~570_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~570\ = \Dout_ff|dffs[21]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~568\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~572\ = CARRY(\Dout_ff|dffs[21]\ & !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~568\ # !\Dout_ff|dffs[21]\ & 
-- (!\Dout_add_Sub|adder|result_node|cs_buffer[8]~568\ # !\data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "9617",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[21]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~228\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~568\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~570\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~572\);

\Dout_add_Sub|adder|result_node|cs_buffer[8]~574_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\ = \Dout_ff|dffs[22]\ $ \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ $ !\Dout_add_Sub|adder|result_node|cs_buffer[8]~572\
-- \Dout_add_Sub|adder|result_node|cs_buffer[8]~576\ = CARRY(\Dout_ff|dffs[22]\ & (\data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~572\) # !\Dout_ff|dffs[22]\ & 
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~572\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "698E",
	operation_mode => "arithmetic",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[22]\,
	datab => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~232\,
	cin => \Dout_add_Sub|adder|result_node|cs_buffer[8]~572\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\,
	cout => \Dout_add_Sub|adder|result_node|cs_buffer[8]~576\);

\reset~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_reset,
	combout => \reset~combout\);

\Dout_ff|dffs[17]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[17]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[17]~240\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~554\ $ !\Dout_ff|dffs[16]~255\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[17]~258\ = CARRY(\Dout_Mult[17]~240\ & (!\Dout_ff|dffs[16]~255\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~554\) # !\Dout_Mult[17]~240\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~554\ & !\Dout_ff|dffs[16]~255\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[17]~240\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~554\,
	cin => \Dout_ff|dffs[16]~255\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[17]\,
	cout => \Dout_ff|dffs[17]~258\);

\Dout_ff|dffs[18]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[18]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[18]~241\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\ $ \Dout_ff|dffs[17]~258\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[18]~261\ = CARRY(\Dout_Mult[18]~241\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\ & !\Dout_ff|dffs[17]~258\ # !\Dout_Mult[18]~241\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~558\ # !\Dout_ff|dffs[17]~258\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[18]~241\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~558\,
	cin => \Dout_ff|dffs[17]~258\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[18]\,
	cout => \Dout_ff|dffs[18]~261\);

\Dout_ff|dffs[19]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[19]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[19]~242\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~562\ $ !\Dout_ff|dffs[18]~261\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[19]~264\ = CARRY(\Dout_Mult[19]~242\ & (!\Dout_ff|dffs[18]~261\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~562\) # !\Dout_Mult[19]~242\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~562\ & !\Dout_ff|dffs[18]~261\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[19]~242\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~562\,
	cin => \Dout_ff|dffs[18]~261\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[19]\,
	cout => \Dout_ff|dffs[19]~264\);

\Dout_ff|dffs[20]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[20]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[20]~243\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\ $ \Dout_ff|dffs[19]~264\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[20]~267\ = CARRY(\Dout_Mult[20]~243\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\ & !\Dout_ff|dffs[19]~264\ # !\Dout_Mult[20]~243\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~566\ # !\Dout_ff|dffs[19]~264\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[20]~243\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~566\,
	cin => \Dout_ff|dffs[19]~264\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[20]\,
	cout => \Dout_ff|dffs[20]~267\);

\Dout_ff|dffs[21]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[21]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[21]~244\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~570\ $ !\Dout_ff|dffs[20]~267\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[21]~270\ = CARRY(\Dout_Mult[21]~244\ & (!\Dout_ff|dffs[20]~267\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~570\) # !\Dout_Mult[21]~244\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~570\ & !\Dout_ff|dffs[20]~267\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[21]~244\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~570\,
	cin => \Dout_ff|dffs[20]~267\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[21]\,
	cout => \Dout_ff|dffs[21]~270\);

\Dout_ff|dffs[22]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[22]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[22]~245\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\ $ \Dout_ff|dffs[21]~270\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[22]~273\ = CARRY(\Dout_Mult[22]~245\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\ & !\Dout_ff|dffs[21]~270\ # !\Dout_Mult[22]~245\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~574\ # !\Dout_ff|dffs[21]~270\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[22]~245\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~574\,
	cin => \Dout_ff|dffs[21]~270\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[22]\,
	cout => \Dout_ff|dffs[22]~273\);

\Dout_Mult[15]~238_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[15]~238\ = \Dout_ff|dffs[22]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0C0C",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => \Dout_ff|dffs[22]\,
	datac => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[15]~238\);

\Dout_ff|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[14]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[14]~237\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\ $ \Dout_ff|dffs[13]~246\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[14]~249\ = CARRY(\Dout_Mult[14]~237\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\ & !\Dout_ff|dffs[13]~246\ # !\Dout_Mult[14]~237\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~542\ # !\Dout_ff|dffs[13]~246\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[14]~237\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~542\,
	cin => \Dout_ff|dffs[13]~246\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[14]\,
	cout => \Dout_ff|dffs[14]~249\);

\Dout_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[15]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_add_Sub|adder|result_node|cs_buffer[8]~546\ $ \Dout_Mult[15]~238\ $ !\Dout_ff|dffs[14]~249\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[15]~252\ = CARRY(\Dout_add_Sub|adder|result_node|cs_buffer[8]~546\ & \Dout_Mult[15]~238\ & !\Dout_ff|dffs[14]~249\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~546\ & (\Dout_Mult[15]~238\ # !\Dout_ff|dffs[14]~249\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_add_Sub|adder|result_node|cs_buffer[8]~546\,
	datab => \Dout_Mult[15]~238\,
	cin => \Dout_ff|dffs[14]~249\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[15]\,
	cout => \Dout_ff|dffs[15]~252\);

\Dout_ff|dffs[16]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[16]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[16]~239\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\ $ \Dout_ff|dffs[15]~252\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[16]~255\ = CARRY(\Dout_Mult[16]~239\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\ & !\Dout_ff|dffs[15]~252\ # !\Dout_Mult[16]~239\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~550\ # !\Dout_ff|dffs[15]~252\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[16]~239\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~550\,
	cin => \Dout_ff|dffs[15]~252\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[16]\,
	cout => \Dout_ff|dffs[16]~255\);

\Dout_Mult[10]~233_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[10]~233\ = !offset_en & \Dout_ff|dffs[17]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[17]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[10]~233\);

\Dout_Mult[9]~232_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[9]~232\ = !offset_en & \Dout_ff|dffs[16]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[16]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[9]~232\);

\Dout_ff|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[8]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[8]~231\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\ $ \Dout_ff|dffs[7]~279\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[8]~231\ = CARRY(\Dout_Mult[8]~231\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\ & !\Dout_ff|dffs[7]~279\ # !\Dout_Mult[8]~231\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~518\ # !\Dout_ff|dffs[7]~279\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[8]~231\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~518\,
	cin => \Dout_ff|dffs[7]~279\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[8]\,
	cout => \Dout_ff|dffs[8]~231\);

\Dout_ff|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[9]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_add_Sub|adder|result_node|cs_buffer[8]~522\ $ \Dout_Mult[9]~232\ $ !\Dout_ff|dffs[8]~231\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[9]~234\ = CARRY(\Dout_add_Sub|adder|result_node|cs_buffer[8]~522\ & \Dout_Mult[9]~232\ & !\Dout_ff|dffs[8]~231\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~522\ & (\Dout_Mult[9]~232\ # !\Dout_ff|dffs[8]~231\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_add_Sub|adder|result_node|cs_buffer[8]~522\,
	datab => \Dout_Mult[9]~232\,
	cin => \Dout_ff|dffs[8]~231\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[9]\,
	cout => \Dout_ff|dffs[9]~234\);

\Dout_ff|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[10]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_add_Sub|adder|result_node|cs_buffer[8]~526\ $ \Dout_Mult[10]~233\ $ \Dout_ff|dffs[9]~234\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[10]~237\ = CARRY(\Dout_add_Sub|adder|result_node|cs_buffer[8]~526\ & (!\Dout_ff|dffs[9]~234\ # !\Dout_Mult[10]~233\) # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~526\ & !\Dout_Mult[10]~233\ & !\Dout_ff|dffs[9]~234\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "962B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_add_Sub|adder|result_node|cs_buffer[8]~526\,
	datab => \Dout_Mult[10]~233\,
	cin => \Dout_ff|dffs[9]~234\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[10]\,
	cout => \Dout_ff|dffs[10]~237\);

\Dout_ff|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[11]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[11]~234\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~530\ $ !\Dout_ff|dffs[10]~237\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[11]~240\ = CARRY(\Dout_Mult[11]~234\ & (!\Dout_ff|dffs[10]~237\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~530\) # !\Dout_Mult[11]~234\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~530\ & !\Dout_ff|dffs[10]~237\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[11]~234\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~530\,
	cin => \Dout_ff|dffs[10]~237\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[11]\,
	cout => \Dout_ff|dffs[11]~240\);

\Dout_ff|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[12]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[12]~235\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\ $ \Dout_ff|dffs[11]~240\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[12]~243\ = CARRY(\Dout_Mult[12]~235\ & \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\ & !\Dout_ff|dffs[11]~240\ # !\Dout_Mult[12]~235\ & (\Dout_add_Sub|adder|result_node|cs_buffer[8]~534\ # !\Dout_ff|dffs[11]~240\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "964D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[12]~235\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~534\,
	cin => \Dout_ff|dffs[11]~240\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[12]\,
	cout => \Dout_ff|dffs[12]~243\);

\Dout_ff|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[13]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[13]~236\ $ \Dout_add_Sub|adder|result_node|cs_buffer[8]~538\ $ !\Dout_ff|dffs[12]~243\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[13]~246\ = CARRY(\Dout_Mult[13]~236\ & (!\Dout_ff|dffs[12]~243\ # !\Dout_add_Sub|adder|result_node|cs_buffer[8]~538\) # !\Dout_Mult[13]~236\ & !\Dout_add_Sub|adder|result_node|cs_buffer[8]~538\ & !\Dout_ff|dffs[12]~243\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[13]~236\,
	datab => \Dout_add_Sub|adder|result_node|cs_buffer[8]~538\,
	cin => \Dout_ff|dffs[12]~243\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[13]\,
	cout => \Dout_ff|dffs[13]~246\);

\Dout_Mult[7]~247_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[7]~247\ = !offset_en & (\Dout_ff|dffs[14]\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "3300",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datab => offset_en,
	datad => \Dout_ff|dffs[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[7]~247\);

\Dout_Mult[6]~249_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[6]~249\ = !offset_en & \Dout_ff|dffs[13]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[6]~249\);

\Dout_Mult[5]~250_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[5]~250\ = !offset_en & \Dout_ff|dffs[12]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[5]~250\);

\Dout_Mult[4]~251_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[4]~251\ = !offset_en & \Dout_ff|dffs[11]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[4]~251\);

\Dout_Mult[3]~252_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[3]~252\ = \Dout_ff|dffs[10]\ & !offset_en

-- pragma translate_off
GENERIC MAP (
	lut_mask => "00F0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[10]\,
	datad => offset_en,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[3]~252\);

\Dout_Mult[2]~253_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[2]~253\ = !offset_en & \Dout_ff|dffs[9]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[2]~253\);

\Dout_ff|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[7]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_ff|dffs[7]\ $ \Dout_Mult[7]~247\ $ !\Dout_ff|dffs[6]~303\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[7]~279\ = CARRY(\Dout_ff|dffs[7]\ & \Dout_Mult[7]~247\ & !\Dout_ff|dffs[6]~303\ # !\Dout_ff|dffs[7]\ & (\Dout_Mult[7]~247\ # !\Dout_ff|dffs[6]~303\))

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "694D",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_ff|dffs[7]\,
	datab => \Dout_Mult[7]~247\,
	cin => \Dout_ff|dffs[6]~303\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[7]\,
	cout => \Dout_ff|dffs[7]~279\);

\Dout_Mult[0]~255_I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_Mult[0]~255\ = !offset_en & \Dout_ff|dffs[7]\

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0F00",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => offset_en,
	datad => \Dout_ff|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \Dout_Mult[0]~255\);

\Dout_ff|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \Dout_ff|dffs[1]\ = DFFE(!GLOBAL(\reset~combout\) & \Dout_Mult[1]~254\ $ \Dout_ff|dffs[1]\ $ !\Dout_ff|dffs[0]~324\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )
-- \Dout_ff|dffs[1]~321\ = CARRY(\Dout_Mult[1]~254\ & (!\Dout_ff|dffs[0]~324\ # !\Dout_ff|dffs[1]\) # !\Dout_Mult[1]~254\ & !\Dout_ff|dffs[1]\ & !\Dout_ff|dffs[0]~324\)

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "692B",
	operation_mode => "counter",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Dout_Mult[1]~254\,
	datab => \Dout_ff|dffs[1]\,
	cin => \Dout_ff|dffs[0]~324\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	sclr => \reset~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Dout_ff|dffs[1]\,
	cout => \Dout_ff|dffs[1]~321\);

\AC_Out_FF|dffs[0]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[0]\ = DFFE(\Dout_ff|dffs[8]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[8]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[0]\);

\AC_Out_FF|dffs[1]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[1]\ = DFFE(\Dout_ff|dffs[9]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[9]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[1]\);

\AC_Out_FF|dffs[2]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[2]\ = DFFE(\Dout_ff|dffs[10]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[10]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[2]\);

\AC_Out_FF|dffs[3]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[3]\ = DFFE(\Dout_ff|dffs[11]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[11]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[3]\);

\AC_Out_FF|dffs[4]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[4]\ = DFFE(\Dout_ff|dffs[12]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[12]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[4]\);

\AC_Out_FF|dffs[5]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[5]\ = DFFE(\Dout_ff|dffs[13]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[13]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[5]\);

\AC_Out_FF|dffs[6]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[6]\ = DFFE(\Dout_ff|dffs[14]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[14]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[6]\);

\AC_Out_FF|dffs[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[7]\ = DFFE(\Dout_ff|dffs[15]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[15]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[7]\);

\AC_Out_FF|dffs[8]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[8]\ = DFFE(\Dout_ff|dffs[16]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[16]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[8]\);

\AC_Out_FF|dffs[9]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[9]\ = DFFE(\Dout_ff|dffs[17]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[17]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[9]\);

\AC_Out_FF|dffs[10]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[10]\ = DFFE(\Dout_ff|dffs[18]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "F0F0",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datac => \Dout_ff|dffs[18]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[10]\);

\AC_Out_FF|dffs[11]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[11]\ = DFFE(\Dout_ff|dffs[19]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[19]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[11]\);

\AC_Out_FF|dffs[12]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[12]\ = DFFE(\Dout_ff|dffs[20]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[20]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[12]\);

\AC_Out_FF|dffs[13]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[13]\ = DFFE(\Dout_ff|dffs[21]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[21]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[13]\);

\AC_Out_FF|dffs[14]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[14]\ = DFFE(\Dout_ff|dffs[22]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[22]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[14]\);

\Din[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "from_pin",
	operation_mode => "input",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => GND,
	ena => VCC,
	padio => ww_Din(15),
	combout => \Din[15]~combout\);

\Din_Del1_ff|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \Din_Del1_ff|dffs[15]\ = DFFE(\Din[15]~combout\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Din[15]~combout\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \Din_Del1_ff|dffs[15]\);

\data_add_sub|adder1_0[1]|result_node|sout_node[7]~I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1_0[1]|result_node|sout_node[7]\ = DFFE(\Din_Del2_ff|dffs[15]\ $ (\data_add_sub|adder1_0[1]|result_node|cout[6]\ $ \Din_Del1_ff|dffs[15]\), GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "A55A",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	dataa => \Din_Del2_ff|dffs[15]\,
	datad => \Din_Del1_ff|dffs[15]\,
	cin => \data_add_sub|adder1_0[1]|result_node|cout[6]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \data_add_sub|adder1_0[1]|result_node|sout_node[7]\);

\data_add_sub|adder1[1]|result_node|cs_buffer[0]~236_I\ : apex20ke_lcell
-- Equation(s):
-- \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\ = \data_add_sub|adder1[1]|result_node|cs_buffer[0]~234\ $ \data_add_sub|adder1_0[1]|result_node|sout_node[7]\

-- pragma translate_off
GENERIC MAP (
	cin_used => "true",
	lut_mask => "0FF0",
	operation_mode => "normal",
	output_mode => "comb_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \data_add_sub|adder1_0[1]|result_node|sout_node[7]\,
	cin => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~234\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	combout => \data_add_sub|adder1[1]|result_node|cs_buffer[0]~236\);

\AC_Out_FF|dffs[15]~I\ : apex20ke_lcell
-- Equation(s):
-- \AC_Out_FF|dffs[15]\ = DFFE(\Dout_ff|dffs[23]\, GLOBAL(\clk~combout\), !GLOBAL(\imr~combout\), , )

-- pragma translate_off
GENERIC MAP (
	lut_mask => "FF00",
	operation_mode => "normal",
	output_mode => "reg_only",
	packed_mode => "false")
-- pragma translate_on
PORT MAP (
	datad => \Dout_ff|dffs[23]\,
	clk => \clk~combout\,
	aclr => \imr~combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	regout => \AC_Out_FF|dffs[15]\);

\AC_Out[0]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[0]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(0));

\AC_Out[1]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[1]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(1));

\AC_Out[2]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[2]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(2));

\AC_Out[3]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[3]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(3));

\AC_Out[4]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[4]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(4));

\AC_Out[5]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[5]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(5));

\AC_Out[6]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[6]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(6));

\AC_Out[7]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[7]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(7));

\AC_Out[8]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[8]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(8));

\AC_Out[9]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[9]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(9));

\AC_Out[10]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[10]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(10));

\AC_Out[11]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[11]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(11));

\AC_Out[12]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[12]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(12));

\AC_Out[13]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[13]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(13));

\AC_Out[14]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[14]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(14));

\AC_Out[15]~I\ : apex20ke_io
-- pragma translate_off
GENERIC MAP (
	feedback_mode => "none",
	operation_mode => "output",
	power_up => "low",
	reg_source_mode => "none")
-- pragma translate_on
PORT MAP (
	datain => \AC_Out_FF|dffs[15]\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	devoe => ww_devoe,
	oe => VCC,
	ena => VCC,
	padio => ww_AC_Out(15));
END structure;


