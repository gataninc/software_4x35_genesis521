------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	fir_ac_couple.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules AC Couples the input bus only when the input is near baseline. When it is above baseline
--		the Baseline Correction is disabled, to prevent a downward shift of the output bus.
--		Those that are enabled and not blocked.
--	History
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity fir_ac_couple is 
	port( 	
	     imr       	: in      std_logic;                    -- Master Reset
		clk       	: in      std_logic;			     -- Master Clock Input
		reset		: in		std_logic;
		KSDD_Mode		: in		std_Logic;
		WDS_Mode		: in		std_Logic;
		Din			: in		std_logic_Vector( 15 downto 0 );
		AC_Out		: buffer 	std_logic_Vector( 15 downto 0 ) );
end fir_ac_couple;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of fir_ac_couple is
	constant Acc_Width		: integer := 34;
	constant THR_SDD		: integer := 15; -- was 25;	-- Originally 30;
	constant THR_WDS		: integer := 10;
	constant THR_OTHER		: integer := 50;
	
--	constant Mult_LSB : integer := 8;		-- 6 = 1/1024
--	constant Mult_LSB : integer := 7;		-- 7 = 1/512
--	constant Mult_LSB : integer := 6;		-- 6 = 1/256	( Default )
--	constant Mult_LSB : integer := 5;		-- 5 = 1/128
	constant Mult_LSB : integer := 4;		-- 4 = 1/64
--	constant Mult_LSB : integer := 3;		-- 3 = 1/32
--	constant Mult_LSB : integer := 2;		-- 2 = 1/16

	signal Vcc			: std_logic;
	signal Gnd			: std_logic;
	signal Din_Dly1		: std_logic_Vector( 15 downto 0 );
	signal Din_Dly2		: std_logic_Vector( 15 downto 0 );

	signal diff_reg		: std_logic_Vector( 15 downto 0 );
	signal Abs_diff		: std_logic_vector( 15 downto 0 );
	signal Threshold		: std_logic_vector( 15 downto 0 );

	signal abs_thr			: std_logic;
	signal Diff_Vec		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout_Reg		: std_logic_vector( Acc_Width-1 downto 0 );
	signal Acc_Sum1 		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Acc_Sum2 		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout_Mult 		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout_Mult_Mux 	: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Abs_Thr_Vec		: std_Logic_Vector(  7 downto 0 );
begin
	Threshold	<= Conv_Std_logic_Vector( THR_SDD, 16 ) when ( KSDD_Mode = '1' ) else
			   Conv_Std_logic_Vector( THR_WDS, 16 ) when ( WDS_Mode = '1' ) else
			   conv_Std_logic_Vector( THR_OTHER, 16 );
	
--	Threshold	<= Conv_Std_logic_Vector( 10, 16 );
	
     --------------------------------------------------------------------------
	-- First Generate diff_regerence Vector from the Preset input and the previous input
	Vcc	<= '1';
	Gnd	<= '0';

	Din_Del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din,
			q				=> Din_Dly1 );

	Din_Del2_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din_dly1,
			q				=> Din_Dly2 );
			
	data_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			Cin				=> Vcc,
			add_sub			=> GND,	-- Subtract
			dataa 			=> din,
			datab			=> din_dly2,
			result			=> diff_reg );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Threshold Comparison
	diff_reg_Abs : lpm_abs
		generic map(
			LPM_WIDTH			=> 16,
			LPM_TYPE			=> "LPM_ABS" )
		port map(
			data				=> diff_reg,
			result			=> abs_diff );
	
	abs_cmp : lpm_compare
		generic map(
			lpm_width			=> 16,
			LPM_representation	=> "signed",
			lpm_type			=> "lpm_compare" )
		port map(
			dataa			=> abs_diff,
			datab			=> Threshold,
			ageb				=> abs_thr );
	-- First Generate diff_regerence Vector from the Preset input and the previous input
     --------------------------------------------------------------------------

		
     --------------------------------------------------------------------------
	-- Next Accumulate all of the diff_regerences
	-- Multiply Diff_Reg by 2^15 
	Diff_Vec_gena : for i in 0 to 15 generate
		Diff_Vec(i) <= '0';
	end generate;
	
	Diff_Vec_genb : for i in 16 to 16+14 generate
		diff_vec(i) <= Diff_Reg(i-16);
	end generate;
	
	Diff_Vec_genc : for i in 16+14+1 to Acc_Width-1 generate
		Diff_Vec(i)	<= Diff_Reg(15);
	end generate;

	PAcc_Add : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> Gnd,
			add_sub			=> Vcc,				-- Add
			dataa			=> Dout_Reg,
			datab			=> diff_Vec,
			Result			=> Acc_Sum1 );

	-- Multiply by 1/256 ------------------------------------------
	Dout_Mult_Gen_L : for i in 0 to Mult_LSB generate
		Dout_Mult(i) 			<= '0';
	end generate;
	
	Dout_Mult_gen_M : for i in 0 to 15 generate
		Dout_Mult(i+Mult_LSB+1) 	<= Dout_Reg(15+1+i);
	end generate;
	
	Dout_Mult_gen_H : for i in 15+1+Mult_LSB+1 to Acc_Width-1 generate
		Dout_Mult(i) 			<= Dout_Reg(14+1+14);
	end generate;
	-- Multiply by 1/256 ------------------------------------------

	Dout_Mult_Mux_Gen : for i in 0 to Acc_Width-1 generate
		Dout_Mult_Mux(i) <= DOut_Mult(i) when ( Conv_Integer( Abs_Thr_Vec ) = 0 ) else '0';
	end generate;

	PAcc_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_TYPE			=> "LPM_ADD_SUB" )
		port map(
			Cin				=> Vcc,
			add_sub			=> Gnd,				-- Subtract
			dataa			=> Acc_Sum1,
			datab			=> Dout_Mult_Mux,
			Result			=> Acc_Sum2 );

	PAcc_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_TYPE			=> "LPM_FF" )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> Reset,
			data				=> Acc_sum2,
			q				=> Dout_Reg );
			
	AC_Out <= Dout_Reg( 18+15 downto 18 );
	-- Next Accumulate all of the diff_regerences
     --------------------------------------------------------------------------
	
     --------------------------------------------------------------------------
	-- Generate a Vector when it is OK to adjust the Baseline in the absense of an input pulse.
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			Abs_Thr_Vec <= x"00";
		elsif(( Clk'Event ) and ( CLK = '1' )) then
			Abs_Thr_Vec	<= Abs_Thr_Vec( 6 downto 0 ) & Abs_Thr;
		end if;
	end process;
-------------------------------------------------------------------------------
end behavioral;               -- fir_ac_couple
-------------------------------------------------------------------------------