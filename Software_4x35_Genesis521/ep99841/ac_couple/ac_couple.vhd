------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ac_couple.VHD
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:
--		This modules AC Couples the input bus only when the input is near baseline. When it is above baseline
--		the Baseline Correction is disabled, to prevent a downward shift of the output bus.
--		Those that are enabled and not blocked.
--	History
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
library LPM;
     use lpm.lpm_components.all;

library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_logic_arith.all;

-------------------------------------------------------------------------------
entity ac_couple is 
	port( 	
	     imr       	: in      std_logic;                    -- Master Reset
		clk       	: in      std_logic;			     -- Master Clock Input
		reset		: in		std_logic;
		Din			: in		std_logic_Vector( 15 downto 0 );
		AC_Out		: buffer 	std_logic_Vector( 15 downto 0 ) );
end ac_couple;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
architecture behavioral of ac_couple is
	constant THRESHOLD		: integer := 5; -- 7;  -- was 10

	constant Dout_LSB		: integer := 8;
	constant Div_Bit		: integer := 8;
	constant FB_Bit 		: integer := 7;
	constant Acc_Width 		: integer := 32; -- 16+Div_Bit; 
	
	signal Din_Dly1		: std_logic_Vector( 15 downto 0 );
	signal Din_Dly2		: std_logic_Vector( 15 downto 0 );
	signal diff_reg		: std_logic_Vector( 15 downto 0 );
	signal Abs_diff		: std_logic_vector( 15 downto 0 );

	signal abs_thr			: std_logic;
	signal Abs_Thr_Vec		: std_Logic_Vector(  7 downto 0 );
	signal offset_en		: std_logic;
	signal Dout_sum		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout			: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout_Mult		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Dout_Diff		: std_logic_Vector( Acc_Width-1 downto 0 );
	signal Diff_reg_Mult 	: std_logic_vector( Acc_Width-1 downto 0 );

begin
     --------------------------------------------------------------------------
	-- First Generate diff_regerence Vector from the Preset input and the previous input
	Din_Del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din,
			q				=> Din_Dly1 );
			
	Din_Del2_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Din_Dly1,
			q				=> Din_Dly2 );

	data_add_sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED",
			LPM_PIPELINE		=> 1 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			Cin				=> '1',
			dataa 			=> din_dly1,
			datab			=> din_dly2,
			result			=> diff_reg );
     --------------------------------------------------------------------------

     --------------------------------------------------------------------------
	-- Threshold Comparison
	diff_reg_Abs : lpm_abs
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			data				=> diff_reg,
			result			=> abs_diff );
	
	abs_cmp : lpm_compare
		generic map(
			lpm_width			=> 16,
			LPM_representation	=> "signed" )
		port map(
			dataa			=> abs_diff,
			datab			=> conv_Std_logic_Vector( THRESHOLD, 16 ),
			agb				=> abs_thr );
	-- First Generate diff_regerence Vector from the Preset input and the previous input
     --------------------------------------------------------------------------

	-- 2^Mult Diff -----------------------------------------------------------
	DRM_A: for i in 0 to Div_Bit-1 generate
		Diff_Reg_Mult(i) <= '0';
	end generate;
	
	DRM_B : for i in 0 to 15 generate
		Diff_Reg_Mult(i+Div_Bit) <= Diff_Reg(i);
	end generate;
	
	DRM_C : for i in Div_Bit+16 to Acc_Width-1 generate
		Diff_Reg_Mult(i) <= Diff_Reg(15);
	end generate;
	
--	DRM_B: for i in Div_Bit to Acc_Width-1 generate
--		Diff_Reg_Mult(i) <= Diff_Reg(i-Div_Bit);
--	end generate;
	-- 2^Mult Diff -----------------------------------------------------------
	
	-- Add Dout to Mult Diff Reg ----------------------------------------------
	Dout_add_Sub : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '0',
			dataa			=> Dout,
			datab			=> Diff_reg_Mult,
			result			=> Dout_Sum );
	-- Add Dout to Mult Diff Reg ----------------------------------------------

	-- Dout/2^FB_Bit Feedback to be subtracted off of the final output when below threshold --------
	doma : for i in 0 to Acc_Width-1-FB_Bit generate
		Dout_Mult(i) <= Dout(i+FB_Bit) when ( Offset_En = '0' ) else '0';
	end generate;

	domb : for i in Acc_Width-1-(FB_Bit-1) to Acc_Width-1 generate
		Dout_Mult(i) <= Dout(Acc_Width-1) when ( Offset_En = '0' ) else '0';
	end generate;
	-- Dout/2^FB_Bit Feedback to be subtracted off of the final output --------
	
	Dout_Dif_as : lpm_add_sub
		generic map(
			LPM_WIDTH			=> Acc_Width,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "SIGNED" )
		port map(
			cin				=> '1',
			dataa			=> Dout_Sum,
			datab			=> Dout_Mult,
			result			=> Dout_Diff );
	
	Dout_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> Acc_Width )
		port map(
			aclr				=> imr,
			clock			=> clk,
			sclr				=> reset,
			data				=> Dout_Diff,
			q				=> Dout );

	AC_Out_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> dout( Dout_LSB+15 downto Dout_LSB ),
			q				=> AC_Out );
			
    --------------------------------------------------------------------------
	-- Generate a Vector when it is OK to adjust the Baseline in the absense of an input pulse.
	clock_proc : process( imr, clk )
	begin
		if( imr = '1' ) then
			Abs_Thr_Vec 	<= x"00";
			offset_en		<= '0';
		elsif(( Clk'Event ) and ( CLK = '1' )) then
			Abs_Thr_Vec	<= Abs_Thr_Vec( 6 downto 0 ) & Abs_Thr;
			if( conv_integer( abs_thr_vec ) = 0 )
				then offset_en <= '0';
				else offset_en <= '1';
			end if;
		end if;
	end process;
-------------------------------------------------------------------------------
end behavioral;               -- ac_couple
-------------------------------------------------------------------------------
	

