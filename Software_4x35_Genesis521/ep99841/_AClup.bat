del *.vqm
del *.esf
del *.pof
del *.htm
del *.rpt
goto exit
del *.ssd
del *.txt
del *.fsf
del *.ini
del transcript.*
del dfp_param.*
del db\*.* /q
del ls_work\*.* /q
del .work.*
del *.xdb
rem del *.sof
rem del *.hexout

cd ad9240
call _aclup.bat
cd ..

cd ads8321
call _aclup.bat
cd ..

cd bit_dac
call _aclup.bat
cd ..

cd dac_int
call _aclup.bat
cd ..

cd ds2401
call _aclup.bat
cd ..

cd dspint
call _aclup.bat
cd ..

cd fifo
call _aclup.bat
cd ..

cd fir_ad_avg
call _aclup.bat
cd ..

cd fir_auto_disc
call _aclup.bat
cd ..

cd fir_beam_blank
call _aclup.bat
cd ..

cd fir_blr
call _aclup.bat
cd ..

cd fir_cntrate
call _aclup.bat
cd ..

cd fir_disc
call _aclup.bat
cd ..

cd fir_discCnts
call _aclup.bat
cd ..

cd fir_dmem
call _aclup.bat
cd ..

cd fir_edisc
call _aclup.bat
cd ..

cd fir_gauss
call _aclup.bat
cd ..

cd fir_gen2
call _aclup.bat
cd ..

cd fir_mem2
call _aclup.bat
cd ..

cd fir_pproc
call _aclup.bat
cd ..

cd fir_psteer
call _aclup.bat
cd ..

cd fir_reset
call _aclup.bat
cd ..

cd fir_scactrl
call _aclup.bat
cd ..

cd fir_shdisc_dly
call _aclup.bat
cd ..

cd ltc1595
call _aclup.bat
cd ..

cd rtemctrl
call _aclup.bat
cd ..

cd tmp04
call _aclup.bat
cd ..

: exit