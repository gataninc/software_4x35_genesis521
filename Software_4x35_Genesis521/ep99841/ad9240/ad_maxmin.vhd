------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	ad_maxmin
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--	Description:	
--		This module monitors the input data stream and determines the max and min values
--		based on an adaptive decay approach that latches the max and slowly drops it back 
--		to baseline
--
--		During a PreAmp reset, it does not update the value
--
--	History
--		06/21/05 - MCS
--			Created with for QuartusII Version 5.0 SP 0.21
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
library lpm;
	use lpm.lpm_components.all;
	
library ieee;
	use ieee.std_logic_1164.all;
     use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

------------------------------------------------------------------------------------
entity ad_maxmin is 
	port( 
		imr			: in		std_logic;					-- Master Reset
		clk			: in		std_logic;					-- MAster Clock
		Din			: in		std_logic_vector( 15 downto 0 );
		Max			: buffer	std_logic_Vector( 15 downto 0 );
		Min			: buffer	std_logic_vector( 15 downto 0 ) );
end ad_maxmin;
------------------------------------------------------------------------------------

------------------------------------------------------------------------------------
architecture behavioral of ad_maxmin is
	constant DECAY				: std_logic_Vector( 31 downto 0 ) := x"00000010";
	constant Reset_End_Cnt_Max 	: integer := 15;

	signal Din_Vec			: std_Logic_Vector( 31 downto 0 );

	signal Max_Cmp			: std_Logic;
	signal Min_Cmp			: std_Logic;

	signal Max_val			: std_logic_Vector( 31 downto 0 );
	signal Min_Val			: std_logic_Vector( 31 downto 0 );

	signal Max_sum			: std_logic_Vector( 31 downto 0 );
	signal Min_sum			: std_logic_Vector( 31 downto 0 );
	
	signal Max_Mux			: std_logic_Vector( 31 downto 0 );
	signal Min_Mux			: std_logic_Vector( 31 downto 0 );
	
begin

	------------------------------------------------------------------
	-- Sign Extend Din to 15 bits
	-- Multiply Din By 0x10000
	Din_Vec_FF : LPM_FF 
		generic map(
			LPM_WIDTH		=> 16 )
		port map(
			aclr			=> imr,
			clock		=> clk,
			data			=> din,
			q			=> Din_Vec( 31 downto 16 ) );

	Din_Vec( 15 downto 0 ) <= x"0000";
	------------------------------------------------------------------
	
	------------------------------------------------------------------
	-- Max Compare
	MAX_CMP_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Din_Vec,
			datab			=> Max_Val,
			agb				=> Max_Cmp );
	-- Max Compare
	------------------------------------------------------------------

	------------------------------------------------------------------
	
	MAX_SUM_ADD_SUB : LPM_ADD_SUB
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "SUB",
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			cin				=> '1',
			dataa			=> Max_val,
			datab			=> Decay,
			result			=> Max_Sum );

	Max_Mux	<= Din_Vec when ( Max_Cmp   = '1' ) else 
			   Max_Sum;

	MAX_val_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Max_Mux,
			q				=> Max_Val );

	-- Final Output Registers ----------------------------------------
	Max_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )	
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> max_val( 31 downto 16 ),
			q				=> Max );
	------------------------------------------------------------------

	------------------------------------------------------------------
	-- Min Compare
	MIN_CMP_COMPARE : lpm_compare
		generic map(
			LPM_WIDTH			=> 32,
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			dataa			=> Din_Vec,
			datab			=> Min_Val,
			alb				=> Min_Cmp );
	-- Min Compare
	------------------------------------------------------------------

	------------------------------------------------------------------
	MIN_SUM_ADD_SUB : LPM_ADD_SUB
		generic map(
			LPM_WIDTH			=> 32,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			cin				=> '0',
			dataa			=> Min_val,
			datab			=> Decay,
			result			=> Min_Sum );
	------------------------------------------------------------------

	Min_Mux	<= Din_Vec when ( Min_Cmp 	= '1' ) else 
		        Min_sum;
	
	Min_val_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 32 )
		port map(
			aclr				=> imr,
			clock			=> clk,
			data				=> Min_Mux,
			q				=> Min_Val );
			
	------------------------------------------------------------------
	
	------------------------------------------------------------------
	-- Final Output Registers ----------------------------------------
	Min_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )	
		port map(	
			aclr				=> imr,
			clock			=> clk,
			data				=> Min_val( 31 downto 16 ),
			q				=> Min );

	-- Final Output Registers ----------------------------------------
	------------------------------------------------------------------

------------------------------------------------------------------------------------
end behavioral;		-- ad_maxmin
------------------------------------------------------------------------------------
