---------------------------------------------------------------------------------------------------
--	EDAX Inc
--	91 McKee Drive
--	Mahwah, NJ 07430
--
--	File:	AD9240.VHD
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
--  	Author         : Michael C. Solazzi
--   Created        : March 3, 1999
--   Board          : DPP-II EDS FPGA
--   Schematic      : 4035.007.19700 Rev C
--
--	Top Level		: 4035.003.99840 S/W, DPP-II, EDS FPGA
--
--   Description:
--		Code to interface to the Analog Devices AD9240 A/D Converter
--		but has the additional logic for Saturated Detection and PreAmp Reset
--		The AD9240 outputs a "straight" binary code, it must be converted to "Signed" by inverting the MSB
--
--   History:       <date> - <Author>
--		08/03/05 - MCS - Ver 356F
--			Enabled Detector Reset Circuitry
--		06/15/05 - MCS
--			Updated for Quartus II Ver 5.0 SP 0.21
--		03/13/02 - MCS
--			Updated for QuartusII Version 2.0
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
library LPM;
	use lpm.lpm_components.all;

library IEEE;
	use ieee.std_logic_1164.all;
	use ieee.std_logic_unsigned.all;
	use ieee.std_Logic_arith.all;

---------------------------------------------------------------------------------------------------
entity AD9240 is 
	port( 
		IMR			: in		std_Logic;					-- Master Reset
		CLK20		: in		std_logic;					-- 5Mhz Clock ( from 20Mhz )
		OTR			: in		std_logic;					-- Out of Range
		ADout		: in		std_logic_vector( 13 downto 0 );	-- A/D Data Output
		raw_ad		: buffer	std_Logic_Vector( 15 downto 0 );
		CLK_FAD		: buffer	std_logic;					-- Clock to A/D Converter
		Dout			: buffer	std_logic_Vector( 15 downto 0 );	-- Buffered A/D output
		Max			: buffer	std_logic_Vector( 15 downto 0 );
		Min			: buffer	std_logic_vector( 15 downto 0 ) );
end AD9240;
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
architecture behavioral of AD9240 is

	constant Dout_LSB 		: integer := 14;		
	constant Pos_Thr 		: integer := 14000;	-- 7500;
	constant Neg_thr		: integer := 7000;	-- -1250;

	signal clamp_hi		: std_logic;
	signal clamp_lo		: std_logic;
	signal clk_fad_n		: std_logic;

	signal Dout_del1 		: std_logic_Vector( 13 downto 0 );
	signal Dout_del2 		: std_logic_Vector( 13 downto 0 );
	signal dout_sum		: std_logic_vector( 15 downto 0 );
	signal product			: std_Logic_vector( 31 downto 0 );

	------------------------------------------------------------------------------------
	component ad_maxmin is 
		port( 
			imr			: in		std_logic;					-- Master Reset
			clk			: in		std_logic;					-- MAster Clock
			Din			: in		std_logic_vector( 15 downto 0 );
			Max			: buffer	std_logic_Vector( 15 downto 0 );
			Min			: buffer	std_logic_vector( 15 downto 0 ) );
	end component ad_maxmin;
	------------------------------------------------------------------------------------

begin
    ------------------------------------------------------------------------------------	
	-- Actual Inteface to the A/D converter --------------------------------------------
	------------------------------------------------------------------------------------	
	-- Sense Out-of-Range from A/D Converter -------------------------------------------			
	clamp_hi		<= '1' when ( OTR = '1' ) and ( ADout(13) = '1' ) else '0';
	clamp_lo 		<= '1' when ( OTR = '1' ) and ( ADout(13) = '0' ) else '0';

	dout_del_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			sset				=> clamp_hi,
			sclr				=> clamp_lo,
			enable			=> clk_fad_n,
			data				=> adout, 
			q				=> dout_del1 );
	-- Sense Out-of-Range from A/D Converter -------------------------------------------
	------------------------------------------------------------------------------------
	-------------------------------------------------------------------------------
	AD_CLK_Proc : process( imr, clk20 )
	begin
		if( imr = '1' ) then
			CLK_FAD		<= '0';
			CLK_FAD_n		<= '1';
		elsif(( CLK20'Event ) and ( CLk20 = '1' )) then
			CLK_FAD		<= not( CLK_FAD );
			clk_FAD_n		<= CLK_FAD;
		end if;
	end process;
	-------------------------------------------------------------------------------

	-- Actual Inteface to the A/D converter --------------------------------------------
	------------------------------------------------------------------------------------

	------------------------------------------------------------------------------------
	-- From the Raw A/D, determine Max Min values
	raw_ad	<= "00" & dout_del1;			-- Output A/D value, before any alterations		

	U_ADMM: ad_maxmin
		port map(
			imr				=> imr,
			clk				=> clk20,
			Din				=> Raw_ad,
			Max				=> Max,
			Min				=> Min );
	-- From the Raw A/D, determine Max Min values
	------------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Pipeline Dout to create the sum
	dout_del1_ff : lpm_ff
		generic map(
			LPM_WIDTH			=> 14 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> dout_del1,
			q				=> dout_del2 );
	-------------------------------------------------------------------------------
	
	-------------------------------------------------------------------------------
	-- Add two Pipelines of Dout for the sum
	dout_sum_ff : lpm_add_sub
		generic map(
			LPM_WIDTH			=> 16,
			LPM_DIRECTION		=> "ADD",
			LPM_REPRESENTATION	=> "UNSIGNED" )
		port map(
			Cin				=> '0',
			dataa			=> "00" & dout_del1,
			datab			=> "00" & dout_del2,
			result			=> dout_sum );
	-------------------------------------------------------------------------------

	-------------------------------------------------------------------------------
	-- Final Output
	Dout_FF : lpm_ff
		generic map(
			LPM_WIDTH			=> 16 )
		port map(
			aclr				=> imr,
			clock			=> clk20,
			data				=> not( Dout_Sum(15)) & Dout_Sum( 14 downto 0 ),
			q				=> dout );
	-------------------------------------------------------------------------------

	----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
end behavioral;			-- AD9240.VHD
---------------------------------------------------------------------------------------------------

